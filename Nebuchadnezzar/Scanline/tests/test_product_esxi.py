import unittest
from mock import MagicMock, patch


class ScanlineESXiProductScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_host = MagicMock(name='scanline_host')
        self.mock_pyVim = MagicMock(name='pyVim')
        self.mock_pyVmomi = MagicMock(name='pyVmomi')
        modules = {
            'pyVim': self.mock_pyVim,
            'pyVmomi': self.mock_pyVmomi,
            'scanline.host.esxi': self.mock_host.esxi
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.esxi import ESXiProductScanner
        self.ESXiProductScanner = ESXiProductScanner
        self.esxi_scanner = self.ESXiProductScanner('ESXi', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], product_id='PID')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hosts(self):
        self.service_instance = self.mock_pyVim.connect.SmartConnect.return_value
        content = self.service_instance.RetrieveContent.return_value
        container_view = content.viewManager.CreateContainerView.return_value
        container_view.view = ['host1', 'host2']
        self.assertEqual(
            list(self.esxi_scanner.get_hosts()),
            ['host1', 'host2']
        )
        self.mock_pyVim.connect.SmartConnect.assert_called_once_with(host='10.220.3.1', pwd='CryptThis', user='tester')
        content.viewManager.CreateContainerView.assert_called_once_with(
            container=content.rootFolder,
            type=[self.mock_pyVmomi.vim.HostSystem],
            recursive=True
        )
        self.mock_pyVim.connect.Disconnect.assert_called_once_with(self.service_instance)

    @patch('scanline.product.esxi.ESXiHostScanner')
    def test_get_hostname(self, mock_esxi_scanner):
        mock_content = MagicMock(name='content')
        mock_propertyCollector = MagicMock(name='propertyCollector')
        self.esxi_scanner.service_instance = MagicMock()
        self.esxi_scanner.service_instance.content = mock_content
        self.esxi_scanner.service_instance.content.propertyCollector = mock_propertyCollector
        mock_host = MagicMock()
        self.assertEqual(
            self.esxi_scanner.get_hostname(mock_host),
            mock_esxi_scanner.get_hostname.return_value
        )
        mock_esxi_scanner.get_hostname.assert_called_once_with(mock_host)

    @patch('scanline.product.esxi.ESXiHostScanner')
    def test_get_hostname_fqdn(self, mock_esxi_scanner):
        mock_content = MagicMock(name='content')
        mock_propertyCollector = MagicMock(name='propertyCollector')
        self.esxi_scanner.service_instance = MagicMock()
        self.esxi_scanner.service_instance.content = mock_content
        self.esxi_scanner.service_instance.content.propertyCollector = mock_propertyCollector
        self.esxi_scanner.get_formatted_hostname = MagicMock(return_value='abc.pqr.esx.xyz')
        mock_host = MagicMock()
        self.assertEqual(
            self.esxi_scanner.get_hostname(mock_host),
            'abc.pqr.esx.xyz'
        )

    def test_get_formatted_hostname(self):
        result = """(vim.host.NetStackInstance) {
                           dynamicType = <unset>,
                           dynamicProperty = (vmodl.DynamicProperty) [],
                           key = 'defaultTcpipStack',
                           name = 'defaultTcpipStack',
                           dnsConfig = (vim.host.DnsConfig) {
                              dynamicType = <unset>,
                              dynamicProperty = (vmodl.DynamicProperty) [],
                              dhcp = false,
                              virtualNicDevice = <unset>,
                              hostName = 'abcdef',
                              domainName = 'pqr.esx.xyz',
                              address = (str) [],
                              searchDomain = (str) [
                                 'pqr.esx.xyz'
                              ]
                           },
                           ipRouteConfig = (vim.host.IpRouteConfig) {
                              dynamicType = <unset>,
                              dynamicProperty = (vmodl.DynamicProperty) [],
            """
        self.assertEqual(
            self.esxi_scanner.get_formatted_hostname(result),
            'abcdef.pqr.esx.xyz'
        )

    def test_create_filter_spec(self):
        self.assertEqual(self.esxi_scanner.create_filter_spec('vm','propset'), self.mock_pyVmomi.vmodl.query.PropertyCollector.FilterSpec.return_value)
        self.mock_pyVmomi.vmodl.query.PropertyCollector.ObjectSpec.assert_called_once_with(obj='vm')
        self.mock_pyVmomi.vmodl.query.PropertyCollector.FilterSpec.assert_called_once_with()
        self.mock_pyVmomi.vmodl.query.PropertyCollector.PropertySpec.assert_called_once_with(all=False)

    @patch('scanline.product.esxi.ESXiHostScanner')
    def test_scan_host(self, mock_esxi_scanner):
        mock_esxi_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'ESXi': {'manufacturer': 'IBM', 'model': 'X3550'}
        }
        self.esxi_scanner.get_hostname = MagicMock(return_value='host1')
        self.esxi_scanner.host_scanner = MagicMock()
        self.esxi_scanner.host_scanner.return_value.to_dict.return_value = {'ModR': {'key1': 'val1'}}
        mock_host = MagicMock()
        self.assertEqual(
            self.esxi_scanner.scan_host(mock_host),
            {
                'ESXi': {'manufacturer': 'IBM', 'model': 'X3550'},
                'ModR': {'key1': 'val1'},
                'address': '10.4.5.6',
                'product_id': 'PID'
            }
        )
        self.esxi_scanner.get_hostname.assert_called_once_with(mock_host)
        mock_esxi_scanner.assert_called_once_with(
            endpoint={'scanner': 'ESXi', 'address': '10.220.3.1'},
            vmware_host=mock_host,
            tags=['Prod'],
        )
        self.esxi_scanner.host_scanner.assert_called_once_with(
            endpoint={'scanner': 'ESXi', 'address': '10.220.3.1'},
            hostname='host1',
            password='CryptThis',
            tags=['Prod'],
            username='tester'
        )


if __name__ == '__main__':
    unittest.main()
