import unittest
from mock import MagicMock, patch


class ScanlineHostAWAHDSTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.aws_archive_hd import AWAHDSHostScanner
        self.AWAHDSHostScanner = AWAHDSHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_property(self):
        self.assertEqual(self.AWAHDSHostScanner.module_name , 'AdvancedWorkflowArchiveHDServices')
        self.assertEqual(self.AWAHDSHostScanner.productid , 'AWS')
        self.assertEqual(self.AWAHDSHostScanner.productname , 'AdvancedWorkflowServices')
        self.assertEqual(self.AWAHDSHostScanner.productversion , '')


if __name__ == '__main__':
    unittest.main()
