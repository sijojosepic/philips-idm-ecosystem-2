import unittest
from mock import MagicMock, patch


class ScanlineHostSHDBTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.shdb import SHDBHostScanner
        self.SHDBHostScanner = SHDBHostScanner
        self.get_version = SHDBHostScanner.get_version

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_facts(self):
        scanner = self.SHDBHostScanner('host1', '192.168.59.41', None, None, None, tags=['x', 'y'])
        mock_scanner = MagicMock(spec=scanner)
        for prop in self.SHDBHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.SHDBHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))

    def test_get_version(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'version': u'1.0'};
        self.mock_request.get.return_value = resp_mock
        resp = self.get_version('host1', 8866, False)
        self.mock_request.get.assert_called_once_with(
            'http://host1:8866/SEService/_status', headers={'accept': 'application/v1+json'}, verify=False)
        self.assertEqual(resp, (u'1.0'))

    def test_get_version_error(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'version': u'1.0'};
        self.mock_request.get.return_value = resp_mock
        resp_mock.raise_for_status.side_effect = Exception('Exception')
        resp = self.get_version('host1', 8866, False)
        self.mock_request.get.assert_called_once_with(
            'http://host1:8866/SEService/_status', headers={'accept': 'application/v1+json'}, verify=False)
        self.assertEqual(resp, ('NA'))

if __name__ == '__main__':
    unittest.main()
