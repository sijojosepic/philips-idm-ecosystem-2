import unittest

from mock import MagicMock, patch, PropertyMock, call


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""
    extra_params = {'address': '10.1.0.1', 'endpoint': {'enp': 'enp'}}

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    if name == '__init__':
                        setattr(cls, name, lambda *args, **kwargs: None)
                    else:
                        setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
            for k, v in Fake.extra_params.iteritems():
                setattr(cls, k, v)
        return cls


class ZVMHostTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import HostScanner
        from scanline.product.disaster_recovery.zvm import ZVMHost
        ZVMHost.__bases__ = (Fake.imitate(HostScanner),)
        self.mock_site = MagicMock(name='site_mcok')
        endpoint = {'address': '10.5.6.7', 'scanner': 'scan1'}
        self.module = ZVMHost('host1', endpoint, self.mock_site, **{})

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        module_name = 'DR'
        self.assertEqual(self.module.module_name, module_name)

    def test_general_properties(self):
        general_properties = frozenset(['tags', 'product_version', 'vcenters', 'address', 'credentials', 'site_type',
                                        'product_id', 'hostname', 'role', 'zerto_attrs', 'product_name',
                                        'environmentType'])
        self.assertEqual(self.module.general_properties, general_properties)

    def test_zerto_attrs(self):
        self.assertEqual(self.module.zerto_attrs,
                         self.mock_site.values)

    def test_site_type(self):
        self.mock_site.values = {'Link': {'type': 'type'}}
        self.assertEqual(self.module.site_type,
                         'type')

    def test_credentials(self):
        self.mock_site.values = {'Link': {'type': 'type'}}
        self.module.get_credentials = MagicMock(name='get_credentials')
        self.assertEqual(self.module.credentials,
                         self.module.get_credentials.return_value)

    def test_get_credentials(self):
        self.module.kwargs = {'username': 'u1', 'password': 'p1',
                              'zerto_username': 'zu1',
                              'zerto_password': 'zp1'}
        encrypt_val = {'u1': 'usr', 'p1': 'pwd',
                       'zu1': 'zusr', 'zp1': 'zpwd'}
        self.module.encrypter.encrypt.side_effect = lambda x: encrypt_val[x]
        exp_resp = {'username': 'usr', 'password': 'pwd',
                    'zerto_username': 'zusr',
                    'zerto_password': 'zpwd'}
        self.assertEqual(self.module.get_credentials(),
                         exp_resp)


if __name__ == '__main__':
    unittest.main()
