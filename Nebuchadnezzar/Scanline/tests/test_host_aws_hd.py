import unittest
from mock import MagicMock, patch


class ScanlineHostAWSHDTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self. mock_http = MagicMock(name = 'scanline.utilities.http')
        self.mock_requests = MagicMock(name = 'requests')
        self.mock_lxml = MagicMock(name='lxml')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities.http': self.mock_http,
            'scanline.utilities.http.HTTPRequester': self.mock_http.HTTPRequester,
            'requests': self.mock_requests,
            'lxml': self.mock_lxml
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.aws_hd import AWSHDHostScanner
        self.AWSHDHostScanner = AWSHDHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_properties_present(self):
        scanner = self.AWSHDHostScanner('host1', '167.81.183.99', username='user', password='secret',
                                                           tags=['x', 'y'])
        mock_scanner = MagicMock(spec=scanner)
        for prop in self.AWSHDHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.AWSHDHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))

class ScanlineHostAWSHDTestCaseWithNoException(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self. mock_http = MagicMock(name = 'scanline.utilities.http')
        self.mock_requests = MagicMock(name = 'requests')
        self.mock_lxml = MagicMock(name='lxml')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities.http': self.mock_http,
            'scanline.utilities.http.HTTPRequester': self.mock_http.HTTPRequester,
            'requests': self.mock_requests,
            'lxml': self.mock_lxml
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.aws_hd import AWSHDHostScanner
        self.AWSHDHostScanner = AWSHDHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_okay_in_response(self):
        obj = MagicMock()
        obj.text.lower = MagicMock(return_value='okay')
        obj.text.encode = MagicMock(return_value='okay')
        self.mock_http.HTTPRequester().suppressed_get = MagicMock(return_value=obj)
        obj2 = MagicMock()
        obj2.iter = MagicMock(return_value='x')
        self.mock_lxml.etree.fromstring = MagicMock(return_value=obj2)
        scanner = self.AWSHDHostScanner('host1', '167.81.183.99', username='user', password='secret',
                                                           tags=['x', 'y'])


if __name__ == '__main__':
    unittest.main()
