import unittest
from mock import MagicMock, patch


class ScanlineAuthTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_hmac = MagicMock(name='hmac')
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_base64 = MagicMock(name='base64')
        modules = {
            'logging': MagicMock(name='logging'),
            'hmac': self.mock_hmac,
            'hashlib': self.mock_hashlib,
            'base64': self.mock_base64,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import auth
        self.auth = auth

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hashkey(self):
        self.assertEqual(
            self.auth.get_hashkey('some_Secret', '2016-09-15T02:45:58.317491'),
            self.auth.base64.b64encode.return_value.decode.return_value
        )
        self.auth.hmac.new.assert_called_once_with(
            'some_Secret',
            digestmod=self.auth.hashlib.sha256,
            msg='2016-09-15T02:45:58.317491'
        )
        self.auth.base64.b64encode.assert_called_once_with(
            self.auth.hmac.new.return_value.digest.return_value
        )


if __name__ == '__main__':
    unittest.main()
