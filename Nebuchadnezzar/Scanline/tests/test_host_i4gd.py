import unittest
from mock import MagicMock, patch


class ScanlineHostI4GDTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.i4gd import I4GDHostScanner
        self.I4GDHostScanner = I4GDHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        self.assertEqual(self.I4GDHostScanner.module_name, 'I4GD')



if __name__ == '__main__':
    unittest.main()