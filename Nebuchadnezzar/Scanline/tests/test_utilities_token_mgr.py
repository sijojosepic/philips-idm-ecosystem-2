import unittest
from mock import MagicMock, patch


class TokenManagerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_hmac = MagicMock(name='hmac')
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_base64 = MagicMock(name='base64')
        self.mock_requests = MagicMock(name='requests')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_StringIO = MagicMock(name='StringIO')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'hmac': self.mock_hmac,
            'hashlib': self.mock_hashlib,
            'base64': self.mock_base64,
            'requests': self.mock_requests,
            'scanline.utilities.config_reader': self.mock_scanline.utilities.config_reader,
            'datetime': self.mock_datetime,
            'datetime.datetime': self.mock_datetime.datetime,

        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import token_mgr
        self.module = token_mgr

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_hmac_identifier(self):
        self.module.get_resource_value = MagicMock(name='get_resource_value')
        self.assertEqual(self.module.HMAC_IDENTIFIERS, {
            'TOKEN': 'iSiteWebApplication',  # Not required
            'TIMESTAMP_LABEL': 'Timestamp',
            'HASH_LABEL': 'Hash',
            'APPLICATIONIDENTIFIER': 'iSiteRadiology',  # Not Required
            'APPID_LABEL': 'AppId',
            'APPID': 'IDM',
            # Loaded from resource.cfg
            'SECRET_KEY': self.mock_scanline.utilities.config_reader.get_resource_value.return_value,
            'SERVERTIMESTAMP_LABEL': 'ServerTimeStamp',
            'RETRIES': 3,  # Possibly part of the protocol
        })
        self.mock_scanline.utilities.config_reader.get_resource_value.assert_called_once_with(
            "$USER113$")

    def test_get_hashkey(self):
        self.assertEqual(
            self.module.get_hashkey(
                'some_Secret', '2016-09-15T02:45:58.317491'),
            self.module.base64.b64encode.return_value.decode.return_value
        )
        self.module.hmac.new.assert_called_once_with(
            'some_Secret',
            digestmod=self.module.hashlib.sha256,
            msg='2016-09-15T02:45:58.317491'
        )
        self.module.base64.b64encode.assert_called_once_with(
            self.module.hmac.new.return_value.digest.return_value
        )

    def test_get_auth_header(self):
        self.assertEqual(
            self.module.get_auth_header(
                'token1'), {'Authorization': 'token1'}
        )

    def test_get_iso_timestamp(self):
        self.assertEqual(
            self.module.get_iso_timestamp(), self.mock_datetime.datetime.utcnow().isoformat()
        )

    def test_generate_token(self):
        self.module.get_hashkey = MagicMock(
            name='get_hashkey', return_value='hash')
        self.module.HMAC_IDENTIFIERS = {'SECRET_KEY': 'sk', 'TIMESTAMP_LABEL': 'TIMESTAMP_LABEL',
                                        'HASH_LABEL': 'HASH_LABEL', 'APPID_LABEL': 'APPID_LABEL',
                                        'APPID': 'APPID'}
        token = self.module.generate_token('isodate')
        self.assertEqual(
            token, 'HMAC TIMESTAMP_LABEL=isodate;HASH_LABEL=hash;APPID_LABEL=APPID')

    def test_get_formatted_token_with_timestamp(self):
        self.module.generate_token = MagicMock(name='generate_token')
        self.assertEqual(
            self.module.get_formatted_token(
                'time_stamp'), self.module.generate_token.return_value
        )
        self.module.generate_token.assert_called_once_with('time_stamp')

    def test_get_formatted_token_without_timestamp(self):
        self.module.generate_token = MagicMock(name='generate_token')
        self.module.get_iso_timestamp = MagicMock(name='get_iso_timestamp')
        self.assertEqual(
            self.module.get_formatted_token(), self.module.generate_token.return_value
        )
        self.module.generate_token.assert_called_once_with(
            self.module.get_iso_timestamp.return_value)

    def test_do_get_request(self):
        response = self.module.do_get_request('url', 'headers', 'timeout', True)
        self.assertEqual(response, self.mock_requests.get.return_value)
        self.mock_requests.get.assert_called_once_with(
            url='url',
            headers='headers',
            timeout='timeout',
            verify=True)
        self.mock_requests.get.return_value.raise_for_status.assert_called_once_with()

    def test_do_get_request_without_timestamp(self):
        response = self.module.do_get_request('url', 'headers')
        self.assertEqual(response, self.mock_requests.get.return_value)
        self.mock_requests.get.assert_called_once_with(
            url='url',
            headers='headers',
            timeout=20,
            verify=False)
        self.mock_requests.get.return_value.raise_for_status.assert_called_once_with()

    def test_do_get_hmac_request(self):
        self.module.get_formatted_token = MagicMock(name='get_formatted_token')
        self.module.get_auth_header = MagicMock(name='get_auth_header')
        self.module.do_get_request = MagicMock(name='do_get_request')
        response = self.module.do_hmac_request('url', 'timeout')
        self.assertEqual(response, self.module.do_get_request.return_value)
        self.module.get_formatted_token.assert_called_once_with()
        self.module.get_auth_header.assert_called_once_with(
            self.module.get_formatted_token.return_value)
        self.module.do_get_request.assert_called_once_with(
            'url', self.module.get_auth_header.return_value, 'timeout', verify=False)

    def test_do_get_hmac_request_verify_True(self):
        self.module.get_formatted_token = MagicMock(name='get_formatted_token')
        self.module.get_auth_header = MagicMock(name='get_auth_header')
        self.module.do_get_request = MagicMock(name='do_get_request')
        response = self.module.do_hmac_request('url', 'timeout', True)
        self.assertEqual(response, self.module.do_get_request.return_value)
        self.module.get_formatted_token.assert_called_once_with()
        self.module.get_auth_header.assert_called_once_with(
            self.module.get_formatted_token.return_value)
        self.module.do_get_request.assert_called_once_with(
            'url', self.module.get_auth_header.return_value, 'timeout', verify=True)

    def test_do_get_hmac_request_no_timeout(self):
        self.module.get_formatted_token = MagicMock(name='get_formatted_token')
        self.module.get_auth_header = MagicMock(name='get_auth_header')
        self.module.do_get_request = MagicMock(name='do_get_request')
        response = self.module.do_hmac_request('url')
        self.assertEqual(response, self.module.do_get_request.return_value)
        self.module.get_formatted_token.assert_called_once_with()
        self.module.get_auth_header.assert_called_once_with(
            self.module.get_formatted_token.return_value)
        self.module.do_get_request.assert_called_once_with(
            'url', self.module.get_auth_header.return_value, 5, verify=False)


if __name__ == '__main__':
    unittest.main()
