import unittest
from mock import MagicMock, patch, call


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class ScanlineHostMgNodeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {
            'logging': MagicMock(name='logging')
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import HostScanner
        from scanline.host.mgnode import MGNodeHostScanner
        MGNodeHostScanner.__bases__ = (Fake.imitate(HostScanner),)
        self.MGNodeHostScanner = MGNodeHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        self.assertEqual(self.MGNodeHostScanner.module_name, 'MGNode')


if __name__ == '__main__':
    unittest.main()
