import unittest
from mock import MagicMock, patch


class ScanlineHostPBHostsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.pbhosts import PBWindowsHostScanner
        self.PBWindowsHostScanner = PBWindowsHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        self.assertEqual(self.PBWindowsHostScanner.module_name, 'PBWindows')


if __name__ == '__main__':
    unittest.main()
