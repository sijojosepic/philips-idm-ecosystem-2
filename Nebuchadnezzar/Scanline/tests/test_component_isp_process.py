import unittest
from mock import MagicMock, patch, PropertyMock, DEFAULT, create_autospec
from lxml import objectify


class ScanlineISPProcessTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_lxml = MagicMock(name='lxml')
            modules = {
                'lxml': self.mock_lxml,
                'lxml.objectify': self.mock_lxml.objectify
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class ScanlineProcessInterface(ScanlineISPProcessTest.TestCase):
    def setUp(self):
        ScanlineISPProcessTest.TestCase.setUp(self)
        import scanline.component.isp_process
        self.module = scanline.component.isp_process
        self.ProcessInterface = self.module.ProcessInterface
        self.process_interface = self.ProcessInterface()

    def tearDown(self):
        ScanlineISPProcessTest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_process_list(self):
        try:
            self.process_interface.get_process_list()
        except NotImplementedError as e:
            self.assertEqual('', str(e))


class ScanlineISPProcessTestCase(ScanlineISPProcessTest.TestCase):
    def setUp(self):
        xml = """<ProcessesAndServices>
      <ModuleType>8</ModuleType>
      <Process>
        <ProcessName>IExport</ProcessName>
        <State>1</State>
      </Process>
      <Process>
        <ProcessName>Dmwl</ProcessName>
        <State>1</State>
      </Process>
    </ProcessesAndServices>"""
        ScanlineISPProcessTest.TestCase.setUp(self)
        import scanline.component.isp_process
        self.module = scanline.component.isp_process
        self.ISPProcess = self.module.ISPProcess
        service_config = MagicMock(name="service_config")
        root = objectify.fromstring(xml)
        service_config.config_root = root
        self.isp_process = self.ISPProcess(service_config)

    def tearDown(self):
        ScanlineISPProcessTest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_process_list(self):
        result = self.isp_process.get_process_list()
        self.assertEqual(result, ['iexport', 'dmwl'])


class ScanlineUDMProcessTestCase(ScanlineISPProcessTest.TestCase):
    def setUp(self):
        self.xml = """<?xml version="1.0" encoding="UTF-8"?> 
        <udmmonitorconfiguration>  
          <retrycount>10</retrycount>  
          <feature>  
            <name>Prefetch</name>  
            <state>1</state>  
            <processes>  
              <process>  
                <name>CILMDataService</name>  
                <state>1</state>  
              </process>  
            </processes>  
          </feature>  
          <feature>  
            <name>ImagingServices</name>  
            <state>1</state>  
            <processes>  
              <process>  
                <name>DicomWebServices</name>  
                <state>1</state>  
              </process>  
            </processes>  
          </feature>    
        </udmmonitorconfiguration>"""
        service_config = [{"ConfigValue": self.xml}]
        ScanlineISPProcessTest.TestCase.setUp(self)
        import scanline.component.isp_process
        self.module = scanline.component.isp_process
        self.UDMProcess = self.module.UDMProcess
        self.udm_process = self.UDMProcess(service_config)

    def tearDown(self):
        ScanlineISPProcessTest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_root(self):
        self.mock_lxml.objectify.fromstring.return_value = 'root'
        result = self.udm_process.root
        self.assertEqual(result, 'root')

    def test_root_exception(self):
        self.udm_process_exp = self.UDMProcess([{"test": "data"}])
        result = self.udm_process_exp.root
        self.assertEqual(None, result)

    def test_get_process_list(self):
        root = objectify.fromstring(self.xml)
        self.mock_lxml.objectify.fromstring.return_value = root
        result = self.udm_process.get_process_list()
        self.assertEqual(result, ['cilmdataservice', 'dicomwebservices'])


if __name__ == '__main__':
    unittest.main()
