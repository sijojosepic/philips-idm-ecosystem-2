import unittest
from mock import MagicMock, patch


class ISPGDHandlerTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_resource = MagicMock(name='resource')
        self.mock_cached_property = MagicMock(
                name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dns': self.mock_scanline.utilities.dns,
            'scanline.utilities.config_reader': self.mock_scanline.utilities.config_reader,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.utilities.auth': self.mock_scanline.utilities.auth,
            'scanline.utilities.hsdp_obs': self.mock_scanline.utilities.hsdp_obs,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'phimutils': self.mock_resource,
            'phimutils.resource': self.mock_resource.resource,
            'scanline.host': self.mock_scanline.host,
            'scanline.host.windows': self.mock_scanline.host.windows,
            'scanline.host.isp': self.mock_scanline.host.isp,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product import isp_gd
        self.module = isp_gd
        self.module.HostScanner = self.mock_scanline.HostScanner
        self.module.GenericProductScanner = self.mock_scanline.GenericProductScanner

    def module_obj(self, hosts_n_types=None, scanner='s1',
                   address='1.1.1.1', tags=None, host_scanner=None,
                   **kwargs):
        hosts_n_types = hosts_n_types if hosts_n_types else []
        return self.module.ISPGDHandler(hosts_n_types, scanner,
                                        address, tags, host_scanner,
                                        **kwargs)

    def test_GD_TYPES_and_URLs(self):
        gd_types = {'I4': 'I4GD', 'Distradconnector': 'DRCGD', 'RWS': 'RWSGD', 'Distradprime': 'DRPGD'}
        urls = {'I4': 'https://{0}/i4gd', 'RWS': 'https://{0}/ConfigurationService/api/configuration/discovery',
                'Distradconnector': 'https://{0}/DistRadNodeManager/v1/discover',
                'Distradprime': 'https://{0}/DistRadNodeManager/v1/discover'}
        obj = self.module_obj()
        self.assertEqual(gd_types, obj.GD_TYPES)
        self.assertEqual(urls, obj.URLs)

    def test_get_scanners_without_scanner_func(self):
        gd_types = {'m1': 'abc'}
        hosts_n_types = [('h1', 'm1')]
        obj = self.module_obj(hosts_n_types)
        obj.GD_TYPES = gd_types
        prd_scanner, host_scanner = obj.get_scanners('m1')
        self.assertEqual(prd_scanner, self.mock_scanline.GenericProductScanner)
        self.assertEqual(host_scanner, self.mock_scanline.HostScanner)

    def test_get_scanners_with_scanner_func(self):
        gd_types = {'m1': 'abc'}
        hosts_n_types = [('h1', 'm1')]
        mock_scanner = MagicMock(name='scanner_func')
        mock_scanner.get_scanners.return_value = {'abc': {'product': 'p1', 'host': 'h1'}}
        kwargs = {'scanners': mock_scanner.get_scanners}
        obj = self.module_obj(hosts_n_types, **kwargs)
        obj.GD_TYPES = gd_types
        self.assertEqual(('p1', 'h1'), obj.get_scanners('m1'))

    def test_url_when_present_in_kwargs_upper_case(self):
        hosts_n_types = [('h1', 'm1')]
        kwargs = {'M1_URL': 'URL'}
        obj = self.module_obj(hosts_n_types, **kwargs)
        self.assertEqual('URL', obj._url('m1', 'h1'))

    def test_url_when_present_in_kwargs_lower_case(self):
        hosts_n_types = [('h1', 'm1')]
        kwargs = {'m1_url': 'url'}
        obj = self.module_obj(hosts_n_types, **kwargs)
        self.assertEqual('url', obj._url('m1', 'h1'))

    def test_url_when_not_present_in_kwargs(self):
        urls = {'m1': 'url/{0}'}
        hosts_n_types = [('h1', 'm1')]
        obj = self.module_obj(hosts_n_types)
        obj.URLs = urls
        self.assertEqual('url/h1', obj._url('m1', 'h1'))

    def test_hmac_enabled_when_present_in_kwargs_upper_case(self):
        hosts_n_types = [('h1', 'm1')]
        kwargs = {'M1_HMAC_ENABLED': 'yes'}
        obj = self.module_obj(hosts_n_types, **kwargs)
        self.assertEqual('yes', obj._hmac_enabled('m1'))

    def test_hmac_enabled_when_present_in_kwargs_lower_case(self):
        hosts_n_types = [('h1', 'm1')]
        kwargs = {'m1_hmac_enabled': 'no'}
        obj = self.module_obj(hosts_n_types, **kwargs)
        self.assertEqual('no', obj._hmac_enabled('m1'))

    def test_hmac_enabled_when_not_present_in_kwargs(self):
        hosts_n_types = [('h1', 'm1')]
        obj = self.module_obj(hosts_n_types)
        self.assertEqual(True, obj._hmac_enabled('m1'))

    def test_discover_all_when_module_type_absent(self):
        gd_types = {}
        hosts_n_types = [('h1', 'm1')]
        obj = self.module_obj(hosts_n_types)
        obj.GD_TYPES = gd_types
        self.assertEqual(({}, []), obj.discover_all())

    def test_discover_all(self):
        gd_types = {'m1': 'abc'}
        hosts_n_types = [('h1', 'm1')]
        obj = self.module_obj(hosts_n_types)
        obj.GD_TYPES = gd_types
        obj.get_scanners = MagicMock(name='get_scanners')
        prd_mock, host_mock = MagicMock(), MagicMock()
        prd_mock.return_value.site_facts.return_value = {'a': 'b'}
        obj.get_scanners.return_value = prd_mock, host_mock
        obj._url = MagicMock(name='_url', return_value='url')
        self.assertEqual(({'a': 'b'}, []), obj.discover_all())
        obj.get_scanners.assert_called_once_with('m1')
        obj._url.assert_called_once_with('m1', 'h1')
        prd_mock.assert_called_once_with(address='1.1.1.1',
                                         discovery_url='url',
                                         hmac_enabled=True,
                                         host_scanner=host_mock,
                                         scanner='s1', tags=None)

    def test_discover_all_no_facts(self):
        hosts_n_types = [('h1', 'Distradprime')]
        obj = self.module_obj(hosts_n_types)
        obj.get_scanners = MagicMock(name='get_scanners')
        prd_mock, host_mock = MagicMock(), MagicMock()
        prd_mock.return_value.site_facts.return_value = {}
        obj.get_scanners.return_value = prd_mock, host_mock
        obj._url = MagicMock(name='_url', return_value='url')
        self.assertEqual(({}, ['DRPGD']), obj.discover_all())
        obj.get_scanners.assert_called_once_with('Distradprime')
        obj._url.assert_called_once_with('Distradprime', 'h1')
        prd_mock.assert_called_once_with(address='1.1.1.1',
                                         discovery_url='url',
                                         hmac_enabled=True,
                                         host_scanner=host_mock,
                                         scanner='s1', tags=None)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
