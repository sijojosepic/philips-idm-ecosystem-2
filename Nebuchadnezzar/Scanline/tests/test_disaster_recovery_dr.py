import unittest
from mock import MagicMock, patch, PropertyMock, call


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""
    extra_params = {'address': '10.1.0.1', 'endpoint': {'enp': 'enp'}, 'tags': 't1'}

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    if name == '__init__':
                        setattr(cls, name, lambda *args, **kwargs: None)
                    else:
                        setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
            for k, v in Fake.extra_params.iteritems():
                setattr(cls, k, v)
        return cls


class DRScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_pacs_config = MagicMock(name='pacs_config')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_logging = MagicMock(name='logging')
        self.mock_zerto_wrapper = MagicMock(name='zerto_wrapper')
        self.mock_zvm = MagicMock(name='zvm')
        modules = {
            'cached_property': self.mock_cached_property,
            'scanline.host': self.mock_scanline.host,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.product.disaster_recovery.zerto_wrapper': self.mock_zerto_wrapper,
            'logging': self.mock_logging,
            'scanline.product.disaster_recovery.pacs_config': self.mock_pacs_config,
            'scanline.product.disaster_recovery.zvm': self.mock_zvm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.disaster_recovery.dr import DRScanner
        from scanline.product import ProductScanner
        DRScanner.__bases__ = (Fake.imitate(ProductScanner),)
        kwargs = dict(infra_address='infra_address', primary_vcenter='primary_vcenter',
                      secondary_vcenter='secondary_vcenter', vcenter_user='vcenter_user',
                      vcenter_password='vcenter_password')
        self.module_obj = DRScanner(
            'scanner', '10.1.0.1', 'u1', 'p1', **kwargs)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_is_enabled(self):
        self.mock_pacs_config.PacsConfiguration.return_value.dr_config = 1
        self.assertEqual(self.module_obj.is_dr_enabled, True)

    def test_get_hosts(self):
        self.module_obj.zobj = MagicMock()
        self.module_obj.kwargs = {}
        mock1 = MagicMock(name='get_peersite')
        self.module_obj.zobj.get_peersite.return_value = [mock1]
        self.mock_zvm.ZVMHost.side_effect = ['h1', 'h2']
        result = [item for item in self.module_obj.get_hosts()]
        self.assertEqual(result, ['h1', 'h2'])
        localsite = self.module_obj.zobj.get_localsite.return_value
        ipaddr = localsite.values.get.return_value
        exp_resp = [call(ipaddr, {'enp': 'enp'}, localsite,
                         password='p1', username='u1', tags='t1'),
                    call(mock1.hostname, {'enp': 'enp'}, mock1,
                         password='p1', username='u1', tags='t1')
                    ]
        self.assertEqual(exp_resp, self.mock_zvm.ZVMHost.call_args_list)
        self.module_obj.zobj.get_peersite.assert_called_once_with()
        self.module_obj.zobj.get_localsite.assert_called_once_with()

    def test_site_facts(self):
        self.module_obj.segregate_credentials = MagicMock()
        self.module_obj.get_hosts = MagicMock(return_value=[MagicMock()])
        with patch('scanline.product.disaster_recovery.dr.DRScanner.is_dr_enabled',
                   new_callable=PropertyMock) as is_dr_enabled:
            is_dr_enabled.return_value = True
            self.assertEqual(self.module_obj.site_facts(),
                             self.module_obj.segregate_credentials.return_value)
            self.assertEqual(self.module_obj.get_hosts.call_count, 1)
            self.assertEqual(
                self.module_obj.segregate_credentials.call_count, 1)

    def test_site_facts_exception(self):
        self.module_obj.zerto_facts = MagicMock()
        with patch('scanline.product.disaster_recovery.dr.DRScanner.is_dr_enabled',
                   new_callable=PropertyMock) as is_dr_enabled:
            is_dr_enabled.return_value = False
            self.assertRaises(RuntimeError, self.module_obj.site_facts)
