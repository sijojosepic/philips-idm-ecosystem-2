import unittest
from mock import MagicMock, patch


class ScanlineHostI4PrepTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.i4prep import I4PrepHostScanner
        self.I4PrepHostScanner = I4PrepHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        self.assertEqual(self.I4PrepHostScanner.module_name, 'I4Prep')


if __name__ == '__main__':
    unittest.main()
