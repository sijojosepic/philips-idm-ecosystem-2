import unittest
from requests.exceptions import RequestException
from mock import MagicMock, patch, call
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)


class ScanlineWinRMTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_b64encode = MagicMock(name='b64encode')
        modules = {
            'winrm': self.mock_winrm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import win_rm
        self.module = win_rm

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_WinRM_initialization(self):
        self.module.WinRM('host', 'user', 'passwd')

    def test_session(self):
        win_rm_obj = self.module.WinRM('host', 'user', 'passwd')
        win_rm_obj._session = None
        win_rm_obj.set_session = MagicMock(name="set_session")
        self.assertEqual(None, win_rm_obj.session)

    def test_execute_ps_script(self):
        win_rm_obj = self.module.WinRM('host', 'user', 'passwd')
        session = MagicMock(name="session")
        session.run_ps.return_value = 'test'
        win_rm_obj._session = session
        output = win_rm_obj.execute_ps_script('ps')
        win_rm_obj.session.run_ps.assert_called_once_with('ps')
        self.assertEqual('test', output)

    def test_execute_cmd(self):
        win_rm_obj = self.module.WinRM('host', 'user', 'passwd')
        session = MagicMock(name="session")
        session.run_cmd.return_value = 'test'
        win_rm_obj._session = session
        output = win_rm_obj.execute_cmd('ps')
        win_rm_obj.session.run_cmd.assert_called_once_with('ps', ())
        self.assertEqual('test', output)

    def test_set_session(self):
        win_rm_obj = self.module.WinRM('host', 'user', 'passwd')
        self.mock_winrm.Session.return_value = 'test'
        output = win_rm_obj.set_session()
        self.mock_winrm.Session.assert_called_once_with('host', auth=(
            'user', 'passwd'), transport='ntlm', server_cert_validation='ignore',
                                                        operation_timeout_sec=45, read_timeout_sec=50)
        self.assertEqual(output, 'test')

    def test_execute_long_script(self):
        win_rm_obj = self.module.WinRM('host', 'user', 'passwd')
        self.mock_b64encode.decode.return_value = 'encode'
        self.mock_p = MagicMock(name="p")
        self.mock_winrm.protocol.Protocol.return_value = self.mock_p
        self.mock_p.open_shell.return_value = "shell"
        self.mock_p.run_command.return_value = "command"
        self.mock_winrm.Response.return_value = "response"
        self.mock_p.cleanup_command.return_value = None
        self.mock_p.close_shell.return_value = None
        self.assertEqual("response", win_rm_obj.execute_long_script('s'))


class WinRMTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'logging': MagicMock(name='logging')
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import win_rm
        self.module = win_rm
        self.mock_winRM = MagicMock(name='WinRM')
        self.module.WinRM = self.mock_winRM
        self.host = '1.2.3.4'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_extract_credentials_in_first_attempt(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        mock_execute_mock.std_out = 'test'
        self.mock_winRM.return_value.execute_cmd.return_value = mock_execute_mock
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        exp_resp = {'username': 'u1', 'password': 'p1'}
        self.assertEqual(response, exp_resp)
        exp_mock_call = [call('1.2.3.4', 'u1', 'p1')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_mock_call)

    def test_extract_credentials_in_second_attempt(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        mock_execute_mock.std_out = 'test'
        self.mock_winRM.return_value.execute_cmd.side_effect = [MagicMock(), mock_execute_mock]
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        exp_resp = {'username': 'u2', 'password': 'p2'}
        self.assertEqual(response, exp_resp)
        exp_mock_call = [call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_mock_call)

    def test_extract_credentials_failed(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        self.mock_winRM.return_value.execute_cmd.return_value = mock_execute_mock
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        self.assertEqual(response, {})
        self.assertEqual(mock_execute_mock.std_out.__contains__.call_count, 4)
        exp_call = [call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2'), call('1.2.3.4', 'u1', 'p1'),
                    call('1.2.3.4', 'u2', 'p2')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_call)

    def test_extract_credentials_exception(self):
        mock_execute_mock = MagicMock(name='execute_cmd')
        mock_execute_mock.std_out = 'test'
        self.mock_winRM.return_value.execute_cmd.side_effect = [Exception(), mock_execute_mock]
        user_list = ('u1', 'u2')
        pwd_list = ('p1', 'p2')
        response = self.module.extract_credentials(self.host, user_list, pwd_list)
        exp_resp = {'username': 'u2', 'password': 'p2'}
        self.assertEqual(response, exp_resp)
        exp_mock_call = [call('1.2.3.4', 'u1', 'p1'), call('1.2.3.4', 'u2', 'p2')]
        self.assertEqual(self.mock_winRM.call_args_list, exp_mock_call)

    def test_execute_powershell_script_case_1(self):
        drive = 'C'
        mock_response = MagicMock(name="response")
        mock_response.status_code = 0
        mock_response.std_out = drive
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_response
        self.module.WinRM = MagicMock(name='WinRM', return_value=mock_WINRM)
        self.module.WinRM.return_value = mock_WINRM
        data = self.module.execute_powershell_script(self.host, 'admin', 'pass',
                                                     'get-content C:\Temp\PBDDoscovery.json')
        self.assertEqual(data, drive)
        mock_WINRM.execute_ps_script.assert_called_once_with('get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_case_2(self):
        mock_response = MagicMock(name="response")
        mock_response.status_code = None
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_response
        self.module.WinRM = MagicMock(name='WinRM', return_value=mock_WINRM)
        data = self.module.execute_powershell_script(self.host, 'admin', 'pass',
                                                     'get-content C:\Temp\PBDDoscovery.json')
        self.assertEqual(data, None)
        mock_WINRM.execute_ps_script.assert_called_once_with('get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_AuthenticationError(self):
        self.module.WinRM.side_effect = AuthenticationError()
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_RequestException(self):
        self.module.WinRM.side_effect = RequestException()
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_WinRMOperationTimeoutError(self):
        self.module.WinRM.side_effect = WinRMOperationTimeoutError()
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_TypeError_case_1(self):
        self.module.WinRM.side_effect = TypeError()
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_TypeError_case_2(self):
        self.module.WinRM.side_effect = TypeError("takes exactly 2")
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_ValueError(self):
        self.module.WinRM.side_effect = ValueError()
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_execute_powershell_script_Exception(self):
        self.module.WinRM.side_effect = Exception()
        self.module.execute_powershell_script(self.host, 'admin', 'pass', 'get-content C:\Temp\PBDDoscovery.json')

    def test_fetch_hostname(self):
        self.module.execute_powershell_script = MagicMock(name='execute_powershell_script', return_value='abc')
        self.assertEqual('abc', self.module.fetch_hostname('address', 'username', 'password'))

    def test_exec_ps_script_AuthenticationError(self):
        self.module.WinRM.side_effect = AuthenticationError()
        data = self.module.exec_ps_script(self.host, 'admin', 'pass', 'Ps_Query')
        self.assertEqual(data, (3, 'UNKNOWN : WinRM Error '))

    def test_exec_ps_script_RequestException(self):
        self.module.WinRM.side_effect = RequestException()
        data = self.module.exec_ps_script(self.host, 'admin', 'pass', 'Ps_Query')
        self.assertEqual(data, (2, 'CRITICAL : Request Error '))

    def test_exec_ps_script_WinRMOperationTimeoutError(self):
        self.module.WinRM.side_effect = WinRMOperationTimeoutError()
        data = self.module.exec_ps_script(self.host, 'admin', 'pass', 'Ps_Query')
        self.assertEqual(data, (2, 'CRITICAL : WinRM Error '))

    def test_exec_ps_script_TypeError(self):
        self.module.WinRM.side_effect = TypeError()
        data = self.module.exec_ps_script(self.host, 'admin', 'pass', 'Ps_Query')
        self.assertEqual(data, (2, 'CRITICAL : Typeerror(May be Issue in connecting to node - 1.2.3.4)'))

    def test_exec_ps_script_TypeError_with_value(self):
        self.module.WinRM.side_effect = TypeError('"takes exactly 2"')
        data = self.module.exec_ps_script(self.host, 'admin', 'pass', 'Ps_Query')
        self.assertEqual(data, (2, 'CRITICAL : Issue in connecting to node - 1.2.3.4'))

    def test_test_exec_ps_script_case_1(self):
        mock_response = "Ps_result"
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_response
        self.module.WinRM = MagicMock(name='WinRM', return_value=mock_WINRM)
        self.module.WinRM.return_value = mock_WINRM
        data = self.module.exec_ps_script(self.host, 'admin', 'pass', 'Ps_Query')
        self.assertEqual(data, (0, 'Ps_result'))
        mock_WINRM.execute_ps_script.assert_called_once_with('Ps_Query')


if __name__ == '__main__':
    unittest.main()
