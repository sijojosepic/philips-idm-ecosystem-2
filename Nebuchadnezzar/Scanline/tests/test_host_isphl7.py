import unittest
from mock import MagicMock, patch


class ScanlineHostISPHL7TestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.isphl7 import ISPHL7HostScanner
        self.ISPHL7HostScanner = ISPHL7HostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        scanner = self.ISPHL7HostScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.assertEqual(self.ISPHL7HostScanner.module_name, 'HL7')


if __name__ == '__main__':
    unittest.main()
 