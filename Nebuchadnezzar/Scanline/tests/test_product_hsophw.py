import unittest
import json
from mock import MagicMock, patch, Mock


class ScanlineHSOPHWScanner(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        modules = {
            'requests': self.mock_request
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.hsophw import HSOPHWScanner
        self.DOMAINNAME = 'cloud_admin'
        self.USERNAME = 'admin'
        self.PASSWORD = 'admin'
        self.HOSTNAME = '130.147.86.205'
        self.SCANNER_NAME = 'HSOPHW'
        self.hsop_hw_product_scanner =HSOPHWScanner(self.SCANNER_NAME, self.HOSTNAME, self.USERNAME,
                                                            self.PASSWORD, self.DOMAINNAME)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_facts(self):
        stubbed_facts = {
            self.HOSTNAME: {self.SCANNER_NAME: {"endpoint": {"scanner": self.SCANNER_NAME, "address": self.HOSTNAME}},
                            "address": self.HOSTNAME,"modules": ["hardware", self.SCANNER_NAME], "product_id": None, 
                            "product_name": None, "product_version": None, "role": "NA"},
        }

        stubbed_floating_api_response = {'floatingips': [{'floating_ip_address': u'130.147.86.170',
                                             'port_id': u'a0867266-ceee-4bf4-a2de-1698e594ec16'}]}
        self.mock_request.get.return_value = Mock(ok=True, status_code=200)
        self.mock_request.get.return_value.json.return_value = stubbed_floating_api_response
        facts = self.hsop_hw_product_scanner.get_facts(self.HOSTNAME)
        assert json.dumps(facts) == json.dumps(stubbed_facts)


if __name__ == '__main__':
    unittest.main()
