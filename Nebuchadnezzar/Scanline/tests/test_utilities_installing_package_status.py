import unittest
from mock import MagicMock, patch

from winrm.exceptions import InvalidCredentialsError
from requests.exceptions import ConnectionError

class ScanlineUtilitiesInstallingPackageStatusTestCase(unittest.TestCase):
    def setUp(self):
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_requests = MagicMock(name='requests')
        self.mock_time = MagicMock(name='time')
        modules = {
            'winrm': self.mock_winrm,
            'requests': self.mock_requests,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import installing_package_status
        self.installing_package_status = installing_package_status


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        
    def test_connect(self):
        self.mock_winrm.Session = MagicMock(return_value = 'value')
        self.assertEqual(
           self.installing_package_status.connect('hostname1', 'username1', 'password1'), 'value'
           )

    def test_check_package_no_exception_with_status_code_zero(self):
        obj = MagicMock()
        obj.status_code = 0
        obj.run_ps = MagicMock(return_value=obj)
        status_code = 0
        msg = 'OK: service1 package is installed successfully.'
        self.assertEqual(
            self.installing_package_status.check_package(obj, 'service1', 'hostname1'), (status_code, msg)
            )

    def test_check_package_no_exception_with_status_code_not_zero(self):
        obj = MagicMock()
        obj.status_code = 1
        obj.run_ps = MagicMock(return_value=obj)
        status_code = 2
        msg = 'CRITICAL: service1 service not found.'
        self.assertEqual(
            self.installing_package_status.check_package(obj, 'service1', 'hostname1'), (status_code, msg)
            )

    def test_check_package_with_winrm_exception_invalidcredentialerror(self):
        obj = MagicMock()
        obj.run_ps.side_effect = InvalidCredentialsError
        self.installing_package_status.winrm.exceptions.InvalidCredentialsError = InvalidCredentialsError
        status_code = 2
        msg = 'CRITICAL: Invalid credentials for server: hostname1'
        self.assertEqual(
            self.installing_package_status.check_package(obj, 'service1', 'hostname1'), (status_code, msg)
            )

    def test_check_package_with_requests_exceptions_connectionerror(self):
        obj = MagicMock()
        obj.run_ps.side_effect = ConnectionError
        self.installing_package_status.requests.exceptions.ConnectionError = ConnectionError
        status_code = 2
        msg = 'Failed to establish a new connection.'
        self.assertEqual(
            self.installing_package_status.check_package(obj, 'service1', 'hostname1'), (status_code, msg)
            )

if __name__ == '__main__':
    unittest.main()
