import unittest
from mock import MagicMock, patch, PropertyMock, call


class IDMZertoTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_logging = MagicMock(name='logging')
        self.mock_zerto = MagicMock(name='zerto')
        self.mock_zvm = MagicMock(name='zvm')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': self.mock_logging,
            'zerto': self.mock_zerto,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import scanline.product.disaster_recovery.zerto_wrapper
        self.module = scanline.product.disaster_recovery.zerto_wrapper
        self.module_obj = self.module.IDMZerto('zhost', 'username', 'password')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_timeout(self):
        self.assertEqual(self.module_obj.timeout, 5)
        obj = self.module.IDMZerto('zhost', 'username', 'password', timeout=12)
        self.assertEqual(obj.timeout, 12)

    def test_zhosts(self):
        self.assertEqual(self.module_obj.hosts, ['zhost'])
        obj = self.module.IDMZerto(['h1','h2'], 'username', 'password')
        self.assertEqual(obj.hosts, ['h1', 'h2'])

    def test_port(self):
        self.assertEqual(self.module_obj.port, 9669)
        obj = self.module.IDMZerto(['h1','h2'], 'username', 'password', port=1)
        self.assertEqual(obj.port, 1)

    def test_get_url(self):
        self.assertEqual(self.module_obj.get_url('host'), 'https://host:9669')

    def test_vpg(self):
        self.module_obj.get_vpgs = MagicMock()
        self.assertEqual(self.module_obj.vpgs, self.module_obj.get_vpgs.return_value)
        self.module_obj.get_vpgs.assert_called_once_with()

    def test_is_protected(self):
        self.module_obj.site_is_protected = MagicMock()
        self.assertEqual(self.module_obj.is_protected,
                         self.module_obj.site_is_protected.return_value)
        self.module_obj.site_is_protected.assert_called_once_with()

    def test_site_is_protected(self):
        self.module_obj.get_vpgs = MagicMock(return_value=[{'ProtectedSite': {'type': 'LocalSiteApi'}}])
        self.assertEqual(self.module_obj.site_is_protected(), True)
        self.module_obj.get_vpgs.assert_called_once_with()

    def test_site_is_protected_false(self):
        self.module_obj.get_vpgs = MagicMock(return_value=[{'ProtectedSite': {'type': 'ProtectedSiteApi'}}])
        self.assertEqual(self.module_obj.site_is_protected(), False)
        self.module_obj.get_vpgs.assert_called_once_with()

    def test_site_is_protected_exception(self):
        self.module_obj.get_vpgs = MagicMock(return_value=[])
        self.assertEqual(self.module_obj.site_is_protected(), False)
        self.module_obj.get_vpgs.assert_called_once_with()

    def test_zerto_connection(self):
        self.assertEqual(self.module_obj._zerto_connection('url'),
                         self.mock_zerto.Zerto.return_value)
        get_api = self.mock_zerto.Zerto.return_value.get_apis
        get_api.assert_called_once_with(timeout=5)
        get_session = self.mock_zerto.Zerto.return_value.get_session
        get_session.assert_called_once_with('username', 'password')

    def test_get_connection_url(self):
        self.module_obj.url = 'url'
        self.module_obj._zerto_connection = MagicMock()
        self.assertEqual(self.module_obj.get_connection(),
                         self.module_obj._zerto_connection.return_value)
        self.module_obj._zerto_connection.assert_called_once_with('url')

    def test_get_connection_host(self):
        self.module_obj._zerto_connection = MagicMock()
        self.module_obj.get_url = MagicMock()
        self.assertEqual(self.module_obj.get_connection(),
                         self.module_obj._zerto_connection.return_value)
        url = self.module_obj.get_url.return_value
        self.module_obj._zerto_connection.assert_called_once_with(url)
        self.module_obj.get_url.assert_called_once_with('zhost')

    def test_get_connection_ConnectionError(self):
        from requests.exceptions import ConnectionError
        self.module_obj.hosts = ['h1', 'h2']
        self.module_obj.get_url = MagicMock()
        self.module_obj.get_url.side_effect = ['h11', 'h22']
        self.module_obj._zerto_connection = MagicMock()
        self.module_obj._zerto_connection.side_effect = [ConnectionError, 'abc']
        self.assertEqual(self.module_obj.get_connection(), 'abc')
        exp_call = [call('h11'), call('h22')]
        self.assertEqual(exp_call,
                         self.module_obj._zerto_connection.call_args_list)

    def test_get_connection_RuntimeError(self):
        from requests.exceptions import ConnectionError
        self.module_obj._zerto_connection = MagicMock()
        self.module_obj.get_url = MagicMock(return_value='zhost')
        self.module_obj._zerto_connection.side_effect = ConnectionError
        self.assertRaises(RuntimeError, self.module_obj.get_connection)
        self.module_obj._zerto_connection.assert_called_once_with('zhost')

    def test_zconn(self):
        self.module_obj.get_connection = MagicMock()
        self.assertEqual(self.module_obj.zconn,
                         self.module_obj.get_connection.return_value)

    def test_get_loalsite(self):
        self.module_obj.get_connection = MagicMock()
        local_site = self.module_obj.get_connection.return_value.get_localsite.return_value
        self.assertEqual(self.module_obj.get_localsite(), local_site)

    def test_get_peersite(self):
        self.module_obj.get_connection = MagicMock()
        peer_site = self.module_obj.get_connection.return_value.get_peersites.return_value
        self.assertEqual(self.module_obj.get_peersite(), peer_site)

    def test_get_request(self):
        mock_obj = MagicMock(name='mockobject')
        mock_obj.get_request.return_value = []
        self.module_obj.get_connection = MagicMock(name='get_connection', return_value=mock_obj)
        self.assertEqual(self.module_obj.get_request('/v1'), [])
        self.module_obj.get_connection.assert_called_once_with()

    def test_get_vpgs(self):
        mock_obj = MagicMock(name='mockobject')
        mock_obj.json.return_value = [{}]
        self.module_obj.get_request = MagicMock(name='get_request', return_value=mock_obj)
        self.assertEqual(self.module_obj.get_vpgs(), [{}])
        self.module_obj.get_request.assert_called_once_with('v1/vpgs')

    def test_get_paring_status(self):
        mock_obj = MagicMock(name='mockobject')
        mock_obj.json.return_value = {'PairingStatus': 0}
        self.module_obj.get_request = MagicMock(return_value=mock_obj)
        self.assertEqual(self.module_obj.get_paring_status('abc'), 0)
        self.module_obj.get_request.assert_called_once_with('/v1/peersites/abc')

    def test_protectedsite_pairing_status(self):
        self.module_obj.get_vpgs = MagicMock(return_value=[{'ProtectedSite': {'identifier': 'abc'}}])
        self.module_obj.get_paring_status = MagicMock(return_value=0)
        self.assertEqual(self.module_obj.protectedsite_pairing_status(), 0)
        self.module_obj.get_vpgs.assert_called_once_with()
        self.module_obj.get_paring_status.assert_called_once_with('abc')


if __name__ == '__main__':
    unittest.main()
