import unittest
from mock import MagicMock, patch


class ScanlineHostISCVWebTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.iscvweb import ISCVWebScanner
        self.ISCVWebScanner = ISCVWebScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        self.assertEqual(self.ISCVWebScanner.module_name, 'ISCVWeb')

if __name__ == '__main__':
    unittest.main()