import unittest
from mock import MagicMock, patch


class ScanlineUtilitiesDbTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        modules = {
            'linecache': MagicMock(name='linecache'),
            'pymssql': self.mock_pymssql,
            'phimutils':MagicMock(),
            'phimutils.resource': MagicMock()
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import db
        self.db = db
        self.cm = db._CursorMgr()
        self.qm = db.QueryMgr()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_connect(self):
        self.assertEqual(self.cm.connect(), self.mock_pymssql.connect().cursor())

    def test___del__(self):
    	self.cm.connect()
    	self.cm.__del__()

    def test_CursorMgr(self):
    	self.db.CursorMgr()

    def test_execute(self):
    	self.assertEqual(self.qm.execute(), self.mock_pymssql.connect().cursor().fetchone())
    
    def test___init__(self):
    	self.qm.__init__()

    	
if __name__ == '__main__':
    unittest.main()
