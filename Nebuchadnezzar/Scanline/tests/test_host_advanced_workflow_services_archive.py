import unittest
from mock import MagicMock, patch


class ScanlineHostAdvancedWorkflowArchiveServicesTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self) 
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.advanced_workflow_services_archive import AdvancedWorkflowArchiveServicesHostScanner
        self.AdvancedWorkflowArchiveServicesHostScanner = AdvancedWorkflowArchiveServicesHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_property_data(self):
        self.assertEqual(self.AdvancedWorkflowArchiveServicesHostScanner.module_name, 'AdvancedWorkflowArchiveServices')
        self.assertEqual(self.AdvancedWorkflowArchiveServicesHostScanner.productid, 'AWS')
        self.assertEqual(self.AdvancedWorkflowArchiveServicesHostScanner.productname, 'AdvancedWorkflowServices')
        self.assertEqual(self.AdvancedWorkflowArchiveServicesHostScanner.productversion, '')         
     

if __name__ == '__main__':
    unittest.main()
