import unittest
from mock import MagicMock, patch


class FakeClass(object):
    """The purpose of this class is to Fake the Base classes"""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class XPERIMGDProdTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_resource = MagicMock(name='resource')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_utilities,
            'scanline.host': self.mock_utilities.host,
            'scanline.utilities.http': self.mock_utilities.http,
            'phimutils.resource': self.mock_resource,
            'cached_property': MagicMock(name='cached_property', cached_property=property)}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product import GenericProductScanner
        import scanline.product.xperim
        self.module = scanline.product.xperim
        self.module.XPERIMGDProductScanner.__bases__ = (FakeClass.imitate(GenericProductScanner),)
        self.scanner_obj = self.module.XPERIMGDProductScanner('scanner', '10.55.66.1', 'u1', 'p1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_site_facts(self):
        self.module.super = MagicMock(name='super')
        mock_seg_cred = MagicMock(name='segregate_credentials')
        self.scanner_obj.segregate_credentials = mock_seg_cred
        site_fact = self.scanner_obj.site_facts()
        self.assertEquals(site_fact, mock_seg_cred.return_value)
        self.assertEquals(mock_seg_cred.call_count, 1)
        mock_seg_cred.assert_called_once_with(self.module.super.return_value.site_facts.return_value)


class XPERIMProductTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_resource = MagicMock(name='resource')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_utilities,
            'scanline.host': self.mock_utilities.host,
            'scanline.utilities.http': self.mock_utilities.http,
            'phimutils.resource': self.mock_resource,
            'cached_property': MagicMock(name='cached_property', cached_property=property)}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product import ProductScanner
        from scanline.product.xperim import XPERIMProductScanner
        XPERIMProductScanner.__bases__ = (FakeClass.imitate(ProductScanner),)
        self.scanner_obj = XPERIMProductScanner('scanner', '10.55.66.1', 'u1', 'p1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_site_facts(self):
        mock_seg_cred = MagicMock(name='segregate_credentials')
        self.scanner_obj.segregate_credentials = mock_seg_cred
        site_fact = self.scanner_obj.site_facts()
        self.assertEquals(site_fact, mock_seg_cred.return_value)
        self.assertEquals(mock_seg_cred.call_count, 1)
