import unittest
from mock import MagicMock, patch


class ScanlineHostLinuxTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.linux import LinuxHostScanner
        self.LinuxHostScanner = LinuxHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_properties_present(self):
        scanner = self.LinuxHostScanner('10.1.2.3', '167.81.183.99', tags=['x', 'y'])
        mock_scanner = MagicMock(spec=scanner)
        for prop in self.LinuxHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.LinuxHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))


if __name__ == '__main__':
    unittest.main()
