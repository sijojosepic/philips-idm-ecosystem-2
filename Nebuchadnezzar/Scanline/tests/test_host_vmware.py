import unittest
from mock import MagicMock, patch, PropertyMock, call


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class VMwareBaseTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pysphere = MagicMock(name='pysphere')
        self.mock_logging = MagicMock(name='logging')
        modules = {
            'logging': self.mock_logging,
            'pysphere': self.mock_pysphere,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import vmware
        self.module = vmware

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


class SizeFMTTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)

    def test_size_fmt_bytes(self):
        self.assertEqual(self.module.sizeof_fmt(1000), '1000.0bytes')

    def test_size_fmt_KB(self):
        self.assertEqual(self.module.sizeof_fmt(2000), '2.0KB')

    def test_size_fmt_MB(self):
        self.assertEqual(self.module.sizeof_fmt(1048576), '1.0MB')

    def test_size_fmt_GB(self):
        self.assertEqual(self.module.sizeof_fmt(1073741824), '1.0GB')

    def test_size_fmt_TB(self):
        self.assertEqual(self.module.sizeof_fmt(1024 * 1024 * 1024 * 1024), '1.0TB')


class vCenterHostScannerTestcase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        from scanline.host import HostScanner
        self.module.vCenterHostScanner.__bases__ = (Fake.imitate(HostScanner),)
        self.vcenter_obj = self.module.vCenterHostScanner(
            'hostname', 'endpoint', 'username', 'password', 'tags', **{})

    def tearDown(self):
        VMwareBaseTestCase.tearDown(self)

    def test_vserver(self):
        self.vcenter_obj.vserver
        exp_call = [call(), call().connect('hostname', 'username', 'password')]
        self.assertEqual(self.mock_pysphere.VIServer.mock_calls, exp_call)

    def test_vserver_when_server_set(self):
        self.vcenter_obj = self.module.vCenterHostScanner(
            'hostname', 'endpoint', 'username', 'password', None, **{})
        self.vcenter_obj.server = 'vserver'
        self.assertEqual(self.vcenter_obj.vserver, 'vserver')
        self.assertEqual(self.mock_pysphere.VIServer.mock_calls, [])

    def test_get_facts(self):
        mock_get_cfact = MagicMock(name='get_child_facts')
        mock_get_cfact.return_value = {'d1': 'd2'}
        self.vcenter_obj.get_child_facts = mock_get_cfact
        self.assertEqual(self.vcenter_obj.get_facts(),
                         {'vCenter': {'d1': 'd2'}})
        mock_get_cfact.assert_called_once_with()

    def test_get_facts_child_facts_empty(self):
        mock_get_cfact = MagicMock(name='get_child_facts')
        mock_get_cfact.return_value = {}
        self.vcenter_obj.get_child_facts = mock_get_cfact
        self.assertEqual(self.vcenter_obj.get_facts(), {})
        mock_get_cfact.assert_called_once_with()

    def test_to_dict_empty(self):
        mock_get_fact = MagicMock(name='get_facts')
        mock_get_fact.return_value = {}
        self.vcenter_obj.get_facts = mock_get_fact
        self.vcenter_obj.hostname = 'hostname'
        self.assertEqual(self.vcenter_obj.to_dict(), {})
        mock_get_fact.assert_called_once_with()

    def test_to_dict(self):
        mock_get_fact = MagicMock(name='get_facts')
        mock_get_fact.return_value = {'vCenter': {'v1': 'vCenter_facts'}}
        self.vcenter_obj.get_facts = mock_get_fact
        self.vcenter_obj.hostname = 'hostname'
        self.vcenter_obj.endpoint = 'endpoint'
        self.vcenter_obj.tags = 'tags'
        self.vcenter_obj.address = 'address'
        exp_result = {'vCenter': {'v1': 'vCenter_facts', 'endpoint': 'endpoint', 'tags': 'tags'}, 'tags': 'tags',
                      'address': 'address'}
        self.assertEqual(self.vcenter_obj.to_dict(), exp_result)
        mock_get_fact.assert_called_once_with()

    def test_to_dict_address_None(self):
        mock_get_fact = MagicMock(name='get_facts')
        mock_get_fact.return_value = {'vCenter': {'v1': 'vCenter_facts'}}
        self.vcenter_obj.get_facts = mock_get_fact
        self.vcenter_obj.hostname = 'hostname'
        self.vcenter_obj.endpoint = 'endpoint'
        self.vcenter_obj.tags = 'tags'
        self.vcenter_obj.address = None
        exp_result = {'vCenter': {'v1': 'vCenter_facts', 'endpoint': 'endpoint', 'tags': 'tags'}, 'tags': 'tags'}
        self.assertEqual(self.vcenter_obj.to_dict(), exp_result)
        mock_get_fact.assert_called_once_with()

    def test_to_dict_tags_None(self):
        mock_get_fact = MagicMock(name='get_facts')
        mock_get_fact.return_value = {'vCenter': {'v1': 'vCenter_facts'}}
        self.vcenter_obj.get_facts = mock_get_fact
        self.vcenter_obj.hostname = 'hostname'
        self.vcenter_obj.endpoint = 'endpoint'
        self.vcenter_obj.tags = None
        self.vcenter_obj.address = 'address'
        exp_result = {'vCenter': {'v1': 'vCenter_facts',
                                  'endpoint': 'endpoint'}, 'address': 'address'}
        self.assertEqual(self.vcenter_obj.to_dict(), exp_result)
        mock_get_fact.assert_called_once_with()


class vCenterHostScannerChildFactTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        from scanline.host import HostScanner
        self.module.vCenterHostScanner.__bases__ = (Fake.imitate(HostScanner),)
        self.vserver_patch = patch(
            'scanline.host.vmware.vCenterHostScanner.vserver', new_callable=PropertyMock)
        self.mock_vserver = self.vserver_patch.start()
        self.vcenter_obj = self.module.vCenterHostScanner(
            'hostname', 'endpoint', 'username', 'password', None, **{})
        self.mock_data_center = MagicMock(name='datacenter')
        self.module.DataCenter = self.mock_data_center

    def tearDown(self):
        VMwareBaseTestCase.tearDown(self)
        self.vserver_patch.stop()

    def test_get_child_facts(self):
        self.mock_data_center.return_value.get_facts.return_value = {
            'cluster': {'c1': 'c2'}}
        self.mock_vserver.return_value.get_datacenters.return_value = {
            'dc_mor1': 'dc_name1'}
        expected_result = {'datacenter': {'cluster': {'c1': 'c2'}}}
        self.assertEqual(self.vcenter_obj.get_child_facts(), expected_result)
        self.mock_data_center.assert_called_once_with(
            self.mock_vserver(), 'dc_mor1', 'dc_name1')
        self.mock_vserver().get_datacenters.assert_called_once_with()
        self.mock_data_center().get_facts.assert_called_once_with()

    def test_get_child_facts_when_empty(self):
        self.mock_vserver.return_value.get_datacenters.return_value = {}
        expected_result = {'datacenter': {}}
        self.assertEqual(self.vcenter_obj.get_child_facts(), expected_result)
        self.assertEqual(self.mock_data_center.called, False)
        self.mock_vserver().get_datacenters.assert_called_once_with()

    def test_get_child_facts_exception(self):
        self.mock_vserver.return_value.get_datacenters.side_effect = Exception()
        expected_result = {}
        self.assertEqual(self.vcenter_obj.get_child_facts(), expected_result)
        self.assertEqual(self.mock_data_center.called, False)
        self.mock_vserver().get_datacenters.assert_called_once_with()


class DataCenterTestcase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_vserver = MagicMock(name='vserver')
        self.dc_obj = self.module.DataCenter(
            self.mock_vserver, 'dc_mor', 'dc_name')
        self.mock_cluster = MagicMock(name='cluster')
        self.module.Cluster = self.mock_cluster
        self.mock_esxihost = MagicMock(name='esxihost')
        self.module.EXSiHost = self.mock_esxihost

    def test_dc_get_facts_child_facts(self):
        self.mock_cluster.return_value.get_facts.return_value = {
            'host': {'h1': 'h2'}}
        self.mock_vserver.get_clusters.return_value = {
            'c_mor1': 'c_name1'}
        expected_result = {'cluster': {'host': {'h1': 'h2'}}}
        self.assertEqual(self.dc_obj.get_child_facts(), expected_result)
        self.mock_cluster.assert_called_once_with(self.mock_vserver, 'c_mor1', 'c_name1')
        self.mock_vserver.get_clusters.assert_called_once_with(from_mor='dc_mor')
        self.mock_cluster().get_facts.assert_called_once_with()

    def test_dc_get_facts_child_facts_empty(self):
        self.mock_vserver.return_value.get_facts.return_value = {}
        expected_result = {'cluster': {}}
        self.assertEqual(self.dc_obj.get_child_facts(), expected_result)
        self.assertEqual(self.mock_cluster.called, False)
        self.mock_vserver.get_clusters.assert_called_once_with(
            from_mor='dc_mor')

    def test_dc_get_facts_child_facts_host(self):
        self.mock_esxihost.return_value.get_facts.return_value = {'name': {'h1': 'h2'}}
        self.mock_vserver.get_hosts.return_value = {'h_mor1': 'h_name1'}
        expected_result = {'host': {'name': {'h1': 'h2'}}}
        self.assertEqual(self.dc_obj.get_child_facts_host(), expected_result)
        self.mock_esxihost.assert_called_once_with(self.mock_vserver, 'h_mor1', 'h_name1')
        self.mock_vserver.get_hosts.assert_called_once_with(from_mor='dc_mor')
        self.mock_esxihost().get_facts.assert_called_once_with()

    def test_dc_get_facts(self):
        mock_get_cfact = MagicMock(name='get_child_facts')
        mock_get_cfact.return_value = {'cluster': {'c1': 'c2'}}
        self.dc_obj.get_child_facts = mock_get_cfact
        exp_result = {'dc_mor': {'cluster': {'c1': 'c2'}, 'name': 'dc_name'}}
        self.assertEqual(self.dc_obj.get_facts(), exp_result)
        mock_get_cfact.assert_called_once_with()


class ClusterTestcase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_vserver = MagicMock(name='vserver')
        self.Cluster = self.module.Cluster(
            self.mock_vserver, 'c_mor', 'c_name')
        self.mock_host = MagicMock(name='EXSiHost')
        self.module.EXSiHost = self.mock_host

    def test_cluster_get_facts_child_facts(self):
        self.mock_host.return_value.get_facts.return_value = {
            'VM': ['VM1', 'VM2']}
        self.mock_vserver.get_hosts.return_value = {
            'h_mor1': 'h_name1'}
        expected_result = {'host': {'VM': ['VM1', 'VM2']}}
        self.assertEqual(self.Cluster.get_child_facts(), expected_result)
        self.mock_host.assert_called_once_with(
            self.mock_vserver, 'h_mor1', 'h_name1')
        self.mock_vserver.get_hosts.assert_called_once_with(from_mor='c_mor')
        self.mock_host().get_facts.assert_called_once_with()

    def test_cluster_get_facts_child_facts_empty(self):
        self.mock_vserver.return_value.get_facts.return_value = {}
        expected_result = {'host': {}}
        self.assertEqual(self.Cluster.get_child_facts(), expected_result)
        self.assertEqual(self.mock_host.called, False)
        self.mock_vserver.get_hosts.assert_called_once_with(from_mor='c_mor')

    def test_cluster_get_facts(self):
        mock_get_cfact = MagicMock(name='get_child_facts')
        mock_get_cfact.return_value = {'h1': 'h2'}
        self.Cluster.get_child_facts = mock_get_cfact
        exp_result = {'c_mor': {'h1': 'h2', 'name': 'c_name'}}
        self.assertEqual(self.Cluster.get_facts(), exp_result)
        mock_get_cfact.assert_called_once_with()


class ESXiDataStore(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_dtstore = MagicMock(name='datastore')
        self.datastore_obj = self.module.EXSiDataStore(self.mock_dtstore)
        self.mock_size_fmt = MagicMock(name='sizeof_fmt')
        self.module.sizeof_fmt = self.mock_size_fmt

    def test_esxidatastore_info(self):
        size_fmt_resp = ['c1', 'f1']

        def _side_effect(*args, **kwars):
            return size_fmt_resp.pop()

        self.mock_size_fmt.side_effect = _side_effect
        self.mock_dtstore.info.name = 'name'
        self.mock_dtstore.info.freeSpace = 'freeSpace'
        self.mock_dtstore.summary.capacity = 'capacity'
        self.mock_dtstore.summary.type = 'type'
        expected_result = {'name': {'freeSpace': 'f1', 'capacity': 'c1', 'type': 'type'}}
        self.assertEqual(self.datastore_obj.info(), expected_result)
        size_fmt_exp_call = [call('freeSpace'), call('capacity')]
        self.assertEqual(self.mock_size_fmt.mock_calls, size_fmt_exp_call)


class EXSiProductTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_product = MagicMock(name='product')
        self.product_obj = self.module.EXSiProduct(self.mock_product)

    def test_esxiproduct_info(self):
        self.mock_product.fullName = 'fullName'
        self.mock_product.osType = 'osType'
        self.mock_product.vendor = 'vendor'
        self.mock_product.version = 'version'
        expected_result = {'fullName': 'fullName', 'osType': 'osType',
                           'vendor': 'vendor', 'version': 'version'}
        self.assertEqual(self.product_obj.info(), expected_result)


class EXSiHardwareTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_hardware = MagicMock(name='product')
        self.hardware_obj = self.module.EXSiHardware(self.mock_hardware)
        self.mock_size_fmt = MagicMock(name='sizeof_fmt')
        self.module.sizeof_fmt = self.mock_size_fmt

    def test_esxihardware_info(self):
        self.mock_hardware.cpuMhz = 'cpuMhz'
        self.mock_hardware.cpuModel = 'cpuModel'
        self.mock_hardware.model = 'model'
        self.mock_hardware.numCpuCores = 'numCpuCores'
        self.mock_hardware.numCpuPkgs = 'numCpuPkgs'
        self.mock_hardware.numCpuThreads = 'numCpuThreads'
        self.mock_hardware.numHBAs = 'numHBAs'
        self.mock_hardware.numNics = 'numNics'
        self.mock_hardware.uuid = 'uuid'
        self.mock_hardware.vendor = 'vendor'
        self.mock_hardware.memorySize = 'memorySize'
        expected_result = {'cpuMhz': 'cpuMhz', 'cpuModel': 'cpuModel',
                           'model': 'model', 'numCpuCores': 'numCpuCores',
                           'numCpuPkgs': 'numCpuPkgs', 'numCpuThreads': 'numCpuThreads',
                           'numHBAs': 'numHBAs', 'numNics': 'numNics',
                           'uuid': 'uuid', 'vendor': 'vendor', 'memorySize': 'memorySize'
                           }
        self.mock_size_fmt.return_value = 'memorySize'
        self.assertEqual(self.hardware_obj.info(), expected_result)
        self.mock_size_fmt.assert_called_once_with('memorySize')


class EXSiRuntimeTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_runtime = MagicMock(name='runtime')
        self.runtime_obj = self.module.EXSiRuntime(self.mock_runtime)

    def test_esxiproduct_info(self):
        self.mock_runtime.connectionState = 'connectionState'
        self.mock_runtime.inMaintenanceMode = 'inMaintenanceMode'
        expected_result = {'connectionState': 'connectionState',
                           'inMaintenanceMode': 'inMaintenanceMode'}
        self.assertEqual(self.runtime_obj.info(), expected_result)


class VMGuestTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_guest = MagicMock(name='guest')
        self.guest_obj = self.module.VMGuest(self.mock_guest)

    def test_vmguest_info(self):
        self.mock_guest.guestState = 'guestState'
        self.mock_guest.guestFamily = 'guestFamily'
        self.mock_guest.guestFullName = 'guestFullName'
        self.mock_guest.guestId = 'guestId'
        self.mock_guest.hostName = 'hostName'
        self.mock_guest.ipAddress = 'ipAddress'
        self.mock_guest.toolsStatus = 'toolsStatus'
        self.mock_guest.toolsVersion = 'toolsVersion'
        expected_result = {'guestState': 'guestState', 'guestFamily': 'guestFamily', 'guestFullName': 'guestFullName',
                           'guestId': 'guestId', 'hostName': 'hostName', 'ipAddress': 'ipAddress',
                           'toolsStatus': 'toolsStatus', 'toolsVersion': 'toolsVersion'}
        self.assertEqual(self.guest_obj.info(), expected_result)


class VMDiskTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_disk = MagicMock(name='disk')
        self.disk_obj = self.module.VMDisk(self.mock_disk)

    def test_vmdisk_info(self):
        self.disk_obj.guestState = 'capacity'
        self.disk_obj.guestFamily = 'freeSpace'
        self.mock_disk.diskPath = 'diskPath'
        expected_result = {'capacity': '1.0bytes',
                           'diskPath': 'diskPath', 'freeSpace': '1.0bytes'}
        self.assertEqual(self.disk_obj.info(), expected_result)


class VMNICDeviceTestCase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.nic_obj = self.module.VMNICDevice(
            {'type': 'VirtualE1000', 'label': 'Network adapter 1'})

    def test_vmdisk_info(self):
        expected_result = {'Network adapter 1': {
            'macAddress': None, 'summary': None, 'type': 'VirtualE1000'}}
        self.assertEqual(self.nic_obj.info(), expected_result)


class EXSiHostTestcase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_viproperty = MagicMock(name='viproperty')
        self.mock_pysphere.VIProperty.return_value = self.mock_viproperty
        self.host_obj = self.module.EXSiHost(
            'vserver', 'h_mor', 'h_name')
        self.mock_pysphere.VIProperty.assert_called_once_with(
            'vserver', 'h_mor')

    def test_esx_host_child_facts(self):
        self.mock_viproperty.vm = ['v1']
        mock_vm_cls = MagicMock(name='VM')
        mock_vm = MagicMock(name='vm')
        mock_vm.name = 'vm_name'
        self.mock_viproperty.vm = [mock_vm]
        mock_vm_cls.return_value.get_facts.return_value = {'vm': 'vm_facts'}
        self.module.VM = mock_vm_cls
        self.assertEqual(self.host_obj.get_child_facts(),
                         {'vms': [{'vm': 'vm_facts'}]})

        exp_call = [call('vserver', 'vm_name'), call().get_facts()]
        self.assertEqual(mock_vm_cls.mock_calls, exp_call)

    def test_esx_host_child_facts_empty(self):
        self.mock_viproperty.vm = []
        exp_result = {'vms': []}
        self.assertEqual(self.host_obj.get_child_facts(), exp_result)

    def test_esx_host_data_store_info(self):
        self.mock_viproperty.datastore = ['d1']
        mock_exsidatastore = MagicMock(name='EXSiDataStore')
        self.mock_viproperty.datastore = ['dt_store']
        mock_exsidatastore.return_value.info.return_value = {'dt1': 'dt1_info'}
        self.module.EXSiDataStore = mock_exsidatastore
        self.assertEqual(self.host_obj.get_datastore_info(),
                         {'datastore': {'dt1': 'dt1_info'}})
        exp_call = [call('dt_store'), call().info()]
        self.assertEqual(mock_exsidatastore.mock_calls, exp_call)

    def test_esx_host_data_store_info_empty(self):
        self.mock_viproperty.datastore = []
        self.assertEqual(self.host_obj.get_datastore_info(),
                         {'datastore': {}})

    def test_esx_host_product_info(self):
        mock_exsiproduct = MagicMock(name='EXSiProduct')
        self.mock_viproperty.summary.config.product = 'product'
        mock_exsiproduct.return_value.info.return_value = {'prd': 'prd1_info'}
        self.module.EXSiProduct = mock_exsiproduct
        self.assertEqual(self.host_obj.get_product_info(),
                         {'product': {'prd': 'prd1_info'}})
        exp_call = [call('product'), call().info()]
        self.assertEqual(mock_exsiproduct.mock_calls, exp_call)

    def test_esx_host_product_info_no_config(self):
        del self.mock_viproperty.summary.config
        self.assertEqual(self.host_obj.get_product_info(),
                         {'product': {}})

    def test_esx_host_product_info_no_product(self):
        del self.mock_viproperty.summary.config.product
        self.assertEqual(self.host_obj.get_product_info(),
                         {'product': {}})

    def test_esx_host_hardware_info(self):
        mock_exsihardware = MagicMock(name='EXSiHardware')
        self.mock_viproperty.summary.hardware = 'hardware'
        mock_exsihardware.return_value.info.return_value = {'hw': 'hw_info'}
        self.module.EXSiHardware = mock_exsihardware
        self.assertEqual(self.host_obj.get_hardware_info(),
                         {'hardware': {'hw': 'hw_info'}})
        exp_call = [call('hardware'), call().info()]
        self.assertEqual(mock_exsihardware.mock_calls, exp_call)

    def test_esx_host_hardware_info_no_hardware(self):
        del self.mock_viproperty.summary.hardware
        self.assertEqual(self.host_obj.get_hardware_info(),
                         {'hardware': {}})

    def test_esx_host_runtime_info(self):
        mock_exsiruntime = MagicMock(name='EXSiRuntime')
        self.mock_viproperty.summary.runtime = 'runtime'
        mock_exsiruntime.return_value.info.return_value = {
            'runtime': 'runtime_info'}
        self.module.EXSiRuntime = mock_exsiruntime
        self.assertEqual(self.host_obj.get_runtime_info(),
                         {'runtime': {'runtime': 'runtime_info'}})
        exp_call = [call('runtime'), call().info()]
        self.assertEqual(mock_exsiruntime.mock_calls, exp_call)

    def test_esx_host_runtime_info_no_runtime(self):
        del self.mock_viproperty.summary.runtime
        self.assertEqual(self.host_obj.get_runtime_info(),
                         {'runtime': {}})

    def test_collect_fact(self):
        mock_get_child_facts = MagicMock(name='get_child_facts')
        mock_get_child_facts.return_value = {'child': 'child_fact'}
        mock_get_datastore_info = MagicMock(name='get_datastore_info')
        mock_get_datastore_info.return_value = {'dt_store': 'dtstore_info'}
        mock_get_product_info = MagicMock(name='get_product_info')
        mock_get_product_info.return_value = {'prd_info': 'prd_info'}
        mock_get_product_info = MagicMock(name='get_product_info')
        mock_get_datastore_info.return_value = {'prd_info': 'prd_info'}
        mock_get_hardware_info = MagicMock(name='get_hardware_info')
        mock_get_hardware_info.return_value = {'hw_info': 'hw_info'}
        mock_get_runtime_info = MagicMock(name='get_runtime_info')
        mock_get_runtime_info.return_value = {'runtime_info': 'runtime_info'}
        fact_attrs = [('get_child_facts', mock_get_child_facts), ('get_datastore_info', mock_get_datastore_info),
                      ('get_product_info', mock_get_product_info), ('get_hardware_info',
                                                                    mock_get_hardware_info),
                      ('get_runtime_info', mock_get_runtime_info)]
        for attr, mock_attr in fact_attrs:
            setattr(self.host_obj, attr, mock_attr)
        exp_result = {'runtime_info': 'runtime_info', 'hw_info': 'hw_info',
                      'prd_info': 'prd_info', 'child': 'child_fact'}
        self.assertEqual(self.host_obj.collect_facts(), exp_result)
        for attr in fact_attrs:
            attr[1].assert_called_once_with()

    def test_get_facts(self):
        mock_collect_facts = MagicMock(name='collect_facts')
        mock_collect_facts.return_value = {'facts': 'f1'}
        self.host_obj.collect_facts = mock_collect_facts
        exp_result = {'h_mor': {'facts': 'f1', 'name': 'h_name'}}
        self.assertEqual(self.host_obj.get_facts(), exp_result)


class VMTestcase(VMwareBaseTestCase):
    def setUp(self):
        VMwareBaseTestCase.setUp(self)
        self.mock_vserver = MagicMock(name='vserver')
        self.mock_vm = MagicMock(name='vm')
        self.mock_vserver.get_vm_by_name.return_value = self.mock_vm
        self.vm_obj = self.module.VM(self.mock_vserver, 'vm_name')
        self.mock_vserver.get_vm_by_name.assert_called_once_with('vm_name')

    def test_vm_guest_info(self):
        mock_vmguest = MagicMock(name='vmguest')
        self.module.VMGuest = mock_vmguest
        self.mock_vm.properties.guest = 'guest'
        mock_vmguest.return_value.info.return_value = {'guest': 'guest_info'}
        self.assertEqual(self.vm_obj.get_vm_guest_info(), {
            'guest': {'guest': 'guest_info'}})
        exp_call = [call('guest'), call().info()]
        self.assertEqual(mock_vmguest.mock_calls, exp_call)

    def test_vm_guest_info_empty(self):
        del self.mock_vm.properties.guest
        self.assertEqual(self.vm_obj.get_vm_guest_info(),
                         {'guest': {}})

    def test_vm_overall_status(self):
        self.mock_vm.properties.summary.overallStatus = 'overallStatus'
        self.assertEqual(self.vm_obj.overall_status(), {
            'overallStatus': 'overallStatus'})

    def test_vm_runtime_attr(self):
        self.mock_vm.properties.runtime.connectionState = 'connectionState'
        self.mock_vm.properties.runtime.powerState = 'powerState'
        self.assertEqual(self.vm_obj.get_runtime_attr(), {'connectionState': 'connectionState',
                                                          'powerState': 'powerState'})

    def test_vm_summary_config_attr(self):
        self.mock_vm.properties.summary.config.name = 'name'
        self.mock_vserver.get_vm_by_name(
        ).properties.summary.config.guestFullName = 'guestFullName'
        self.mock_vm.properties.summary.config.guestId = 'guestId'
        self.mock_vm.properties.summary.config.memorySizeMB = 'memorySizeMB'
        self.mock_vm.properties.summary.config.numCpu = 'numCpu'
        self.mock_vserver.get_vm_by_name(
        ).properties.summary.config.numEthernetCards = 'numEthernetCards'
        self.mock_vserver.get_vm_by_name(
        ).properties.summary.config.numVirtualDisks = 'numVirtualDisks'
        expected_result = {'name': 'name', 'guestFullName': 'guestFullName', 'guestId': 'guestId',
                           'memorySizeMB': 'memorySizeMB', 'numCpu': 'numCpu', 'numEthernetCards': 'numEthernetCards',
                           'numVirtualDisks': 'numVirtualDisks'}
        self.assertEqual(
            self.vm_obj.get_summary_config_attrs(), expected_result)

    def test_vm_get_facts(self):
        mock_get_vm_guest_info = MagicMock(name='get_vm_guest_info')
        mock_get_vm_guest_info.return_value = {'guest': 'guest_info'}
        mock_get_vm_disk_info = MagicMock(name='get_vm_disk_info')
        mock_get_vm_disk_info.return_value = {'disks': 'disk_info'}
        mock_overall_status = MagicMock(name='overall_status')
        mock_overall_status.return_value = {'overall_status': 'overall_status'}
        mock_get_runtime_attr = MagicMock(name='get_runtime_attr')
        mock_get_runtime_attr.return_value = {'runtime_attr': 'runtime_attr'}
        mock_get_nic_info = MagicMock(name='get_nic_info')
        mock_get_nic_info.return_value = {'nics': 'nics_dev_info'}
        mock_get_summary_config_attrs = MagicMock(name='get_summary_config_attrs')
        mock_get_summary_config_attrs.return_value = {'summary_attrs': 'summary_attrs'}
        fact_attrs = (('get_vm_guest_info', mock_get_vm_guest_info), ('get_vm_disk_info', mock_get_vm_disk_info),
                      ('overall_status', mock_overall_status), ('get_runtime_attr',
                                                                mock_get_runtime_attr),
                      ('get_nic_info', mock_get_nic_info), ('get_summary_config_attrs', mock_get_summary_config_attrs))
        for attr, mock_attr in fact_attrs:
            setattr(self.vm_obj, attr, mock_attr)
        exp_result = {'guest': 'guest_info', 'disks': 'disk_info',
                      'overall_status': 'overall_status', 'runtime_attr': 'runtime_attr', 'nics': 'nics_dev_info',
                      'summary_attrs': 'summary_attrs'}
        self.assertEqual(self.vm_obj.get_facts(), exp_result)
        for attr in fact_attrs:
            attr[1].assert_called_once_with()

    def test_vm_get_vm_disk_info(self):
        self.mock_vm.properties.guest.disk = ['disk1']
        mock_vm_disk = MagicMock(name='VMDisk')
        mock_vm_disk.return_value.info.return_value = {'d1': 'disk_info'}
        self.module.VMDisk = mock_vm_disk
        exp_result = {'disks': {'virtual_disk1': {'d1': 'disk_info'}}}
        self.assertEqual(self.vm_obj.get_vm_disk_info(), exp_result)
        exp_call = [call('disk1'), call().info()]
        self.assertEqual(mock_vm_disk.mock_calls, exp_call)

    def test_vm_get_vm_disk_info_no_disk(self):
        del self.mock_vm.properties.guest.disk
        exp_result = {'disks': {}}
        self.assertEqual(self.vm_obj.get_vm_disk_info(), exp_result)

    def test_vm_get_vm_disk_info_empty_disk(self):
        self.mock_vm.properties.guest.disk = []
        exp_result = {'disks': {}}
        self.assertEqual(self.vm_obj.get_vm_disk_info(), exp_result)

    def test_vm_get_nic_info(self):
        mock_vmnicdevice = MagicMock(name='VMNICDevice')
        mock_vmnicdevice.return_value.info.return_value = {'d1': 'dev1'}
        self.module.VMNICDevice = mock_vmnicdevice
        self.mock_vm.get_property.return_value.values.return_value = ['device1']
        exp_result = {'nics': {'d1': 'dev1'}}
        self.assertEqual(self.vm_obj.get_nic_info(), exp_result)
        exp_call = [call('device1'), call().info()]
        self.assertEqual(mock_vmnicdevice.mock_calls, exp_call)


if __name__ == '__main__':
    unittest.main()

# import unittest
# from mock import MagicMock, patch
#
#
# class ScanlineVCenterHostScannerTestCase(unittest.TestCase):
#     def setUp(self):
#         unittest.TestCase.setUp(self)
#         self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
#         self.mock_pysphere = MagicMock(name='pysphere')
#         self.mock_utilities = MagicMock()
#         modules = {
#             'cached_property': self.mock_cached_property,
#             'logging': MagicMock(name='logging'),
#             'pysphere': self.mock_pysphere,
#             'scanline.utilities.vmware': self.mock_utilities
#         }
#         self.module_patcher = patch.dict('sys.modules', modules)
#         self.module_patcher.start()
#         from scanline.host.vmware import vCenterHostScanner
#         self.vCenterHostScanner = vCenterHostScanner
#
#     def tearDown(self):
#         unittest.TestCase.tearDown(self)
#         self.module_patcher.stop()
#
#     def test_basic_check(self):
#         scanner = self.vCenterHostScanner('host2', '167.81.183.99', 'tester', 'CryptThis', tags=['x', 'y'])
#         expected = {'clusters': {}, 'datacenters': {}, 'hosts': {}, 'resource_pools': {}, 'vms': []}
#         self.assertEqual(scanner.get_facts(), expected)
#
#
# if __name__ == '__main__':
#     unittest.main()
