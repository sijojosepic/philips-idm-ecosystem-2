import unittest
from mock import MagicMock, patch, call


class ScanlineGenericProductTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_host = MagicMock()
        self.mock_http = MagicMock()
        self.mock_utilities = MagicMock()
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.token_mgr': self.mock_utilities.token_mgr,
            'scanline.utilities.dns': self.mock_utilities.dns,
            'scanline.utilities.config_reader': self.mock_utilities.dns,
            'scanline.utilities.http': self.mock_utilities.dns,
            'logging': MagicMock(name='logging')
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.http_patch = patch('scanline.product.HTTPRequester')
        self.http_patcher = self.http_patch.start()
        self.mock_host_scanner = MagicMock(name='host_scanner')
        from scanline.product import GenericProductScanner
        self.moduleclass = GenericProductScanner
        self.GPS_obj = GenericProductScanner(
            scanner='scan1',
            address='167.81.183.99',
            tags=['y', 'u'],
            hmac_enabled=False,
            host_scanner=self.mock_host_scanner,
            discovery_url='http://testurl.com',
            environmentType='production')
        self.GPS_obj_1 = GenericProductScanner(
            scanner='scan1',
            address='167.81.183.99',
            tags=['y', 'u'],
            hmac_enabled=False,
            host_scanner=self.mock_host_scanner,
            discovery_url='http://testurl.com'
        )

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.http_patch.stop()

    def test_hmac_enabled_when_available_in_kwargs(self):
        obj = self.moduleclass(
            scanner='s1', address='1.1.1.1', hmac_enabled='yes')
        self.assertEqual('yes', obj.hmac_enabled)

    def test_hmac_enabled_when_not_available_in_kwargs(self):
        obj = self.moduleclass(scanner='s1', address='1.1.1.1')
        self.assertEqual(True, obj.hmac_enabled)

    def test_discovery_info_empty_rsp(self):
        self.http_patcher.return_value.suppressed_get.return_value = None
        self.assertEqual(self.GPS_obj.discovery_info, {})
        self.http_patcher().suppressed_get.assert_called_once_with('http://testurl.com')

    def test_discovery_info_proper_rsp(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {'test': 'ok'}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.discovery_info, {'test': 'ok'})
        self.http_patcher().suppressed_get.assert_called_once_with('http://testurl.com')
        mk_obj.json.assert_called_once_with()

    def test_environment_type(self):
        self.GPS_obj.get_environment_type = MagicMock(name='get_environment_type')
        self.GPS_obj.get_environment_type.return_value = 'production'
        self.assertEqual(self.GPS_obj.environmentType, 'production')

    def test_get_environment_type(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {'environmentType': 'test'}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.get_environment_type(), 'test')

    def test_get_environment_type_empty(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {'environmentType': ''}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.get_environment_type(), 'production')

    def test_get_environment_type_missing(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj_1.get_environment_type(), '')

    def test_get_hosts_proper_rsp(self):
        mk_obj = MagicMock()
        mk_obj.json.return_value = {'hostinfo': ['h1', 'h2']}
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.get_hosts(), ['h1', 'h2'])
        self.http_patcher().suppressed_get.assert_called_once_with('http://testurl.com')

    def test_do_request_when_hmac_enabled(self):
        self.GPS_obj.hmac_enabled = True
        self.assertEqual(self.GPS_obj.do_request(),
                         self.mock_utilities.token_mgr.do_hmac_request.return_value)
        self.mock_utilities.token_mgr.do_hmac_request.assert_called_once_with(self.GPS_obj.uri)

    def test_do_request_when_hmac_not_enabled(self):
        self.assertEqual(self.GPS_obj.do_request(),
                         self.http_patcher().suppressed_get.return_value)
        self.http_patcher().suppressed_get.assert_called_once_with(self.GPS_obj.uri)


    # def test_get_hosts_exception_case1(self):
    #     mk_obj = MagicMock()
    #     mk_obj.json.return_value = {'hostinfo': []}
    #     self.http_patcher.return_value.suppressed_get.return_value = mk_obj
    #     self.assertRaises(Exception, self.GPS_obj.get_hosts)

    # def test_get_hosts_exception_case2(self):
    #     mk_obj = MagicMock()
    #     mk_obj.json.return_value = {}
    #     self.http_patcher.return_value.suppressed_get.return_value = mk_obj
    #     self.assertRaises(Exception, self.GPS_obj.get_hosts)

    def test_get_hostname(self):
        host = {'address': '1.0.1.1'}
        self.assertEqual(self.GPS_obj.get_hostname(host), '1.0.1.1')

    def test_get_hostname_empty_resp(self):
        self.assertEqual(self.GPS_obj.get_hostname({}), '')

    def test_scan_host(self):
        mk_obj = MagicMock()
        mk_obj.to_dict.return_value = {'test': 'ok'}
        self.mock_host_scanner.return_value = mk_obj
        discovery_info = {'productversion': '1.2',
                          'productname': 'prod', 'productid': 'productid'}
        mk_obj = MagicMock()
        mk_obj.json.return_value = discovery_info
        self.http_patcher.return_value.suppressed_get.return_value = mk_obj
        self.assertEqual(self.GPS_obj.scan_host(
            {'address': '1.2.3.4', 'role': 'application'}),
            {'test': 'ok', 'applications': None, 'service_excludes': [], 'osType': None})
        self.mock_host_scanner.assert_called_once_with('1.2.3.4',
                                                       {'scanner': 'scan1',
                                                           'address': '167.81.183.99'},
                                                       discovery_url='http://testurl.com', environmentType='production',
                                                       hmac_enabled=False, product_id='productid', product_name='prod',
                                                       product_version='1.2', role=['application'],
                                                       tags=['y', 'u'])
        # mk_obj.to_dict.assert_called_once()

    def test_site_facts(self):
        h_names = {'h1': '1.2.3.4', 'h2': '3.4.5.6'}
        h_facts = {'h1': ['f11', 'f12', 'f13'], 'h2': ['f21', 'f22', 'f23']}
        mk_name = MagicMock(side_effect=lambda host: h_names[host])
        mk_facts = MagicMock(side_effect=lambda host: h_facts[host])
        self.GPS_obj.get_hostname = mk_name
        self.GPS_obj.get_host_facts = mk_facts
        self.GPS_obj.get_hosts = MagicMock(
            return_value=['h1', 'h2'])
        self.assertEqual(self.GPS_obj.site_facts(), {'1.2.3.4': [
                         'f11', 'f12', 'f13'], '3.4.5.6': ['f21', 'f22', 'f23']})
        expected_call = [call('h1'), call('h2')]
        mk_expected_call = [call('h1'), call('h2')]
        self.assertEqual(mk_name.call_args_list, expected_call)
        self.assertEqual(mk_facts.call_args_list, mk_expected_call)

    def test_get_host_role(self):
        host = {'name': 'ibe.cloudapp.net',
                'address': '100.75.164.20', 'role': 'application'}
        self.assertEqual(self.GPS_obj.get_host_role(host), ['application'])

    def test_get_service_dsc(self):
        application = {"name": "DHCPClient","applicationType": "Windows-service",
        "applicationMonitoring": {"monitoringDetails":
         {"resource": "Dhcp"}}}
        self.GPS_obj.discovery_info.get.return_value = 'ISPACS'
        self.assertEqual(self.GPS_obj.get_service_dsc(application),
         'Product__ISPACS__Windows-service__DHCPClient__Status')

    def test_get_service_dsc_webapi(self):
        application = {"name": "DHCPClient","applicationType": "WebAPI",
        "applicationMonitoring": {"monitoringDetails":
         {"resource": "Dhcp", "checkType": "Ping"}}}
        self.GPS_obj.discovery_info.get.return_value = 'ISPACS'
        self.assertEqual(self.GPS_obj.get_service_dsc(application),
         'Product__ISPACS__WebAPI_Ping__DHCPClient__Status')


if __name__ == '__main__':
    unittest.main()
