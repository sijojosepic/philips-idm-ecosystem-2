import unittest
from mock import MagicMock, patch


class FakeClass(object):
    """The purpose of this class is to Fake the Base classes"""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class ScanlineNextGenTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'cached_property': self.mock_cached_property,
            'scanline.host': self.mock_scanline.host,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product import nextgengd
        from scanline.product.nextgengd import NextGenProductScanner
        from scanline.product import GenericProductScanner
        self.module = nextgengd
        self.NextGenProductScanner = NextGenProductScanner
        NextGenProductScanner.__bases__ = (FakeClass.imitate(GenericProductScanner),)
        self.scanner_obj = NextGenProductScanner('scanner', '10.55.66.1', 'username', 'password')
        self.PS_SCRIPT = 'Get-Content -Path {0}'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_ps_scirpt(self):
        self.scanner_obj = self.NextGenProductScanner('scanner', '10.55.66.1', 'u1', 'p1', {'file_path': 'C:\Temp'})
        self.scanner_obj.get_ps_scirpt()

    def test_get_discovery_info_file_path_exist(self):
        self.mock_scanline.utilities.win_rm.execute_powershell_script.return_value = '{"key": "value"}'
        self.scanner_obj.uri = None
        self.scanner_obj.file_path = 'file/path'
        data = self.scanner_obj.get_discovery_info()
        self.assertEqual(data, {'key': 'value'})

    def test_get_discovery_info_url_exist(self):
        self.mock_scanline.utilities.win_rm.execute_powershell_script.return_value = None
        self.scanner_obj.uri = None
        self.scanner_obj.file_path = 'file/path'
        self.assertRaises(RuntimeError, self.scanner_obj.get_discovery_info)

    def test_get_discovery_info(self):
        mock_info = MagicMock(name='info')
        mock_info.get_discovery_info.return_value = {'key': 'value'}
        self.module.super = MagicMock(name='super', return_value=mock_info)
        self.scanner_obj.uri = 'file/path'
        data = self.scanner_obj.get_discovery_info()
        self.assertEqual(data, {'key': 'value'})


class ScanlineISEEGDTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'cached_property': self.mock_cached_property,
            'scanline.host': self.mock_scanline.host,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.nextgengd import ISEEGDProductScanner
        from scanline.product.nextgengd import NextGenProductScanner
        self.module = ISEEGDProductScanner
        self.ISEEGDProductScanner = ISEEGDProductScanner
        ISEEGDProductScanner.__bases__ = (FakeClass.imitate(NextGenProductScanner),)
        self.scanner_obj = ISEEGDProductScanner('scanner', '10.55.66.1', 'username', 'password')

    def test_get_host_info(self):
        obj = MagicMock(name='result')
        obj.to_dict.return_value = {'test_data': 'data'}
        self.scanner_obj.get_host_role = MagicMock(name='get_host_role', return_value='role')
        self.scanner_obj.endpoint = {'scanner': 'abc'}
        self.scanner_obj.tags = 'windows_service_flag'
        self.scanner_obj.discovery_info = {'productid': 'iSEG', 'productname': 'Intellispace', 'productversion': '1.1.1.1'}
        self.scanner_obj.username = 'user'
        self.scanner_obj.password = 'pass'
        self.scanner_obj.environmentType = 'production'
        self.scanner_obj.kwargs = {}
        self.scanner_obj.host_scanner = MagicMock(name='host_scanner', return_value=obj)
        output = self.scanner_obj.get_host_info({'address': '1.1.1.1'})
        self.assertEqual(output, {'test_data': 'data'})

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()

if __name__ == '__main__':
    unittest.main()
