import unittest
from mock import MagicMock, patch
from redis.exceptions import RedisError


class ScanlineUtilitiesCacheCacheMgrTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_redis = MagicMock(name='redis')
        self.mock_db = MagicMock(name='db')
        modules = {
            'redis': self.mock_redis,
            'redis.Redis': self.mock_redis.Redis,
            'redis.Redis.hmset': self.mock_redis.Redis.hmset,
            'redis.Redis.set': self.mock_redis.Redis.set,
            'redis.Redis.setex': self.mock_redis.Redis.setex,
         }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities.cache import CacheMgr  
        self.CacheMgr = CacheMgr

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_cachemgr_set_with_dict(self):
        value = {
            "dictionary": "true",
        }
        self.CacheMgr.set('key1', value)
        self.assertEqual(self.CacheMgr.server.hmset.call_count, 1)
        self.assertEqual(self.CacheMgr.server.set.call_count, 0)

    def test_cachemgr_set_with_no_dict(self):
        value = 'not dicts'
        self.CacheMgr.set('key1', value)
        self.assertEqual(self.CacheMgr.server.hmset.call_count, 0)
        self.assertEqual(self.CacheMgr.server.set.call_count, 1)

    def test_cachemgr_get_with_mulit_true(self):
        self.CacheMgr.get('key1', True)
        self.assertEqual(self.CacheMgr.server.hgetall.call_count, 1)
        self.assertEqual(self.CacheMgr.server.get.call_count, 0)

    def test_cachemgr_get_with_multi_false(self):
        self.CacheMgr.get('key1', False)
        self.assertEqual(self.CacheMgr.server.hgetall.call_count, 0)
        self.assertEqual(self.CacheMgr.server.get.call_count, 1)

    def test_cachemgr_setex(self):
        self.CacheMgr.setex('key1', 'value', 10)
        self.assertEqual(self.CacheMgr.server.setex.call_count, 1)

    def test_cachemgr_remove(self):
        self.CacheMgr.remove('key1')
        self.assertEqual(self.CacheMgr.server.delete.call_count, 1)


if __name__ == '__main__':
    unittest.main()
