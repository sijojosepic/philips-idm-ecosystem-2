import unittest
from mock import MagicMock, patch, call


class FakeClass(object):
    """The purpose of this class is to Fake the Base classes"""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class XPERIMGDHostScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_resource = MagicMock(name='resource')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.dns': self.mock_utilities.dns,
            'scanline.utilities.config_reader': self.mock_utilities.config_reader,
            'scanline.utilities.wmi': self.mock_utilities.wmi,
            'phimutils.resource': self.mock_resource,
            'cached_property': MagicMock(name='cached_property', cached_property=property)}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import HostScanner
        from scanline.host.xperimgd import XPERIMGDHostScanner
        XPERIMGDHostScanner.__bases__ = (FakeClass.imitate(HostScanner),)
        self.xperscanner = XPERIMGDHostScanner
        self.prd_obj = XPERIMGDHostScanner('scanner', '10.55.66.1', 'u1', 'p1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_credentials(self):
        mock_get_credentials = MagicMock(name='get_credentials')
        self.prd_obj.get_credentials = mock_get_credentials
        self.assertEquals(self.prd_obj.credentials,
                          mock_get_credentials.return_value)
        mock_get_credentials.assert_called_once_with()

    def test_encrypter(self):
        self.assertEquals(self.prd_obj.encrypter,
                          self.mock_resource.Encrypter.return_value)
        self.mock_resource.Encrypter.assert_called_once_with(
            self.mock_utilities.config_reader.get_idm_secret.return_value)
        self.mock_utilities.config_reader.get_idm_secret.assert_called_once_with()

    def test_get_credentials(self):
        self.mock_resource.Encrypter().encrypt.side_effect = ['usr', 'pass']
        self.prd_obj = self.xperscanner('scanner', '10.55.66.1', 'u1', 'p1')
        self.assertEquals(self.prd_obj.get_credentials(), {
                          'username': 'usr', 'password': 'pass'})
        exp_call = [call('u1'), call('p1')]
        self.assertEquals(self.mock_resource.Encrypter(
        ).encrypt.call_args_list, exp_call)
