import unittest
from mock import patch, MagicMock


class UtilsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_ast = MagicMock(name='ast')
        modules = {
            'ast': self.mock_ast,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import utils
        self.module = utils

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_str_to_list_str_value(self):
        val = "'slkdfjasldfj'"
        self.mock_ast.literal_eval.return_value = 'slkdfjasldfj'
        self.assertEqual(self.module.str_to_list(val), [])
        self.mock_ast.literal_eval.assert_called_once_with(val)

    def test_str_to_list_str_list_value(self):
        val = "'['a','b','c']'"
        exp_val = ['a', 'b', 'c']
        self.mock_ast.literal_eval.return_value = exp_val
        self.assertEqual(self.module.str_to_list(val), exp_val)

    def test_format_status_ok(self):
        self.assertEquals((0, 'ok_msg | perf_msg'), self.module.format_status(
            '', '', 'ok_msg', 'perf_msg'))

    def test_format_status_critical(self):
        self.assertEquals((2, 'crt_msg | perf_msg'), self.module.format_status(
            '', 'crt_msg', '', 'perf_msg'))

    def test_format_status_warning(self):
        self.assertEquals((1, 'warn_msg | perf_msg'), self.module.format_status(
            'warn_msg', '', '', 'perf_msg'))

    def test_safe_mongo_keys_empty_dict(self):
        input_dict = {}
        exp_dict = {}
        self.assertEquals(exp_dict, self.module.safe_mongo_keys(input_dict))

    def test_safe_mongo_keys_non_nested_dict(self):
        input_dict = {'a.a': 'aaa', 'b.b': 'b.b', 'c': 'c'}
        exp_dict = {'a-a': 'aaa', 'b-b': 'b.b', 'c': 'c'}
        self.assertEquals(exp_dict, self.module.safe_mongo_keys(input_dict))

    def test_safe_mongo_keys_clean_dict(self):
        input_dict = {'a': 'a', 'b': 'b.b', 'c': 'c'}
        exp_dict = {'a': 'a', 'b': 'b.b', 'c': 'c'}
        self.assertEquals(exp_dict, self.module.safe_mongo_keys(input_dict))

    def test_safe_mongo_keys_nested_dict(self):
        input_dict = {'a.a': {'b.b.b': 'bbb',
                              'e.e': 'ff',
                              'p.p': {'g.g': 'll', 'm.m': 'n.n.n', 'o.o': {'q.q': 'qq', 't.t': 'tt'}}},
                      'e.e.e.e': 'eeee'}
        exp_dict = {'a-a': {'b-b-b': 'bbb',
                            'e-e': 'ff',
                            'p-p': {'g-g': 'll', 'm-m': 'n.n.n', 'o-o': {'q-q': 'qq', 't-t': 'tt'}}},
                    'e-e-e-e': 'eeee'}
        self.assertEquals(exp_dict, self.module.safe_mongo_keys(input_dict))

    def test_safe_mongo_keys_place_holder(self):
        input_dict = {'a.a': {'b.b.b': 'bbb',
                              'e.e': 'ff',
                              'p.p': {'g.g': 'll', 'm.m': 'n.n.n', 'o.o': {'q.q': 'qq', 't.t': 'tt'}}},
                      'e.e.e.e': 'eeee'}
        exp_dict = {'a_a': {'b_b_b': 'bbb',
                            'e_e': 'ff',
                            'p_p': {'g_g': 'll', 'm_m': 'n.n.n', 'o_o': {'q_q': 'qq', 't_t': 'tt'}}},
                    'e_e_e_e': 'eeee'}
        self.assertEquals(exp_dict,
                          self.module.safe_mongo_keys(input_dict, '_'))


if __name__ == '__main__':
    unittest.main()
