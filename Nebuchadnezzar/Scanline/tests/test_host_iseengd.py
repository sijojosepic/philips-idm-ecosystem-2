import unittest
from mock import MagicMock, patch


class FakeClass(object):
    """The purpose of this class is to Fake the Base classes"""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class ScanlineISEETestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.iseengd import ISEEHostScanner
        from scanline.product import GenericProductScanner
        self.ISEEHostScanner = ISEEHostScanner
        ISEEHostScanner.__bases__ = (FakeClass.imitate(GenericProductScanner),)
        self.scanner_obj = ISEEHostScanner('scanner', '10.55.66.1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_win_service_flag_no(self):
        self.assertEqual(self.scanner_obj.win_service_flag, 'no')

    def test_win_service_flag_yes(self):
        win_flag = "yes"
        self.scanner_obj.win_service_flag = win_flag
        self.assertEqual(self.scanner_obj.win_service_flag, 'yes')


class ScanlineIECGGDTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.iseengd import IECGGDHostScanner
        from scanline.product import GenericProductScanner
        self.IECGGDHostScanner = IECGGDHostScanner
        IECGGDHostScanner.__bases__ = (FakeClass.imitate(GenericProductScanner),)
        self.scanner_obj = IECGGDHostScanner('scanner', '10.55.66.1')
        self.PS_SCRIPT = 'Get-Content -Path {0}'
        self.scanner_obj.role = ['iECGAPP']
        self.scanner_obj.hostname = '1.1.1.1'
        self.scanner_obj.username = 'test'
        self.scanner_obj.password = 'pass'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_registry_path(self):
        self.scanner_obj.get_registry_path = MagicMock(name='get_registry_path', return_value="F")
        self.assertEqual(self.scanner_obj.registry_path, "F")

    def test_get_registry_path_accessible(self):
        mock_execute_powershell_script = MagicMock(name="execute_powershell_script", return_value="C")
        self.mock_scanline.utilities.win_rm.execute_powershell_script.return_value = mock_execute_powershell_script
        data = self.scanner_obj.get_registry_path()
        self.assertEqual(data, "C")

    def test_get_registry_path_not_accessible(self):
        drive = None
        self.scanner_obj.drive = drive
        data = self.scanner_obj.get_registry_path()
        self.assertEqual(data, "C")


if __name__ == '__main__':
    unittest.main()

if __name__ == '__main__':
    unittest.main()
