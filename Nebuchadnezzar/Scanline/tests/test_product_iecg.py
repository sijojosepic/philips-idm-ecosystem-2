import unittest
from mock import patch, MagicMock


class FakeGenericProductScanner(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    if name == '__init__':
                        setattr(cls, name, lambda x, y, z: None)
                    else:
                        setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class ScanlineIECGProductScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_logging = MagicMock(name='logging')
        # self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)

        modules = {
            'logging': self.mock_logging,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.iecg import IECGProductScanner
        from scanline.product import GenericProductScanner
        IECGProductScanner.__bases__ = (
            FakeGenericProductScanner.imitate(GenericProductScanner),)
        self.IECGProductScanner = IECGProductScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_product_iecg_scanner_called(self):
        scanner = self.IECGProductScanner('scan1', '161.81.183.99')
        self.assertEquals(hasattr(scanner, 'get_hostname'), True)
        self.assertEquals(hasattr(scanner, 'discovery_info'), True)


if __name__ == '__main__':
    unittest.main()
