import unittest
from mock import MagicMock, patch, mock_open


class ConfigReaderTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_yaml = MagicMock(name='yaml')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_json = MagicMock(name='json')
        self.mock_warnings = MagicMock(name = 'warnings')
        self.mock_requests = MagicMock(name = 'requests')
        self.mock_base64 = MagicMock(name = 'base64')
        modules = {
            'linecache': self.mock_linecache,
            'tbconfig': self.mock_tbconfig,
            'yaml': self.mock_yaml,
            'phimutils': self.mock_phimutils,
            'phimutils.resource': self.mock_phimutils.resource,
            'json': self.mock_json,
            'warnings': self.mock_warnings,
            'requests': self.mock_requests,
            'base64': self.mock_base64
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import config_reader
        self.module = config_reader
        self.module.DISCOVERY = {'SECRET_FILE': 'etc',
                                 'CONFIGURATION': '/etc/philips/discovery.yml'
                                 }

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_idm_secret(self):
        self.mock_linecache.getline.return_value = 'secret\n'
        self.assertEquals(self.module.get_idm_secret(), 'secret')

    def test_get_idm_secret_plain_text(self):
        self.mock_linecache.getline.return_value = 'secret'
        self.assertEquals(self.module.get_idm_secret(), 'secret')

    def test_get_site_id(self):
        mock_opn = mock_open(read_data='\n SITEID \n\r')
        with patch('scanline.utilities.config_reader.open', mock_opn, create=True):
            self.assertEqual(self.module.get_site_id(),
                             'SITEID')
            mock_opn.assert_called_once_with('/etc/siteid', 'r')

    def test_get_site_id_non_default(self):
        mock_opn = mock_open(read_data='\n SITEID \n\r')
        with patch('scanline.utilities.config_reader.open', mock_opn, create=True):
            self.assertEqual(self.module.get_site_id('abc'),
                             'SITEID')
            mock_opn.assert_called_once_with('abc', 'r')

    def test_discovery_yml_contents(self):
        mock_opn = mock_open(read_data='\n enp:// \n\r')
        with patch('scanline.utilities.config_reader.open', mock_opn, create=True):
            self.assertEqual(self.module.discovery_yml_contents(),
                             self.mock_yaml.load.return_value)

    def test_get_scanners(self):
        mock_discovery_yml_contents = MagicMock(name='yml_contents')
        mock_discovery_yml_contents.return_value = [{'scanner': 'ISP'}]
        self.module.discovery_yml_contents = mock_discovery_yml_contents
        self.assertEqual(self.module.get_scanners(), ['isp'])

    def test_get_resource_value(self):
        self.module.tbconfig.DISCOVERY = {'NAGIOS_RESOURCE_FILE': 'nf'}
        mock_ng_resourcer = MagicMock(name='NagiosResourcer')
        mock_get_idm_secret = MagicMock(name='get_idm_secret', return_value='secret')
        self.module.get_idm_secret = mock_get_idm_secret
        self.mock_phimutils.resource.NagiosResourcer.return_value = mock_ng_resourcer
        resource_val = self.module.get_resource_value('r1')
        self.assertEqual(resource_val, mock_ng_resourcer.get_resource.return_value)
        self.mock_phimutils.resource.NagiosResourcer.assert_called_once_with('nf', 'secret')

    def test_read_from_tbconfig(self):
        self.module.tbconfig.VERSION = {'enp': 'enp'}
        response = self.module.read_from_tbconfig('VERSION')
        self.assertEqual(response, {'enp': 'enp'})

    def test_get_thruk_response(self):
        self.module.read_from_tbconfig = MagicMock(name='read_from_tbconfig')
        config = {'URL':'url','username':'usr','password':'pwd'}
        self.module.read_from_tbconfig.return_value = config
        resp = self.module.get_thruk_response('sub_url')
        self.assertEqual(self.mock_requests.get.return_value.json.return_value,resp)
        self.module.read_from_tbconfig.assert_called_once_with('THRUK')
        self.mock_base64.b64decode.assert_called_once_with('pwd')
        self.mock_requests.get.assert_called_once_with('url/sub_url',auth=('usr',self.mock_base64.b64decode.return_value),
                                                       verify=False,params={'view_mode': 'json'})
