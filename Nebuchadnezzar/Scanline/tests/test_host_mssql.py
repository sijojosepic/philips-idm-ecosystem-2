import unittest
from mock import MagicMock, patch


class ScanlineHostMSSQLTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.mssql import MSSQLHostScanner
        self.MSSQLHostScanner = MSSQLHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_facts(self):
        scanner = self.MSSQLHostScanner('host1', '167.81.183.99', None, None, None, tags=['x', 'y'])
        mock_scanner = MagicMock(spec=scanner)
        for prop in self.MSSQLHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.MSSQLHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))


if __name__ == '__main__':
    unittest.main()
