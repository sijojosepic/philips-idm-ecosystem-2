import unittest
from mock import MagicMock, patch


class ScanlineHostISiteTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.isite import ISiteHostScanner
        self.ISiteHostScanner = ISiteHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        scanner = self.ISiteHostScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.assertEqual(self.ISiteHostScanner.module_name, 'ISPACS Billing')


if __name__ == '__main__':
    unittest.main()
