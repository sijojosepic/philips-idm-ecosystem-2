import unittest
from mock import MagicMock, patch


class ScanlineHostIBETestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.ibe import IBEHostScanner
        self.IBEHostScanner = IBEHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        self.scanner_instance = self.IBEHostScanner(
        'host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y']
    )
        self.assertEqual(self.IBEHostScanner.module_name, 'IBE')

 
if __name__ == '__main__':
    unittest.main()
