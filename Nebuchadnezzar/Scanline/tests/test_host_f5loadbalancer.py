import unittest
from mock import MagicMock, patch


class ScanlineF5loadBalancerHostScannerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_logging = MagicMock(name='logging')
        modules = {
            'requests': self.mock_request,
            'logging': self.mock_logging
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host import f5loadbalancer
        self.module = f5loadbalancer
        self.f5lb_scanner = self.module.F5LBHostScanner(
            'vCenter', '10.220.3.1', 'tester', 'CryptThis')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_request_200(self):
        mock_get = MagicMock(name='get_request')
        json_val = {'status.availabilityState': {'description': 'offline'}}
        mock_get.json.return_value = json_val
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_request(
            'http://testurl.com'), json_val)

    def test_get_request_404(self):
        mock_get = MagicMock(name='get_request')
        mock_get.status_code = 404
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_request(
            'http://testurl.com'), False)

    def test_val_dict_False(self):
        self.assertEqual(self.f5lb_scanner.val_dict(pool_stats={}), False)

    def test_val_dict_json(self):
        pool_stats = {'status.availabilityState': {'description': 'offline'}}
        self.assertEqual(self.f5lb_scanner.val_dict(
            pool_stats=pool_stats), 'offline')

    def test_val_dict_not_dict(self):
        pool_stats = {'availabilityState': 'description'}
        self.assertEqual(self.f5lb_scanner.val_dict(
            pool_stats=pool_stats), False)

    def test_get_facts_false(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {}
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_facts(), {})

    def test_get_facts_no_items(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {
            'kind': 'abc', 'selflink': 'http://abc.oom'}
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_facts(), {})

    def test_get_facts_items_with_nopoolmembers(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {
            'items': [{'name': 'DICOM-102', 'fullPath': ''}]}
        mock_get.status_code = 200
        mock_pool_stats = MagicMock(name='pool_stats')
        mock_pool_stats.json.return_value = {
            'status.availabilityState': {'description': 'offline'}}
        mock_pool_stats.status_code = 200
        mock_pool_members = MagicMock(name='pool_stats')
        mock_pool_members.json.return_value = {'items': []}
        mock_pool_members.status_code = 200
        self.mock_request.get.side_effect = [
            mock_get, mock_pool_stats, mock_pool_members]
        self.assertEqual(self.f5lb_scanner.get_facts(), {
                         'DICOM-102': {'status': 'offline', 'pool_members': {}}})

    def test_get_facts_items_with_poolmembers(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {
            'items': [{'name': 'DICOM-102', 'fullPath': ''}]}
        mock_get.status_code = 200
        mock_pool_stats = MagicMock(name='pool_stats')
        mock_pool_stats.json.return_value = {
            'status.availabilityState': {'description': 'offline'}}
        mock_pool_stats.status_code = 200
        mock_pool_members = MagicMock(name='pool_stats')
        mock_pool_members.json.return_value = {
            'items': [{'name': 'mem1', 'address': '192.168.180.12', 'state': 'up'}]}
        mock_pool_members.status_code = 200
        self.mock_request.get.side_effect = [
            mock_get, mock_pool_stats, mock_pool_members]
        self.assertEqual(self.f5lb_scanner.get_facts(), {
            'DICOM-102': {'status': 'offline', 'pool_members': {'mem1': {'address': '192.168.180.12', 'state': 'up'}}}})

    def test_get_facts_items_with_nopoolstats(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {
            'items': [{'name': 'DICOM-102', 'fullPath': ''}]}
        mock_get.status_code = 200
        mock_pool_stats = MagicMock(name='pool_stats')
        mock_pool_stats.json.return_value = {
            'status.avilabilityState': {'description': 'offline'}}
        mock_pool_stats.status_code = 200
        mock_pool_members = MagicMock(name='pool_stats')
        mock_pool_members.json.return_value = {
            'items': [{'name': 'mem1', 'address': '192.168.180.12', 'state': 'up'}]}
        mock_pool_members.status_code = 200
        self.mock_request.get.side_effect = [
            mock_get, mock_pool_stats, mock_pool_members]
        self.assertEqual(self.f5lb_scanner.get_facts(), {
            'DICOM-102': {'status': False, 'pool_members': {'mem1': {'address': '192.168.180.12', 'state': 'up'}}}})

    def test_get_certs_false(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {}
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_certs(), {})

    def test_get_certs_no_items(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {
            'kind': 'abc', 'selflink': 'http://abc.oom'}
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_certs(), {})

    def test_get_certs_items(self):
        mock_get = MagicMock(name='get_request')
        mock_get.json.return_value = {
            'items': [{'name': 'ca', 'expirationString': 'Dec 31 23:59:59 2029 GMT'}]}
        mock_get.status_code = 200
        self.mock_request.get.return_value = mock_get
        self.assertEqual(self.f5lb_scanner.get_certs(), {
                         'ca': {'expiration date': 'Dec 31 23:59:59 2029 GMT'}})

    def test_to_dict(self):
        self.module.safe_mongo_keys = MagicMock(name='safe_mongo_keys')
        self.f5lb_scanner.get_certs = MagicMock(name='get_certs')
        self.f5lb_scanner.get_facts = MagicMock(name='get_facts')
        exp_result = {'modules': 'F5LB',
                      'F5LB': {'Pool': self.module.safe_mongo_keys.return_value,
                               'Certificate': self.module.safe_mongo_keys.return_value
                               }
                      }
        self.assertEqual(exp_result, self.f5lb_scanner.to_dict())
        self.assertEqual(2, self.module.safe_mongo_keys.call_count)
        self.f5lb_scanner.get_certs.assert_called_once_with()
        self.f5lb_scanner.get_facts.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
