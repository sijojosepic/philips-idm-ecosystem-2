import unittest
from mock import MagicMock, patch


class TrinityTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline_host = MagicMock(name='host')
        self.mock_scanline_product = MagicMock(name='product')
        modules = {
            'logging': MagicMock(name='logging'),
            'scanline.host': self.mock_scanline_host,
            'scanline.product': self.mock_scanline_product,
            'scanline.product.legacy_nagios': self.mock_scanline_product.legacy_nagios,
            'scanline.product.isp': self.mock_scanline_product.isp,
            'scanline.product.windows': self.mock_scanline_product.windows,
            'scanline.product.vmware': self.mock_scanline_product.vmware,
            'scanline.product.ibe': self.mock_scanline_product.ibe,
            'scanline.product.analytics_publisher': self.mock_scanline_product.analytics_publisher,
            'scanline.product.iecg': self.mock_scanline_product.iecg,
            'scanline.product.iscv': self.mock_scanline_product.iscv,
            'scanline.product.xperim': self.mock_scanline_product.xperim,
            'scanline.product.hsopvm': self.mock_scanline_product.hsopvm,
            'scanline.product.database': self.mock_scanline_product.database,
            'scanline.product.hsophw': self.mock_scanline_product.hsophw,
            'scanline.product.hsop': self.mock_scanline_product.hsop,
            'scanline.product.i4': self.mock_scanline_product.i4,
            'scanline.product.disaster_recovery': self.mock_scanline_product.disaster_recovery,
            'scanline.product.disaster_recovery.dr': self.mock_scanline_product.disaster_recovery.dr,
            'scanline.product.mgnode': self.mock_scanline_product.mgnode,
            'scanline.product.nextgengd': self.mock_scanline_product.nextgengd,
            'scanline.product.extended': self.mock_scanline_product.extended,
            'scanline.product.esxi': self.mock_scanline_product.esxi,
            'scanline.product.prometheusgd': self.mock_scanline_product.prometheusgd
            
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import scanline.trinity
        self.module = scanline.trinity
        self.scanline_endpoints = self.module.scanline_endpoints
        self.scanline_scanner = self.module.scanline_scanner
        self.blob = {
            'username': 'tester',
            'password': 'CryptThis',
            'scanner': 'ISP',
            'address': '167.81.183.99',
            'tags': ['Prod', 'Primary']
        }
        self.fake_scanners = {
           'ISP': {'product': MagicMock(name='isp_product')},
           'Windows': {'host': MagicMock(name='windows_host')},
           'Magic': {'product': MagicMock(name='magic_product'), 'host': MagicMock(name='magic_host')}
        }

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('scanline.trinity.yaml.load')
    def test_scanline_endpoints(self, mock_load):
        with patch('__builtin__.open'):
            scanners = self.scanline_endpoints('manifest')
        self.assertEqual(scanners, mock_load.return_value)

    def test_scanline_scanner_in_scanners_product(self):
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), self.fake_scanners['ISP']['product'].return_value)
        self.fake_scanners['ISP']['product'].assert_called_once_with(
            address='167.81.183.99',
            host_scanner=None,
            password='CryptThis',
            tags=['Prod', 'Primary'],
            username='tester',
            scanner='ISP'
        )

    def test_scanline_scanner_in_scanners_product_and_host(self):
        self.blob['scanner'] = 'Magic'
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), self.fake_scanners['Magic']['product'].return_value)
        self.fake_scanners['Magic']['product'].assert_called_once_with(
            address='167.81.183.99',
            host_scanner=self.fake_scanners['Magic']['host'],
            password='CryptThis',
            tags=['Prod', 'Primary'],
            username='tester',
            scanner='Magic'
        )

    @patch('scanline.trinity.ProductScanner')
    def test_scanline_scanner_in_scanners_host(self, mock_product_scanner):
        self.blob['scanner'] = 'Windows'
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), mock_product_scanner.return_value)
        mock_product_scanner.assert_called_once_with(
            address='167.81.183.99',
            host_scanner=self.fake_scanners['Windows']['host'],
            password='CryptThis',
            tags=['Prod', 'Primary'],
            username='tester',
            scanner='Windows'
        )

    def test_scanline_scanner_not_in_scanners(self):
        self.blob['scanner'] = 'Unknown'
        with patch.dict('scanline.trinity.scanners', self.fake_scanners, clear=True):
            self.assertEqual(self.scanline_scanner(self.blob), None)

    def test_get_scanners(self):
        self.module.scanners = {'s1': 's2'}
        self.assertEqual({'s1': 's2'}, self.module.get_scanners())


if __name__ == '__main__':
    unittest.main()
