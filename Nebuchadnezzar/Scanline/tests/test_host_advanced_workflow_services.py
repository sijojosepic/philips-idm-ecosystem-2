import unittest
from mock import MagicMock, patch


class ScanlineHostAdvancedWorkflowServicesTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_util = MagicMock (name='scanline.utilities')
        self.mock_lxml = MagicMock(name='lxml')
        self.mock_http = MagicMock(name='scanline.utilities.http')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'lxml': self.mock_lxml,
            'requests': MagicMock(name='requests'),
            'bs4': MagicMock(name='bs4'),
            'scanline.utilities.http': self.mock_http
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.advanced_workflow_services import AdvancedWorkflowServicesHostScanner
        self.AdvancedWorkflowServicesHostScanner = AdvancedWorkflowServicesHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_properties_present(self):
        scanner = self.AdvancedWorkflowServicesHostScanner('host1', '167.81.183.99', username='user', password='secret',
                                                           tags=['x', 'y'])
        mock_scanner = MagicMock(spec=scanner)
        for prop in self.AdvancedWorkflowServicesHostScanner.module_properties:
            self.assertTrue(getattr(mock_scanner, prop))
        for prop in self.AdvancedWorkflowServicesHostScanner.general_properties:
            self.assertTrue(getattr(mock_scanner, prop))

    def test_with_response_okay(self):
        obj = MagicMock()
        obj.text.lower = MagicMock(return_value='okay')
        obj.text.encode = MagicMock(return_value='okay')
        self.mock_http.HTTPRequester().suppressed_get = MagicMock(return_value=obj)
        obj2 = MagicMock()
        obj2.iter = MagicMock(return_value='x')
        self.mock_lxml.etree.fromstring = MagicMock(return_value=obj2)
        scanner = self.AdvancedWorkflowServicesHostScanner('host1', '167.81.183.99', username='user', password='secret',
                                                           tags=['x', 'y'])
        

if __name__ == '__main__':
    unittest.main()
