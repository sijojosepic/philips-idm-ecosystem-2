import unittest
from mock import MagicMock, patch


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    if name == '__init__':
                        setattr(cls, name, lambda *args, **kwargs: None)
                    else:
                        setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class PacsConfigurationTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(
            name='cached_property', cached_property=property)
        self.mock_logging = MagicMock(name='logging')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': self.mock_logging,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product.isp import ISP4XHiSecConfiguration
        from scanline.product.disaster_recovery.pacs_config import PacsConfiguration
        PacsConfiguration.__bases__ = (Fake.imitate(ISP4XHiSecConfiguration),)
        self.module_obj = PacsConfiguration('server')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test__get_dr_config(self):
        self.module_obj.server = 'server'
        self.module_obj.auth_session = MagicMock()
        self.module_obj.query_host_config = MagicMock(
            return_value={'EnableDisasterRecovery': 'EnableDisasterRecovery'})
        self.assertEqual(self.module_obj._get_dr_config(),
                         'EnableDisasterRecovery')
        self.module_obj.auth_session.assert_called_once_with()
        self.module_obj.query_host_config.assert_called_once_with('server',
                                                                  'iSyntaxServer\\iSiteSystemCommon')

    def test__get_dr_config_exception(self):
        self.module_obj.auth_session = MagicMock(side_effect=Exception)
        self.assertRaises(RuntimeError, self.module_obj._get_dr_config)

    def test_dr_config(self):
        self.module_obj._get_dr_config = MagicMock()
        self.assertEqual(self.module_obj.dr_config,
                         self.module_obj._get_dr_config.return_value)
