import unittest
from mock import MagicMock, patch, mock_open
from requests.exceptions import RequestException


class LocalHostTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_platform = MagicMock(name='platform')
        self.mock_socket = MagicMock(name='socket')
        self.mock_netifaces = MagicMock(name='netifaces')
        self.mock_cached_property = MagicMock(name='cached_property',
                                              cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'requests': self.mock_requests,
            'requests.exceptions': self.mock_requests.exceptions,
            'platform': self.mock_platform,
            'socket': self.mock_socket,
            'netifaces': self.mock_netifaces
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.component import localhost
        self.module = localhost
        self.host_obj = localhost.LocalHost()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_transport(self):
        mock_get_transport = MagicMock(name='get_transport')
        self.host_obj._get_transport = mock_get_transport
        self.assertEqual(self.host_obj.transport, mock_get_transport.return_value)
        mock_get_transport.assert_called_once_with('/etc/cron.d/ansible_pull')

    def test_hostname(self):
        mock_get_hostname = MagicMock(name='get_hostname')
        self.host_obj._get_hostname = mock_get_hostname
        self.assertEqual(self.host_obj.hostname, mock_get_hostname.return_value)
        mock_get_hostname.assert_called_once_with()

    def test_ip_address(self):
        mock_get_ip_address = MagicMock(name='get_ip_address')
        self.host_obj._get_ip_address = mock_get_ip_address
        self.assertEqual(self.host_obj.ip_address, mock_get_ip_address.return_value)
        mock_get_ip_address.assert_called_once_with()

    def test_ssl_status(self):
        mock_get_ssl_status = MagicMock(name='get_ssl_status')
        self.host_obj._get_ssl_status = mock_get_ssl_status
        self.assertEqual(self.host_obj.ssl_status, mock_get_ssl_status.return_value)
        mock_get_ssl_status.assert_called_once_with()

    def test_distribution(self):
        mock_get_distribution = MagicMock(name='get_distribution')
        self.host_obj._get_distribution = mock_get_distribution
        self.assertEqual(self.host_obj.distribution, mock_get_distribution.return_value)
        mock_get_distribution.assert_called_once_with()

    def test_kernel(self):
        mock_get_kernel = MagicMock(name='get_kernel')
        self.host_obj._get_kernel = mock_get_kernel
        self.assertEqual(self.host_obj.kernel, mock_get_kernel.return_value)
        mock_get_kernel.assert_called_once_with()

    def test_python_version(self):
        mock_get_python_version = MagicMock(name='get_python_version')
        self.host_obj._get_python_version = mock_get_python_version
        self.assertEqual(self.host_obj.python_version, mock_get_python_version.return_value)
        mock_get_python_version.assert_called_once_with()

    def test_get_transport_http(self):
        mock_opn = mock_open(read_data='\n http:// \n\r')
        with patch('scanline.component.localhost.open', mock_opn, create=True):
            transport = self.host_obj._get_transport('test.txt')
            mock_opn.assert_called_once_with('test.txt')
            self.assertEqual(transport, 'HTTP')

    def test_get_transport_https(self):
        mock_opn = mock_open(read_data='\n https:// \n\r')
        with patch('scanline.component.localhost.open', mock_opn, create=True):
            transport = self.host_obj._get_transport('test.txt')
            mock_opn.assert_called_once_with('test.txt')
            self.assertEqual(transport, 'HTTPS')

    def test_get_ssl_status(self):
        expected_ssl_status = {
            "is_ssl_port_open": True,
            "operation": 'https://vigilant/health',
            "message": self.mock_requests.get.return_value.reason
        }
        ssl_status = self.host_obj._get_ssl_status()
        self.assertEqual(ssl_status, expected_ssl_status)
        self.mock_requests.get.assert_called_once_with('https://vigilant/health', verify=False, timeout=5)

    def test_get_ssl_status_no_reason_attr(self):
        del self.mock_requests.get.return_value.reason
        expected_ssl_status = {
            "is_ssl_port_open": True,
            "operation": 'https://vigilant/health',
            "message": ''
        }
        ssl_status = self.host_obj._get_ssl_status()
        self.assertEqual(ssl_status, expected_ssl_status)
        self.mock_requests.get.assert_called_once_with('https://vigilant/health', verify=False, timeout=5)

    def test_get_ssl_status_exception(self):
        expected_ssl_status = {
            "is_ssl_port_open": False,
            "operation": 'https://vigilant/health',
            "message": 'invalid request'
        }
        self.module.RequestException = RequestException
        self.mock_requests.get.side_effect = RequestException('invalid request')
        ssl_status = self.host_obj._get_ssl_status()
        self.assertEqual(ssl_status, expected_ssl_status)

    def test_get_distribution(self):
        self.mock_platform.linux_distribution.return_value = 'CentOS', '6.9', 'Final'
        self.assertEqual(self.host_obj._get_distribution(), 'CentOS-6.9-Final')

    def test_get_kernel(self):
        self.assertEqual(self.host_obj._get_kernel(),
                         self.mock_platform.release.return_value)

    def test_get_hostname(self):
        self.assertEqual(self.host_obj._get_hostname(),
                         self.mock_socket.gethostname.return_value)

    def test_get_python_version(self):
        self.assertEqual(self.host_obj._get_python_version(),
                         self.mock_platform.python_version.return_value)

    def test_get_attributes(self):
        self.host_obj.lhost_attributes = ['attr1']
        self.host_obj.attr1 = 'atr1'
        self.assertEqual(self.host_obj.get_attributes(),
                         {'attr1': 'atr1'})


if __name__ == '__main__':
    unittest.main()
