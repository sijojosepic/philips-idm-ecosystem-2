import unittest
from mock import MagicMock, patch


class ScanlineHostHSOPTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.hsop import HSOPHostScanner
        self.HSOPHostScanner = HSOPHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        scanner = self.HSOPHostScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.assertEqual(self.HSOPHostScanner.module_name, 'HSOP')

if __name__ == '__main__':
    unittest.main()
