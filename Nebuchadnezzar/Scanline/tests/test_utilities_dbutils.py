import sys
import unittest
import mock
from mock import MagicMock, patch


class CheckSqlDBUtilsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_redis = MagicMock(name='redis')
        self.mock_json = MagicMock(name='json')
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            'radis': self.mock_redis,
            'json': self.mock_json,
            'linecache': self.mock_linecache,
            'pymssql': self.mock_pymssql,
            'scanline.utilities.http.HTTPRequester': self.mock_scanline.utilities.http.HTTPRequester,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import dbutils
        self.module = dbutils

    def test_get_siteid(self):
        self.mock_linecache.getline.return_value = "test1"
        self.assertEqual(self.module.get_siteid(), "test1")

    def test_get_dbpartner_in_dbscanner(self):
        self.module.get_backoffice_data = MagicMock(return_value={
            u'result': [{u'hostname': u'192.168.180.80', u'dbpartner': u'192.168.180.26'},
                        {u'hostname': u'192.168.180.26', u'dbpartner': u'192.168.180.80'}]})
        self.assertEqual(self.module.get_dbpartner_in_dbscanner('192.168.180.80', 'http://vigilant/'),
                         ['192.168.180.80', '192.168.180.26'])

    def test_get_dbpartner_in_dbscanner_exception(self):
        self.module.get_backoffice_data = MagicMock(
            return_value={u'result': [{u'hostname': u'192.168.180.80'}, {u'hostname': u'192.168.180.26'}]})
        self.assertEqual(self.module.get_dbpartner_in_dbscanner('192.168.180.80', 'https://vigilant/'), [])

    def test_get_dbnodes(self):
        self.module.get_backoffice_data = MagicMock(return_value={u'result': [{u'ISP': {
            u'endpoint': {u'scanner': u'ISP', u'address': u'IDM04IF1.IDM04.isyntax.net'}, u'tags': u'production',
            u'dbclustercheck': False, u'noncore_node': u'DB', u'flags': [u'hisec'],
            u'input_folder': u'S:\\Philips\\Common\\Databases', u'module_type': u'Database', u'identifier': u'4x'},
            u'hostname': u'idm04db1.idm04.isyntax.net'},
            {u'ISP': {
                u'endpoint': {u'scanner': u'ISP',
                              u'address': u'IDM04IF1.IDM04.isyntax.net'},
                u'tags': u'production',
                u'dbclustercheck': False,
                u'noncore_node': u'DB',
                u'flags': [u'hisec'],
                u'input_folder': u'S:\\Philips\\Common\\Databases',
                u'module_type': u'Database',
                u'identifier': u'4x'},
                u'hostname': u'idm04db2.idm04.isyntax.net'}]})
        self.assertEqual(self.module.get_dbnodes('http://vigilant/'),
                         [[u'idm04db1.idm04.isyntax.net', u'idm04db2.idm04.isyntax.net']])

    def test_get_cluster_primary_db(self):
        self.module.get_dbnodes = MagicMock(
            return_value=[[u'idm04db1.idm04.isyntax.net', u'idm04db2.idm04.isyntax.net']])
        self.module.get_cluster_primary_db('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'http://vigilant/')
        self.assertEqual(self.mock_pymssql.connect.call_count, 1)

    def test_get_cluster_primary_db_exception(self):
        self.module.get_dbnodes = MagicMock(
            return_value=[[u'idm04db1.idm04.isyntax.net', u'idm04db2.idm04.isyntax.net']])
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_cluster_primary_db('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb',
                                               'http://vigilant/'), False)

    def test_get_backoffice_data(self):
        mock_site_id = MagicMock(name="site_id")
        mock_site_id.return_value = 'test1'
        self.module.get_siteid = mock_site_id
        mock_content = MagicMock(name='content')
        mock_content.content.return_value = {'a': 'b'}
        mock_http_obj = MagicMock(name='http_obj')
        mock_http_obj.suppressed_get.return_value = mock_content
        self.module.HTTPRequester = MagicMock(name='httprequester')
        self.module.HTTPRequester.return_value = mock_http_obj
        self.module.json.loads = MagicMock(name='json_load')
        self.module.json.loads.return_value = {'a': 'b'}
        response = self.module.get_backoffice_data('https://vigilant/', fact_type='host',
                                                   MODULE_URL='pma/facts/{site_id}/modules/ISP/keys/module_type')
        self.assertEqual(response, {'a': 'b'})

    def test_get_mirroring_primary_check(self):
        self.module.get_mirroring_primary_check('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb')
        self.assertEqual(self.mock_pymssql.connect.call_count, 1)

    def test_get_mirroring_primary_check_exception(self):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_mirroring_primary_check('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb'),
            "Connection Error")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
