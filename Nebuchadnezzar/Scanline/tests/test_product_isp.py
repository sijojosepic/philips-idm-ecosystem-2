import unittest
from mock import MagicMock, patch, PropertyMock, DEFAULT


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""
    extra_params = {'address': '10.1.0.1'}

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    if name == '__init__':
                        setattr(cls, name, lambda *args, **kwargs: None)
                    else:
                        setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
            for k, v in Fake.extra_params.iteritems():
                setattr(cls, k, v)
        return cls


class ScanlineISPTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_cached_property = MagicMock(
                name='cached_property', cached_property=property)
            self.mock_requests = MagicMock(name='requests')
            self.mock_datetime = MagicMock(name='datetime')
            self.mock_dateutil = MagicMock(name='dateutil')
            self.mock_lxml = MagicMock(name='lxml')
            self.mock_pytz = MagicMock(name='pytz')
            self.mock_linecache = MagicMock(name='linecache')
            self.mock_host = MagicMock()
            self.mock_phimutils = MagicMock()
            self.mock_utilities = MagicMock()
            modules = {
                'cached_property': self.mock_cached_property,
                'logging': MagicMock(name='logging'),
                'linecache': self.mock_linecache,
                'phimutils': self.mock_phimutils,
                'phimutils.resource': self.mock_phimutils.resource,
                'requests': self.mock_requests,
                'requests.exceptions': self.mock_requests.exceptions,
                'lxml': self.mock_lxml,
                'datetime': self.mock_datetime,
                'dateutil': self.mock_dateutil,
                'pytz': self.mock_pytz,
                'scanline.host.isp': self.mock_host.isp,
                'scanline.host.windows': self.mock_host.windows,
                'scanline.utilities.dns': self.mock_utilities.dns,
                'scanline.utilities.config_reader': self.mock_utilities.config_reader,
                'scanline.utilities.auth': self.mock_utilities.auth,
                'scanline.utilities.win_rm': self.mock_utilities.win_rm,
                'scanline.host.udm': self.mock_host.udm,
                'scanline.component': self.mock_host.component,
                'scanline.component.localhost': self.mock_host.component.localhost,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class ISPProductScannerTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.module = scanline.product.isp
        self.ISPAuthenticationError = scanline.product.isp_config.ISPAuthenticationError
        self.ISPProductScanner = scanline.product.isp.ISPProductScanner
        mock_ispdbconfig = MagicMock(name='ISPDBConfiguration')
        self.ISPDBConfiguration = mock_ispdbconfig
        self.module.ISPGDHandler = MagicMock(name='ISPGDHandler')
        from scanline.product import ProductScanner
        self.ISPProductScanner.__bases__ = (Fake.imitate(ProductScanner),)
        self.isp = self.ISPProductScanner('scanner', '10.55.66.1', 'u1', 'p1')

    def test_get_dbhosts(self):
        self.module.iter = MagicMock()
        self.isp.ispdb = MagicMock()
        db_hosts = self.isp.get_dbhosts()
        self.assertEqual(db_hosts, self.module.iter.return_value)
        self.module.iter.assert_called_once_with(self.isp.ispdb.non_gd_hosts.return_value)

    def test_get_dbhosts_no_ispdb(self):
        self.isp.ispdb = None
        self.module.iter = MagicMock()
        self.assertEqual(self.isp.get_dbhosts(), self.module.iter.return_value)
        self.module.iter.assert_called_once_with([])

    def test_do_generic_discovery(self):
        resp = self.module.ISPGDHandler.return_value.discover_all.return_value
        self.isp.endpoint = {'scanner': 's1'}
        self.isp.tags = 't1'
        self.isp.kwargs = {'a': 'b'}
        self.assertEqual(resp, self.isp.do_generic_discovery('h_n_t'))
        self.module.ISPGDHandler.assert_called_once_with('h_n_t', 's1',
                                                         '10.1.0.1',
                                                         't1', a='b')

    def test_get_hosts(self):
        self.module.iter = MagicMock()
        self.isp.create_isp = MagicMock()
        hosts = self.isp.get_hosts()
        self.assertEqual(hosts, self.module.iter.return_value)
        host_value = self.isp.create_isp.return_value.get_hosts.return_value
        self.module.iter.assert_called_once_with(host_value)

    def test_get_hosts_no_isp(self):
        self.isp.create_isp = MagicMock(**{'return_value': None})
        self.module.iter = MagicMock()
        self.assertEqual(self.isp.get_hosts(), self.module.iter.return_value)
        self.module.iter.assert_called_once_with([])

    def test_cluster_win_rm_request_value(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.std_err = ''
        mock_WinRM.execute_ps_script.return_value.std_out = 'cluster_name'
        self.mock_utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.isp.cluster_win_rm_request(['node1', 'node2']), 'cluster_name')

    def test_cluster_win_rm_request_novalue(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.std_err = ''
        mock_WinRM.execute_ps_script.return_value.std_out = ''
        self.mock_utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.isp.cluster_win_rm_request(['node1', 'node2']), None)

    def test_add_infra_high_availability_with_data(self):
        self.isp.compose_cluster_facts = MagicMock(return_value={'result': 'json_data'})
        self.isp.create_isp = MagicMock(name='isp')
        self.isp.address = MagicMock(return_value='master.domain.isyntax.net')
        self.mock_utilities.dns.get_address = MagicMock(name='get_address')
        passing_result = {'node1': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}},
                          'node2': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}}}
        expected_result = {'node1': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}},
                           'node2': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}},
                           'result': 'json_data'}
        self.assertEqual(self.isp.add_infra_high_availability(passing_result), expected_result)

    def test_add_infra_high_availability_with_no_data(self):
        self.isp.compose_cluster_facts = MagicMock(return_value=None)
        self.isp.create_isp = MagicMock(name='isp')
        passing_result = {'node': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}}}
        expected_result = {'node': {
            'ISP': {'infra_high_availability_check': False, 'existing_result': 'existing_json_data',
                    'module_type': 128}}}
        self.assertEqual(self.isp.add_infra_high_availability(passing_result), expected_result)

    def test_add_infra_high_availability_with_exception(self):
        self.isp.add_cluster = MagicMock(name='add_cluster')
        self.isp.create_isp = MagicMock(name='isp')
        self.isp.add_cluster.side_effect = Exception
        self.assertRaises(Exception, self.isp.add_infra_high_availability(
            {'node1': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}},
             'node2': {'ISP': {'existing_result': 'existing_json_data', 'module_type': 128}}}))

    def test_check_cluster_dbcluster(self):
        self.isp.endpoint = 'endpoint'
        self.isp.tags = 'tags'
        # self.isp.get_clusterdb_credentials = MagicMock(return_value='credentials')
        self.isp.compose_cluster_facts = MagicMock(return_value={'c1': 'c2'})
        self.isp.cluster_win_rm_request = MagicMock(return_value='dbcluster')
        self.assertEqual(self.isp.check_cluster(
            ['db1', 'db2'], 'dbcluster'), {'c1': 'c2'})
        self.mock_utilities.dns.get_address.assert_called_once_with('dbcluster.1.0.1')
        self.isp.cluster_win_rm_request.assert_called_once_with(['db1', 'db2'])
        self.isp.compose_cluster_facts.assert_called_once_with('dbcluster',
                                                               'dbcluster.1.0.1',
                                                               self.mock_utilities.dns.get_address.return_value,
                                                               'dbcluster')
    def test_check_cluster_hl7cluster(self):
        self.isp.endpoint = 'endpoint'
        self.isp.tags = 'tags'
        self.isp.cluster_win_rm_request = MagicMock(return_value='hl7cluster')
        self.isp.compose_cluster_facts = MagicMock(return_value={'c1': 'c2'})
        self.assertEqual(self.isp.check_cluster(['hl7', 'hl8'], 'hl7cluster'),
                         {'c1': 'c2'})
        self.isp.cluster_win_rm_request.assert_called_once_with(['hl7', 'hl8'])
        self.isp.compose_cluster_facts.assert_called_once_with('hl7cluster',
                                                               'hl7cluster.1.0.1',
                                                               self.mock_utilities.dns.get_address.return_value,
                                                               'hl7cluster')

    def test_check_cluster_hl7cluster_empty(self):
        self.isp.endpoint = 'endpoint'
        self.isp.tags = 'tags'
        self.isp.get_clusterdb_credentials = MagicMock(
            return_value='credentials')
        self.isp.cluster_win_rm_request = MagicMock(return_value='')
        self.assertEqual(self.isp.check_cluster(
            ['hl7', 'hl8'], 'hl7cluster'), None)
        self.isp.cluster_win_rm_request.assert_called_once_with(['hl7', 'hl8'])

    def test_check_cluster_dbcluster_empty(self):
        self.isp.endpoint = 'endpoint'
        self.isp.tags = 'tags'
        self.isp.get_clusterdb_credentials = MagicMock(return_value='credentials')
        self.isp.cluster_win_rm_request = MagicMock(return_value='')
        self.assertEqual(self.isp.check_cluster(['db1', 'db2'], 'dbcluster'), None)
        self.isp.cluster_win_rm_request.assert_called_once_with(['db1', 'db2'])

    def test_add_cluster_with_single_node(self):
        self.isp.endpoint = 'endpoint'
        self.isp.tags = 'tags'
        self.isp.get_clusterdb_credentials = MagicMock(return_value='credentials')
        self.assertEqual(self.isp.add_cluster(['if1'], 1, 'infra-high-availability'), None)

    def test_add_cluster_with_no_node(self):
        self.isp.endpoint = 'endpoint'
        self.isp.tags = 'tags'
        self.isp.get_clusterdb_credentials = MagicMock(return_value='credentials')
        self.assertEqual(self.isp.add_cluster([], 0, 'infra-high-availability'), None)

    def test_segregate_credentials(self):
        facts = {'h1': {'f1': 'v1', 'credentials': {'username': 'u1', 'password': 'p1'}}}
        exp_facts = {'h1': {'f1': 'v1'}, 'credentials': {'h1': {'username': 'u1', 'password': 'p1'}}}
        self.assertEqual(self.isp.segregate_credentials(facts), exp_facts)

    def test_segregate_credentials_no_facts(self):
        self.assertEqual(self.isp.segregate_credentials({}), {})

    def test_segregate_credentials_no_credentials(self):
        facts = {'h1': {'f1': 'v1'}}
        self.assertEqual(self.isp.segregate_credentials(facts), facts)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


class ScanlineISP4xConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISP4XConfiguration
        import scanline.product.isp as spi
        self.module = spi
        self.ISP4XConfiguration = ISP4XConfiguration
        self.mock_phimutils.resource.NagiosResourcer.return_value.get_resources.return_value = 'u1', 'p1'
        self.ISP4XConfiguration.__bases__ = (Fake.imitate(spi.ISPConfiguration),)
        self.isp4x = self.ISP4XConfiguration('10.4.4.6')
        self.isp4x.server = 'server'

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_load_credentials(self):
        self.assertEqual(self.isp4x.CREDENTIALS, {'username': 'u1',
                         'password': 'p1'})

    def test_auth_path(self):
        self.assertEqual(
            self.isp4x.auth_path,
            'InfrastructureServices/AuthenticationService/v1_0/authenticationservice.ashx'
        )

    def test_config_path(self):
        self.assertEqual(
            self.isp4x.config_path,
            'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
        )

    def test_identifier_4x(self):
        self.assertEqual(
            self.isp4x.identifier,
            '4x'
        )

    def test_get_authentication_query(self):
        with patch('scanline.product.isp.ISP4XConfiguration.auth_username', new_callable=PropertyMock) as user_name:
            with patch('scanline.product.isp.ISP4XConfiguration.auth_password', new_callable=PropertyMock) as password:
                user_name.return_value = "username"
                password.return_value = "password"
                self.isp4x.E = MagicMock(name='E')
                self.assertEqual(
                    self.isp4x.get_authentication_query(),
                    self.mock_lxml.etree.tostring.return_value
                )

    def test_TICKET_XPATH_valid(self):
        self.assertEqual(self.isp4x.TICKET_XPATH, '//*[ local-name() = "Ticket" ]')

    def test_get_auth_token_valid(self):
        self.isp4x.query_xml = MagicMock(name='query_xml')
        self.isp4x.get_authentication_query = MagicMock(name='get_authentication_query', return_value='q')
        self.assertEqual(self.isp4x.get_auth_token(),
                         self.isp4x.query_xml.return_value.xpath.return_value[0])
        self.isp4x.query_xml.assert_called_once_with(self.isp4x.auth_url, 'q')
        self.isp4x.get_authentication_query.assert_called_once_with()

    def test_get_auth_no_root(self):
        self.isp4x.query_xml = MagicMock(name='query_xml', return_value=None)
        self.isp4x.get_authentication_query = MagicMock(name='get_authentication_query', return_value='q')
        self.assertEqual(self.isp4x.get_auth_token(), None)
        self.isp4x.query_xml.assert_called_once_with(self.isp4x.auth_url, 'q')
        self.isp4x.get_authentication_query.assert_called_once_with()

    def test_get_auth_token_invalid(self):
        self.isp4x.query_xml = MagicMock(name='query_xml')
        self.isp4x.get_authentication_query = MagicMock(name='get_authentication_query', return_value='q')
        self.isp4x.query_xml.return_value.xpath.return_value = []
        self.assertEqual(self.isp4x.get_auth_token(), None)

    def test_logout(self):
        self.isp4x.query_xml = MagicMock(name='query_xml', return_value='root')
        self.isp4x.logout_query = MagicMock(name='logout_query')
        self.assertEqual(self.isp4x.logout(), None)
        self.isp4x.query_xml.assert_called_once_with(self.isp4x.auth_url, self.isp4x.logout_query.return_value)

    def test_logout_query(self):
        self.isp4x.E = MagicMock(name='E')
        host_name = self.mock_host.component.localhost.LocalHost.return_value.hostname
        ip_address = self.mock_host.component.localhost.LocalHost.return_value.ip_address
        self.assertEqual(self.isp4x.logout_query(),
                         self.mock_lxml.etree.tostring.return_value)
        self.isp4x.E.HostName.assert_called_once_with(host_name)
        self.isp4x.E.IpAddress.assert_called_once_with(ip_address)
        self.isp4x.E.Logout.assert_called_once_with(self.isp4x.E.HostName.return_value,
                                                    self.isp4x.E.IpAddress.return_value)
        self.isp4x.E.Message.assert_called_once_with(self.isp4x.E.Logout.return_value)


class ScanlineISP4xHiSecConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.module = scanline.product.isp
        self.ISP4XHiSecConfiguration = scanline.product.isp.ISP4XHiSecConfiguration
        self.ISP4XHiSecConfiguration.__bases__ = (Fake.imitate(scanline.product.isp.ISPConfiguration),)
        self.isp4xHS = self.ISP4XHiSecConfiguration('10.4.4.6')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_paths(self):
        self.assertEqual(
            self.isp4xHS.config_path,
            'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
        )
        self.assertEqual(
            self.isp4xHS.auth_path,
            ''
        )
        self.assertEqual(
            self.isp4xHS.config_scheme,
            'https'
        )
        self.assertEqual(
            self.isp4xHS.intrinsic_flags,
            ['hisec']
        )

    def test_auth_header_syntax(self):
        self.assertEqual(
            self.isp4xHS.auth_header_syntax,
            'HMAC {TIMESTAMP_LABEL}={ISODATE};{HASH_LABEL}={HASH};{APPID_LABEL}={APPID}'
        )

    def test_HMAC_IDENTIFIERS(self):
        HMAC_IDENTIFIERS = {
        'TOKEN': 'iSiteWebApplication',  # Not required
        'TIMESTAMP_LABEL': 'Timestamp',
        'HASH_LABEL': 'Hash',
        'APPLICATIONIDENTIFIER': 'iSiteRadiology',  # Not Required
        'APPID_LABEL': 'AppId',
        'APPID': 'IDM',
        'SECRET_KEY': self.mock_phimutils.resource.NagiosResourcer.return_value.get_resource.return_value,
        'SERVERTIMESTAMP_LABEL': 'ServerTimeStamp',
        'RETRIES': 3,  # Possibly part of the protocol
        }
        self.assertEqual(
            self.isp4xHS.HMAC_IDENTIFIERS,
            HMAC_IDENTIFIERS
            )

    def test_auth_timestamp_none(self):
        self.assertEqual(self.isp4xHS.auth_timestamp,
                         self.module.datetime.now.return_value)

    def test_auth_timestamp_set(self):
        self.isp4xHS._auth_timestamp = 5678
        self.assertEqual(self.isp4xHS.auth_timestamp, 5678)

    def test_set_auth_timestamp_none(self):
        self.isp4xHS.set_auth_timestamp(None, None)
        self.assertEqual(self.isp4xHS._auth_timestamp, None)

    def test_set_auth_timestamp(self):
        self.isp4xHS.set_auth_timestamp('2016-09-15T02:45:58.317491', 9)
        self.assertEqual(
            self.isp4xHS._auth_timestamp,
            self.module.parser.parse.return_value.__add__.return_value
        )
        self.module.parser.parse.assert_called_once_with(
            '2016-09-15T02:45:58.317491')
        self.module.timedelta.assert_called_once_with(seconds=4)
        self.module.parser.parse.return_value.__add__.assert_called_once_with(
            self.module.timedelta.return_value
        )

    def test_get_auth_header(self):
        self.assertEqual(self.isp4xHS.get_auth_header(
            'chuck'), {'Authorization': 'chuck'})

    # def test_get_formatted_token(self):
    #     self.isp4xHS._auth_timestamp = MagicMock()
    #     self.isp4xHS._auth_timestamp.isoformat.return_value = '2016-09-15T02:45:58.317491'
    #     self.module.get_hashkey = MagicMock(
    #         name='get_hashkey', return_value='abc123')
    #     self.assertEqual(
    #         self.isp4xHS.get_formatted_token(),
    #         'HMAC Timestamp=2016-09-15T02:45:58.317491;Hash=abc123;AppId=IDM'
    #     )
    #     self.module.get_hashkey.assert_called_once()

    def test_get_auth_token_ok_on_first_try(self):
        self.isp4xHS.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.assertEqual(self.isp4xHS.get_auth_token(),
                         self.isp4xHS.attempt_authorization.return_value)

    def test_get_auth_token_ok_on_second_try(self):
        self.isp4xHS.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.isp4xHS.attempt_authorization.side_effect = [None, 'Token-1']
        self.assertEqual(self.isp4xHS.get_auth_token(), 'Token-1')

    def test_get_auth_token_exception(self):
        self.isp4xHS.attempt_authorization = MagicMock(
            name='attempt_authorization')
        self.isp4xHS.attempt_authorization.return_value = None
        self.assertRaises(self.module.ISPAuthenticationError, self.isp4xHS.get_auth_token)
        self.assertEqual(self.isp4xHS.attempt_authorization.call_count, 3)

    def test_attempt_authorization_ok_response(self):
        self.isp4xHS.is_ok_response = MagicMock(
            name='is_ok_response', return_value=True)
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        response = MagicMock()
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(response, 5))
        self.assertEqual(self.isp4xHS.attempt_authorization(),
                         self.isp4xHS.get_formatted_token.return_value)
        self.isp4xHS.get_formatted_token.assert_called_once_with()
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)

    def test_attempt_authorization_no_response(self):
        self.isp4xHS.is_ok_response = MagicMock(
            name='is_ok_response', return_value=False)
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        mock_set_auth_timestamp = MagicMock(name='set_auth_timestamp')
        self.isp4xHS.set_auth_timestamp = mock_set_auth_timestamp
        response = MagicMock()
        response.headers = {'ServerTimeStamp': 'servertimestamp'}
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(response, 5))
        self.assertEqual(self.isp4xHS.attempt_authorization(), None)
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)
        mock_set_auth_timestamp.assert_called_once_with('servertimestamp', 5)

    def test_attempt_authorization_no_response_headers(self):
        self.isp4xHS.is_ok_response = MagicMock(
            name='is_ok_response', return_value=False)
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        mock_set_auth_timestamp = MagicMock(name='set_auth_timestamp')
        self.isp4xHS.set_auth_timestamp = mock_set_auth_timestamp
        response = MagicMock()
        del response.headers
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(response, 5))
        self.assertEqual(self.isp4xHS.attempt_authorization(), None)
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)

    def test_attempt_authorization_maybe_next_time(self):
        self.isp4xHS.is_ok_response = MagicMock(
            name='is_ok_response', return_value=False)
        self.isp4xHS.get_formatted_token = MagicMock(
            name='get_formatted_token')
        self.isp4xHS.set_auth_timestamp = MagicMock(name='set_auth_timestamp')
        response = MagicMock()
        self.isp4xHS.post_auth_request = MagicMock(
            name='post_auth_request', return_value=(response, 5))
        self.assertEqual(self.isp4xHS.attempt_authorization(), None)
        self.isp4xHS.get_formatted_token.assert_called_once_with()
        self.isp4xHS.post_auth_request.assert_called_once_with(
            self.isp4xHS.get_formatted_token.return_value)
        self.isp4xHS.set_auth_timestamp.assert_called_once_with(
            response.headers.get.return_value, 5)
        response.headers.get.assert_called_once_with('ServerTimeStamp')

    def test_post_auth_request(self):
        time2 = MagicMock()
        time2.__sub__.return_value.seconds = 2
        self.module.datetime.utcnow.side_effect = [MagicMock(), time2]
        self.isp4xHS.suppressed_post = MagicMock(name='suppressed_post')
        self.isp4xHS.get_server_info_query = MagicMock(name='get_server_info_query')
        self.isp4xHS.get_auth_header = MagicMock(name='get_auth_header')
        self.isp4xHS.config_url = 'config_url'
        self.assertEqual(self.isp4xHS.post_auth_request(
            'token'), (self.isp4xHS.suppressed_post.return_value, 2))
        self.isp4xHS.get_server_info_query.assert_called_once_with()
        self.isp4xHS.get_auth_header.assert_called_once_with('token')
        self.isp4xHS.suppressed_post.assert_called_once_with(
            self.isp4xHS.config_url, self.isp4xHS.get_server_info_query.return_value,
            self.isp4xHS.get_auth_header.return_value, ok_only=False
        )

    def test_query_xml_ok_resp(self):
        self.isp4xHS.suppressed_post = MagicMock()
        self.isp4xHS.suppressed_post.return_value.status_code = 200
        self.isp4xHS.suppressed_post.return_value.content = 'content'
        self.assertEqual(self.isp4xHS.query_xml('url', 'query'),
            self.mock_lxml.objectify.fromstring.return_value)
        self.isp4xHS.suppressed_post.assert_called_once_with(url='url', data='query', ok_only=False)

    def test_query_xml_exception(self):
        self.isp4xHS.suppressed_post = MagicMock()
        self.isp4xHS.suppressed_post.return_value = None
        self.assertRaises(RuntimeError, self.isp4xHS.query_xml, 'url', 'query')
        self.isp4xHS.suppressed_post.assert_called_once_with(url='url', data='query', ok_only=False)


    def test_query_xml_unauthorized_401_status(self):
        mock1 = MagicMock()
        mock1.status_code = 401
        mock2 = MagicMock()
        mock2.status_code = 200
        mock2.content = 'content'
        self.isp4xHS.suppressed_post = MagicMock()
        self.isp4xHS.suppressed_post.side_effect = [mock1, mock2]
        self.assertEqual(self.isp4xHS.query_xml('url', 'query'),
            self.mock_lxml.objectify.fromstring.return_value)
        self.assertEqual(self.isp4xHS.suppressed_post.call_count, 2)


    def test_query_xml_unauthorized_401_status_raises_exception(self):
        self.isp4xHS.suppressed_post = MagicMock()
        self.isp4xHS.suppressed_post.return_value.status_code = 401
        self.assertRaises(RuntimeError, self.isp4xHS.query_xml, 'url', 'query')
        self.assertEqual(self.isp4xHS.suppressed_post.call_count, 3)

class ScanlineISP3xConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.ISP3XConfiguration = scanline.product.isp.ISP3XConfiguration
        self.ISP3XConfiguration.__bases__ = (Fake.imitate(scanline.product.isp.ISPConfiguration),)
        self.isp3x = self.ISP3XConfiguration('10.3.6.1')
        self.isp3x.server = '10.3.6.1'

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_auth_path(self):
        self.assertEqual(self.isp3x.auth_path,
                         'iSiteWeb/Authentication/Authentication.ashx')

    def test_config_path(self):
        self.assertEqual(
            self.isp3x.config_path, 'iSiteWeb/Configuration/ConfigurationService.ashx')

    def test_get_authentication_query(self):
        self.isp3x.E = MagicMock()
        self.assertEqual(
            self.isp3x.get_authentication_query(),
            self.mock_lxml.etree.tostring.return_value
        )

    def test_calls_on_get_auth_token(self):
        self.isp3x.suppressed_post = MagicMock(name='suppressed_post')
        self.isp3x.get_authentication_query = MagicMock(
            name='get_authentication_query')
        self.isp3x.auth_url = 'auth_url'
        self.assertEqual(self.isp3x.get_auth_token(),
            self.isp3x.suppressed_post.return_value.cookies.get('iSiteWebApplication')
        )
        self.isp3x.suppressed_post.assert_called_once_with(
            url='auth_url',
            data=self.isp3x.get_authentication_query.return_value
        )

    def test_get_auth_token_invalid(self):
        self.isp3x.suppressed_post = MagicMock(
            name='suppressed_post', **{'return_value.cookies': {}})
        self.isp3x.get_authentication_query = MagicMock(
            name='get_authentication_query')
        self.assertEqual(self.isp3x.get_auth_token(), None)


class ScanlineISP4_1ConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.ISP4_1Configuration = scanline.product.isp.ISP4_1Configuration
        self.ISP4_1Configuration.__bases__ = (Fake.imitate(scanline.product.isp.ISP4XConfiguration),)
        self.isp4_1 = self.ISP4_1Configuration('10.4.1.1')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_verify_attributes(self):
        auth_path = '%2fiSiteWeb%2fAuthentication%2fv1_0%2fauthenticationservice.ashx'
        config_path = '%2fiSiteWeb%2fConfiguration%2fConfigurationService.ashx'
        identifier = '4_1'
        self.assertEqual(auth_path, self.isp4_1.auth_path)
        self.assertEqual(config_path, self.isp4_1.config_path)
        self.assertEqual(identifier, self.isp4_1.identifier)


class ScanlineISPDBConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp
        self.module = scanline.product.isp
        self.ISPAuthenticationError = scanline.product.isp_config.ISPAuthenticationError
        self.ISPDBConfiguration = scanline.product.isp.ISPDBConfiguration
        self.ISPDBConfiguration.__bases__ = (Fake.imitate(scanline.product.isp.ISP4XHiSecConfiguration),)
        self.ispdb = self.ISPDBConfiguration('10.4.4.6')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_config_path(self):
        self.assertEqual(
            self.ispdb.config_path,
            'InfrastructureServices/RegistryService/RegistryService.ashx'
        )

    def test_get_server_info_query(self):
        self.ispdb.E = MagicMock()
        self.assertEqual(self.ispdb.get_server_info_query(), self.mock_lxml.etree.tostring.return_value)
        self.mock_lxml.objectify.ElementMaker.assert_called_once_with(annotate=False)

    def test_get_hosts(self):
        self.ispdb.config_url = 'config_url'
        self.ispdb.auth_session = MagicMock()
        self.ispdb.query_xml = MagicMock()
        self.ispdb.get_server_info_query = MagicMock()
        self.assertEqual(self.ispdb.get_hosts(),
                         self.ispdb.query_xml.return_value.xpath.return_value)
        self.assertEqual(self.ispdb.auth_session.call_count, 2)
        self.assertEqual(self.ispdb.get_server_info_query.call_count, 2)
        self.assertEqual(self.ispdb.query_xml.call_count, 2)

    def test_identifier(self):
        result = self.ispdb.identifier
        self.assertEqual(result, None)

    def software_version(self):
        result = self.ispdb.software_version
        self.assertEqual(result, None)

    def test_get_noncore_node(self):
        result = self.ispdb.get_noncore_node()
        self.assertEqual(result, 'DB')

    def test_EXTENDEDNODES(self):
        EXTENDEDNODES = ('Rabbitmqnode', 'Database', 'Hl7', 'Rviewer',
                         'Eviewer', 'I4Processing', 'Mgnode')
        self.assertEqual(EXTENDEDNODES, self.ispdb.EXTENDEDNODES)

    def test_ext_config(self):
        self.ispdb.extended_hosts_config = MagicMock(return_value='r1')
        self.assertEqual('r1', self.ispdb.ext_config)

    def test_extended_hosts_config(self):
        self.ispdb.get_server_info_query = MagicMock(name='get_server_info_query',
                                                     return_value='q1')
        self.ispdb.auth_session = MagicMock()
        self.ispdb.query_xml = MagicMock(return_value='xml')
        self.ispdb.config_url = 'c1'
        self.assertEqual('xml', self.ispdb.extended_hosts_config())
        self.ispdb.get_server_info_query.assert_called_once_with()
        self.ispdb.auth_session.assert_called_once_with()
        self.ispdb.query_xml.assert_called_once_with('c1', 'q1')

    def test_extended_hosts_config_ISPAuthenticationError(self):
        from scanline.product.isp_config import ISPAuthenticationError
        self.ispdb.auth_session = MagicMock(side_effect=ISPAuthenticationError)
        self.assertEqual(None, self.ispdb.extended_hosts_config())

    def test_hosts_with_type(self):
        host_n_type = [('n1', 'M1'), ('n2', 'M2')]
        self.ispdb.get_hosts_with_type = MagicMock(return_value=host_n_type)
        self.assertEqual(host_n_type, self.ispdb.hosts_with_type)

    def test_get_hosts_with_type(self):
        self.ispdb.extended_hosts_config = MagicMock()
        nodes_mock = MagicMock()
        self.ispdb.extended_hosts_config.return_value.ListNonCoreNodesResponse.ArrayOfNode = nodes_mock
        children = MagicMock(HostName='h1')
        children.NodeType.text.title.return_value = 'T1'
        nodes_mock.iterchildren.return_value = [children]
        self.assertEqual([('h1', 'T1')], self.ispdb.get_hosts_with_type())

    def test_get_host_config(self):
        self.ispdb.extended_hosts_config = MagicMock()
        nodes_mock = MagicMock()
        self.ispdb.extended_hosts_config.return_value.ListNonCoreNodesResponse.ArrayOfNode = nodes_mock
        children = MagicMock(HostName='h1')
        children.NodeType.text.title.return_value = 'T1'
        nodes_mock.iterchildren.return_value = [children]
        self.assertEqual(children, self.ispdb.get_host_config('h1'))

    def test_get_hosts_with_type_AttributeError(self):
        self.ispdb.extended_hosts_config = MagicMock(return_value='s1')
        self.assertEqual([], self.ispdb.get_hosts_with_type())

    def test_non_gd_hosts(self):
        self.ispdb.EXTENDEDNODES = ('M1')
        host_n_type = [('n1', 'M1'), ('n2', 'M2')]
        self.ispdb.get_hosts_with_type = MagicMock(return_value=host_n_type)
        self.assertEqual(['n1'], self.ispdb.non_gd_hosts())

    def test_gd_hosts(self):
        self.ispdb.EXTENDEDNODES = ('M1')
        host_n_type = [('n1', 'M1'), ('n2', 'M2')]
        self.ispdb.get_hosts_with_type = MagicMock(return_value=host_n_type)
        self.assertEqual([('n2', 'M2')], self.ispdb.gd_hosts())


class ScanlineISPProductScannerTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp import ISPProductScanner
        from scanline.host.isp import ISPHostScanner
        from scanline.host.isp import ExtendedISPHost
        from scanline.host.udm import UDMRedisHostScanner
        from scanline.host.windows import WindowsHostScanner
        self.ISPProductScanner = ISPProductScanner
        self.ISPHostScanner = ISPHostScanner
        self.ExtendedISPHost = ExtendedISPHost
        self.UDMRedisHostScanner = UDMRedisHostScanner
        self.WindowsHostScanner = WindowsHostScanner
        self.ispcs = patch.multiple(
            'scanline.product.isp',
            ISP4_1Configuration=DEFAULT,
            ISP4XConfiguration=DEFAULT,
            ISP3XConfiguration=DEFAULT,
            ISP4XHiSecConfiguration=DEFAULT,
            ISPDBConfiguration=DEFAULT
        )
        self.mock_ispcs = self.ispcs.start()
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'])
        self.ISPHostScanner.to_dict = MagicMock(name='to_dict')
        self.ExtendedISPHost.to_dict = MagicMock(name='to_dict')
        self.WindowsHostScanner.to_dict = MagicMock(name='to_dict')
        self.UDMRedisHostScanner.to_dict = MagicMock(name='to_dict')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)
        self.ispcs.stop()

    # def test_create_isp_3x(self):
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP3XConfiguration'].return_value)

    # def test_create_isp_4x(self):
    #     self.mock_ispcs[
    #         'ISP3XConfiguration'].return_value.get_software_version.return_value = None
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP4XConfiguration'].return_value)

    # def test_create_isp_4xHS(self):
    #     self.mock_ispcs[
    #         'ISP3XConfiguration'].return_value.get_software_version.return_value = None
    #     self.mock_ispcs[
    #         'ISP4XConfiguration'].return_value.get_software_version.return_value = None
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP4XHiSecConfiguration'].return_value)

    # def test_create_isp_4_1(self):
    #     self.mock_ispcs[
    #         'ISP3XConfiguration'].return_value.get_software_version.return_value = None
    #     self.mock_ispcs[
    #         'ISP4XConfiguration'].return_value.get_software_version.return_value = None
    #     self.mock_ispcs[
    #         'ISP4XHiSecConfiguration'].return_value.get_software_version.return_value = None
    #     self.assertEqual(self.isp_scanner.create_isp(), self.mock_ispcs[
    #                      'ISP4_1Configuration'].return_value)

    @patch('scanline.product.isp.ISPProductScanner.isp')
    def test_get_hosts(self, mock_isp):
        mock_isp.get_hosts.return_value = ['host1']
        self.assertEqual(list(self.isp_scanner.get_hosts()), ['host1'])

    @patch('scanline.product.isp.ISPProductScanner.isp', None)
    def test_get_hosts_no_isp(self):
        self.assertEqual(list(self.isp_scanner.get_hosts()), [])

    def test_get_hostname(self):
        self.assertEqual(self.isp_scanner.get_hostname(
            {'a': 'b'}), "{'a': 'b'}")

    @patch('scanline.product.isp.ISPHostScanner')
    @patch('scanline.product.isp.WindowsHostScanner')
    def test_scan_host(self, mock_windows_scanner, mock_isp_scanner):
        mock_windows_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'Windows': {'version': 'Windows 2k', 'patch': 'please'}
        }
        mock_isp_scanner.return_value.to_dict.return_value = {
            'address': '10.4.5.6',
            'ISP': {'module_type': '4', 'version': '4,4,4,4'}
        }
        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        self.mock_linecache.getline.return_value = 'secret'
        self.assertEqual(
            self.isp_scanner.scan_host('host1'),
            {
                'Windows': {'version': 'Windows 2k', 'patch': 'please'},
                'ISP': {'version': '4,4,4,4', 'module_type': '4'},
                'ModR': {'key1': 'val1'},
                'address': '10.4.5.6',
            }
        )
        mock_isp_scanner.assert_called_once_with(
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1', ipv6_enabled=False, is_distrad_prime='no',
            isp=self.isp_scanner.isp,
            password='CryptThis',
            secret='secret',
            tags=['Prod'],
            username='tester', )

        mock_windows_scanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            ipv6_enabled=False,
            password=mock_isp_scanner().password,
            tags=['Prod'],
            username=mock_isp_scanner().username
        )

        self.isp_scanner.host_scanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            ipv6_enabled=False,
            password=mock_isp_scanner().password,
            tags=['Prod'],
            username=mock_isp_scanner().username
        )

    def test_scan_dbhost(self):
        self.ExtendedISPHost.return_value.to_dict = MagicMock(name='to_dict',
                                                              return_value={'address': '10.4.5.6',
                                                                            'ISP': {'module_type': '4',
                                                                                    'version': '4,4,4,4'}
                                                                            }
                                                              )

        self.WindowsHostScanner.return_value.to_dict = MagicMock(name='to_dict',
                                                                 return_value={'address': '10.4.5.6',
                                                                               'Windows': {'version': 'Windows 2k',
                                                                                           'patch': 'please',
                                                                                           'endpoint': {
                                                                                               'scanner': 'ISP'}}
                                                                               }
                                                                 )
        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.isp.software_version = '4,4,4,4'
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.isp_scanner.scan_dbhost('host1'),
            {
                'Windows': {'version': 'Windows 2k', 'patch': 'please', 'endpoint': {'scanner': 'ISP'}},
                'ISP': {'version': '4,4,4,4', 'module_type': '4'},
                'ModR': {'key1': 'val1'},
                'product_version': '4,4,4,4',
                'address': '10.4.5.6',
            }
        )

        self.WindowsHostScanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            password=self.ExtendedISPHost().password,
            tags=['Prod'],
            username=self.ExtendedISPHost().username
        )

    def test_scan_dbhost_without_hl7(self):
        self.ExtendedISPHost.return_value.to_dict = MagicMock(name='to_dict',
                                                              return_value={'address': '10.4.5.6',
                                                                            'ISP': {'module_type': 'Hl7',
                                                                                    'version': '4,4,4,4'}
                                                                            }
                                                              )

        self.WindowsHostScanner.return_value.to_dict = MagicMock(name='to_dict',
                                                                 return_value={'address': '10.4.5.6',
                                                                               'Windows': {'version': 'Windows 2k',
                                                                                           'patch': 'please',
                                                                                           'endpoint': {
                                                                                               'scanner': 'ISP'}}
                                                                               }
                                                                 )
        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.isp.software_version = '4,4,4,4'
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        expected_response = {'Windows': {'version': 'Windows 2k', 'endpoint': {'scanner': 'ISP'}, 'patch': 'please'},
                             'ModR': {'key1': 'val1'}, 'ISP': {'version': '4,4,4,4', 'module_type': 'Hl7'},
                             'product_version': '4,4,4,4', 'address': '10.4.5.6'}
        self.assertEqual(
            self.isp_scanner.scan_dbhost('host1'),
            expected_response
        )

        self.WindowsHostScanner.assert_called_once_with(
            domain=None,
            endpoint={'scanner': 'ISP', 'address': '10.220.3.1'},
            hostname='host1',
            password=self.ExtendedISPHost().password,
            tags=['Prod'],
            username=self.ExtendedISPHost().username
        )

    def test_scan_redis_host(self):
        self.UDMRedisHostScanner.return_value.to_dict = MagicMock(name='to_dict',
                                                                  return_value={'address': '10.4.5.6',
                                                                                'UDMRedis': {'module_type': '4',
                                                                                             'version': '4,4,4,4'}
                                                                                }
                                                                  )

        self.isp_scanner.host_scanner = MagicMock()
        self.isp_scanner.host_scanner.return_value.to_dict.return_value = {
            'ModR': {'key1': 'val1'}}
        self.assertEqual(
            self.isp_scanner.scan_redis_host('host1'),
            {
                'UDMRedis': {'version': '4,4,4,4', 'module_type': '4'},
                'ModR': {'key1': 'val1'},
                'product_version': self.mock_ispcs['ISP4XHiSecConfiguration'].return_value.software_version,
                'address': '10.4.5.6',
                'product_id': None
            }
        )

    def test_get_redis_host_facts(self):
        self.isp_scanner.scan_redis_host = MagicMock(
            name='scan_redis_host',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM'}
        )
        self.isp_scanner.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.isp_scanner.get_redis_host_facts({'hostname': 'host1'}),
            {'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'], 'address': '10.0.0.1', 'manufacturer': 'IBM'}
        )

    def test_get_dbhost_facts(self):
        self.isp_scanner.scan_dbhost = MagicMock(
            name='scan_dbhost',
            return_value={'Windows': {}, 'address': '10.0.0.1', 'ISP': {}, 'manufacturer': 'IBM'}
        )
        self.isp_scanner.get_present_modules = MagicMock(
            name='get_present_modules', return_value=['Windows', 'ISP']
        )
        self.assertEqual(
            self.isp_scanner.get_dbhost_facts({'hostname': 'host1'}),
            {'Windows': {}, 'ISP': {}, 'modules': ['Windows', 'ISP'], 'address': '10.0.0.1', 'manufacturer': 'IBM'}
        )

    def test_site_facts(self):
        self.isp_scanner.get_dbhost_facts = MagicMock(name='get_dbhost_facts', return_value='167.81.183.99')
        self.isp_scanner.get_hostname = MagicMock(name='get_hostname', return_value='167.81.183.99')

        self.isp_scanner.isp.__class__.__name__ = 'ISP4XHiSecConfiguration'
        self.isp_scanner.site_facts = MagicMock(name='site_facts', return_value={'167.81.183.99':
                                                                                     self.isp_scanner.isp.get_dbhost_facts.return_value
                                                                                 }
                                                )
        self.assertEqual(
            self.isp_scanner.site_facts(),
            {'167.81.183.99': self.isp_scanner.isp.get_dbhost_facts.return_value}
        )

    def test_site_facts_noncore(self):
        self.isp_scanner.get_dbhost_facts = MagicMock(name='get_dbhost_facts',
                                                      return_value='167.81.183.99')
        self.isp_scanner.get_hostname = MagicMock(name='get_hostname',
                                                  return_value='167.81.183.99')

        self.isp_scanner.isp.__class__.__name__ = 'ISP4XHiSecConfiguration'
        self.isp_scanner.site_facts = MagicMock(name='site_facts',
                                                return_value={'167.81.183.199':
                                                                  self.isp_scanner.isp.get_dbhost_facts.return_value
                                                              }
                                                )
        self.assertEqual(
            self.isp_scanner.site_facts(),
            {'167.81.183.199': self.isp_scanner.isp.get_dbhost_facts.return_value}
        )

    def test_get_clusterdb_credentials(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbuser='dbuser', dbpassword='dbpassword')
        credentials = self.isp_scanner.get_clusterdb_credentials(self.ISPHostScanner, 'dbcluster')
        self.assertEqual(self.ISPHostScanner.encrypter.encrypt.call_count, 4)

    def test_get_clusterdb_credentials_without_db(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'])
        credentials = self.isp_scanner.get_clusterdb_credentials(self.ISPHostScanner, 'dbcluster')
        self.assertEqual(self.ISPHostScanner.encrypter.encrypt.call_count, 2)

    def test_get_clusterdb_dbuser(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbuser='dbuser')
        credentials = self.isp_scanner.get_clusterdb_credentials(self.ISPHostScanner, 'dbcluster')
        self.assertEqual(self.ISPHostScanner.encrypter.encrypt.call_count, 3)

    def test_get_clusterdb_dbpassword(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword')
        credentials = self.isp_scanner.get_clusterdb_credentials(self.ISPHostScanner, 'dbcluster')
        self.assertEqual(self.ISPHostScanner.encrypter.encrypt.call_count, 3)

    @patch('scanline.product.isp.ISPHostScanner')
    def test_compose_cluster_facts(self, isp_host_scanner):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword')
        host_scanner_instance = isp_host_scanner.return_value
        self.isp_scanner.get_clusterdb_credentials = MagicMock(name='get_clusterdb_credentials',
                                                               return_value='credentials')
        output_data = self.isp_scanner.compose_cluster_facts('cluster_name', 'cluster_host', 'address', 'role')
        version = self.mock_ispcs['ISP4XHiSecConfiguration'].return_value.software_version
        cluster_host = {'cluster_name': 'cluster_name', 'ISP': {}, 'address': 'address', 'role': 'role',
                        'product_id': None, 'product_version': version, 'credentials': 'credentials'}
        self.assertEqual(output_data, {'cluster_host': cluster_host})
        self.isp_scanner.get_clusterdb_credentials.assert_called_once_with(host_scanner_instance, 'role')

    @patch('scanline.product.isp.ISPHostScanner')
    def test_compose_cluster_facts_enable_billing_with_dbcluster_role(self, isp_host_scanner):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword', enable_billing=True)
        host_scanner_instance = isp_host_scanner.return_value
        self.isp_scanner.get_clusterdb_credentials = MagicMock(name='get_clusterdb_credentials',
                                                               return_value='credentials')
        output_data = self.isp_scanner.compose_cluster_facts('cluster_name', 'cluster_host', 'address', role='dbcluster')
        version = self.mock_ispcs['ISP4XHiSecConfiguration'].return_value.software_version
        cluster_host = {'cluster_name': 'cluster_name', 'ISP': {}, 'address': 'address', 'role': 'dbcluster',
                        'product_id': None, 'product_version': version, 'credentials': 'credentials', 'enable_billing': 'yes'}
        self.assertEqual(output_data, {'cluster_host': cluster_host})
        self.isp_scanner.get_clusterdb_credentials.assert_called_once_with(host_scanner_instance, 'dbcluster')

    @patch('scanline.product.isp.ISPHostScanner')
    def test_compose_cluster_facts_enable_billing_without_dbcluster_role(self, isp_host_scanner):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword', enable_billing=True)
        host_scanner_instance = isp_host_scanner.return_value
        self.isp_scanner.get_clusterdb_credentials = MagicMock(name='get_clusterdb_credentials',
                                                               return_value='credentials')
        output_data = self.isp_scanner.compose_cluster_facts('cluster_name', 'cluster_host', 'address', role='nondbcluster')
        version = self.mock_ispcs['ISP4XHiSecConfiguration'].return_value.software_version
        cluster_host = {'cluster_name': 'cluster_name', 'ISP': {}, 'address': 'address', 'role': 'nondbcluster',
                        'product_id': None, 'product_version': version, 'credentials': 'credentials'}
        self.assertEqual(output_data, {'cluster_host': cluster_host})
        self.isp_scanner.get_clusterdb_credentials.assert_called_once_with(host_scanner_instance, 'nondbcluster')

    @patch('scanline.product.isp.ISPHostScanner')
    def test_compose_cluster_facts_exception(self, isp_host_scanner):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword')
        host_scanner_instance = isp_host_scanner.return_value
        self.isp_scanner.get_clusterdb_credentials = MagicMock(name='get_clusterdb_credentials',
                                                               side_effect=Exception('e'))
        output_data = self.isp_scanner.compose_cluster_facts('cluster_name', 'cluster_host', 'address', 'role')
        facts = {'cluster_host': {'ISP': {}, 'product_id': None, 'product_version': self.mock_ispcs['ISP4XHiSecConfiguration'].return_value.software_version, 'cluster_name': 'cluster_name',
                                  'role': 'role', 'address': 'address'}}
        self.assertEqual(facts, output_data)
        self.isp_scanner.get_clusterdb_credentials.assert_called_once_with(host_scanner_instance, 'role')

    def test_rabbitmq_cluster_manager(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword')
        self.isp_scanner.compose_cluster_facts = MagicMock(name='compose_cluster_facts',
                                                           return_value={'json_data': 'data'})
        ISP = {'a':'b'}
        output_data = self.isp_scanner.rabbitmq_cluster_manager('role', ISP)
        self.assertEqual(output_data, {'json_data': 'data'})
        self.isp_scanner.compose_cluster_facts.assert_called_once_with(self.isp_scanner.rabbit_cluster_host,
                                                                       self.isp_scanner.rabbit_cluster_host,
                                                                       self.isp_scanner.rabbit_cluster_host, 'role', ISP)

    def test_add_cluster_with_two_node(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword')
        self.isp_scanner.check_cluster = MagicMock(name='check_cluster', return_value={'json_data': 'data'})
        output_data = self.isp_scanner.add_cluster('cluster_nodes', 2, 'role')
        self.assertEqual(output_data, {'json_data': 'data'})
        self.isp_scanner.check_cluster.assert_called_once_with('cluster_nodes', 'role')

    def test_add_cluster_with_single_node(self):
        self.isp_scanner = self.ISPProductScanner(
            'ISP', '10.220.3.1', 'tester', 'CryptThis', tags=['Prod'], dbpassword='dbpassword')
        output_data = self.isp_scanner.add_cluster('cluster_nodes', 1, 'role')
        self.assertEqual(output_data, None)


if __name__ == '__main__':
    unittest.main()
