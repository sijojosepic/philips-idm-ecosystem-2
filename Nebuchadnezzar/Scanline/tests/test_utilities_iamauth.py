import unittest
from mock import MagicMock, patch

 
class HSDPIAMUserTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_hmac = MagicMock(name='hmac')
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_base64 = MagicMock(name='base64')
        self.mock_requests = MagicMock(name='requests')
        self.mock_time = MagicMock(name='time')
        self.mock_proxy = MagicMock(name='proxy')
        modules = {
            'hmac': self.mock_hmac,
            'hashlib': self.mock_hashlib,
            'base64': self.mock_base64,
            'requests': self.mock_requests,
            'time': self.mock_time,
            'proxy': self.mock_proxy,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities.iamauth import HSDPIAMUser
        self.HSDPIAMUser = HSDPIAMUser
        self.hsdp_obj = self.HSDPIAMUser()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_create_hmac(self):
        h = self.mock_hmac.new('secret1', digestmod=self.mock_hashlib.sha256)
        h.update('data1') 
        obj = self.HSDPIAMUser()
        self.assertEqual(self.HSDPIAMUser.create_hmac(obj,'data1', 'secret1'), h.digest())

    def test_sig_header(self):
        obj = self.HSDPIAMUser()
        secret_key_prefix = 'DHPWS'
        sign_time = self.mock_time.strftime("%Y-%m-%dT%H:%M:%SZ", self.mock_time.gmtime())
        secret = ''.join([secret_key_prefix, self.HSDPIAMUser.secret_key])
        sig = self.HSDPIAMUser.create_hmac(obj,  self.mock_base64.b64encode(sign_time), secret)
        sig_header = ("HmacSHA256;Credential:{0};SignedHeaders:SignedDate;"
                      "Signature:{1}".format(self.HSDPIAMUser.shared_key, self.mock_base64.b64encode(sig)))
        self.assertEqual(self.HSDPIAMUser.sig_header(obj), sig_header)



    def test_get_response(self):
        obj = self.HSDPIAMUser()
        self.assertEqual(self.HSDPIAMUser.get_response(obj), self.HSDPIAMUser.response)

    def test_get_status(self):
        obj = self.HSDPIAMUser()
        self.assertEqual(self.HSDPIAMUser.get_status(obj), self.HSDPIAMUser.status_code)

    def test_get_headers(self):
        obj = self.HSDPIAMUser()
        self.assertEqual(self.HSDPIAMUser.get_headers(obj), self.HSDPIAMUser.headers)

    def test_get_payload(self):
        obj = self.HSDPIAMUser()
        self.assertEqual(self.HSDPIAMUser.get_payload(obj), self.HSDPIAMUser.json)

    def test_get_url(self):
        obj = self.HSDPIAMUser()
        self.assertEqual(self.HSDPIAMUser.get_url(obj), self.HSDPIAMUser.url + self.HSDPIAMUser.uri_path) 

class UserLoginTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_hmac = MagicMock(name='hmac')
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_base64 = MagicMock(name='base64')
        self.mock_requests = MagicMock(name='requests')
        self.mock_time = MagicMock(name='time')
        self.mock_proxy = MagicMock(name='proxy')
        modules = {
            'hmac': self.mock_hmac,
            'hashlib': self.mock_hashlib,
            'base64': self.mock_base64,
            'base64.b64encode': self.mock_base64.b64encode,
            'base64.encodestring': self.mock_base64.encodestring,
            'requests': self.mock_requests,
            'time': self.mock_time,
            'proxy': self.mock_proxy,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities.iamauth import UserLogin
        self.UserLogin = UserLogin

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    

    def test_get_payload(self):
        userlogin = self.UserLogin('url1', 'secret_key1', 'shared_key1', 'user1', 'password1')
        result = {"password": userlogin.password, "loginId": userlogin.user}
        self.assertEqual(userlogin.get_payload(), result)

    def test_get_headers(self):
        userlogin = self.UserLogin('url1', 'secret_key1', 'shared_key1', 'user1', 'password1')
        result = {
            'hsdp-api-signature': userlogin.sig_header(),
            'Content-Type': 'application/json',
            'SignedDate': self.mock_time.strftime("%Y-%m-%dT%H:%M:%SZ", self.mock_time.gmtime()),
            'Accept': 'application/json'
        }
        self.assertEqual(userlogin.get_headers(), result)

if __name__ == '__main__':
    unittest.main()