import unittest
from mock import MagicMock, patch
from redis.exceptions import RedisError


class BackOfficeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_http = MagicMock(name='http')
        self.mock_requests = MagicMock(name='requests')
        modules = {
            'scanline.utilities.http': self.mock_http,
            'logging': MagicMock(name='logging'),
            'requests': self.mock_requests,
         }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities.back_office import BackOffice  
        self.module = BackOffice

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_BackOffice_class_attributes(self):
        HEADERS = {'X-Requested-With': "True"}
        HOST_URL = '/pma/facts/{site_id}/hosts'
        MODULE_URL = '/pma/facts/{site_id}/modules/ISP/keys/module_type'
        FACT_URL = '/pma/facts/{site_id}/nodes'
        self.assertEqual(HEADERS, self.module.HEADERS)
        self.assertEqual(HOST_URL, self.module.HOST_URL)
        self.assertEqual(MODULE_URL, self.module.MODULE_URL)
        self.assertEqual(FACT_URL, self.module.FACT_URL)

    def test_base_url_rstrip(self):
        obj = self.module('base', 'site_id')
        obj1 = self.module('base/', 'site_id')
        self.assertEqual(obj.base_url, 'base')
        self.assertEqual(obj1.base_url, 'base')

    def test_verify(self):
        obj = self.module('base', 'site_id')
        obj1 = self.module('base', 'site_id', True)
        self.assertEqual(obj.verify, False)
        self.assertEqual(obj1.verify, True)

    def test_form_url(self):
        obj = self.module('base', 'site_id')
        self.assertEqual(obj.form_url('url'), 'baseurl')

    def test_req_with_headers(self):
        obj = self.module('base', 'site_id')
        obj.requster = MagicMock(name='requster')
        obj.HEADERS = {'a': 'b'}
        self.assertEqual(obj.req('url', {'h1': 'h2'}),
                         obj.requster.suppressed_get.return_value)
        headers = {'a': 'b', 'h1': 'h2'}
        obj.requster.suppressed_get.assert_called_once_with('url', headers=headers,
                                                            verify=False)

    def test_req_without_headers(self):
        obj = self.module('base', 'site_id')
        obj.requster = MagicMock(name='requster')
        obj.HEADERS = {'a': 'b'}
        self.assertEqual(obj.req('url', None),
                         obj.requster.suppressed_get.return_value)
        headers = {'a': 'b'}
        obj.requster.suppressed_get.assert_called_once_with('url', headers=headers,
                                                            verify=False)

    def test_hosts_with_mod_type(self):
        obj = self.module('base', 'site_id')
        obj.form_url = MagicMock()
        obj.req = MagicMock()
        obj.MODULE_URL = 'M1'
        self.assertEqual(obj.hosts_with_mod_type('h1'),
                         obj.req.return_value)
        obj.form_url.assert_called_once_with('M1')
        obj.req.assert_called_once_with(obj.form_url.return_value, 'h1')


if __name__ == '__main__':
    unittest.main()
