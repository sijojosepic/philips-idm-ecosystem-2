'''
Created by Vijender.singh@philips.com on 7/15/18
'''
import unittest
import json
from mock import MagicMock, patch, Mock


class ScanlineHSOPVMProductScanner(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_logging = MagicMock(name='logging')
        self.mock_json = MagicMock(name='json')
        self.mock_collection = MagicMock(name='collection')
        self.mock_host =MagicMock(name='scanline.host')
        self.mock_utilities = MagicMock(name='scanline.utilities')
        self.mock_cached_properties = MagicMock(name='cached_property')
        modules = {
            'logging' : self.mock_logging,
            'json': self.mock_json,
            'collection': self.mock_collection,
            'collection.namedtuple': self.mock_collection.namedtuple,
            'scanline.host': self.mock_host,
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.http': self.mock_utilities.http,
            'cached_property': self.mock_cached_properties,
            # 'scanline.host.ProductScanner': self.mock_product.ProductScanner,
            'requests': self.mock_request
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.product import hsopvm 
        self.DOMAINNAME = 'cloud_admin'
        self.USERNAME = 'admin'
        self.PASSWORD = 'admin'
        self.HOSTNAME = '130.147.86.205'
        self.SCANNER_NAME = 'HSOPVM'
        self.hsop_vm_product_scanner = hsopvm.HSOPVMProductScanner(self.SCANNER_NAME, self.HOSTNAME, self.USERNAME,
                                                            self.PASSWORD, self.DOMAINNAME)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_create_token_payload(self):
        expected_body_val = self.mock_json.dumps()
        expected_header = {'content-type': 'application/json'}
        result = self.hsop_vm_product_scanner.get_token_payload()
        assert expected_header == result.headers
        assert expected_body_val == result.body

    def test_get_token(self):
        mock_headers = {'x-subject-token': 'SomeBase64String'}
        self.mock_request.post.return_value = Mock(ok=True, headers=mock_headers, status_code=200)
        token_payload = self.hsop_vm_product_scanner.get_token_payload()
        token = self.hsop_vm_product_scanner.get_token(token_payload)
        self.assertEqual(token, 'SomeBase64String')

    def test_get_product_id(self):
        stubbed_response = [{"is_domain": "false", "name": "default", "enabled": "true", "domain_name": "cloud_admin",
                             "domain_id": "default", "parent_id": "", "id": "d134786daf55448d8c02b97601aa38e8",
                             "description": "Default project"}]
        self.mock_request.get.return_value = Mock(ok=True, status_code=200)
        self.mock_request.get.return_value.json.return_value = stubbed_response
        token = 'SomeBase64String'
        product_id = self.hsop_vm_product_scanner.get_product_id(token)
        self.assertEqual(product_id, 'd134786daf55448d8c02b97601aa38e8')

    def test_get_product_token_payload(self):
        stubbed_token = 'SomeBase64String'
        stubbed_product_id = 'd134786daf55448d8c02b97601aa38e8'
        expected_body_val = self.mock_json.dumps()
        expected_header = {'content-type': 'application/json'}
        result = self.hsop_vm_product_scanner.get_product_token_payload(stubbed_token, stubbed_product_id)
        assert expected_header == result.headers
        assert expected_body_val == result.body

    def test_discover_hsop_vms(self):
        stubbed_response = [
            {
                "disable_delete": "false",
                "imageId": "3967a755-7ea6-4fb5-bde4-c585afabf099",
                "id": "a923f8ae-a211-4279-96dd-00b5acefecd8",
                "user_id": "admin",
                "key_map": "en-us",
                "slaProfile": "on demand",
                "hostname": "stratonode3.node.strato",
                "restart_on_failure": "false",
                "networks": [
                    {
                        "port_id": "158d2c23-bf67-4195-ae0a-5f85a78fc676",
                        "id": "c4d23a7a-e903-4035-804c-da3df64ddf2f",
                        "address": "10.11.14.28"
                    }
                ],
                "key_pair": "null",
                "metadata": {},
                "status": "active",
                "updated": "2018-07-11T06:07:54Z",
                "tags": [],
                "diskGB": 100.0,
                "hw_firmware_type": "bios",
                "bootVolume": "b9952237-92c4-413d-aaab-f060fe11cc2b",
                "name": "SQL for Stratoscale",
                "created": "2018-07-11T06:03:02Z",
                "tenantId": "d134786daf55448d8c02b97601aa38e8",
                "vcpus": 4,
                "volumes": [],
                "ramMB": 8192,
                "vpc_id": "null",
                "instanceType": "c5.xlarge"
            },
            {
                "disable_delete": "false",
                "imageId": "a904f979-5a8a-48db-be22-73819c45f25d",
                "id": "0cad0509-230e-4d4c-951a-aaf8dd308f05",
                "user_id": "7c26ce439bfc4521bd9afdf9db3a5096",
                "key_map": "en-us",
                "slaProfile": "on demand",
                "hostname": "stratonode0.node.strato",
                "restart_on_failure": "false",
                "networks": [
                    {
                        "port_id": "a0867266-ceee-4bf4-a2de-1698e594ec16",
                        "id": "c4d23a7a-e903-4035-804c-da3df64ddf2f",
                        "address": "10.11.14.30"
                    }
                ],
                "key_pair": "null",
                "metadata": {},
                "status": "active",
                "updated": "2018-07-10T11:58:22Z",
                "tags": [],
                "diskGB": 60.0,
                "hw_firmware_type": "bios",
                "bootVolume": "ce5b1c1f-8105-4607-9742-4e6be68de971",
                "name": "IDM-VIGILANT",
                "created": "2018-07-10T11:50:07Z",
                "tenantId": "d134786daf55448d8c02b97601aa38e8",
                "vcpus": 2,
                "volumes": [
                    "bf9af9c5-91b9-40c3-a226-1e3f4cb780b2"
                ],
                "ramMB": 4096,
                "vpc_id": "null",
                "instanceType": "__strato_2CPUS_4096ramMb_d134786daf55448d8c02b97601aa38e8"
            }]
        self.mock_request.get.return_value = Mock(ok=True, status_code=200)
        self.mock_request.get.return_value.json.return_value = stubbed_response
        response = self.hsop_vm_product_scanner.discover_hsop_vms()
        assert json.dumps(response) == json.dumps(stubbed_response)

    def test_get_facts(self):
        stubbed_facts = {
            "10.11.14.30": {self.SCANNER_NAME: {"endpoint": {"scanner": self.SCANNER_NAME, "address": self.HOSTNAME}},
                            "product_id": None, "product_version": None,
                            "modules": ["linux", self.SCANNER_NAME], "address": self.HOSTNAME,
                            "product_name": None, "uuid": "0cad0509-230e-4d4c-951a-aaf8dd308f05",
                            'OS': None, 'floating_ip': '130.147.86.170', 'role' : 'NA', 'cluster_check': False},
            "10.11.14.28": {self.SCANNER_NAME: {"endpoint": {"scanner": self.SCANNER_NAME, "address": self.HOSTNAME}},
                            "product_id": None, "product_version": None,
                            "modules": ["linux", self.SCANNER_NAME], "address": self.HOSTNAME,
                            "product_name": None, "uuid": "a923f8ae-a211-4279-96dd-00b5acefecd8",
                            'OS': None, 'floating_ip': None, 'role' : 'NA', 'cluster_check': False},
            self.HOSTNAME: {self.SCANNER_NAME: {"endpoint": {"scanner": self.SCANNER_NAME, "address": self.HOSTNAME}},
                            "product_id": None, "product_version": None,
                            "modules": ["linux", self.SCANNER_NAME], "address": self.HOSTNAME,
                            "product_name": None,  'role' : 'NA', 'cluster_check': True}
            }
        stubbed_floating_api_response = {'floatingips': [{'floating_ip_address': u'130.147.86.170',
                                             'port_id': u'a0867266-ceee-4bf4-a2de-1698e594ec16'}]}
        self.mock_request.get.return_value = Mock(ok=True, status_code=200)
        self.mock_request.get.return_value.json.return_value = stubbed_floating_api_response
        stubbed_vm_details = [
            {
                "disable_delete": "false",
                "imageId": "3967a755-7ea6-4fb5-bde4-c585afabf099",
                "id": "a923f8ae-a211-4279-96dd-00b5acefecd8",
                "user_id": "admin",
                "key_map": "en-us",
                "slaProfile": "on demand",
                "hostname": "stratonode3.node.strato",
                "restart_on_failure": "false",
                "networks": [
                    {
                        "port_id": "158d2c23-bf67-4195-ae0a-5f85a78fc676",
                        "id": "c4d23a7a-e903-4035-804c-da3df64ddf2f",
                        "address": "10.11.14.28"
                    }
                ],
                "key_pair": "null",
                "metadata": {},
                "status": "active",
                "updated": "2018-07-11T06:07:54Z",
                "tags": [],
                "diskGB": 100.0,
                "hw_firmware_type": "bios",
                "bootVolume": "b9952237-92c4-413d-aaab-f060fe11cc2b",
                "name": "SQL for Stratoscale",
                "created": "2018-07-11T06:03:02Z",
                "tenantId": "d134786daf55448d8c02b97601aa38e8",
                "vcpus": 4,
                "volumes": [],
                "ramMB": 8192,
                "vpc_id": "null",
                "instanceType": "c5.xlarge"
            },
            {
                "disable_delete": "false",
                "imageId": "a904f979-5a8a-48db-be22-73819c45f25d",
                "id": "0cad0509-230e-4d4c-951a-aaf8dd308f05",
                "user_id": "7c26ce439bfc4521bd9afdf9db3a5096",
                "key_map": "en-us",
                "slaProfile": "on demand",
                "hostname": "stratonode0.node.strato",
                "restart_on_failure": "false",
                "networks": [
                    {
                        "port_id": "a0867266-ceee-4bf4-a2de-1698e594ec16",
                        "id": "c4d23a7a-e903-4035-804c-da3df64ddf2f",
                        "address": "10.11.14.30"
                    }
                ],
                "key_pair": "null",
                "metadata": {},
                "status": "active",
                "updated": "2018-07-10T11:58:22Z",
                "tags": [],
                "diskGB": 60.0,
                "hw_firmware_type": "bios",
                "bootVolume": "ce5b1c1f-8105-4607-9742-4e6be68de971",
                "name": "IDM-VIGILANT",
                "created": "2018-07-10T11:50:07Z",
                "tenantId": "d134786daf55448d8c02b97601aa38e8",
                "vcpus": 2,
                "volumes": [
                    "bf9af9c5-91b9-40c3-a226-1e3f4cb780b2"
                ],
                "ramMB": 4096,
                "vpc_id": "null",
                "instanceType": "__strato_2CPUS_4096ramMb_d134786daf55448d8c02b97601aa38e8"
            }]
        hsop_vms_data = self.hsop_vm_product_scanner.get_facts(stubbed_vm_details)
        self.assertEqual(hsop_vms_data, stubbed_facts)


    def test_get_token_exception(self):
        mock_headers = {'x-subject-token': 'SomeBase64String'}
        self.mock_request.post.raise_effect = Exception
        def test_error(self):
                with self.assertRaises(Exception): self.hsop_vm_product_scanner.get_token('token_payload')
    
    def test_get_product_id_with_exception(self):
        stubbed_response = [{"is_domain": "false", "name": "default", "enabled": "true", "domain_name": "cloud_admin",
                                 "domain_id": "default", "parent_id": "", "id": "d134786daf55448d8c02b97601aa38e8",
                                 "description": "Default project"}]
        self.mock_request.get.raise_effect = Exception
        def test_error(self):
                with self.assertRaises(Exception):product_id = self.hsop_vm_product_scanner.get_product_id(token)

    def test_get_product_id_from_response(self):
        self.assertEqual(self.hsop_vm_product_scanner.get_product_id_from_response([{'parent_id': '', 'is_domain': 'false', 'name': 'default', 'id': 'd134786daf55448d8c02b97601aa38e8', 'description': 'Default project', 'enabled': 'true', 'domain_name': 'cloud_admin', 'domain_id': 'default'}]), 'd134786daf55448d8c02b97601aa38e8')


    def test_site_facts(self):
        self.hsop_vm_product_scanner.get_token_payload = MagicMock()
        self.hsop_vm_product_scanner.get_token = MagicMock()
        self.hsop_vm_product_scanner.get_product_id = MagicMock()
        self.hsop_vm_product_scanner.get_product_token_payload = MagicMock()
        r = {'130.147.86.205': {'HSOPVM': {'endpoint': {'scanner': 'HSOPVM', 'address': '130.147.86.205'}}, 'role': 'NA', 'product_id': None, 'address': '130.147.86.205', 'product_version': None, 'modules': ['linux', 'HSOPVM'], 'product_name': None, 'cluster_check': True}}
        self.assertEqual(self.hsop_vm_product_scanner.site_facts(), r)
    
    def test_get_ostype_with_os(self):
        self.assertEqual(self.hsop_vm_product_scanner.get_ostype(['OS']), 'OS')

    def test_get_ostype_with_os(self):
        self.assertEqual(self.hsop_vm_product_scanner.get_ostype(['OS']), 'OS')

if __name__ == '__main__':
    unittest.main()
