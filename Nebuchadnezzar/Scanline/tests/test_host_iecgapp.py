import unittest
from mock import MagicMock, patch
from requests.exceptions import RequestException
from winrm.exceptions import WinRMOperationTimeoutError, AuthenticationError


class ScanlineIECGAPPTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.iecgapp import IECGAppScanner
        self.IECGAppScanner = IECGAppScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_module_name(self):
        scanner = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.assertEqual(self.IECGAppScanner.module_name, 'iECGAPP')

    def test_get_registry_path_Case_1(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        iecg.get_registry_path = MagicMock(name='execute_powershell', return_value="C")
        self.assertEqual(iecg.get_registry_path(), 'C')

    def test_get_registry_path_Case_2(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        mock_get_registry_path = MagicMock(name='execute_powershell', return_value=None)
        iecg.execute_powershell = mock_get_registry_path
        self.IECGAppScanner.DEFAULT_DRIVE = 'F'
        self.assertEqual(iecg.get_registry_path(), 'F')

    def test_execute_powershell_case_1(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        mock_drive = MagicMock(name="drive")
        mock_drive.status_code = 0
        mock_drive.std_out = "C"
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_drive
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        data = iecg.execute_powershell()
        self.assertEqual(data, "C")

    def test_execute_powershell_case_2(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        mock_drive = MagicMock(name="drive")
        mock_drive.status_code = 1
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_drive
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        data = iecg.execute_powershell()
        self.assertEqual(data, None)

    def test_execute_powershell_Exception(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception()
        data = iecg.execute_powershell()
        self.assertEqual(data, None)

    def test_execute_powershell_AuthenticationError(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError()
        data = iecg.execute_powershell()
        self.assertEqual(data, None)

    def test_execute_powershell_RequestException(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = RequestException()
        data = iecg.execute_powershell()
        self.assertEqual(data, None)

    def test_execute_powershell_WinRMOperationTimeoutError(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError()
        data = iecg.execute_powershell()
        self.assertEqual(data, None)

    def test_execute_powershell_TypeError_case_1(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError()
        data = iecg.execute_powershell()
        self.assertEqual(data, None)

    def test_execute_powershell_TypeError_case_2(self):
        iecg = self.IECGAppScanner('host1', '167.81.18.99', username='user', password='secret', tags=['x', 'y'])
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError("takes exactly 2")
        data = iecg.execute_powershell()
        self.assertEqual(data, None)


if __name__ == '__main__':
    unittest.main()
