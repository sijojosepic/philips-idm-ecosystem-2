import unittest
from mock import MagicMock, patch


class ScanlineUtilitiesProxyTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_yaml = MagicMock(name='yaml')
        modules = {
            'yaml': self.mock_yaml,
         }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.utilities import proxy  
        self.proxy = proxy

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_proxy_with_proxy(self):
        stream =[[
        {'proxy':{'proxy': 'proxy',
            'address': '167.81.183.99',
            'password': 'CryptThis',
            'tags':'Prod',
            'scanner': 'ISP',
            'username': 'tester'},
        }]]
        self.proxy.open = MagicMock(return_value=stream)
        self.mock_yaml.load_all = MagicMock(return_value=stream)
        result = {'http': {'username': 'tester', 'scanner': 'ISP', 'tags': 'Prod', 'proxy': 'proxy', 'address': '167.81.183.99', 'password': 'CryptThis'}, 'https': {'username': 'tester', 'scanner': 'ISP', 'tags': 'Prod', 'proxy': 'proxy', 'address': '167.81.183.99', 'password': 'CryptThis'}}
        self.assertEqual(
            self.proxy.get_proxy(), result
            )
       

    def test_get_proxy_without_proxy(self):
        stream =[[
        {'notproxy':{'proxy': 'proxy',
            'address': '167.81.183.99',
            'password': 'CryptThis',
            'tags':'Prod',
            'scanner': 'ISP',
            'username': 'tester'},
        }]]
        self.proxy.open = MagicMock(return_value=stream)
        self.mock_yaml.load_all = MagicMock(return_value=stream)
        result = ''
        self.assertEqual(
            self.proxy.get_proxy(), result
            )

if __name__ == '__main__':
    unittest.main()
