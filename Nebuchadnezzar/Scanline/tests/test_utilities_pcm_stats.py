import unittest
from mock import MagicMock, patch


class PCMStatsTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_json = MagicMock(name='json')
        self.mock_logging = MagicMock(name='logging')
        modules = {
            'requests': self.mock_requests,
            'json': self.mock_json,
            'logging': self.mock_logging,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from scanline.utilities.pcm_stats import PCMStats
        self.PCMStats = PCMStats
        self.pcm_stats = self.PCMStats()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_safe_get_one(self):
        mock_get = MagicMock(name='get')
        mock_get.raise_for_status.return_value = True
        self.mock_requests.get.return_value = mock_get

        result = self.pcm_stats.safe_get('http://localhost')
        self.assertEqual(result, mock_get)

    def test_safe_get_two(self):
        self.mock_requests.get.side_effect = Exception
        result = self.pcm_stats.safe_get('http://localhost')
        self.assertEqual(result, False)

    def test_get_payload(self):
        self.mock_json.dumps.return_value = {'a':'b'}
        result = self.pcm_stats.get_payload()
        self.assertEqual(result, {'a': 'b'})

    def test_pcm_get(self):
        self.PCMStats.safe_get = MagicMock(name='safe_get')
        self.PCMStats.safe_get.return_value = True
        result = self.pcm_stats.pcm_get('localhost')
        self.assertEqual(result, True)

    def test_get_pcmcomponent_one(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {'productname':'PCM',
                                           'productversion': '5.0'}
        self.PCMStats.pcm_get = MagicMock(name='pcm_get')
        self.PCMStats.pcm_get.return_value = mock_response

        result = self.pcm_stats.get_pcmcomponent('localhost')
        self.assertEqual(result, {'version': '5.0', 'name': 'PCM'})

    def test_get_pcmcomponent_two(self):
        self.PCMStats.pcm_get = MagicMock(name='pcm_get')
        self.PCMStats.pcm_get.side_effect = Exception
        result = self.pcm_stats.get_pcmcomponent('localhost')
        self.assertEqual(result, {})

if __name__ == '__main__':
    unittest.main()
