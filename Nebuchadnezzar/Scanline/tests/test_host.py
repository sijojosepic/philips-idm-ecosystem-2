import unittest

from mock import MagicMock, patch, call, PropertyMock


FoundComponent = MagicMock(**{'scan.return_value': True})
NotFoundComponent = MagicMock(**{'scan.return_value': False})
NoAccess = MagicMock(**{'scan.return_value': False})


class ScanlineHostTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
            'scanline.utilities.dns': self.mock_utilities.dns,
            'scanline.utilities.config_reader': self.mock_utilities.config_reader,
            'phimutils': self.mock_phimutils,
            'phimutils.resource': self.mock_phimutils.resource
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline import host
        self.module = host
        self.HostScanner = host.HostScanner
        self.host_scanner = self.HostScanner('host1', {'address': '10.5.6.7', 'scanner': 'scan1'}, tags=['x', 'l'])

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_component_flags(self):
        components = {
            'anywhere': FoundComponent,
            'vlcapture': FoundComponent,
            'cca': NotFoundComponent
        }
        self.host_scanner.get_components = MagicMock(return_value=components)
        self.assertEqual(sorted(self.host_scanner.flags), sorted(['vlcapture', 'anywhere']))

    def test_get_component_flags_non_found(self):
        components = {
            'anywhere': NotFoundComponent,
            'vlcapture': NotFoundComponent,
            'cca': NotFoundComponent
        }
        self.host_scanner.get_components = MagicMock(return_value=components)
        self.assertEqual(sorted(self.host_scanner.flags), [])

    def test_get_component_flags_no_components(self):
        self.assertEqual(sorted(self.host_scanner.flags), [])

    def test_get_address(self):
        self.assertEqual(self.host_scanner.address, self.mock_utilities.dns.get_address.return_value)
        self.mock_utilities.dns.get_address.assert_called_once_with('host1', False)

    def test_get_found_properties(self):
        self.host_scanner.prop1 = 'one'
        self.host_scanner.prop2 = 'two'
        self.host_scanner.find_properties = MagicMock()
        self.host_scanner.find_properties.return_value = [('a', None), ('b', 'b')]
        self.assertEqual(
            self.host_scanner.get_found_properties(['prop1', 'prop2']),
            {'b': 'b'}
        )
        self.host_scanner.find_properties.assert_called_once_with(['prop1', 'prop2'])

    def test_encrypter(self):
        self.assertEqual(self.host_scanner.encrypter,
                         self.mock_phimutils.resource.Encrypter.return_value)
        config_reder = self.mock_utilities.config_reader.get_idm_secret.return_value
        self.mock_phimutils.resource.Encrypter.assert_called_once_with(config_reder)
        self.mock_utilities.config_reader.get_idm_secret.assert_called_once_with()

    def test_find_properties_missing_prop(self):
        self.host_scanner.prop1 = 'one'
        self.assertEqual(
            self.host_scanner.find_properties(['prop1', 'prop2']),
            [('prop1', 'one'),('prop2', None)]
        )

    def test_find_properties_prop_with_none(self):
        self.host_scanner.prop1 = None
        self.host_scanner.prop2 = 'two'
        self.assertEqual(
            self.host_scanner.find_properties(['prop1', 'prop2']),
            [('prop1', None),('prop2', 'two')]
        )

    def test_find_properties_prop_with_exception(self):
        self.host_scanner.prop1 = 'one'
        self.module.getattr = MagicMock(name='getattr')
        self.module.getattr.side_effect = ['one', Exception]
        self.assertEqual(self.host_scanner.find_properties(['prop1', 'prop2']), [('prop1', 'one')])

    def test_to_dict(self):
        expected_gen_props = {'g_prop1': 'g_val_1', 'g_prop2': 'g_val_2'}
        expected_mod_props = {'m_prop1': 'm_val_1', 'm_prop2': 'm_val_2'}
        self.HostScanner.general_properties = expected_gen_props.keys()
        self.HostScanner.module_properties = expected_mod_props.keys()
        mock_found_props = MagicMock(name='get_found_properties')
        mock_found_props.side_effect = [expected_gen_props, expected_mod_props]
        self.host_scanner.get_found_properties = mock_found_props
        self.assertEqual(
            self.host_scanner.to_dict(),
            {
                'g_prop1': 'g_val_1',
                'g_prop2': 'g_val_2',
                self.HostScanner.module_name: {
                    'm_prop2': 'm_val_2',
                    'm_prop1': 'm_val_1'
                }
            }
        )
        mock_found_props.assert_has_calls([call(['g_prop1', 'g_prop2']), call(['m_prop2', 'm_prop1'])])

    def test_to_dict_without_access(self):
        expected_gen_props = {'g_prop1': 'g_val_1', 'g_prop2': 'g_val_2'}
        expected_mod_props = {'m_prop1': 'm_val_1', 'm_prop2': 'm_val_2'}
        self.HostScanner.general_properties = expected_gen_props.keys()
        self.HostScanner.module_properties = expected_mod_props.keys()
        mock_found_props = MagicMock(name='get_found_properties')
        mock_found_props.side_effect = [expected_gen_props, expected_mod_props]
        self.host_scanner.get_found_properties = mock_found_props
        self.host_scanner.to_dict = MagicMock(name='to_dict', return_value={})
        self.assertEqual(self.host_scanner.to_dict(), {})

    def test_format_str_username(self):
        result = self.host_scanner.format_username('idm02\\username')
        self.assertEqual(result, 'idm02\\\\username')

    def test_format_unicode_username(self):
        result = self.host_scanner.format_username(u'idm02\\username')
        self.assertEqual(result, 'idm02\\\\username')

    def test_format_without_double_slashes(self):
        result = self.host_scanner.format_username('username')
        self.assertEqual(result, 'username')

    def test_format_non_basestring(self):
        result = self.host_scanner.format_username(None)
        self.assertEqual(result, None)

    def test_get_product_id(self):
        result = self.host_scanner.get_product_id()
        self.assertEqual(result, None)

    def test_host__iter__(self):
        iteritem_list = [ each for each in iter(self.HostScanner)]
        self.assertEqual(iteritem_list, [('Host', type(self.host_scanner))])

    def test_get_host_scanner(self):
        result = self.HostScanner.get_host_scanners()
        self.assertEqual(result, ['Host'])

    def test_credentials(self):
        mock_get_credentials = MagicMock(name='get_credentials')
        self.host_scanner.get_credentials = mock_get_credentials
        self.assertEquals(self.host_scanner.credentials, mock_get_credentials.return_value)
        mock_get_credentials.assert_called_once_with()

    def test_get_credentails(self):
        self.host_scanner.CREDENTIALS_TYPES = ['username','password']
        self.module.getattr = MagicMock(name='getattr')
        self.module.getattr.return_value = 'something'
        self.host_scanner.encrypter.encrypt = PropertyMock(return_value='something')
        self.assertEquals(self.host_scanner.get_credentials(), {'username': 'something', 'password': 'something'})

if __name__ == '__main__':
    unittest.main()
