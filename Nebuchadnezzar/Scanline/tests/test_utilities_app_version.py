import unittest
from mock import MagicMock, call, patch
from HTMLParser import HTMLParseError


class ScanlineUtilitiesAppVersionTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_requests = MagicMock(name='requests')
            self.mock_host = MagicMock()
            modules = {
                'logging': MagicMock(name='logging'),
                'requests': self.mock_requests,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            from scanline.utilities import app_version
            self.app_version = app_version

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


PAGE_TOP = """\
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>ISP Anywhere Version Information</title>
  </head>
  <body>
    <h1><strong>ISP Anywhere Version Information Page</strong></h1>
    <h1>ISP Anywherere Version  : </h1>
"""

PAGE_BOTTOM = """\
    <h1>ISP PACS Minimum Backend Version  : </h1><h2 id="APP_BACKEND_MINVERSION">4.4.229.0</h2>
  </body>
</html>"""


class AppVersionHTMLParserTestCase(ScanlineUtilitiesAppVersionTest.TestCase):
    def setUp(self):
        ScanlineUtilitiesAppVersionTest.TestCase.setUp(self)
        self.page_top = PAGE_TOP
        self.page_bottom = PAGE_BOTTOM
        self.mock_requests = MagicMock(name='requests')

    def get_page(self, page_middle):
        return self.page_top + page_middle + self.page_bottom

    def tearDown(self):
        ScanlineUtilitiesAppVersionTest.TestCase.tearDown(self)

    def test_AppVersionReader_parser_ok(self):
        page_middle_part = """    <h2 id="APP_VERSION">
1.0.24.0
    </h2>"""
        parser = self.app_version.AppVersionHTMLParser(self.get_page(page_middle_part))
        self.assertEqual(parser.get_appversion(), '1.0.24.0')

    def test_AppVersionReader_parser_ok_long(self):
        page_middle_part = """    <h2 id="APP_VERSION">
4545.34505435.245304.53453490
    </h2>"""
        parser = self.app_version.AppVersionHTMLParser(self.get_page(page_middle_part))
        self.assertEqual(parser.get_appversion(), '4545.34505435.245304.53453490')

    def test_AppVersionReader_parser_version_wrong(self):
        page_middle_part = """    <h2 id="APP_VERSION">
1.0.XX
    </h2>"""
        parser = self.app_version.AppVersionHTMLParser(self.get_page(page_middle_part))
        self.assertEqual(parser.get_appversion(), None)

    def test_AppVersionReader_parser_no_version(self):
        parser = self.app_version.AppVersionHTMLParser(self.get_page(''))
        self.assertEqual(parser.get_appversion(), None)

    def test_AppVersionReader_parser_ok_deeper(self):
        page_middle_part = """        <p>This is a paragraph</p>
    <dl>
        <dt>ISP Anywherere Version:</dt><dd data-itutu="none" id="APP_VERSION" class="APP_LOOK ">2.1.2554.2    </dd>
        <dt>Some other thing Version:</dt><dd data-id="none" class="APP_VERSION ">1.0.12.1</dd>
    <dl>"""
        parser = self.app_version.AppVersionHTMLParser(self.get_page(page_middle_part))
        self.assertEqual(parser.get_appversion(), '2.1.2554.2')

    @patch('scanline.utilities.app_version.AppVersionHTMLParser.feed', side_effect=HTMLParseError('Error here'))
    def test_get_appversion_parse_error(self, mock_feed):
        parser = self.app_version.AppVersionHTMLParser('TEXT')
        self.assertEqual(parser.get_appversion(), None)


class AppVersionJSONRetrieverTestCase(ScanlineUtilitiesAppVersionTest.TestCase):
    def setUp(self):
        ScanlineUtilitiesAppVersionTest.TestCase.setUp(self)

    def tearDown(self):
        ScanlineUtilitiesAppVersionTest.TestCase.tearDown(self)

    def test_invalid_json(self):
        parser = self.app_version.AppVersionJSONRetriever('')
        self.assertEqual(parser.get_appversion(), None)

    def test_get_appversion_good_json(self):
        text = '{"Applications": [ {    "Version": "2.1.12.0",    "ApplicationName": "IntelliSpace Visible Light"}] }'
        parser = self.app_version.AppVersionJSONRetriever(text)
        parser.application_name = "IntelliSpace Visible Light"
        self.assertEqual(parser.get_appversion(), '2.1.12.0')

    def test_get_appversion_json_but_not_right_data(self):
        text = '{"Components": [ {    "Foo": "Else", "Bar": "Someting"}] }'
        parser = self.app_version.AppVersionJSONRetriever(text)
        parser.application_name = "IntelliSpace Visible Light"
        self.assertEqual(parser.get_appversion(), None)


class ScanlineAppVersionTestCase(ScanlineUtilitiesAppVersionTest.TestCase):
    def setUp(self):
        ScanlineUtilitiesAppVersionTest.TestCase.setUp(self)
        self.versioner1 = MagicMock(name='Versioner1')
        self.versioner2 = MagicMock(name='Versioner2')
        targets = [
            ('anywhere/version.html', self.versioner1),
            ('anywhere/version.json', self.versioner2)
        ]
        self.app_version_scanner = self.app_version.AppVersionHTTPScanner('10.2.2.0', 'IntelliSpace PACS Anywhere', targets)
        self.app_version_scanner.suppressed_get = MagicMock(name='suppressed_get')

    def tearDown(self):
        ScanlineUtilitiesAppVersionTest.TestCase.tearDown(self)

    def test_get_page_text_not_found(self):
        self.app_version_scanner.suppressed_get.return_value = None
        result = self.app_version_scanner.get_page_text('Anywhere/Version.json')
        self.assertEqual(result, None)
        self.app_version_scanner.suppressed_get.assert_has_calls(
            [call('https://10.2.2.0/Anywhere/Version.json', timeout=10), call('http://10.2.2.0/Anywhere/Version.json', timeout=10)])

    def test_get_page_text_found_on_first(self):
        self.app_version_scanner.suppressed_get.side_effect = [MagicMock(name='first', content='TEXT'), None]
        result = self.app_version_scanner.get_page_text('Anywhere/Version.json')
        self.assertEqual(result, 'TEXT')
        self.app_version_scanner.suppressed_get.assert_has_calls([call('https://10.2.2.0/Anywhere/Version.json', timeout=10)])

    def test_get_page_text_found_on_last(self):
        self.app_version_scanner.suppressed_get.side_effect = [None, MagicMock(name='second', content='TEXT')]
        result = self.app_version_scanner.get_page_text('Anywhere/Version.json')
        self.assertEqual(result, 'TEXT')
        self.app_version_scanner.suppressed_get.assert_has_calls(
            [call('https://10.2.2.0/Anywhere/Version.json', timeout=10), call('http://10.2.2.0/Anywhere/Version.json', timeout=10)])

    def test_get_version_no_text(self):
        self.app_version_scanner.get_page_text = MagicMock(name='get_version_page_text', return_value=None)
        self.assertEqual(self.app_version_scanner.get_version(), None)

    def test_get_version_text_but_no_version(self):
        self.app_version_scanner.get_page_text = MagicMock(name='get_version_page_text', side_effect=['TEXT1', 'TEXT2'])
        self.app_version_scanner.format_text = MagicMock(name='format_text', return_value='formatted_text')
        self.versioner1.return_value.get_appversion.return_value = None
        self.versioner2.return_value.get_appversion.return_value = None
        self.assertEqual(self.app_version_scanner.get_version(), None)
        self.versioner1.assert_called_once_with('formatted_text', 'IntelliSpace PACS Anywhere')
        self.versioner2.assert_called_once_with('formatted_text', 'IntelliSpace PACS Anywhere')
        exp_call = [call('TEXT1'), call('TEXT2')]
        self.assertEqual(self.app_version_scanner.format_text.mock_calls, exp_call)
        exp_call = [call('anywhere/version.html'), call('anywhere/version.json')]
        self.assertEqual(self.app_version_scanner.get_page_text.mock_calls, exp_call)

    def test_get_version_text_version_found(self):
        self.app_version_scanner.get_page_text = MagicMock(name='get_version_page_text', return_value='TEXT')
        self.app_version_scanner.format_text = MagicMock(name='format_text', return_value='formatted_text')
        self.versioner1.return_value.get_appversion.return_value = '1.4'
        self.assertEqual(self.app_version_scanner.get_version(), '1.4')
        self.versioner1.assert_called_once_with('formatted_text', 'IntelliSpace PACS Anywhere')
        self.app_version_scanner.get_page_text.assert_called_once_with('anywhere/version.html')
        self.app_version_scanner.format_text.assert_called_once_with('TEXT')

    def test_format_text_when_bom_utf16_true(self):
        mock_text = MagicMock(name='text')
        mock_text.startswith.return_value = True
        self.assertEqual(self.app_version_scanner.format_text(mock_text),
                         mock_text.decode.return_value)

    def test_format_text_when_bom_utf16_false(self):
        mock_text = MagicMock(name='text')
        mock_text.startswith.return_value = False
        self.assertEqual(self.app_version_scanner.format_text(mock_text),
                         mock_text)

    def test_format_text_when_UnicodeError(self):
        mock_text = MagicMock(name='text')
        mock_text.startswith.side_effect = UnicodeError
        self.assertEqual(self.app_version_scanner.format_text(mock_text),
                         mock_text)


if __name__ == '__main__':
    unittest.main()
