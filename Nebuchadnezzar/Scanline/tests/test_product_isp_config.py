import unittest
from mock import MagicMock, patch, PropertyMock, DEFAULT, create_autospec
from lxml import objectify, etree
from datetime import datetime, timedelta
from phimutils.resource import NagiosResourcer
from StringIO import StringIO
import json


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""
    extra_params = {'address': '10.1.0.1'}

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
            for k, v in Fake.extra_params.iteritems():
                setattr(cls, k, v)
        return cls


class ScanlineISPTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_cached_property = MagicMock(
                name='cached_property', cached_property=property)
            self.mock_requests = MagicMock(name='requests')
            self.mock_datetime = MagicMock(name='datetime')
            self.mock_dateutil = MagicMock(name='dateutil')
            self.mock_pytz = MagicMock(name='pytz')
            self.mock_linecache = MagicMock(name='linecache')
            self.mock_host = MagicMock()
            self.mock_utilities = MagicMock()
            modules = {
                'cached_property': self.mock_cached_property,
                'logging': MagicMock(name='logging'),
                'linecache': self.mock_linecache,
                'requests': self.mock_requests,
                'datetime': self.mock_datetime,
                'dateutil': self.mock_dateutil,
                'pytz': self.mock_pytz,
                'scanline.host.isp_config': self.mock_host.isp_config,
                'scanline.host.windows': self.mock_host.windows,
                'scanline.utilities.auth': self.mock_utilities.auth,
                'scanline.utilities.win_rm': self.mock_utilities.win_rm,
                'scanline.host.udm': self.mock_host.udm,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class ScanlineISPConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        import scanline.product.isp_config
        self.module = scanline.product.isp_config
        self.ISPAuthenticationError = scanline.product.isp_config.ISPAuthenticationError
        self.ISPConfiguration = scanline.product.isp_config.ISPConfiguration
        self.isp_config = self.ISPConfiguration('10.55.66.1')
        self.isp_config.config_path = 'config_path'
        self.isp_config.auth_path = 'auth_path'
        self.ISPHostConfiguration = scanline.product.isp_config.ISPHostConfiguration

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_get_list_hosts_query(self):
        self.assertEqual(self.isp_config.get_list_hosts_query(), '<ListHosts/>')

    def test_get_server_info_query(self):
        self.assertEqual(self.isp_config.get_server_info_query(), '<GetServerInfo/>')

    def test_config_url(self):
        url = self.isp_config.config_url
        self.assertEqual(url, 'http://10.55.66.1/config_path')

    def test_auth_url(self):
        url = self.isp_config.auth_url
        self.assertEqual(url, 'https://10.55.66.1/auth_path')

    def test_get_host_config_query(self):
        self.assertEqual(
            self.isp_config.get_host_config_query('host01', 'Test/config'),
            '<Retrieve><Host>host01</Host><ConfigurationName>Test/config</ConfigurationName></Retrieve>'
        )

    def test_get_auth_header(self):
        self.assertEqual(self.isp_config.get_auth_header('chuck'), {
                         'Cookie': 'iSiteWebApplication=chuck'})

    def test_reset_auth_session(self):
        self.isp_config.auth_session = MagicMock(name='auth_session')
        self.isp_config._session = 'abc'
        self.isp_config.reset_auth_session()
        self.isp_config.auth_session.assert_called_once_with()
        self.assertEqual(self.isp_config._session, None)

    def test_auth_session(self):
        self.isp_config.get_auth_header = MagicMock(name='get_auth_header')
        self.isp_config.get_auth_token = MagicMock(name='get_auth_token')
        self.isp_config.auth_session()
        self.module.requests.session.assert_called_once_with()
        self.isp_config.get_auth_header.assert_called_once_with(
            self.isp_config.get_auth_token.return_value)
        self.module.requests.session.return_value.headers.update.assert_called_once_with(
            self.isp_config.get_auth_header.return_value
        )

    def test_auth_session_auth_not_possible(self):
        self.isp_config.get_auth_token = MagicMock(
            name='get_auth_token', return_value=None)
        self.assertRaises(self.ISPAuthenticationError, self.isp_config.auth_session)

    @patch('scanline.product.isp_config.objectify')
    def test_query_xml(self, objectify_mock):
        self.isp_config.suppressed_post = MagicMock(name='suppressed_post')
        result = self.isp_config.query_xml('http://host1/version', '</list>')
        self.isp_config.suppressed_post.assert_called_once_with(
            url='http://host1/version', data='</list>')
        objectify_mock.fromstring.assert_called_once_with(
            self.isp_config.suppressed_post.return_value.content)
        self.assertEqual(result, objectify_mock.fromstring.return_value)

    def test_query_xml_no_response(self):
        self.isp_config.suppressed_post = MagicMock(
            name='suppressed_post', return_value=None)
        self.assertEqual(self.isp_config.query_xml(
            'http://host1/version', '</list>'), None)
        self.isp_config.suppressed_post.assert_called_once_with(
            url='http://host1/version', data='</list>')

    def test_get_host_config(self):

        self.isp_config.query_host_config = MagicMock(name='query_host_config')
        self.isp_config.query_host_config.return_value = {'key': 'value'}
        result = self.isp_config.get_host_config('hostname')
        self.assertEqual(result, {'key': 'value'})
        self.isp_config.query_host_config.assert_called_once_with('hostname', 'iSyntaxServer\\ProcessesAndServices')

    def test_get_udm_services_info(self):
        obj = MagicMock(name='response')
        obj.content = '{"key":"value"}'
        self.isp_config.suppressed_get = MagicMock(name='suppressed_get', return_value=obj)
        result = self.isp_config.get_udm_services_info('host')
        self.assertEqual(result, {'key': 'value'})
        self.isp_config.suppressed_get.assert_called_once_with("https://host/UDMServices/Configuration/UDMPROCESSANDSERVICES/true")

    def test_get_udm_services_info_none(self):
        self.isp_config.suppressed_get = MagicMock(name='suppressed_get', return_value=None)
        result = self.isp_config.get_udm_services_info('host')
        self.assertEqual(result, None)
        self.isp_config.suppressed_get.assert_called_once_with("https://host/UDMServices/Configuration/UDMPROCESSANDSERVICES/true")

    def test_get_input_folder(self):
        self.isp_config.query_host_config = MagicMock(name='query_host_config')
        self.isp_config.query_host_config.return_value.get_configuration_attribute.return_value = {
            'key': 'value'}
        result = self.isp_config.get_input_folder('host1')
        self.isp_config.query_host_config.assert_called_once_with(
            'host1', 'iSyntaxServer\\iSiteSystem')
        self.assertEqual(result, "{'key': 'value'}")
        self.isp_config.query_host_config.return_value.get_configuration_attribute.assert_called_once_with(
            'Stack', 'DICOMInputDirectory'
        )

    def test_federation_info(self):
        self.isp_config.query_host_config = MagicMock(name='query_host_config')
        self.isp_config.query_host_config.return_value.get.return_value = '1'
        result = self.isp_config.get_federation_info()
        self.isp_config.query_host_config.assert_called_once_with(
            '10.55.66.1', 'iSyntaxServer\\FederationConfiguration')
        self.assertEqual(result, 1)
        self.isp_config.query_host_config.return_value.get.assert_called_once_with(
            'EnableFederation')

    def test_federation_exception(self):
        self.isp_config.query_host_config = MagicMock(name='query_host_config')
        self.isp_config.query_host_config.side_effect = Exception()
        result = self.isp_config.get_federation_info()
        self.assertEqual(result, None)

    def test_get_input_folder_none_found(self):
        self.isp_config.query_host_config = MagicMock(name='query_host_config')
        self.isp_config.query_host_config.return_value.get_configuration_attribute.return_value = None
        result = self.isp_config.get_input_folder('host1')
        self.isp_config.query_host_config.assert_called_once_with(
            'host1', 'iSyntaxServer\\iSiteSystem')
        self.assertEqual(result, None)
        self.isp_config.query_host_config.return_value.get_configuration_attribute.assert_called_once_with(
            'Stack', 'DICOMInputDirectory'
        )

    def test_noncore_node(self):
        result = self.isp_config.get_noncore_node()
        self.assertEqual(result, None)

    def test_logout(self):
        self.assertEqual(self.isp_config.logout(), None)

    def test__del__(self):
        self.isp_config.logout = MagicMock(name='logout')
        self.isp_config.__del__()
        self.isp_config.logout.assert_called_once_with()


class ScanlineISPConfigurationURLMethodsTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp_config import ISPConfiguration
        self.ISPConfiguration = ISPConfiguration
        self.config_url_patch = patch(
            'scanline.product.isp_config.ISPConfiguration.config_url', new_callable=PropertyMock)
        self.mock_config_url = self.config_url_patch.start()
        self.mock_config_url.return_value = 'http://host1/config'
        self.isp_config = self.ISPConfiguration('10.55.66.1')
        self.isp_config.auth_session = MagicMock(name='auth_session')
        self.isp_config.query_xml = MagicMock(name='query_xml')
        self.isp_config.get_server_info_query = MagicMock(
            name='get_server_info_query')
        self.isp_config.get_list_hosts_query = MagicMock(name='get_list_hosts_query')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)
        self.config_url_patch.stop()

    @patch('scanline.product.isp_config.ISPHostConfiguration')
    def test_query_host_config(self, mock_isphostconfiguration):
        self.isp_config.get_host_config_query = MagicMock(
            name='get_host_config_query')
        result = self.isp_config.query_host_config('host1', 'server/config')
        self.isp_config.get_host_config_query.assert_called_once_with(
            'host1', 'server/config')
        self.isp_config.query_xml.assert_called_once_with(
            'http://host1/config', self.isp_config.get_host_config_query.return_value)
        self.assertEqual(result, mock_isphostconfiguration.return_value)
        mock_isphostconfiguration.assert_called_once_with(data=self.isp_config.query_xml.return_value.__getitem__.return_value,
                                                          host='host1')

    def test_calls_for_get_software_version(self):
        result = self.isp_config.get_software_version()
        self.isp_config.auth_session.assert_called_once_with()
        self.isp_config.query_xml.assert_called_once_with(self.mock_config_url.return_value,
                                                   self.isp_config.get_server_info_query.return_value)

    def test_get_software_version_found(self):
        expected = '4,6,7,8'
        self.isp_config.query_xml.return_value.ServerInfoBlob.ServerInfo.SoftwareVersion = expected
        result = self.isp_config.get_software_version()
        self.assertEqual(result, expected)

    def test_get_software_version_not_found(self):
        self.isp_config.query_xml.return_value.mock_add_spec([])
        result = self.isp_config.get_software_version()
        self.assertEqual(result, None)

    def test_get_hosts(self):
        result = self.isp_config.get_hosts()
        self.assertEqual(
            result, self.isp_config.query_xml.return_value.xpath.return_value)
        self.isp_config.auth_session.assert_called_once_with()
        self.isp_config.query_xml.assert_called_once_with(self.mock_config_url.return_value,
                                                   self.isp_config.get_list_hosts_query.return_value)

    def test_software_version(self):
        self.isp_config.get_software_version = MagicMock(name='get_software_version')
        self.isp_config.get_software_version.return_value = "4.4.5"
        self.assertEqual(self.isp_config.software_version, "4.4.5")


class ScanlineISPHostConfigurationTestCase(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp_config import ISPHostConfiguration
        self.ISPHostConfiguration = ISPHostConfiguration
        self.isp_host_configuration = self.ISPHostConfiguration('', 'host1')
        self.config_root_patch = patch('scanline.product.isp_config.ISPHostConfiguration.config_root',
                                       new_callable=PropertyMock)
        self.mock_config_root = self.config_root_patch.start()
        self.mock_config_root.return_value.mock_add_spec(['key'])
        self.mock_config_root.return_value.key = 'value'

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)
        self.config_root_patch.stop()

    def test_decode_inflate_not_valid(self):
        self.assertFalse(self.ISPHostConfiguration.decode_inflate('24f3rfgf'))

    def test_decode_inflate_valid(self):
        result = self.ISPHostConfiguration.decode_inflate(
            'GQAAAHicK8nILFYAovy8nEqFRIWS1OISAEXhBvg=\n')
        self.assertEqual(result, 'this is only a test')

    def test_get_attr_valid(self):
        self.assertEqual(self.isp_host_configuration.get('key'), 'value')

    def test_get_attr_invalid(self):
        self.assertEqual(self.isp_host_configuration.get('fail'), None)

    def test_get_attr_default_set(self):
        self.assertEqual(self.isp_host_configuration.get(
            'fail', 'DefaultVal'), 'DefaultVal')

    def test_remove_xml_descriptor_no_match(self):
        self.assertEqual(
            self.isp_host_configuration.remove_xml_descriptor('not xml'), 'not xml')

    def test_remove_xml_descriptor_removed(self):
        in_xml = '<? xml version="1.0" encoding="utf-16" ?>\r\n<iSiteSystem>\r\n</iSiteSystem>'
        self.assertEqual(self.isp_host_configuration.remove_xml_descriptor(
            in_xml), '<iSiteSystem>\r\n</iSiteSystem>')

    def test_remove_xml_descriptor_removed_compact(self):
        in_xml = '<?xml version="1.0" encoding="utf-8"?>\n<iSiteSystem>\r\n</iSiteSystem>'
        self.assertEqual(self.isp_host_configuration.remove_xml_descriptor(
            in_xml), '<iSiteSystem>\r\n</iSiteSystem>')


class ScanlineISPHostConfigurationTestCase2(ScanlineISPTest.TestCase):
    def setUp(self):
        ScanlineISPTest.TestCase.setUp(self)
        from scanline.product.isp_config import ISPHostConfiguration
        self.ISPHostConfiguration = ISPHostConfiguration
        self.isp_host_configuration = self.ISPHostConfiguration('', 'host1')

    def tearDown(self):
        ScanlineISPTest.TestCase.tearDown(self)

    def test_config_blob(self):
        self.isp_host_configuration.remove_xml_descriptor = MagicMock(name='remove_xml_descriptor')
        self.isp_host_configuration.remove_xml_descriptor.return_value = "xml data"
        self.assertEqual(self.isp_host_configuration.config_blob, "xml data")

    def test_config_root(self):
        xml_data = """<data></data>"""
        self.config_blob_patch = patch('scanline.product.isp_config.ISPHostConfiguration.config_blob',
                                       new_callable=PropertyMock)
        self.mock_config_blob = self.config_blob_patch.start()
        self.mock_config_blob.return_value = xml_data
        expect_result = etree.tostring(objectify.fromstring(xml_data))
        actual_result = etree.tostring(self.isp_host_configuration.config_root)
        self.config_blob_patch.stop()
        self.assertEqual(actual_result, expect_result)

    def test_config_root_xmlexception(self):
        xml_data = ""
        self.config_blob_patch = patch('scanline.product.isp_config.ISPHostConfiguration.config_blob',
                                       new_callable=PropertyMock)
        self.mock_config_blob = self.config_blob_patch.start()
        self.mock_config_blob.return_value = xml_data
        try:
            self.isp_host_configuration.config_root
        except etree.XMLSyntaxError as e:
            self.assertEqual('None', str(e))
        self.config_blob_patch.stop()

    def test_get_configuration_attribute(self):
        obj = Fake()
        self.isp_host_configuration.get = MagicMock(name='get')
        self.isp_host_configuration.get.return_value = obj
        self.assertEqual(self.isp_host_configuration.get_configuration_attribute(obj, 'extra_params'), {'address': '10.1.0.1'})

    def test_get_configuration_attribute_exception(self):
        try:
            obj = Fake()
            self.isp_host_configuration.get = MagicMock(name='get')
            self.isp_host_configuration.get.return_value = obj
            self.isp_host_configuration.get_configuration_attribute(obj, 'wrong_input')
        except AttributeError as e:
            self.assertEqual('None', str(e))


if __name__ == '__main__':
    unittest.main()
