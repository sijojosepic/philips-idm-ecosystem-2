import unittest
from mock import MagicMock, patch


class ScanlineHostI4EVTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        modules = {
            'cached_property': self.mock_cached_property,
            'logging': MagicMock(name='logging'),
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from scanline.host.i4ev import I4EVHostScanner
        self.I4EVHostScanner = I4EVHostScanner

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_module_name(self):
        self.assertEqual(self.I4EVHostScanner.module_name, 'I4EV')


if __name__ == '__main__':
    unittest.main()
