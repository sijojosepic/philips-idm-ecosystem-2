import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class HSOPGDHostScanner(HostScanner):
    module_name = 'HSOPGD'
