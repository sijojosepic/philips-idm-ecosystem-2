import logging
from scanline.host import HostScanner
logger = logging.getLogger(__name__)


class GenomicsHostScanner(HostScanner):
    module_name = 'GENOMICS'

    def __init__(self, hostname, endpoint, tags=None, product_id='GENOMICS',
                 product_name='GENOMICS', product_version='NA', role='NA', **kwargs):
        super(GenomicsHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, role=role, **kwargs)
