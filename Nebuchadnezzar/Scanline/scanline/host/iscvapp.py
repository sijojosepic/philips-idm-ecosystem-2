import logging
from scanline.host import HostScanner

logger = logging.getLogger(__name__)

class ISCVAppScanner(HostScanner):
    module_name = 'ISCVApp'

    def __init__(self, hostname, endpoint, tags=None, product_id='ISCV',
                 product_name='IntelliSpaceCardioVascular', product_version='NA', **kwargs):
        super(ISCVAppScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
