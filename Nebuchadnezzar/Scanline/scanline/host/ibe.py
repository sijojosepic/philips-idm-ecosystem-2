import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class IBEHostScanner(WindowsHostScanner):
    module_name = 'IBE'

    def __init__(self, hostname, endpoint, tags=None, product_id='IBE',
                 product_name='IBE', product_version='NA', **kwargs):
        super(IBEHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
