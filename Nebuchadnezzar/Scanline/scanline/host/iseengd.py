import logging
from cached_property import cached_property
from scanline.host import HostScanner
from scanline.utilities.win_rm import execute_powershell_script

logger = logging.getLogger(__name__)


class ISEEHostScanner(HostScanner):
    general_properties = HostScanner.general_properties.union(['win_service_flag'])

    def __init__(self, hostname, endpoint, tags=None, **kwargs):
        super(ISEEHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.module_name = kwargs.get('scanner')
        self.win_service_flag = 'yes' if kwargs.get('win_service_flag') == True else 'no'


class IECGGDHostScanner(ISEEHostScanner):
    DEFAULT_DRIVE = "C"
    PS_SCRIPT = """$data = Get-ChildItem "HKLM:\SOFTWARE\Wow6432Node\PHILIPS\TraceMasterVue" | Get-ItemProperty | select InstallDir;
                  $installation_drive = ($data.InstallDir|Out-String)[0]; Write-host $installation_drive"""
    general_properties = ISEEHostScanner.general_properties.union(['registry_path'])

    def __init__(self, hostname, endpoint, tags=None, product_id='iECGAPP', product_name='iECG', product_version='NA',
                 **kwargs):
        super(IECGGDHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
        self.module_name = kwargs.get('scanner')
        self.role = kwargs.get('role')
        self.iecg_username = kwargs.get('iecg_username')
        self.iecg_password = kwargs.get('iecg_password')

    @cached_property
    def registry_path(self):
        """This will read the path from windows registry after executing a powershell script
        and update the installation path in facts collection in mongodb. """
        return self.get_registry_path()

    def get_registry_path(self):
        if 'iECGAPP' in self.role:
            drive = execute_powershell_script(self.hostname, self.iecg_username, self.iecg_password, self.PS_SCRIPT)
            return drive or IECGGDHostScanner.DEFAULT_DRIVE
