import logging
from scanline.host.windows import WindowsScanner

logger = logging.getLogger(__name__)


class DatabaseHostScanner(WindowsScanner):
    module_name = 'Database'
    general_properties = WindowsScanner.general_properties.union(['enable_billing'])
    CREDENTIALS_TYPES = frozenset(['username', 'password','dbuser','dbpassword'])

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(DatabaseHostScanner, self).__init__(hostname, endpoint, username, password, tags=tags, **kwargs)
        self.dbuser = kwargs.get('dbuser')
        self.dbpassword = kwargs.get('dbpassword')
        self.enable_billing = 'yes' if kwargs.get('enable_billing') == True else 'no'
