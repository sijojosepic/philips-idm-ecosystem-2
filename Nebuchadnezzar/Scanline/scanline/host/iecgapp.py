import logging
from cached_property import cached_property
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException
from scanline.utilities.win_rm import WinRM

logger = logging.getLogger(__name__)


class IECGAppScanner(WindowsHostScanner):
    module_name = 'iECGAPP'
    DEFAULT_DRIVE = "C"
    PSCRIPT = """$data = Get-ChildItem "HKLM:\SOFTWARE\Wow6432Node\PHILIPS\TraceMasterVue" | Get-ItemProperty | select InstallDir;
                  $installation_drive = ($data.InstallDir|Out-String)[0]; Write-host $installation_drive"""
    general_properties = WindowsHostScanner.general_properties.union(['registry_path'])

    def __init__(self, hostname, endpoint, tags=None, product_id='iECGAPP',
                 product_name='iECG', product_version='NA', role='NA', **kwargs):
        super(IECGAppScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, role=role, **kwargs)
        self.hostname = hostname

    @cached_property
    def registry_path(self):
        """This will read the path from windows registry after executing a powershell script
        and update the installation path in facts collection in mongodb. """
        return self.get_registry_path()

    def get_registry_path(self):
        drive = self.execute_powershell()
        return drive or IECGAppScanner.DEFAULT_DRIVE

    def execute_powershell(self):
        sys_drive = None
        try:
            win_rm = WinRM(self.hostname, self.user, self.password)
            drive = win_rm.execute_ps_script(IECGAppScanner.PSCRIPT)
            if drive.status_code == 0:
                sys_drive = drive.std_out.strip()
            else:
                logger.error('Error while retrieving the information {0}'.format(str(drive.std_err)))
        except AuthenticationError as e:
            logger.exception('Invalid login credentials')
        except RequestException as e:
            logger.exception('Request Error {0}'.format(e))
        except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
            logger.exception('WinRM Error {0}'.format(str(e)))
        except TypeError as e:
            if 'takes exactly 2' in str(e):
                logger.exception('Issue in connecting to node - {0}'.format(self.hostname))
            else:
                logger.exception('Typeerror(May be Issue in connecting to node - {0})'.format(self.hostname))
        except Exception as e:
            logger.exception('Exception {0}'.format(str(e)))
        return sys_drive
