import logging
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class ISiteHostScanner(WindowsHostScanner):
    module_name = 'ISPACS Billing'

    def __init__(self, hostname, endpoint, tags=None, product_id='NA',
                 product_name='ISPACS Billing', product_version='2.0', **kwargs):
        super(ISiteHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
