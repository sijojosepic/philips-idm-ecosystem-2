import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class HSOPHostScanner(HostScanner):
    module_name = 'HSOP'

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(HSOPHostScanner, self).__init__(hostname, endpoint, tags=tags, **kwargs)
        self.username = username
        self.password = password
        self.host = hostname
