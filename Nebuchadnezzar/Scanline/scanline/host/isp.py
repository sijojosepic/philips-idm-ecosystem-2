import logging
from cached_property import cached_property
from scanline.component.isp import AnywhereComponent, CCAComponent
from scanline.component.isp_process import ISPProcess, UDMProcess
from scanline.host import HostScanner
from scanline.utilities.pcm_stats import PCMStats
from scanline.utilities.win_rm import extract_credentials
from phimutils.resource import Encrypter


logger = logging.getLogger(__name__)


class ISPHostScanner(HostScanner):
    module_name = 'ISP'
    general_properties = HostScanner.general_properties.union(['credentials', 'enable_billing', 'enable_pcm',
                                                               'is_distrad_prime'])
    module_properties = HostScanner.module_properties.union(['module_type', 'identifier',
                                                             'version', 'input_folder',
                                                             'noncore_node', 'pcm_component',
                                                             'federation_status', 'custom_process', 'udm_process' ])

    def __init__(self, hostname, endpoint, isp, tags=None,**kwargs):
        super(ISPHostScanner, self).__init__(
            hostname, endpoint, tags=tags, **kwargs)
        self.isp = isp
        self.product_id = 'ISPACS'
        self.product_name = 'IntelliSpace PACS'
        self.product_version = self.isp.software_version
        self.pcm_obj = PCMStats()
        self.kwargs = kwargs
        self.is_distrad_prime = kwargs.get('is_distrad_prime')
        self.enable_pcm = 'yes' if kwargs.get('enable_pcm') == True else 'no'
        try:
            self._flags.update(self.isp.intrinsic_flags)
        except AttributeError:
            pass

    @property
    def username(self):
        return self.raw_credentials.get('username')

    @cached_property
    def host_config(self):
        return self.isp.get_host_config(self.hostname)

    @cached_property
    def udm_service_config(self):
        if self.module_type in [1024, 512, 2, 3]:
            return self.isp.get_udm_services_info(self.hostname)

    @cached_property
    def encrypter(self):
        return Encrypter(self.kwargs.get('secret', ''))

    @property
    def password(self):
        return self.raw_credentials.get('password')

    @property
    def enable_billing(self):
        if self.module_type == 'Database':
            return 'yes' if self.kwargs.get('enable_billing') == True else 'no'

    @cached_property
    def pcm_component(self):
        return self.pcm_obj.get_pcmcomponent(self.hostname)

    @cached_property
    def credentials(self):
        return self.get_credentials()

    @cached_property
    def module_type(self):
        return self.get_module_type()

    @cached_property
    def input_folder(self):
        return self.isp.get_input_folder(self.hostname)

    @cached_property
    def version(self):
        return self.isp.software_version

    @cached_property
    def identifier(self):
        return self.isp.identifier

    @cached_property
    def federation_status(self):
        return self.get_federation_status()

    @cached_property
    def custom_process(self):
        return ISPProcess(self.host_config).get_process_list()

    @cached_property
    def udm_process(self):
        return UDMProcess(self.udm_service_config).get_process_list()

    @cached_property
    def raw_credentials(self):
        usr, pwd = self.kwargs.get('username'), self.kwargs.get('password')
        if isinstance(usr, tuple):
            return extract_credentials(self.hostname, usr, pwd)
        return {'username': usr, 'password': pwd}

    def get_module_type(self):
        return int(self.host_config.get('ModuleType'))

    def get_federation_status(self):
        if self.module_type == 16:
            return self.isp.get_federation_info()

    def get_credentials(self):
        _credentials = {}
        if self.username:
            _credentials['username'] = self.encrypter.encrypt(
                self.format_username(self.username))
        if self.password:
            _credentials['password'] = self.encrypter.encrypt(self.password)
        if self.module_type == 'Database':
            if self.kwargs.get('dbuser'):
                _credentials['dbuser'] = self.encrypter.encrypt(
                    self.kwargs.get('dbuser'))
            if self.kwargs.get('dbpassword'):
                _credentials['dbpassword'] = self.encrypter.encrypt(
                    self.kwargs.get('dbpassword'))
        return _credentials

    def get_components(self):
        if self.noncore_node:
            return {}
        if not self.address:
            logger.warning(
                'No address for Host: %s, components not retrieved', self.hostname)
            return {}
        return {
            'anywhere': AnywhereComponent(),
            # Commenting VL Capture as for workaround VL Capture version is not responding for many sites as well as other sites its None
            # 'vlcapture': VLCaptureComponent(),
            'cca': CCAComponent()
        }

    @cached_property
    def noncore_node(self):
        return self.isp.get_noncore_node()

class ExtendedISPHost(ISPHostScanner):
    """Extended hosts in iSiteRaw config"""

    def get_module_type(self):
        return self.host_config.NodeType.text.title() if self.host_config else ''
