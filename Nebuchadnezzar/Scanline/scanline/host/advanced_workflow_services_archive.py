import logging
import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class AdvancedWorkflowArchiveServicesHostScanner(WindowsHostScanner):
    module_name = 'AdvancedWorkflowArchiveServices'
    productid = 'AWS'
    productname = 'AdvancedWorkflowServices'
    productversion = ''
