import logging
from cached_property import cached_property
from scanline.utilities.dns import get_address
from scanline.utilities.config_reader import get_idm_secret
from phimutils.resource import Encrypter


logger = logging.getLogger(__name__)


class RegisterHostScanners(type):
    def __init__(cls, name, bases, class_dict):
        super(RegisterHostScanners, cls).__init__(name, bases, class_dict)
        if not hasattr(cls, 'registry'):
            cls.registry = {}
        cls.registry[cls.module_name] = cls

    def __iter__(cls):
        return cls.registry.iteritems()

    def get_host_scanners(cls):
        return cls.registry.keys()


class HostScanner(object):
    __metaclass__ = RegisterHostScanners
    module_name = 'Host'
    general_properties = frozenset(
        ['address', 'product_id', 'product_name', 'product_version', 'role', 'tags', 'environmentType'])
    module_properties = frozenset(['tags', 'flags', 'endpoint'])
    CREDENTIALS_TYPES = frozenset([])

    def __init__(self, hostname, endpoint, tags=None, product_id='NA',
                 product_name='NA', product_version='NA', role='NA', **kwargs):
        self.hostname = hostname
        self.endpoint = endpoint
        self.tags = tags
        self.environmentType = kwargs.get('environmentType')
        self.product_id = product_id
        self.product_name = product_name
        self.product_version = product_version
        self.role = role
        self._flags = set()
        self.kwargs = kwargs
        self.ipv6_enabled = kwargs.get('ipv6_enabled', False)

    @property
    def flags(self):
        self.get_component_flags()
        return list(self._flags)

    @cached_property
    def encrypter(self):
        return Encrypter(get_idm_secret())

    @cached_property
    def credentials(self):
        return self.get_credentials()

    def get_credentials(self):
        _credentials = {}
        for item in self.CREDENTIALS_TYPES:
            val = getattr(self, item)
            if val:
                if item == 'username':
                    val = self.format_username(val)
                _credentials[item] = self.encrypter.encrypt(val)
        return _credentials

    def get_component_flags(self):
        add_on_flags = [flag for flag, add_on in self.components.iteritems() if add_on.scan(self.hostname)]
        self._flags.update(add_on_flags)

    @cached_property
    def components(self):
        return self.get_components()

    def get_components(self):
        # dictionary of flag name and component scanner. 'anywhere': AnywhereComponent()
        # This is over-ridden in child classes to set up means to find components
        return {}

    @cached_property
    def address(self):
        return get_address(self.hostname, self.ipv6_enabled)

    def format_username(self, user_name):
        """
            The user_name has to be formatted with four backward slashes,
            instead of double backslashes(if there is), in order to make the
            wmi queries to pass during shinken service check. Otherwise when the
            username is  taken to the respective host's cfg file,
            the wmi queries fails under that host.
        """
        if isinstance(user_name, basestring) and user_name.count('\\') == 1:
            user_name = user_name.replace('\\', '\\\\')
        return user_name

    def find_properties(self, properties):
        """
           Avoid the exceptions while finding
           a property.
        """
        property_items = []
        for x in properties:
            try:
                property_items.append((x, getattr(self, x, None)))
            except Exception:
                logger.exception('Exception While finding property - %s', x)
        return property_items

    def get_found_properties(self, properties):
        return dict(filter(lambda x: x[1], self.find_properties(properties)))

    def to_dict(self):
        result = self.get_found_properties(self.general_properties)
        result[self.module_name] = self.get_found_properties(self.module_properties)
        return result

    def get_product_id(self):
        pass
