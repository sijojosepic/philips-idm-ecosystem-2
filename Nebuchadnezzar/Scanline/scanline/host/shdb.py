import logging
import requests
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class SHDBHostScanner(WindowsHostScanner):
    module_name = 'SHDB'

    def __init__(self, hostname, endpoint, username, password, product_id='SHDB',
            product_name='SHDB', shdb_service_port=8866, https_enabled=False, **kwargs):
        super(SHDBHostScanner, self).__init__(hostname, endpoint, username, password,
            product_id=product_id, product_name=product_name, product_version=self.get_version(hostname, shdb_service_port, https_enabled), **kwargs)
    
    @staticmethod
    def get_version(hostname, shdb_service_port, https_enabled):
        """get product details"""
        version = 'NA'
        proto = 'http'
        if https_enabled:
            proto = 'https'
        if not hostname:
            logger.error("Connection error - Missing hostname")
        uri = '{proto}://{hostname}:{port}/SEService/_status'.format(proto=proto, hostname=hostname, port=shdb_service_port)
        try:
            headers = {
              'accept': 'application/v1+json',
            }
            res = requests.get(uri, headers=headers, verify=False)
            res.raise_for_status()
            version = res.json().get('version')
        except Exception as e:
            logger.error("SHDB Service error %s : %s", hostname, e)
        return version
