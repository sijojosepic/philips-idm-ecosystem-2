import logging
from scanline.host.windows import WindowsHostScanner
from cached_property import cached_property
from scanline.utilities import config_reader


logger = logging.getLogger(__name__)


class ISEEUtilHostScanner(WindowsHostScanner):
    module_name = 'ISEEUtilityServer'
    general_properties = WindowsHostScanner.general_properties.union([
                                                                     'isee_backups'])
    IBE = ['ibe', 'ibeapp', 'ibedb', 'ibegd']
    ISCV = ['iscv', 'iscvgd']
    IECG = ['iecg', 'iecggd']
    DWP = ['dwp']
    XPERIM = ['xperim', 'xperimgd', 'xperconnect', 'xperdatacenter']
    ISPORTAL = ['isportal']
    CONCERTO = ['concerto']
    PRODUCT_MAPPING = {'ibe': IBE,
                       'iscv': ISCV,
                       'iecg': IECG,
                       'dwp': DWP,
                       'xperim': XPERIM,
                       'isportal': ISPORTAL,
                       'concerto': CONCERTO
                       }

    def __init__(self, hostname, endpoint, username, password,
                 domain=None, tags=None, **kwargs):
        super(ISEEUtilHostScanner, self).__init__(hostname, endpoint,
                                                  username, password,
                                                  domain, tags,
                                                  **kwargs)

    @cached_property
    def isee_backups(self):
        return self.extract_isee_backups()

    def find_product_name(self, mapping, prd_name):
        for name, scanner_names in mapping.iteritems():
            if prd_name in scanner_names:
                return name

    def extract_isee_backups(self):
        '''
            During Utility server discovery, back up needs to be
            added for only the ISEE products which are available
            in the site.
            This function identifies the ISEE products for the site,
            by using the discovery.yml's scanners list.
        '''
        products_set = set()
        scanners = config_reader.get_scanners()
        for scanner in scanners:
            prd_name = self.find_product_name(
                self.PRODUCT_MAPPING, scanner)
            if prd_name:
                products_set.add(prd_name)
        return list(products_set)
