import logging
from scanline.host.windows import WindowsScanner


logger = logging.getLogger(__name__)


class ISPHL7HostScanner(WindowsScanner):
    module_name = 'HL7'

    def __init__(self, hostname, endpoint, tags=None, product_id='HL7',
                 product_name='HL7', product_version='NA', **kwargs):
        super(ISPHL7HostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, **kwargs)
