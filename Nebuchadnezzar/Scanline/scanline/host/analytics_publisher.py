import logging
from scanline.host import HostScanner
from scanline.host.windows import WindowsHostScanner


logger = logging.getLogger(__name__)


class AnalyticsPublisherHostScanner(WindowsHostScanner):
    module_name = 'AnalyticsPublisher'
    module_properties = HostScanner.module_properties.union(['fact'])
    def __init__(self, hostname, endpoint, tags=None, product_id='Analytics', 
    	product_name='AnalyticsPublisher', product_version='NA', role='NA', **kwargs):
        super(AnalyticsPublisherHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, role=role, **kwargs)

    @property
    def fact(self):
        return 'ExampleAPFact'
