import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class RWSGDHostScanner(HostScanner):
    module_name = 'RWSGD'
