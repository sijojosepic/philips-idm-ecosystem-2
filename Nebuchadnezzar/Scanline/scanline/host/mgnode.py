import logging
from scanline.host.windows import WindowsScanner

logger = logging.getLogger(__name__)


class MGNodeHostScanner(WindowsScanner):
    module_name = 'MGNode'
    general_properties = WindowsScanner.general_properties.union(['one_way_trust', 'two_way_trust', 'time_sync'])

    def __init__(self, hostname, endpoint, username, password, tags=None, **kwargs):
        super(MGNodeHostScanner, self).__init__(hostname, endpoint, username, password, tags=tags, **kwargs)
        self.one_way_trust = 'yes' if kwargs.get('one_way_trust') == True else 'no'
        self.two_way_trust = 'yes' if kwargs.get('two_way_trust') == True else 'no'
        self.time_sync = 'yes' if kwargs.get('time_sync') == True else 'no'
