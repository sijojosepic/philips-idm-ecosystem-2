import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class CiscoSwitchHostScanner(HostScanner):
    module_name = 'CiscoSwitch'


class HPSwitchHostScanner(HostScanner):
    module_name = 'HPSwitch'
    module_properties = HostScanner.module_properties.union(['switch_model'])

    def __init__(self, hostname, endpoint, switch_model, **kwargs):
        super(HPSwitchHostScanner, self).__init__(hostname, endpoint, **kwargs)
        self.switch_model = switch_model
