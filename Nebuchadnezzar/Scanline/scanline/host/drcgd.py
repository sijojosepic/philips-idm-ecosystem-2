import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class DRCGDHostScanner(HostScanner):
    module_name = 'DRCGD'

