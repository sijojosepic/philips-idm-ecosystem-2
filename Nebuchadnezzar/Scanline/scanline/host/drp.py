import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class DRPHostScanner(HostScanner):
    module_name = 'DRPGD'
