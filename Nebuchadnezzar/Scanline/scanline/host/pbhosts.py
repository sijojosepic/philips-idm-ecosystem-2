import logging
from scanline.host.windows import WindowsHostScanner
from scanline.host import HostScanner

logger = logging.getLogger(__name__)


class PBWindowsHostScanner(WindowsHostScanner):
    module_name = 'PBWindows'

    def __init__(self, hostname, endpoint, tags=None, product_id='PBWindows',
                 product_name='PBWindows', product_version='NA', role='NA', **kwargs):
        super(PBWindowsHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, role=role, **kwargs)


class PBLinuxHostScanner(HostScanner):
    module_name = 'PBLinux'

    def __init__(self, hostname, endpoint, tags=None, product_id='PBLinux',
                 product_name='PBLinux', product_version='NA', role='NA', **kwargs):
        super(PBLinuxHostScanner, self).__init__(
            hostname, endpoint, tags=tags, product_id=product_id,
            product_name=product_name, product_version=product_version, role=role, **kwargs)
