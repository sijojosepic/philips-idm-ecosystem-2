import logging
from scanline.host import HostScanner


logger = logging.getLogger(__name__)


class I4GDHostScanner(HostScanner):
    module_name = 'I4GD'
