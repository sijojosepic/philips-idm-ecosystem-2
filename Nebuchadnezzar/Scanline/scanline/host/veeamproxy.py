from scanline.host import HostScanner

class VeeamProxyScanner(HostScanner):
    module_name = 'VEEAM_PROXY'