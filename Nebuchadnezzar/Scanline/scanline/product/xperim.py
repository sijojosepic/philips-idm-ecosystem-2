from scanline.product import ProductScanner, GenericProductScanner


class XPERIMGDProductScanner(GenericProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(XPERIMGDProductScanner, self).__init__(
            scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)

    def site_facts(self):
        facts = super(XPERIMGDProductScanner, self).site_facts()
        return self.segregate_credentials(facts)


class XPERIMProductScanner(ProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(XPERIMProductScanner, self).__init__(
            scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)

    def site_facts(self):
        facts = super(XPERIMProductScanner, self).site_facts()
        return self.segregate_credentials(facts)
