import json
from scanline.product import GenericProductScanner
from scanline.utilities.win_rm import execute_powershell_script


class NextGenProductScanner(GenericProductScanner):
    PS_SCRIPT = 'Get-Content -Path {0}'

    def __init__(self, scanner, address, username, password, tags=None, host_scanner=None, **kwargs):
        super(NextGenProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)
        self.address = address
        self.username = username
        self.password = password
        self.file_path = kwargs.get('file_path', '')

    def get_ps_scirpt(self):
        return self.PS_SCRIPT.format(self.file_path)

    def get_discovery_info(self):
        msg = ''
        if self.uri:
            return super(NextGenProductScanner, self).get_discovery_info()
        elif self.file_path:
            _discovery_info = execute_powershell_script(self.address, self.username, self.password, self.get_ps_scirpt())
            if _discovery_info:
                return json.loads(_discovery_info)
            msg = 'Failed to read the JSON information'
        msg = msg or 'Discovery url or File path not found'
        raise RuntimeError(msg)


class ISEEGDProductScanner(NextGenProductScanner):
    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(ISEEGDProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)

    def get_host_info(self, host):
        _scanner_obj = self.host_scanner(host['address'], self.endpoint, tags=self.tags,
                                         product_id=self.discovery_info.get('productid', ''),
                                         product_name=self.discovery_info.get('productname', ''),
                                         product_version=self.discovery_info.get('productversion', ''),
                                         role=self.get_host_role(host), scanner=self.endpoint['scanner'],
                                         username=self.username, password=self.password,
                                         environmentType=self.environmentType, **self.kwargs)
        return _scanner_obj.to_dict()
