import logging
import json
from collections import namedtuple
import requests
from scanline.product import ProductScanner

logger = logging.getLogger(__name__)

class HSOPHWScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, domain=None, tags=None, host_scanner=None, **kwargs):
        super(HSOPHWScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner,
                                                   product_id=None)
        self.username = username
        self.password = password
        self.address = address
        self.domain = domain
        self.scanner_name = scanner
        self.product_id = kwargs.get('product_id')
        self.product_name = kwargs.get('product_name')
        self.product_version = kwargs.get('product_version')

    def get_facts(self, hostname):
        facts = {}
        for host in hostname.split(','):
            host = host.lower()
            cluster_fact = {host: {self.scanner_name: {"endpoint": {"address": host, "scanner": self.scanner_name}},
                        "address": host, "modules": ["hardware", self.scanner_name], 'role' : 'NA',
                        "product_id": self.product_id, "product_name": self.product_name, "product_version": self.product_version}}
            facts.update(cluster_fact)       
        return facts

    def site_facts(self):
        facts = self.get_facts(self.address)
        return facts
