from scanline.product.extended import ExtendedProductScanner
import logging

logger = logging.getLogger(__name__)

class DatabaseProductScanner(ExtendedProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(DatabaseProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)
        self.dbpartner = kwargs.get('dbpartner', None)

    def site_facts(self):
        facts = super(DatabaseProductScanner, self).site_facts()
        for host_name, host_fact in facts.iteritems():
            if self.dbpartner:
                facts[host_name].update({'dbpartner': self.dbpartner})
            else:
                facts[host_name].update({'dbpartner': 'NA'})
        return self.segregate_credentials(facts)
