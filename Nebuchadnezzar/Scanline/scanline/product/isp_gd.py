import logging

from scanline.product import GenericProductScanner
from scanline.host import HostScanner
from requests.exceptions import RequestException, HTTPError, ConnectTimeout

logger = logging.getLogger(__name__)


class ISPGDHandler(object):
    # 'moudle_type - scanner' mapping, in order to reuse
    # the scanner configuration defined in the trinity.py
    GD_TYPES = {'I4': 'I4GD', 'Distradconnector': 'DRCGD', 'RWS': 'RWSGD', 'Distradprime': 'DRPGD'}
    URLs = {'I4': 'https://{0}/i4gd', 'RWS': 'https://{0}/ConfigurationService/api/configuration/discovery',
            'Distradconnector': 'https://{0}/DistRadNodeManager/v1/discover',
            'Distradprime': 'https://{0}/DistRadNodeManager/v1/discover'}
    HM_STS = '{0}_hmac_enabled'

    def __init__(self, hosts_n_types, scanner, address, tags=None, host_scanner=None, **kwargs):
        self.hosts_n_types = hosts_n_types
        self.scanner = scanner
        self.address = address
        self.tags = tags
        self.host_scanner = host_scanner
        self.kwargs = kwargs

    def get_scanners(self, module_type):
        scanner_name = self.GD_TYPES[module_type]
        # Getting scanner obj from trinity scanner definition
        sc_conf = self.kwargs.get('scanners')() if self.kwargs.get('scanners') else {}
        scanner = sc_conf.get(scanner_name) or {}
        product_scanner = scanner.get('product') or GenericProductScanner
        host_scanner = scanner.get('host') or HostScanner
        return product_scanner, host_scanner

    def _hmac_enabled(self, module_type):
        # Generic Discovery by default HMAC authorized
        # During ISP extended Discovery, if it requires a product
        # can disable HMAC autorization during GD by setting
        # [module_type]_hmac_enabled to 'no' in the endpoint configuration
        # Eg - rws_hmac_enabled: no
        # Case insensitive
        hmac_str = self.HM_STS.format(module_type).upper()
        hmac_enable = self.kwargs.get(hmac_str) or self.kwargs.get(hmac_str.lower())
        return True if hmac_enable is None else hmac_enable

    def _url(self, module_type, host):
        # In lhost.yml endpoint definition url can be given
        # by adding _URL suffix with module_type
        # eg : I4_URL, RWS_URL
        name = '{0}_URL'.format(module_type.upper())
        url = self.kwargs.get(name) or self.kwargs.get(name.lower())
        return url or self.URLs.get(module_type, '').format(host)

    def discover_all(self):
        facts = {}
        exclude_modules = []
        for host, module_type in self.hosts_n_types:
            if module_type in self.GD_TYPES:
                prd_scanner, host_scanner = self.get_scanners(module_type)
                url = self._url(module_type, host)
                obj = prd_scanner(scanner=self.scanner, address=self.address, tags=self.tags,
                                  host_scanner=host_scanner, discovery_url=url,
                                  hmac_enabled=self._hmac_enabled(module_type),
                                  **self.kwargs)
                try:
                    _site_fact = obj.site_facts()
                    facts.update(_site_fact)
                    if not _site_fact:
                        exclude_modules.append(self.GD_TYPES[module_type])
                except RequestException as e:
                    # During discovery discovery_purge_missing function in the Backoffice fact.py, removes all hosts
                    # information which are previously discovered and missing with present discovery.
                    # But in case of extended discovery upon GD API failure the hosts information needs to be retained.
                    # exclude_modules variable will be updated with module information, when it fails to retrieve
                    # information from a GDAPI. So that it will be ignored from purging. Moreover in order to delete
                    # the hosts cfg files, of a particular GD type it can be removed from iSiteWeb extended discovery.
                    exclude_modules.append(self.GD_TYPES[module_type])
                    msg = '''{0} Extended discovery failed for {1} : {2} '''.format(module_type, host, str(e))
                    logger.exception(msg)
            else:
                logger.info('Module_type - %s, is not defined in the class ISPGDHandler' % module_type)
        return facts, exclude_modules
