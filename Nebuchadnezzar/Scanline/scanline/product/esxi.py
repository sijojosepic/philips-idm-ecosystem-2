import logging
from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim
from scanline.product import ProductScanner
from scanline.host.esxi import ESXiHostScanner


logger = logging.getLogger(__name__)


class ESXiProductScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, tags=None, host_scanner=None, product_id=None, **kwargs):
        super(ESXiProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, product_id=None, **kwargs)
        self.username = username
        self.password = password
        self.product_id = product_id
        self.service_instance = None

    def get_hosts(self):
        try:
            self.service_instance = connect.SmartConnect(host=self.address, user=self.username, pwd=self.password)
            content = self.service_instance.RetrieveContent()
            container_view = content.viewManager.CreateContainerView(
                container=content.rootFolder, type=[vim.HostSystem], recursive=True
            )
            for child in container_view.view:
                yield child
        except vmodl.MethodFault as error:
            logger.error('Caught vmodl fault : %s', error.msg)
        finally:
            connect.Disconnect(self.service_instance)

    def get_formatted_hostname(self, result):
        search_str = '(vim.host.DnsConfig)'
        result = result[result.find(search_str)+len(search_str):]
        result = result[:result.find('}')+1]
        result = [line[:-1].split('=')[1].strip().strip("''") for line in result.split('\n') if "hostName" in line or "domainName" in line]
        hostname = '.'.join(result).strip('.')
        return hostname

    def get_hostname(self, host):
        options = vmodl.query.PropertyCollector.RetrieveOptions()
        pc = self.service_instance.content.propertyCollector
        filter_spec = self.create_filter_spec(host, 'config')
        result = str(pc.RetrievePropertiesEx([filter_spec], options))
        hostname = self.get_formatted_hostname(result)
        if not hostname or 'localhost' in hostname:
            return ESXiHostScanner.get_hostname(host)
        else:
            return hostname

    def create_filter_spec(self, vm, prop):
        objSpec = vmodl.query.PropertyCollector.ObjectSpec(obj=vm)
        filterSpec = vmodl.query.PropertyCollector.FilterSpec()
        filterSpec.objectSet = [objSpec]
        propSet = vmodl.query.PropertyCollector.PropertySpec(all=False)
        propSet.type = vim.HostSystem
        propSet.pathSet = [prop]
        filterSpec.propSet = [propSet]
        return filterSpec

    def scan_host(self, host):
        esxi_scanner = ESXiHostScanner(vmware_host=host, endpoint=self.endpoint, tags=self.tags)
        result = esxi_scanner.to_dict()
        if self.host_scanner is not None:
            params = dict(
                hostname=self.get_hostname(host),
                endpoint=self.endpoint,
                username=self.username,
                password=self.password,
                tags=self.tags
            )
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
            result.update({"product_id": self.product_id})
        return result
