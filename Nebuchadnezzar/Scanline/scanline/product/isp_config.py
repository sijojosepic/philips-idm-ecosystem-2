import logging
from lxml import objectify, etree
from cached_property import cached_property
import requests
import zlib
import re
import json
from scanline.utilities.http import HTTPRequester
from operator import attrgetter

logger = logging.getLogger(__name__)


class ISPAuthenticationError(Exception):
    pass


class ISPConfiguration(HTTPRequester):
    auth_scheme = 'https'
    config_scheme = 'http'
    url_format = '{scheme}://{server}/{path}'
    UDM_SERVICES_URL = "https://{0}/UDMServices/Configuration/UDMPROCESSANDSERVICES/true"

    def __init__(self, server):
        super(ISPConfiguration, self).__init__()
        self.server = server
        self.E = objectify.ElementMaker(annotate=False)
        self.host_xml_object = None

    @property
    def config_url(self):
        return self.url_format.format(scheme=self.config_scheme, server=self.server, path=self.config_path)

    @property
    def auth_url(self):
        return self.url_format.format(scheme=self.auth_scheme, server=self.server, path=self.auth_path)

    @cached_property
    def software_version(self):
        return self.get_software_version()

    def get_list_hosts_query(self):
        """Generate
    <ListHosts/>"""
        return etree.tostring(self.E.ListHosts())

    def get_server_info_query(self):
        """Generate
    <GetServerInfo/>"""
        return etree.tostring(self.E.GetServerInfo())

    def get_host_config_query(self, hostname, config):
        """Generate
    <Retrieve>
        <Host>%s</Host>
        <ConfigurationName>%s</ConfigurationName>
    </Retrieve>"""
        root = self.E.Retrieve(
            self.E.Host(hostname),
            self.E.ConfigurationName(config)
        )
        return etree.tostring(root)

    def get_auth_header(self, token):
        return dict(Cookie='iSiteWebApplication={token}'.format(token=token))

    def auth_session(self):
        if not self._session:
            token = self.get_auth_token()
            if not token:
                raise ISPAuthenticationError
            self._session = requests.session()
            self._session.headers.update(self.get_auth_header(token))
        return self._session

    def reset_auth_session(self):
        logger.info('Resetting Auth Session')
        self._session = None
        self.auth_session()

    def query_xml(self, url, query):
        response = self.suppressed_post(url=url, data=query)
        if response:
            return objectify.fromstring(response.content)

    def get_host_config(self, hostname):
        path = 'iSyntaxServer\\ProcessesAndServices'
        return self.query_host_config(hostname, path)

    def get_udm_services_info(self, hostname):
        response = self.suppressed_get(self.UDM_SERVICES_URL.format(hostname))
        return json.loads(response.content) if response else None

    def get_input_folder(self, hostname):
        system_config = self.query_host_config(
            hostname, 'iSyntaxServer\\iSiteSystem')
        result = system_config.get_configuration_attribute(
            'Stack', 'DICOMInputDirectory')
        if result is not None:
            return str(result)

    def get_federation_info(self):
        """
            Federation configuration has to be
            read from infra node or master.isyntax.server
        """
        try:
            federation_info = self.query_host_config(
                self.server.lower(), 'iSyntaxServer\\FederationConfiguration')
            return int(federation_info.get('EnableFederation'))
        except Exception:
            logger.exception('Error fetching FederationConfiguration')

    def query_host_config(self, hostname, config):
        query = self.get_host_config_query(hostname, config)
        root = self.query_xml(self.config_url, query)
        return ISPHostConfiguration(data=root['Configuration'], host=hostname)

    def get_software_version(self):
        self.auth_session()
        server_info = self.query_xml(
            self.config_url, self.get_server_info_query())
        try:
            return str(server_info.ServerInfoBlob.ServerInfo.SoftwareVersion)
        except AttributeError:
            pass

    def get_hosts(self):
        self.auth_session()
        hosts_xml = self.query_xml(
            self.config_url, self.get_list_hosts_query())
        return hosts_xml.xpath('//HostName')

    def get_noncore_node(self):
        return None

    def logout(self):
        pass

    def __del__(self):
        self.logout()


class ISPHostConfiguration(object):
    XML_DESCRIPTOR_RE = '^<\\?\\s*xml.*\\?>\r?\n'

    def __init__(self, data, host):
        self.data = data
        self.host = host

    @staticmethod
    def decode_inflate(stream):
        stream_decoded = repr(stream).decode('base64')
        # the first four bytes are the length (Compression.cs)
        # uncompressed_length = struct.unpack('<I', stream_decoded[:4])[0]
        try:
            result = zlib.decompress(stream_decoded[4:])
        except zlib.error:
            result = ''
            logger.exception('Could not decompress')
        return result

    def remove_xml_descriptor(self, data):
        return re.sub(self.XML_DESCRIPTOR_RE, '', data, 1)

    @cached_property
    def config_blob(self):
        logger.debug('Decompressing config for host %s', self.host)
        return self.remove_xml_descriptor(self.decode_inflate(self.data))

    @cached_property
    def config_root(self):
        logger.debug('Parsing config for host %s', self.host)
        try:
            result = objectify.fromstring(self.config_blob)
        except etree.XMLSyntaxError:
            logger.exception(
                'Configuration for host %s could not be parsed', self.host)
            raise
        return result

    def get(self, attr, default=None):
        return getattr(self.config_root, attr, default)

    def get_configuration_attribute(self, config, attr):
        attribute_getter = attrgetter(attr)
        try:
            return attribute_getter(self.get(config))
        except AttributeError:
            logger.info('Host %s could not find attribute %s from configuration %s', self.host, attr, config)
