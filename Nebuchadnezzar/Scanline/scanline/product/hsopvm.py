import logging
import json
from collections import namedtuple
import requests
from scanline.product import ProductScanner

logger = logging.getLogger(__name__)


class HSOPVMProductScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, domain=None,
                 tags=None, host_scanner=None, **kwargs):
        super(HSOPVMProductScanner, self).__init__(scanner, address, tags=tags,
                                                   host_scanner=host_scanner,
                                                   product_id=None)
        self.username = username
        self.password = password
        self.address = address
        self.domain = domain
        self.scanner_name = scanner
        self._api_session_token = None
        self.product_id = kwargs.get('product_id')
        self.product_name = kwargs.get('product_name')
        self.product_version = kwargs.get('product_version')

    def get_token_payload(self):
        payload = {'auth': {'identity': {'methods': ['password'], 'password': {
            'user': {'name': self.username, 'password': self.password,
                     'domain': {'name': self.domain}}}},
                            'scope': {'domain': {'name': self.domain}}}}

        headers = {'content-type': 'application/json'}
        body = json.dumps(payload)
        TokenPayload = namedtuple('TokenPayload', 'headers body')
        token_payload = TokenPayload(headers, body)
        return token_payload

    def get_token(self, token_payload):
        try:
            url = 'https://{0}/api/v2/identity/auth'.format(self.address)
            response = requests.post(url, verify=False,
                                     headers=token_payload.headers,
                                     data=token_payload.body)
            return response.headers['x-subject-token']
        except Exception:
            raise Exception('Token generation failed')

    def get_product_id(self, token):
        try:
            url = 'https://{0}/api/v2/identity/users/myself/projects'.format(
                self.address)
            headers = {'content-type': 'application/json',
                       'x-auth-token': token}
            response = requests.get(url, headers=headers, verify=False)
            result = response.json()
            product_id = None

            for product_details in result:
                if product_details['domain_name'] == self.domain:
                    product_id = product_details['id']

            if not product_id:
                raise Exception('No corresponding product found')
            return product_id
        except Exception:
            raise Exception('No corresponding product found')

    def get_product_id_from_response(self, response):
        product_id = None
        for product_details in response:
            if product_details['domain_name'] == self.domain:
                product_id = product_details['id']
        return product_id

    def get_product_token_payload(self, token, product_id):
        payload = {
            'auth': {'identity': {'methods': ['token'],
                                  'token': {'id': token}},
                     'scope': {'project': {'id': product_id}}}}
        headers = {'content-type': 'application/json'}
        body = json.dumps(payload)
        TokenPayload = namedtuple('TokenPayload', 'headers body')
        token_payload = TokenPayload(headers, body)
        return token_payload

    def discover_hsop_vms(self):
        return self._get_api('/api/v2/VMs')

    def get_facts(self, result):
        facts = {}
        for vm_details in result:
            if vm_details['networks'] and vm_details['status'] == 'active':
                os_type = None
                if vm_details['tags'] is not None:
                    os_type = self.get_ostype(vm_details['tags'])
                facts[vm_details['networks'][0]['address']] = {
                    self.scanner_name: {"endpoint":
                                            {"address": self.address,
                                             "scanner": self.scanner_name}},
                    "address": self.address,
                    "modules": ["linux", self.scanner_name],
                    "product_id": self.product_id, "product_name": self.product_name,
                    "product_version": self.product_version,
                    "role": "NA",
                    "uuid": vm_details['id'],
                    "OS": os_type,
                    "floating_ip": self._get_floating_ip(vm_details['networks']),
                    "cluster_check": False
                }
        cluster_fact = {self.address:
                            {self.scanner_name:
                                {"endpoint": {"address": self.address,
                                              "scanner": self.scanner_name}},
                                "address": self.address,
                                "modules": ["linux", self.scanner_name],
                                "product_id": self.product_id,
                                "product_name": self.product_name,
                                "product_version": self.product_version,
                                "role":"NA",
                                "cluster_check": True}}
        facts.update(cluster_fact)
        return facts

    def site_facts(self):
        token_payload = self.get_token_payload()
        token = self.get_token(token_payload)
        product_id = self.get_product_id(token)
        logger.info('HSOP:product_id: ' + str(product_id))
        token_payload = self.get_product_token_payload(token, product_id)
        self._api_session_token = self.get_token(token_payload)
        result = self.discover_hsop_vms()
        hsop_vms_data = self.get_facts(result)
        return hsop_vms_data

    def get_ostype(self, tags):
        try:
            for tag in tags:
                if 'OS' in tag:
                    os_type = tag.split("=")[-1]
                    return os_type
        except Exception:
            raise Exception('GET OS TYPE Tag Failed')

    def _get_api(self, urn):
        url = 'https://' + self.address + urn
        headers = {'content-type': 'application/json',
                   'x-auth-token': self._api_session_token}
        logger.info('HSOP:Invoking URL = ' + url)
        return requests.get(url, headers=headers, verify=False).json()

    def _get_floating_ip(self, vm_networks):
        """
        Find the floating ip(public ip) of the vm
        :param vm_networks: example = [{'address': '10.11.14.34',
                'id': '29fe70c6-18d2-46e3-b6cb-54eef3dcf414',
                'port_id': '80b4d0f0-042a-4cf9-9948-6fdcd81aa5ce'}]
        :return: floating ip (str) or None (by function default)
        """
        port_ip = self._get_port_ip()
        for port_id in self._get_port_ids(vm_networks):
            try:
                return port_ip[port_id]
            except KeyError:
                continue

    def _get_port_ip(self):
        """
        Invoke stratoscale API to get floating ips data. Map the port_id to
        floating ip
        :return: example = {None: u'130.147.86.164',
                            u'126376b9-4d45-4258-aafa-67d690c5a214':
                                                            u'130.147.86.168'}
        """
        port_ip = {}
        response = self._get_api('/api/openstack/networking/v2.0/floatingips')
        for fip in response['floatingips']:
            port_ip[fip['port_id']] = fip['floating_ip_address']
        return port_ip

    def _get_port_ids(self, vm_networks):
        """
        Extract list of port ids of a vm
        :param vm_networks: example = [{'address': '10.11.14.34',
                'id': '29fe70c6-18d2-46e3-b6cb-54eef3dcf414',
                'port_id': '80b4d0f0-042a-4cf9-9948-6fdcd81aa5ce'}]
        :return: example = ['80b4d0f0-042a-4cf9-9948-6fdcd81aa5ce']
        """
        port_ids = []
        for network in vm_networks:
            try:
                if network['port_id'] is not None:
                    port_ids.append(network['port_id'])
            except KeyError:
                continue
        return port_ids
