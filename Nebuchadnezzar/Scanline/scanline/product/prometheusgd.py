import logging
from scanline.product import GenericProductScanner
logger = logging.getLogger(__name__)
class PrometheusProductScanner(GenericProductScanner):
    def scan_host(self, host):
        host['address'] = host.get('name') if host.get('name') else host.pop('address')
        logger.info('Gathering host attributes from {address}'.format(
            address=host['address']))
        result = self.get_host_info(host)
        if not result:
            return
        result.update({'osType': host.get('osType'),
            'prometheusAgents': host.get('prometheusAgents')})
        logger.info('final result is {result} from {address}'.format(
            result=result, address=host['address']))
        return result