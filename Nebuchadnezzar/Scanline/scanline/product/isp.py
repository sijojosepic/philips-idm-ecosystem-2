import logging
import linecache
from lxml import objectify, etree
from datetime import datetime, timedelta
from dateutil import parser
from cached_property import cached_property
import pytz
from scanline.component import localhost
from scanline.product import ProductScanner
from scanline.host.isp import ISPHostScanner, ExtendedISPHost
from scanline.host.windows import WindowsHostScanner
from scanline.host.udm import UDMRedisHostScanner
from scanline.utilities.hsdp_obs import OBJSTConnection
from scanline.utilities.auth import get_hashkey
from phimutils.resource import NagiosResourcer
from scanline.utilities.rhapsody_stats import RhapsodyStats
from scanline.utilities.win_rm import WinRM
from scanline.product.isp_gd import ISPGDHandler
from scanline.utilities.dns import get_address
from scanline.product.isp_config import (ISPConfiguration,
                                         ISPAuthenticationError)


logger = logging.getLogger(__name__)

DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}


class ISPProductScanner(ProductScanner):
    def __init__(self, scanner, address, username, password, domain=None, tags=None, host_scanner=None, product_id=None,
                 **kwargs):
        super(ISPProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner,
                                                product_id=product_id, **kwargs)
        self.username = username
        self.password = password
        self.domain = domain
        self.product_id = product_id
        self.ispdb = ISPDBConfiguration(self.address)
        self.ipv6_enabled = kwargs.get('ipv6_enabled', False)
        self.kwargs = kwargs
        # Fetch RabbitMQ cluster hostname from discovery.yml.
        # else use 'rabbitmq-server.isyntax.net' as default value if rabbitmq_cluster_host is not configured.
        self.rabbit_cluster_host = kwargs.get('rabbitmq_cluster_host', 'rabbitmq-server.isyntax.net')

    @cached_property
    def isp(self):
        return self.create_isp(self.address)

    @cached_property
    def is_distrad_prime(self):
        return self.is_distrad_prime_env()

    @cached_property
    def secret(self):
        return linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()

    @property
    def product_version(self):
        if self.isp:
            return self.isp.software_version

    @staticmethod
    def create_isp(address):
        isp_classes = [ISP3XConfiguration, ISP4_1Configuration,
                       ISP4XConfiguration, ISP4XHiSecConfiguration]
        while isp_classes:
            isp_class = isp_classes.pop()
            result = isp_class(address)
            logger.info('Trying %s as %s', address,
                        result.__class__.__name__)
            try:
                sw_version = result.software_version
                if sw_version:
                    return result
            except ISPAuthenticationError:
                logger.info('Could not authenticate to %s as %s',
                            address, result.__class__.__name__)
        logger.info('Could not create ISP for %s', address)

    def get_hosts(self):
        hosts = self.isp.get_hosts() if self.isp else []
        return iter(hosts)

    def get_dbhosts(self):
        hosts = self.ispdb.non_gd_hosts() if self.ispdb else []
        return iter(hosts)

    def get_redis_hosts(self):
        redis_nodes = []
        try:
            db_obj = OBJSTConnection()
            json_obj = db_obj.to_dict()
            redis_config = json_obj['RedisConfig']
            if redis_config['Enabled']:
                for redis_node in redis_config['RedisEndPoints']:
                    redis_nodes.append(redis_node['ClusterIP'])
        except Exception:
            pass
        return iter(redis_nodes)

    def get_hostname(self, host):
        return str(host)

    def is_distrad_prime_env(self):
        """ This method will set 'is_distrad_prime' 'yes' if module type "Distradprime" is available
        else it will set is_distrad_prime to 'no'."""
        if hasattr(self, 'isp') and self.isp.__class__.__name__ == 'ISP4XHiSecConfiguration':
            for host, module_type in self.ispdb.hosts_with_type:
                if 'Distradprime' == module_type:
                    return 'yes'
        return 'no'

    def scan_host(self, host):
        hostname = self.get_hostname(host)
        isp_scanner = ISPHostScanner(
            hostname=hostname,
            endpoint=self.endpoint,
            isp=self.isp,
            tags=self.tags,
            ipv6_enabled=self.ipv6_enabled,
            username=self.username,
            password=self.password,
            secret=self.secret,
            is_distrad_prime=self.is_distrad_prime,
            **self.kwargs
        )
        result = isp_scanner.to_dict()
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=isp_scanner.username,
            password=isp_scanner.password,
            domain=self.domain,
            tags=self.tags,
            ipv6_enabled=self.ipv6_enabled
        )
        windows_scanner = WindowsHostScanner(**params)
        windows_facts = windows_scanner.to_dict()
        windows_facts.update(result)
        result = windows_facts
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        return result

    def scan_dbhost(self, host):
        hostname = self.get_hostname(host)
        isp_scanner = ExtendedISPHost(
            hostname=hostname,
            endpoint=self.endpoint,
            isp=self.ispdb,
            tags=self.tags,
            username=self.username,
            password=self.password,
            secret=self.secret,
            is_distrad_prime=self.is_distrad_prime,
            **self.kwargs
        )
        result = isp_scanner.to_dict()
        result['product_version'] = self.product_version
        result['ISP']['version'] = self.isp.software_version
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=isp_scanner.username,
            password=isp_scanner.password,
            domain=self.domain,
            tags=self.tags
        )
        windows_scanner = WindowsHostScanner(**params)
        windows_facts = windows_scanner.to_dict()
        windows_facts.update(result)
        result = windows_facts
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        scanner_name = result.get('Windows', {}).get('endpoint', {}).get('scanner', '')
        module_type = result.get(scanner_name, {}).get('module_type', '')
        if str(module_type).upper() == 'HL7':
            rhapsodystats = RhapsodyStats()
            rhapsody_components = rhapsodystats.get_rhapsody_components(host)
            if rhapsody_components:
                result.update({'rhapsody_components': rhapsody_components})
        return result

    def scan_redis_host(self, host):
        hostname = self.get_hostname(host)
        redis_scanner = UDMRedisHostScanner(
            hostname=hostname,
            endpoint=self.endpoint,
            tags=self.tags
        )
        result = redis_scanner.to_dict()
        result['product_version'] = self.product_version
        params = dict(
            hostname=hostname,
            endpoint=self.endpoint,
            username=self.username,
            password=self.password,
            domain=self.domain,
            tags=self.tags
        )
        result.update({"product_id": self.product_id})
        if self.host_scanner is not None:
            other_scanner = self.host_scanner(**params)
            result.update(other_scanner.to_dict())
        return result

    def get_dbhost_facts(self, host):
        result = self.scan_dbhost(host)
        if result:
            result['modules'] = self.get_present_modules(result)
        return result

    def get_redis_host_facts(self, host):
        result = self.scan_redis_host(host)
        result['modules'] = self.get_present_modules(result)
        return result

    def segregate_credentials(self, facts):
        """
            Segregating the credentilas from each host's fact, in order to avoid,
            getting it written to the facts collection at the backoffice.
        """
        credentials = dict((k, v.pop('credentials', {})) for k, v in facts.iteritems())
        if credentials:
            facts.update({'credentials': credentials})
        return facts

    def rabbitmq_cluster_manager(self, role, ISP):
        return self.compose_cluster_facts(self.rabbit_cluster_host, self.rabbit_cluster_host, self.rabbit_cluster_host,
                                          role, ISP)

    def do_generic_discovery(self, hosts_n_types):
        handler = ISPGDHandler(hosts_n_types, self.endpoint['scanner'],
                               self.address, self.tags,
                               **self.kwargs)
        return handler.discover_all()

    def get_clusterdb_credentials(self, isp_scanner, role):
        """ This method will return encrypted credential for cluster node.
        If it is dbcluster then it will return ecrypted dbuser and dbpassword """
        _credentials = {}
        if self.username:
            _credentials['username'] = isp_scanner.encrypter.encrypt(
                isp_scanner.format_username(self.username))
        if self.password:
            _credentials['password'] = isp_scanner.encrypter.encrypt(self.password)
        if role == 'dbcluster':
            if self.kwargs.get('dbuser'):
                _credentials['dbuser'] = isp_scanner.encrypter.encrypt(
                    self.kwargs.get('dbuser'))
            if self.kwargs.get('dbpassword'):
                _credentials['dbpassword'] = isp_scanner.encrypter.encrypt(
                    self.kwargs.get('dbpassword'))
        return _credentials

    def compose_cluster_facts(self, cluster_name, cluster_host, address, role, ISP=None):
        cluster_json = {}
        try:
            cluster_json[cluster_host] = {}
            cluster_json[cluster_host]['cluster_name'] = cluster_name
            cluster_json[cluster_host]['ISP'] = ISP or {}
            cluster_json[cluster_host]['address'] = address
            cluster_json[cluster_host]['role'] = role
            cluster_json[cluster_host]['product_id'] = self.product_id
            cluster_json[cluster_host]['product_version'] = self.product_version
            isp_scanner = ISPHostScanner(
                hostname=cluster_host,
                endpoint=self.endpoint,
                isp=self.ispdb,
                tags=self.tags,
                username=self.username,
                password=self.password,
                secret=self.secret,
                **self.kwargs
            )
            cluster_json[cluster_host]['credentials'] = self.get_clusterdb_credentials(isp_scanner, role)
            if role == 'dbcluster' and 'enable_billing' in self.kwargs:
                cluster_json[cluster_host]['enable_billing'] = 'yes' if self.kwargs.get(
                    'enable_billing') == True else 'no'

        except Exception:
            logger.exception('Error while generation Cluster Json for %s', cluster_host)
        return cluster_json

    def cluster_win_rm_request(self, cluster_nodes):
        for node in cluster_nodes:
            try:
                win_rm = WinRM(node, self.username, self.password)
                powershell_output = win_rm.execute_ps_script('Get-Cluster | Write-Host')
                if powershell_output.std_out:
                    cluster_name = powershell_output.std_out.strip()
                    return cluster_name
            except Exception:
                logger.exception('Error while getting cluster_name for the host %s', node)
        return None

    def check_cluster(self, cluster_nodes, role):
        cluster_name = self.cluster_win_rm_request(cluster_nodes)
        if cluster_name:
            domain_name = '.'.join(self.address.split('.')[1:])
            cluster_host = cluster_name + '.' + domain_name
        else:
            return None
        return self.compose_cluster_facts(cluster_name, cluster_host, get_address(cluster_host), role)

    def add_cluster(self, cluster_nodes, clustercheck, role):
        if clustercheck >= 2:
            cluster_json = self.check_cluster(cluster_nodes, role)
            return cluster_json
        else:
            return None

    def add_infra_high_availability(self, result):
        """Infra node High availability is implemented with UDM version 2.1, that is ISPACS 4.5.0.0 on wards.
        Prior to 4.5.0.0. versions of ISPACS, even if there are more than one instances of infra nodes is
        running, still it should not be considered as HA, but individual infra set up instead."""
        ha_check = False
        infra_module_code = 128
        cluster_nodes_infra = [node for node in result.keys() if
                               result[node].get('ISP', {}).get('module_type') == infra_module_code]
        if self.product_version.startswith('4,5'):
            try:
                if len(cluster_nodes_infra) >= 2:
                    cluster_json_infra = self.compose_cluster_facts(
                        'infra-ha',
                        self.address.lower(),
                        get_address(self.address),
                        role='infra-high-availability'
                    )
                    if cluster_json_infra:
                        result.update(cluster_json_infra)
                        ha_check = True
            except Exception as e:
                logger.exception('Error while checking infra node high availability')
        if not ha_check:
            for node in cluster_nodes_infra:
                result[node]['ISP']['infra_high_availability_check'] = False
        return result

    def site_facts(self):
        result = super(ISPProductScanner, self).site_facts()
        result = dict(
            (hostname.lower(), attribute) for hostname, attribute in
            result.iteritems())
        result = self.add_infra_high_availability(result)
        exclude_modules = None
        if hasattr(self,
                   'isp') and self.isp.__class__.__name__ == 'ISP4XHiSecConfiguration':
            noncore_dict = dict((self.get_hostname(host),
                                 self.get_dbhost_facts(host))
                                for host in self.get_dbhosts())
            noncore_dict = dict(
                (hostname.lower(), attribute) for hostname, attribute in
                noncore_dict.iteritems())
            dbclustercheck = 0
            hl7clustercheck = 0
            rabit_cluster_counts = 0
            cluster_nodes_rabbitmq = list()
            cluster_nodes = list()
            cluster_nodes_hl7 = list()
            for node in noncore_dict.keys():
                if noncore_dict[node]:
                    noncore_dict[node]['ISP']['identifier'] = '4x'
                    if node in result.keys():
                        noncore_isp = noncore_dict[node]['ISP']
                        result_isp = result[node]['ISP']
                        noncore_isp['ex_module_type'] = noncore_isp['module_type']
                        noncore_isp['module_type'] = result_isp['module_type']
                        noncore_isp['version'] = result_isp['version']
                        noncore_isp['input_folder'] = result_isp['input_folder']
                        noncore_isp['custom_process'] = result_isp['custom_process']
                        if result_isp.get('udm_process'):
                            noncore_isp['udm_process'] = result_isp['udm_process']
                        if 'infra_high_availability_check' in result_isp:
                            noncore_isp['infra_high_availability_check'] = result_isp.get(
                                'infra_high_availability_check')
                    isp_attr = noncore_dict[node].get('ISP')
                    if isp_attr:
                        module_type = isp_attr.get('module_type')
                        if module_type == 'Database':
                            dbclustercheck = dbclustercheck + 1
                            noncore_dict[node]['ISP']['dbclustercheck'] = False
                            cluster_nodes.append(node)
                        if module_type == 'Hl7':
                            hl7clustercheck = hl7clustercheck + 1
                            cluster_nodes_hl7.append(node)
                        if module_type == 'Rabbitmqnode':
                            rabit_cluster_counts = rabit_cluster_counts + 1
                            cluster_nodes_rabbitmq.append(node)
                    else:
                        logger.warning('ISP attribute is missing for the node -%s', node)
                else:
                    logger.warning('%s has no facts data(if it is HL7 check for Rhapsody API)', node)
                    del noncore_dict[node]

            cluster_json_db = self.add_cluster(cluster_nodes, dbclustercheck, role='dbcluster')
            if cluster_json_db:
                for node in cluster_nodes:
                    noncore_dict[node]['ISP']['dbclustercheck'] = True
                noncore_dict.update(cluster_json_db)
            cluster_json_hl7 = self.add_cluster(cluster_nodes_hl7, hl7clustercheck, role='hl7cluster')
            if cluster_json_hl7:
                noncore_dict.update(cluster_json_hl7)
            else:
                for node in cluster_nodes_hl7:
                    noncore_dict[node]['ISP']['hl7clustercheck'] = False
            if rabit_cluster_counts >= 2:
                endpoint = noncore_dict[cluster_nodes_rabbitmq[0]]['ISP']['endpoint'].copy()
                ISP = {'endpoint': endpoint}
                cluster_json_rabbitmq = self.rabbitmq_cluster_manager(role='rabbitmqcluster', ISP=ISP)
                noncore_dict.update(cluster_json_rabbitmq)
                for node in cluster_nodes_rabbitmq:
                    noncore_dict[node]['ISP']['rabbitclustercheck'] = True
            else:
                for node in cluster_nodes_rabbitmq:
                    noncore_dict[node]['ISP']['rabbitclustercheck'] = False

            result.update(noncore_dict)
            generic_discovery_facts, exclude_modules = self.do_generic_discovery(self.ispdb.gd_hosts())
            result.update(generic_discovery_facts)
        result.update(dict((self.get_hostname(host),
                            self.get_redis_host_facts(host))
                           for host in self.get_redis_hosts()))
        result = self.segregate_credentials(result)
        if exclude_modules:
            result.update({'exclude_modules': exclude_modules})
        return result


class ISP4XConfiguration(ISPConfiguration):
    auth_path = 'InfrastructureServices/AuthenticationService/v1_0/authenticationservice.ashx'
    config_path = 'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
    identifier = '4x'

    TICKET_XPATH = '//*[ local-name() = "Ticket" ]'
    CREDENTIALS = {}

    def __init__(self, server):
        super(ISP4XConfiguration, self).__init__(server)
        self.auth_login_request_ns = 'uri://stentor.com/iSite/Authentication/Messages'
        self.auth_authentication_source = 'ISITE'
        # self.auth_username = 'Administrator'
        # self.auth_password = 'changeme'
        self.auth_application_name = 'iSiteServer'
        self.auth_application_version = '1.0.0.0'
        self.auth_culture = '1033'
        self.auth_uiculture = '1033'
        self.localhost = localhost.LocalHost()
        self.load_credentials()

    @staticmethod
    def load_credentials():
        """ Load the username(USER111) and password(USER112) from the resource.cfg file
            Which will in turn use for accessing the API end point
        """
        secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
        nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
        username, password = nr.get_resources("$USER111$", "$USER112$")
        ISP4XConfiguration.CREDENTIALS["username"] = username
        ISP4XConfiguration.CREDENTIALS["password"] = password

    @property
    def auth_username(self):
        return self.CREDENTIALS["username"]

    @property
    def auth_password(self):
        return self.CREDENTIALS["password"]

    def logout_query(self):
        """
        <Message><Logout>
        <HostName>ingbtcpic5dt9ah.code1.emi.philips.com</HostName>
        <IpAddress>161.85.27.158</IpAddress>
        </Logout></Message>
        """
        message = self.E.Message(
            self.E.Logout(
                self.E.HostName(self.localhost.hostname),
                self.E.IpAddress(self.localhost.ip_address)
            )
        )
        return etree.tostring(message)

    def logout(self):
        self.query_xml(self.auth_url, self.logout_query())

    def get_authentication_query(self):
        """generate
    <Message><Login><loginRequest xmlns="uri://stentor.com/iSite/Authentication/Messages">
    <AuthenticationSource>ISITE</AuthenticationSource>
    <UserName>Administrator</UserName>
    <Password>changeme</Password>
    <HostName>ingbtcpic5dt9ah.code1.emi.philips.com</HostName>
    <IpAddress>161.85.27.158</IpAddress>
    <ApplicationName>iSiteServer</ApplicationName>
    <ApplicationVersion>1.0.0.0</ApplicationVersion>
    <Culture>1033</Culture>
    <UICulture>1033</UICulture>
    </loginRequest>
    </Login>
    </Message>"""
        E = self.E
        E2 = objectify.ElementMaker(annotate=False,
                                    namespace=self.auth_login_request_ns,
                                    nsmap={None: self.auth_login_request_ns})
        message = E.Message(
            E.Login(
                E2.loginRequest(
                    E.AuthenticationSource(self.auth_authentication_source),
                    E.UserName(self.auth_username),
                    E.Password(self.auth_password),
                    E.HostName(self.localhost.hostname),
                    E.IpAddress(self.localhost.ip_address),
                    E.ApplicationName(self.auth_application_name),
                    E.ApplicationVersion(self.auth_application_version),
                    E.Culture(self.auth_culture),
                    E.UICulture(self.auth_uiculture)
                )
            )
        )
        result = etree.tostring(message)
        logger.debug('Query message xml: %s', result)
        return result

    def get_auth_token(self):
        logger.info('Getting ticket for server %s', self.server)
        root = self.query_xml(self.auth_url, self.get_authentication_query())
        if root is not None:
            try:
                return root.xpath(self.TICKET_XPATH)[0]
            except IndexError:
                logger.exception('Error getting Ticket from %s', self.auth_url)


class ISP4XHiSecConfiguration(ISPConfiguration):
    auth_path = ''
    config_scheme = 'https'
    config_path = 'InfrastructureServices/ConfigurationService/ConfigurationService.ashx'
    identifier = '4x'
    intrinsic_flags = ['hisec']
    HMAC_IDENTIFIERS = {
        'TOKEN': 'iSiteWebApplication',  # Not required
        'TIMESTAMP_LABEL': 'Timestamp',
        'HASH_LABEL': 'Hash',
        'APPLICATIONIDENTIFIER': 'iSiteRadiology',  # Not Required
        'APPID_LABEL': 'AppId',
        'APPID': 'IDM',
        'SECRET_KEY': '',  # Loaded from resource.cfg
        'SERVERTIMESTAMP_LABEL': 'ServerTimeStamp',
        'RETRIES': 3,  # Possibly part of the protocol
    }
    auth_header_syntax = 'HMAC {TIMESTAMP_LABEL}={ISODATE};{HASH_LABEL}={HASH};{APPID_LABEL}={APPID}'

    def __init__(self, server):
        super(ISP4XHiSecConfiguration, self).__init__(server)
        self._auth_timestamp = None
        self.load_credentials()

    @staticmethod
    def load_credentials():
        """ Load the secretkey(USER113) from the resource.cfg file
            Which will in turn use for accessing the API end point
        """
        secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
        nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
        secret_key = nr.get_resource("$USER113$")
        ISP4XHiSecConfiguration.HMAC_IDENTIFIERS["SECRET_KEY"] = secret_key

    @property
    def auth_timestamp(self):
        if self._auth_timestamp is None:
            return datetime.now(tz=pytz.utc)
        return self._auth_timestamp

    def set_auth_timestamp(self, value, latency):
        self._auth_timestamp = parser.parse(
            value) + timedelta(seconds=(latency / 2)) if value else None

    def get_auth_header(self, token):
        return dict(Authorization=token)

    def query_xml(self, url, query, attempt=0):
        """
            As the HMAC Tokens will be invalidated within the
            given time limit(10mns for now), resetting the
            Token upon receiving 401 (unauthorized), status.
            This is important during discovery, that is if discovery
            takes more time than the token life time, discovery fails to
            computes facts for the remaining nodes.
        """
        if attempt < 3:
            response = self.suppressed_post(url=url, data=query, ok_only=False)
            if getattr(response, 'status_code', None) == 401:  # unauthorized
                self.reset_auth_session()
                attempt += 1
                # resume now
                return self.query_xml(url, query, attempt)
            if response and getattr(response, 'content', None):
                return objectify.fromstring(response.content)
        raise RuntimeError('Failed to retrive information - {0}, using url - {1}'.format(query, url))

    def get_formatted_token(self):
        '''
        This method return HMAC token required to auth iSite Server2Server communication
        sample token: 'HMAC Timestamp=2016-07-14T16:40:21.9763242+05:30;Hash=7zFZTBpbmmJvEPDry7b7XdB3LMl5tNRpwb5UBINpyT0=;AppId=IDM'
        :param timestamp:
        :return: token string
        '''
        isodate = self.auth_timestamp.isoformat()
        auth_header = self.auth_header_syntax.format(
            ISODATE=isodate,
            HASH=get_hashkey(self.HMAC_IDENTIFIERS['SECRET_KEY'], isodate),
            **self.HMAC_IDENTIFIERS
        )
        return auth_header

    def get_auth_token(self):
        for retry in range(self.HMAC_IDENTIFIERS['RETRIES']):
            auth_token = self.attempt_authorization()
            if auth_token:
                return auth_token
        raise ISPAuthenticationError

    def attempt_authorization(self):
        auth_token = self.get_formatted_token()
        response, latency = self.post_auth_request(auth_token)
        if self.is_ok_response(response):
            return auth_token
        if hasattr(response, 'headers'):
            self.set_auth_timestamp(response.headers.get(
                self.HMAC_IDENTIFIERS['SERVERTIMESTAMP_LABEL']), latency)

    def post_auth_request(self, token):
        start_time = datetime.utcnow()
        response = self.suppressed_post(
            self.config_url,
            self.get_server_info_query(),
            self.get_auth_header(token),
            ok_only=False
        )
        latency = (datetime.utcnow() - start_time).seconds
        return response, latency


class ISP3XConfiguration(ISPConfiguration):
    auth_path = 'iSiteWeb/Authentication/Authentication.ashx'
    config_path = 'iSiteWeb/Configuration/ConfigurationService.ashx'
    identifier = '3x'

    def __init__(self, server):
        super(ISP3XConfiguration, self).__init__(server)
        self.auth_user_login_id = 'rgomez@stentor.com'
        self.auth_password = '2dc33c7'
        self.auth_source = '__STENTOR_iSiteSimple_'
        self.auth_login_type = 'Administrative'
        self.auth_system_dsn = 'ISITE'
        self.auth_machine_name = 'deveid2k4-1'
        self.auth_ip_address = '192.168.43.50'
        self.auth_application_name = 'iSiteServer'
        self.auth_application_version = '3.6.0.0'

    def get_authentication_query(self):
        """generate
    <Message><LoginRequest>
    <UserLoginID>rgomez@stentor.com</UserLoginID>
    <Password>2dc33c7</Password>
    <AuthSource>__STENTOR_iSiteSimple_</AuthSource>
    <LoginType>Administrative</LoginType>
    <SystemDSN>ISITE</SystemDSN>
    <MachineName>deveid2k4-1</MachineName>
    <IpAddress>192.168.43.50</IpAddress>
    <ApplicationName>iSiteServer</ApplicationName>
    <ApplicationVersion>3.6.0.0</ApplicationVersion>
    </LoginRequest>
    </Message>"""
        E = self.E
        message = E.Message(
            E.LoginRequest(
                E.UserLoginID(self.auth_user_login_id),
                E.Password(self.auth_password),
                E.AuthSource(self.auth_source),
                E.LoginType(self.auth_login_type),
                E.SystemDSN(self.auth_system_dsn),
                E.MachineName(self.auth_machine_name),
                E.IpAddress(self.auth_ip_address),
                E.ApplicationName(self.auth_application_name),
                E.ApplicationVersion(self.auth_application_version),
            )
        )
        result = etree.tostring(message)
        logger.debug('Query message xml: %s', result)
        return result

    def get_auth_token(self):
        logger.info('Getting cookie for server %s', self.server)
        response = self.suppressed_post(
            url=self.auth_url, data=self.get_authentication_query())
        if response:
            return response.cookies.get('iSiteWebApplication')


class ISP4_1Configuration(ISP4XConfiguration):
    auth_path = '%2fiSiteWeb%2fAuthentication%2fv1_0%2fauthenticationservice.ashx'
    config_path = '%2fiSiteWeb%2fConfiguration%2fConfigurationService.ashx'
    identifier = '4_1'


class ISPDBConfiguration(ISP4XHiSecConfiguration):
    config_path = 'InfrastructureServices/RegistryService/RegistryService.ashx'
    EXTENDEDNODES = ('Rabbitmqnode', 'Database', 'Hl7', 'Rviewer',
                     'Eviewer', 'I4Processing', 'Mgnode')

    def __init__(self, server):
        super(ISPDBConfiguration, self).__init__(server)

    def get_server_info_query(self):
        """ Generate
            <Message>
                <ListNonCoreNodes/>
            </Message>
        """
        result = objectify.ElementMaker(annotate=False)
        return etree.tostring(self.E.Message(result.ListNonCoreNodes()))

    @cached_property
    def ext_config(self):
        return self.extended_hosts_config()

    def extended_hosts_config(self):
        try:
            self.auth_session()
            return self.query_xml(self.config_url, self.get_server_info_query())
        except ISPAuthenticationError:
            logger.warning('Extended discovery - failed to retrive host configuration')

    def get_hosts(self):
        return self.ext_config.xpath('//HostName') if self.ext_config else []

    def non_gd_hosts(self):
        return [x[0] for x in self.hosts_with_type if x[1] in self.EXTENDEDNODES]

    def gd_hosts(self):
        return [x for x in self.hosts_with_type if x[1] not in self.EXTENDEDNODES]

    @cached_property
    def hosts_with_type(self):
        return self.get_hosts_with_type()

    def get_hosts_with_type(self):
        hosts = []
        try:
            if self.ext_config and etree.tostring(self.ext_config):
                nodes = self.ext_config.ListNonCoreNodesResponse
                nodes = nodes.ArrayOfNode
                for node in nodes.iterchildren():
                    hosts.append((node.HostName, node.NodeType.text.title()))
        except AttributeError:
            pass
        return hosts

    def get_host_config(self, hostname):
        try:
            if self.ext_config and etree.tostring(self.ext_config):
                nodes = self.ext_config.ListNonCoreNodesResponse
                nodes = nodes.ArrayOfNode
                for node in nodes.iterchildren():
                    if node.HostName == hostname:
                        return node
        except AttributeError:
            msg = 'Module type information is not available for host - {host}, url - {url}'
            logger.warning(msg.format(host=self.hostname, url=self.config_url))

    def get_input_folder(self, hostname=None):
        mount_path = None
        try:
            if self.ext_config and etree.tostring(self.ext_config):
                nodes = self.ext_config.ListNonCoreNodesResponse
                nodes = nodes.ArrayOfNode
                for node in nodes.iterchildren():
                    if node.NodeType == 'Database' and node.HostName == hostname:
                        mount_path = node.FilePath.string
                        logger.info('Database mount path from {url} is {mount_path}'.format(url=self.config_url,
                                                                                            mount_path=mount_path))
                return mount_path.text
        except AttributeError:
            logger.warning('Database mount path from {url} is not available'.format(url=self.config_url))
            pass

    @cached_property
    def identifier(self):
        return None

    def get_noncore_node(self):
        return "DB"
