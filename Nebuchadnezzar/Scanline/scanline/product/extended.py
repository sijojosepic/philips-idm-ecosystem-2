from scanline.product import ProductScanner
from scanline.utilities.win_rm import fetch_hostname
import logging

logger = logging.getLogger(__name__)


class ExtendedProductScanner(ProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(ExtendedProductScanner, self).__init__(scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)

    def get_hostname(self, hostname):
    	host = fetch_hostname(hostname, self.kwargs.get('username'), self.kwargs.get('password'))
        return host.lower()


    def site_facts(self):
        facts = super(ExtendedProductScanner, self).site_facts()
        return self.segregate_credentials(facts)
