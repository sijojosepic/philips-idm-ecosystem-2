import logging
from scanline.host import HostScanner
from scanline.utilities.http import HTTPRequester
from cached_property import cached_property

from scanline.utilities import token_mgr


logger = logging.getLogger(__name__)


class ProductScanner(object):
    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        self.address = address
        self.tags = tags
        self.host_scanner = host_scanner
        self.endpoint = {'scanner': scanner, 'address': address}
        self.kwargs = kwargs
        self.ipv6_enabled = kwargs.get('ipv6_enabled', False)

    def get_hosts(self):
        yield self.address

    def get_hostname(self, host):
        return host.lower()

    def scan_host(self, host):
        return self.host_scanner(host, self.endpoint, tags=self.tags, **self.kwargs).to_dict()

    def get_present_modules(self, host_facts):
        return list(set(host_facts.keys()).intersection(HostScanner.get_host_scanners()))

    def segregate_credentials(self, facts):
        """
            Segregating the credentials from each host's fact, in order to avoid,
            getting it written to the facts collection at the back office.
        """
        credentials = {}
        for host_name, host_fact in facts.iteritems():
            host_credentials = host_fact.pop('credentials', {})
            if host_credentials:
                credentials.update({host_name: host_credentials})
        if credentials:
            facts.update({'credentials': credentials})
        return facts

    def get_host_facts(self, host):
        result = self.scan_host(host)
        if not result:
            return
        if 'address' not in result:
            result['address'] = self.address
        result['modules'] = self.get_present_modules(result)
        if self.host_scanner:
            host_scanner = self.host_scanner(host, self.endpoint, tags=self.tags, **self.kwargs)
        result['product_id'] = result['product_id'] if 'product_id' in result and result['product_id'] not in [ 'NA'] else (
            self.kwargs.get('product_id', None) or host_scanner.get_product_id() if self.host_scanner else None)
        return result

    def site_facts(self):
        logger.info('Gathering site facts from endpoint %s', self.endpoint)
        facts = {}
        for host in self.get_hosts():
            logger.debug('FACTS - collecting facts for %s', host)
            host_facts = self.get_host_facts(host)
            if host_facts:
                facts.update({self.get_hostname(host): host_facts})
        return facts


class GenericProductScanner(ProductScanner):

    def __init__(self, scanner, address, tags=None, host_scanner=None, **kwargs):
        super(GenericProductScanner, self).__init__(
            scanner, address, tags=tags, host_scanner=host_scanner, **kwargs)
        self.uri = self.kwargs.get('discovery_url', '')
        self._session = None
        self.roles = {}
        self.role = set()
        self.hmac_enabled = self.kwargs['hmac_enabled'] if 'hmac_enabled' in self.kwargs else True
        self.HTTPRequester = HTTPRequester()

    @cached_property
    def discovery_info(self):
        return self.get_discovery_info()

    @cached_property
    def environmentType(self):
        return self.get_environment_type()

    def get_environment_type(self):
        endpoint_environment_type = self.kwargs.pop('environmentType', '').lower()
        gd_endpoint = self.discovery_info.get('environmentType', '').lower()
        return gd_endpoint or endpoint_environment_type

    def do_request(self):
        if self.hmac_enabled:
            return token_mgr.do_hmac_request(self.uri)
        return self.HTTPRequester.suppressed_get(self.uri)

    def get_discovery_info(self):
        _discovery_info = {}
        if self.uri:
            response = self.do_request()
            if response:
                _discovery_info = response.json()
        else:
            msg = 'Discovery URL Missing for scanner - {scanner}, Address - {address}'
            logger.info(msg.format(**self.endpoint))
        return _discovery_info

    def get_hosts(self):
        host_info = self.discovery_info.get('hostinfo', [])
        if not host_info:
            msg = 'Host information is missing in json fetched from {uri}'
            logger.info(msg.format(uri=self.uri))
        return host_info

    def get_service_dsc(self, application):
        service_name = application['name'][:32]
        product_id = self.discovery_info.get('productid', '')
        application_type=application['applicationType']
        if application_type.lower() == 'webapi':
            applicationMonitoring = application.get('applicationMonitoring')
            checkType = applicationMonitoring.get('monitoringDetails').get('checkType')
            application_type = '{application_type}_{checkType}'.format(
                application_type=application_type,checkType=checkType)
        return 'Product__{product_id}__{application_type}__{service_name}__Status'.format(
            product_id=product_id, application_type=application_type, service_name=service_name)


    def service_config(self, host):
        service_list = []
        applications = host.get('applications')
        if not applications or applications == 'None':
            return None, service_list
        for application in applications:
            service_fields = self.get_service_dsc(application)
            if(application['enableMonitoring'] == "false"):
                service_list.append(service_fields)
            application['serviceDescription'] = service_fields
        return applications, service_list

    def get_hostname(self, host):
        return host.get('address', '')

    def get_host_role(self, host):
        if host['address'] not in self.roles.keys():
            self.role = set()
        self.role.add(host['role'])
        self.roles[host['address']] = self.role
        return list(self.roles[host['address']])

    def get_host_info(self, host):
        _scanner_obj = self.host_scanner(host['address'], self.endpoint, tags=self.tags,
                                         product_id=self.discovery_info.get('productid', ''),
                                         product_name=self.discovery_info.get('productname', ''),
                                         product_version=self.discovery_info.get('productversion', ''),
                                         role=self.get_host_role(host),
                                         environmentType=self.environmentType, **self.kwargs)
        return _scanner_obj.to_dict()

    def scan_host(self, host):
        host['address'] = host.get('name') if host.get('name') else host.pop('address')
        logger.info('Gathering host attributes from {address}'.format(
            address=host['address']))
        result = self.get_host_info(host)
        if not result:
            return
        applications, service_list = self.service_config(host)
        osType = host.get('osType')
        result.update({'service_excludes': service_list, 'applications': applications, 'osType': osType})
        logger.info('final result is {result} from {address}'.format(
            result=result, address=host['address']))
        return result
