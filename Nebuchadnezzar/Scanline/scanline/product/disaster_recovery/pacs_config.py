import logging
from cached_property import cached_property

from scanline.product.isp import ISP4XHiSecConfiguration

logger = logging.getLogger(__name__)


class PacsConfiguration(ISP4XHiSecConfiguration):

    def __init__(self, server):
        super(PacsConfiguration, self).__init__(server)

    def _get_dr_config(self):
        try:
            self.auth_session()
            sys_common = 'iSyntaxServer\\iSiteSystemCommon'
            _dr_config = self.query_host_config(self.server, sys_common)
            return _dr_config.get('EnableDisasterRecovery')
        except Exception:
            err_msg = 'Error reading the configuration from iSiteweb'
            logger.exception(err_msg)
            raise RuntimeError(err_msg)

    @cached_property
    def dr_config(self):
        return self._get_dr_config()
