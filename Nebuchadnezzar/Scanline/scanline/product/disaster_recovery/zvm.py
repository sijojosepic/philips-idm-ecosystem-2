from cached_property import cached_property

from scanline.host import HostScanner


class ZVMHost(HostScanner):
    module_name = 'DR'
    general_properties = HostScanner.general_properties.union(['hostname', 'vcenters', 'credentials', 'site_type',
                                                               'zerto_attrs'])
    LOCAL = 'local'
    PEER = 'peer'

    def __init__(self, hostname, endpoint, site_obj, tags=None, **kwargs):
        super(ZVMHost, self).__init__(hostname, endpoint, tags=tags,
                                      **kwargs)
        self.site_obj = site_obj
        self.product_name = site_obj.name
        self.product_version = site_obj.version
        self.vcenters = kwargs.get('vcenters')
        self.kwargs = kwargs

    @property
    def zerto_attrs(self):
        return self.site_obj.values

    @property
    def site_type(self):
        return self.site_obj.values.get('Link').get('type')

    @cached_property
    def credentials(self):
        return self.get_credentials()

    def get_credentials(self):
        items = ('username', 'password', 'zerto_username', 'zerto_password')
        # dict([('a', 'b'), ('c', 'd')])
        return dict([(item, self.encrypter.encrypt(self.kwargs.get(item))) for item in items])
