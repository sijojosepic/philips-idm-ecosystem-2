import logging
from cached_property import cached_property
from requests.exceptions import ConnectionError
try:
    import zerto
except ImportError:
    # TODO: This Exception could be removed once zerto library
    # is part of the Neb, as of now tar ball is not available
    # in PyPI simple.
    pass

logger = logging.getLogger(__name__)


class IDMZerto(object):
    """
        All interaction with respect to zerto liabrary supposed to be
        through this module.
    """
    URL_TEMPLATE = 'https://{host}:{port}'

    def __init__(self, zhost, username, password, url='', port=9669, **kwargs):
        """
            :param zhost: - ZVM hosts, can be an iterables ['zvm1', 'zvm2'] or a string 'zvm1'

        """
        self.hosts = [zhost] if isinstance(zhost, str) else zhost
        self.user = username
        self.password = password
        self.url = url
        self.port = port
        self._zconn = None
        self.timeout = kwargs.get('timeout') or 5

    def get_url(self, host):
        return self.URL_TEMPLATE.format(host=host, port=self.port)

    def _zerto_connection(self, url):
        zconn = zerto.Zerto(url)
        zconn.get_apis(timeout=self.timeout)
        zconn.get_session(self.user, self.password)
        return zconn

    def get_connection(self):
        if self.url:
            return self._zerto_connection(self.url)
        for host in self.hosts:
            try:
                return self._zerto_connection(self.get_url(host))
            except ConnectionError:
                logger.exception('Could not connect to Zerto using -%s' % host)
        raise RuntimeError('Could not establish a connection to Zerto')

    @cached_property
    def zconn(self):
        return self.get_connection()

    @cached_property
    def vpgs(self):
        return self.get_vpgs()

    def get_vpgs(self):
        response = self.get_request('v1/vpgs')
        return response.json()

    def get_localsite(self):
        return self.zconn.get_localsite()

    def get_peersite(self, site_id=None):
        return self.zconn.get_peersites(siteid=site_id)

    @cached_property
    def is_protected(self):
        return self.site_is_protected()

    def site_is_protected(self):
        try:
            if self.vpgs[0]['ProtectedSite']['type'] == 'LocalSiteApi':
                return True
        except IndexError:
            pass
        return False

    def get_paring_status(self, site_id):
        response = self.get_request('/v1/peersites/{0}'.format(site_id))
        peersite = response.json()
        return peersite['PairingStatus']

    def protectedsite_pairing_status(self):
        site_id = self.vpgs[0]['ProtectedSite']['identifier']
        return self.get_paring_status(site_id)

    def get_request(self, path):
        return self.zconn.get_request(path)


