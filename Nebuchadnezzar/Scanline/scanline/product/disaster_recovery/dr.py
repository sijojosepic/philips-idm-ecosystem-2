from cached_property import cached_property

from scanline.product import ProductScanner
from scanline.product.disaster_recovery.pacs_config import PacsConfiguration
from scanline.product.disaster_recovery.zerto_wrapper import IDMZerto
from scanline.product.disaster_recovery.zvm import ZVMHost


class DRScanner(ProductScanner):
        module_name = 'DR'

        def __init__(self, scanner, address,
                     username, password, domain=None,
                     tags=None, host_scanner=None,
                     product_id='DisasterRecovery', **kwargs):
            super(DRScanner, self).__init__(scanner, address,
                                            tags=tags,
                                            host_scanner=host_scanner,
                                            product_id=product_id,
                                            **kwargs)
            self.pconfig = PacsConfiguration(kwargs['infra_address'])
            self.username = username
            self.password = password
            self.zobj = IDMZerto(address, kwargs.get('zerto_username'),
                                 kwargs.get('zerto_password'), **kwargs)

        @cached_property
        def is_dr_enabled(self):
            return int(self.pconfig.dr_config) == 1

        def get_hosts(self):
            hosts = []
            local_site = self.zobj.get_localsite()
            local_host = ZVMHost(local_site.values.get('IpAddress'), self.endpoint,
                                 local_site,
                                 username=self.username,
                                 password=self.password, tags=self.tags,
                                 **self.kwargs)
            hosts.append(local_host)
            for site in self.zobj.get_peersite():
                hosts.append(ZVMHost(site.hostname, self.endpoint, site,
                                     username=self.username, tags=self.tags,
                                     password=self.password,
                                     **self.kwargs))
            return iter(hosts)

        def site_facts(self):
            facts = {}
            if self.is_dr_enabled:
                for host in self.get_hosts():
                    facts.update({host.hostname: host.to_dict()})
                return self.segregate_credentials(facts)
            raise RuntimeError('Disaster Recovery is not enabled for the Site')
