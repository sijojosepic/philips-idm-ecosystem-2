import hmac
import hashlib
import base64
import requests
from datetime import datetime

from scanline.utilities.config_reader import get_resource_value


HMAC_IDENTIFIERS = {
    'TOKEN': 'iSiteWebApplication',  # Not required
    'TIMESTAMP_LABEL': 'Timestamp',
    'HASH_LABEL': 'Hash',
    'APPLICATIONIDENTIFIER': 'iSiteRadiology',  # Not Required
    'APPID_LABEL': 'AppId',
    'APPID': 'IDM',
    'SECRET_KEY': '',  # Loaded from resource.cfg
    'SERVERTIMESTAMP_LABEL': 'ServerTimeStamp',
    'RETRIES': 3,  # Possibly part of the protocol
}


HMAC_IDENTIFIERS["SECRET_KEY"] = get_resource_value("$USER113$")


def get_hashkey(key, timestamp):
    hashkey = hmac.new(key, msg=timestamp, digestmod=hashlib.sha256).digest()
    return base64.b64encode(hashkey).decode()


def get_auth_header(token):
    return dict(Authorization=token)


def get_iso_timestamp():
    return datetime.utcnow().isoformat()


def generate_token(isodate):
    '''
    Format: 'HMAC Timestamp=2016-07-14T16:40:21.9763242+05:30;Hash=7zFZTBpbmmJvEPDry7b7XdB3LMl5tNRpwb5UBINpyT0=;AppId=IDM'
    '''
    auth_header_syntax = 'HMAC {TIMESTAMP_LABEL}={ISODATE};{HASH_LABEL}={HASH};{APPID_LABEL}={APPID}'
    return auth_header_syntax.format(
        ISODATE=isodate,
        HASH=get_hashkey(HMAC_IDENTIFIERS['SECRET_KEY'], isodate),
        **HMAC_IDENTIFIERS
    )


def get_formatted_token(iso_time_stamp=None):
    iso_time_stamp = iso_time_stamp or get_iso_timestamp()
    return generate_token(iso_time_stamp)


def do_get_request(url, headers, timeout=20, verify=False):
    """
        Consumers of this function should catch
        RequestExceptions
    """
    response = requests.get(
        url=url,
        headers=headers,
        timeout=timeout,
        verify=verify
    )
    response.raise_for_status()
    return response


def do_hmac_request(url, timeout=5, verify=False):
    """
        This function will do HMAC based, GET requests.
    """
    auth_token = get_formatted_token()
    return do_get_request(url, get_auth_header(auth_token), timeout, verify=verify)
