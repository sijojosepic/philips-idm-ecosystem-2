########################################################
# Purpose of this file is to make it easy to read the
# configuration provided in the tbconfig.py and along with
# other basic utilities like site id. This helps to
# Avoid the hardcoding of configuration values across files/plugins
# which is already availbale in the /usr/lib/celery/tbconfig.py
# file
#
########################################################

import linecache
import yaml
import json
import warnings
import requests
import base64

from phimutils.resource import NagiosResourcer
try:
    import tbconfig
except ImportError:
    # Make it possible to use/import the config_reader.py file
    # even in the Plugins, as the tbconfig file is part of celery
    # /usr/lib/celery, its not possible to import it directly when
    # its not invoked through a celery task
    import imp
    imp.load_source('tchelp', '/usr/lib/celery/tchelp.py')
    imp.load_source('tbconfig', '/usr/lib/celery/tbconfig.py')
    import tbconfig


def get_idm_secret():
    return linecache.getline(tbconfig.DISCOVERY['SECRET_FILE'], 1).strip()


def get_site_id(filepath='/etc/siteid'):
    with open(filepath, 'r') as fd:
        return fd.read().strip()


def discovery_yml_contents():
    with open(tbconfig.DISCOVERY['CONFIGURATION'], 'rb') as f:
        return yaml.load(f)


def read_from_tbconfig(item):
    """
        read_from_tbconfig('VERSION') =>
        Returns the value of VERSION key in the
        tbconfig file.

        version in tbconfig
        --------------
        'VERSION': {
                'ENDPOINT': 'http://vigilant/version'
        }
        then function returns ->
        {
            'ENDPOINT': 'http://vigilant/version'
        }
    """
    return getattr(tbconfig, item)


def get_scanners():
    """
        Get the product scanner information from the discovery.yml file
        which will be used during the utility server discovery for enabling
        the back up for any site, based on its ISEE products subscription
    """
    scanner_set = set()
    for end_point in discovery_yml_contents():
        scanner_set.add(end_point['scanner'].lower())
    return list(scanner_set)


def get_resource_value(resource):
    """
       This function returns value of the resource
       from DISCOVERY['NAGIOS_RESOURCE_FILE'] =>
       /etc/philips/shinken/resource.d/resource.cfg
    """
    nr = NagiosResourcer(
        tbconfig.DISCOVERY['NAGIOS_RESOURCE_FILE'], get_idm_secret())
    return nr.get_resource(resource)


def get_thruk_response(sub_url):
    config = read_from_tbconfig('THRUK')
    thruk_auth_uname = config['username']
    thruk_auth_pw = base64.b64decode(config['password'])
    url = config['URL'].rstrip("/")+ '/' + sub_url
    try:
        params = {'view_mode': 'json'}
        response = requests.get(url, auth=(thruk_auth_uname, thruk_auth_pw), verify=False, params=params)
        return response.json()
    except requests.exceptions.RequestException as ex:
        return {}