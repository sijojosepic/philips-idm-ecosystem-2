import logging
import requests
import linecache
import json
from phimutils.resource import NagiosResourcer

DISCOVERY = {
    'NAGIOS_RESOURCE_FILE': '/etc/philips/shinken/resource.d/resource.cfg',
    'NAGIOS_THRESHOLD_FILE': '/etc/philips/shinken/resource.d/threshold.cfg',
    'SECRET_FILE': '/etc/philips/secret'
}
secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
nt = NagiosResourcer(DISCOVERY['NAGIOS_THRESHOLD_FILE'], secret)


logger = logging.getLogger(__name__)


class RhapsodyStats(object):
    def __init__(self):
        self._rhapsody_components = {}
        self._headers = {
            'Accept': 'application/vnd.orchestral.rhapsody.6_0+json', }
        self._rhapsody_list = ['memoryusage', 'diskspace', 'messagecount']
        self._username = nr.get_resource("$USER143$")
        self._password = nr.get_resource("$USER144$")
        self._port = nt.get_resource("$ISP_HL7_PORT$")
        self._auth = (self._username, self._password)

    def get_rhapsody_components(self, hostname):
        url = 'https://{0}:{1}/api/info'.format(hostname, self._port)
        self.fetch_version(url, self._headers, self._auth)
        for component_name in self._rhapsody_list:
            url = 'https://{0}:{1}/api/statistics/{2}'.format(hostname,
                                                              self._port,
                                                              component_name)
            self.fetch_components(component_name, url, self._auth)
        return self._rhapsody_components

    def fetch_version(self, url, headers, auth, timeout=3):
        try:
            response = requests.get(url, headers=headers, verify=False,
                                    auth=auth, timeout=timeout)
            version = json.loads(response.content)['data']['version']
            self._rhapsody_components.update({'version': version})
        except Exception:
            pass

    def fetch_components(self, component_name, url, auth, timeout=3):
        headers = {
            'Accept': 'application/json',
            'Accept': 'application/vnd.orchestral.rhapsody.6_2+json', }
        if component_name == 'messagecount':
            headers = {
            'Accept': 'application/json, text/html',
            'Accept': 'application/vnd.orchestral.rhapsody.6_0+json', }
        try:
            response = requests.get(url, headers=headers, verify=False,
                                    auth=auth, timeout=timeout)
            data = json.loads(response.content)['data']
            self._rhapsody_components.update({component_name: data})
        except Exception:
            pass
