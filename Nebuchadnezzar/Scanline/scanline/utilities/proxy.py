import yaml

def get_proxy():
    proxy_url = ''
    ret_obj = ''
    stream = open("/etc/philips/discovery.yml", "r")
    docs = yaml.load_all(stream)
    for doc in docs:
        if 'proxy' not in doc[0]:
            continue
        proxy_url = doc[0]['proxy']

    if proxy_url:
        ret_obj = {"http"  : proxy_url, "https" : proxy_url}
    return ret_obj