# Common utitilies add here
# can be utilized in multiple plugins
from ast import literal_eval

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

def str_to_list(str_val):
    # "['Q1,34,50']" -> ['Q1,34,50']
    val = literal_eval(str_val)
    return val if isinstance(val, list) else []

def format_status(warning_msg, critical_msg, ok_msg, perf_msg):
    state, msg = OK, ''
    if ok_msg and not warning_msg and not critical_msg:
        state = OK
        msg = ok_msg
    elif critical_msg:
        state = CRITICAL
        msg = critical_msg
    elif warning_msg:
        state = WARNING
        msg = warning_msg
    output = '{0} | {1}'.format(msg, perf_msg)
    return state, output


def safe_mongo_keys(data, place_holder='-'):
    '''
        @data => dictionary
        At present Mongodb doesn't support '.' in keys
        safe gurad keys by replacing '.' with 'place_holder'
        at all nested levels
    '''
    temp = {}
    for k, v in data.iteritems():
        k = k.replace('.', place_holder)
        if isinstance(v, dict):
            temp[k] = safe_mongo_keys(v, place_holder)
        else:
            temp[k] = v
    return temp
