from scanline.utilities.http import HTTPRequester


class BackOffice(object):
    HEADERS = {'X-Requested-With': "True"}
    HOST_URL = '/pma/facts/{site_id}/hosts'
    MODULE_URL = '/pma/facts/{site_id}/modules/ISP/keys/module_type'
    FACT_URL = '/pma/facts/{site_id}/nodes'

    def __init__(self, base_url, site_id, verify=False):
        self.base_url = base_url.rstrip('/')
        self.site_id = site_id
        self.verify = verify
        self.requster = HTTPRequester()

    def form_url(self, url):
        return self.base_url + url.format(site_id=self.site_id)

    def req(self, url, headers):
        _headers = self.HEADERS.copy()
        if headers:
            _headers.update(headers)
        return self.requster.suppressed_get(url, headers=_headers, verify=self.verify)

    def hosts_with_mod_type(self, headers=None):
        mod_url = self.form_url(self.MODULE_URL)
        return self.req(mod_url, headers)
