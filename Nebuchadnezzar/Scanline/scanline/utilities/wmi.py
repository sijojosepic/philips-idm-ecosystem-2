import csv
import logging
from uuid import UUID
from subprocess import Popen, PIPE
from itertools import dropwhile


WMIC = '/usr/local/bin/wmic'

logger = logging.getLogger(__name__)


class WMIHostDriller(object):
    MANUFACTURER_QUERY = "SELECT Manufacturer FROM Win32_ComputerSystem"
    UUID_QUERY = "SELECT UUID FROM Win32_ComputerSystemProduct"
    SERVICES_QUERY = "SELECT Name, Started, State, StartMode FROM Win32_Service WHERE Name LIKE '{service}'"
    PROCESS_QUERY = "SELECT * FROM Win32_Process"
    ROLES_QUERY = "SELECT Roles FROM Win32_ComputerSystem"
    WINDOWS_QUERY = "SELECT Caption, CSDVersion, OSLanguage, SerialNumber, ServicePackMajorVersion, ServicePackMinorVersion, Version FROM Win32_OperatingSystem"

    def __init__(self, host, user, password, domain=None, delimiter="\01"):
        self.user = user
        self.password = password
        self.domain = domain
        self.host = host
        self.delimiter = delimiter

    def get_command(self, query):
        delimiter_param = '--delimiter={delimiter}'.format(delimiter=self.delimiter)
        domain_part = '{domain}/'.format(domain=self.domain) if self.domain else ''
        user_template = '{domain_part}{user}%{password}'
        user_param = user_template.format(domain_part=domain_part, user=self.user, password=self.password)
        host_param = '//{host}'.format(host=self.host)
        wmi_args = '--option=client ntlmv2 auth=Yes'
        return [WMIC, delimiter_param, '-U', user_param, host_param, query, wmi_args]

    def fetch_reader(self, query_lines):
        # get to the class name "CLASS:"
        actual_query_output = dropwhile(lambda x: not x.startswith('CLASS:'), query_lines)
        # skip the class name
        class_name = next(actual_query_output, None)
        logger.debug('[%s] WMI Class found - %s', self.host, class_name)
        return csv.DictReader(actual_query_output, delimiter=self.delimiter)

    def query_wmi(self, query):
        command = self.get_command(query)
        try:
            logger.debug('[%s] Trying to run WMI query command: %s', self.host, command)
            pp = Popen(command, stdout=PIPE, stderr=PIPE)
            query_stdout, query_stderr = pp.communicate()
        except OSError:
            logger.error('[%s] OSError running command: %s', self.host, command)
            return
        logger.debug('[%s] WMI query return code : %s', self.host, pp.returncode)
        if pp.returncode == 0:
            logger.debug('[%s] Output from query: %s', self.host, query_stdout)
            output_lines = query_stdout.splitlines()
            return self.fetch_reader(output_lines)

    def parse_single_line(self, query):
        service_reader = self.query_wmi(query)
        try:
            return service_reader.next()
        except (StopIteration, AttributeError):
            pass

    def get_field_from_query(self, query, field):
        results = self.parse_single_line(query)
        if results:
            result = results.get(field)
            logger.debug('[%s] Field: %s Value is %s.', self.host, field, result)
            return result
        logger.debug('[%s] WMI Query for %s did not return proper data %s', self.host, field, str(results))

    def get_manufacturer(self):
        return self.get_field_from_query(self.MANUFACTURER_QUERY, 'Manufacturer')

    def get_uuid(self):
        result = self.get_field_from_query(self.UUID_QUERY, 'UUID')
        try:
            # Flip UUID from Windows UUID style
            return str(UUID(bytes_le=UUID(result).bytes))
        except (ValueError, TypeError):
            pass

    def is_service_auto_start(self, service):
        query = self.SERVICES_QUERY.format(service=service)
        return self.get_field_from_query(query, 'StartMode') == 'Auto'

    def get_windows_info(self):
        service_results = self.parse_single_line(self.WINDOWS_QUERY)
        if service_results:
            return {
                'Name': service_results.get('Caption'),
                'ServicePack': service_results.get('CSDVersion'),
                'OSLanguage': service_results.get('OSLanguage'),
                'SerialNumber': service_results.get('SerialNumber'),
                'ServicePackVersion': '{major}.{minor}'.format(
                    major=service_results.get('ServicePackMajorVersion'),
                    minor=service_results.get('ServicePackMinorVersion')
                ),
                'Version': service_results.get('Version')
            }
        logger.debug('[%s] WMI Query for Windows Information did not return data.', self.host)
