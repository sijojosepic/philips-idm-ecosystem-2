import requests
import json
import logging

logger = logging.getLogger(__name__)


class PCMStats(object):
    IDM_TOKEN = 'idm'

    def __init__(self):
        self._pcm_components = {}

    def get_payload(self):
        data = {
            'Token_ID': PCMStats.IDM_TOKEN
        }
        return json.dumps(data)

    def safe_get(self, url, data=None):
        try:
            get_response = requests.get(url, verify=False, data=data, timeout=5)
            get_response.raise_for_status()
            return get_response
        except Exception as e:
            logger.exception("Error while getting pcm listener version url:{0}. data:{1} and "
                             "exception:{2}".format(url, data, str(e)))
            return False

    def pcm_get(self, host):
        """
        Make a https call to get pcm listener version.
        :param host: target host IP address type str
        :return: response object
        """
        https_url = 'https://{0}:8182/pcm/version'.format(host)
        response = self.safe_get(https_url, data=self.get_payload())
        return response

    def get_pcmcomponent(self, host):
        """
        This method gets the pcm components. It contains name and version.
        Ex: {"version": "5.0.0.0", "name": "PCMFramework"}
        """
        try:
            response = self.pcm_get(host)
            data = response.json()
            self._pcm_components['name'] = data['productname']
            self._pcm_components['version'] = data['productversion']
            return self._pcm_components
        except Exception as e:
            logger.exception("Error while getting pcm component for host:{0}, exception:{1}".format(host, str(e)))
            return {}
