import redis
import json
import linecache
import pymssql
import collections
from http import HTTPRequester

QUERY_GET_PRIMARY = """SELECT replica_server_name
                    FROM sys.dm_hadr_availability_replica_states AS drs
                    INNER JOIN sys.availability_replicas AS ar 
                    ON drs.replica_id = ar.replica_id and
                       role_desc='PRIMARY';"""

QUERY_PRIMARY_CHECK_MIRRORING = """SELECT mirroring_role_desc
                        FROM  sys.database_mirroring m
                        WHERE  mirroring_state_desc IS NOT NULL"""

SITEID_FILE = '/etc/siteid'
HOST_URL = 'pma/facts/{site_id}/hosts'
MODULE_URL_DEFAULT = 'pma/facts/{site_id}/modules/ISP'
FACT_URL = 'pma/facts/{site_id}/nodes'


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_backoffice_data(vigilant_url, fact_type='host', MODULE_URL=MODULE_URL_DEFAULT):
    """ This function returns back office data."""
    SITEID = get_siteid()
    host_url = vigilant_url + HOST_URL.format(site_id=SITEID)
    module_url = vigilant_url + MODULE_URL.format(site_id=SITEID)
    fact_url = vigilant_url + FACT_URL.format(site_id=SITEID)
    fact_dict = {'host': host_url, 'module': module_url, 'fact': fact_url}
    http_obj = HTTPRequester()
    result = http_obj.suppressed_get(fact_dict[fact_type], headers={'X-Requested-With': "True"})
    if result:
        return json.loads(result.content)


def get_dbpartner_in_dbscanner(hostaddress, vigilant_url):
    db_host = None
    try:
        facts = get_backoffice_data(vigilant_url, fact_type='module', MODULE_URL='pma/facts/{site_id}/modules/dbpartner')
        if facts:
            db_host = [str(host['dbpartner']) for host in facts['result'] if host['hostname'] == hostaddress]
        if db_host:
            if db_host[0] != 'NA':
                return [hostaddress, db_host[0]]
            else:
                return [hostaddress]
        else:
            return []
    except Exception as e:
        return []


def get_dbnodes(vigilant_url):
    """ This function returns db server names within current environment. """
    db_hosts = []
    try:
        facts = get_backoffice_data(vigilant_url, 'module')
        if facts:
            for host in facts['result']:
                if 'hostname' in host and 'ISP' in host and 'endpoint' in host['ISP'] and 'module_type' in host[
                    'ISP'] and 'address' in host['ISP']['endpoint']:
                    if host['ISP']['module_type'] == 'Database':
                        db_hosts.append(host['hostname'] + '#' + host['ISP']['endpoint']['address'])
            separated_dict = collections.defaultdict(list)
            for node in db_hosts:
                separated_dict[node.split('#')[1]].append(node.split('#')[0])
            db_hosts = dict(separated_dict).values()
            return db_hosts
        else:
            return []
    except Exception as e:
        return []


def get_cluster_primary_db(hostname, username, password, database, vigilant_url):
    """ This function returns Primary db server name for sql always on."""
    error_connection = []
    db_nodes = filter(lambda x: hostname.split('.')[1] in x[0], get_dbnodes(vigilant_url))
    if db_nodes:
        db_nodes = db_nodes[0]
    for node in db_nodes:
        try:
            conn = pymssql.connect(server=node, user=username, password=password, database=database, login_timeout=10)
        except Exception as e:
            error_connection.append(node)
            continue
        cursor = conn.cursor()
        cursor.execute(QUERY_GET_PRIMARY)
        result = cursor.fetchall()
        conn.close()
        if result:
            return result[0][0] + hostname[hostname.find('.'):]
    return False


def get_mirroring_primary_check(hostname, username, password, database):
    """ This function checks 'is current db primary?' and returns result in boolean."""
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=10)
    except Exception as e:
        return "Connection Error"
    if conn:
        cursor = conn.cursor()
        cursor.execute(QUERY_PRIMARY_CHECK_MIRRORING)
        db_role = [str(i[0]) for i in cursor.fetchall()]
        conn.close()
        if 'PRINCIPAL' in db_role:
            return True
    return False


if __name__ == '__main__':
    print get_dbnodes('')
    print get_dbpartner_in_dbscanner('192.168.180.80', '')
