import redis
from redis.exceptions import RedisError


class CacheMgr(object):
    url = 'localhost'
    server = redis.Redis(url)

    @classmethod
    def set(cls, key, value):
        if type(value) == dict:
            cls.server.hmset(key, value)
        else:
            cls.server.set(key, value)

    @classmethod
    def get(cls, key, multi=False):
        if multi:
            return cls.server.hgetall(key)
        else:
            return cls.server.get(key)

    @classmethod
    def remove(cls, key):
        cls.server.delete(key)

    @classmethod
    def setex(cls, key, value, timeout):
        cls.server.setex(key, value, timeout)


class SafeCache(object):
    @classmethod
    def do(cls, method, *args, **kwargs):
        try:
            return getattr(CacheMgr, method)(*args, **kwargs)
        except RedisError:
            pass
