import logging
import winrm
from base64 import b64encode
from requests.exceptions import RequestException
from time import sleep
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)

logger = logging.getLogger(__name__)

OK = 0
CRITICAL = 2
UNKNOWN = 3


class WinRM(object):
    def __init__(self, host_name, user, passwd, transport='ntlm', server_cert_validation='ignore'):
        self.host_name = host_name
        self.user = user
        self.passwd = passwd
        self.transport = transport
        self.server_cert_validation = server_cert_validation
        self._session = None

    @property
    def session(self):
        if not self._session:
            self.set_session()
        return self._session

    def set_session(self):
        self._session = winrm.Session(self.host_name, auth=(self.user, self.passwd),
                                      transport=self.transport,
                                      server_cert_validation=self.server_cert_validation,
                                      operation_timeout_sec=45, read_timeout_sec=50)
        return self._session

    def execute_ps_script(self, ps_script):
        return self.session.run_ps(ps_script)

    def execute_cmd(self, cmd, args=()):
        return self.session.run_cmd(cmd, args)

    def execute_long_script(self, script, operation_timeout_sec=45, read_timeout_sec=50):
        """
            This method is intended to use when the ps script size is more
            and to avoid TypeError while formatting the out put within pywinrm
            library upon WinRM timeout.

            TypeError: find() takes exactly 2 arguments (3 given) pywinrm
            https://github.com/ansible/ansible/issues/39031

            @ operation_timeout_sec -> WinRM timeout
            @ read_timeout_sec -> requests timeout

            Where 'operation_timeout_sec' should be greater than 'read_timeout_sec'


        """
        url = 'http://{0}:5985/wsman'.format(self.host_name)
        cmd = """
        # Load script from env-vars
        . ([ScriptBlock]::Create($Env:WINRM_SCRIPT))
        """
        encoded_cmd = b64encode(cmd.encode('utf_16_le')).decode('ascii')
        p = winrm.protocol.Protocol(url, username=self.user,
                                    password=self.passwd,
                                    transport=self.transport,
                                    operation_timeout_sec=operation_timeout_sec,
                                    read_timeout_sec=read_timeout_sec)

        shell = p.open_shell(env_vars=dict(WINRM_SCRIPT=script))
        command = p.run_command(shell, "powershell -EncodedCommand {0}".format(encoded_cmd))
        rs = winrm.Response(p.get_command_output(shell, command))
        p.cleanup_command(shell, command)
        p.close_shell(shell)
        return rs


def extract_credentials(host, user_list, pwd_list, attempt=0):
    """
       This method extracts the right username and password from the given
       list of username and password for the host.
       As wmi call may not be successfull at the first attempt, in the worst case
       function will attempt to figure out the right credentials twice.
    """
    if attempt < 2:
        for i in range(len(user_list)):
            try:
                resp = WinRM(host, user_list[i], pwd_list[i]).execute_cmd('echo test')
                if 'test' in resp.std_out:
                    return {'username': user_list[i], 'password': pwd_list[i]}
            except Exception:
                pass
        else:
            sleep(.5)
            return extract_credentials(host, user_list, pwd_list, attempt + 1)
    return {}


def exec_ps_script(hostname, username, password, ps_script):
    """ This function is intended to avoid duplication,in shinken plugins.
Within plugin this method can be used to execute powershell scripts."""
    status = CRITICAL
    try:
        win_rm = WinRM(hostname, username, password)
        out_put = win_rm.execute_ps_script(ps_script)
        status, msg = OK, out_put
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : WinRM Error {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(hostname)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(
                hostname)
    return status, msg


def execute_powershell_script(address, username, password, ps_script):
    """
        This function reads the file from a windows host, and
        returns the content.
        :param address: address of the node where *.json file is kept
        :param username: username of the node
        :param password: password of the node
        :param ps_script: 'Get-Content -Path C:\file.*'
    """
    try:
        win_rm = WinRM(address, username, password)
        out_put = win_rm.execute_ps_script(ps_script)
        if out_put.status_code == 0:
            logger.info('Successfully read the file content')
            return out_put.std_out.strip()
        else:
            logger.error('Error while reading the file content {0}'.format(str(out_put.std_err)))
    except AuthenticationError as e:
        logger.exception('WinRM Error {0}'.format(e))
    except RequestException as e:
        logger.exception('Request Error {0}'.format(e))
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        logger.exception('WinRM Error {0}'.format(str(e)))
    except ValueError as e:
        logger.exception('JSON Format Error {0}'.format(e))
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            logger.exception('Issue in connecting to node - {0}'.format(address))
        else:
            logger.exception('Typeerror(May be Issue in connecting to node - {0})'.format(address))
    except Exception as e:
        logger.exception('Exception {0}'.format(e))


def fetch_hostname(address, user, password):
    ps_script = '[Net.Dns]::GetHostByAddress("{0}").Hostname'.format(address)
    return execute_powershell_script(address, user, password, ps_script)
