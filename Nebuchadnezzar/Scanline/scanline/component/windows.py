import logging


logger = logging.getLogger(__name__)


class WindowsServiceComponent(object):
    def __init__(self, wmi_driller, service_name):
        self.wmi_driller = wmi_driller
        self.service_name = service_name

    def service_auto_start_status(self, hostname):
        auto_start = self.wmi_driller.is_service_auto_start(self.service_name)
        if auto_start:
            logger.info('%s set to auto start on %s', self.service_name, hostname)
            return self.service_name

    def scan(self, hostname):
        logger.info('Scanning for "%s" on %s', self.service_name, hostname)
        return self.service_auto_start_status(hostname)
