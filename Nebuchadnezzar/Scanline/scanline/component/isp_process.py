from lxml import objectify


class ProcessInterface(object):

    def get_process_list(self):
        raise NotImplementedError()


class ISPProcess(ProcessInterface):
    SERVICE_MAPPING = {'IExport': 'iexport',
                       'StentorHeartbeat': 'stentor_heartbeat',
                       'iSiteServer': 'isite_server',
                       'Dmwl': 'dmwl',
                       'AuditJPM': 'audit_jpm',
                       'StorageService': 'storage_service',
                       'iArchive': 'iarchive', 'StudyMigration': 'study_migration',
                       'DICOMService': 'dicom_service',
                       'StudyRouter': 'study_router',
                       'NotificationService': 'notification_service'
                       }

    def __init__(self, service_config):
        self.service_config = service_config

    def get_process_list(self):
        process_list = []
        if self.service_config:
            known_process = set(self.SERVICE_MAPPING.keys())
            for item in self.service_config.config_root.iterchildren():
                if item.tag == 'Process':
                    process_name = str(item.ProcessName)
                    if item.State == 1 and process_name in known_process:
                        process_list.append(self.SERVICE_MAPPING[process_name])
        return process_list


class UDMProcess(ProcessInterface):

    def __init__(self, service_config):
        self.service_config = service_config
        self._root = None

    @property
    def root(self):
        if not self._root:
            if self.service_config:
                try:
                    info = self.service_config[0]["ConfigValue"].encode(
                        'utf-8')
                    self._root = objectify.fromstring(info)
                except KeyError, IndexError:
                    pass
        return self._root

    def get_process_list(self):
        """ Capturing all the UDM process and enabling monitoring for only those processes which has hostgroup"""
        process_list = []
        if self.root:
            for item in self.root.iter():
                if item.tag == 'process':
                    if item.state == 1:
                        process_list.append(str(item.name).lower())
        return process_list
