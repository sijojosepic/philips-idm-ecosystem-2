from distutils.core import setup
import os
phiversion = os.environ['phiversion']
setup(name='psmith',
      version=phiversion,
      description='Philips IDM Repo Synchronization',
      author='Leonardo Ruiz',
      author_email='leonardo.ruiz@philips.com',
      py_modules=['psmith', 'enums', 'package_status', 'subscription', 'package_service', 'package_deletion',
                  'package_data', 'deletion', 'siteinfo'],
      #packages=['psmith']
      )
