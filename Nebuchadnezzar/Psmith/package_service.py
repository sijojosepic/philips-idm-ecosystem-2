from package_deletion import PackageDeletion
from package_data import PackageData


class CleanUpService:

    def execute(self):
        raise NotImplementedError


class PackageCleanUpService(CleanUpService):
    """
    This is a package clean up service class which initiates the package deletion
    for each product i.e ISP, AWV.
    """
    NOT_FOUND = "No package files found for deletion."

    def __init__(self):
        self.PackageDeletion = PackageDeletion
        self.package_data = PackageData()

    def _get_available_packages(self):
        return self.package_data.available_packages

    def execute(self):
        final_msg = ""
        for pkg_data in self._get_available_packages():
            final_msg += self.PackageDeletion(pkg_data.get('product_name'), pkg_data.get('product_value'),
                                              pkg_data.get('package_list')).delete()
        return final_msg if final_msg else self.NOT_FOUND
