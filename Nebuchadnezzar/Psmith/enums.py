# Define your all enums here!

class PackageStatus:
    SUBMITTED = 'Submitted'
    SUBSCRIBED = 'Subscribed'
    STARTED = 'Started'
    INPROGRESS = 'In Progress'
    FAILED = 'Failed'
    AVAILABLE = 'Available'
    DELETED = 'Deleted'
    NONE = 'None'


class FileExtensions:
    PART_SUFFIX = '.part'
    ZIP_SUFFIX = '.zip'
    XML_SUFFIX = '.xml'
