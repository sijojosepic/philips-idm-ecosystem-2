import requests
import logging
import linecache
from phimutils.threshold import get_threshold_value
from tbconfig import SITEID_FILE, PACKAGE_CONFIG, VMWARE


logger = logging.getLogger(__name__)

class PackageData(object):
    """
    This class is responsible for to get the all available package data
    for each product from backoffice.
    """
    def __init__(self):
        self.__url = None
        self.__available_packages = None
        self.siteid_file = SITEID_FILE
        self.__audit_data = None

    @property
    def _site_id(self):
        return linecache.getline(self.siteid_file, 1).strip()

    @property
    def url(self):
        if self.__url is None:
            self.__url = PACKAGE_CONFIG['URL'] + '/' + self._site_id
        return self.__url

    @property
    def available_packages(self):
        if self.__available_packages is None:
            self.__available_packages = self._get_available_packages()
        return self.__available_packages

    def __safe_get(self):
        try:
            logger.info('SWD: Getting package list from url: %s', self.url)
            response = requests.get(self.url, headers=PACKAGE_CONFIG['HEADERS'], verify=False, timeout=10)
            response.raise_for_status()
        except requests.exceptions.RequestException as ex:
            logger.error('SWD: Failed to get data from url: %s failed with exception: %s' % (self, str(ex)))
            return {}
        return response.json()

    def _get_available_packages(self):
        response_data = self.__safe_get()
        return response_data.get('package_data') if response_data else []


class FilterPackageData(object):
    """
    This is the class where we are filtering the available packages based on configured value in threshold.cfg
    If configured value is
    6 ==> Maintain 6 latest packages on Neb for each product and delete rest.
    3 ==> Maintain 3 latest packages on Neb for each product and delete rest

    The default value is 100 if not specified
    """
    DEFAULT_COUNT = 100

    def __init__(self, data):
        self.data = data
        self.threshold_value = get_threshold_value("$RETAINED_PACKAGES$")

    @property
    def _package_count(self):
        return int(self.threshold_value) if self.threshold_value else self.DEFAULT_COUNT

    def filter_data(self):
        self.data.sort(key=lambda x: x['timestamp'])
        del self.data[-self._package_count:]
        return self.data


class FilterProductData(FilterPackageData):
    """
    This class filters available package list based on the product value.
    """
    def __init__(self, product_value, data):
        super(FilterProductData, self).__init__(data)
        self.product_value = product_value

    @property
    def _package_count(self):
        return int(self.product_value) if self.product_value else self.DEFAULT_COUNT
