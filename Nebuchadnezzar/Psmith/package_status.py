import os
import requests
import linecache
import logging
from phimutils.pcm import PCMPackage
from phimutils.message import Message
from scanline.component.localhost import LocalHost
from tbconfig import VERSION, SITEID_FILE, DEPLOYMENT_STATUS, ENDPOINT, DISCOVERY
from enums import PackageStatus
from lxml import etree

logger = logging.getLogger(__name__)

class PackageUpdate(object):
    def __init__(self, filename):
        self.__filename = filename

    @property
    def file_path(self):
        return self.__filename

    @property
    def xml_file(self):
        return os.path.splitext(self.__filename)[0] + ''.join('.xml')

    @property
    def file_extension(self):
        return os.path.splitext(self.__filename)[1]

    def get_package_data(self, status):
        """
        Returns package data which will get posted to backoffice
        :param status: Package status
        :return: package data type dict
        """
        package_list = []
        package_data = None

        # If zip file being downloaded which means respected xml is already downloaded.
        # Read the respected xml data to post it to backoffice.
        if self.file_extension == '.zip':
            try:
                xml_file = self.xml_file
                package = PCMPackage(xml_file)
            except IOError as e:
                logger.exception("No Xml found: Failed with Exception: %s", str(e))
                return None
            package_list.append({
                'name': package.Name + ''.join(package.Version.split('.')),
                'version': package.Version.replace('.', ','),
                'status': status
            })
            data = {'localhost': {'PCM': package_list}}
            data['localhost'].update(LocalHost().get_attributes())
            package_data = {'facts': data}
        return package_data

    @property
    def site_id(self, siteid=None, siteid_file=SITEID_FILE):
        """
        Returns site ID from linecache
        """
        if not siteid:
            siteid = linecache.getline(siteid_file, 1).strip()
        return siteid

    def send_data_to_backoffice(self, url, data):
        """
        Posting data to backoffice
        :param url: Destination url type str
        :param data: type dict
        :return: None
        """
        siteid = self.site_id
        if not siteid:
            raise Exception('Could not post due to missing siteid')
        message = Message(siteid=siteid, payload=data)
        req = requests.post(url, json=message.to_dict())
        req.raise_for_status()

    def update_status(self, status):
        """
        This method sends Downloading started/Failed status to backoffice
        :param status: Package status i.e Downloading started or Failed
        :return: None
        """
        package_data = self.get_package_data(status)

        if package_data:
            self.send_data_to_backoffice(VERSION['ENDPOINT'], data=package_data)

    def update_download_status(self):
        self.update_status(status=PackageStatus.STARTED)

    def update_failed_status(self):
        self.update_status(status=PackageStatus.FAILED)


class PackageSubscriptions(PackageUpdate):
    def __init__(self, filepath):
        super(PackageSubscriptions, self).__init__(filepath)
        self._filename = os.path.basename(filepath)
        self._version = None

    @property
    def name(self):
        return os.path.splitext(self._filename)[0]

    @property
    def subscription_id(self):
        return self.name

    @property
    def version(self):
        if not self._version:
            self._version = self.get_package_version()
        return self._version

    def get_package_version(self):
        """
        This method returns a version from a package file name:
        Exp:
        Lets say package file name is: ISP-4.4.551.4.201712052034.xml
        1. Split the name with '-'.
        2. Replace dots(.) with comma (,)
        o/p version = '4,4,551,4,201712052034'

        """
        try:
            package_files = self.get_package_files()
            package_file = package_files[0] if package_files else None
            if package_file is None:
                return None
            full_version = os.path.splitext(package_file)[0].split('-')[-1]
            version = ','.join(full_version.split('.'))
            return version
        except Exception as e:
            logger.exception("Exception occurred while getting package version: %s", str(e))
            return None

    def get_package_files(self):
        """
        This method reads the data from subscription xml present on Neb node at loc: /etc/philips/psmith/subscriptions
        Subscription xml will have two files (package xml and package zip file).
        Read both files and return.
        """
        tree = etree.parse(self.file_path)
        result = [file.get('name') for file in tree.iter('file')]
        return result

    def get_package_data(self, status):
        """
        Prepare a list with package name and its version.
        """
        package_list = []

        package_list.append({
            'name': self.name,
            'version': self.version,
            'status': status,
            'subscription_id': self.subscription_id
        })
        data = {'localhost': {'PCM': package_list}}
        data['localhost'].update(LocalHost().get_attributes())
        package_data = {'facts': data}
        return package_data

    def update_subscribed_status(self):
        self.update_status(status=PackageStatus.SUBSCRIBED)


class UpdateStatus(PackageUpdate):
    """
    This class updates the Deleted status in backoffice Audit collection once
    subscription file is deleted from neb and SVN.
    """
    def __init__(self, data):
        super(UpdateStatus, self).__init__(data.get('package_file'))
        self.data = data

    def _get_package_list(self, status):
        package_list = []
        package_list.append({
            'name': self.data.get('name'),
            'version': self.data.get('version'),
            'status': status,
            'fsize': self.data.get('size'),
            'subscription_id': self.data.get('subscription_id')
        })
        return package_list

    def get_package_data(self, status):
        data = {'localhost': {'PCM': self._get_package_list(status)}}
        data['localhost'].update(LocalHost().get_attributes())
        return {'facts': data}

    def update_deleted_status(self):
        self.update_status(status=PackageStatus.DELETED)


class DeploymentStatus(object):
    """
    This class updates the deployment status in back office audit collection
    Once siteinfo is deleted from Neb and SVN
    """

    @property
    def _siteid(self):
        return linecache.getline(SITEID_FILE, 1).strip()

    def _status_data(self, deployment_id):
        data = {
            'deployment_id': deployment_id,
            'status': DEPLOYMENT_STATUS['DELETED'],
            'output': 'Deployment request cancelled as respective package is deleted'
        }
        return data

    def _update_deployment_status(self, url, data, verify=ENDPOINT['VERIFY']):
        message = Message(siteid=self._siteid, payload=data)
        request = requests.post(url, json=message.to_dict(), verify=verify)
        request.raise_for_status()

    def update(self, deployment_id):
        self._update_deployment_status(DISCOVERY['DEPLOYMENT'], self._status_data(deployment_id))
