import os
from lxml import etree
from glob import glob
from tbconfig import PCM


class Subscription(object):
    def __init__(self):
        self.__subscription_data = None

    @property
    def subscription_data(self):
        if not self.__subscription_data:
            self.__subscription_data = self._get_subscription_data()
        return self.__subscription_data

    def _filebasename(self, filepath):
        return os.path.basename(filepath)

    def _splitext(self, filename):
        return os.path.splitext(filename)

    def _get_package_xml(self, sub_xml):
        tree = etree.parse(sub_xml)
        return [file.get('name') for file in tree.iter('file') if self._splitext(file.get('name'))[1] == '.xml']

    def _get_subscription_data(self):
        xml_file_iter = (xml_file for xml_file in glob(os.path.join(PCM['SUBSCRIPTION_PATH'], '*.xml')))
        subscriptions = []
        for sub_xml in xml_file_iter:
            package_xml = self._get_package_xml(sub_xml)
            data = {
                'subscription_id': self._splitext(self._filebasename(sub_xml))[0],
                'package_xml': package_xml[0] if package_xml else '',
            }
            subscriptions.append(data)
        return subscriptions

    def get_subscription_id(self, package_xml):
        subscription_ids = [subscription.get('subscription_id') for subscription in self.subscription_data if
                            subscription.get('package_xml') == self._filebasename(package_xml)]
        return subscription_ids[0] if subscription_ids else None

