import os
from xml.dom import minidom
from glob import glob
from tbconfig import PACKAGE_CONFIG


class SiteInfo(object):
    """
    This class is responsible to get the data from siteinfos present under /var/philips/info.
    o/p data format:
    [{'deployment_id':'XXXXXXX', 'name': 'ISP', 'version': '4,4,554,33,34201756'},{}]
    """
    def __init__(self):
        self.__siteinfo_data = None
        self.__final_data = []

    @property
    def _siteinfo_data(self):
        if self.__siteinfo_data is None:
            self.__siteinfo_data = self._get_siteinfo_data()
        return self.__siteinfo_data

    def _get_package_element(self, siteinfo_xml):
        return minidom.parse(siteinfo_xml).getElementsByTagName('PackageInfo')[0]

    def __get_deployment_id(self, siteinfo_xml):
        return os.path.splitext(siteinfo_xml)[0].split('_')[1]

    def __update_final_data(self, siteinfo_xml, package_info_element):
        siteinfo_data = {
            'product_name': package_info_element.getAttribute('Name'),
            'version': package_info_element.getAttribute('Version'),
            'deployment_id': self.__get_deployment_id(siteinfo_xml)
        }
        self.__final_data.append(siteinfo_data)

    def _get_siteinfo_data(self):
        xml_file_iter = (xml_file for xml_file in glob(os.path.join(PACKAGE_CONFIG['SITE_INFO_PATH'], '*.xml')))
        for siteinfo_xml in xml_file_iter:
            package_info_element = self._get_package_element(siteinfo_xml)
            self.__update_final_data(siteinfo_xml, package_info_element)
        return self.__final_data

    def get_data(self):
        return self._siteinfo_data
