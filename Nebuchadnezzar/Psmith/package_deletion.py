from deletion import DeletePackage, DeleteSubscription, DeleteSiteInfo
from package_data import FilterProductData


class ContextDelete(object):
    # This is a context class to execute the respective deletion at runtime.
    def __init__(self, delete_obj):
        self.delete_obj = delete_obj

    def delete(self):
        return self.delete_obj.delete()


class DeletionClass(object):
    PACKAGE = 'package'
    SUBSCRIPTION = 'subscription'
    SITEINFO = 'siteinfo'

    # Add any other delete operation class for a package here.
    CLASSES = {
        PACKAGE: DeletePackage,
        SUBSCRIPTION: DeleteSubscription,
        SITEINFO: DeleteSiteInfo
    }


class PackageClasses(object):
    # This class returns respective deletion class.
    def __init__(self):
        self.classes = DeletionClass.CLASSES

    def get_deletion_class(self, class_name):
        return self.classes[class_name]


class PackageDeletion(object):
    """
    A package deletion class which is responsible for deleting below data.
    1. Initiate delete package file based on the configured value.
    2. Initiate delete Subscription file if any for that package.
    3. Initiate delete Siteinfo file if any for that package.
    """
    def __init__(self, product_name, product_value, data):
        self.product_name = product_name
        self.filter_obj = FilterProductData(product_value, data)
        self.package_obj = PackageClasses()
        self.ContextDelete = ContextDelete
        self._filtered_data = None

    @property
    def filtered_package(self):
        if self._filtered_data is None:
            self._filtered_data = self.filter_obj.filter_data()
        return self._filtered_data

    def _get_class_name(self, name):
        return self.package_obj.get_deletion_class(name)

    def _get_context_delete_response(self, class_name, package):
        return self.ContextDelete(class_name(self.product_name, package)).delete()

    def _delete_package_related_data(self, package):
        pkg_msg = ""
        deletion_classes = [self._get_class_name(DeletionClass.SUBSCRIPTION),
                            self._get_class_name(DeletionClass.SITEINFO)]
        for class_name in deletion_classes:
            status, message = self._get_context_delete_response(class_name, package)
            pkg_msg += message
        return pkg_msg

    def _delete_package(self, package):
        msg = ""
        class_name = self._get_class_name(DeletionClass.PACKAGE)
        status, message = self._get_context_delete_response(class_name, package)
        if status:
            msg = self._delete_package_related_data(package)
        return message + msg

    def delete(self):
        delete_msg = ""
        for package in self.filtered_package:
            delete_msg += self._delete_package(package)
        return delete_msg
