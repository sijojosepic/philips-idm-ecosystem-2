import unittest
from mock import MagicMock, patch

class SiteInfoTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_xml = MagicMock(name="xml")
        self.mock_glob = MagicMock(name="glob")
        self.mock_tbconfig = MagicMock(name="tbconfig")

        modules = {
            'xml': self.mock_xml,
            'xml.dom': self.mock_xml.dom,
            'glob': self.mock_glob,
            'tbconfig': self.mock_tbconfig
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import siteinfo
        self.module = siteinfo
        self.SiteInfo = siteinfo.SiteInfo

        self.module.PACKAGE_CONFIG = {'SITE_INFO_PATH': '/test/subscription_path'}
        self.siteinfo_obj = self.SiteInfo()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test__get_package_element(self):
        mock_element = MagicMock(name="minidon_parse")
        mock_element.getElementsByTagName.return_value = ['packageinfo']
        self.mock_xml.dom.minidom.parse.return_value = mock_element
        self.assertEqual(self.siteinfo_obj._get_package_element('/test/siteinfo.xml'), "packageinfo")

    def test__get_siteinfo_data(self):
        self.mock_glob.glob.return_value = ['/test/subscription/siteinfo1_fefef432gv.xml']
        mock_element = MagicMock(name="package_info_element")
        mock_element.getAttribute.side_effect = ['ISP','4,4,553,23']
        self.siteinfo_obj._get_package_element = MagicMock(name="_get_package_element", return_value=mock_element)
        exp_value = [{'deployment_id': 'fefef432gv', 'version': '4,4,553,23', 'product_name': 'ISP'}]
        self.assertEqual(self.siteinfo_obj._get_siteinfo_data(), exp_value)

    def test_get_data(self):
        value = [{'deployment_id': 'fefef432gv', 'name': 'ISP', 'version': '4,4,554,33,34201756'}]
        self.siteinfo_obj._get_siteinfo_data = MagicMock(name="_get_siteinfo_data", return_value=value)
        self.assertEqual(self.siteinfo_obj.get_data(), value)

if __name__ == '__main__':
    unittest.main()
