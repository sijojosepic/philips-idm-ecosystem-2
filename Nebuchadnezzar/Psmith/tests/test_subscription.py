import unittest
from mock import MagicMock, patch


class SubscriptionTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_lxml = MagicMock(name="lxml")
        self.mock_tbconfig = MagicMock(name="tbconfig")
        self.mock_glob = MagicMock(name="glob")

        modules = {
            'tbconfig': self.mock_tbconfig,
            'lxml': self.mock_lxml,
            'lxml.etree': self.mock_lxml.etree,
            'glob': self.mock_glob
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import subscription
        self.module = subscription
        self.Subscription = subscription.Subscription
        self.subscription_obj = self.Subscription()
        self.mock_tbconfig.PCM = '/test/subs/'

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_subscription_data(self):
        data = [{'subscription_id': 'sub', 'package_xml': 'sub1.xml'}]
        self.subscription_obj._get_subscription_data = MagicMock(name="_get_subscription_data", return_value=data)
        result = self.subscription_obj.subscription_data
        self.assertEqual(result, data)

    def test__filebasename(self):
        self.assertEqual(self.subscription_obj._filebasename('/test/sub.xml'), 'sub.xml')

    def test__splitext(self):
        self.assertEqual(self.subscription_obj._splitext('sub.xml'), ('sub', '.xml'))

    def test__get_package_xml(self):
        mock_tree = MagicMock(name='tree')
        mock_tree.iter.return_value = [{'name': '/test/sub1.xml'}]
        self.mock_lxml.etree.parse.return_value = mock_tree
        result = self.subscription_obj._get_package_xml('test/sub.xml')
        self.assertEqual(result, ['/test/sub1.xml'])

    def test__get_subscription_data(self):
        self.mock_glob.glob.return_value = ['/test/sub1.xml']
        self.subscription_obj._get_package_xml = MagicMock(name='_get_package_xml', return_value = ['sub1.xml'])
        result = self.subscription_obj._get_subscription_data()
        self.assertEqual(result, [{'package_xml': 'sub1.xml', 'subscription_id': 'sub1'}])

    def test_get_subscription_id(self):
        data = [{'subscription_id': 'sub', 'package_xml': 'sub1.xml'}]
        self.subscription_obj._get_subscription_data = MagicMock(name="_get_subscription_data", return_value=data)
        result = self.subscription_obj.get_subscription_id('/test/sub1.xml')
        self.assertEqual(result, 'sub')


if __name__ == '__main__':
    unittest.main()
