import unittest
from mock import MagicMock, patch, PropertyMock


class FakeSiteInfo(object):
    def get_data(self):
        return [{'product_name': 'ISP', 'version': '4,4,553,23', 'deployment_id': 'ewf3432rf'}]


class DeletionTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_tbconfig = MagicMock(name="tbconfig")
        self.mock_package_status = MagicMock(name="package_status")
        self.mock_siteinfo = MagicMock(name="siteinfo")

        modules = {
            'logging': self.mock_logging,
            'tbconfig': self.mock_tbconfig,
            'package_status': self.mock_package_status,
            'siteinfo': self.mock_siteinfo
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import deletion
        self.module = deletion
        self.DeletePackage = deletion.DeletePackage
        self.DeleteSubscription = deletion.DeleteSubscription
        self.DeleteSiteInfo = deletion.DeleteSiteInfo
        self.Delete = deletion.Delete
        self.module.PCM = {'REPO_PATH':'test/path', 'SUBSCRIPTION_PATH': '/test/subscription_path'}
        self.module.PACKAGE_CONFIG = {'SITE_INFO_PATH': '/test/siteinfo'}

        self.data = {'package_file': "ISP-4.4.553.33.zip", "subscription_id": "ISP4455333", 'version':'4,4,553,23'}
        self.delete_pkg = self.DeletePackage("ISP", self.data)
        self.delete_subscription = self.DeleteSubscription("ISP", self.data)
        self.delete_siteinfo = self.DeleteSiteInfo("ISP", self.data)


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_delete_get_data(self):
        self.delete_obj = self.Delete()
        try:
            self.delete_obj.get_data()
        except NotImplementedError:
            pass

    def test_delete_delete(self):
        self.delete_obj = self.Delete()
        try:
            self.delete_obj.delete()
        except NotImplementedError:
            pass

    def test__xml_file(self):
        self.assertEqual(self.delete_pkg._xml_file, 'test/path/ISP/ISP-4.4.553.33.xml')

    def test__package_file(self):
        self.assertEqual(self.delete_pkg._package_file, 'test/path/ISP/ISP-4.4.553.33.zip')

    def test_get_data(self):
        exp_out = ['test/path/ISP/ISP-4.4.553.33.zip', 'test/path/ISP/ISP-4.4.553.33.xml']
        self.assertEqual(self.delete_pkg.get_data(), exp_out)

    def test_delete(self):
        value = ['test/path/ISP/ISP-4.4.553.33.zip']
        self.delete_pkg.get_data = MagicMock(name="get_data", return_value=value)
        self.module.os.remove = MagicMock(name="os", return_value=True)

        exp_value = (True, 'Deleted package file test/path/ISP/ISP-4.4.553.33.zip.\n')
        self.assertEqual(self.delete_pkg.delete(), exp_value)

    def test_delete_exception(self):
        value = ['test/path/ISP/ISP-4.4.553.33.zip']
        self.delete_pkg.get_data = MagicMock(name="get_data", return_value=value)
        self.module.os.remove = MagicMock(name="os")
        self.module.os.remove.side_effect = OSError

        exp_value = (False, 'Failed to delete package file test/path/ISP/ISP-4.4.553.33.zip.\n')
        self.assertEqual(self.delete_pkg.delete(), exp_value)

    def test__subscription_file(self):
        self.assertEqual(self.delete_subscription._subscription_file, "/test/subscription_path/ISP4455333.xml")

    def test__update_package_status(self):
        self.mock_package_status.UpdateStatus = MagicMock(name='UpdateStatus')
        self.assertEqual(self.delete_subscription._update_package_status(), None)

    def test_get_data_subscription(self):
        self.assertEqual(self.delete_subscription.get_data(), '/test/subscription_path/ISP4455333.xml')

    def test_delete_subscription(self):
        value = '/test/subscription_path/ISP4455333.xml'
        self.delete_subscription.get_data = MagicMock(name="get_data", return_value=value)
        self.module.os.remove = MagicMock(name="os", return_value=True)
        self.delete_subscription._update_package_status = MagicMock(name="_update_package_status", return_value=True)

        exp_value = (True, 'Deleted subscription file /test/subscription_path/ISP4455333.xml.\n')
        self.assertEqual(self.delete_subscription.delete(), exp_value)

    def test_delete_subscription_exception(self):
        self.delete_subscription.get_data = MagicMock(name="get_data")
        self.delete_subscription.get_data.side_effect = OSError
        self.delete_subscription._update_package_status = MagicMock(name="_update_package_status", return_value=True)
        exp_value = (False, '')
        self.assertEqual(self.delete_subscription.delete(), exp_value)

    def test__siteinfo_data(self):
        exp_value = [{'product_name':'ISP', 'version':'4,4,553,23', 'deployment_id':'ewf3432rf'}]
        self.mock_siteinfo.SiteInfo.return_value = FakeSiteInfo()
        self.delete_siteinfo = self.DeleteSiteInfo("ISP", self.data)
        self.assertEqual(self.delete_siteinfo._siteinfo_data, exp_value)

    def test__delete_siteinfos(self):
        self.module.os.remove = MagicMock(name="os", return_value=True)
        self.assertEqual(self.delete_siteinfo._delete_siteinfos('ewf3432rf'), None)
        self.assertEqual(self.module.os.remove.call_count, 2)

    def test_get_data_siteinfo(self):
        self.mock_siteinfo.SiteInfo.return_value = FakeSiteInfo()
        self.delete_siteinfo = self.DeleteSiteInfo("ISP", self.data)
        self.assertEqual(self.delete_siteinfo.get_data(), ['ewf3432rf'])

    def test_delete_siteinfo(self):
        self.delete_siteinfo.get_data = MagicMock(name="get_data", return_value=['ewf3432rf'])
        self.delete_siteinfo._delete_siteinfos = MagicMock(name='_delete_siteinfos', return_value=True)
        self.delete_siteinfo.deployment_status.update = MagicMock(name="update", return_value=True)
        self.assertEqual(self.delete_siteinfo.delete(), (True, 'Deleted Siteinfo with deployment id: ewf3432rf\n'))
        self.delete_siteinfo._delete_siteinfos.assert_called_once_with('ewf3432rf')
        self.delete_siteinfo.deployment_status.update.assert_called_once_with('ewf3432rf')

    def test_delete_siteinfo_exception(self):
        self.delete_siteinfo.get_data = MagicMock(name="get_data", return_value=['ewf3432rf'])
        self.delete_siteinfo._delete_siteinfos = MagicMock(name='_delete_siteinfos')
        self.delete_siteinfo._delete_siteinfos.side_effect = OSError
        expt_value = (False, 'Failed to delete siteinfo with deployment id: ewf3432rf\n')
        self.assertEqual(self.delete_siteinfo.delete(), expt_value)


if __name__ == '__main__':
    unittest.main()
