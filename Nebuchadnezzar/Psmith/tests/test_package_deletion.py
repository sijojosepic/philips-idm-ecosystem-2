import unittest
from mock import MagicMock, patch, PropertyMock

class FakeContextDelete(object):
    def __init__(self, delete_obj):
        self.delete_obj = delete_obj

    def delete(self):
        return True, "Package Deleted"

class FakeDeletion(object):
    def __init__(self, product_name, data):
        self.product_name = product_name
        self.data = data

    def delete(self):
        pass


class PackageDeletionTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_deletion = MagicMock(name="deletion")
        self.mock_package_data = MagicMock(name="package_data")

        modules = {
            'deletion': self.mock_deletion,
            'package_data': self.mock_package_data,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import package_deletion
        self.module = package_deletion
        self.PackageDeletion = package_deletion.PackageDeletion
        self.ContextDelete = package_deletion.ContextDelete
        self.PackageClasses = package_deletion.PackageClasses

        self.package_class_obj = self.PackageClasses()
        self.package_deletion_obj = self.PackageDeletion('ISP', 100, [{'test':'test'}])

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_context_delete(self):
        mock_delete_obj = MagicMock(name="delete_obj")
        mock_delete_obj.delete.return_value = "Deleted Success"
        self.context_delete = self.ContextDelete(mock_delete_obj)
        self.assertEqual(self.context_delete.delete(), "Deleted Success")

    def test_package_get_deletion_class(self):
        self.package_class_obj.classes = {'fake': FakeDeletion}
        self.assertEqual(self.package_class_obj.get_deletion_class('fake'), FakeDeletion)

    def test_filtered_package(self):
        mock_obj = MagicMock(name="filter_obj")
        mock_obj.filter_data.return_value = [{'test':'test'}]
        self.package_deletion_obj.filter_obj = mock_obj
        self.assertEqual(self.package_deletion_obj.filtered_package, [{'test':'test'}])

    def test__get_class_name(self):
        mock_obj = MagicMock(name="get_class_obj")
        mock_obj.get_deletion_class.return_value = "FakeClass"
        self.package_deletion_obj.package_obj = mock_obj
        self.assertEqual(self.package_deletion_obj._get_class_name('package'), "FakeClass")

    def test__get_context_delete_response(self):
        mock_fack_obj = MagicMock(name="fake_deletion")
        self.package_deletion_obj.ContextDelete = MagicMock(name="ContextDelete",
                                                            return_value=FakeContextDelete(mock_fack_obj))
        self.assertEqual(self.package_deletion_obj._get_context_delete_response(FakeDeletion, {'test': 'test'}), (True,
                         "Package Deleted"))

    def test__delete_package_related_data(self):
        self.package_deletion_obj._get_class_name = MagicMock(name="_get_class_name",
                                                              return_value=FakeDeletion)
        self.package_deletion_obj._get_context_delete_response = MagicMock(name="_get_context_delete_response",
                                                            return_value=(True, "Package Deleted\n"))
        expt_value = 'Package Deleted\nPackage Deleted\n'
        self.assertEqual(self.package_deletion_obj._delete_package_related_data({'test':'test'}), expt_value)

    def test__delete_package(self):
        self.package_deletion_obj._get_class_name = MagicMock(name="_get_class_name",
                                                                     return_value=FakeDeletion)
        self.package_deletion_obj._get_context_delete_response = MagicMock(name="_get_context_delete_response",
                                                            return_value=(True, "Package Deleted\n"))
        self.package_deletion_obj._delete_package_related_data = MagicMock(name="_delete_package_related_data",
                                                                           return_value="site info deleted")
        self.assertEqual(self.package_deletion_obj._delete_package({'test': 'test'}), "Package Deleted\nsite info deleted")

    def test_delete(self):
        type(self.package_deletion_obj).filtered_package = PropertyMock(return_value=[{'test': 'test'}])
        self.package_deletion_obj._delete_package = MagicMock(name="_delete_package", return_value="Deleted")
        self.assertEqual(self.package_deletion_obj.delete(), "Deleted")


if __name__ == '__main__':
    unittest.main()
