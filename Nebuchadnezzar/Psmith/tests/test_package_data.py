import unittest
from mock import MagicMock, patch, PropertyMock

class MyException(Exception):
    pass

class PackageDetaTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_requests = MagicMock(name='requests')
        self.mock_phimutils = MagicMock(name="phimutils")
        self.mock_tbconfig = MagicMock(name="tbconfig")

        modules = {
            'logging': self.mock_logging,
            'linecache': self.mock_linecache,
            'requests': self.mock_requests,
            'phimutils': self.mock_phimutils,
            'phimutils.threshold': self.mock_phimutils.threshold,
            'tbconfig': self.mock_tbconfig,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import package_data
        self.module = package_data
        self.PackageData = package_data.PackageData
        self.FilterPackageData = package_data.FilterPackageData
        self.FilterProductData = package_data.FilterProductData
        self.module.PACKAGE_CONFIG = {'URL':'test/url', 'HEADERS': {'test':'test'}}
        self.mock_linecache.getline.return_value = "RAJ00"

        self.pkg_data = self.PackageData()
        self.data = [{'timestamp': "2019/12/10 12:34:10"}, {'timestamp': "2019/12/10 12:42:22"},
                {'timestamp': "2019/12/10 12:40:15"}]

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test__site_id(self):
        self.assertEqual(self.pkg_data._site_id, 'RAJ00')

    def test_url(self):
        self.assertEqual(self.pkg_data.url, 'test/url/RAJ00')

    def test_available_packages(self):
        value = [{'test1':"test1"}, {"test2":"test2"}]
        self.pkg_data._get_available_packages = MagicMock(name="_get_available_packages", return_value=value)
        self.assertEqual(self.pkg_data.available_packages, value)

    def test__get_available_packages(self):
        value = {'package_data':[{'test1': "test1"}, {"test2": "test2"}]}
        mock_request = MagicMock(name="request_get")
        mock_request.raise_for_status.return_value = True
        mock_request.json.return_value = value
        self.mock_requests.get.return_value = mock_request
        self.assertEqual(self.pkg_data._get_available_packages(), [{'test1': "test1"}, {"test2": "test2"}])

    def test__get_available_packages_exception(self):
        self.mock_requests.exceptions.RequestException = MyException
        self.mock_requests.get.side_effect = MyException
        self.assertEqual(self.pkg_data._get_available_packages(), [])

    def test_filter_data(self):
        self.mock_phimutils.threshold.get_threshold_value.return_value = '2'
        self.filter_data = self.FilterPackageData(self.data)
        self.assertEqual(self.filter_data.filter_data(), [{'timestamp': "2019/12/10 12:34:10"}])


class FilterProductDataTest(PackageDetaTest):

    def test_package_count(self):
        self.filter_product_data = self.FilterProductData(2, self.data)
        self.assertEqual(self.filter_product_data._package_count, 2)

    def test_package_count_else(self):
        self.filter_product_data = self.FilterProductData(None, self.data)
        self.assertEqual(self.filter_product_data._package_count, 100)



if __name__ == '__main__':
    unittest.main()
