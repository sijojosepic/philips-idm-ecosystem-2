# import sys
import unittest
from mock import MagicMock, patch, PropertyMock
# from lxml import etree
# from blist import sortedset


# sys.modules['phimutils.plogging'] = MagicMock(name='phimutils.plogging')
# sys.modules['phimutils'] = MagicMock(name='phimutils')


class PSmithTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pcm = MagicMock(name='phimutils.pcm')
        self.mock_message = MagicMock(name='phimutils.message')
        self.mock_lxml = MagicMock(name='lxml')
        self.mock_blist = MagicMock(name='blist')
        self.mock_shutil = MagicMock(name='shutil')
        self.mock_distutils = MagicMock(name='distutils')
        self.mock_collections = MagicMock(name='collections')
        self.mock_itertools = MagicMock(name='itertools')
        self.mock_glob = MagicMock(name='glob')
        self.mock_logging = MagicMock(name='logging')
        self.mock_time = MagicMock(name='time')
        self.mock_os = MagicMock(name='os')
        self.mock_signal = MagicMock(name='signal')
        self.mock_package_status = MagicMock(name='package_status')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phimutils.pcm': self.mock_pcm,
            'phimutils.message': self.mock_message,
            'package_status': self.mock_package_status,
            'lxml': self.mock_lxml,
            'blist': self.mock_blist,
            'shutil': self.mock_shutil,
            'distutils': self.mock_distutils,
            'distutils.dir_util': self.mock_distutils.dir_util,
            'collections': self.mock_collections,
            'itertools': self.mock_itertools,
            'glob': self.mock_glob,
            'logging': self.mock_logging,
            'time': self.mock_time,
            'os': self.mock_os,
            'signal': self.mock_signal
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.patch_filename = patch('psmith.ZSyncFile.filename', new_callable=PropertyMock)
        self.mock_filename = self.patch_filename.start()

        from psmith import PSmith, ZSyncFile
        self.PSmith = PSmith
        self.ZSyncFile = ZSyncFile
        self.ZSyncFile.PERMISSIONS = 0644
        self.zsync_file_obj = self.ZSyncFile('/test', 'http://test/philips', '/test/test.xml')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
        self.patch_filename.stop()

    def test__sortedset_from_xml_dirs(self):
        tree_mock = MagicMock()
        elm_mock = MagicMock()
        tree_mock.iter.return_value = [elm_mock]
        result = self.PSmith._sortedset_from_xml(tree_mock, 'dir')
        self.assertEqual(result, self.mock_blist.sortedset())
        tree_mock.iter.assert_called_once_with('dir')
        elm_mock.xpath.assert_called_once_with('ancestor::dir/@name')

    def test_filename(self):
        self.mock_filename.return_value = '/test/test.xml'
        result = self.zsync_file_obj.filename
        self.assertEqual(result, '/test/test.xml')

    def test_update_timestamp_success(self):
        self.mock_filename.return_value = '/test/test.xml'
        self.mock_os.system.return_value = 0
        self.zsync_file_obj.update_timestamp()
        self.mock_os.system.assert_called_once_with("touch -m /test/test.xml")

    def test_update_timestamp_failed(self):
        self.mock_filename.return_value = '/test/test.xml'
        self.mock_os.system.return_value = 1
        self.zsync_file_obj.update_timestamp()
        self.mock_os.system.assert_called_once_with("touch -m /test/test.xml")

    def test_sync(self):
        self.zsync_file_obj.run_zsync = MagicMock(name='run_zsync')
        self.zsync_file_obj.update_timestamp = MagicMock(name='update_timestamp')
        self.PSmith.delete_from_fs = MagicMock(name = 'delete_from_fs')
        self.mock_os.chmod.return_value = True
        self.zsync_file_obj.update_timestamp.return_value = True
        self.zsync_file_obj.run_zsync.return_value = True
        self.PSmith.delete_from_fs.return_value = True
        result = self.zsync_file_obj.sync()
        self.assertEqual(result, True)
        self.zsync_file_obj.update_timestamp.assert_called_once_with()
        self.zsync_file_obj.run_zsync.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
