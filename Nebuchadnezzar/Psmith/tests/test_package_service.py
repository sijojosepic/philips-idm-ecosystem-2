import unittest
from mock import MagicMock, patch


class PackageServiceTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_package_deletion = MagicMock(name="package_deletion")
        self.mock_package_data = MagicMock(name="package_data")

        modules = {
            'package_deletion': self.mock_package_deletion,
            'package_data': self.mock_package_data,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import package_service
        self.module = package_service
        self.PackageCleanUpService = package_service.PackageCleanUpService
        self.CleanUpService = package_service.CleanUpService

        self.cleanup_service_obj = self.PackageCleanUpService()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_cleanup_service_delete(self):
        self.cleanup_obj = self.CleanUpService()
        try:
            self.cleanup_obj.execute()
        except NotImplementedError:
            pass

    def test__get_available_packages(self):
        mock_package_data = MagicMock(name="package_data")
        value =[{'product_name':'ISP', 'package_list':[]}]
        mock_package_data.available_packages = value

        self.mock_package_data.PackageData.return_value = mock_package_data

        self.cleanup_service_obj = self.PackageCleanUpService()
        self.assertEqual(self.cleanup_service_obj._get_available_packages(), value)

    def test_execute(self):
        value = [{'product_name': 'ISP', 'package_list': []}]
        self.cleanup_service_obj._get_available_packages = MagicMock(name='_get_available_packages', return_value=value)

        mock_delete = MagicMock(name='PackageDeletion')
        mock_delete.delete.return_value = "Package Deleted"
        self.mock_package_deletion.PackageDeletion.return_value = mock_delete

        self.assertEqual(self.cleanup_service_obj.execute(), "Package Deleted")


if __name__ == '__main__':
    unittest.main()
