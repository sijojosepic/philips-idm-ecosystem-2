import unittest
from mock import MagicMock, patch, Mock


class PackageUpdateTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_logging = MagicMock(name='logging')
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_requests = MagicMock(name='requests')
        self.mock_enums = MagicMock(name="enums")
        self.mock_lxml = MagicMock(name="lxml")
        modules = {
            'tbconfig': self.mock_tbconfig,
            'scanline.component.localhost': self.mock_scanline.component.localhost,
            'phimutils': self.mock_phimutils,
            'phimutils.pcm': self.mock_phimutils.pcm,
            'phimutils.message': self.mock_phimutils.message,
            'linecache': self.mock_linecache,
            'requests': self.mock_requests,
            'logging': self.mock_logging,
            'scanline': self.mock_scanline,
            'scanline.component': self.mock_scanline.component,
            'enums': self.mock_enums,
            'lxml': self.mock_lxml,
            'lxml.etree': self.mock_lxml.etree
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import package_status
        self.package_status = package_status
        self.PackageUpdate = package_status.PackageUpdate
        self.PackageSubscriptions = package_status.PackageSubscriptions
        self.UpdateStatus = package_status.UpdateStatus
        self.DeploymentStatus = package_status.DeploymentStatus

        self.package_status.VERSION = {'ENDPOINT': "backoffice_url"}
        self.package_status.PackageStatus.STARTED = 'Started'
        self.package_status.PackageStatus.FAILED = 'Failed'
        self.package_status.PackageStatus.SUBSCRIBED = 'Subscribed'
        self.package_status.PackageStatus.DELETED = 'Deleted'

        self.package_status.DEPLOYMENT_STATUS = {'DELETED': 'Deleted'}
        self.package_status.ENDPOINT = {'VERIFY': False}

        self.data = {'package_file': 'somefile.zip', 'name': 'ISP44553', 'version': '4,4,553,23',
                     'subscription_id': 'ISP44553', 'size': '2 GB'}
        self.update_status = self.UpdateStatus(self.data)
        self.deployment_status = self.DeploymentStatus()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_file_path(self):
        pkg_update = self.PackageUpdate("somepath/somefile.xml")
        self.assertEqual(pkg_update.file_path, "somepath/somefile.xml")

    def test_xml_file(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.assertEqual(pkg_update.xml_file, "somefile.xml")

    def test_file_extension(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.assertEqual(pkg_update.file_extension, ".zip")

    def test_site_id(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.mock_linecache.getline.return_value = "RAJ00"
        self.assertEqual(pkg_update.site_id, 'RAJ00')

    def raise_exception(self, pkg_update):
        try:
            pkg_update.send_data_to_backoffice("backoffice_url", {'site_id': "RAJ00"})
        except Exception:
            return "Exception! Could not post due to missing siteid"

    def test_send_data_to_backoffice_case_one(self):
        # With site ID
        self.mock_linecache.getline.return_value = "RAJ00"
        pkg_update = self.PackageUpdate("somefile.zip")

        mock_message_obj = MagicMock(name='message')
        mock_message_obj.to_dict.return_value = {'site_id': 'RAJ00'}
        self.mock_phimutils.message.Message.return_value = mock_message_obj

        self.mock_requests.post.status_code.return_value = 200
        pkg_update.send_data_to_backoffice("backoffice_url", {'site_id': "RAJ00"})
        self.mock_requests.post.assert_called_once_with("backoffice_url", json={'site_id': "RAJ00"})

    def test_send_data_to_backoffice_case_two(self):
        # Without site ID
        self.mock_linecache.getline.return_value = ""
        pkg_update = self.PackageUpdate("somefile.zip")

        reponse = self.raise_exception(pkg_update)
        self.assertEqual(reponse, "Exception! Could not post due to missing siteid")
        self.mock_requests.post.assert_not_called()

    def test_get_package_data_case_one(self):
        mock_pcm_package_obj = MagicMock(name='pcm_package_obj')
        mock_pcm_package_obj.Name = 'name'
        mock_pcm_package_obj.Version = '1.3.12.0.1862.24'
        self.mock_phimutils.pcm.PCMPackage.return_value = mock_pcm_package_obj

        mock_localhost_obj = MagicMock(name='LocalHost')
        mock_localhost_obj.get_attributes.return_value = {'a': 'b'}
        self.mock_scanline.component.localhost.LocalHost.return_value = mock_localhost_obj

        updator_obj = self.PackageUpdate('something.zip')
        response = updator_obj.get_package_data('falied')
        expected_output = {'facts': {'localhost': {'a': 'b', 'PCM': [{'status': 'falied', 'version': '1,3,12,0,1862,24', 'name': 'name13120186224'}]}}}

        self.assertEqual(response, expected_output)

        self.mock_phimutils.pcm.PCMPackage.assert_called_once_with('something.xml')
        self.mock_scanline.component.localhost.LocalHost.assert_called_once_with()
        mock_localhost_obj.get_attributes.assert_called_once_with()

    def test_get_package_data_case_two(self):
        self.mock_phimutils.pcm.PCMPackage.side_effect = IOError

        pkg_obj = self.PackageUpdate('something.zip')
        self.assertEqual(pkg_obj.get_package_data('falied'), None)
        self.mock_scanline.component.localhost.LocalHost.assert_not_called()

    def test_update_status_case_one(self):
        pkg_update = self.PackageUpdate("somefile.zip")

        self.PackageUpdate.get_package_data = MagicMock(name='get_package_data')
        self.PackageUpdate.get_package_data.return_value = {'Key1' : 'val1'}
        self.PackageUpdate.send_data_to_backoffice = MagicMock(name='send_data_to_backoffice')

        pkg_update.update_status("Failed")
        self.PackageUpdate.send_data_to_backoffice.assert_called_once_with("backoffice_url", data={'Key1' : 'val1'})
        self.PackageUpdate.get_package_data.assert_called_once_with("Failed")

    def test_update_status_case_two(self):
        # With Empty package data
        self.PackageUpdate.get_package_data = MagicMock(name='get_package_data')
        self.PackageUpdate.get_package_data.return_value = None
        self.PackageUpdate.send_data_to_backoffice = MagicMock(name='send_data_to_backoffice')

        pk_update = self.PackageUpdate("somefile.zip")
        pk_update.update_status("Failed")

        self.PackageUpdate.get_package_data.assert_called_once_with("Failed")
        self.PackageUpdate.send_data_to_backoffice.assert_not_called()

    def test_update_download_status(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.PackageUpdate.update_status = MagicMock(name='update_status')

        pkg_update.update_download_status()
        self.PackageUpdate.update_status.assert_called_once_with(status="Started")

    def test_update_failed_status(self):
        pkg_update = self.PackageUpdate("somefile.zip")
        self.PackageUpdate.update_status = MagicMock(name='update_status')

        pkg_update.update_failed_status()
        self.PackageUpdate.update_status.assert_called_once_with(status="Failed")


class PackageSubscriptionsTest(PackageUpdateTest):

    def test_name(self):
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.name , "somefile")

    def test_subscription_id(self):
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.subscription_id, "somefile")

    def test_version(self):
        self.PackageSubscriptions.get_package_version = MagicMock(name="get_package_version", return_value="x.y.z")
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.version, "x.y.z")

    def test_get_package_version_case_one(self):
        xml_files = ['ISP-4.4.551.4.201712052034.xml']
        self.PackageSubscriptions.get_package_files = MagicMock(name='package_file')
        self.PackageSubscriptions.get_package_files.return_value = xml_files
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.get_package_version(), "4,4,551,4,201712052034")

    def test_get_package_version_case_two(self):
        xml_files = []
        self.PackageSubscriptions.get_package_files = MagicMock(name='package_file')
        self.PackageSubscriptions.get_package_files.return_value = xml_files
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.get_package_version(), None)

    def test_get_package_version_case_three(self):
        self.PackageSubscriptions.get_package_files = MagicMock(name='package_file')
        self.PackageSubscriptions.get_package_files.side_effect = Exception
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.get_package_version(), None)

    def test_get_package_files(self):
        mock_tree = MagicMock(name='tree')
        mock_tree.iter.return_value = [{'name': 'f1.xml'}]
        self.mock_lxml.etree.parse.return_value = mock_tree
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.get_package_files(), ['f1.xml'])

    def test_get_package_data(self):
        mock_localhost_obj = MagicMock(name='LocalHost')
        mock_localhost_obj.get_attributes.return_value = {'a': 'b'}
        self.mock_scanline.component.localhost.LocalHost.return_value = mock_localhost_obj
        expected_output = {'facts': {'localhost': {'a': 'b', 'PCM': [
            {'status': 'Subscribed', 'version': None, 'name': 'somefile', 'subscription_id': 'somefile'}]}}}
        subscription = self.PackageSubscriptions("path/somefile.xml")
        self.assertEqual(subscription.get_package_data("Subscribed"), expected_output)

    def test_update_subscribed_status(self):
        self.PackageSubscriptions.update_status = MagicMock(name="update_status")
        subscription = self.PackageSubscriptions("path/somefile.xml")
        subscription.update_subscribed_status()
        self.PackageSubscriptions.update_status.assert_called_once_with(status='Subscribed')


class UpdateStatusTest(PackageUpdateTest):

    def test__get_package_list(self):
        exp_result = [{'status': 'Deleted', 'subscription_id': 'ISP44553', 'version': '4,4,553,23', 'fsize': '2 GB',
                       'name': 'ISP44553'}]
        self.assertEqual(self.update_status._get_package_list('Deleted'), exp_result)

    def test_get_package_data(self):
        value = [{'status': 'Deleted', 'subscription_id': 'ISP44553', 'version': '4,4,553,23'}]
        self.update_status._get_package_list = MagicMock(name="_get_package_list", return_value=value)
        mock_attr = MagicMock(name="get_attributes")
        mock_attr.get_attributes.return_value = {'test': 'test'}
        self.mock_scanline.component.localhost.LocalHost.return_value = mock_attr
        exp_result = {'facts': {'localhost': {'test': 'test', 'PCM': [
            {'status': 'Deleted', 'subscription_id': 'ISP44553', 'version': '4,4,553,23'}]}}}
        self.assertEqual(self.update_status.get_package_data('Deleted'), exp_result)

    def test_update_deleted_status(self):
        self.update_status.update_status = MagicMock(name="update_status", return_value=True)
        self.assertEqual(self.update_status.update_deleted_status(), None)
        self.update_status.update_status.assert_called_once_with(status='Deleted')


class DeploymentStatusTest(PackageUpdateTest):

    def test__siteid(self):
        self.mock_linecache.getline.return_value = "RAJ00"
        self.assertEqual(self.deployment_status._siteid, "RAJ00")

    def test__status_data(self):
        exp_result = {'status': 'Deleted', 'output': 'Deployment request cancelled as respective package is deleted',
                      'deployment_id': 'dsds321c'}
        self.assertEqual(self.deployment_status._status_data('dsds321c'), exp_result)

    def test__update_deployment_status(self):
        mock_message_obj = MagicMock(name='message')
        mock_message_obj.to_dict.return_value = {'site_id': 'RAJ00'}
        self.mock_phimutils.message.Message.return_value = mock_message_obj
        self.mock_requests.post.status_code.return_value = 200
        self.assertEqual(self.deployment_status._update_deployment_status('test/url', {'test': 'test'}), None)

    def test_update(self):
        self.package_status.DISCOVERY = {'DEPLOYMENT': 'deployment'}
        self.deployment_status._update_deployment_status = MagicMock(name="_update_deployment_status", return_value=True)
        self.deployment_status._status_data = MagicMock(name="_status_data", return_value={'test':'test'})
        self.assertEqual(self.deployment_status.update({}), None)
        self.deployment_status._update_deployment_status.assert_called_once_with('deployment', {'test': 'test'})


if __name__ == '__main__':
    unittest.main()
