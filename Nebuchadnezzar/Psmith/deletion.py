import os
import logging
from package_status import UpdateStatus, DeploymentStatus
from siteinfo import SiteInfo
from tbconfig import PCM, PACKAGE_CONFIG

logger = logging.getLogger(__name__)


class Delete:
    def get_data(self):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError


class DeletePackage(Delete):
    """
    This class is responsible to delete the package files from Neb.
    There will be two package file files.
    Ex:
    ISP-4.4.552.11.201712051356.zip
    ISP-4.4.552.11.201712051356.xml
    """
    def __init__(self, product_name, data):
        self.product_name = product_name
        self.package_data = data
        self.__package_file = None

    @property
    def _xml_file(self):
        return os.path.splitext(self._package_file)[0] + ''.join('.xml')

    @property
    def _package_file(self):
        if self.__package_file is None:
            self.__package_file = PCM['REPO_PATH'] + '/' + self.product_name + '/' + self.package_data.get(
                "package_file")
        return self.__package_file

    def get_data(self):
        return [self._package_file, self._xml_file]

    def delete(self):
        status, message = False, ""
        for file in self.get_data():
            try:
                msg = "Deleted package file {0}.\n".format(file)
                os.remove(file)
                status = True
            except OSError as ex:
                msg = "Failed to delete package file {0}.\n".format(file)
                logger.exception("{0} with with exception: {1}".format(msg, str(ex)))
            logger.info(msg)
            message += msg
        return status, message


class DeleteSubscription(Delete):
    """
    This class is responsible to delete subscription file from Neb and svn.
    Also update the status in backoffice audit collection.
    """
    def __init__(self, product_name, data):
        self.product_name = product_name
        self.package_data = data
        self.update_status = UpdateStatus(data)
        self.__subscription_file = None

    @property
    def _subscription_file(self):
        if self.__subscription_file is None:
            self.__subscription_file = "{path}/{subscription}.xml".format(path=PCM['SUBSCRIPTION_PATH'],
                                                  subscription=self.package_data.get('subscription_id'))
        return self.__subscription_file

    def _update_package_status(self):
        self.update_status.update_deleted_status()

    def get_data(self):
        return self._subscription_file

    def delete(self):
        status, message = False, ""
        try:
            os.remove(self.get_data())
            message = "Deleted subscription file {0}.\n".format(self._subscription_file)
            status = True
        except Exception:
            return False, message
        finally:
            self._update_package_status()
        logger.info(message)
        return status, message



class DeleteSiteInfo(Delete):
    """
    This class is responsible to delete siteinfo file from neb and svn.
    Also update the deployment status in back office audit collection.
    """
    def __init__(self, product_name, data):
        self.product_name = product_name
        self.package_data = data
        self.siteinfo = SiteInfo()
        self.deployment_status = DeploymentStatus()
        self.__siteinfo_path = PACKAGE_CONFIG['SITE_INFO_PATH']

    @property
    def _siteinfo_data(self):
            return self.siteinfo.get_data()

    def __delete_file(self, path):
        os.remove(path)

    def __delete_siteinfo_xml(self, id):
        self.__delete_file('{path}/SiteInfo_{deploymentid}.xml'.format(path=self.__siteinfo_path, deploymentid=id))

    def __delete_siteinfo_json(self, id):
        self.__delete_file('{path}/pcm_manifest_{deploymentid}.json'.format(path=self.__siteinfo_path, deploymentid=id))

    def _delete_siteinfos(self, id):
        self.__delete_siteinfo_xml(id)
        self.__delete_siteinfo_json(id)

    def get_data(self):
        return [data.get('deployment_id') for data in self._siteinfo_data if (
                self.product_name == data.get('product_name') and self.package_data.get('version') == data.get(
            'version'))]

    def delete(self):
        status, message = False, ""
        for deployment_id in self.get_data():
            try:
                msg = "Deleted Siteinfo with deployment id: {0}\n".format(deployment_id)
                self._delete_siteinfos(deployment_id)
                self.deployment_status.update(deployment_id)
                status = True
            except OSError as ex:
                msg = "Failed to delete siteinfo with deployment id: {0}\n".format(deployment_id)
                logger.exception("{0} with with exception: {1}".format(msg, str(ex)))
            logger.info(msg)
            message += msg
        return status, message
