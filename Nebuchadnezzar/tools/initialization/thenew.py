#!/usr/bin/env python
import npyscreen


class myEmployeeForm(npyscreen.Form):
    def afterEditing(self):
        if self.myName2.value == 'mm':
            self.parentApp.setNextForm(None)
        else:
            self.parentApp.setNextForm('MAIN')
            #

    def create(self):
        self.siteid = self.add(npyscreen.TitleText, name='SiteID')
        self.ipaddress = self.add(npyscreen.TitleText, name='IP Address')
        self.netmask = self.add(npyscreen.TitleText, name='Netmask')
        self.gateway = self.add(npyscreen.TitleText, name='Gateway')
        self.dns1 = self.add(npyscreen.TitleText, name='DNS Server 1')
        self.dns2 = self.add(npyscreen.TitleText, name='DNS Server 2')
        self.endpoint = self.add(npyscreen.TitleText, name='Endpoint')
        self.svnrepo = self.add(npyscreen.TitleText, name='Svn Repo', value='/var/philips/views/')
        self.ansible = self.add(npyscreen.Checkbox, max_height=2, name='Ansible', value=True)


class MyApplication(npyscreen.NPSAppManaged):
   def onStart(self):
       self.addForm('MAIN', myEmployeeForm, name='New Employee')
       # A real application might define more forms here.......

if __name__ == '__main__':
   TestApp = MyApplication().run()
