#!/usr/bin/env python
"""
DESCRIPTION:

This script will call the LegacyNagiosScanner class in order to initiate migration of monitoring settings from a previously
deployed Compute Environment Monitoring solution into the Intelligent Device Management Ecosystem.  The script requires
the IP address of the previous monitoring solution and the credentials to access it.  The settings will be saved in
json format to the migrator_settings file (default location will be /etc/philips).
"""
# Module importing
# Standard Python libraries
import argparse
import os
import logging
import sys
import json
import getpass
# for IP Address verification
import ipaddr
# for http library
import requests
# for notifying backoffice via celery
sys.path.append('/usr/lib/celery/')
import phim_onsite.transmit

from scanline.trinity import scanline_scanner

from tbconfig import DISCOVERY

# Globals
logger = logging.getLogger(__name__)
states = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')
BACKOFFICE_CONNECTIVITY_URL = 'vigilant/health'
MIGRATOR_SETTINGS_FILE = '/etc/philips/migrator_settings'

# Functions
def check_arg(args=None):
    """
    Check the arguments passed
    """
    parser = argparse.ArgumentParser(description='Script to initiate migration of settings into IDM Ecosystem.')
    parser.add_argument('-i', '--ip',
                        help='the ip address of the CE Monitoring end point.',
                        default=None,
                        required=False)
    parser.add_argument('-u', '--user',
                        help='user name to access monitoring node.',
                        default=None,
                        required=False)
    parser.add_argument('-S', '--https',
                        help='flag for using https for back office endpoints if present.',
                        action='store_true',
                        required=False)
    results = parser.parse_args(args)
    return results.ip, results.user, results.https


def obtain_password(username):
    """
    This function will prompt the user to enter a password and then re-enter the password.  If the passwords match the
    password will be saved and if they do not match the user will be asked to enter the passwords again.  This will
    continue until the user enters matching passwords.  The function requires username so that it will print the name
    of the account for which the password is associated.
    """
    logger.debug('Attempting to obtain password for user %s', username)
    got_password = False
    first_password = ''
    while got_password is False:
        print 'Please enter the password for %s' % username
        first_password = getpass.getpass()
        print 'Re-enter the password for %s' % username
        second_password = getpass.getpass()
        if first_password == second_password:
            logger.debug('Password for %s has been obtained.' % username)
            got_password = True
        else:
            print 'The passwords provided did NOT match, please try again.'
    return first_password


def is_valid_ip(ip_address):
    """
    Uses ipaddr module to determine if an IP Address is valid.  Works for both IPv4 and IPv6.  The
    function takes in either a string or an integer.  For integers less than 2^32 it will assume it is IPv4.
    There is a flag that can be passed to indicate whether IPv6 is intended for the integer, but this function
    does not utilize it.
    """
    result = False
    try:
        ipaddr.IPAddress(ip_address)
    except ValueError:
        logger.debug('Invalid IPv4 or IPv6 Address: %s', ip_address)
    else:
        logger.debug('Valid IPv4 or IPv6 Address: %s', ip_address)
        result = True
    return result


def is_valid_url(url, auth=None, verify=False):
    """
    Uses requests module to verify whether a URL can be contacted without error.  auth is a tuple containing
    the username and password of the website if required.  By default auth is set to None.  If the status is
    200, the function will return True and False otherwise.

    USAGE:
    is_valid_url('http://sc.phim.isyntax.net/svn/ibc', (username, password))
    """
    params = dict(verify=verify)
    if auth is None:
        params['auth'] = auth
    r = requests.get(url, **params)
    return r.status_code == requests.codes.ok


def verify_migration_parameters(settings):
    """
    This function will verify whether all the required parameters have been obtained and return a value of True.
    If not it will return a value of False.  It also will verify if the IP Address provided is a valid IP address and
    return False if the IP address is not valid.
    """
    logger.debug('Validating the parameters passed to %s.', os.path.basename(__file__))
    valid_args = True
    for key in settings:
        if not settings[key]:
            logger.debug('Parameter %s is missing.', key)
            valid_args = False
            break
    if not is_valid_ip(settings['ip']):
        valid_args = False
    return valid_args


def create_parameter_dictionary(parameter_dict):
    """
    This function requires a dictionary of the parameters passed to the script.  From the directory provided it will
    determine if the settings file exists.  If it exists, the values will be loaded from the file in a dictionary
    object.  If the file does not exist it will load the command line parameters into a dictionary object.  The
    function will return a dictionary object containing all ofthe settings.  The function uses the Python json module.
    The json module stores the data as unicode instead of ascii strings so they should be converted back to ascii when
    loaded back into the script.
    """
    if os.path.exists(MIGRATOR_SETTINGS_FILE):
        logger.info('Settings file %s exists, attempting to load settings.', MIGRATOR_SETTINGS_FILE)
        with open(MIGRATOR_SETTINGS_FILE, 'rU') as f:
            stored_settings = json.load(f)
        # Overwrite command line provided parameters with stored parameters from the file.
        for key, value in stored_settings.items():
            if key in parameter_dict:
                if key == 'password':
                    value = value.decode('base64', 'strict')
                parameter_dict[key] = value
    else:
        logger.info('Loading parameters provided via command line.')
        if parameter_dict['user']:
            parameter_dict['password'] = obtain_password(parameter_dict['user'])
    return parameter_dict


def save_parameters_to_file(parameter_dict):
    """
    This function will copy the migration IP, user name, and password to the settings file in the provided directory.
    The file will be a json dump using the Python json module.
    """
    parameters = parameter_dict.copy()
    parameters['password'] = parameters.pop('password').encode('base64', 'strict')
    jsondata = json.dumps(parameters)
    with open(MIGRATOR_SETTINGS_FILE, 'w') as f:
        f.write(jsondata + '\n')
    logger.debug('Parameters saved successfully to %s.', MIGRATOR_SETTINGS_FILE)


def initiate_migration(parameter_dict, datatype='LN', url=DISCOVERY['ENDPOINT'], killswitch=DISCOVERY['KILLSWITCH']):
    """
    This function will load the LegacyNagiosScanner class to initiate migration of the current CE Monitoring solution in the
    field to the IDM Ecosystem.  The script will require a dictionary that contains the IP of the CE monitoring system
    and the credentials to access it.  Also required are the data type, discovery endpoint, and kill switch.  The data
    type will be LN for Legacy Nagios and the endpoint and kill switch are obtained from the tbconfig import.
    """
    logger.debug('Beginning migration of monitoring data.')
    scanner = scanline_scanner({
        'scanner': 'LN',
        'address': parameter_dict['ip'],
        'username': parameter_dict['user'],
        'password': parameter_dict['password']
    })

    facts = scanner.site_facts()
    if not facts:
        raise Exception('No site fact data was received.')
    if not is_valid_url(parameter_dict['protocol'] + '://' + BACKOFFICE_CONNECTIVITY_URL):
        raise Exception('%s is inaccessible, please investigate.' % url)
    # transmit data via http post
    phim_onsite.transmit.tx_data(url, facts, killswitch)
    logger.debug('Data successfully transmitted.')


def main():
    ip_address, username, https = check_arg()
    #Determine if valid input is provided to the script.
    settings_dict = dict(
        ip=ip_address,
        user=username,
        directory=MIGRATOR_SETTINGS_FILE,
        password='',
        protocol='https' if https else 'http'
    )
    state = 2
    try:
        valid_settings = create_parameter_dictionary(settings_dict)
        if not verify_migration_parameters(valid_settings):
            raise ValueError('Invalid input into the script %s', os.path.basename(__file__))
        else:
            save_parameters_to_file(valid_settings)
            initiate_migration(valid_settings)
            message = 'Migrated configurations have been sent for processing.'
            state = 0
    except ValueError:
        message = 'Invalid input please investigate.'
    except IOError:
        message = 'Error accessing file.  Please investigate.'
    except ImportError:
        message = 'Unable to import properly.  Please investigate.'
    except OSError:
        message = 'Error accessing executable. Please investigate'
    except Exception as e:
        message = 'Error found. %s' % e
        state = 3
    if state == 0:
        logger.info('%s STATUS - %s ', message, states[state])
    else:
        logger.exception('%s STATUS - %s ', message, states[state])
    sys.exit(state)


if __name__ == '__main__':
    main()
