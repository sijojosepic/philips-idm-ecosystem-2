# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimmailintercept %{_prefix}/lib/philips/mailintercept/

# Basic Information
Name:      phim-mailintercept
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   This is a filter for postfix to change transport for IDM

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phim-celery-tasks-client
Requires:  python-argparse

Provides:  phim-mailintercept = %{version}-%{release}

%description
This is a filter for postfix to change transport for IDM

%prep
%setup -q

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{phimmailintercept}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/mail_jackin.py %{buildroot}%{phimmailintercept}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/philips/mailintercept


%pre
groupadd -f philips
if ! getent passwd filter >/dev/null ; then
    useradd -g philips filter
fi

%post
if [ ! -d %{_var}/log/philips ]; then
    %{__install} -d -m 0775 -g philips %{_var}/log/philips
fi

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{phimmailintercept}/mail_jackin.py
%{_sysconfdir}/philips/mailintercept

%changelog
* Wed Feb 04 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Require argparse

* Mon Oct 27 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Conditionally create log directory

* Fri Mar 28 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
