# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%define localbin %{_prefix}/local/bin
%define localsbin %{_prefix}/local/sbin

# Basic Information
Name:      snmptt
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   SNMPTT (SNMP Trap Translator) is an SNMP trap handler written in Perl 

Group:     none
License:   GPL
URL:       http://snmptt.sourceforge.net/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}_%{version}.tgz
#Patch0:

Requires:  net-snmp 
Requires:  perl-Time-HiRes
Requires(post):   chkconfig
Requires(preun):  chkconfig
# This is for /sbin/service
Requires(preun):  initscripts
Requires(postun): initscripts
Provides:  snmptt

%description
SNMPTT (SNMP Trap Translator) is an SNMP trap handler written in Perl 
for use with the Net-SNMP / UCD-SNMP snmptrapd program (www.net-snmp.org). 

%prep
%setup -q -n %{name}_%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{localbin}
%{__install} -d -m 0755 %{buildroot}%{localsbin}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/snmp
%{__install} -d -m 0755 %{buildroot}%{_initrddir}
%{__install} -Dp -m 0755 %{_builddir}/%{name}_%{version}/snmptt %{buildroot}%{localsbin}/
%{__cp} %{_builddir}/%{name}_%{version}/snmptt.ini %{buildroot}%{_sysconfdir}/snmp/
%{__install} -Dp -m 0755 %{_builddir}/%{name}_%{version}/snmptt-init.d %{buildroot}%{_initrddir}/snmptt
%{__install} -Dp -m 0755 %{_builddir}/%{name}_%{version}/snmpttconvertmib %{buildroot}%{localbin}/
%{__install} -Dp -m 0755 %{_builddir}/%{name}_%{version}/snmptthandler %{buildroot}%{localsbin}/
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/spool/snmptt
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/log/snmptt
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/logrotate.d
%{__install} -Dp -m 0755 %{_builddir}/%{name}_%{version}/snmptt.logrotate %{buildroot}%{_sysconfdir}/logrotate.d/snmptt




%clean
%{__rm} -rf %{buildroot}

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    # fix the location in the init script
    %{__sed} -i 's#/usr/sbin#%{localsbin}#' %{_initrddir}/snmptt
    # %{_sbindir} != /sbin
    /sbin/chkconfig --add snmptt
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    # %{_sbindir} != /sbin
    /sbin/service snmptt stop >/dev/null 2>&1
    /sbin/chkconfig --del snmptt
fi

#%postun
#if [ "$1" -ge "1" ] ; then
    # package upgrade
    # does this need to be restarted?
    # %{_sbindir}/service snmptt restart >/dev/null 2>&1 || :
#fi

%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{localsbin}/snmptt
%config %{_sysconfdir}/snmp/snmptt.ini
%attr(0755,root,root) %{_initrddir}/snmptt
%attr(0755,root,root) %{localbin}/snmpttconvertmib
%attr(0755,root,root) %{localsbin}/snmptthandler
%dir %{_localstatedir}/spool/snmptt
%dir %{_localstatedir}/log/snmptt
%attr(0755,root,root) %{_sysconfdir}/logrotate.d/snmptt

%changelog
* Tue Jul 24 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
