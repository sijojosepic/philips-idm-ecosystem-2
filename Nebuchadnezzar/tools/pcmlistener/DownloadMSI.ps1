<#
.SYNOPSIS
 This script is used to download file from the NEB node.
.DESCRIPTION
  This script is used to download file from the NEB node.
.PARAMETER HTTPFilePathOnNEBNode
    Full http file path on the NEB node to download file.
.PARAMETER LocalFolderPath
    Full path to the folder on the local machine to save the remote file.
.INPUTS
    None
.OUTPUTS
    None
.NOTES
  Version:        1.0
  Author:         Ashok Banakar
  Modified Date:  07/05/2018
  Purpose/Change: Initial release.
#>
Param(
    [Parameter(Mandatory = $true)]
    $HTTPFilePathOnNEBNode,
    $LocalFolderPath = "C:\Temp"
)
#---------------------------------------------------------[Initialisations]--------------------------------------------------------
$ErrorActionPreference = "Stop"
#----------------------------------------------------------[Declarations]----------------------------------------------------------
$WebClient = New-Object System.Net.WebClient
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
#-----------------------------------------------------------[Execution]------------------------------------------------------------
try {

	############### Download PCM MSI #############################################
    #if $LocalFilePath does not exist create it.
    If (!(Test-Path -Path $LocalFolderPath)) {
        New-Item -Path $LocalFolderPath -Force -ErrorAction Stop -Type Directory
    }
    #Update local full path.
    $MSIFileName = [System.IO.Path]::GetFileName($HTTPFilePathOnNEBNode)
    $LocalMSIFullPath = Join-Path -Path $LocalFolderPath -ChildPath $MSIFileName
    #If local file already exist delete it.
    if ((Test-Path -Path $LocalMSIFullPath)) {
        Remove-Item -Path $LocalMSIFullPath -Force -ErrorAction "Stop"
    }
    $WebClient.DownloadFile($HTTPFilePathOnNEBNode, $LocalMSIFullPath)
    if((Test-Path -Path $LocalMSIFullPath))
    {
        Write-Output -InputObject "Successfully downloaded $HTTPFilePathOnNEBNode to $LocalMSIFullPath"
    }
    else
	{
        throw "Failed to download $HTTPFilePathOnNEBNode to LocalMSIFullPath"
    }

	############### Download PsExec #############################################

	$PSExecFilePathOnNEBNode = $HTTPFilePathOnNEBNode.replace($MSIFileName,"PsExec.exe")
	$LocalPSExecFullPath = Join-Path -Path $LocalFolderPath -ChildPath "PsExec.exe"

	#If local file already exist delete it.
    if ((Test-Path -Path $LocalPSExecFullPath)) {
        Remove-Item -Path $LocalPSExecFullPath -Force -ErrorAction "Stop"
    }
    $WebClient.DownloadFile($PSExecFilePathOnNEBNode, $LocalPSExecFullPath)
    if((Test-Path -Path $LocalPSExecFullPath))
    {
        Write-Output -InputObject "Successfully downloaded $PSExecFilePathOnNEBNode to $LocalPSExecFullPath"
    }
    else
	{
        throw "Failed to download $LocalPSExecFullPath to LocalFullPath"
    }
}
catch {
    throw "$_"
}
