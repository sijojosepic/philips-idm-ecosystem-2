<#
.SYNOPSIS
 This script is used Install PCM Framework MSI.
.DESCRIPTION
  This script is used Install PCM Framework MSI.
.PARAMETER UserName
    User name.
.PARAMETER UserPassword
    User password.
.PARAMETER UserDomain
    User domain, if user is local specify the machine name as user domain else specify the domain user.
.PARAMETER MSIPathOnNEBNode
    MSI HTTP path on the NEB node.
.INPUTS
    None
.OUTPUTS
    None
.NOTES
  Version:        1.0
  Author:         Ashok Banakar
  Modified Date:  07/05/2018
  Purpose/Change: Initial release.
#>
[System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingPlainTextForPassword","")]
Param(
    [Parameter(Mandatory = $true)]
    $UserName,
    [Parameter(Mandatory = $true)]
    $UserPassword,
    [Parameter(Mandatory = $true)]
    $UserDomain,
    [Parameter(Mandatory = $true)]
    $HostName,
    [Parameter(Mandatory = $true)]
    $HTTPMSIFolderPathOnNEBNode,
    [Parameter(Mandatory = $true)]
    $MSIFullPathOnLocalMachine
)
#---------------------------------------------------------[Initializations]--------------------------------------------------------
$ErrorActionPreference = "Stop"
#----------------------------------------------------------[Declarations]----------------------------------------------------------
$TimeStamp = Get-Date -Format yyyyMMdd_HHmmss
#-----------------------------------------------------------[Execution]------------------------------------------------------------
try {
        #Check msi exist or not
        if(!(Test-Path -Path $MSIFullPathOnLocalMachine))
        {
            throw "$MSIFullPathOnLocalMachine does not exist."
        }
        $MSIFileFolder = [System.IO.PATH]::GetDirectoryName($MSIFullPathOnLocalMachine)
        $MSILogFilePath = Join-Path -Path $MSIFileFolder -ChildPath "MSILog_$TimeStamp.log"
		$PSExePath= Join-Path -Path $MSIFileFolder -ChildPath "PsExec.exe"
        $MSIExeciParameters = "/i $MSIFullPathOnLocalMachine /q /norestart /l*v $MSILogFilePath USERNAME=$UserName USERDOMAIN=$UserDomain USERPASSWORD=$UserPassword HOSTNAME=$HostName PCMHTTPFOLDERPATH=$HTTPMSIFolderPathOnNEBNode"
        & $PSExePath -s /accepteula cmd /C "msiexec $MSIExeciParameters" 2>$null
	if($LASTEXITCODE -ne 0)
        {            
            throw "Failed to install $MSIFullPathOnLocalMachine with exit code $LASTEXITCODE. Please check MSI logs from $MSILogFilePath for more information."
        }
        Write-Output -InputObject "Successfully installed PCM Framework."
}
catch {
    throw "$_"
}