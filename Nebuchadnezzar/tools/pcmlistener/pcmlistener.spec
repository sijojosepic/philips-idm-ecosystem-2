# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimpcmlistener %{_prefix}/lib/philips/pcmlistener/

# Basic Information
Name:      phim-pcmlistener
Version:   %{_phiversion}
Release:   2%{?dist}
Summary:   Script to perform initial setup of an onsite IDM instance

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Chandan Kumar <chandan.kumar_1@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:


Provides:  phim-pcmlistener = %{version}-%{release}

%description
Script to perform initial setup of an onsite IDM instance

%prep
%setup -q

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{phimpcmlistener}
%{__install} -Dp -m 0766 %{_builddir}/%{name}-%{version}/DownloadMSI.ps1 %{buildroot}%{phimpcmlistener}/
%{__install} -Dp -m 0766 %{_builddir}/%{name}-%{version}/InstallMSI.ps1 %{buildroot}%{phimpcmlistener}/


%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0766,root,root) %{phimpcmlistener}/DownloadMSI.ps1
%attr(0766,root,root) %{phimpcmlistener}/InstallMSI.ps1

%changelog
* Wed Jun 06 2018 Chandan Kumar <chandan.kumar_1@philips.com>
- Initial Spec File
