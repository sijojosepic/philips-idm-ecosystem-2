#!/usr/bin/python

# Shinken module based on ws_arbiter

#    Leonardo Ruiz, leonardo.ruiz@philips.com

# This Class is an Arbiter module for having a webservice
# where results of service checks can be submitted,
# uses a file for mapping to service names (to not rely on knowing service name)
# mapping file is a yaml file containing a single dictionary
# it maps ip addresses to hostnames based on shinken configured hosts

"""This is based on ws_arbiter.

This is an example:
curl -XPOST -H "Content-Type: application/json" --data '{"service": "super", "return_code": "2", "output": "passive submission\nmore than one line", "host": "10.0.25.5" }' http://localhost:7764/check_result

This is an example using a list for output
curl -XPOST -H "Content-Type: application/json" --data '{"service": "Administrative__Philips__Localhost__Information__IPAddress", "return_code": "2", "output": [ "passive #win", "second line", "thirdline" ]  }' http://localhost:7760/check_result
"""

import os
import sys
import select
import time
import yaml
from operator import itemgetter

from shinken.basemodule import BaseModule
from shinken.external_command import ExternalCommand
from shinken.log import logger
from shinken.arbiterlink import ArbiterLink

from shinken.webui.bottlewebui import Bottle, run, static_file, view, route, request, response, abort, parse_auth

properties = {
    'daemons': ['arbiter', 'receiver'],
    'type': 'ws_passive_service_map',
    'external': True,
}


# called by the plugin manager to get a broker
def get_instance(plugin):
    instance = Ws_psm(plugin)
    return instance

# Main app var. Will be filled with running module instance
app = None


# Class used to get hosts information from running arbiter
class ShinkenHostsAddresses():
    def __init__(self, address='localhost', port='7770', arbiter_name='arbiter-master'):
        self._arbiter = None
        self.address = address
        self.port = port
        self.arbiter_name = arbiter_name

    @property
    def arbiter(self):
        if not self._arbiter:
            logger.debug("[Ws_psm] Connection to arbiter at %s:%s" % (self.address, self.port))
            ArbiterLink.use_ssl = False
            self._arbiter = ArbiterLink({'arbiter_name': self.arbiter_name, 'address': self.address, 'port': self.port})
            self._arbiter.fill_default()
            self._arbiter.pythonize()
            self._arbiter.update_infos()
        return self._arbiter

    def get_host_addresses(self):
        if not self.arbiter.reachable:
            logger.error("[Ws_psm] Connection to the arbiter got a problem")
            return {}
        logger.debug("[Ws_psm] Connection to arbiter OK")
        #host_properties = [ 'host_name','use','address']
        host_properties = ['address', 'host_name']
        hosts = self.arbiter.get_objects_properties('hosts', host_properties)
        get_props = itemgetter(*host_properties)
        logger.debug("[Ws_psm] hosts addresses: %s" % str(hosts))
        return dict(get_props(host) for host in hosts)


def get_command(service, return_code, output, host='localhost', timestamp=None):
    if not timestamp:
        timestamp = int(time.time())
    # try to get host name from map
    host = app.hosts_addresses.get(host, host)
    # try to get the service name from map
    service = app.service_map.get(service, service)
    # if output is not a string try to make it a multi line one
    if not isinstance(output, basestring):
        output = os.linesep.join(output)
    cmd = '[{timestamp}] PROCESS_SERVICE_CHECK_RESULT;{host};{service};{return_code};{output}'.format(**locals())
    logger.debug("[Ws_psm] Command: %s" % cmd)
    return cmd


def get_yaml(yaml_file):
    """Retrieves YAML data, a dictionary is expected.
    returns empty dictionary if not able to retrieve data from YAML file"""
    result = {}
    try:
        with open(yaml_file) as f:
            result = yaml.load(f)
    except IOError:
        logger.warning('[Ws_psm] File %s does not exist' % yaml_file)
    except yaml.scanner.ScannerError:
        logger.error('[Ws_psm] Could not parse YAML')
    return result


def get_page():
    # Check for auth if it's not anonymously allowed
    if app.username != 'anonymous':
        basic = parse_auth(request.environ.get('HTTP_AUTHORIZATION', ''))
        # user/pass not given. If so, bail out
        if not basic:
            abort(401, 'Authentication required')
        # credentials not good
        if basic[0] != app.username or basic[1] != app.password:
            abort(403, 'Authentication denied')

    try:
        logger.debug("[Ws_psm] request json: %s" % request.json)
        command = get_command(**request.json)
    except Exception, e:
        logger.error("[Ws_psm] error getting command: %s" % str(e))
        abort(400, "Incorrect JSON")
    # Adding command to the main queue()
    ext = ExternalCommand(command)
    app.from_q.put(ext)

    # OK here it's ok, it will return a 200 code


# This module will open an HTTP service, where a user can send a check return.
class Ws_psm(BaseModule):
    def __init__(self, modconf):
        BaseModule.__init__(self, modconf)
        self.hosts_addresses = None
        self.service_map = None
        try:
            self.username = getattr(modconf, 'username', 'anonymous')
            self.password = getattr(modconf, 'password', '')
            self.port = int(getattr(modconf, 'port', '7764'))
            self.host = getattr(modconf, 'host', '0.0.0.0')
            self.arbiter_address = getattr(modconf, 'arbiter_address', 'localhost')
            self.arbiter_port = getattr(modconf, 'arbiter_port', '7770')
            self.arbiter_name = getattr(modconf, 'arbiter_name', 'arbiter-master')
            self.service_map_file = getattr(modconf, 'service_map', '/etc/philips/shinken/servicemap.yml')
        except AttributeError:
            logger.error("[Ws_psm] The module is missing a property, check module declaration in shinken-specific.cfg")
            raise

    def get_hosts_addresses(self):
        ha = ShinkenHostsAddresses(address=self.arbiter_address, port=self.arbiter_port, arbiter_name=self.arbiter_name)
        self.hosts_addresses = ha.get_host_addresses()

    # Initialize the HTTP part. It's a simple wsgi backend
    # with a select hack so that it may exit if requested
    def init_http(self):
        logger.info("[Ws_psm] Starting WS arbiter http socket")
        self.srv = run(host=self.host, port=self.port, server='wsgirefselect')
        # And we link our page
        route('/check_result', callback=get_page, method='POST')

    # When you are in "external" mode, that is the main loop of your process
    def main(self):
        global app

        # Change process name (seen in ps or top)
        self.set_proctitle(self.name)

        # It's an external module, so signals must be managed
        self.set_exit_handler()

        # get the address - host dictionary
        self.get_hosts_addresses()

        self.service_map = get_yaml(self.service_map_file)

        # Go for Http open :)
        self.init_http()

        # Fill the global variable with the Queue() link
        # with the arbiter, because the page should be a non-class
        # one function
        app = self

        # loop forever on the http socket
        input = [self.srv.socket]

        # Main blocking loop
        while not self.interrupted:
            input = [self.srv.socket]
            inputready, _, _ = select.select(input, [], [], 1)
            for s in inputready:
                # If it's a web request, ask the webserver to do it
                if s == self.srv.socket:
                    self.srv.handle_request()
