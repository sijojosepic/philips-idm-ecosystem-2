#!/usr/bin/python

# -*- coding: utf-8 -*-

# Copyright (C) 2014:
#    Leonardo Ruiz, leonardo.ruiz@philips.com


"""This Class is a plugin for the Shinken Broker. It is in charge
to brok information of the service perfdata into a celery task
it just manages the service_check_return
"""

import sys

from shinken.basemodule import BaseModule
from shinken.log import logger

from celery import Celery


properties = {
    'daemons': ['broker'],
    'type': 'celery_task',
#    'external': True,
    'external': False,
    'phases': ['running'],
}


# Called by the plugin manager to get a broker
def get_instance(mod_conf):
    logger.info("[Celery task perfdata broker] Get a celery task data module for plugin %s" % mod_conf.get_name())
    path = getattr(mod_conf, 'path', None)
    task = getattr(mod_conf, 'task', None)
    conf_obj = getattr(mod_conf, 'conf_obj', None)
    instance = Celery_task_perfdata_broker(mod_conf, path, task, conf_obj)
    return instance


# Class for the Celery Task perfdata Broker
# Get broks and send them to a Celery task
class Celery_task_perfdata_broker(BaseModule):
    STATES = {0: 'OK', 1: 'WARNING', 2: 'CRITICAL', 3: 'UNKNOWN'}
    def __init__(self, modconf, path, task, conf_obj):
        BaseModule.__init__(self, modconf)
        self.path = path
        self.task = task
        self.conf_obj = conf_obj

        self.buffer = []

    # Called by Broker so we can do init stuff
    def init(self):
        print "[%s] Initializing broker for celery task on path '%s'" % (self.task, self.path)
        sys.path.append(self.path)
        self.celery = Celery()
        # Configure from module
        self.celery.config_from_object(self.conf_obj)

    # We've got a 0, 1, 2 or 3 (or something else? ->3
    # And want a real OK, WARNING, CRITICAL, etc...
    def resolve_service_state(self, state):
        return self.STATES.get(state, 'UNKNOWN')

    # A service check have just arrived, we UPDATE data info with this
    def manage_service_check_result_brok(self, b):
        data = b.data

        params = {
            'hostname': data['host_name'],
            'description': data['service_description'],
            'timestamp': data['last_chk'],
            'perfdata': data['perf_data'],
            'state': self.resolve_service_state(data['state_id']),
            'output': data['output']
        }
        self.buffer.append(params)

    # Each second the broker say it's a new second. Let use this to
    # dump to the file
    def hook_tick(self, brok):
        # Go to write it :)
        buf = self.buffer
        self.buffer = []
        for s in buf:
            self.celery.send_task(self.task, [], kwargs=s)
