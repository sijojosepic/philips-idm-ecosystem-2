# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the module
%global module_name npcdmod

# Basic Information
Name:      shinken-module-%{module_name}
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Shinken NPCD module

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{module_name}.tar.gz
#Patch0:

Requires:  shinken-broker
Provides:  shinken-module-%{module_name}

%description
Shinken NPCD module


%prep
%setup -q -c

%build
echo OK


%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{_sharedstatedir}/shinken/modules/%{module_name}
%{__cp} -r %{_builddir}/%{name}-%{version}/module/* %{buildroot}%{_sharedstatedir}/shinken/modules/%{module_name}


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sharedstatedir}/shinken/modules/%{module_name}


%changelog
* Fri Dec 11 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
