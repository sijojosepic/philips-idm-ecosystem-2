#!/usr/bin/env python
'''The code to recieve data from a alertmanager which will post the alerts of prometheus using a web hook to NEB node and send the data to backoffice and then to dashboard of
idm.'''
from bottle import Bottle, request
import sys
import json
from celery import Celery


from ptconfig import (
        CONFIGURATION_OBJECT,
        CONFIGURATION_PATH,
        SEND_NOTIFICATION)

sys.path.append(CONFIGURATION_PATH)
from tbconfig import PROMETHEUS_KILLSWITCH
from tbconfig import ALERTMANAGER_ENDPOINT

celery = Celery()
celery.config_from_object(CONFIGURATION_OBJECT)

app = Bottle()

#function which will accept json file through api
@app.post('/notification/events')
def get_prometheus_event():
    try:
        data = request.json
        celery.send_task(SEND_NOTIFICATION, [ALERTMANAGER_ENDPOINT, data, PROMETHEUS_KILLSWITCH])
    except Exception as e:
        return 'Check Json format'

if __name__ == '__main__':
        app.run(host='0.0.0.0', debug=True, reload= True)