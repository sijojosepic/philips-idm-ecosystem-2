# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}
%global prometheusdir %{_libdir}/prometheus

# Basic Information
Name:      prometheus
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Prometheus monitoring tool

Group:     none
License:   GPL
URL:       https://github.com/prometheus/prometheus/releases/download/v2.7.0-rc.2/prometheus-2.7.0-rc.2.linux-amd64.tar.gz
# Packager Information
Packager:  Siddharth Singh <siddharth.singh_1@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: x86_64

# Source Information
Source0:  prometheus-2.7.0.tar.gz
#Patch0:

#Requires:

%description
Prometheus monitoring tool


%prep
%setup -q


%build
echo

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{prometheusdir}/prometheussrc
%{__cp} -R %{_builddir}/prometheus-2.7.0/* %{buildroot}%{prometheusdir}/prometheussrc

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%attr(0755,root,root) /usr/lib64/prometheus/prometheussrc


%changelog
* Mon Jan 28 2019 Siddharth Singh <siddharth.singh_1@philips.com>
- Initial Spec File
