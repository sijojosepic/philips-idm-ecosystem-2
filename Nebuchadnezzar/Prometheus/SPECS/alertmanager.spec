# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}
%global prometheusdir %{_libdir}/prometheus

# Basic Information
Name:      alertmanager
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Alert manager for prometheus

Group:     none
License:   GPL
URL:       https://github.com/prometheus/alertmanager/releases/download/v0.16.1/alertmanager-0.16.1.linux-amd64.tar.gz
# Packager Information
Packager:  Siddharth Singh <siddharth.singh_1@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: x86_64

# Source Information
Source0:   alertmanager-0.16.1.tar.gz
#Patch0:

#Requires:

%description
Alert manager for prometheus


%prep
%setup -q


%build
echo

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{prometheusdir}/alertmanagersrc/
%{__cp} -R %{_builddir}/alertmanager-0.16.1/* %{buildroot}%{prometheusdir}/alertmanagersrc/

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%attr(0755,root,root) /usr/lib64/prometheus/alertmanagersrc


%changelog
* Mon Jan 28 2019 Siddharth Singh <siddharth.singh_1@philips.com>
- Initial Spec File
