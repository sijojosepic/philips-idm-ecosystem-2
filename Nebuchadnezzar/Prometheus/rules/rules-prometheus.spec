# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global prometheusdir %{_libdir}/prometheus

# Basic Information
Name:      rules-prometheus
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   rules-prometheus setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Vijeesh K <vijeesh.k@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:


%description
Generic Linux monitoring


%prep
%setup -q

%build
echo OK

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{prometheusdir}/prometheussrc/prometheusrules
%{__cp} -R %{_builddir}/%{name}-%{version}/rules.yml %{buildroot}%{prometheusdir}/prometheussrc/prometheusrules

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%attr(0755,root,root) /usr/lib64/prometheus/prometheussrc

%changelog
* Tue Aug 20 2019 Vijeesh K <vijeesh.k@philips.com>
- Initial Spec File
