# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimuwsgi %{_prefix}/lib/philips/uwsgi/

# Basic Information
Name:      phim-prometheus
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-prometheus setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Siddharth Singh <siddharth.singh_1@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:


Requires:  phimutils
Requires:  phim-celery-tasks-client
Requires:  phim-uwsgi
Provides:  phim-prometheus_alert
Conflicts: phim-gateway

%description
phim-prometheus_alert setup.


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__cp} %{_builddir}/%{name}-%{version}/uwsgi.prometheus.ini %{buildroot}%{_sysconfdir}/uwsgi/prometheus.ini
%{__install} -d -m 0755 %{buildroot}%{phimuwsgi}/prometheus/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/prometheus_alert.py %{buildroot}%{phimuwsgi}/prometheus/
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/ptconfig.py %{buildroot}%{phimuwsgi}/prometheus/

%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/uwsgi/prometheus.ini
%{phimuwsgi}/prometheus/


%changelog
* Tue Jun 14 2019 Siddharth Singh <siddharth.singh_1@philips.com>
- Added uwsgi ini to be picked up by uwsgi emperor
