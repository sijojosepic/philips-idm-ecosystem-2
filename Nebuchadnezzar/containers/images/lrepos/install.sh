#! /bin/bash

#export http_proxy=http://165.225.96.34:9480/
#export https_proxy=http://165.225.96.34:9480/

cd /etc/yum.repos.d
mv CentOS-Base.repo CentOS-Base.repo.old
mv CentOS-Debuginfo.repo CentOS-Debuginfo.repo.old
mv CentOS-Media.repo CentOS-Media.repo.old
mv CentOS-Vault.repo CentOS-Vault.repo.old
mv CentOS-fasttrack.repo CentOS-fasttrack.repo.old

printf "130.147.168.99 repo.phim.isyntax.net\n130.147.168.99 pypi.phim.isyntax.net">>/etc/hosts

#yum remove -y glibc-common krb5-libs
#yum install -y glibc-common krb5-libs
#yum downgrade -y glibc glibc-common krb5-libs
yum install -y gcc
yum install -y libffi-devel
yum install -y openssl-devel
yum install -y python-devel
yum install -y --disablerepo=base extras updates python-setuptools
yum install -y --disablerepo=base extras updates python-requests python-beautifulsoup4 python-netifaces python-minimock python-mock python-yaml python-ordereddict
yum remove -y perl-XML-SAX-Base
yum install -y perl-XML-LibXML perl-CGI --exclude=perl-XML-SAX-Base
yum install -y perl perl-ExtUtils-Embed
yum install -y libxml2 libxml2-devel zlib-devel wget pcre pcre-devel sudo make autoconf automake
yum install -y ansible
yum install -y python-pyvmomi
yum install -y python-sphere
yum -y install perl-File-Slurp perl-Date-Calc local-perl-Module-Load perl-Template-Toolkit perl-Log-Log4perl perl-GD perl-Apache-LogFormat-Compiler perl-File-ShareDir-Install perl-HTTP-Tiny perl-Plack perl-podlators perl-Pod-Usage perl-Stream-Buffered
yum -y install perl-JSON-XS

#unset http_proxy https_proxy

/usr/bin/easy_install -i 'http://pypi.phim.isyntax.net/simple/' pip
#export http_proxy=http://165.225.96.34:9480/
#export https_proxy=http://165.225.96.34:9480/
pip install setuptools --upgrade

useradd philips && useradd -G philips nagios

