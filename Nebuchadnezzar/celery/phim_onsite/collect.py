from __future__ import absolute_import

import os

from celery.utils.log import get_logger
from phim_onsite.celery import app
from phimutils.perfdata import perfdata2dict, info2dict
from phimutils.stateredis import StateRedis
from taskbase.redistask import create_redis_task
from tbconfig import REDIS, PERFDATA, STATE

logger = get_logger(__name__)


@app.task(ignore_result=True)
def handle_sperfdata(hostname, description, timestamp, perfdata, state, output):
    parsed_perfdata = perfdata2dict(perfdata)
    if parsed_perfdata:
        rabbit_perfdata.delay(hostname, description, timestamp, parsed_perfdata)
    parsed_infos = info2dict(output)
    redis_state.delay(hostname, description, parsed_perfdata, state, parsed_infos)


@app.task(ignore_result=True)
def rabbit_perfdata(hostname, description, timestamp, perfdata, killswitch=PERFDATA['KILLSWITCH']):
    """Store performance data in RabbitMQ for later consumption"""
    if os.path.exists(killswitch):
        return

    with app.connection_or_acquire(connection=None) as conn:
        queue = conn.SimpleQueue(PERFDATA['QUEUE'])
        queue.put(dict(timestamp=timestamp, hostname=hostname, service=description, perfdata=perfdata))


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True)
def redis_state(hostname, service, perfdata, state, infos, killswitch=STATE['KILLSWITCH']):
    """Insert state information/metrics to redis,
    this information will represent the latest recorded state of the service.
    """
    if os.path.exists(killswitch):
        return

    state_redis = StateRedis(redis_state.rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET'])
    state_redis.insert(hostname, service, state, infos, perfdata)
