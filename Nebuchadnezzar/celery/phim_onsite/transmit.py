from __future__ import absolute_import

import linecache
import os
from Queue import Empty
from glob import glob

import requests
import redis
import atexit
import json
from pyVim import connect
from pyVmomi import vmodl
from pyVmomi import vim
from celery.canvas import chord
from celery.utils.log import get_logger
from phim_onsite.celery import app

from phim_onsite.shinken import passive_service
from phimutils import pcm
from phimutils.message import Message
from phimutils.stateredis import StateRedis
from phimutils.timestamp import iso_timestamp
from phimutils.resource import NagiosResourcer
from scanline.trinity import scanline_endpoints, scanline_scanner, get_scanners
from taskbase.redistask import create_redis_task
from scanline.component.localhost import LocalHost
from tbconfig import (SITEID_FILE, DISCOVERY_VERSION_KEYS,
                      REDIS, PERFDATA, METRICSDATA, STATE, SITE_CONFIG, SITE_CONFIG_HEADERS,
                      DISCOVERY, PCM, ENDPOINT, VERSION, REDIS_STATUS)
from package_status import PackageSubscriptions
from enums import PackageStatus
from subscription import Subscription

logger = get_logger(__name__)
requests.packages.urllib3.disable_warnings()


def get_perfblob(conn):
    """Get items form a queue and build a list of performance metrics dictionaries"""
    perfblob = []
    queue = conn.SimpleQueue(PERFDATA['QUEUE'])
    for i in range(len(queue)):
        try:
            m = queue.get_nowait()
        except Empty:
            break

        try:
            perfblob.append(msg2perfdict(m))
        except KeyError:
            pass
        finally:
            m.ack()
    return perfblob


def msg2perfdict(msg):
    """Map message payload to a performance metrics dictionary"""
    return {
        'timestamp': iso_timestamp(msg.payload['timestamp']),
        'hostname': msg.payload['hostname'],
        'service': msg.payload['service'],
        'perfdata': msg.payload['perfdata']
    }


def get_siteid(siteid='', siteid_file=SITEID_FILE):
    if not siteid:
        siteid = linecache.getline(siteid_file, 1).strip()
    return siteid


def get_metrics_json(data):
    data['timestamp'] = iso_timestamp(data.get('timestamp'))
    return data


@app.task(ignore_result=True)
def tx_data(url, data, killswitch=None, verify=ENDPOINT['VERIFY'], is_metricsdata=False, **kwargs):
    """Transmit data via http post"""
    if killswitch and os.path.exists(killswitch):
        logger.warning('killswitch %s found, aborting', killswitch)
        return
    siteid = get_siteid()
    if not siteid:
        raise Exception('Could not post due to missing siteid')
    if not data:
        logger.warning('no data to post')
        return
    if is_metricsdata:
        req = requests.post(url, json=get_metrics_json(data), verify=verify)
    else:
        message = Message(siteid=siteid, payload=data, **kwargs)
        req = requests.post(url, json=message.to_dict(), verify=verify)
    req.raise_for_status()


## Tasks that are to be triggered from the trigger nagios plugin need to support taking hostname and service kwargs,
## even if not used


@app.task(ignore_result=True)
def tx_perfdata(hostname=None, service=None):
    """Transmit the gathered performance data via http post"""
    with app.connection_or_acquire(connection=None) as conn:
        tx_data(PERFDATA['ENDPOINT'], get_perfblob(conn), PERFDATA['KILLSWITCH'])


@app.task(ignore_result=True)
def tx_metricsdata(payload=None):
    """Transmit the gathered performance data via http post"""
    with app.connection_or_acquire(connection=None) as conn:
        tx_data(METRICSDATA['ENDPOINT'], payload, METRICSDATA['KILLSWITCH'], is_metricsdata=True)


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True)
def tx_state(hostname=None, service=None):
    """Transmit gathered state information via http post"""
    state_redis = StateRedis(tx_state.rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET'])
    tx_data(STATE['ENDPOINT'], state_redis.query(), killswitch=STATE['KILLSWITCH'])


@app.task(ignore_result=True)
def tx_discovery(hostname=None, service=None, facts=None, endpoint=None, credentials=None, exclude_modules=None):
    """Transmit the gathered discovery data via http post"""
    output = {'facts': facts}
    if endpoint:
        output['endpoint'] = endpoint
    if credentials:
        output['credentials'] = credentials
    if exclude_modules:
        output['exclude_modules'] = exclude_modules
    tx_data(DISCOVERY['ENDPOINT'], output, DISCOVERY['KILLSWITCH'])


def tx_site_config_status(site_config):
    if site_config:
        try:
            tx_data(SITE_CONFIG['ENDPOINT'], site_config, SITE_CONFIG['KILLSWITCH'])
            status, msg = 0, 'Site Configurations sent.'
        except Exception as e:
            status, msg = 2, 'Unable to post to backoffice due to:\n {exp}'.format(exp=str(e))
    else:
        status, msg = 2, 'Site Configuration is Empty.'
    return status, msg


def get_module_type(host_facts, hostaddress):
    host_fact = [host for host in host_facts if 'address' in host and host['address'] == hostaddress]
    if host_fact and host_fact[0]:
        host_fact = host_fact[0]
        if 'module_type' in host_fact:
            module_type = host_fact['module_type']
            return str(module_type).encode('utf-8')
        elif 'modules' in host_fact:
            return host_fact['modules']


def get_module_type_config():
    try:
        url = SITE_CONFIG['URL'].format(site_id=get_siteid())
        response = requests.get(url=url, headers=SITE_CONFIG_HEADERS, verify=False, timeout=10)
        response.raise_for_status()
        host_facts = response.json()
        if host_facts:
            return host_facts['results']
    except Exception as e:
        logger.exception('Unable to get facts for url %s due to: %s ', url, str(e))

def get_module_type_with_hostname(host_facts, hostname):
    host_fact = [host for host in host_facts if 'hostname' in host and host['hostname'].lower() == hostname.lower()]
    if host_fact and host_fact[0]:
        host_fact = host_fact[0]
        if 'module_type' in host_fact:
            module_type = host_fact['module_type']
            return str(module_type).encode('utf-8')
        elif 'modules' in host_fact:
            return host_fact['modules']


def get_vcenter_obj(endpoint):
    try:
        service_instance = connect.SmartConnect(host=endpoint['address'], user=endpoint['username'],
                                                pwd=endpoint['password'])
        atexit.register(connect.Disconnect, service_instance)
        content = service_instance.RetrieveContent()
        container = content.rootFolder
        view_type = [vim.VirtualMachine]
        recursive = True
        container_view = content.viewManager.CreateContainerView(container, view_type, recursive)
        return container_view.view
    except vmodl.MethodFault as error:
        logger.exception("Caught vmodl fault : %s", error.msg)


@app.task(ignore_result=True)
def tx_site_config(hostname=None, service=None):
    """Transmit the gathered site configuration data via http post"""
    all_status, all_msg = [], []
    endpoints = [endpoint_config for endpoint_config in scanline_endpoints(DISCOVERY['CONFIGURATION']) if
                 'vcenter' == endpoint_config['scanner'].lower()]
    if not endpoints:
        all_status, all_msg = [2], ['Discovery.yml does not contain Vcenter endpoint']
    else:
        for endpoint in endpoints:
            verify_endpoint(endpoint)
            endpoint = process_endpoint_config(endpoint)
            vms = get_vcenter_obj(endpoint)
            if not vms:
                status, msg = 2, 'Unable to get vms with vcenter ('+ endpoint['address'] +') API.'
            else:
                status, msg = tx_site_config_details(vms, vc_address=endpoint['address'])
        all_status.append(status)
        all_msg.append(msg)
    passive_service.delay(hostname, service, max(all_status), str(all_msg).strip('[]').replace("'", ""))


def tx_site_config_details(vms, vc_address):
    site_config = []
    real_value = lambda arg: arg if arg else ""
    host_facts = get_module_type_config()
    if not host_facts:
        return 2, 'Unable to get facts.'
    for vm in vms:
        try:
            summary = vm.summary
            hostname = real_value(summary.guest.hostName)
            hostaddress = real_value(summary.guest.ipAddress) if real_value(summary.guest.ipAddress) else ''
            cpu_cores = real_value(summary.config.numCpu)
            ram_size = real_value(summary.config.memorySizeMB)
            if hostname:
                module_type = get_module_type_with_hostname(host_facts, hostname)
                if module_type:
                    site_config.append({"hostname": hostname, "hostaddress": hostaddress, "cpu_cores": cpu_cores,
                                        "ram_size": ram_size, "module_type": module_type})
        except Exception as e:
            logger.exception('VM object internal Error: %s', str(e))
    return tx_site_config_status(site_config)


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True)
def tx_insert_cache(hostname=None, service=None, facts=None, endpoint=None):
    try:
        state_redis = StateRedis(tx_insert_cache.rc, "facts", facts)
        if endpoint and endpoint['scanner']:
            state_redis.add_scanners('scanners', endpoint['scanner'])
        for cache_host, cache_host_info in facts.items():
            info = {}
            info['hostname'] = cache_host
            product_id = facts[cache_host]['product_id']
            info['product_id'] = product_id
            info['product_version'] = facts[cache_host]['product_version']
            info['product_name'] = facts[cache_host]['product_name']
            info['ipaddress'] = facts[cache_host]['address']
            module_type = ''
            if 'ISP' in facts[cache_host]['modules']:
                module_type = facts[cache_host]['ISP']['module_type']
                info['module_type'] = module_type
            if 'Database' in facts[cache_host]['modules']:
                module_type = 'Database'
                product_id = 'ISPACS'
            redis_key = '#' + str(cache_host) + '#' + str(product_id) + '#' + str(module_type)
            state_redis.insert_facts(redis_key, info)
    except:
        pass

def delete_package_status_redis_keys():
    rc = redis.StrictRedis.from_url(REDIS['URL'])
    rc.delete(REDIS_STATUS['SUBSCRIBED'])
    rc.delete(REDIS_STATUS['AVAILABLE'])

@app.task(ignore_result=True)
def tx_trinity_discovery(hostname=None, service=None):
    #redis.Redis().flushall()
    delete_package_status_redis_keys()
    header = [
        tx_trinity_discovery_data.s(endpoint_config)
        for endpoint_config
        in scanline_endpoints(DISCOVERY['CONFIGURATION'])
    ]
    callback = tx_trinity_discovery_status.subtask(kwargs={'hostname': hostname, 'service': service})
    chord(header)(callback)


def resolve_resource(resourcer, item):
    if isinstance(item, dict) and '$resource' in item:
        resource = item.get('$resource') if item.get('$resource') else ''
        result = resourcer.get_resource(resource.upper())
        if result is None:
            logger.warning('resource "%s" could not be retrieved', resource)
        return result
    result = []
    if isinstance(item, list):
        for entry in item:
            result.append(resourcer.get_resource(entry.upper()) or entry)
        return tuple(result)
    return item


def verify_endpoint(endpoint):
    if isinstance(endpoint.get('username'), list) or isinstance(endpoint.get('password'), list):
        if not (isinstance(endpoint.get('password'), list) and isinstance(endpoint.get('username'), list)):
            raise RuntimeError('Type of username and password, both have to be list or None can be list - ', endpoint)
        if len(endpoint.get('username')) != len(endpoint.get('password')):
            raise RuntimeError('When username and password are list, length should match', endpoint)


def process_endpoint_config(endpoint_config):
    secret = linecache.getline(DISCOVERY['SECRET_FILE'], 1).strip()
    nr = NagiosResourcer(DISCOVERY['NAGIOS_RESOURCE_FILE'], secret)
    return dict((key, resolve_resource(nr, value)) for key, value in endpoint_config.iteritems())


def add_environment_type(obj, endpoint):
    """
        obj -> product scanner obj
    """
    if hasattr(obj, 'environmentType') and obj.environmentType:
        endpoint['environmentType'] = obj.environmentType
    if endpoint.get('environmentType') and endpoint['environmentType'].lower() not in ('production', 'test', 'bcs'):
        raise RuntimeError('environmentType must be one of these - production, test, bcs')
    return endpoint


@app.task(time_limit=3600)
def tx_trinity_discovery_data(endpoint_config):
    try:
        rc, state, endpoint = 2, 'Failed', {}
        verify_endpoint(endpoint_config)
        resolved_endpoint_config = process_endpoint_config(endpoint_config)
        if endpoint_config['scanner'] == 'ISP':
            # Required while doing discovery by consuming GD API in ISP discovery.
            resolved_endpoint_config['scanners'] = get_scanners
        scanner = scanline_scanner(resolved_endpoint_config)
        endpoint = dict(scanner=resolved_endpoint_config['scanner'], address=resolved_endpoint_config['address'])
        if 'environmentType' in resolved_endpoint_config:
            endpoint['environmentType'] = resolved_endpoint_config['environmentType']
        if 'monitor' in resolved_endpoint_config:
            endpoint['monitor'] = resolved_endpoint_config['monitor']
        if scanner is not None:
            facts = scanner.site_facts()
            if facts:
                endpoint = add_environment_type(scanner, endpoint)
                tx_discovery(facts=facts, endpoint=endpoint, credentials=facts.pop('credentials', None),
                             exclude_modules=facts.pop('exclude_modules', None))
                tx_insert_cache(facts=facts, endpoint=endpoint)
                rc, state = 0, 'Succeeded'
    except Exception, ex:
        logger.exception(ex)
    endpoint = endpoint or endpoint_config
    return rc, 'Using {scanner} on {address} {state}'.format(state=state, **endpoint)


@app.task(ignore_result=True)
def tx_trinity_discovery_status(task_result_list, hostname, service):
    logger.info(task_result_list)
    # [(2, 'Using ISP on 10.6.88.60'), (0, 'Using AdvancedWorkflowServices on 167.81.184.155')]

    states, messages = zip(*task_result_list)
    state = 0 if sum(states) == 0 else 2
    msg = ', '.join(messages)
    passive_service.delay(hostname, service, state, msg)


@app.task(base=create_redis_task(REDIS['URL']), ignore_result=True)
def tx_version_discovery(hostname=None, service=None):
    """Transmit gathered version information via http post"""

    state_redis = StateRedis(tx_version_discovery.rc, STATE['REDIS_PREFIX'], STATE['REDIS_SET'])
    version_data = state_redis.query(keys=DISCOVERY_VERSION_KEYS.keys(), key_map=DISCOVERY_VERSION_KEYS)

    # Filter assumes a format of {"localhost" : { "KeyOne" : "ValueOne", "KeyTwo" : None, "KeyThree" : None, "KeyFour" : None}},
    #                       {"host1" : { "KeyOne" : None, "KeyTwo" : "ValueTwo", "KeyThree" : None, "KeyFour" : None}}

    filtered_data = {}
    for host, host_data in version_data.iteritems():
        filtered_host_data = [dict(name=k, version=v) for k, v in host_data.items() if v]
        filtered_data[host] = {'Components': filtered_host_data}
    tx_data(VERSION['ENDPOINT'], data={'facts': filtered_data})


def delete_subscription(subscription):
    """
    This function deletes the subscription xml from the neb.
    :param subscription: type str, subscription  name
    :return: None
    """
    try:
        filepath = "{path}/{subscription}.xml".format(path=PCM['SUBSCRIPTION_PATH'], subscription=subscription)
        os.remove(filepath)
        logger.info("Deleted package subscription: {0} from neb.".format(subscription))
    except OSError:
        pass

def update_redis(key, value):
    rc = redis.StrictRedis.from_url(REDIS['URL'])
    rc.sadd(key, value)

def get_packages(key):
    rc = redis.StrictRedis.from_url(REDIS['URL'])
    return rc.smembers(key)

def get_package_subscription_id(package_xml):
    return Subscription().get_subscription_id(package_xml)


@app.task(ignore_results=True)
def tx_pcm_discovery(hostname=None, service=None):
    """Transmit available pcm files and required databags to facts via http post"""
    available_key = "package.status.available"
    # This sends the subscription status to back office.
    send_subscription_status()
    package_list = []

    file_iter = (
        os.path.join(root, single_file) for root, dirs, files in os.walk(PCM['REPO_PATH']) for single_file in files
    )
    xml_file_iter = (file_name for file_name in file_iter if os.path.splitext(file_name)[1] == '.xml')
    for pcm_xml in xml_file_iter:
        if pcm_xml in get_packages(available_key):
            continue
        subscription_id = get_package_subscription_id(pcm_xml)
        package = pcm.PCMPackage(pcm_xml)
        status, fsize = package.get_download_status_and_size()
        package_name = package.Name + ''.join(package.Version.split('.'))
        if status == PackageStatus.AVAILABLE:
            delete_subscription(subscription_id)    # Delete subscription xml from Neb once package is available.
            update_redis(available_key, pcm_xml) # Update available status into redis.
        package_list.append({
            'name': package_name,
            'version': package.Version.replace('.', ','),
            'dependencies': package.get_dependencies(),
            'databags': package.get_required_databags(),
            'status': status,
            'fsize': fsize,
            'subscription_id': subscription_id
        })
    if package_list:
        data = {'localhost': {'PCM': package_list}}
        data['localhost'].update(LocalHost().get_attributes())
        tx_data(VERSION['ENDPOINT'], data={'facts': data})


@app.task(ignore_results=True)
def tx_deployment_status(id, status, output=''):
    """Transmit available siteifno files and required databags to audit via http post"""
    data = {"deployment_id": id, 'status': status, 'output': output}
    tx_data(DISCOVERY['DEPLOYMENT'], data)


@app.task(ignore_result=True)
def send_subscription_status():
    """
    This function reads all the subscription xml files from Neb.
    Post the request to back office with subscribed status.
    """
    subscribed_key = "package.status.subscribed"
    xml_file_iter = (xml_file for xml_file in glob(os.path.join(PCM['SUBSCRIPTION_PATH'], '*.xml')))

    for sub_xml in xml_file_iter:
        subscription = PackageSubscriptions(sub_xml)
        version = subscription.version
        if version is None or sub_xml in get_packages(subscribed_key):
            continue
        subscription.update_subscribed_status()
        update_redis(subscribed_key, sub_xml) # Update subscribed status into redis.
