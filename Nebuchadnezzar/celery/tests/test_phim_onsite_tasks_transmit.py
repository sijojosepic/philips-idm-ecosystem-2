import unittest
from mock import MagicMock, patch, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        func.func_dict = {'rc': 'rc'}
        return func

    return fakeorator


class POTasksTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_po = MagicMock(name='phim_onsite')
            self.mock_po.celery.app.task = fakeorator_arg
            self.mock_taskbase = MagicMock(name='taskbase')
            self.mock_tbconfig = MagicMock(name='tbconfig')
            self.mock_celery = MagicMock(name='celery')
            self.mock_phimutils = MagicMock(name='phimutils')
            self.mock_scanline = MagicMock(name='scanline')
            self.mock_package_status = MagicMock(name="package_status")
            self.mock_enums = MagicMock(name="enums")
            self.mock_redis = MagicMock(name="redis")
            self.mock_subscription = MagicMock(name="subscription")
            modules = {
                'tbconfig': self.mock_tbconfig,
                'taskbase': self.mock_taskbase,
                'taskbase.redistask': self.mock_taskbase.redistask,
                'phim_onsite.celery': self.mock_po.celery,
                'phim_onsite.shinken': self.mock_po.shinken,
                'phim_onsite.shinken.passive_service': self.mock_po.shinken.passive_service,
                'celery': self.mock_celery,
                'celery.canvas': self.mock_celery.canvas,
                'celery.utils': self.mock_celery.utils,
                'celery.utils.log': self.mock_celery.utils.log,
                'phimutils': self.mock_phimutils,
                'phimutils.stateredis': self.mock_phimutils.stateredis,
                'phimutils.message': self.mock_phimutils.message,
                'phimutils.timestamp': self.mock_phimutils.timestamp,
                'phimutils.resource': self.mock_phimutils.resource,
                'scanline': self.mock_scanline,
                'scanline.trinity': self.mock_scanline.trinity,
                'scanline.component': self.mock_scanline.component,
                'scanline.component.localhost': self.mock_scanline.localhost,
                'package_status': self.mock_package_status,
                'enums': self.mock_enums,
                'redis': self.mock_redis,
                'subscription': self.mock_subscription
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import phim_onsite.transmit
            self.transmit = phim_onsite.transmit
            self.transmit.PERFDATA = {'QUEUE': 'perfdata_queue'}
            self.transmit.DISCOVERY = {
                'ENDPOINT': 'ENDPOINT', 'KILLSWITCH': 'KILLSWITCH', 'CONFIGURATION': 'CONFIGURATION'}
            self.transmit.SITE_CONFIG = {'ENDPOINT': 'ENDPOINT', 'KILLSWITCH': 'KILLSWITCH', 'URL': 'http://{site_id}'}
            self.transmit.SITE_CONFIG_HEADERS = {'X-Requested-With': 'XMLHttpRequest', 'X-Auth-Valid': 'True',
                                                 'Content-Type': 'application/json'}
            self.transmit.endpoint = {'scanner': 'vcenter', 'tags': 'vcenter', 'address': 'address',
                                      'username': 'username', 'password': 'password'}
            self.transmit.DISCOVERY_VERSION_KEYS = {'a': 'b', 'c': 'd'}
            self.transmit.STATE = {
                'REDIS_PREFIX': 'PREFIX', 'REDIS_SET': 'SET'}
            self.transmit.VERSION = {'ENDPOINT': 'enp'}
            self.transmit.PCM = {'REPO_PATH': 'REPO_PATH', 'SUBSCRIPTION_PATH': 'SUBSCRIPTION_PATH'}
            self.transmit.REDIS = {'URL': 'REDIS_URL'}
            self.transmit.REDIS_STATUS = {'AVAILABLE':'available', 'SUBSCRIBED':'subscribed'}

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class GetPerfblobTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

        self.msgs = []
        self.msgs.append(
            {
                'timestamp': '2013-10-16T09:15:54.353045Z',
                'hostname': 'host1',
                'hostaddress': '127.0.0.2',
                'service': 'Cat#Ven#Obj#Atr#Dim1',
                'perfdata': 'x=1m;1;2'
            }
        )
        self.msgs.append(
            {
                'timestamp': '2013-10-16T12:23:14.233540Z',
                'hostname': 'host2',
                'hostaddress': '127.0.0.3',
                'service': 'Cat#Ven#Obj#Atr#Dim2',
                'perfdata': 'y=31k;3;4'
            }
        )
        self.msgs.append(
            {
                'timestamp': '2013-11-16T02:44:61.468134Z',
                'hostname': 'host1',
                'hostaddress': '127.0.0.2',
                'service': 'Cat#Ven#Obj#Atr#Dim1',
                'perfdata': 'x=2m;1;2'
            }
        )

        self.conn = MagicMock(name='conn')
        self.queue = self.conn.SimpleQueue.return_value
        self.queue.__len__.return_value = 3

        self.transmit.msg2perfdict = MagicMock(
            name='msg2perfdict', side_effect=self.msgs)

    def test_get_perfblob_normal(self):
        self.assertEqual(
            self.transmit.get_perfblob(self.conn),
            [
                {
                    'timestamp': '2013-10-16T09:15:54.353045Z',
                    'hostname': 'host1',
                    'hostaddress': '127.0.0.2',
                    'service': 'Cat#Ven#Obj#Atr#Dim1',
                    'perfdata': 'x=1m;1;2'
                },
                {
                    'timestamp': '2013-10-16T12:23:14.233540Z',
                    'hostname': 'host2',
                    'hostaddress': '127.0.0.3',
                    'service': 'Cat#Ven#Obj#Atr#Dim2',
                    'perfdata': 'y=31k;3;4'
                },
                {
                    'timestamp': '2013-11-16T02:44:61.468134Z',
                    'hostname': 'host1',
                    'hostaddress': '127.0.0.2',
                    'service': 'Cat#Ven#Obj#Atr#Dim1',
                    'perfdata': 'x=2m;1;2'
                }
            ]
        )
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 3)

    def test_get_perfblob_index_error(self):
        self.transmit.msg2perfdict.side_effect = IndexError
        self.assertRaises(IndexError, self.transmit.get_perfblob, self.conn)
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')

    def test_get_perfblob_key_error(self):
        self.transmit.msg2perfdict.side_effect = KeyError
        self.assertEqual(self.transmit.get_perfblob(self.conn), [])
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 3)

    def test_get_perfblob_queue_empty_before_len_0(self):
        self.queue.get_nowait.side_effect = self.transmit.Empty
        self.assertEqual(self.transmit.get_perfblob(self.conn), [])
        self.conn.SimpleQueue.assert_called_once_with('perfdata_queue')
        self.assertEqual(self.queue.get_nowait.call_count, 1)

    def test_msg2perfdict(self):
        self.assertEqual(self.transmit.msg2perfdict(MagicMock()),
                         {'timestamp': '2013-10-16T09:15:54.353045Z', 'hostname': 'host1', 'hostaddress': '127.0.0.2',
                          'service': 'Cat#Ven#Obj#Atr#Dim1', 'perfdata': 'x=1m;1;2'})

    def test_get_siteid(self):
        self.assertEqual(self.transmit.get_siteid(None, 'siteid_file'), '')

    def test_get_metrics_json(self):
        self.transmit.get_metrics_json(MagicMock())
        self.assertEqual(self.mock_phimutils.timestamp.iso_timestamp.call_count, 1)

    def test_tx_data(self):
        self.transmit.get_siteid = MagicMock(return_value='val')
        self.transmit.requests.post = MagicMock()
        self.transmit.tx_data('url', MagicMock(), 'killswitch', 'verify', True)

    def test_tx_data_with_no_data(self):
        self.transmit.get_siteid = MagicMock(return_value='val')
        self.transmit.requests.post = MagicMock()
        self.transmit.tx_data('url', None, 'killswitch', 'verify', True)

    def test_tx_data_with_no_metrics(self):
        self.transmit.get_siteid = MagicMock(return_value='val')
        self.transmit.requests.post = MagicMock()
        self.transmit.tx_data('url', MagicMock(), 'killswitch', 'verify')

    def test_tx_data_with_no_site_id(self):
        self.transmit.get_siteid = MagicMock(return_value=None)
        self.transmit.requests.post = MagicMock()

        def test_error(self):
            with self.assertRaises(Exception): self.transmit.tx_data('url', MagicMock(), 'killswitch', 'verify')

    def test_tx_insert_cache(self):
        obj = {
            'x': {
                'product_id': 'pi',
                'product_version': 'pv',
                'product_name': 'pn',
                'address': 'ip',
                'modules': ['ISP', 'Database'],
                'ISP': {'module_type': 'mt'},
                'Database': {}
            }
        }

        self.transmit.tx_insert_cache(MagicMock(), MagicMock(), obj, MagicMock())

    def test_tx_trinity_discovery(self):
        self.transmit.DISCOVERY = {'CONFIGURATION': 'config'}
        self.transmit.tx_trinity_discovery_status = MagicMock()
        self.transmit.tx_trinity_discovery()

    def test_process_endpoint_config(self):
        self.transmit.DISCOVERY = {'SECRET_FILE': 'file', 'NAGIOS_RESOURCE_FILE': 'file'}
        self.transmit.process_endpoint_config(MagicMock())

    def test_tx_trinity_discovery_status(self):
        self.mock_po.shinken.phim_onsite.delay = MagicMock()
        self.transmit.sum = MagicMock(return_value=True)
        var = ['28', '34']
        self.transmit.tx_trinity_discovery_status(var, 'host', 'service')

    def test_tx_deployment_status(self):
        self.transmit.DISCOVERY = {'DEPLOYMENT': 'config'}
        self.transmit.tx_data = MagicMock()
        self.transmit.tx_deployment_status('id', 'status')

    def test_tx_perfdata(self):
        self.transmit.PERFDATA = {'ENDPOINT': 'config', 'KILLSWITCH': 'switch'}
        self.transmit.tx_data = MagicMock()
        self.transmit.get_perfblob = MagicMock()
        self.transmit.tx_perfdata()

    def test_tx_metricsdata(self):
        self.transmit.METRICSDATA = {'ENDPOINT': 'config', 'KILLSWITCH': 'switch'}
        self.transmit.tx_data = MagicMock()
        self.transmit.get_perfblob = MagicMock()
        self.transmit.tx_metricsdata()

    def test_tx_state(self):
        self.transmit.STATE = {'ENDPOINT': 'config', 'KILLSWITCH': 'switch', 'REDIS_PREFIX': 'val', 'REDIS_SET': 'val'}
        self.transmit.tx_data = MagicMock()
        self.transmit.get_perfblob = MagicMock()
        self.transmit.tx_state()


class TxTrinityDiscoveryDataTestCase(POTasksTest.TestCase):

    def test_tx_trinity_discovery_data_monitor_flg_true(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'monitor': True, 'environmentType': 'production'}
        self.transmit.verify_endpoint = MagicMock(name="verify_endpoint")
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.add_environment_type = MagicMock(name="add_environment_type", return_value={'monitor': True, 'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'})
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")

        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_tx_discovery = [call(credentials=None, endpoint={
            'monitor': True,'environmentType': 'production','scanner': 'TestScanner', 'address': '1.0.0.1'}, exclude_modules=None, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)
        _expected_call_process_end_config = [call(
            {'monitor': True,'environmentType': 'production', 'scanner': 'TestScanner', 'address': '1.0.0.1'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)

    def test_tx_trinity_discovery_data_monitor_flg_false(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'monitor': False, 'environmentType': 'production'}
        self.transmit.verify_endpoint = MagicMock(name="verify_endpoint")
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.add_environment_type = MagicMock(name="add_environment_type", return_value={'monitor': False, 'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'})
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'monitor': False, 'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=None, endpoint={'monitor': False,
                                                                        'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'},
                                            exclude_modules=None, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)

    def test_tx_trinity_discovery_ISP_scanner(self):
        data = {'scanner': 'ISP', 'tags': ['y', 'u'], 'address': '167.81.183.99',
                'discovery_url': 'http://testurl.com', 'environmentType': 'production'}

        _resolvd_endpnt_cfg = {'scanner': 'ISP',
                               'address': '1.0.0.1', 'monitor': True, 'environmentType': 'production'}
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.add_environment_type = MagicMock(name="add_environment_type",
                                                       return_value={'monitor': True, 'scanner': 'scan1',
                                                                     'address': '167.81.183.99',
                                                                     'environmentType': 'production'})
        self.transmit.verify_endpoint = MagicMock(name="verify_endpoint")
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        self.transmit.add_environment_type.return_value = data
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg.copy())
        self.assertEqual(
            response, (0, 'Using ISP on 167.81.183.99 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'monitor': True, 'scanner': 'ISP', 'address': '1.0.0.1', 'environmentType': 'production'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=None, endpoint={'tags': ['y', 'u'],
                                                                        'scanner': 'ISP', 'address': '167.81.183.99',
                                                                        'discovery_url': 'http://testurl.com',
                                                                        'environmentType': 'production'},
                                            exclude_modules=None, facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)
        self.transmit.verify_endpoint.assert_called_once_with({'scanner': 'ISP',
                                                               'address': '1.0.0.1',
                                                               'monitor': True, 'environmentType': 'production'})
        scanners = {'scanners': self.mock_scanline.trinity.get_scanners}
        exp_scanline_call = _resolvd_endpnt_cfg.copy()
        exp_scanline_call.update(scanners)
        self.mock_scanline.trinity.scanline_scanner.assert_called_once_with(exp_scanline_call)

    def test_tx_trinity_discovery_data_without_monitor_flg(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'environmentType': 'production'}
        self.transmit.verify_endpoint = MagicMock(name="verify_endpoint")
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1"}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.add_environment_type = MagicMock(name="add_environment_type", return_value={'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'})
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=None, endpoint={
            'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'},  exclude_modules=None,
                                            facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)

    def test_tx_trinity_discovery_data_verify_endpoint_expection(self):
        endpoint = {'scanner': 'TestScanner',
                    'address': '1.0.0.1'}
        mock_verify_endpoint = MagicMock(name='verify endpoint')
        mock_verify_endpoint.side_effect = RuntimeError
        self.transmit.verify_endpoint = mock_verify_endpoint
        response = self.transmit.tx_trinity_discovery_data(endpoint)
        self.assertEqual(
            response, (2, 'Using TestScanner on 1.0.0.1 Failed'))

    def test_tx_trinity_discovery_data_with_credentials(self):
        _resolvd_endpnt_cfg = {'scanner': 'TestScanner',
                               'address': '1.0.0.1', 'environmentType': 'production'}
        self.transmit.verify_endpoint = MagicMock(name="verify_endpoint")
        self.transmit.process_endpoint_config = MagicMock(
            name='process_endpoint_config', return_value=_resolvd_endpnt_cfg
        )
        credentials = {'u': 'u1', 'p': 'p1'}
        _mk_scanner = MagicMock(name='scanline_scanner',
                                **{'site_facts.return_value': {'f1': "fact1", 'credentials': credentials}})
        self.mock_scanline.trinity.scanline_scanner.return_value = _mk_scanner
        self.transmit.add_environment_type = MagicMock(name="add_environment_type", return_value={'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'})
        self.transmit.tx_discovery = MagicMock(name="tx_discovery")
        response = self.transmit.tx_trinity_discovery_data(_resolvd_endpnt_cfg)
        self.assertEqual(
            response, (0, 'Using TestScanner on 1.0.0.1 Succeeded'))
        _mk_scanner.site_facts.assert_called_once_with()
        _expected_call_process_end_config = [call(
            {'scanner': 'TestScanner', 'address': '1.0.0.1','environmentType': 'production'})]
        self.assertEqual(self.transmit.process_endpoint_config.call_args_list,
                         _expected_call_process_end_config)
        _expected_call_tx_discovery = [call(credentials=credentials, endpoint={
            'scanner': 'TestScanner', 'address': '1.0.0.1', 'environmentType': 'production'},  exclude_modules=None,
                                            facts={'f1': 'fact1'})]
        self.assertEqual(
            self.transmit.tx_discovery.call_args_list, _expected_call_tx_discovery)


class TxDiscoveryDataTestCase(POTasksTest.TestCase):
    def test_tx_discovery_empty_value(self):
        tx_data_mock = MagicMock(name='tx_data')
        self.transmit.tx_data = tx_data_mock
        self.transmit.tx_discovery()
        tx_data_mock.assert_called_once_with(
            'ENDPOINT', {'facts': None}, 'KILLSWITCH')

    def test_tx_discovery_non_empty_values(self):
        tx_data_mock = MagicMock(name='tx_data')
        self.transmit.tx_data = tx_data_mock
        self.transmit.tx_discovery(
            'hostname', 'service', 'facts', 'endpoint', 'credentials')
        out_put = {'facts': 'facts',
                   'credentials': 'credentials', 'endpoint': 'endpoint'}
        tx_data_mock.assert_called_once_with('ENDPOINT', out_put, 'KILLSWITCH')


class ResolveResourceTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)
        self.mock_resource = MagicMock(name='resource')

    def test_resolve_resource_dict_resource(self):
        item = {'$resource': '$USER56$'}
        self.mock_resource.get_resource.return_value = 'user1'
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, 'user1')
        self.mock_resource.get_resource.assert_called_once_with('$USER56$')


    def test_resolve_resource_dict_resource_lower(self):
        item = {'$resource': '$user56$'}
        self.mock_resource.get_resource.return_value = 'user1'
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, 'user1')
        self.mock_resource.get_resource.assert_called_once_with('$USER56$')

    def test_resolve_resource_dict_item_without_resource_key(self):
        item = {'item': '$USER56$'}
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, item)
        self.assertEqual(self.mock_resource.get_resource.call_count, 0)

    def test_resolve_resource_list_item(self):
        item = ['u1', '$USER45$', '$user46$']
        self.mock_resource.get_resource.side_effect = ('u1', 'u2', 'u3')
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, ('u1', 'u2', 'u3'))
        exp_call = [call('U1'), call('$USER45$'), call('$USER46$')]
        self.assertEqual(
            self.mock_resource.get_resource.call_args_list, exp_call)

    def test_resolve_resource_str_item(self):
        item = 'user'
        result = self.transmit.resolve_resource(self.mock_resource, item)
        self.assertEqual(result, 'user')


class VerifyEndpointTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

    def test_verify_endpoint_usr_pwd_length_match(self):
        endpoint = {'username': ['u1', 'u2']}
        endpoint['password'] = ['p1', 'p2']
        self.transmit.verify_endpoint(endpoint)

    def test_verify_endpoint_pwd_not_list(self):
        endpoint = {'username': ['u1', 'u2']}
        endpoint['password'] = 'pwd'
        self.assertRaises(
            RuntimeError, self.transmit.verify_endpoint, endpoint)

    def test_verify_endpoint_usr_not_list(self):
        endpoint = {'username': 'u1'}
        endpoint['password'] = ['p1', 'p2']
        self.assertRaises(
            RuntimeError, self.transmit.verify_endpoint, endpoint)

    def test_verify_endpoint_length_mismatch(self):
        endpoint = {'username': ['u1']}
        endpoint['password'] = ['p1', 'p2']
        self.assertRaises(
            RuntimeError, self.transmit.verify_endpoint, endpoint)

    def test_add_environment_type(self):
        GPS_obj = MagicMock(name='obj')
        GPS_obj.scanner='scan1'
        GPS_obj.tags=['y', 'u']
        GPS_obj.address='167.81.183.99'
        GPS_obj.discovery_url='http://testurl.com'
        GPS_obj.environmentType='production'
        self.assertEqual(self.transmit.add_environment_type(GPS_obj, endpoint={'username': ['u1']}),  {'username': ['u1'], 'environmentType': 'production'})

    def test_add_environment_type_no_environment_type(self):
        endpoint = {'username': ['u1']}
        GPS_obj = MagicMock(name='obj')
        try:
            self.transmit.add_environment_type(GPS_obj, endpoint)
        except RuntimeError:
            pass


class VersionDiscoveryTestCase(POTasksTest.TestCase):
    def setUp(self):
        POTasksTest.TestCase.setUp(self)

    def test_tx_version_discovery(self):
        mock_tx_data = MagicMock(name='tx_data')
        self.transmit.tx_data = mock_tx_data
        host_info = {"localhost": {"KeyOne": "ValueOne",
                                   "KeyTwo": None, "KeyThree": None, "KeyFour": None}}
        host_info['host1'] = {
            "KeyOne": None, "KeyTwo": "ValueTwo", "KeyThree": None, "KeyFour": None}
        self.mock_phimutils.stateredis.StateRedis.return_value.query.return_value = host_info
        self.transmit.tx_version_discovery()
        self.mock_phimutils.stateredis.StateRedis.assert_called_once_with(
            'rc', 'PREFIX', 'SET')
        exp_facts = {'localhost': {'Components': [{'version': 'ValueOne', 'name': 'KeyOne'}]},
                     'host1': {'Components': [{'version': 'ValueTwo', 'name': 'KeyTwo'}]}}
        mock_tx_data.assert_called_once_with('enp', data={'facts': exp_facts})
        self.mock_phimutils.stateredis.StateRedis.return_value.query.assert_called_once_with(
            key_map={'a': 'b', 'c': 'd'}, keys=['a', 'c'])

    def test_delete_package_status_keys(self):
        mock_rc = MagicMock(name="rc")
        mock_rc.delete.return_value = True
        self.mock_redis.StrictRedis.from_url.return_value = mock_rc
        self.transmit.delete_package_status_redis_keys()
        mock_rc.delete.assert_called_with('available')

    def test_update_redis(self):
        mock_rc = MagicMock(name="rc")
        mock_rc.sadd.return_value = True
        self.mock_redis.StrictRedis.from_url.return_value = mock_rc
        self.transmit.update_redis('available', 'test.xml')
        mock_rc.sadd.assert_called_once_with('available', 'test.xml')

    def test_get_packages(self):
        mock_rc = MagicMock(name="rc")
        mock_rc.smembers.return_value = ['test.xml']
        self.mock_redis.StrictRedis.from_url.return_value = mock_rc
        result = self.transmit.get_packages('available')
        self.assertEqual(result, ['test.xml'])
        mock_rc.smembers.assert_called_once_with('available')

    def test_delete_subscription(self):
        self.transmit.os = MagicMock(name='os')
        self.transmit.os.remove.side_effect = OSError
        self.transmit.delete_subscription('subscription')
        self.transmit.os.remove.assert_called_once_with('SUBSCRIPTION_PATH/subscription.xml')

    def test_tx_pcm_discovery(self):
        mock_tx_data = MagicMock(name='tx_data')
        self.transmit.tx_data = mock_tx_data
        mock_os_walk = MagicMock(name='os.walk')
        mock_os_walk.return_value = [('/root', ['dir'], ('f1.xml', 'f2.xml'))]
        self.transmit.os.walk = mock_os_walk
        self.mock_scanline.localhost.LocalHost.return_value.get_attributes.return_value = {
            'a': 'b'}
        pcm_mock = MagicMock(name='pcm_obj')
        pcm_mock.get_download_status_and_size.return_value = ('Available', '0B')

        self.mock_phimutils.pcm.PCMPackage = MagicMock(name='PCMPackage')
        self.mock_phimutils.pcm.PCMPackage.return_value = pcm_mock
        self.transmit.send_subscription_status = MagicMock(name="send_subscription_status")
        self.transmit.delete_subscription = MagicMock(name="delete_subscription")
        self.mock_enums.PackageStatus.AVAILABLE = 'Available'
        self.transmit.get_packages = MagicMock(name="get_packages", return_value = ['/root/f1.xml'])
        self.transmit.update_redis = MagicMock(name="update_redis")
        self.transmit.get_package_subscription_id = MagicMock(name="get_package_subscription_id", return_value='sub')

        self.transmit.tx_pcm_discovery()

        exp_facts = {'localhost': {'a': 'b', 'PCM': [{'version': self.mock_phimutils.pcm.PCMPackage().Version.replace(),
                                                      'name': self.mock_phimutils.pcm.PCMPackage().Name.__add__(),
                                                      'dependencies': self.mock_phimutils.pcm.PCMPackage().get_dependencies(),
                                                      'databags': self.mock_phimutils.pcm.PCMPackage().get_required_databags(),
                                                      'status': 'Available',
                                                      'fsize': '0B',
                                                      'subscription_id': 'sub'}
                                                      ]}}
        mock_tx_data.assert_called_once_with('enp', data={'facts': exp_facts})
        mock_os_walk.assert_called_once_with('REPO_PATH')
        self.transmit.send_subscription_status.assert_called_once_with()
        self.transmit.delete_subscription.assert_called_with('sub')
        self.transmit.update_redis.assert_called_with('package.status.available', '/root/f2.xml')
        self.transmit.get_packages.assert_called_with('package.status.available')
        self.transmit.get_package_subscription_id.assert_called_with('/root/f2.xml')

    def test_send_subscription_status_case_one(self):
        self.transmit.glob = MagicMock(name='glob')
        self.transmit.glob.return_value = ['f1.xml', 'f2.xml']

        mock_subscription = MagicMock(name="PackageSubscriptions")
        mock_subscription.version = "X.y.Z"
        self.mock_package_status.PackageSubscriptions.return_value = mock_subscription

        self.mock_package_status.PackageSubscriptions().update_subscribed_status = MagicMock(
            name="update_subscribed_status")
        self.transmit.get_packages = MagicMock(name="get_packages", return_value=['/root/f1.xml'])
        self.transmit.update_redis = MagicMock(name="update_redis")
        self.transmit.send_subscription_status()
        self.mock_package_status.PackageSubscriptions().update_subscribed_status.assert_called_with()
        self.transmit.update_redis.assert_called_with('package.status.subscribed', 'f2.xml')
        self.transmit.get_packages.assert_called_with('package.status.subscribed')

    def test_send_subscription_status_case_two(self):
        self.transmit.glob = MagicMock(name='glob')
        self.transmit.glob.return_value = ['f1.xml', 'f2.xml']

        mock_subscription = MagicMock(name="PackageSubscriptions")
        mock_subscription.version = None
        self.mock_package_status.PackageSubscriptions.return_value = mock_subscription

        self.mock_package_status.PackageSubscriptions().update_subscribed_status = MagicMock(
            name="update_subscribed_status")
        self.transmit.send_subscription_status()
        self.mock_package_status.PackageSubscriptions().update_subscribed_status.assert_not_called()


class TxSiteConfigTestCase(POTasksTest.TestCase):
    def test_tx_site_config_status_1(self):
        self.transmit.tx_data = Exception()
        self.assertEqual(self.transmit.tx_site_config_status('Data'),
                         (2, "Unable to post to backoffice due to:\n 'exceptions.Exception' object is not callable"))

    def test_tx_site_config_status_2(self):
        self.transmit.tx_data = MagicMock()
        self.assertEqual(self.transmit.tx_site_config_status('Data'), (0, 'Site Configurations sent.'))

    def test_tx_site_config_status_3(self):
        self.transmit.tx_data = MagicMock()
        self.assertEqual(self.transmit.tx_site_config_status(''), (2, 'Site Configuration is Empty.'))

    def test_tx_site_config(self):
        self.transmit.scanline_endpoints = MagicMock(return_value=[self.transmit.endpoint])
        self.transmit.get_vcenter_obj = MagicMock()
        self.transmit.process_endpoint_config = MagicMock()
        self.transmit.tx_site_config_details = MagicMock(return_value=('status', 'msg'))
        self.assertEqual(self.transmit.tx_site_config(), None)

    def test_tx_site_config_1(self):
        self.transmit.scanline_endpoints = MagicMock(return_value=[])
        self.transmit.get_vcenter_obj = MagicMock()
        self.transmit.tx_site_config_details = MagicMock(return_value=('status', 'msg'))
        self.assertEqual(self.transmit.tx_site_config(), None)

    def test_tx_site_config_2(self):
        self.transmit.scanline_endpoints = MagicMock(return_value=[self.transmit.endpoint])
        self.transmit.get_vcenter_obj = MagicMock(return_value=[])
        self.transmit.process_endpoint_config = MagicMock()
        self.transmit.tx_site_config_details = MagicMock(return_value=('status', 'msg'))
        self.assertEqual(self.transmit.tx_site_config(), None)

    def test_get_module_type_1(self):
        host_facts = [{"module_type": "module_type", "hostname": "hostname", "modules": ["Windows", "ISP"],
                       "address": "hostaddress"}]
        hostaddress = 'hostaddress'
        self.assertEqual(self.transmit.get_module_type(host_facts, hostaddress), 'module_type')

    def test_get_module_type_2(self):
        host_facts = [{"module_type": "module_type", "hostname": "hostname", "modules": ["Windows", "ISP"],
                       "address": "hostaddress"}]
        hostaddress = 'hostadd'
        self.assertEqual(self.transmit.get_module_type(host_facts, hostaddress), None)

    def test_get_module_type_3(self):
        host_facts = [{"hostname": "hostname", "modules": ["Windows", "Database"], "address": "hostaddress"}]
        hostaddress = 'hostaddress'
        self.assertEqual(self.transmit.get_module_type(host_facts, hostaddress), ['Windows', 'Database'])

    def test_get_module_type_4(self):
        host_facts = [{"hostname": "hostname", "modules": ["Windows", "Database"], "address": "hostaddress"}]
        hostaddress = 'hostadd'
        self.assertEqual(self.transmit.get_module_type(host_facts, hostaddress), None)

    def test_get_module_type_config(self):
        self.transmit.requests = MagicMock(name='requests.get')
        self.transmit.get_siteid = MagicMock(return_value='siteid')
        self.assertEqual(self.transmit.get_module_type_config(), self.transmit.requests.get().json().__getitem__())

    def test_get_module_type_config_1(self):
        self.transmit.requests = Exception
        self.transmit.get_siteid = MagicMock(return_value='siteid')
        self.transmit.get_module_type_config()
        self.transmit.logger.exception.assert_called_once_with('Unable to get facts for url %s due to: %s ',
                                                               'http://siteid',
                                                               "type object 'exceptions.Exception' has no attribute 'get'")

    def test_get_vcenter_obj(self):
        self.transmit.connect = MagicMock()
        self.transmit.connect.SmartConnect = MagicMock()
        self.transmit.register = MagicMock(name='atexit.register')
        self.assertEqual(self.transmit.get_vcenter_obj(self.transmit.endpoint),
                         self.transmit.connect.SmartConnect().RetrieveContent().viewManager.CreateContainerView().view)

    def test_tx_site_config_details_1(self):
        self.transmit.tx_site_config_status = MagicMock(return_value='mocked')
        self.transmit.get_module_type_config = MagicMock()
        self.assertEqual(self.transmit.tx_site_config_details([], 'vc_address'), 'mocked')
        self.transmit.get_module_type_config.assert_called_once_with()

    def test_tx_site_config_details_2(self):
        self.transmit.tx_site_config_status = MagicMock(return_value='mocked')
        self.transmit.get_module_type_config = MagicMock()
        self.vms = [MagicMock() for i in range(10)]
        self.assertEqual(self.transmit.tx_site_config_details(self.vms, 'vc_address'), 'mocked')
        self.transmit.get_module_type_config.assert_called_once_with()

    def test_tx_site_config_details_3(self):
        self.transmit.tx_site_config_status = MagicMock(return_value='mocked')
        self.transmit.get_module_type_config = MagicMock(return_value=[])
        self.vms = [MagicMock() for i in range(10)]
        self.assertEqual(self.transmit.tx_site_config_details(self.vms, 'vc_address'), (2, 'Unable to get facts.'))
        self.transmit.get_module_type_config.assert_called_once_with()

    def test_tx_site_config_details_4(self):
        self.transmit.tx_site_config_status = MagicMock(return_value='mocked')
        self.transmit.get_module_type_config = MagicMock()
        self.vms = [MagicMock() for i in range(10)]
        self.vms[1].summary = Exception
        self.assertEqual(self.transmit.tx_site_config_details(self.vms, 'vc_address'), 'mocked')
        self.transmit.logger.exception.assert_called_once_with('VM object internal Error: %s',
                                                               "type object 'exceptions.Exception' has no attribute 'guest'")

    def test_tx_site_config_details_5(self):
        self.transmit.tx_site_config_status = MagicMock(return_value='mocked')
        self.transmit.get_module_type_config = MagicMock()
        self.transmit.get_module_type = MagicMock()
        self.vms = [MagicMock() for i in range(10)]
        self.assertEqual(self.transmit.tx_site_config_details(self.vms, 'vc_address'), 'mocked')
        self.transmit.get_module_type_config.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
