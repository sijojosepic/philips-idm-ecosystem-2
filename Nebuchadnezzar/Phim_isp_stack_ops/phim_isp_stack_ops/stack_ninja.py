import pysphere
import logging
from cached_property import cached_property


TASK_EXECUTION_TIMEOUT = 300

VSPHERE_URL_PATTERN = '{protocol}://{address}:{port}/sdk'

spit_bucket = logging.getLogger('phim.isp_stack_ops.stack_ninja')
"""
spit_bucket.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
spit_bucket.addHandler(handler)
"""

MAX_UNIT_NUMBER = 15
DEFAULT_SCSI_CONTROLLER_KEY = 1000


class VMwareOperator(object):
    """
    This class receives the data required to migrate stacks from one VM to a destination VM.  It also connects
    to vCenter and finds the necessary Vm objects.
    """
    def __init__(self, vi_server):
        self.vi_server = vi_server

    def get_vm_by_name(self, vm_name):
        """
        Searches vCenter for a Vm object that matches the Vm name provided.  An exception is raised if the Vm
        is not found.
        """
        spit_bucket.debug('Searching for VM named %s.', vm_name)
        vm_obj = self.vi_server.get_vm_by_name(name=vm_name)
        spit_bucket.debug('Found VM %s.', vm_name)
        return vm_obj

    @staticmethod
    def create_vm_change_request(vm_object_reference):
        """
        Create a Vm Change Object which is required for changing Vm configuration.  Requires the module object
        reference ID of the Vm in question and returns a request object set for the Vm.
        """
        request = pysphere.resources.VimService_services.ReconfigVM_TaskRequestMsg()
        _this = request.new__this(vm_object_reference)
        _this.set_attribute_type(vm_object_reference.get_attribute_type())
        request.set_element__this(_this)
        return request

    def execute_vm_change_task(self, vm_request):
        """
        Method to execute VM reconfiguration task.  It will wait for the task to complete and raise an exception if
        it encounters an error.  The timeout for task execution has be set to 5 minutes.
        """
        task = self.vi_server._proxy.ReconfigVM_Task(vm_request)._returnval
        vi_task = pysphere.VITask(task, self.vi_server)
        status = vi_task.wait_for_state([vi_task.STATE_SUCCESS, vi_task.STATE_ERROR], timeout=TASK_EXECUTION_TIMEOUT)
        if status == vi_task.STATE_ERROR:
            raise ValueError('Error in VM change request %s' % vi_task.get_error_message())
        return status


class VMwareVMOperator(VMwareOperator):
    """
    A sub class of VMwareOperator that adds functionality to operate on drives.
    """
    def __init__(self, vi_server, vm_name):
        super(VMwareVMOperator, self).__init__(vi_server)
        self.vm_name = vm_name

    @cached_property
    def vm(self):
        return self.get_vm_by_name(self.vm_name)

    @cached_property
    def disks(self):
        return self.vm.get_property(name='disks', from_cache=True)

    def get_disk(self, disk_name):
        return next((disk for disk in self.disks if disk['descriptor'] == disk_name), None)

    def get_disk_device(self, unit_number):
        return next((device for device in self.vm.properties.config.hardware.device
                     if device._type == 'VirtualDisk' and device.unitNumber == unit_number), None)

    def get_disk_property(self, disk_name, property):
        disk = self.get_disk(disk_name)
        return disk['device'][property]

    def get_disk_capacity_in_kb(self, disk_name):
        return self.get_disk_property(disk_name, 'capacityInKB')

    def get_disk_unit_number(self, disk_name):
        return self.get_disk_property(disk_name, 'unitNumber')

    def get_data_store(self):
        return ((self.vm.get_property(name='path', from_cache=False)).split())[0]

    def get_next_unit_number(self):
        unit_number = max(disk['device']['unitNumber'] for disk in self.disks)
        if unit_number >= MAX_UNIT_NUMBER:
            return
        return unit_number + 1

    def remove_disk(self, disk_name):
        disk = self.get_disk(disk_name)
        device = self.get_disk_device(disk['device']['unitNumber'])
        if not device:
            raise ValueError('Device %s is not in the VM configuration' % disk_name)
        self.remove_device(device._obj)

    def remove_device(self, device_obj):
        """
        Method that will remove a device from a VM.  This procedure was adapted from a discussion
        in a PySphere Google Group (https://groups.google.com/forum/#!msg/pysphere/pS18hcjPleo/18xLDyIB5IUJ)
        """
        # Set up object to create vm request task for source VM
        request = self.create_vm_change_request(self.vm._mor)
        # Create new VM Spec for the source VM
        spec = request.new_spec()
        device_change = spec.new_deviceChange()
        device_change.Operation = 'remove'
        device_change.Device = device_obj
        spec.DeviceChange = [device_change]
        request.Spec = spec
        # Start device removal
        spit_bucket.debug('Starting removal of device')
        self.execute_vm_change_task(request)
        spit_bucket.debug('Successfully removed device.')

    def attach_disk(self, disk_name, disk_capacity_in_kb, unit_number, controller_key=DEFAULT_SCSI_CONTROLLER_KEY):
        """
        Method used to attach stack drives from source Vm to the destination Vm.  It requires the attribute
        stack_disks to be set for the StackNinja class.  This procedure was adapted from a PySphere Google Group
        discussion (https://groups.google.com/forum/#!topic/pysphere/XejOd1VqqWU).
        """
        # Set up object to create vm request task for destination VM
        request = self.create_vm_change_request(self.vm._mor)
        # Create new VM Spec for the source VM
        spec = request.new_spec()
        device_change = spec.new_deviceChange()
        device_change.Operation = 'add'
        # Create Virtual Disk object
        hd = pysphere.resources.VimService_services.ns0.VirtualDisk_Def("hd").pyclass()
        hd.Key = -100
        hd.UnitNumber = unit_number
        hd.CapacityInKB = disk_capacity_in_kb
        hd.ControllerKey = controller_key
        # Create disk backing
        backing = pysphere.resources.VimService_services.ns0.VirtualDiskFlatVer2BackingInfo_Def("backing").pyclass()
        backing.FileName = disk_name
        backing.DiskMode = "persistent"
        backing.ThinProvisioned = False
        hd.Backing = backing
        # Make drive connectable
        connectable = hd.new_connectable()
        connectable.StartConnected = True
        connectable.AllowGuestControl = False
        connectable.Connected = True
        hd.Connectable = connectable
        device_change.Device = hd
        spec.DeviceChange = [device_change]
        request.Spec = spec
        # Start disk add
        spit_bucket.debug('Starting move of vmdk %s', disk_name)
        self.execute_vm_change_task(request)
        spit_bucket.info('Successfully moved vmdk: %s.', disk_name)

    def is_disk_remove_possible(self, disk_name):
        """
        Test whether the conditions to remove the disk are been met.
        The following conditions are tested:
        - The VM is powered off
        - The VM has no snapshots
        - The disk is present in machine.
        """
        if not self.vm.is_powered_off():
            spit_bucket.error('%s vm must be powered off for operation.', self.vm.properties.name)
            return False
        if self.vm.get_snapshots():
            spit_bucket.error('%s vm cannot have snapshots for operation.', self.vm.properties.name)
            return False
        disk = self.get_disk(disk_name)
        if not disk:
            spit_bucket.error('%s vm does not have disk "%s" for operation.', self.vm.properties.name, disk_name)
            return False
        return True

    def is_disk_move_possible(self, disk_name, target_data_store):
        """
        Test whether the conditions to move the disk are been met.
        The following conditions are tested:
        - The disk can be removed
        - The disk is already on the target data store.
        """
        if not self.is_disk_remove_possible(disk_name):
            return False
        if not disk_name.startswith(target_data_store):
            spit_bucket.error('Disk %s of %s vm is not in data store %s. Disk must be in target data store to be moved',
                              disk_name, self.vm.properties.name, target_data_store)
            return False
        return True

    def make_available_unit_number(self, disk_name=None):
        if disk_name:
            if not self.is_disk_remove_possible(disk_name):
                raise ValueError('Destination disk remove is not possible.')
            unit_number = self.get_disk_unit_number(disk_name)
            if unit_number:
                self.remove_disk(disk_name)
            return unit_number
        return self.get_next_unit_number()


class StackNinja(object):
    """
    This class can handle disk moves from one VM to a destination VM.
    """
    def __init__(self, address, login, password, port='443', protocol='https'):
        self.address = address
        self.login = login
        self.password = password
        self.port = port
        self.protocol = protocol

    @cached_property
    def vm_server(self):
        return pysphere.VIServer()

    def connect(self):
        url = VSPHERE_URL_PATTERN.format(protocol=self.protocol, address=self.address, port=self.port)
        self.vm_server.connect(url, self.login, self.password)
        spit_bucket.debug('Connected to vCenter %s.', self.address)

    def disconnect(self):
        self.vm_server.disconnect()
        spit_bucket.debug('Disconnected from vCenter.')

    def move_disk(self, source_vm, source_disk, destination_vm, destination_disk=None):
        """"
        Move disk from source to destination, if destination_disk_name is passed, then it will be removed before
        moving source disk
        """
        source_vm = VMwareVMOperator(self.vm_server, vm_name=source_vm)
        destination_vm = VMwareVMOperator(self.vm_server, vm_name=destination_vm)

        destination_data_store = destination_vm.get_data_store()
        if not source_vm.is_disk_move_possible(source_disk, destination_data_store):
            raise ValueError('Source disk move is not possible.')
        source_disk_capacity_in_kb = source_vm.get_disk_capacity_in_kb(source_disk)
        destination_unit_number = destination_vm.make_available_unit_number(destination_disk)
        source_vm.remove_disk(source_disk)
        destination_vm.attach_disk(source_disk, source_disk_capacity_in_kb, destination_unit_number)
