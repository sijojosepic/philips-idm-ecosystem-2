import sys
import unittest
from mock import MagicMock, patch

sys.modules['logging'] = MagicMock(name='logging')
sys.modules['pysphere'] = MagicMock(name='pysphere')
sys.modules['cached_property'] = MagicMock(name='cached_property')

from phim_isp_stack_ops import stack_ninja


class StackNinjaTestCase(unittest.TestCase):
    def setUp(self):
        address = "167.81.177.235"
        port = "443"
        protocol = "https"
        login = "administrator"
        password = "secret"
        unittest.TestCase.setUp(self)
        self.vmserver_patcher = patch('phim_isp_stack_ops.stack_ninja.StackNinja.vm_server')
        self.vmserver = self.vmserver_patcher.start()
        self.stackninja = stack_ninja.StackNinja(address, login, password, port, protocol)
        self.source_mock = MagicMock(name='source_mock')
        self.destination_mock = MagicMock(name='destination_mock')
        self.vmwo_patcher = patch('phim_isp_stack_ops.stack_ninja.VMwareVMOperator')
        self.vmwo_mock = self.vmwo_patcher.start()
        self.vmwo_mock.side_effect = [self.source_mock, self.destination_mock]

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.vmserver_patcher.stop()
        self.vmwo_patcher.stop()

    def test_connect_vm_server(self):
        self.stackninja.connect()
        self.vmserver.connect.assert_called_once_with(
            'https://167.81.177.235:443/sdk', 'administrator', 'secret')

    def test_disconnect_vm_server(self):
        self.stackninja.disconnect()
        self.vmserver.disconnect.assert_called_once_with()

    def test_move_disk_disk_move_not_possible(self):
        self.source_mock.is_disk_move_possible.return_value = False
        self.assertRaises(ValueError, self.stackninja.move_disk, 'vm1', 'disk1', 'vm2', 'disk2')

    def test_move_disk_disk_move(self):
        self.source_mock.is_disk_move_possible.return_value = True
        self.stackninja.move_disk('vm1', 'disk1', 'vm2', 'disk2')
        self.source_mock.remove_disk.assert_called_once_with('disk1')
        self.destination_mock.attach_disk.assert_called_once_with(
            'disk1',
            self.source_mock.get_disk_capacity_in_kb.return_value,
            self.destination_mock.make_available_unit_number.return_value
        )

    def test_vm_server(self):
        self.stackninja.vm_server()


class VMWareOperatorTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.server = MagicMock(name='vm_server')
        self.vmwareoperator = stack_ninja.VMwareOperator(self.server)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_get_vm_by_name_vm_server(self):
        self.vmwareoperator.get_vm_by_name('somevm')
        self.server.get_vm_by_name.assert_called_once_with(name='somevm')

    def test_create_vm_change_request(self):
        obj = MagicMock()
        obj.get_attribute_type = MagicMock(return_value="val")
        self.assertEqual(self.vmwareoperator.create_vm_change_request(obj), sys.modules['pysphere'].resources.VimService_services.ReconfigVM_TaskRequestMsg())

    def test_execute_vm_change_task(self):
        self.assertEqual(self.vmwareoperator.execute_vm_change_task("obj"),  sys.modules['pysphere'].VITask().wait_for_state())



class VMwareVMOperatorTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.server = MagicMock(name='vm_server')
        self.vm_patcher = patch('phim_isp_stack_ops.stack_ninja.VMwareVMOperator.vm')
        self.vm = self.vm_patcher.start()
        self.disks_patcher = patch('phim_isp_stack_ops.stack_ninja.VMwareVMOperator.disks')
        self.disks = self.disks_patcher.start()
        self.vmwarevmoperator = stack_ninja.VMwareVMOperator(self.server, "smovest1")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.vm_patcher.stop()
        self.disks_patcher.stop()

    def test_get_disk(self):
        self.disks.__iter__.return_value = [{'descriptor': '[LocalDS] smovest1/smovest1_2.vmdk'},
                                            {'descriptor': '[LocalDS] beastst3/beastst3_2.vmdk'}]
        self.assertEqual(self.vmwarevmoperator.get_disk('[LocalDS] beastst3/beastst3_2.vmdk'),
                         {'descriptor': '[LocalDS] beastst3/beastst3_2.vmdk'})

    def test_get_disk_not_found(self):
        self.vmwarevmoperator._disks = [{'descriptor': '[LocalDS] smovest1/smovest1_2.vmdk'},
                                        {'descriptor': '[LocalDS] beastst3/beastst3_2.vmdk'}]
        self.assertEqual(self.vmwarevmoperator.get_disk('[LocalDS] beastst3/beastst3_X.vmdk'), None)

    def test_get_disk_device(self):
        devices = [
            MagicMock(_type='VirtualDisk', unitNumber = 45),
            MagicMock(_type='VirtualMachine', unitNumber = 46),
            MagicMock(_type='VirtualDisk', unitNumber = 49),
        ]
        self.vm.properties.config.hardware.device = devices
        self.assertEqual(self.vmwarevmoperator.get_disk_device(49), devices[2])

    def test_get_disk_property(self):
        self.vmwarevmoperator.get_disk = MagicMock(name='get_disk', return_value={'device': {'test': 'val1'}})
        self.assertEqual(self.vmwarevmoperator.get_disk_property('disk1', 'test'), 'val1')
        self.vmwarevmoperator.get_disk.assert_called_once_with('disk1')

    def test_get_disk_capacity_in_kb(self):
        self.vmwarevmoperator.get_disk_property = MagicMock(name='get_disk_property', return_value='TEST1')
        self.assertEqual(self.vmwarevmoperator.get_disk_capacity_in_kb('disk1'), 'TEST1')
        self.vmwarevmoperator.get_disk_property.assert_called_once_with('disk1', 'capacityInKB')

    def test_get_disk_unit_number(self):
        self.vmwarevmoperator.get_disk_property = MagicMock(name='get_disk_property', return_value='TEST2')
        self.assertEqual(self.vmwarevmoperator.get_disk_unit_number('disk1'), 'TEST2')
        self.vmwarevmoperator.get_disk_property.assert_called_once_with('disk1', 'unitNumber')

    def test_get_data_store(self):
        self.vm.get_property.return_value = '[LocalDS] smovest1/smovest1_2.vmdk'
        self.assertEqual(self.vmwarevmoperator.get_data_store(), '[LocalDS]')
        self.vm.get_property.assert_called_once_with(from_cache=False, name='path')

    def test_get_next_unit_number(self):
        disks = [{'device': {'unitNumber': x}} for x in range(0, 10)]
        self.disks.__iter__.return_value = disks
        self.assertEqual(self.vmwarevmoperator.get_next_unit_number(), 10)

    def test_get_next_unit_number_too_high(self):
        disks = [{'device': {'unitNumber': x}} for x in range(0, stack_ninja.MAX_UNIT_NUMBER + 1)]
        self.disks.__iter__.return_value = disks
        self.assertEqual(self.vmwarevmoperator.get_next_unit_number(), None)

    def test_get_next_unit_number_at_edge(self):
        disks = [{'device': {'unitNumber': x}} for x in range(0, stack_ninja.MAX_UNIT_NUMBER)]
        self.disks.__iter__.return_value = disks
        self.assertEqual(self.vmwarevmoperator.get_next_unit_number(), 15)

    def test_remove_disk(self):
        self.vmwarevmoperator.get_disk = MagicMock(name='get_disk')
        self.vmwarevmoperator.get_disk_device = MagicMock(name='get_disk_device')
        self.vmwarevmoperator.remove_device = MagicMock(name='remove_device')
        self.vmwarevmoperator.remove_disk('disk1')
        self.vmwarevmoperator.get_disk.assert_called_once_with('disk1')
        self.vmwarevmoperator.get_disk_device.assert_called_once_with(
            self.vmwarevmoperator.get_disk.return_value.__getitem__.return_value.__getitem__.return_value)
        self.vmwarevmoperator.remove_device.assert_called_once_with(
            self.vmwarevmoperator.get_disk_device.return_value._obj)

    def test_is_disk_remove_possible_powered_on(self):
        self.vm.is_powered_off.return_value = False
        self.assertFalse(self.vmwarevmoperator.is_disk_remove_possible('disk1'))

    def test_is_disk_remove_possible_has_snapshots(self):
        self.vm.is_powered_off.return_value = True
        self.vm.get_snapshots.return_value = [{'snapshot': 'yes, here!'}]
        self.assertFalse(self.vmwarevmoperator.is_disk_remove_possible('disk1'))

    def test_is_disk_remove_possible_disk_not_found(self):
        self.vm.is_powered_off.return_value = True
        self.vm.get_snapshots.return_value = []
        self.vmwarevmoperator.get_disk = MagicMock(name='get_disk', return_value=None)
        self.assertFalse(self.vmwarevmoperator.is_disk_remove_possible('disk1'))

    def test_is_disk_remove_possible(self):
        self.vm.is_powered_off.return_value = True
        self.vm.get_snapshots.return_value = []
        self.vmwarevmoperator.get_disk = MagicMock(name='get_disk', return_value={'device':{}})
        self.assertTrue(self.vmwarevmoperator.is_disk_remove_possible('disk1'))

    def test_is_disk_move_possible_remove_not_possible(self):
        self.vmwarevmoperator.is_disk_remove_possible = MagicMock(name='is_disk_remove_possible', return_value=False)
        self.assertFalse(self.vmwarevmoperator.is_disk_move_possible('[LocalDS] smovest1/smovest1_2.vmdk', '[LocalDS]'))

    def test_is_disk_move_possible_not_int_target_datastore(self):
        self.vmwarevmoperator.is_disk_remove_possible = MagicMock(name='is_disk_remove_possible', return_value=True)
        self.assertFalse(self.vmwarevmoperator.is_disk_move_possible('[OtherDS] smovest1/smovest1_2.vmdk', '[LocalDS]'))

    def test_is_disk_move_possible(self):
        self.vmwarevmoperator.is_disk_remove_possible = MagicMock(name='is_disk_remove_possible', return_value=True)
        self.assertTrue(self.vmwarevmoperator.is_disk_move_possible('[LocalDS] smovest1/smovest1_2.vmdk', '[LocalDS]'))

    def test_make_available_unit_number_remove(self):
        self.vmwarevmoperator.is_disk_remove_possible = MagicMock(name='is_disk_remove_possible', return_value=True)
        self.vmwarevmoperator.get_disk_unit_number = MagicMock(name='get_disk_unit_number', return_value=10)
        self.vmwarevmoperator.remove_disk = MagicMock(name='remove_disk')
        self.assertEqual(self.vmwarevmoperator.make_available_unit_number('disk1'), 10)
        self.vmwarevmoperator.is_disk_remove_possible.assert_called_once_with('disk1')
        self.vmwarevmoperator.get_disk_unit_number.assert_called_once_with('disk1')
        self.vmwarevmoperator.remove_disk.assert_called_once_with('disk1')

    def test_make_available_unit_number_no_disk_name(self):
        self.vmwarevmoperator.get_next_unit_number = MagicMock(name='get_next_unit_number', return_value=12)
        self.assertEqual(self.vmwarevmoperator.make_available_unit_number(None), 12)

    def test_remove_device(self):
        self.vmwarevmoperator.remove_device("val")
        count = 0
        if sys.modules['logging'].getLogger().debug.call_count>0:
            count = 1
        self.assertEqual(count,1)

    def test_attach_device(self):
        self.vmwarevmoperator.attach_disk("name", "capacity", "number")
        count = 0
        if sys.modules['logging'].getLogger().debug.call_count>0:
            count = 1
        self.assertEqual(count,1)
        count = 0
        if sys.modules['logging'].getLogger().info.call_count>0:
            count = 2
        self.assertEqual(count,2)

    def test_vm(self):
        self.vmwarevmoperator.vm()

    def test_disks(self):
        self.vmwarevmoperator.disks()

if __name__ == '__main__':
    unittest.main()
