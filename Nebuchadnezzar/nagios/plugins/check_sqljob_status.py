#!/usr/bin/env python

"""
This plugin is used to monitor SQL Backup Jobs.
MSSQLServerLogsCollect
MSSQLSWBaseline
Maint_Backup_Full
Maint_Backup_Diff
PhilipsDatacenter.Weekly
PhilipsDatacenter.Every 15 Minutes
PhilipsDatacenter.Hourly
PhilipsDatacenter.Daily
Enable_Disable_Jobs
AdminDBfullBackup
AdminDBTrnBackup
AdminDBdiffBackup
AdminDBDBCCCheck
AdminDBRecycleLogs
Database_Failover
PurgeJobHistory
syspolicy_purge_history
Witness_Check
MSSQLBaselining

example
./check_sqljob_status.py  -H 1.1.1.1 -U 'ISEE02\phiadmin' -P 'password' -D mydb -J 'PhilipsDatacenter.Every 15 Minutes'
    OK: Database PhilipsDatacenter.Every 15 Minutes job is successful on 1.1.1.1.
    CRITICAL - Connection error with 1.1.1.1.

"""

from __future__ import print_function
import sys
import argparse
import pymssql
from scanline.utilities import dbutils

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

QUERY = """select top 1 j.Name as 'Job Name', j.job_id,
           case j.enabled  when 1 then 'Enable'
           when 0 then 'Disable'  end as 'Job Status',
           jh.run_date as [Last_Run_Date(YY-MM-DD)] ,
           STUFF(STUFF(RIGHT(REPLICATE('0', 6) +
           CAST(jh.run_time as varchar(6)), 6), 3, 0, ':'), 6, 0, ':') [Last_Run_Time] ,
           case jh.run_status when 0 then 'Failed'
           when 1 then 'Successful' when 2 then 'Retry'
           when 3 then 'Cancelled'
           when 4 then 'In Progress' end as Job_Execution_Status from sysJobHistory jh
           inner join  sysJobs j on j.job_id = jh.job_id
           where j.name ='{job_name}' and jh.run_date = (select max(hi.run_date)
           from sysJobHistory hi where jh.job_id = hi.job_id) order by jh.run_time desc"""
QUERY_PRIMARY_CHECK = """SELECT mirroring_role_desc
                        FROM  sys.database_mirroring m
                        WHERE  mirroring_state_desc IS NOT NULL"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=False, help='argument to post')
    parser.add_argument('-P', '--password', required=False, help='argument to post', default=2)
    parser.add_argument('-D', '--database', required=False, help='argument to post', default='')
    parser.add_argument('-J', '--job', required=False, help='The user name of the server', default='')
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.job, results.vigilant_url)


def get_job_status(hostname, username, password, database, job, vigilant_url):
    dbnodes = filter(lambda x: hostname in x, dbutils.get_dbnodes(vigilant_url))
    if dbnodes:
        dbnodes = dbnodes[0]
    else:
        dbnodes = dbutils.get_dbpartner_in_dbscanner(hostname, vigilant_url)

    if len(dbnodes) > 1:
        if not dbutils.get_mirroring_primary_check(hostname=hostname, username=username, password=password,
                                                   database=database):
            status, outmsg = OK, 'OK: This service check only applicable for primary node. {node} is not the primary node.'.format(
                node=hostname)
            return status, outmsg
    is_success = False
    is_enabled = False
    msg_dict = {'AdminDBfullBackup': 'full back up job',
                'AdminDBTrnBackup': 'transactional back up job',
                'AdminDBdiffBackup': 'differential back up job',
                'AdminDBDBCCCheck': 'consistency check job',
                'AdminDBRecycleLogs': 'recycle logs job',
                'Database_Failover': 'failover job',
                'PurgeJobHistory': 'purge job history job',
                'syspolicy_purge_history': 'syspolicy purge history job',
                'Witness_Check': 'witness check job',
                'MSSQLBaselining': 'MSSQL baselining job',
                'MSSQLServerLogsCollect': 'MSSQL server log collect job',
                'MSSQLSWBaseline': 'MSSQL SW baseline job',
                'Maint_Backup_Full': 'Maint_Backup_Full job',
                'Maint_Backup_Diff': 'Maint_Backup_Diff job',
                'PhilipsDatacenter.Weekly': 'PhilipsDatacenter.Weekly job',
                'PhilipsDatacenter.Every 15 Minutes': 'PhilipsDatacenter.Every 15 Minutes job',
                'PhilipsDatacenter.Hourly': 'PhilipsDatacenter hourly job',
                'PhilipsDatacenter.Daily': 'PhilipsDatacenter Daily job',
                'Enable_Disable_Jobs': 'Enable Disable Jobs'}
    ok_msg = 'OK: Database {job_type} is successful on {node}.'
    disable_critical = 'CRITICAL: Database {job_type} is not running on {node}'
    failed_critical = 'CRITICAL: Database {job_type} Failed on the node {node}'

    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
    except Exception as e:
        status, outmsg = CRITICAL, 'CRITICAL - Connection error with {node}. '.format(node=hostname)
        return status, outmsg

    cursor = conn.cursor()
    query = QUERY.format(job_name=job)
    cursor.execute(query)
    result = cursor.fetchone()
    if result:
        is_enabled = True if 'Enable' in result else False
        is_success = True if 'Successful' in result else False

    if not is_enabled:
        status, outmsg = CRITICAL, disable_critical.format(job_type=msg_dict[job], node=hostname)
        return status, outmsg
    if not is_success:
        status, outmsg = CRITICAL, failed_critical.format(job_type=msg_dict[job], node=hostname)
        return status, outmsg

    status, outmsg = OK, ok_msg.format(job_type=msg_dict[job], node=hostname)
    return status, outmsg

def main():
    cmd_args = check_arg()
    try:
        status, msg = get_job_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()
