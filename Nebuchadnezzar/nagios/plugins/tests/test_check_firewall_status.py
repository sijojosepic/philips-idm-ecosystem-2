from StringIO import StringIO

import unittest
from mock import MagicMock, patch, Mock
from requests.exceptions import RequestException
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)


class CheckFwallStatusParserTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_re = MagicMock(name='re')
        self.mock_sys = MagicMock(name='sys')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            're': self.mock_re,
            'sys': self.mock_sys,
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'scanline.utilities.utils': self.mock_scanline.utilities.str_to_list
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import check_firewall_status
        self.module = check_firewall_status
        self.FwallStatusParser = check_firewall_status.FwallStatusParser
        hostname = 'localhost'
        username = 'user1'
        password = 'pass'
        firewall_profiles = 'domain,public'
        self.fwallstatus_parser = self.FwallStatusParser(hostname, username, password, firewall_profiles)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.fwall_profiles = 'domain'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'domain'))

    def test_get_profiles(self):
        self.assertEqual(self.fwallstatus_parser.get_profiles(), set(['domain']))

    def test_clean_ps_response(self):
        data = '\r\nDomain Profile Settings: \r\n----------------------------------------------------------------------\r\nState                                 OFF\r\n\r\n'
        self.fwallstatus_parser.clean_ps_response(data)
        self.assertEqual(self.fwallstatus_parser.cleaned_data, 'Domain: State                                 OFF')

    def test_format_msg(self):
        state = 'OFF'
        profile = 'Domain'
        self.assertEqual(self.fwallstatus_parser.format_msg(profile, state), 'The Domain Firewall state is OFF.\n')

    def test_compose_msg(self):
        ok_msg = 'The Domain Firewall state is ON.\n'
        crt_msg = 'The Domain Firewall state is OFF.\n'
        self.assertEqual(self.fwallstatus_parser.compose_msg(ok_msg, crt_msg),
                         'CRITICAL : The Domain Firewall state is OFF.\nOK : The Domain Firewall state is ON.\n')

    def test_matched_fwall_profiles_case_1(self):
        data = 'Domain: State                                 OFF'
        self.fwallstatus_parser.cleaned_data = data
        self.mock_re.findall.return_value = [('Domain', 'OFF')]
        self.assertEqual(self.fwallstatus_parser.matched_fwall_profiles, [('Domain', 'OFF')])

    def test_matched_fwall_profiles_case_2(self):
        data = 'Domain: State                                 ON'
        self.fwallstatus_parser.cleaned_data = data
        self.mock_re.findall.return_value = [('Domain', 'ON')]
        self.assertEqual(self.fwallstatus_parser.matched_fwall_profiles, [('Domain', 'ON')])

    def test_execute_powershell_ok(self):
        mock_output = MagicMock(name="output")
        mock_output.status_code = 0
        mock_output.std_out = True
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        msg = 'OK : The Domain Firewall state is ON.\n'
        status = 0
        self.fwallstatus_parser.get_status = MagicMock(name='status', return_value=(status, msg))
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        self.assertEqual(self.fwallstatus_parser.execute_powershell(),
                         (0, 'OK : The Domain Firewall state is ON.\n| service status=0;1;2;0;2'))

    def test_execute_powershell_critical(self):
        mock_output = MagicMock(name="output")
        mock_output.status_code = 0
        mock_output.std_out = True
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        msg = 'CRITICAL : The Domain Firewall state is OFF.\n'
        status = 2
        self.fwallstatus_parser.get_status = MagicMock(name='status', return_value=(status, msg))
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        self.assertEqual(self.fwallstatus_parser.execute_powershell(),
                         (2, 'CRITICAL : The Domain Firewall state is OFF.\n| service status=2;1;2;0;2'))

    def test_execute_powershell_service_not_runnning(self):
        mock_output = MagicMock(name="output")
        mock_output.status_code = 1
        mock_output.std_out = 'An error occured while attempting to contact the Windows Firewall Service...'
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        msg = 'CRITICAL : The Domain Firewall state is OFF.\n'
        status = 2
        self.fwallstatus_parser.get_status = MagicMock(name='status', return_value=(status, msg))
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        self.assertEqual(self.fwallstatus_parser.execute_powershell(), (2,
                                                                        'CRITICAL : An error occured while attempting to contact the Windows Firewall Service...| service status=2;1;2;0;2'))

    def test_get_status_no_data(self):
        self.fwallstatus_parser.clean_ps_response = MagicMock(name='clean_ps_response')
        self.fwallstatus_parser.compose_state = MagicMock(name='compose_state',
                                                          return_value=('0', 'No firewall state found'))
        self.assertEqual(self.fwallstatus_parser.get_status('Fwall_status'), ('0', 'No firewall state found'))

    def test_get_status_ok(self):
        self.fwallstatus_parser.clean_ps_response = MagicMock(name='clean_ps_response')
        self.fwallstatus_parser.compose_state = MagicMock(name='compose_state',
                                                          return_value=(0, 'OK : The Domain Firewall state is ON'))
        self.assertEqual(self.fwallstatus_parser.get_status('Fwall_status'),
                         (0, 'OK : The Domain Firewall state is ON'))

    def test_get_status_critical(self):
        self.fwallstatus_parser.clean_ps_response = MagicMock(name='clean_ps_response')
        self.fwallstatus_parser.compose_state = MagicMock(name='compose_state', return_value=(
            2, 'CRITICAL : An error occured while attempting to contact the Windows Firewall Service'))
        self.assertEqual(self.fwallstatus_parser.get_status('Fwall_status'),
                         (2, 'CRITICAL : An error occured while attempting to contact the Windows Firewall Service'))

    def test_compose_state_ok(self):
        data = '\r\nDomain Profile Settings: \r\n----------------------------------------------------------------------\r\nState                                 ON'
        cleaned_data = [('Domain', 'ON')]
        self.fwallstatus_parser.clean_ps_response(data)
        self.mock_re.findall.return_value = cleaned_data
        self.assertEqual(self.fwallstatus_parser.compose_state(), (0, 'OK : The domain Firewall state is on.\n'))

    def test_compose_state_critical(self):
        data = '\r\nDomain Profile Settings: \r\n----------------------------------------------------------------------\r\nState                                 OFF'
        cleaned_data = [('Domain', 'OFF')]
        self.fwallstatus_parser.clean_ps_response(data)
        self.mock_re.findall.return_value = cleaned_data
        self.assertEqual(self.fwallstatus_parser.compose_state(), (2, 'CRITICAL : The domain Firewall state is off.\n'))

    def test_compose_state_no_data(self):
        data = '\r\n  xxxx'
        cleaned_data = [('aa', 'bbb')]
        self.fwallstatus_parser.clean_ps_response(data)
        self.mock_re.findall.return_value = cleaned_data
        self.assertEqual(self.fwallstatus_parser.compose_state(), (2, 'No firewall state found'))

    def test_matched_fwall_profiles(self):
        data = '\r\nDomain Profile Settings: \r\n----------------------------------------------------------------------\r\nState                                 ON'
        cleaned_data = ('Domain', 'ON')
        self.fwallstatus_parser.clean_ps_response(data)
        self.mock_re.findall.return_value = cleaned_data
        self.assertEqual(self.fwallstatus_parser.matched_fwall_profiles, ('Domain', 'ON'))

    def test_main_ok(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_obj = MagicMock(name='execute_powershell')
        mock_obj.execute_powershell.return_value = 0, 'msg'
        self.module.FwallStatusParser = MagicMock(name="FwallStatusParser", return_value=mock_obj)
        hostname = 'localhost'
        username = 'user1'
        password = 'pass'
        firewall_profiles = 'domain,public'
        self.module.main(hostname, username, password, firewall_profiles)
        self.sys_mock.exit.assert_called_once_with(0)
        mock_obj.execute_powershell.assert_called_once_with()

    def test_main_critical(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_obj = MagicMock(name='execute_powershell')
        mock_obj.execute_powershell.return_value = 1, 'msg'
        self.module.FwallStatusParser = MagicMock(name="FwallStatusParser", return_value=mock_obj)
        hostname = 'localhost'
        username = 'user1'
        password = 'pass'
        firewall_profiles = 'domain,public'
        self.module.main(hostname, username, password, firewall_profiles)
        self.sys_mock.exit.assert_called_once_with(1)
        mock_obj.execute_powershell.assert_called_once_with()

    def test_execute_powershell_AuthenticationError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError()
        data = self.fwallstatus_parser.execute_powershell()
        self.assertEqual(data, (3, 'UNKNOWN : WinRM Error | service status=3;1;2;0;2'))

    def test_execute_powershell_RequestException(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = RequestException()
        data = self.fwallstatus_parser.execute_powershell()
        self.assertEqual(data, (2, 'CRITICAL : Request Error | service status=2;1;2;0;2'))

    def test_execute_powershell_WinRMOperationTimeoutError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError()
        data = self.fwallstatus_parser.execute_powershell()
        self.assertEqual(data, (2, 'CRITICAL : WinRM Error | service status=2;1;2;0;2'))

    def test_execute_powershell_TypeError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError()
        data = self.fwallstatus_parser.execute_powershell()
        self.assertEqual(data, (
            2, 'CRITICAL : Typeerror(May be Issue in connecting to node - localhost)| service status=2;1;2;0;2'))

    def test_execute_powershell_TypeError_with_value(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('"takes exactly 2"')
        data = self.fwallstatus_parser.execute_powershell()
        self.assertEqual(data, (2, 'CRITICAL : Issue in connecting to node - localhost| service status=2;1;2;0;2'))

    def test_execute_powershell_Exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception()
        data = self.fwallstatus_parser.execute_powershell()
        self.assertEqual(data, (2, 'CRITICAL : Exception | service status=2;1;2;0;2'))


if __name__ == '__main__':
    unittest.main()
