from StringIO import StringIO
import sys
import unittest
import requests

from mock import MagicMock, patch
from winrm.exceptions import AuthenticationError, WinRMOperationTimeoutError, WinRMError

class VeeamBackupTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'requests': self.mock_request,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'winrm': self.mock_winrm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_veeam_backup
        self.module = check_veeam_backup

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.jobtype = 'backup'
        mock_result.backuptype = 'veeam_backup'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'backup', 'veeam_backup'))

    def test_get_ps_vbr_script(self):
        vbr_script = """ Add-PSSnapin VeeamPSSnapin
        $VJobs = Get-VBRJob
        ForEach ($VJob in $VJobs) {
        $name = $VJob.Info.Name
        $jobtype = $VJob.Info.JobType
        $result = $VJob.Info.LatestStatus
        Write-Output $name`t$jobtype`t$result
        } """
        self.assertEqual(self.module.get_ps_vbr_script(), vbr_script)

    def test_get_ps_vsb_script(self):
        vsb_script = """ Add-PSSnapin VeeamPSSnapin
        $VJobs = Get-VSBJob
        ForEach ($VJob in $VJobs) {
        $name = $VJob.Name
        $jobtype = $VJob.JobType
        $result = $VJob.GetLastResult()
        Write-Output $name`t$jobtype`t$result
        } """
        self.assertEqual(self.module.get_ps_vsb_script(), vsb_script)

    def test_get_backup_type_veeam_backup(self):
        backuptype = 'veeam_backup'
        mock_get_ps_vbr_script = MagicMock(name='get_ps_vbr_script')
        mock_get_ps_vbr_script.return_value = 'vbr_script'
        self.module.get_ps_vbr_script = mock_get_ps_vbr_script
        self.assertEqual(self.module.get_backup_type(backuptype), 'vbr_script')
        mock_get_ps_vbr_script.assert_called_once_with()
        mock_get_ps_vsb_script = MagicMock(name='get_ps_vsb_script')
        mock_get_ps_vsb_script.return_value = 'vsb_script'
        self.module.get_ps_vsb_script = mock_get_ps_vsb_script
        self.assertEqual(mock_get_ps_vsb_script.call_count, 0)

    def test_get_backup_type_sure_backup(self):
        backuptype = 'sure_backup'
        mock_get_ps_vsb_script = MagicMock(name='get_ps_vsb_script')
        mock_get_ps_vsb_script.return_value = 'vsb_script'
        self.module.get_ps_vsb_script = mock_get_ps_vsb_script
        self.assertEqual(self.module.get_backup_type(backuptype), 'vsb_script')
        mock_get_ps_vsb_script.assert_called_once_with()

    def test_get_status_critical1(self):
        failed_job = ['failed_job1', 'failed_job2', 'failed_job3']
        success_job = ['success_job1', 'success_job2', 'success_job3']
        warning_job = []
        self.assertEqual(self.module.get_status(failed_job, success_job, warning_job),
                         (2, 'CRITICAL - Failed Jobs 3\nfailed_job1\nfailed_job2\nfailed_job3'))
    
    def test_get_status_critical2(self):
        failed_job = ['failed_job1', 'failed_job2', 'failed_job3']
        success_job = ['success_job1', 'success_job2', 'success_job3']
        warning_job = ['warning_job1', 'warning_job2', 'warning_job3']
        self.assertEqual(self.module.get_status(failed_job, success_job, warning_job),
                         (2, 'CRITICAL - Failed Jobs 3\nfailed_job1\nfailed_job2\nfailed_job3\n\nWarning Jobs 3\nwarning_job1\nwarning_job2\nwarning_job3'))

    def test_get_status_warning(self):
        failed_job = []
        success_job = ['success_job1', 'success_job2', 'success_job3']
        warning_job = ['warning_job1', 'warning_job2', 'warning_job3']
        self.assertEqual(self.module.get_status(failed_job, success_job, warning_job),
                         (1, 'WARNING - Warning Jobs 3\nwarning_job1\nwarning_job2\nwarning_job3'))
    
    def test_get_status_ok1(self):
        failed_job = []
        success_job = []
        warning_job = []
        self.assertEqual(self.module.get_status(failed_job, success_job, warning_job), (0, 'OK - No jobs are running'))

    def test_get_status_ok2(self):
        failed_job = []
        success_job = ['success_job1', 'success_job2', 'success_job3']
        warning_job = []
        self.assertEqual(self.module.get_status(failed_job, success_job, warning_job),
                         (0, 'OK - All jobs run successfully'))
    
    def test_get_job_status_stderr(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.std_err = 'cmdlets not found'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'Error while running cmdlets -cmdlets not found'))

    def test_get_job_status_stdout1(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.status_code = 0
        mock_WinRM.execute_ps_script.return_value.std_out = 'prod_backup1\tBackup\tFailed\r\nprod_backup2\tBackup\tSuccess\r\n'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL - Failed Jobs 1\nprod_backup1'))

    def test_get_job_status_stdout2(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.status_code = 0
        mock_WinRM.execute_ps_script.return_value.std_out = 'prod_backup1\tBackup\tSuccess\r\nprod_backup2\tBackup\tSuccess\r\n'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (0, 'OK - All jobs run successfully'))

    def test_get_job_status_stdout3(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.status_code = 0
        mock_WinRM.execute_ps_script.return_value.std_out = 'prod_backup1\tBackup\tWarning\r\nprod_backup2\tBackup\tWarning\r\n'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (1, 'WARNING - Warning Jobs 2\nprod_backup1\nprod_backup2'))

    def test_get_job_status_stdout4(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value.status_code = 0
        mock_WinRM.execute_ps_script.return_value.std_out = 'prod_backup1\tBackup\tFailed\r\nprod_backup2\tBackup\tFailed\r\n'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL - Failed Jobs 2\nprod_backup1\nprod_backup2'))

    def test_get_job_status_Exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL : Exception e'))

    def test_get_job_status_WinRMOperationTimeoutError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError('WinRMOperationTimeoutError')
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL : WinRM Error WinRMOperationTimeoutError'))

    def test_get_job_status_AuthenticationError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('AuthenticationError')
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL : WinRM Error AuthenticationError'))

    def test_get_job_status_WinRMError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('WinRMError')
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL : WinRM Error WinRMError'))

    def test_get_job_status_exception_type_error_takes_exactly_2(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype')
        self.assertEqual(self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype'),
                         (2, 'CRITICAL : Issue in connecting to node - hostname'))

    def test_get_job_status_exception_type_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        result = self.module.get_job_status('hostname', 'username', 'password', 'Backup', 'backuptype')
        self.assertEqual(result, (2, 'CRITICAL : Typeerror(May be Issue in connecting to node - hostname)'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_job_status = MagicMock(name="get_job_status")
        self.module.get_job_status = mock_get_job_status
        self.module.get_job_status.return_value = 0, 'msg'
        mock_check_arg = MagicMock(name='check_arg')
        self.module.check_arg = mock_check_arg
        self.module.check_arg.return_value = ('hostname', 'username', 'password', 'Backup', 'backuptype')
        self.module.main()
        self.sys_mock.exit.assert_called_once_with(0)
        mock_get_job_status.assert_called_once_with('hostname', 'username', 'password', 'Backup', 'backuptype')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

if __name__ == '__main__':
    unittest.main()
