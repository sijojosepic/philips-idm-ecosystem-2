import unittest
from mock import MagicMock, patch
from StringIO import StringIO


class TestDisk:
    def __init__(self, diskpath, capacity, freeSpace):
        self.diskPath = diskpath
        self.capacity = capacity
        self.freeSpace = freeSpace


class VcenterDiskTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_pyVim = MagicMock(name='pyVim')
        self.mock_pyVmomi = MagicMock(name='pyVmomi')
        self.mock_atexit = MagicMock(name='atexit')
        self.mock_sys = MagicMock(name='sys')
        modules = {
            'argparse': self.mock_argparse,
            'pyVim': self.mock_pyVim,
            'pyVmomi': self.mock_pyVmomi,
            'atexit': self.mock_atexit,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_vcenter_disk
        self.module = check_vcenter_disk

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.hostname ='h'
        obj2.username='u'
        obj2.password ='p'
        obj2.warning_val ='dri'
        obj2.critical_val ='f'        
        obj2.property = 'prop'
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.module.check_arg(), ('h','u','p','dri','f','prop'))
    

    def test_size_fmt_bytes(self):
        self.assertEqual(self.module.sizeof_fmt(1000), '1000.0bytes')

    def test_size_fmt_KB(self):
        self.assertEqual(self.module.sizeof_fmt(2000), '2.0KB')

    def test_size_fmt_MB(self):
        self.assertEqual(self.module.sizeof_fmt(1048576), '1.0MB')

    def test_size_fmt_GB(self):
        self.assertEqual(self.module.sizeof_fmt(1073741824), '1.0GB')

    def test_size_fmt_TB(self):
        self.assertEqual(self.module.sizeof_fmt(1024*1024*1024*1024), '1.0TB')

    def test_convert_to_gb_1GB(self):
        self.assertEqual(self.module.convert_to_gb(1073741824), '1.0')

    def test_convert_to_gb_1TB(self):
        self.assertEqual(self.module.convert_to_gb(1024*1024*1024*1024), '1024.0')

    def test_calculate_percentage(self):
        self.assertEqual(self.module.calculate_percentage(88888888, 8888888888), '99.00')

    def test_server_connect(self):
        connection = self.mock_pyVim.connect.SmartConnect.return_value
        disconnect = self.mock_pyVim.connect.Disconnect
        self.assertEqual(self.module.server_connect('10.220.3.1', 'tester', 'CryptThis'), connection)
        self.mock_pyVim.connect.SmartConnect.assert_called_once_with(host='10.220.3.1', user='tester', pwd='CryptThis')
        self.mock_atexit.register.assert_called_once_with(disconnect, connection)

    def test_disk_info(self):
        disks = iter([TestDisk('S:\\', 8586784768L, 1462870016L)])
        self.assertEqual(self.module.disk_info(disks, '80', '90'),
                         (['82.96'], 'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n',
                          "'S:\\'=82.96%;80;90;\n"))

    def test_datastore_info(self):
        mock_datastore1 = MagicMock()
        mock_datastore1.summary.name, mock_datastore1.summary.capacity, mock_datastore1.summary.freeSpace = 'localstore1', 8586784768L, 1462870016L
        mock_datastore2 = MagicMock()
        mock_datastore2.summary.name, mock_datastore2.summary.capacity, mock_datastore2.summary.freeSpace = 'localstore2', 17173569536L, 1462870016L
        mock_datastore3 = MagicMock()
        mock_datastore3.summary.name, mock_datastore3.summary.capacity, mock_datastore3.summary.freeSpace = 'localstore3', 17173569536L, 1073741824L
        self.assertEqual(self.module.datastore_info([mock_datastore1, mock_datastore2, mock_datastore3], 93),
                         (['Name= localstore1, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n',
                          'Name= localstore2, Capacity= 16.0GB, Free space= 1.4GB, Percentage used= 91.48\n'],
                         ['Name= localstore3, Capacity= 16.0GB, Free space= 1.0GB, Percentage used= 93.75\n'],
                         "'localstore1'=6.6GB;;7.4;0;8.0\n'localstore2'=14.6GB;;14.9;0;16.0\n'localstore3'=15.0GB;;14.9;0;16.0\n"))

    def test_check_threshold(self):
        self.assertEqual(self.module.check_threshold(['50.66', '55.77', '60.11'], '61', '80', critical=False), False)
        self.assertEqual(self.module.check_threshold(['50.66', '55.77', '60.11'], '60', '80'), True)

    def test_get_status_warning(self):
        self.assertEqual(self.module.get_status(['50.66', '55.77', '62.11'],
                                                'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB,'
                                                ' Percentage used= 82.96\n', 'S:\\=82.96%;80;90;', '61', '80'),
                         (1, 'WARNING: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n '
                             '| S:\\=82.96%;80;90;'))

    def test_get_status_critical(self):
        self.assertEqual(self.module.get_status(['50.66', '55.77', '82.11'],
                                                'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB,'
                                                ' Percentage used= 82.96\n', 'S:\\=82.96%;80;90;', '61', '80'),
                         (2, 'CRITICAL: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n '
                             '| S:\\=82.96%;80;90;'))

    def test_get_status_ok(self):
        self.assertEqual(self.module.get_status(['50.66', '55.77', '62.11'],
                                                'Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB,'
                                                ' Percentage used= 82.96\n', 'S:\\=82.96%;80;90;', '63', '80'),
                         (0, 'OK: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96\n '
                             '| S:\\=82.96%;80;90;'))

    def test_get_status_datastore_usage(self):
        ok_msgs = ['Name= Localstore15, Capacity= 30731.8GB, Free space= 22163.2GB, Percentage used= 27.88\n',
                   'Name= Localstore16, Capacity= 30731.8GB, Free space= 23458.6GB, Percentage used= 23.67\n']
        critical_msgs = ['Name= Localstore70, Capacity= 5581.2GB, Free space= 208.9GB, Percentage used= 96.26\n']
        self.assertEqual(self.module.get_status_datastore_usage(ok_msgs, critical_msgs, 'perf_msg', '93'),
                         (2, 'CRITICAL: There are 1/3 datastores which have crossed critical value of 93%\n'
                             'Name= Localstore70, Capacity= 5581.2GB, Free space= 208.9GB, Percentage used= 96.26\n'
                             'OK: There are 2/3 datastores which are under critical value of 93%\n'
                             'Name= Localstore15, Capacity= 30731.8GB, Free space= 22163.2GB, Percentage used= 27.88\n'
                             'Name= Localstore16, Capacity= 30731.8GB, Free space= 23458.6GB, Percentage used= 23.67\n | perf_msg'))

    def test_get_status_datastore_usage_only_critical(self):
        critical_msgs = ['Name= Localstore70, Capacity= 5581.2GB, Free space= 208.9GB, Percentage used= 96.26\n',
                         'Name= Localstore71, Capacity= 5581.2GB, Free space= 368.5GB, Percentage used= 93.40\n']
        self.assertEqual(self.module.get_status_datastore_usage([], critical_msgs, 'perf_msg', '93'),
                         (2, 'CRITICAL: There are 2/2 datastores which have crossed critical value of 93%\n'
                             'Name= Localstore70, Capacity= 5581.2GB, Free space= 208.9GB, Percentage used= 96.26\n'
                             'Name= Localstore71, Capacity= 5581.2GB, Free space= 368.5GB, Percentage used= 93.40\n | perf_msg'))

    def test_get_status_datastore_usage_only_ok(self):
        ok_msgs = ['Name= Localstore15, Capacity= 30731.8GB, Free space= 22163.2GB, Percentage used= 27.88\n',
                   'Name= Localstore16, Capacity= 30731.8GB, Free space= 23458.6GB, Percentage used= 23.67\n']
        self.assertEqual(self.module.get_status_datastore_usage(ok_msgs, [], 'perf_msg', '93'),
                         (0, 'OK: There are 2/2 datastores which are under critical value of 93%\n'
                             'Name= Localstore15, Capacity= 30731.8GB, Free space= 22163.2GB, Percentage used= 27.88\n'
                             'Name= Localstore16, Capacity= 30731.8GB, Free space= 23458.6GB, Percentage used= 23.67\n | perf_msg'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_disk(self, std_out):
        self.module.get_status = MagicMock(name='get_status', return_value=(0, 'OK: disk info | perf msg'))
        self.module.sys = MagicMock(name='sys')
        self.module.main('h', 'u', 'p', 'w', 'c', 'disk')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'OK: disk info | perf msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_datastore(self, std_out):
        self.module.get_status_datastore_usage = MagicMock(name='get_staus_datastore_usage',
                                                          return_value=(2, 'CRITICAL: datastore info | perf msg'))
        self.module.datastore_info = MagicMock(name='datastore_info', return_value=('','',''))
        self.module.sys = MagicMock(name='sys')
        self.module.main('h', 'u', 'p', 'w', 'c', 'datastore')
        self.module.sys.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL: datastore info | perf msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.server_connect = MagicMock(name='server_connect')
        self.module.server_connect.side_effect = Exception('Exception')
        self.module.sys = MagicMock(name='sys')
        self.module.main('h', 'u', 'p', 'w', 'c', 'd')
        self.module.sys.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'Cannot complete login due to an incorrect user name or password or '
                                             'Could not connect to vCenter server.\n')
    

if __name__ == '__main__':
    unittest.main()
