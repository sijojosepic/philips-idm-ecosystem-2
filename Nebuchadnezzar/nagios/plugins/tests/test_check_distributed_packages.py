import unittest
from mock import MagicMock, patch

class GetDistributedPackagesTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_os =  MagicMock(name='os')
        modules = {
            'scanline': self.mock_scanline,
            'os': self.mock_os,
            'scanline.component': self.mock_scanline.component,
            'scanline.component.localhost': self.mock_scanline.component.localhost,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.utils': self.mock_scanline.utilities.utils
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_distributed_packages
        self.module = check_distributed_packages

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_neb_ip(self):
        mock_localhost_obj = MagicMock(name='LocalHost')
        mock_localhost_obj.__setattr__('ip_address', '1.2.3.4')
        self.mock_scanline.component.localhost.LocalHost.return_value = mock_localhost_obj
        self.assertEqual(self.module.get_neb_ip(), '1.2.3.4')

    def test_check_valid_file(self):
        self.assertEqual(self.module.check_valid_file('abc.xml', ['xml', 'zip']), True)

    def test_check_valid_file_invalid(self):
        self.assertEqual(self.module.check_valid_file('abc.json', ['xml', 'zip']), False)

    def test_get_directory_struct_package_info(self):
        self.mock_os.listdir.side_effect = [['ISP', 'AWV'], ['def.xml', 'def.zip'], ['abc.xml', 'abc.zip']]
        self.mock_os.path.isdir.side_effect = [True, False, False, True, False, False]
        self.module.check_valid_file = MagicMock(name='check_valid_file')
        self.module.check_valid_file.side_effect = [True, True, True, True]
        self.assertEqual(self.module.get_directory_struct('', 'repo/Tools/PCM/packages', '', '1.1.1.1', ['xml', 'zip'], 'packages'),
                         'AWV\n'
                         'https://1.1.1.1/repo/Tools/PCM/packages/AWV/def.xml\n'
                         'https://1.1.1.1/repo/Tools/PCM/packages/AWV/def.zip\n'
                         'ISP\n'
                         'https://1.1.1.1/repo/Tools/PCM/packages/ISP/abc.xml\n'
                         'https://1.1.1.1/repo/Tools/PCM/packages/ISP/abc.zip\n')

    def test_get_directory_struct_site_info(self):
        self.mock_os.listdir.return_value = ['abc.xml', 'abc.zip']
        self.mock_os.path.isdir.side_effect = [False, False]
        self.module.check_valid_file = MagicMock(name='check_valid_file')
        self.module.check_valid_file.side_effect = [True, True]
        self.assertEqual(self.module.get_directory_struct('', 'info', '', '1.1.1.1', ['xml', 'zip'], 'info'),
                         'https://1.1.1.1/info/abc.xml\n'
                         'https://1.1.1.1/info/abc.zip\n')

    def test_get_files_list_parameters(self):
        self.module.get_neb_ip = MagicMock(name='get_neb_ip', return_value='1.1.1.1')
        self.module.get_directory_struct = MagicMock(name='get_directory_struct')
        self.mock_os.path.exists.return_value = True
        self.mock_scanline.utilities.utils.str_to_list.return_value = ['xml', 'zip']
        self.module.get_files_list('/var/philips/repo/Tools/PCM/packages/', '["zip", "xml"]')
        self.module.get_directory_struct.assert_called_once_with('/var/philips/repo/Tools/PCM/packages/',
                                                                 'repo/Tools/PCM/packages', '', '1.1.1.1', ['xml', 'zip'], 'packages')

    def test_get_files_list_message_no_files(self):
        self.module.get_neb_ip = MagicMock(name='get_neb_ip', return_value='1.1.1.1')
        self.mock_os.path.exists.return_value = False
        self.assertEqual(self.module.get_files_list('', ''), 'Specified path does not exist in Neb')

    def test_main(self):
        self.module.get_files_list = MagicMock(name='get_files_list',return_value='xyz')
        self.module.check_arg = MagicMock(name='check_arg', return_value='')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()