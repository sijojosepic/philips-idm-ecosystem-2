'''
created by Vijender.singh@philips.com on 8/5/18
'''
import unittest
import json
from argparse import Namespace
from mock import MagicMock, patch, Mock
from winrm.exceptions import AuthenticationError


class TestCheckStratoscaleVmHealth(unittest.TestCase):
    CRITICAL_STATUS = 2
    WARNING_STATUS = 1
    OK_STATUS = 0
    EXPECTED_CPU_USAGE = 30
    EXPECTED_RAM_USAGE = 83
    EXPECTED_STORAGE_USAGE = 94

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_redis = MagicMock(name='redis')
        modules = {
            'requests': self.mock_requests,
            'redis': self.mock_redis
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'winrm': self.mock_winrm,
        }
        self.mock_redis.StrictRedis = MagicMock(name='strictredis')
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_stratoscale_vm_health

        self.check_stratoscale_vm_health = check_stratoscale_vm_health
        self.domainname = 'cloud_admin'
        self.username = 'admin'
        self.password = 'admin'
        self.hostname = '130.147.86.215'
        self.vmid = 'a4ebd786-0109-4aa5-a975-140915da5dbd'
        self.warningthreshold = 80
        self.criticalthreshold = 90
        self.resourceType = 'CPU'
        self.floatingip = '130.147.86.161'
        self.ostype = 'LINUX'
        self.vmusername = 'root'
        self.vmpassword = 'ph!lt0r@st3nt0r'
        self.servicetype = None

        self.vm_info_response_stub = {
            "network__rx_kbps__of__vm__in__kbps": [["a4ebd786-0109-4aa5-a975-140915da5dbd", 2607.9749],
                                                   ["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2547.3064]],
            "cpu__used__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 46.306805],
                                               ["a4ebd786-0109-4aa5-a975-140915da5dbd", 90.031555]],
            "network__tx_kbps__of__vm__in__kbps": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2666.5305],
                                                   ["a4ebd786-0109-4aa5-a975-140915da5dbd", 2078.196]],
            "memory__used_by_guest_os__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 75.246735],
                                                              ["a4ebd786-0109-4aa5-a975-140915da5dbd", 44.29414]]}

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('argparse.ArgumentParser.parse_args')
    def test_check_arg(self, mock_argparse):
        mock_argparse.return_value = Namespace(hostname=self.hostname, domainname=self.domainname,
                                               username=self.username, password=self.password,
                                               vmid=self.vmid,
                                               warningthreshold=self.warningthreshold,
                                               criticalthreshold=self.criticalthreshold,
                                               resourceType=self.resourceType,
                                               floatingip=self.floatingip,
                                               ostype=self.ostype,
                                               vmusername=self.vmusername,
                                               vmpassword=self.vmpassword, servicetype=self.servicetype)
        results = self.check_stratoscale_vm_health.check_arg()
        assert results == (
            self.hostname, self.domainname, self.username, self.password, self.vmid, self.warningthreshold,
            self.criticalthreshold, self.resourceType, self.floatingip, self.ostype, self.vmusername,
            self.vmpassword, self.servicetype)

    def test_get_token_payload(self):
        expected_body_val = json.dumps({'auth': {'identity': {'methods': ['password'], 'password': {
            'user': {'name': 'admin', 'password': 'admin', 'domain': {'name': 'cloud_admin'}}}},
                                                 'scope': {'domain': {'name': 'cloud_admin'}}}})
        expected_header = {'content-type': 'application/json'}
        result = self.check_stratoscale_vm_health.get_token_payload(self.domainname, self.username, self.password)
        assert expected_header == result.headers
        assert expected_body_val == result.body

    def test_get_token(self):
        mock_headers = {'x-subject-token': 'SomeBase64String'}
        self.mock_requests.post.return_value = Mock(ok=True, headers=mock_headers, status_code=200)
        token_payload = self.check_stratoscale_vm_health.get_token_payload(self.domainname, self.username,
                                                                           self.password)
        token = self.check_stratoscale_vm_health.get_token(self.hostname, token_payload)
        self.assertEqual(token, 'SomeBase64String')

    def test_get_product_id(self):
        stub_productid_json_response = [
            {
                "is_domain": "false",
                "name": "default",
                "enabled": "true",
                "domain_name": "cloud_admin",
                "id": "d134786daf55448d8c02b97601aa38e8",
                "parent_id": "null",
                "domain_id": "default",
                "description": "Default project"
            }
        ]
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stub_productid_json_response
        product_id = self.check_stratoscale_vm_health.get_product_id(self.hostname, 'SomeBase64String', self.domainname)
        self.assertEqual(product_id, 'd134786daf55448d8c02b97601aa38e8')

    def test_get_product_token_payload(self):
        expected_body_val = json.dumps({
            'auth': {'identity': {'methods': ['token'], 'token': {'id': 'SomeBase64String'}},
                     'scope': {'project': {'id': 'd134786daf55448d8c02b97601aa38e8'}}}})
        expected_header = {'content-type': 'application/json'}
        result = self.check_stratoscale_vm_health.get_product_token_payload('SomeBase64String',
                                                                            'd134786daf55448d8c02b97601aa38e8')
        assert expected_header == result.headers
        assert expected_body_val == result.body

    def test_get_stratoscale_vms_info(self):
        stub_vm_info_response = {
            "network__rx_kbps__of__vm__in__kbps": [["a4ebd786-0109-4aa5-a975-140915da5dbd", 2607.9749],
                                                   ["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2547.3064]],
            "cpu__used__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 46.306805],
                                               ["a4ebd786-0109-4aa5-a975-140915da5dbd", 90.031555]],
            "network__tx_kbps__of__vm__in__kbps": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 2666.5305],
                                                   ["a4ebd786-0109-4aa5-a975-140915da5dbd", 2078.196]],
            "memory__used_by_guest_os__of__vm__in__percent": [["fe081659-4acb-4cf0-8925-47b6cf69cd65", 75.246735],
                                                              ["a4ebd786-0109-4aa5-a975-140915da5dbd", 44.29414]]}

        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stub_vm_info_response
        result = self.check_stratoscale_vm_health.get_stratoscale_vms_info(self.hostname, 'SomeBase64String')
        assert result == self.vm_info_response_stub

    def test_get_memory_status(self):
        status, message = self.check_stratoscale_vm_health.get_memory_status(
            self.vm_info_response_stub, self.vmid,
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 0)

    def test_get_memory_status_for_shutoff_vm(self):
        # "1234" : Non-existent VM Id
        status, message = self.check_stratoscale_vm_health.get_memory_status(
            self.vm_info_response_stub, "1234",
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 3)

    def test_get_cpu_status(self):
        status, message = self.check_stratoscale_vm_health.get_cpu_status(
            self.vm_info_response_stub, self.vmid,
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 2)

    def test_get_cpu_status_for_shutoff_vm(self):
        # "1234" : Non-existent VM Id
        status, message = self.check_stratoscale_vm_health.get_cpu_status(
            self.vm_info_response_stub, "1234",
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 3)

    def test_get_network_tx(self):
        status, message = self.check_stratoscale_vm_health.get_network_tx(
            self.vm_info_response_stub, self.vmid,
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 0)

    def test_get_network_tx_for_shutoff_vm(self):
        # "1234" : Non-existent VM Id
        status, message = self.check_stratoscale_vm_health.get_network_tx(
            self.vm_info_response_stub, "1234",
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 3)

    def test_get_network_rx(self):
        status, message = self.check_stratoscale_vm_health.get_network_rx(
            self.vm_info_response_stub, self.vmid,
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 0)

    def test_get_network_rx_for_shutoff_vm(self):
        # "1234" : Non-existent VM Id
        status, message = self.check_stratoscale_vm_health.get_network_rx(
            self.vm_info_response_stub, "1234",
            self.warningthreshold, self.criticalthreshold)
        self.assertEqual(status, 3)

    def test_get_storage_status_without_ostag(self):
        status, message = self.check_stratoscale_vm_health.get_storage_status('None', self.floatingip,
                                                                              self.vmusername,
                                                                              self.vmpassword,
                                                                              self.warningthreshold,
                                                                              self.criticalthreshold)
        self.assertEqual(status, 1)

    def test_get_storage_status_without_fip(self):
        status, message = self.check_stratoscale_vm_health.get_storage_status(self.ostype, 'None', self.vmusername,
                                                                              self.vmpassword,
                                                                              self.warningthreshold,
                                                                              self.criticalthreshold)
        self.assertEqual(status, 1)

    def test_linux_storage_status(self):
        mock_obj = Mock()
        attrs = {'self.check_stratoscale_vm_health.get_linux_storage_usage.return_value': (
        16, 'Drives:DATA = 16% SYSTEM = 0%')}
        mock_obj.configure_mock(**attrs)
        percent, message = mock_obj.self.check_stratoscale_vm_health.get_linux_storage_usage(self.floatingip,
                                                                                             self.vmusername,
                                                                                             self.vmpassword)
        self.assertEqual(percent, 16)

    def test_windows_storage_status_none(self):
        self.check_stratoscale_vm_health.check_windows_storage_usage = MagicMock(name='check_windows_storage_usage', return_value=(None,'None'))
        status, message = self.check_stratoscale_vm_health.get_storage_status('WINDOWS', 'floating_ip', 'username', 'password', 'warn_threshold', 'crit_threshold')
        self.assertEqual(status, 3)
        self.assertEqual(message, 'None')
        self.check_stratoscale_vm_health.check_windows_storage_usage.assert_called_once_with('floating_ip', 'username', 'password')

    def test_windows_storage_status_warning(self):
        self.check_stratoscale_vm_health.check_windows_storage_usage = MagicMock(name='check_windows_storage_usage', return_value=(80,'80'))
        self.check_stratoscale_vm_health.get_health_status = MagicMock(name='get_health_status', return_value=(1,'Warning'))
        status, message = self.check_stratoscale_vm_health.get_storage_status('WINDOWS', 'floating_ip', 'username', 'password', 'warn_threshold', 'crit_threshold')
        self.assertEqual(status, 1)
        self.assertEqual(message, 'Warning')
        self.check_stratoscale_vm_health.check_windows_storage_usage.assert_called_once_with('floating_ip', 'username', 'password')
        self.check_stratoscale_vm_health.get_health_status.assert_called_once_with(80, 'warn_threshold', 'crit_threshold', 'STORAGE', '80')

    def test_check_windows_storage_usage_auth(self):
        self.check_stratoscale_vm_health._get_win_label_wise_usage = MagicMock(name='_get_win_label_wise_usage',return_value='Authentication Error')
        percentage, message = self.check_stratoscale_vm_health.check_windows_storage_usage('ip','user','password')
        self.assertEqual(percentage,None)
        self.assertEqual(message, 'Authentication Error please check username and password')
        self.check_stratoscale_vm_health._get_win_label_wise_usage.assert_called_once_with('ip','user','password')

    def test_check_windows_storage_usage_vm(self):
        self.check_stratoscale_vm_health._get_win_label_wise_usage = MagicMock(name='_get_win_label_wise_usage',return_value='VM Connection Error')
        percentage, message = self.check_stratoscale_vm_health.check_windows_storage_usage('ip','user','password')
        self.assertEqual(percentage,None)
        self.assertEqual(message, 'VM maybe ShutOff or Winrm service is not running')
        self.check_stratoscale_vm_health._get_win_label_wise_usage.assert_called_once_with('ip','user','password')

    def test_check_windows_storage_usage_other(self):
        self.check_stratoscale_vm_health._get_win_label_wise_usage = MagicMock(name='_get_win_label_wise_usage',return_value=80)
        self.check_stratoscale_vm_health._is_storage_monitored = MagicMock(name='_is_storage_monitored',return_value=1)
        self.check_stratoscale_vm_health._get_highest_storage_usage = MagicMock(name='_get_highest_storage_usage',return_value=1)
        percentage, message = self.check_stratoscale_vm_health.check_windows_storage_usage('ip','user','password')
        self.assertEqual(percentage,1)
        self.assertEqual(message, '80')
        self.check_stratoscale_vm_health._get_win_label_wise_usage.assert_called_once_with('ip','user','password')
        self.check_stratoscale_vm_health._is_storage_monitored.assert_called_once_with(80)
        self.check_stratoscale_vm_health._get_highest_storage_usage.assert_called_once_with(80)

    def test_check_windows_storage_usage_other(self):
        self.check_stratoscale_vm_health._get_win_label_wise_usage = MagicMock(name='_get_win_label_wise_usage',return_value=80)
        self.check_stratoscale_vm_health._is_storage_monitored = MagicMock(name='_is_storage_monitored',return_value=0)
        percentage, message = self.check_stratoscale_vm_health.check_windows_storage_usage('ip','user','password')
        self.assertEqual(percentage,None)
        self.assertEqual(message,'No drives to be monitored as no suitable labels found')
        self.check_stratoscale_vm_health._get_win_label_wise_usage.assert_called_once_with('ip','user','password')
        self.check_stratoscale_vm_health._is_storage_monitored.assert_called_once_with(80)

    def test_get_reachability_status_vm(self):
        self.check_stratoscale_vm_health.get_token_payload = MagicMock(name='token_payload')
        self.check_stratoscale_vm_health.get_token = MagicMock(name='token')
        status, message = self.check_stratoscale_vm_health.get_reachability_status_vm('ip','domain','user','pwd')
        self.assertEqual(status, 0)
        self.assertEqual(message, 'Ok - Token generation passed for ip')

    def test_get_reachability_status_vm_exception(self):
        self.check_stratoscale_vm_health.get_token_payload = MagicMock(name='token_payload')
        self.check_stratoscale_vm_health.get_token = MagicMock(name='token')
        self.check_stratoscale_vm_health.get_token.side_effect = Exception('e')
        status, message = self.check_stratoscale_vm_health.get_reachability_status_vm('ip','domain','user','pwd')
        self.assertEqual(status, 2)
        self.assertEqual(message, 'Critical - Token generation failed for ip, Please check credentials')

    def test_get_reachability_status_valid(self):
        self.mock_redis.StrictRedis.return_value.hget.return_value = '1'
        status, message = self.check_stratoscale_vm_health.get_reachability_status('ip','domain','user','pwd')
        self.assertEqual(status, 0)
        self.assertEqual(message, 'Ok - Token generation passed for ip')

    def test_get_reachability_status_notvalid(self):
        self.mock_redis.StrictRedis.return_value.hget.return_value = '0'
        status, message = self.check_stratoscale_vm_health.get_reachability_status('ip','domain','user','pwd')
        self.assertEqual(status, 2)
        self.assertEqual(message, 'Critical - Token generation failed for ip, Please check credentials')

    def test_get_reachability_status_exception(self):
        self.mock_redis.StrictRedis.return_value.hget.side_effect = Exception('e')
        self.check_stratoscale_vm_health.get_reachability_status_vm = MagicMock(name='reachability')
        self.check_stratoscale_vm_health.get_reachability_status('ip','domain','user','pwd')
        self.check_stratoscale_vm_health.get_reachability_status_vm.assert_called_once_with('ip','domain','user','pwd')
     
    def test_get_token_from_redis(self):
        self.mock_redis.StrictRedis.return_value.hgetall.return_value = {'token': 'value'}
        self.assertEqual(self.check_stratoscale_vm_health.get_token_from_redis(), 'value')

    def test_get_token_from_redis_notoken(self):
        self.mock_redis.StrictRedis.return_value.hgetall.return_value = None
        self.assertEqual(self.check_stratoscale_vm_health.get_token_from_redis(), False)
    
    def test_insert_token_to_redis(self):
        self.mock_redis.StrictRedis.return_value.hmset = MagicMock(name="hmset")
        self.check_stratoscale_vm_health.insert_token_to_redis('token')
        self.mock_redis.StrictRedis.return_value.hmset.assert_called_once_with('hsopToken', {'token': 'token'})

if __name__ == '__main__':
    unittest.main()

