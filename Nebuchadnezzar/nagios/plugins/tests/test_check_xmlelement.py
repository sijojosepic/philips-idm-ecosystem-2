import unittest
from mock import MagicMock, patch


class CheckXMLElementTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_requests = MagicMock(name='requests')
            self.mock_argparse = MagicMock(name='argparse')
            self.mock_sys = MagicMock(name='sys')
            modules = {
                'requests': self.mock_requests,
                'argparse':self.mock_argparse,
                'sys': self.mock_sys,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import check_xmlelement
            self.check_xmlelement = check_xmlelement

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()

        def test_check_arg(self):
            obj = MagicMock()
            obj2 = MagicMock()
            obj2.url = 'u'
            obj2.element ='e'
            obj2.text ='t'
            obj2.description ='d'
            obj2.insecure='i'
            self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
            obj.parse_args.return_value = obj2
            self.assertEqual(self.check_xmlelement.check_arg(),('u','e','t','d','i'))
        
        def test_main(self):
            self.check_xmlelement.check_xmlelement = MagicMock(return_value=(1,'msg'))
            self.mock_sys.exit = MagicMock()
            self.check_xmlelement.main()


class CheckXMLElementGetElementContentTestCase(CheckXMLElementTest.TestCase):
    def setUp(self):
        CheckXMLElementTest.TestCase.setUp(self)
        doc_to_parse = """
<root>
  <ele1>
    <good1>Super</good1>
  </ele1>
  <output>Test finding output!</output>
</root>"""
        self.doc1 = self.check_xmlelement.libxml2.parseDoc(doc_to_parse)

    def tearDown(self):
        CheckXMLElementTest.TestCase.tearDown(self)

    def test_get_element_content_simple(self):
        self.assertEqual(self.check_xmlelement.get_element_content(self.doc1, 'output'), 'Test finding output!')

    def test_get_element_content_nested(self):
        self.assertEqual(self.check_xmlelement.get_element_content(self.doc1, 'good1'), 'Super')

    def test_get_element_content_drill(self):
        self.assertEqual(self.check_xmlelement.get_element_content(self.doc1, 'ele1'), 'Super')

    def test_get_element_content_absent(self):
        self.assertEqual(self.check_xmlelement.get_element_content(self.doc1, 'not_existing'), None)


class CheckXMLElementTestCase(CheckXMLElementTest.TestCase):
    def setUp(self):
        CheckXMLElementTest.TestCase.setUp(self)
        self.check_xmlelement.libxml2 = MagicMock(name='libxml2')
        self.check_xmlelement.get_element_content = MagicMock(name='get_element_content')
        return_map = {'element': 'text', 'description': 'Everything is dandy'}
        self.check_xmlelement.get_element_content.side_effect = lambda y, x: return_map.get(x)

    def tearDown(self):
        CheckXMLElementTest.TestCase.tearDown(self)

    def test_check_xmlelement_all_ok(self):
        self.assertEqual(
            self.check_xmlelement.check_xmlelement('url', 'element', 'text', 'description', False),
            (0, 'text : Everything is dandy')
        )

    def test_check_xmlelement_text_mismatch(self):
        self.assertEqual(
            self.check_xmlelement.check_xmlelement('url', 'element', 'fail', 'description', False),
            (2, 'text : Everything is dandy')
        )

    def test_check_xmlelement_msg_missing(self):
        self.assertEqual(
            self.check_xmlelement.check_xmlelement('url', 'element', 'text', 'no_description', False),
            (0, 'text : None')
        )

    def test_check_xmlelement_msg_and_element_missing(self):
        self.assertEqual(
            self.check_xmlelement.check_xmlelement('url', 'no_element', 'text', 'no_description', False),
            (3, 'Status could not be determined')
        )

    def test_check_xmlelement_request_exception(self):
        self.check_xmlelement.requests.exceptions.RequestException = MyException
        self.check_xmlelement.get_element_content.side_effect = MyException('some http error')
        self.assertEqual(
            self.check_xmlelement.check_xmlelement('url', 'no_element', 'text', 'no_description', False),
            (2, "Address 'url' could not be read: some http error")
        )

    def test_check_xmlelement_xmlparse_exception(self):
        self.check_xmlelement.libxml2.parserError = MyException
        self.check_xmlelement.get_element_content.side_effect = MyException
        self.assertEqual(
            self.check_xmlelement.check_xmlelement('url', 'no_element', 'text', 'no_description', False),
            (3, 'Could not parse xml')
        )


class MyException(Exception):
    pass


if __name__ == '__main__':
    unittest.main()


