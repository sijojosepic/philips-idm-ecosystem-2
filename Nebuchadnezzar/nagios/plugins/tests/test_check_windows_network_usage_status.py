import unittest
from mock import MagicMock, patch
from requests.exceptions import RequestException
from winrm.exceptions import WinRMOperationTimeoutError, AuthenticationError

class NetWorkUsageTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_windows_network_usage_status
        self.module = check_windows_network_usage_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_parse_cmd_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.critical = 90
        mock_result.warning = 75
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.parse_cmd_args()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 90, 75))

    def test_process_output_OK(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = 0
        response = self.module.process_output(mock_out_put, 90, 75)
        self.assertEqual(response, (0, 'OK - The network usage is 0 percentage| network usage=0;75;90;0;100'))

    def test_process_output_OK(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = 0
        response = self.module.process_output(mock_out_put, 90, 75)
        self.assertEqual(response, (0, 'OK - The network usage is 0 percentage| network usage=0;75;90;0;100'))

    def test_process_output_WARNING(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = 76
        response = self.module.process_output(mock_out_put, 90, 75)
        self.assertEqual(response, (1, 'WARNING - The network usage is 76 percentage which has breached the threshold value of 75 percentage| network usage=1;75;90;0;100'))

    def test_process_output_CRITICAL(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = 93
        response = self.module.process_output(mock_out_put, 90, 75)
        self.assertEqual(response, (2, 'CRITICAL - The network usage is 93 percentage which has breached the threshold value of 90 percentage| network usage=2;75;90;0;100'))

    def test_process_output_ERROR(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 1
        mock_out_put.std_err = 'Status Code'
        response = self.module.process_output(mock_out_put, 90, 75)
        self.assertEqual(response, (2, 'WinRM Error Status Code'))

    def test_main(self):
        self.module.sys = MagicMock(name='sys')
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 10
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.return_value = mock_WinRM
        self.module.process_output = MagicMock(name="process_output")
        self.module.process_output.return_value = (0, "Ok")
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.process_output.assert_called_once_with(10, 90, 75)
        self.module.sys.exit.assert_called_once_with(0)

    def test_main_winrm_exception(self):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = Exception()
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.sys.exit.assert_called_once_with(2)

    def test_main_winrm_AuthenticationError(self):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = AuthenticationError()
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.sys.exit.assert_called_once_with(3)

    def test_main_winrm_WinRMOperationTimeoutError(self):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = WinRMOperationTimeoutError()
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.sys.exit.assert_called_once_with(2)

    def test_main_winrm_TypeError_Case_1(self):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = TypeError()
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.sys.exit.assert_called_once_with(2)

    def test_main_winrm_TypeError_Case_2(self):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = TypeError("takes exactly 2")
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.sys.exit.assert_called_once_with(2)

    def test_main_winrm_RequestException(self):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = RequestException
        self.module.main('localhost', 'test1', 'pass1', 90, 75)
        self.module.sys.exit.assert_called_once_with(2)


if __name__ == '__main__':
    unittest.main()