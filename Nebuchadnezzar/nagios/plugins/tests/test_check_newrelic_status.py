from StringIO import StringIO

import unittest
from mock import MagicMock, patch, call
from requests.exceptions import RequestException
import requests
import winrm


class NewRelicStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_requests = MagicMock(name='requests')
        modules = {
            'argparse': self.mock_argparse,
            'requests': self.mock_requests,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_newrelic_status
        self.module = check_newrelic_status
        self.StatusVerifier = check_newrelic_status.StatusVerifier
        violations = {'json': 'data'}
        self.status_verifier = self.StatusVerifier(violations)

    def test_check_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.api_key = 'temp-key'
        mock_result.url = 'url'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('temp-key', 'url'))

    def test_get_url(self):
        self.mock_requests.get.return_value = {'json': 'data'}
        output = self.module.get_url('key', 'url')
        self.mock_requests.get.assert_called_once_with('url',
                                                       headers={'X-Api-Key': 'key', 'content-type': 'application/json'},
                                                       timeout=5, verify=False)
        self.assertEqual(output, {'json': 'data'})

    def test_error_stats_401(self):
        mock_response = MagicMock(name='response')
        mock_response.status_code = 401
        self.assertEqual(self.module.error_stats(mock_response), (2, 'CRITICAL - Invalid API key or Your New Relic API access is not enabled, status code 401.'))

    def test_error_stats_500(self):
        mock_response = MagicMock(name='response')
        mock_response.status_code = 500
        self.assertEqual(self.module.error_stats(mock_response), (2, 'CRITICAL - We hit a server error or error occurred while trying to list violations or resource timed out, status code 500.'))

    def test_error_stats_other_err(self):
        mock_response = MagicMock(name='response')
        mock_response.status_code = 700
        self.assertEqual(self.module.error_stats(mock_response), (2, 'CRITICAL - Error connecting to New Relic API, status code 700.'))

    def test_format_msg(self):
        obj = {'entity': {'name': 'app'}}
        response = self.status_verifier.format_msg(obj)
        self.status_verifier.MSG_TEMP = MagicMock(name='status', return_value=".APM name - newrelicname, Alert Policy - , Alert label - .")
        self.assertEqual(response, "APM name - app, Alert Policy - , Alert label - .\n")

    def test_compose_status(self):
        data = {'closed_a': 'data', 'priority': 'Critical'}
        self.status_verifier.violations = MagicMock(name="violations")
        self.status_verifier.violations = [data]
        self.status_verifier.format_msg = MagicMock(name='format_msg')
        self.status_verifier.compose_status()
        self.status_verifier.format_msg.assert_called_once_with(data)
        self.assertEqual(self.status_verifier.format_msg.call_count, 1)

    def test_compose_msg(self):
        msg_lst = ['x', 'y']
        msg_template = "MSG TEMPLATE{0}"
        response = self.status_verifier.compose_msg(msg_lst, msg_template)
        self.assertEqual(response, 'MSG TEMPLATExy')

    def test_get_status_crit_warn(self):
        self.status_verifier.st_dct = {'Critical': 'CRITICAL_VAL', 'Warning': 'WARNING_VAL'}
        data = {'closed_a': 'data', 'priority': 'Critical'}
        self.status_verifier.compose_status = MagicMock(name="violations",  return_value=data)
        response = self.status_verifier.get_status()
        self.assertEqual(response, (2, 'CRITICAL - CRITICAL_VALWARNING - WARNING_VAL'))

    def test_get_status_ok(self):
        self.status_verifier.st_dct = {'Critical': None, 'Warning': None}
        data = {'closed_a': 'data', 'priority': 'Critical'}
        self.status_verifier.compose_status = MagicMock(name="violations",  return_value=data)
        response = self.status_verifier.get_status()
        self.assertEqual(response, (0, 'OK - No Alerts violations found.'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock

        mock_get_url = MagicMock(name='get_url')
        mock_get_url.json.return_value = {'violations': []}
        self.module.get_url = mock_get_url

        mock_obj = MagicMock(name='get_status')
        mock_obj.get_status.return_value = 0, 'msg'
        self.module.StatusVerifier = MagicMock(name="ststus_verify", return_value=mock_obj)

        self.module.main('key', 'url')
        self.sys_mock.exit.assert_called_once_with(0)
        mock_get_url.assert_called_once_with('key', 'url')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_err(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.get_url = MagicMock(name='get_url', return_value=False)
        self.module.error_stats = MagicMock(name="error_status", return_value=(2, "msg"))
        self.module.main('key', 'url')
        self.sys_mock.exit.assert_called_once_with(2)
        self.module.get_url.assert_called_once_with('key', 'url')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_requests.get.side_effect = RequestException('e')
        self.module.RequestException = RequestException
        self.module.main('key', 'url')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Request Error - e.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_requests.get.side_effect = Exception('e')
        self.module.main('key', 'url')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Error - e.\n')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
