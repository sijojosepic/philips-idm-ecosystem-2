import unittest
import mock
import requests
from mock import MagicMock, patch, Mock


class HelionProxyURLStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils.collection.DictNoEmpty = dict
        self.OBJSTMock = MagicMock(name='blah')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        self.mock_time = MagicMock(name='time')
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.hsdp_obs': self.mock_scanline.utilities.hsdp_obs,
            'scanline.utilities.hsdp_obs.OBJSTConnection': self.OBJSTMock,
            'argparse': self.mock_argparse,
            'sys': self.mock_sys,
            'time': self.mock_time,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_helion_heartbeat_status
        from scanline.utilities import hsdp_obs
        self.OBJSTConnection = hsdp_obs.OBJSTConnection()
        self.check_helion_heartbeat_status = check_helion_heartbeat_status
        from check_helion_heartbeat_status import get_stored_data
        self.get_stored_data = get_stored_data
        from check_helion_heartbeat_status import create_file_to_post
        self.create_file_to_post = create_file_to_post
        from check_helion_heartbeat_status import delete_file_from_disk
        self.delete_file_from_disk = delete_file_from_disk
        from check_helion_heartbeat_status import create_container
        self.create_container = create_container
        from check_helion_heartbeat_status import put_file_to_container
        self.put_file_to_container = put_file_to_container
        from check_helion_heartbeat_status import manipulate_file_on_obs
        self.manipulate_file_on_obs = manipulate_file_on_obs
        from check_helion_heartbeat_status import delete_container
        self.delete_container = delete_container
        from check_helion_heartbeat_status import check_args
        self.check_args = check_args
        from check_helion_heartbeat_status import main
        self.main = main


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.container_name ='c'
        obj2.filename='f'
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.check_args(),('c','f'))


    def test_main(self):
      self.check_args = MagicMock(return_value=('container_name', 'file_name'))
      self.check_helion_heartbeat_status.get_stored_data = MagicMock(return_value='stored_data')
      self.check_helion_heartbeat_status.create_file_to_post = MagicMock(return_value='val')
      self.check_helion_heartbeat_status.create_container = MagicMock(return_value=('container_created', 'state', 'message'))
      self.check_helion_heartbeat_status.put_file_to_container = MagicMock(return_value=('file_posted', 'state', 'message'))
      self.check_helion_heartbeat_status.manipulate_file_on_obs = MagicMock(return_value=('file_deleted', 'state', 'message'))
      self.check_helion_heartbeat_status.delete_container = MagicMock(return_value=('state','message'))
      self.check_helion_heartbeat_status.delete_file_from_disk = MagicMock(return_value='val')
      self.main()

    def test_main_with_all_ifs(self):
      self.check_args = MagicMock(return_value=('container_name', 'file_name'))
      self.check_helion_heartbeat_status.get_stored_data = MagicMock(return_value='stored_data')
      self.check_helion_heartbeat_status.create_file_to_post = MagicMock(return_value='val')
      self.check_helion_heartbeat_status.create_container = MagicMock(return_value=(None, 'state', 'message'))
      self.check_helion_heartbeat_status.put_file_to_container = MagicMock(return_value=(None, 'state', 'message'))
      self.check_helion_heartbeat_status.manipulate_file_on_obs = MagicMock(return_value=(None, 'state', 'message'))
      self.check_helion_heartbeat_status.delete_container = MagicMock(return_value=('state','message'))
      self.check_helion_heartbeat_status.delete_file_from_disk = MagicMock(return_value='val')
      self.main()

    @patch('check_helion_heartbeat_status.requests')
    def test_create_container_status_200(self, mock_requests):
        mock_requests.get.return_value.status_code = 200
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.create_container(stored_data, 'idm_test'),
                         (True, 0, 'Container created successfully.'))



    def test_stored_data_ok(self):
        self.OBJSTConnection.to_dict.return_value = {1:'hello'}
        self.assertEqual(self.get_stored_data(), {1:'hello'})

    def test_stored_data_exception(self):
        self.OBJSTConnection.to_dict = MagicMock(side_effect=Exception)
        self.assertEqual(self.get_stored_data(), {})

    @patch('check_helion_heartbeat_status.open', create=True)
    def test_create_file_to_post(self, mock_open):
        self.assertEqual(self.create_file_to_post('idm_check.txt'), None)

    def test_delete_file_from_disk_ok(self):
        self.assertEqual(self.delete_file_from_disk('idm_check.txt'), None)

    def test_delete_file_from_disk_exception(self):
        self.assertEqual(self.delete_file_from_disk('idm_checks.txt'), None)

    @patch('check_helion_heartbeat_status.requests')
    def test_create_container_status_401(self, mock_requests):
        mock_requests.get.return_value.status_code = 401
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.create_container(stored_data, 'idm_test'),
                         (False, 2, 'Could not create container, status code: 401'))

    @patch('check_helion_heartbeat_status.requests')
    def test_create_container_status_404(self, mock_requests):
        mock_requests.get.return_value.status_code = 404
        mock_requests.put.return_value.status_code = 200
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.create_container(stored_data, 'idm_test'),
                         (True, 0, 'Container created successfully.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_create_container_status_204(self, mock_requests):
        mock_requests.get.return_value.status_code = 404
        mock_requests.put.return_value.status_code = 204
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.create_container(stored_data, 'idm_test'),
                         (True, 0, 'Container created successfully.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_create_container_status_201(self, mock_requests):
        mock_requests.get.return_value.status_code = 404
        mock_requests.put.return_value.status_code = 201
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.create_container(stored_data, 'idm_test'),
                         (True, 0, 'Container created successfully.'))

    @patch('check_helion_heartbeat_status.requests.get')
    def test_create_container_status_invalid_request(self, mock_get):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_get.side_effect = requests.exceptions.ConnectionError
        self.assertEqual(self.create_container(stored_data, 'idm_test'),
                         (False, 2, 'Invalid put request, could not create container.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_put_file_to_container_invalid_request(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        self.assertEqual(self.put_file_to_container(stored_data,
                                                    'idm_check.txt', 'idm_test',
                                                    False),
                         (False, 2, 'Invalid put request, could not post file.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_put_file_to_container_ok(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.put.return_value.status_code = 201
        with mock.patch('__builtin__.open') as file_mock:
            file_mock.return_value.__enter__ = lambda s: s
            file_mock.return_value.__exit__ = mock.Mock()
            file_mock.return_value.read.return_value = 'sample data'
            self.assertTrue(self.put_file_to_container(stored_data,
                                                       'idm_check.txt',
                                                       'idm_test', True))

    @patch('check_helion_heartbeat_status.requests')
    def test_put_file_to_container_exception(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.put.return_value.status_code = 404
        with mock.patch('__builtin__.open') as file_mock:
            file_mock.return_value.__enter__ = lambda s: s
            file_mock.return_value.__exit__ = mock.Mock()
            file_mock.return_value.read.return_value = 'sample data'
            self.assertEqual(self.put_file_to_container(stored_data,
                                                        'idm_check.txt',
                                                        'idm_test', True),
                             (False, 2, 'Could not post file, status code: 404'))

    @patch('check_helion_heartbeat_status.requests')
    def test_manipulate_file_on_obs_get_ok(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.get.return_value.status_code = 200
        mock_requests.get.__name__ = 'get'
        self.assertEqual(self.manipulate_file_on_obs(stored_data, True,
                                                     'idm_test', 'idm_check.txt',
                                                     mock_requests.get, False),
                         (True, 0, 'File found on OBS.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_manipulate_file_on_obs_get_404(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.get.return_value.status_code = 404
        mock_requests.get.__name__ = 'get'
        self.assertEqual(self.manipulate_file_on_obs(stored_data, True, 'idm_test',
                                                     'idm_check.txt',
                                                     mock_requests.get, False),
                         (False, 2, 'File not found on OBS, status code: 404'))

    @patch('check_helion_heartbeat_status.requests')
    def test_manipulate_file_on_obs_get_invalid_request(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.get.return_value.status_code = 404
        mock_requests.get.__name__ = 'get'
        self.assertEqual(self.manipulate_file_on_obs(stored_data, False,
                                                     'idm_test', 'idm_check.txt',
                                                     mock_requests.get, False),
                         (False, 2, 'Invalid get request, could not find file on OBS.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_manipulate_file_on_obs_delete_ok(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.delete.return_value.status_code = 204
        mock_requests.delete.__name__ = 'delete'
        self.assertEqual(self.manipulate_file_on_obs(stored_data, False,
                                                     'idm_test', 'idm_check.txt',
                                                     mock_requests.delete, True),
                         (True, 0, 'File successfully deleted.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_manipulate_file_on_obs_delete_404(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.delete.return_value.status_code = 404
        mock_requests.delete.__name__ = 'delete'
        self.assertEqual(self.manipulate_file_on_obs(stored_data, False,
                                                     'idm_test', 'idm_check.txt',
                                                     mock_requests.delete, True),
                         (False, 2, 'Could not delete file, status code: 404'))

    @patch('check_helion_heartbeat_status.requests')
    def test_manipulate_file_on_obs_delete_invalid_request(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.delete.return_value.status_code = 404
        mock_requests.delete.__name__ = 'delete'
        self.assertEqual(self.manipulate_file_on_obs(stored_data, False,
                                                     'idm_test', 'idm_check.txt',
                                                     mock_requests.delete, False),
                         (False, 2, 'Invalid delete request, could not delete file from OBS.'))

    @patch('check_helion_heartbeat_status.requests')
    def test_delete_container_ok(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.delete.return_value.status_code = 204
        self.assertTrue(self.delete_container(stored_data, True, 'idm_test'))

    @patch('check_helion_heartbeat_status.requests')
    def test_delete_container_exception(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.delete.return_value.status_code = 404
        self.assertEqual(self.delete_container(stored_data, True, 'idm_test'),
                         (2, 'Could not delete container, status code: 404'))

    @patch('check_helion_heartbeat_status.requests')
    def test_delete_container_invalid_request(self, mock_requests):
        stored_data = {'token_issued_at': u'2017-08-03T11:11:20.850448Z',
                       'token_id': u'db55f21b592f4c56b671039ed2841625',
                       'keystone_url': u'https://localhost:5000/v2.0/tokens',
                       'obs_url': u'https://localhost:8080/v1/AUTH_4c2bc62d02ef42efbc9a27036113a1f9',
                       'payload': u"{'auth': {'passwordCredentials':{'username"
                                  u"' : 'username', 'password' : 'password'},"
                                  u"'tenantName':'tenent1'}}",
                       'token_expires_at': u'2017-08-03T15:11:20Z'}
        mock_requests.delete.return_value.status_code = 404
        self.assertEqual(self.delete_container(stored_data, False, 'idm_test'),
                         (2, 'Invalid delete request, could not delete container.'))


if __name__ == '__main__':
    unittest.main()
