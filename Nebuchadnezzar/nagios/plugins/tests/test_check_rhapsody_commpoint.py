import unittest
from mock import MagicMock, patch, Mock
import sys

class CheckRhapsodyCommpointTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_request,
            'argparse': self.mock_argparse
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rhapsody_commpoint
        self.module = check_rhapsody_commpoint
        

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_output_state0(self):
        self.assertEqual(self.module.get_output_state(10, 12, '20', '40'), 0)

    def test_get_output_state1(self):
        self.assertEqual(self.module.get_output_state(10, 12, '5', '40'), 1)

    def test_get_output_state2(self):
        self.assertEqual(self.module.get_output_state(10, 12, '2', '10'), 2)

    @patch('check_rhapsody_commpoint.requests.post')
    def test_fetch_component_id_200(self, mock_post):
        mock_post.return_value.status_code = 200
        mock_post.return_value.content = 153
        self.assertEqual(self.module.fetch_component_id('127.0.0.1', 8544, 'guest',
                                                 'home/user/hl7web'),
                         (200, 153))

    @patch('check_rhapsody_commpoint.requests.post')
    def test_fetch_component_id_404(self, mock_post):
        mock_post.return_value.status_code = 404
        self.assertEqual(self.module.fetch_component_id('127.0.0.1', 8544, 'guest',
                                                 'home/user/hl7web'),
                         (404, None))

    @patch('check_rhapsody_commpoint.requests.post')
    def test_fetch_component_id_exception(self, mock_post):
        mock_post.side_effect = Exception
        self.assertEqual(self.module.fetch_component_id('127.0.0.1', 8544, 'guest',
                                                 'home/user/hl7web'), None)

    @patch('check_rhapsody_commpoint.requests.get')
    @patch('check_rhapsody_commpoint.requests.post')
    def test_get_queue_messages_200(self, mock_get, mock_post):
        mock_post.return_value.status_code = 200
        mock_get.return_value.status_code = 200
        content_val = '{"data": {"inQueueSize": 10, "outQueueSize": 20}}'
        mock_post.return_value.content = content_val
        self.assertEqual(self.module.get_queue_messages('127.0.0.1', 8544, 'guest',
                                                 'guest', 'home/user/hl7web',
                                                 5, 10),
                         ( {'home/user/hl7web': {'status': 2, 'msg': 'home/user/hl7web : Message(s) found in inQueue = 10, outQueue = 20'}}))

    @patch('check_rhapsody_commpoint.requests.get')
    @patch('check_rhapsody_commpoint.requests.post')
    def test_get_queue_messages_404(self, mock_get, mock_post):
        mock_post.return_value.status_code = 404
        mock_get.return_value.status_code = 404
        self.assertEqual(self.module.get_queue_messages('127.0.0.1', 8544, 'guest',
                                                 'guest', 'home/user/hl7web',
                                                 5, 10),
                         ({'home/user/hl7web': {'status': 2, 'msg': 'Could not fetch attributes for home/user/hl7web, status code: 404 \n'}}))

    @patch('check_rhapsody_commpoint.requests.get')
    @patch('check_rhapsody_commpoint.requests.post')
    def test_get_queue_messages_exception(self, mock_get, mock_post):
        mock_post.side_effect = Exception
        mock_get.side_effect = Exception
        self.assertEqual(self.module.get_queue_messages('127.0.0.1', 8544, 'guest',
                                                 'guest', 'home/user/hl7web',
                                                 5, 10),
                         ({'home/user/hl7web': {'status': 2, 'msg': 'Could not connect to Rhapsody end point 127.0.0.1 \n'}}))

    def test_get_status(self):
        result = {'Locker/commpoint1':{'status': 0, 'msg': 'inQueue 5 outQueue 10'}}
        self.assertEqual(self.module.get_status(result, 15, 20), (0, 'inQueue 5 outQueue 10\n'))

    def test_get_status_not_commpoint(self):
        result = {'Locker/commpoint1':{'status': 2, 'msg': 'Could not fetch attributes for Locker/commpoint1, status code: 404 \n'}}
        self.assertEqual(self.module.get_status(result, 15, 20), (2, 'Could not fetch attributes for Locker/commpoint1, status code: 404 \n'))

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.port = 56
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.component_path = 'Locker/commpoint1'
        mock_result.warning_val = 5
        mock_result.critical_val = 10
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 56, 'test1', 'pass1', 'Locker/commpoint1', 5, 10))

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value=('hostname', 'port', 'username', 'password', 'component_path',
         'warning_val','critical_val')
        self.module.get_queue_messages = MagicMock(name='get_queue_messages')
        result = {'Locker/commpoint1':{'status': 0, 'msg': 'inQueue 5 outQueue 10'}}
        self.module.get_queue_messages.return_value = {'Locker/commpoint1':{'status': 0, 'msg': 'inQueue 5 outQueue 10'}}
        self.module.get_status = MagicMock(name='get_status')
        self.module.get_status.return_value = (0, '')
        self.module.exit = MagicMock(name = 'exit')
        response = self.module.main()
        self.module.get_status.assert_called_once_with(result, 'warning_val', 'critical_val')


if __name__ == '__main__':
    unittest.main()
