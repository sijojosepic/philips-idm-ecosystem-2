import unittest
from mock import MagicMock, patch, Mock
from requests.exceptions import RequestException
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)


class CheckLogSqlBackupTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_re = MagicMock(name='re')
        self.mock_sys = MagicMock(name='sys')
        self.mock_dateutil = MagicMock(name='dateutil')
        self.mock_functools = MagicMock(name='functools')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            're': self.mock_re,
            'sys': self.mock_sys,
            'dateutil': self.mock_dateutil,
            'functools': self.mock_functools,
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import check_log_sqlbackup_status
        self.module = check_log_sqlbackup_status
        self.DBLogParser = check_log_sqlbackup_status.DBLogParser
        log = '''
                 10/09/2019 15:35:46 AM BACKUP failed to complete the command BACKUP DATABASE Advntures2012. Check the backup application log for detailed messages.
                 10/09/2019 15:19:24 AM BACKUP DATABASE successfully processed 24195 pages in 0.823 seconds (229.671 MB/sec).
                 '''
        self.dblog_parser = self.DBLogParser(log)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1'))

    def test_process_log(self):
        self.dblog_parser.log = 'test\ntest'
        self.dblog_parser.check_failures = MagicMock(name='check_failures', return_value=False)
        self.dblog_parser.check_success = MagicMock(name='check_success', return_value=True)

        self.dblog_parser.process_log()
        self.dblog_parser.check_success.assert_called_with('test')
        self.dblog_parser.check_failures.assert_called_with('test')

    def test_compose_status(self):
        job_status = {'db_list': {'backup': {'FAILED': '10/09/2019', 'SUCCESS': '10/12/2019'}}}
        self.dblog_parser.job_status = job_status
        self.dblog_parser._job_status = MagicMock(name='_job_status')
        self.dblog_parser._job_status.return_value = 'WARNING'
        data = {'CRITICAL': [], 'OK': [], 'WARNING': [('db_list', 'backup', '10/09/2019', '10/12/2019')]}
        result = self.dblog_parser.compose_status()

    def test_update(self):
        job_status = {}
        self.dblog_parser.job_status = job_status
        self.dblog_parser.update('Xcelera', 'FULL', 'FAILED', '11/6/2019 3:28:14 PM')
        job_status = {'Xcelera': {'FULL': {'FAILED': '11/6/2019 3:56:09 PM'},
                                  'TRANSACTIONAL_LOG': {'SUCCESS': '11/6/2019 4:30:01 PM'}},
                      'model': {'TRANSACTIONAL_LOG': {'SUCCESS': '11/6/2019 4:30:01 PM'}},
                      'Echo': {'DIFFERENTIAL': {'FAILED': '11/6/2019 5:13:32 PM'},
                               'FULL': {'FAILED': '11/6/2019 5:09:30 PM'},
                               'TRANSACTIONAL_LOG': {'FAILED': '11/6/2019 5:13:37 PM',
                                                     'SUCCESS': '11/6/2019 4:30:00 PM'}}}
        self.dblog_parser.job_status = job_status
        self.dblog_parser.update('Xcelera', 'FULL', 'FAILED', '11/6/2019 3:28:14 PM')
        job_status = {'Xcelera': {'TRANSACTIONAL_LOG': {'FAILED': '11/6/2019 3:28:14 PM'}},
                      'model': {'TRANSACTIONAL_LOG': {'SUCCESS': '11/6/2019 3:30:01 PM'}},
                      'Echo': {'TRANSACTIONAL_LOG': {'SUCCESS': '11/6/2019 3:30:01 PM'}}}
        self.dblog_parser.job_status = job_status
        self.dblog_parser.update('Xcelera', 'FULL', 'FAILED', '11/6/2019 3:56:09 PM')

    def test_status(self):
        status = {'CRITICAL': [], 'WARNING': [], 'OK': []}
        self.dblog_parser.process_log = MagicMock(name='process_log', return_value=True)
        self.dblog_parser.compose_status = MagicMock(name='compose_status', return_value=status)
        result = self.dblog_parser.status()
        self.assertEqual(result, {'CRITICAL': [], 'WARNING': [], 'OK': []})

    def test_get_logdate_and_db(self):
        mock_match = MagicMock(name='match')
        mock_match.group.side_effect = AttributeError()
        mock_date_pattern = MagicMock(name='match')
        mock_date_pattern.match.return_value = mock_match

        self.dblog_parser.LOG_DATE_PATTERN = mock_date_pattern
        self.dblog_parser.LOG_FAILURE_PATTERN = mock_date_pattern
        line = '17/09/2019 15:28:44 AM BACKUP failed to complete the command BACKUP LOG adventure. Check the backup application log for detailed messages.'
        self.mock_re.return_value.side_effect = IndexError
        data = self.module.get_logdate_and_db(self.dblog_parser.LOG_DATE_PATTERN, self.dblog_parser.LOG_FAILURE_PATTERN,
                                              line)
        self.assertEqual(data, (None, None))

    def test_job_status(self):
        self.dblog_parser._dc = MagicMock(name='dc', side_effect=['17/09/2019', '20/09/2019'])
        result = self.dblog_parser._job_status('17/09/2019', '20/09/2019')
        self.assertEqual(result, 'WARNING')

    def test_job_status_case_else(self):
        self.dblog_parser._dc = MagicMock(name='dc', side_effect=['20/09/2019', '17/09/2019'])
        result = self.dblog_parser._job_status('20/09/2019', '17/09/2019')
        self.assertEqual(result, 'CRITICAL')
        self.dblog_parser._dc.assert_called_with('17/09/2019')

    def test_add_messge(self):
        items = [('Xcelera', 'FULL', '11/6/2019 3:56:09 PM', '11/6/2019 5:15:22 PM'),
                 ('Echo', 'DIFFERENTIAL', '11/6/2019 5:13:32 PM', '11/7/2019 12:15:03 AM'),
                 ('Echo', 'TRANSACTIONAL_LOG', '11/6/2019 5:13:37 PM', '11/7/2019 11:30:00 AM')]
        prefix = 'WARNING :'
        wrn_template = "Database {0} BackupType {1} failed On {2} successful On {3}.\n"
        data = self.module.add_messge(wrn_template, prefix, items)
        result = 'WARNING :Database Xcelera BackupType FULL failed On 11/6/2019 3:56:09 PM successful On 11/6/2019 5:15:22 PM.\nDatabase Echo BackupType DIFFERENTIAL failed On 11/6/2019 5:13:32 PM successful On 11/7/2019 12:15:03 AM.\nDatabase Echo BackupType TRANSACTIONAL_LOG failed On 11/6/2019 5:13:37 PM successful On 11/7/2019 11:30:00 AM.\n'
        self.assertEqual(data, result)

    def test_format_msg_case_1(self):
        status = {'CRITICAL': [('Echo', 'FULL', '11/6/2019 5:09:30 PM', '')],
                  'OK': [('Xcelera', 'DIFFERENTIAL', '', '11/7/2019 12:15:09 AM'),
                         ('Xcelera', 'TRANSACTIONAL_LOG', '', '11/7/2019 11:30:01 AM'),
                         ('model', 'DIFFERENTIAL', '', '11/7/2019 12:15:04 AM'),
                         ('model', 'TRANSACTIONAL_LOG', '', '11/7/2019 11:30:00 AM'),
                         ('master', 'FULL', '', '11/6/2019 5:17:50 PM'),
                         ('msdb', 'DIFFERENTIAL', '', '11/7/2019 12:15:07 AM')],
                  'WARNING': [('Xcelera', 'FULL', '11/6/2019 3:56:09 PM', '11/6/2019 5:15:22 PM'),
                              ('Echo', 'DIFFERENTIAL', '11/6/2019 5:13:32 PM', '11/7/2019 12:15:03 AM'),
                              ('Echo', 'TRANSACTIONAL_LOG', '11/6/2019 5:13:37 PM', '11/7/2019 11:30:00 AM')]}
        self.module.add_message = MagicMock(name='add_msg')
        final_msg = '''FAILED : Database Echo BackupType FULL failed On 11/6/2019 5:09:30 PM.
WARNING : Database Xcelera BackupType FULL failed On 11/6/2019 3:56:09 PM successful On 11/6/2019 5:15:22 PM.
Database Echo BackupType DIFFERENTIAL failed On 11/6/2019 5:13:32 PM successful On 11/7/2019 12:15:03 AM.
Database Echo BackupType TRANSACTIONAL_LOG failed On 11/6/2019 5:13:37 PM successful On 11/7/2019 11:30:00 AM.
SUCCESSFUL : Database Xcelera BackupType DIFFERENTIAL is successful.
Database Xcelera BackupType TRANSACTIONAL_LOG is successful.
Database model BackupType DIFFERENTIAL is successful.
Database model BackupType TRANSACTIONAL_LOG is successful.
Database master BackupType FULL is successful.
Database msdb BackupType DIFFERENTIAL is successful.'''
        self.module.add_message.return_value = final_msg
        result = (2,
                  'FAILED : Database Echo BackupType FULL failed On 11/6/2019 5:09:30 PM.\nWARNING : Database Xcelera BackupType FULL failed On 11/6/2019 3:56:09 PM successful On 11/6/2019 5:15:22 PM.\nDatabase Echo BackupType DIFFERENTIAL failed On 11/6/2019 5:13:32 PM successful On 11/7/2019 12:15:03 AM.\nDatabase Echo BackupType TRANSACTIONAL_LOG failed On 11/6/2019 5:13:37 PM successful On 11/7/2019 11:30:00 AM.\nSUCCESSFUL : Database Xcelera BackupType DIFFERENTIAL is successful. \nDatabase Xcelera BackupType TRANSACTIONAL_LOG is successful. \nDatabase model BackupType DIFFERENTIAL is successful. \nDatabase model BackupType TRANSACTIONAL_LOG is successful. \nDatabase master BackupType FULL is successful. \nDatabase msdb BackupType DIFFERENTIAL is successful. \n')
        data = self.module.format_msg(status)
        self.assertEqual(data, result)

    def test_format_msg_case_2(self):
        status = {'CRITICAL': [], 'OK': [], 'WARNING': []}
        self.module.add_message = MagicMock(name='add_msg')
        final_msg = ''' '''
        self.module.add_message.return_value = final_msg
        result = (0, 'No backup Data Found')
        data = self.module.format_msg(status)
        self.assertEqual(data, result)

    def test_check_failures(self):
        line = '17/09/2019 15:28:44 AM BACKUP failed to complete the command BACKUP LOG adventure. Check the backup application log for detailed messages.'
        self.dblog_parser.LOG, self.dblog_parser.DIFFERENTIAL, self.dblog_parser.MAIN = "LOG", "DIFFERENTIAL", "FULL"
        self.dblog_parser._retrieve = MagicMock(name='check_retrieve')
        self.dblog_parser._retrieve.return_value = 'master', '17/09/2019 15:28:44 AM'
        data = self.dblog_parser.check_failures(line)
        self.assertEqual(data, True)
        line = '10/09/2019 15:17:40 AM BACKUP failed to complete the command BACKUP DATABASE PubsDB WITH DIFFERENTIAL. Check the backup application log for detailed messages'
        self.dblog_parser.LOG, self.dblog_parser.DIFFERENTIAL, self.dblog_parser.MAIN = "LOG", "DIFFERENTIAL", "FULL"
        self.dblog_parser._retrieve = MagicMock(name='check_retrieve')
        self.dblog_parser._retrieve.return_value = 'PubsDB', '10/09/2019 15:17:40 AM'
        data = self.dblog_parser.check_failures(line)
        self.assertEqual(data, True)

    def test_check_success_case_1(self):
        line = '''10/09/2019 15:19:24 AM Database backed up. Database: xcelera<c/> creation date(time): 2019/10/09(13:02:54)<c/> pages dumped: 24203<c/> first LSN: 42:248:37<c/> last LSN: 42:288:1<c/> number of dump devices: 1<c/> device information: (FILE=1<c/> TYPE=DISK: {'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\Advntures2012.bak'}). This is an informational message only. No user action is required.'''
        self.dblog_parser.LOG, self.dblog_parser.DIFFERENTIAL, self.dblog_parser.MAIN = "LOG", "DIFFERENTIAL", "FULL"
        self.dblog_parser._retrieve = MagicMock(name='check_retrieve')
        self.dblog_parser._retrieve.return_value = 'xcelera', '10/09/2019 15:19:24 AM'
        self.dblog_parser.update = MagicMock(name='update')
        self.dblog_parser.update.return_value = True
        data = self.dblog_parser.check_success(line)
        line = '''10/09/2019 15:21:18 AM Database differential changes were backed up. Database: PubsDB<c/> creation date(time): 2019/10/09(13:01:13)<c/> pages dumped: 138<c/> first LSN: 42:312:34<c/> last LSN: 42:344:1<c/> full backup LSN: 42:248:37<c/> number of dump devices: 1<c/> device information: (FILE=2<c/> TYPE=DISK: {'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\PubsDB.bak'}). This is an informational message. No user action is required.'''
        self.dblog_parser._retrieve = MagicMock(name='check_retrieve')
        self.dblog_parser._retrieve.return_value = 'PubsDB', '10/09/2019 15:17:40 AM'
        data = self.dblog_parser.check_success(line)
        line = '''10/09/2019 16:02:29 AM Log was backed up. Database: model<c/> creation date(time): 2003/04/08(09:13:36)<c/> first LSN: 31:432:37<c/> last LSN: 32:32:1<c/> number of dump devices: 1<c/> device information: (FILE=3<c/> TYPE=DISK: {'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\model.bak'}). This is an informational message only. No user action is required.'''
        self.dblog_parser._retrieve = MagicMock(name='check_retrieve')
        self.dblog_parser._retrieve.return_value = 'model', '10/09/2019 16:02:29 AM'
        data = self.dblog_parser.check_success(line)

    def test_main(self):
        self.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        line = '''data'''
        mock_output = MagicMock(name="output")
        mock_output.status_code = 0
        mock_output.std_out = line
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        self.dblog_parser.status = MagicMock(name='status', return_value=True)
        data = self.module.format_msg = MagicMock(name='status', return_value=True)
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        self.module.main()

    def test_main_else(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        error = 'Error'
        mock_output = MagicMock(name="output")
        mock_output.status_code = 1
        mock_output.std_err = error
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        self.dblog_parser.status = MagicMock(name='status', return_value=True)
        data = self.module.format_msg = MagicMock(name='status', return_value=True)
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WINRM
        data = self.module.main()
        self.assertEqual(data, None)

    def test_main_AuthenticationError(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError()
        data = self.module.main()
        self.assertEqual(data, None)

    def test_main_RequestException(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = RequestException()
        data = self.module.main()
        self.assertEqual(data, None)

    def test_main_WinRMOperationTimeoutError(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError()
        data = self.module.main()
        self.assertEqual(data, None)

    def test_main_TypeError(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError()
        data = self.module.main()
        self.assertEqual(data, None)

    def test_main_TypeError_with_value(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('"takes exactly 2"')
        data = self.module.main()
        self.assertEqual(data, None)

    def test_main_Exception(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('host1', 'user', 'pass')
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception()
        data = self.module.main()
        self.assertEqual(data, None)


if __name__ == '__main__':
    unittest.main()
