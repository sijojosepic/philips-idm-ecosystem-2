import unittest
import mock
from mock import MagicMock, patch, call


class CheckZertoAlertsTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_json = MagicMock(name='json')
        self.mock_args = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name="scanline")
        modules = {
            'requests': self.mock_request,
            'json': self.mock_json,
            'argparse': self.mock_args,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.product': self.mock_scanline.product,
            'scanline.product.disaster_recovery': self.mock_scanline.product.disaster_recovery,
            'scanline.product.disaster_recovery.zerto_wrapper': self.mock_scanline.product.disaster_recovery.zerto_wrapper,
            'scanline.utilities.cache': self.mock_scanline.utilities.cache,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_zerto_status
        self.module = check_zerto_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        results = MagicMock(name='mock_result')
        results.hostaddress = '1.1.1.1'
        results.username = 'user'
        results.password = 'pass'
        results.port = 1234
        results.url = ''
        results.category = 'category'
        results.subcategory = 'sub-category'
        results.enable_cache = 'yes'
        results.cache_timeout = 240
        results.previous_state = 'OK'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = results
        self.module.argparse.ArgumentParser = self.mock_args
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('1.1.1.1', 'user', 'pass', 1234, '', 'category', 'sub-category', 'yes', 240, 'OK'))

    @mock.patch('scanline.utilities.cache.SafeCache.do')
    def test_write_cache(self, mock_safecache_do):
        mock_safecache_do.return_value = None
        self.assertEqual(self.module.write_cache('key', 'json', 540), None)

    @mock.patch('scanline.utilities.cache.SafeCache.do')
    def test_read_cache(self, mock_safecache_do):
        mock_safecache_do.return_value = 'json'
        self.assertEqual(self.module.read_cache('key'), 'json')

    def test_get_zerto_obj(self):
        mock_obj = MagicMock(name='idmzerto')
        mock_obj.zconn = 'object'
        self.mock_scanline.product.disaster_recovery.zerto_wrapper.IDMZerto.return_value = mock_obj
        output = self.module.get_zerto_obj('1.1.1.1', 'user', 'pass', 'url', 1234)
        self.assertEqual(output, mock_obj)
        self.mock_scanline.product.disaster_recovery.zerto_wrapper.IDMZerto.assert_called_once_with('1.1.1.1', 'user',
                                                                                                    'pass', 'url', 1234)

    def test_do_request(self):
        mock_obj = MagicMock(name='idmzerto')
        self.mock_json.json.return_value = [{}]
        mock_obj.get_request.return_value = self.mock_json
        self.module.get_zerto_obj = MagicMock(name='get_zerto_obj', return_value=mock_obj)
        output = self.module.do_request('1.1.1.1', 'user', 'pass', 'url', 123, 'abc')
        self.module.get_zerto_obj.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(output, [{}])

    def test_get_zerto_alerts_else(self):
        self.module.do_request = MagicMock(name='do_request', return_value=[])
        output = self.module.get_zerto_alerts('1.2.2.1', 'user', 'pass', 'url', 1234, 'no', 123, 'OK')
        self.module.do_request.assert_called_once_with('1.2.2.1', 'user', 'pass', 'url', 1234, 'v1/alerts')
        self.assertEqual(output, [])

    def test_get_zerto_alerts_if(self):
        self.module.write_cache = MagicMock(name='write_cache')
        self.module.read_cache = MagicMock(name='read_cache', return_value='[{},{}]')
        self.module.do_request = MagicMock(name='do_request', return_value=[])
        output = self.module.get_zerto_alerts('1.2.2.1', 'user', 'pass', 'url', 1234, 'yes', 123, 'CRITICAL')
        self.module.read_cache.assert_called_once_with('1.2.2.1_zerto_alerts')
        self.module.do_request.assert_called_once_with('1.2.2.1', 'user', 'pass', 'url', 1234, 'v1/alerts')
        self.module.write_cache.assert_called_once_with('1.2.2.1_zerto_alerts', [], 123)
        self.assertEqual(output, [])

    def test_get_excluded_alertids(self):
        output = self.module.get_excluded_alertids()
        self.assertEqual(output, ['VRA0001', 'VRA0003', 'VRA0049', 'VRA0050', 'VRA0020', 'VRA0023', 'VRA0024',
                                  'VRA0028', 'VRA0029', 'VRA0032', 'VRA0033', 'VRA0055', 'VRA0056', 'VRA0025',
                                  'VRA0002', 'VRA0004', 'VRA0005', 'VRA0037', 'VRA0038', 'LIC0001', 'LIC0002',
                                  'LIC0003', 'LIC0004', 'LIC0006', 'LIC0007', 'LIC0009', 'LIC0010', 'LIC0011',
                                  'VRA0030', 'VRA0039', 'VRA0040', 'VRA0054', 'VRA0007', 'VRA0006', 'VRA0008',
                                  'VRA0009', 'VRA0010', 'VRA0011', 'VRA0012', 'VRA0013', 'VRA0014', 'VRA0015',
                                  'VRA0016', 'VRA0017', 'VRA0018', 'VRA0019', 'VRA0021', 'VRA0022', 'VRA0026',
                                  'VRA0027', 'VRA0052', 'VRA0053', 'VPG0009', 'VPG0010'])

    def test_check_alert_status_critical(self):
        msg = 'CRITICAL:  VRA0001: Host 1 \n VRA0001: Host 2 \n'
        self.module.get_zerto_alerts = MagicMock(name='get_zerto_alerts',
                                                 return_value=[{'HelpIdentifier': 'VRA0001', 'Description': 'Host 1'},
                                                               {'HelpIdentifier': 'VRA0001', 'Description': 'Host 2'}])

        state, output_msg = self.module.check_alert_status('1.1.1.1', 'user', 'pass', 123, 'url', 'HOST', 'yes', 1, 'OK')
        self.module.get_zerto_alerts.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'yes', 1, 'OK')
        self.assertEqual(state, 2)
        self.assertEqual(output_msg, msg)

    def test_check_alert_status_all(self):
        msg = 'CRITICAL:  VPG0003: Host 1 \n'
        self.module.get_zerto_alerts = MagicMock(name='get_zerto_alerts',
                                                 return_value=[{'HelpIdentifier': 'VPG0003', 'Description': 'Host 1'},
                                                               {'HelpIdentifier': 'VRA0001', 'Description': 'Host 2'}])

        state, output_msg = self.module.check_alert_status('1.1.1.1', 'user', 'pass', 123, 'url', 'OTHER', 'yes', 1, 'OK')
        self.module.get_zerto_alerts.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'yes', 1, 'OK')
        self.assertEqual(state, 2)
        self.assertEqual(output_msg, msg)

    def test_check_alert_status_ok(self):
        msg = 'OK: No VRA Host alerts'
        self.module.get_zerto_alerts = MagicMock(name='get_zerto_alerts',
                                                 return_value=[{'HelpIdentifier': 'ZVM0001', 'Description': 'Host 1'},
                                                               {'HelpIdentifier': 'ZVM0001', 'Description': 'Host 2'}])

        state, output_msg = self.module.check_alert_status('1.1.1.1', 'user', 'pass', 123, 'url', 'HOST', 'yes', 1, 'OK')
        self.module.get_zerto_alerts.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'yes', 1, 'OK')
        self.assertEqual(state, 0)
        self.assertEqual(output_msg, msg)

    def test_check_alert_status_rpo(self):
        self.module.get_zerto_alerts = MagicMock(name='get_zerto_alerts',
                                                 return_value=[{'HelpIdentifier': 'ZVM0001', 'Description': 'Host 1'},
                                                               {'HelpIdentifier': 'ZVM0001', 'Description': 'Host 2'}])
        self.module.get_present_rpo = MagicMock(name='get_present_rpo', return_value="OK: RPO")
        state, output_msg = self.module.check_alert_status('1.1.1.1', 'user', 'pass', 123, 'url', 'RPO', 'yes', 1, 'OK')
        self.module.get_zerto_alerts.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'yes', 1, 'OK')
        self.module.get_present_rpo.assert_called_once_with('1.1.1.1', 'user', 'pass', 123, 'url', 'RPO')
        self.assertEqual(state, 0)
        self.assertEqual(output_msg, "OK: RPO")

    def test_check_alert_status_license(self):
        self.module.get_zerto_alerts = MagicMock(name='get_zerto_alerts', return_value=[])
        self.module.get_license_details = MagicMock(name='get_license_details', return_value="OK")
        state, output_msg = self.module.check_alert_status('1.1.1.1', 'user', 'pass', 123, 'url', 'LICENSE', 'yes', 1,
                                                           'OK')
        self.module.get_zerto_alerts.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'yes', 1, 'OK')
        self.module.get_license_details.assert_called_once_with('1.1.1.1', 'user', 'pass', 123, 'url', 'LICENSE')
        self.assertEqual(state, 0)
        self.assertEqual(output_msg, "OK")

    def test_check_zvm_status_protectedsite(self):
        mock_obj = MagicMock(name='idmzerto')
        mock_obj.is_protected = True
        msg = 'OK: ProtectedSite, all dependent service monitoring is enabled'
        self.module.get_zerto_obj = MagicMock(name='get_zerto_obj', return_value=mock_obj)
        state, out_msg = self.module.check_zvm_status('1.1.1.1', 'user', 'pass', 'url', 123)
        self.module.get_zerto_obj.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(state, 0)
        self.assertEqual(out_msg, msg)

    def test_check_zvm_status_recovery_disablemonitoring(self):
        mock_obj = MagicMock(name='idmzerto')
        mock_obj.is_protected = False
        mock_obj.protectedsite_pairing_status.return_value = 0
        msg = 'UNKNOWN: ProtectedSite pairing is successful, dependent service monitoring is disabled for the ' \
              'RecoverySite'
        self.module.get_zerto_obj = MagicMock(name='get_zerto_obj', return_value=mock_obj)
        state, out_msg = self.module.check_zvm_status('1.1.1.1', 'user', 'pass', 'url', 123)
        self.module.get_zerto_obj.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(state, 3)
        self.assertEqual(out_msg, msg)

    def test_check_zvm_status_recovery_enablemonitoring(self):
        mock_obj = MagicMock(name='idmzerto')
        mock_obj.is_protected = False
        mock_obj.protectedsite_pairing_status.return_value = 2
        msg = 'OK: ProtectedSite pairing is unsuccessful - dependent service monitoring is enabled'
        self.module.get_zerto_obj = MagicMock(name='get_zerto_obj', return_value=mock_obj)
        state, out_msg = self.module.check_zvm_status('1.1.1.1', 'user', 'pass', 'url', 123)
        self.module.get_zerto_obj.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(state, 0)
        self.assertEqual(out_msg, msg)

    def test_check_status(self):
        self.module.check_alert_status = MagicMock(name='check_alert_status', return_value=(0, 'OK'))
        state, msg = self.module.check_status('1.1.1.1', 'user', 'pass', 123, 'url', 'alerts', 'ABC', 'yes', 1, 'OK')
        self.module.check_alert_status.assert_called_once_with('1.1.1.1', 'user', 'pass', 123, 'url', 'ABC', 'yes', 1,
                                                               'OK')
        self.assertEqual(state, 0)
        self.assertEqual(msg, 'OK')

    def test_check_status_auth(self):
        self.module.check_zerto_auth = MagicMock(name='check_zerto_auth', return_value=(0, 'OK'))
        state, msg = self.module.check_status('1.1.1.1', 'user', 'pass', 123, 'url', 'auth_status', None, 'yes', 1, 'OK')
        self.module.check_zerto_auth.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(state, 0)
        self.assertEqual(msg, 'OK')

    def test_check_status_zvm(self):
        self.module.check_zvm_status = MagicMock(name='check_zvm_status', return_value=(0, 'OK'))
        state, msg = self.module.check_status('1.1.1.1', 'user', 'pass', 123, 'url', 'zvm_status', None, 'yes', 1, 'OK')
        self.module.check_zvm_status.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(state, 0)
        self.assertEqual(msg, 'OK')

    def test_check_auth_status(self):
        mock_obj = MagicMock(name='idmzerto')
        mock_obj.get_connection.return_value = 'OK'
        self.module.get_zerto_obj = MagicMock(name='get_zerto_obj', return_value=mock_obj)
        state, msg = self.module.check_zerto_auth('1.1.1.1', 'user', 'pass', 'url', 123)
        self.module.get_zerto_obj.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123)
        self.assertEqual(state, 0)
        self.assertEqual(msg, 'OK: Zerto APIs are accessible')

    def test_check_status_critical(self):
        state, msg = self.module.check_status('1.1.1.1', 'user', 'pass', 123, 'url', 'abc', 'ABC', 'yes', 1, 'OK')
        self.assertEqual(state, 2)
        self.assertEqual(msg, '')

    def test_check_status_exception(self):
        self.module.check_alert_status = MagicMock(name='check_alert_status', side_effect=Exception('e'))
        state, msg = self.module.check_status('1.1.1.1', 'user', 'pass', 123, 'url', 'alerts', 'ABC', 'yes', 1, 'OK')
        self.module.check_alert_status.assert_called_once_with('1.1.1.1', 'user', 'pass', 123, 'url', 'ABC', 'yes', 1,
                                                               'OK')
        self.assertEqual(state, 2)
        self.assertEqual(msg, 'CRITICAL : e')

    def test_get_present_rpo(self):
        self.module.do_request = MagicMock(name='do_request', return_value=[{'ActualRPO': 4}, {'ActualRPO': 4}])
        output = self.module.get_present_rpo('1.1.1.1', 'user', 'pass', 123, 'url', 'RPO')
        self.module.do_request.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'v1/vpgs')
        self.assertEqual(output, "OK: Average RPO value is 4 | 'Average RPO '=4;\n")

    def test_get_license_details(self):
        msg = 'OK: License is valid and applicable for Max. 250 Vms. \n Presently total protected VMs are 21'
        self.module.do_request = MagicMock(name='do_request', return_value={"Usage": {"TotalVmsCount": 21, },
                                                                            "Details": {"MaxVms": 250}})
        output = self.module.get_license_details('1.1.1.1', 'user', 'pass', 123, 'url', 'LICENSE')
        self.module.do_request.assert_called_once_with('1.1.1.1', 'user', 'pass', 'url', 123, 'v1/license')
        self.assertEqual(output, msg)

    def test_main(self):
        self.module.check_args = MagicMock(name='check_arg', return_value=('1.1.1.1', 'user', 'pass', 1234, '',
                                                                           'category', 'id', 'yes', 240, 'OK'))
        self.module.check_status = MagicMock(name='check_status', return_value=(0, 'OK'))

        try:
            self.module.main()
            self.module.check_args.assert_called_once_with()
            self.module.check_status.assert_called_once_with('1.1.1.1', 'user', 'pass', 1234, '', 'category',
                                                             'id', 'yes', 240, 'OK')
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
