import unittest
from mock import MagicMock, patch


class TriggerTaskTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_celery = MagicMock(name='celery')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_sys = MagicMock(name='sys')
        modules = {
            'celery': self.mock_celery,
            'argparse': self.mock_argparse,
            'phimutils': self.mock_phimutils,
            'phimutils.argument': self.mock_phimutils.argument,
            'sys': self.mock_sys,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import trigger_task
        self.module = trigger_task

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_trigger_task(self):
        self.module.get_celery = MagicMock(name='get_celery')
        self.assertEqual(
            self.module.trigger_task(
                'host01',
                'service_01',
                'tasks.task01',
                ['arg1', 'arg2'],
                {'kwarg1': 'val1', 'kwarg2': 'val2'},
                {'config1': 'valc1'},
                '/tmp/path'
            ),
            'Task tasks.task01 sent'
        )
        self.module.get_celery.assert_called_once_with({'config1': 'valc1'}, '/tmp/path')
        self.module.get_celery.return_value.send_task.assert_called_once_with(
            'tasks.task01',
            ['arg1', 'arg2'],
            kwargs={'service': 'service_01', 'hostname': 'host01', 'kwarg1': 'val1', 'kwarg2': 'val2'}
        )

    def test_check_arg(self):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.hostname = 'h'
        obj2.service ='s'
        obj2.task ='t'
        obj2.args ='a'
        obj2.kwargs='k'
        obj2.config_object ='c'
        obj2.path ='p'        
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.module.check_arg(),('h','s','t','a','k','c','p'))

    def test_get_celery(self):
        obj = MagicMock()
        self.mock_celery.Celery.return_value = obj
        self.assertEqual(self.module.get_celery('config_object','path'),self.mock_celery.Celery())
        obj.config_from_object.assert_called_once_with('config_object')

    def test_main_with_no_exception(self):
        self.module.trigger_task = MagicMock(return_value = 'msg')
        self.module.main()
        self.mock_sys.exit.assert_called_once_with(0)
        
    def test_main_with_exception(self):
        self.module.trigger_task = MagicMock(side_effect = Exception)
        self.module.main()
        self.mock_sys.exit.assert_called_once_with(3)



if __name__ == '__main__':
    unittest.main()
