import unittest


class CheckClusterinputdirectoryTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        import check_cluster_input_directory
        self.module = check_cluster_input_directory

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_check_cluster_input_directory(self):
        data = [[1, '2'], [2, '0']]
        self.assertEqual(self.module.check_cluster_input_directory('iSite', None, data), (1, 'iSite:'))
        data = [[1, '2'], [2, '2']]
        self.assertEqual(self.module.check_cluster_input_directory('iSite', None, data), (2, 'iSite:'))
        data = [[1, '0'], [2, '2']]
        self.assertEqual(self.module.check_cluster_input_directory('', None, data), (1, ''))


if __name__ == '__main__':
    unittest.main()
