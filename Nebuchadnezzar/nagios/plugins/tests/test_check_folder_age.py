import unittest
from mock import MagicMock, patch


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.datetime = MagicMock(name='datetime')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.wmi': self.mock_scanline.wmi,
            'scanline.utilities.dns': self.mock_scanline.dns,
            'phimutils': self.mock_phimutils,
            'phimutils.timestamp': self.mock_phimutils.timestamp,
            'argparse': self.mock_argparse,
            'sys': self.mock_sys
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_folder_age
        self.check_folder_age = check_folder_age
        from check_folder_age import wmi_folder_age
        self.wmi_folder_age = wmi_folder_age
        from scanline.utilities.wmi import WMIHostDriller
        self.WMIHostDriller = WMIHostDriller(host='127.0.0.1', user='user', password='pass', domain='Domain')
        from phimutils.timestamp import wmi_datetime
        self.wmi_datetime = wmi_datetime

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.delta = 'del'
        obj2.unit ='u'
        obj2.domain ='dom'
        obj2.hostaddress ='h'
        obj2.username='u'
        obj2.password ='p'
        obj2.drive ='dri'
        obj2.folder ='f'        
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.check_folder_age.check_arg(),('del','u','dom','h','u','p','dri','f'))
    

    def test_wmi_folder_age(self):
        self.wmi_datetime.return_value = '1234567'
        self.mock_scanline.dns.get_address = MagicMock('get_address', return_value='127.0.0.1')
        self.WMIHostDriller.query_wmi.return_value = iter([1,2,3])
        self.assertEqual(self.wmi_folder_age(720, 'minutes', 'Domain', '127.0.0.1', 'user', 'pass', 'C:', '/test/'),\
                         (0, 'OK - 3 Backup found compared to timestamp 1234567')
                        )

    # def test_wmi_folder_age_with_exception(self):
    #     self.wmi_datetime.return_value = '1234567'
    #     self.mock_scanline.dns.get_address = MagicMock('get_address', return_value='127.0.0.1')
    #     obj = MagicMock()
    #     obj.query_wmi.side_effect = TypeError
    #     self.mock_scanline.wmi.WMIHostDriller.return_value = obj
    #     self.WMIHostDriller.return_value = obj#s.query_wmi.side_effect = MagicMock(side_effect=TypeError)
    #     # self.mock_argparse.len = MagicMock(side_effect = TypeError)
    #     self.assertRaises(TypeError, self.check_folder_age.wmi_folder_age, 720, 'minutes', 'Domain', '127.0.0.1', 'user', 'pass', 'C:', '/test/')

    def test_wmi_folder_age_okstatus(self):
        self.wmi_datetime.return_value = '1234567'
        self.mock_scanline.dns.get_address = MagicMock('get_address', return_value='127.0.0.1')
        self.WMIHostDriller.query_wmi.return_value = iter([])
        self.assertEqual(self.wmi_folder_age(720, 'minutes', 'Domain', '127.0.0.1', 'user', 'pass', 'C:', '/test/'),\
                         (2, 'CRITICAL - 0 Backup found compared to timestamp 1234567')
                        )

    def test_main(self):
        self.check_folder_age.wmi_folder_age = MagicMock(return_value=('state','val'))
        self.check_folder_age.main()


if __name__ == '__main__':
    unittest.main()
