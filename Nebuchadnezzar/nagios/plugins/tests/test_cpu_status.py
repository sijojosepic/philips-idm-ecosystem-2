import unittest
from mock import MagicMock, patch
from winrm.exceptions import AuthenticationError, WinRMOperationTimeoutError, WinRMError
from requests.exceptions import RequestException

class CPUStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import cpu_status
        self.module = cpu_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_result.warning = 80
        mock_result.critical = 90
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user', 'pass', 80, 90))

    def test_get_status_ok(self):
        status, msg = self.module.get_status(40, 'cpu_fact', 'perf_msg', 80, 90)
        self.assertEqual(msg, 'OK: {0} | {1}'.format('cpu_fact', 'perf_msg'))
        self.assertEqual(status, 0)

    def test_get_status_warn(self):
        status, msg = self.module.get_status(85, 'cpu_fact', 'perf_msg', 80, 90)
        self.assertEqual(msg, 'WARNING: {0} | {1}'.format('cpu_fact', 'perf_msg'))
        self.assertEqual(status, 1)

    def test_get_status_crit(self):
        status, msg = self.module.get_status(95, 'cpu_fact', 'perf_msg', 80, 90)
        self.assertEqual(msg, 'CRITICAL: {0} | {1}'.format('cpu_fact', 'perf_msg'))
        self.assertEqual(status, 2)

    def test_cpu_check_ok(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = """Instance cpu -- -- total 100 idle 70 p1 10 p2 10 p3 10"""
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 0)
        test_msg = "OK: Total CPU usage - 30.0%, Top 3 processes: p1 - 10%, p2 - 10%, p3 - 10% | {0}\n"\
            .format("'Avg CPU Utilisation'=30.0%;80;90;")
        self.assertEqual(msg, test_msg)

    def test_cpu_check_value_error(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = """Instance cpu -- -- total 100 p1 40 p2 40 p3 20"""
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = "CRITICAL: Total CPU usage - 100%, Top 3 processes: p1 - 40%, p2 - 40%, p3 - 20% | {0}\n"\
            .format("'Avg CPU Utilisation'=100%;80;90;")
        self.assertEqual(msg, test_msg)

    def test_cpu_check_warn(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = """Instance cpu -- -- total 100 idle 20 p1 30 p2 30 p3 20"""
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 1)
        test_msg = "WARNING: Total CPU usage - 80.0%, Top 3 processes: p1 - 30%, p2 - 30%, p3 - 20% | {0}\n"\
            .format("'Avg CPU Utilisation'=80.0%;80;90;")
        self.assertEqual(msg, test_msg)

    def test_cpu_check_crit(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = """Instance cpu -- -- total 100 idle 10 p1 30 p2 30 p3 30"""
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = "CRITICAL: Total CPU usage - 90.0%, Top 3 processes: p1 - 30%, p2 - 30%, p3 - 30% | {0}\n"\
            .format("'Avg CPU Utilisation'=90.0%;80;90;")
        self.assertEqual(msg, test_msg)

    def test_cpu_check_req_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = RequestException('RequestException')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 3)
        test_msg = 'UNKNOWN : Authentication Error - {0}'.format('RequestException')
        self.assertEqual(msg, test_msg)

    def test_cpu_check_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Exception e'
        self.assertEqual(msg, test_msg)

    def test_get_job_status_timeout_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError('WinRMOperationTimeoutError')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMOperationTimeoutError')
        self.assertEqual(msg, test_msg)

    def test_get_job_status_authentication_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('AuthenticationError')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 3)
        test_msg = 'UNKNOWN : Authentication Error - AuthenticationError'
        self.assertEqual(msg, test_msg)

    def test_get_job_status_winrm_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('WinRMError')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMError')
        self.assertEqual(msg, test_msg)

    def test_cpu_check_typeerror(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format('localhost')
        self.assertEqual(msg, test_msg)

    def test_cpu_check_type_error1(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('find() takes exactly 2')
        status, msg = self.module.cpu_check('localhost', "user", "pass", 80, 90)
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Issue in connecting to node - {0}'.format('localhost')
        self.assertEqual(msg, test_msg)

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('localhost', 'user', 'pass', 80, 90)
        self.module.wmi_status = MagicMock(name='cpu_check')
        self.module.wmi_status.return_value = (0, 'OK')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
