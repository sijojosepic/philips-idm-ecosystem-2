import unittest
from mock import MagicMock, patch
from StringIO import StringIO

class HelionKeystoneServerStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_heartbeat_status
        self.check_rabbitmq_heartbeat_status = check_rabbitmq_heartbeat_status
        from check_rabbitmq_heartbeat_status import form_header
        from check_rabbitmq_heartbeat_status import get_message
        from check_rabbitmq_heartbeat_status import check_rabbimq_availability
        self.form_header = form_header
        self.get_message = get_message
        self.check_rabbimq_availability = check_rabbimq_availability
        self.sys_mock = MagicMock(name = 'sys')
        self.check_rabbitmq_heartbeat_status.sys = self.sys_mock


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('check_rabbitmq_heartbeat_status.requests.get')
    @patch('sys.stdout', new_callable = StringIO)
    def test_check_rabbimq_availability_ok(self, std_out, mock_get):
        mock_get.return_value.status_code = 200
        headers = {'content-type': 'text/plain'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        self.check_rabbimq_availability(headers, auth, hostname, port)
        self.assertEqual(std_out.getvalue(), '')

    @patch('check_rabbitmq_heartbeat_status.requests.get')
    @patch('sys.stdout', new_callable = StringIO)
    def test_check_rabbimq_availability_404(self, std_out, mock_get):
        mock_get.return_value.status_code = 404
        headers = {'content-type': 'text/plain'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        self.check_rabbimq_availability(headers, auth, hostname, port)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL: Could not connect to RabbitMQ node at rabbitmq.isyntax.net, status code: 404\n')

    @patch('check_rabbitmq_heartbeat_status.requests.get')
    @patch('sys.stdout', new_callable = StringIO)
    def test_check_rabbimq_availability_exception(self, std_out, mock_get):
        headers = {'content-type': 'text/plain'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        mock_get.side_effect = Exception
        self.check_rabbimq_availability(headers, auth, hostname, port)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL: Could not connect to RabbitMQ node at rabbitmq.isyntax.net\n')

    def test_form_header(self):
        username, password = 'guest', 'guest'
        self.assertEqual(self.form_header(username, password),
                         ({'content-type': 'text/plain'}, ('guest', 'guest')))

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    @patch('sys.stdout', new_callable = StringIO)
    def test_get_message_ok(self, std_out, mock_post):
        mock_post.return_value.status_code = 200
        headers = {'content-type': 'text/plain'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        timeout = 3
        self.get_message(headers, auth, hostname, port, queue_name, timeout)
        self.sys_mock.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'OK: RabbitMQ Heartbeat is up.\n')

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    @patch('sys.stdout', new_callable = StringIO)
    def test_get_message_404(self, std_out, mock_post):
        mock_post.return_value.status_code = 404
        headers = {'content-type': 'text/plain'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        timeout = 3
        self.get_message(headers, auth, hostname, port, queue_name, timeout)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL: Could not fetch message from idm_test_q queue, status code: 404\n')

    @patch('check_rabbitmq_heartbeat_status.requests.post')
    @patch('sys.stdout', new_callable = StringIO)
    def test_get_message_exception(self, std_out, mock_post):
        mock_post.return_value.status_code = 404
        headers = {'content-type': 'text/plain'}
        auth = ('guest', 'guest')
        hostname = 'rabbitmq.isyntax.net'
        port = '15671'
        queue_name = 'idm_test_q'
        timeout = 3
        mock_post.side_effect = Exception
        self.get_message(headers, auth, hostname, port, queue_name, timeout)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL: Invalid post(get message) request for idm_test_q queue.\n')


if __name__ == '__main__':
    unittest.main()
