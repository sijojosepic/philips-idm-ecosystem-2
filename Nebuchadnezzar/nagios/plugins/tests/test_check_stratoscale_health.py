'''
Created by Vijender.singh@philips.com on 4/30/2018
'''
import unittest
import json
from argparse import Namespace
from mock import MagicMock, patch, Mock

class TestCheckStratoscaleHealth(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_redis = MagicMock(name='redis')
        modules = {
            'requests': self.mock_requests,
            'redis': self.mock_redis
        }
        self.mock_redis.StrictRedis = MagicMock(name='strictredis')
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_stratoscale_health
        self.check_stratoscale_health = check_stratoscale_health
        from check_stratoscale_health import get_cpu_status, get_token, \
            check_arg, create_token_payload, get_storage_status, \
            get_memory_status
        self.get_cpu_status = get_cpu_status
        self.get_token = get_token
        self.check_arg = check_arg
        self.create_token_payload = create_token_payload
        self.get_storage_status = get_storage_status
        self.get_memory_status = get_memory_status
        self.DOMAINNAME = 'cloud_admin'
        self.USERNAME = 'admin'
        self.PASSWORD = 'admin'
        self.HOSTNAME = '130.147.86.205'
        self.WARNING_THRESHOLD = 80
        self.CRITICAL_THRESHOLD = 90
        self.CRITICAL_STATUS = 2
        self.WARNING_STATUS = 1
        self.OK_STATUS = 0
        self.EXPECTED_CPU_USAGE = 30
        self.EXPECTED_RAM_USAGE = 83
        self.EXPECTED_STORAGE_USAGE = 94
        self.RESOURCE_TYPE = 'CPU'
        self.servicetype = ''

    def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()

    @patch('argparse.ArgumentParser.parse_args')
    def test_check_arg(self, mock_argparse):
        mock_argparse.return_value = Namespace(hostname=self.HOSTNAME, domainname=self.DOMAINNAME,
                                                        username=self.USERNAME, password=self.PASSWORD,
                                                        warningthreshold=self.WARNING_THRESHOLD,
                                                        criticalthreshold=self.CRITICAL_THRESHOLD,
                                                        resourceType=self.RESOURCE_TYPE,servicetype=self.servicetype)
        results = self.check_arg()
        assert results == (
            self.HOSTNAME, self.DOMAINNAME, self.USERNAME, self.PASSWORD, self.WARNING_THRESHOLD,
            self.CRITICAL_THRESHOLD, self.RESOURCE_TYPE, self.servicetype)

    def test_create_token_payload(self):
        expected_body_val = json.dumps({'auth': {'identity': {'methods': ['password'], 'password': {
            'user': {'name': 'admin', 'password': 'admin', 'domain': {'name': 'cloud_admin'}}}},
                                                 'scope': {'domain': {'name': 'cloud_admin'}}}})
        expected_header = {'content-type': 'application/json'}
        result = self.create_token_payload(self.DOMAINNAME, self.USERNAME, self.PASSWORD)
        assert expected_header == result.headers
        assert expected_body_val == result.body


    def test_get_token(self):
        mock_headers = {'x-subject-token': 'SomeBase64String'}
        self.mock_requests.post.return_value = Mock(ok=True, headers=mock_headers, status_code=200)
        token_payload = self.create_token_payload(self.DOMAINNAME, self.USERNAME, self.PASSWORD)
        token = self.get_token(self.HOSTNAME, token_payload)
        self.assertEqual(token, 'SomeBase64String')

    def test_get_token_fail(self):
        mock_headers = {}
        self.mock_requests.post.return_value = Mock(ok=False, headers=mock_headers, status_code=404)
        token_payload = self.create_token_payload(self.DOMAINNAME, self.USERNAME, self.PASSWORD)
        try:
            self.get_token(self.HOSTNAME, token_payload)
        except Exception:
            self.assertRaises(Exception, self.get_token)

    def test_get_cpu_status(self):
        stubbed_token = 'SomeBase64String'
        stubbed_cpu_ram_response = {"memory_used": [["cluster", 233736.931941]],
                                    "memory_used_by_system": [["cluster", 102183.652617]],
                                    "cpu_count": [["cluster", 64]],
                                    "memory_provisioned": [["cluster", 393216]],
                                    "memory_reserved_for_system": [["cluster", 969.417648221]],
                                    "memory_used_by_vms": [["cluster", 136362.814192]],
                                    "memory_available_for_vms": [["cluster", 67376.2566847]],
                                    "memory_reserved_for_vms": [["cluster", 86357.2188649]],
                                    "cpu_used": [["cluster", 64180.4762574]], "cpu_provisioned": [["cluster", 214400]]}
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stubbed_cpu_ram_response
        status, message = self.get_cpu_status(self.HOSTNAME,self.DOMAINNAME, self.USERNAME, self.PASSWORD,
         stubbed_token, self.WARNING_THRESHOLD, self.CRITICAL_THRESHOLD)
        assert status ==  self.OK_STATUS
        expected_message = '{0} usage = {1}%, Status is Ok'.format('CPU', self.EXPECTED_CPU_USAGE)
        self.assertTrue(message.startswith(expected_message))

    def test_get_cpu_status_fail(self):
        stubbed_token = 'SomeBase64String'
        mock_get = Mock(ok=False, status_code=404)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = None
        try:
            status, message = self.get_cpu_status(self.HOSTNAME, stubbed_token, self.WARNING_THRESHOLD, self.CRITICAL_THRESHOLD)
            self.fail("Exception didn't occur")
        except Exception:
            self.assertRaises(Exception, self.get_cpu_status)

    def test_get_storage_status(self):
        stubbed_token = 'SomeBase64String'
        stubbed_storage_response = [
            {"volume_count": 87, "type_id": "00000000-0000-0000-0000-000000000000", "used_capacity_raw_mb": 3157275,
             "volume_total_mb": 4136869, "updated_at": "2018-04-17T14:30:28.015651", "health_info": "null",
             "unusable_capacity_mb": 0, "type": "rack-storage", "deleted_at": "null",
             "id": "8b759cd6-3ede-4224-a51c-73b61ec10be1", "state_info": "Pool is active and can be used",
             "system_capacity_raw_mb": 142368, "total_capacity_raw_mb": 4554752, "total_capacity_mb": 2277376,
             "free_capacity_effective_mb": 627553, "state": "active", "health": "healthy",
             "free_capacity_raw_mb": 1255107, "description": "null", "free_capacity_mb": 126933, "cache_size_mb": 0,
             "system_capacity_mb": 71184, "snapshot_count": 33, "properties": {"replica_count": 2}, "name": "edge2POOL",
             "image_total_mb": 0, "is_external": "false", "used_capacity_mb": 1578637,
             "created_at": "2018-02-26T13:40:09.318908", "snapshot_total_mb": 1016577, "image_count": 0}]

        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stubbed_storage_response
        self.DOMAINNAME = 'cloud_admin'
        self.USERNAME = 'admin'
        self.PASSWORD = 'admin'
        status, message = self.get_storage_status(self.HOSTNAME,self.DOMAINNAME, self.USERNAME, self.PASSWORD,
         stubbed_token, self.WARNING_THRESHOLD, self.CRITICAL_THRESHOLD)
        assert status == self.CRITICAL_STATUS
        expected_message = '{0} usage = {1}%, Status is Critical'.format('STORAGE', self.EXPECTED_STORAGE_USAGE)
        self.assertTrue(message.startswith(expected_message))

    def test_get_storage_status_fail(self):
        stubbed_token = 'SomeBase64String'
        mock_get = Mock(ok=False, status_code=404)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = None
        try:
            status, message = self.get_storage_status(self.HOSTNAME, stubbed_token, self.WARNING_THRESHOLD, self.CRITICAL_THRESHOLD)
            self.fail("Exception didn't occur")
        except Exception:
            self.assertRaises(Exception, self.get_storage_status)

    def test_get_memory_status(self):
        stubbed_token = 'SomeBase64String'
        stubbed_cpu_ram_response = {"memory_used": [["cluster", 233736.931941]],
                                    "memory_used_by_system": [["cluster", 102183.652617]],
                                    "cpu_count": [["cluster", 64]],
                                    "memory_provisioned": [["cluster", 393216]],
                                    "memory_reserved_for_system": [["cluster", 969.417648221]],
                                    "memory_used_by_vms": [["cluster", 136362.814192]],
                                    "memory_available_for_vms": [["cluster", 67376.2566847]],
                                    "memory_reserved_for_vms": [["cluster", 86357.2188649]],
                                    "cpu_used": [["cluster", 64180.4762574]], "cpu_provisioned": [["cluster", 214400]]}
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = stubbed_cpu_ram_response

        status, message = self.get_memory_status(self.HOSTNAME,self.DOMAINNAME, self.USERNAME, self.PASSWORD,
         stubbed_token, self.WARNING_THRESHOLD, self.CRITICAL_THRESHOLD)
        assert status ==  self.WARNING_STATUS
        expected_message = '{0} usage = {1}%, Status is Warning'.format('MEMORY', self.EXPECTED_RAM_USAGE)
        self.assertTrue(message.startswith(expected_message))

    def test_get_memory_status_fail(self):
        stubbed_token = 'SomeBase64String'

        mock_get = Mock(ok=False, status_code=404)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = None
        try:
            status, message = self.get_memory_status(self.HOSTNAME, stubbed_token, self.WARNING_THRESHOLD, self.CRITICAL_THRESHOLD)
            self.fail("Exception didn't occur")
        except Exception:
            self.assertRaises(Exception, self.get_memory_status)

    def test_get_cluster_reachability(self):
        self.check_stratoscale_health.create_token_payload = MagicMock(name='token_payload')
        self.check_stratoscale_health.get_token = MagicMock(name='token')
        status, message = self.check_stratoscale_health.get_cluster_reachability('ip','domain','user','pwd')
        self.assertEqual(status, 0)
        self.assertEqual(message, 'Ok - Token generation passed for ip')

    def test_get_cluster_reachability_exception(self):
        self.check_stratoscale_health.create_token_payload = MagicMock(name='token_payload')
        self.check_stratoscale_health.get_token = MagicMock(name='token')
        self.check_stratoscale_health.get_token.side_effect = Exception('e')
        status, message = self.check_stratoscale_health.get_cluster_reachability('ip','domain','user','pwd')
        self.assertEqual(status, 2)
        self.assertEqual(message, 'Critical - Token generation failed for ip, Please check credentials')

    def test_get_token_from_redis(self):
        self.mock_redis.StrictRedis.return_value.hgetall.return_value = {'token': 'value'}
        self.assertEqual(self.check_stratoscale_health.get_token_from_redis(), 'value')

    def test_get_token_from_redis_notoken(self):
        self.mock_redis.StrictRedis.return_value.hgetall.return_value = None
        self.assertEqual(self.check_stratoscale_health.get_token_from_redis(), False)
    
    def test_insert_token_to_redis(self):
        self.mock_redis.StrictRedis.return_value.hmset = MagicMock(name="hmset")
        self.check_stratoscale_health.insert_token_to_redis('token')
        self.mock_redis.StrictRedis.return_value.hmset.assert_called_once_with('hsopClusterToken', {'token': 'token'})

if __name__ == '__main__':
    unittest.main()
