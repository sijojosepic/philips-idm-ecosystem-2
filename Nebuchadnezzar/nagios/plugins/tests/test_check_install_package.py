import unittest
import mock
from mock import MagicMock, patch, Mock
from StringIO import StringIO


class CheckInstallPackageTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_os = MagicMock(name='os')
        self.mock_glob = MagicMock(name='glob')
        modules = {
            'requests': self.mock_request,
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.installing_package_status': self.mock_scanline.utilities.installing_package_status,
            'argparse': self.mock_argparse,
            'os': self.mock_os,
            'glob': self.mock_glob,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_install_package
        self.module = check_install_package
        self.sys_mock = MagicMock(name = 'sys')
        self.module.sys = self.sys_mock
        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.hostaddress = '0.0.0.0'
        mock_result.username = 'pass1'
        mock_result.password = 'db1'
        mock_result.download_script = 'test.ps1'
        mock_result.install_script = 'test.ps1'
        mock_result.repo_path = 'https:/repo_path/'
        mock_result.download_path = 'https:/download_path/'
        mock_result.target_service_name = 'PCMService'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.mock_argparse.ArgumentParser = MagicMock(name='argparse')
        self.mock_argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('localhost', '0.0.0.0', 'pass1', 'db1', 'test.ps1', 'test.ps1',
                                    'https:/repo_path/', 'https:/download_path/', 'PCMService'))

    @mock.patch("__builtin__.open", create=True)
    def test_read_files(self, mock_open):
    	download_content = 'download_script'
    	install_content = 'install_script'
    	mock_open.side_effect = [
            mock.mock_open(read_data="Data1").return_value,
            mock.mock_open(read_data="Data2").return_value
        ]
        self.assertEqual(self.module.read_files('download_script', 'install_script'),('Data1', 'Data2'))

    
    def test_upload_ps_scripts(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, 'download_content', 'install_content', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,3)

    def test_upload_ps_scripts_no_installcontent(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, 'download_content', '', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,2)

    def test_upload_ps_scripts_no_downloadcontent(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, '', 'install_content', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,2)

    def test_upload_ps_scripts_no_content(self): 
        mock_conn = MagicMock(name = 'conn')
    	self.module.upload_ps_scripts(mock_conn, '', '', 'download_path',
                      '/usr/lib/download_script', '/usr/lib/install_script')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_download_package(self):
    	mock_conn = MagicMock(name = 'conn')
    	self.module.download_package(mock_conn, 'http://132.32.33.2/pcm/pcm.1.1.msi', 's:\pcm',
                      '/usr/lib/download_script')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_download_package_ex(self):
        mock_conn = MagicMock(name = 'conn')
        mock_conn.run_ps.side_effect = TypeError()
        self.module.download_package(mock_conn, 'http://132.32.33.2/pcm/pcm.1.1.msi', 's:\pcm',
                      '/usr/lib/download_script')
        self.assertEqual(mock_conn.run_ps.call_count,1)

    @patch('sys.stdout', new_callable = StringIO)
    def test_download_package_exception(self, std_out):
    	self.module.download_package('conn', 'http://132.32.33.2/pcm/pcm.1.1.msi', 's:\pcm',
                      '/usr/lib/download_script')
    	self.assertEqual(std_out.getvalue(), 'CRITICAL: Could not download package.\n')
    	self.sys_mock.exit.assert_called_once_with(2)


    def test_install_package(self):
    	mock_conn = MagicMock(name = 'conn')
    	self.module.install_package(mock_conn, 'username', 'password', 'domain', 'hostaddress', 'http://132.32.33.2/pcm/pcm.1.1.msi',
                's:\pcm', '/usr/lib/install_script', 'pcmlistenerservice')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_install_package_ex(self):
        mock_conn = MagicMock(name = 'conn')
        mock_conn.run_ps.side_effect = Exception()
        self.module.install_package(mock_conn, 'username', 'password', 'domain', 'hostaddress', 'http://132.32.33.2/pcm/pcm.1.1.msi',
                's:\pcm', '/usr/lib/install_script', 'pcmlistenerservice')
        self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_delete_partial_files(self):
    	mock_conn = MagicMock(name = 'conn')
    	self.module.delete_partial_files(mock_conn,'s:\pcm')
    	self.assertEqual(mock_conn.run_ps.call_count,1)

    def test_get_neb_ip_address(self):
        mock_out = MagicMock(name='output')
        mock_out.read.return_value = '\n 0.0.0.0 \n\r'
        self.mock_os.popen.return_value = mock_out
        self.assertEqual(self.module.get_neb_ip_address(), '0.0.0.0')

    def test_get_pcm_package(self):
        self.mock_glob.glob.return_value = ['/var/philips/test.msi']
        self.module.max = MagicMock(name='max')
        self.module.max.return_value = '/var/philips/test.msi'
        self.mock_os.path.basename.return_value = 'test.msi'

        self.assertEqual(self.module.get_pcm_package(), 'test.msi')

    def test_get_pcm_package_ex(self):
        self.mock_glob.glob.side_effect = Exception()
        self.assertEqual(self.module.get_pcm_package(), None)

    def test_main_case_one(self):
        self.module.check_args = MagicMock(name='check_args')
        self.module.check_args.return_value = ('localhost', '0.0.0.0', 'pass1\\\\12', 'db1', 'test.ps1', 'test.ps1',
                                    'https:/repo_path/', 'https:/download_path/', 'PCMService')

        mock_conn = MagicMock(name='conn')
        self.mock_scanline.utilities.installing_package_status.connect.return_value = mock_conn
        self.mock_scanline.utilities.installing_package_status.check_package.return_value = (0, 'Found')
        self.module.get_pcm_package = MagicMock(name='get_pcm_package', return_value=False)
        self.module.sys.exit.return_value = True
        self.module.get_neb_ip_address = MagicMock(name='get_neb_ip_address')

        self.module.main()
        self.module.get_pcm_package.assert_called_once_with()
        self.module.sys.exit.assert_called_with(0)
        self.module.get_neb_ip_address.assert_not_called()

    def test_main_case_two(self):
        check_args_val = ('localhost', '0.0.0.0', 'pass1\\\\username', 'db1', 'test.ps1', 'test.ps1',
                                    'https:/{ip_address}/repo_path/{pcm_package}', 'https:/download_path/', 'PCMService')
        self.module.check_args = MagicMock(name='check_args', return_value=check_args_val)

        mock_conn = MagicMock(name='conn')
        self.mock_scanline.utilities.installing_package_status.connect.return_value = mock_conn
        self.mock_scanline.utilities.installing_package_status.check_package.return_value = (2, 'Not Found')
        self.module.get_pcm_package = MagicMock(name='get_pcm_package', return_value='PCM-5.0')
        self.module.sys.exit.return_value = True
        self.module.get_neb_ip_address = MagicMock(name='get_neb_ip_address', return_value='0.0.0.0')
        self.module.delete_partial_files = MagicMock(name='delete_partial_files', return_value=True)
        self.module.read_files = MagicMock(name='read_files', return_value = ('download_content', 'install_content'))
        self.module.upload_ps_scripts = MagicMock(name='upload_ps_scripts', return_value=True)
        self.module.download_package = MagicMock(name='download_package', return_value=True)
        self.module.install_package = MagicMock(name='install_package', return_value=True)


        self.module.main()
        self.module.get_pcm_package.assert_called_once_with()
        self.module.sys.exit.assert_called_once_with(0)
        self.module.get_neb_ip_address.assert_called_once_with()
        self.module.delete_partial_files.assert_called_once_with(mock_conn, 'https:/download_path/')
        self.module.read_files.assert_called_once_with('test.ps1', 'test.ps1')
        self.module.upload_ps_scripts.assert_called_once_with(mock_conn,'download_content', 'install_content',
                                                              'https:/download_path/', 'test.ps1', 'test.ps1')
        self.module.download_package.assert_called_once_with(mock_conn, 'https:/0.0.0.0/repo_path/PCM-5.0',
                                                             'https:/download_path/', 'test.ps1')
        self.module.install_package.assert_called_once_with(mock_conn, 'username', 'db1', 'pass1', '0.0.0.0',
                                                            'https:/0.0.0.0/repo_path/PCM-5.0', 'https:/download_path/',
                                                            'test.ps1', 'PCMService')


if __name__ == '__main__':
    unittest.main()
