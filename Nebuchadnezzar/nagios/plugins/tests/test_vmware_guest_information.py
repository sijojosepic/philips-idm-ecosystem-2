import sys
import unittest
from mock import MagicMock, patch

sys.modules['pyVim.connect'] = MagicMock(name='pyVim.connect')
sys.modules['pyVim'] = MagicMock(name='pyVim')
sys.modules['phimutils.perfdata'] = MagicMock(name='phimutils.perfdata')
sys.modules['phimutils.perfdata'].INFO_MARKER = '++I#'
sys.modules['phimutils'] = MagicMock(name='phimutils')
sys.modules['requests.exceptions'] = MagicMock(name='requests.exceptions')
sys.modules['requests'] = MagicMock(name='requests')

import vmware_guest_information


class VMwareGuestInformationTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)

    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_get_server(self):
        self.assertEqual(vmware_guest_information.get_server('123,456'), '123')

    @patch('vmware_guest_information.linecache')
    def test_get_server_file(self, mock_linecache):
        mock_linecache.getline.return_value = 'abc'
        self.assertEqual(vmware_guest_information.get_server('@123'), 'abc')

    def test_gen_uuid(self):
        self.assertEqual(vmware_guest_information.gen_uuid('21F21242-8B6B-873C-4A6A-A3A451F6E1CC'),
                         '21f21242-8b6b-873c-4a6a-a3a451f6e1cc')

    def test_gen_uuid_windows(self):
        self.assertEqual(vmware_guest_information.gen_uuid('21F21242-8B6B-873C-4A6A-A3A451F6E1CC', windows=True),
                         '4212f221-6b8b-3c87-4a6a-a3a451f6e1cc')

    def test_gen_uuid_invalid(self):
        self.assertEqual(vmware_guest_information.gen_uuid('nope'), None)

    def test_get_information(self):
        vm = MagicMock(name='vm')
        vm.name = 'vmobj_name1'
        vm.runtime.host.parent.name = 'cluster_obj1'
        result = vmware_guest_information.get_information(vm)
        self.assertEqual(result, '''++I# Object_Name="vmobj_name1"; Cluster_Name="cluster_obj1"; Disks='[]' ''')

    def test_get_information_no_cluster(self):
        vm = MagicMock(name='vm', spec=['name', 'layout'])
        vm.name = 'vmobj_name1'
        result = vmware_guest_information.get_information(vm)
        self.assertEqual(result, '''++I# Object_Name="vmobj_name1"; Cluster_Name=""; Disks='[]' ''')

    def test_get_information_disks(self):
        vm = MagicMock(name='vm', spec=['name', 'layout'])
        vm.name = 'vmobj_name1'
        disk1 = MagicMock(name='disk1')
        disk1.diskFile = ['[LocalDS237] test/test_1.vmdk', '[LocalDS237] test/test_1-000001.vmdk']
        disk2 = MagicMock(name='disk2')
        disk2.diskFile = ['[LocalDS237] test/test_2.vmdk', '[LocalDS237] test/test_2-000001.vmdk']
        vm.layout.disk = [disk1, disk2]
        result = vmware_guest_information.get_information(vm)
        self.assertEqual(result, '''++I# Object_Name="vmobj_name1"; Cluster_Name=""; Disks='["[LocalDS237] test/test_1.vmdk", "[LocalDS237] test/test_2.vmdk"]' ''')

if __name__ == '__main__':
    unittest.main()
