import unittest
from mock import MagicMock, patch, call
import mock
from winrm.exceptions import AuthenticationError, WinRMOperationTimeoutError, WinRMError
from requests.exceptions import RequestException

class CheckISPTimeSyncTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_socket = MagicMock(name='socket')
        self.mock_uuid = MagicMock(name = 'uuid')
        modules = {
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'socket': self.mock_socket,
            'uuid' : self.mock_uuid,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'scanline.utilities.utils': self.mock_scanline.utilities.utils,
            'scanline.utilities.cache': self.mock_scanline.utilities.cache,
            'scanline.utilities.config_reader': self.mock_scanline.utilities.config_reader,
            'scanline.component': self.mock_scanline.component,
            'scanline.component.localhost': self.mock_scanline.component.localhost
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_isp_timesync
        self.module = check_isp_timesync

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostaddress = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_result.hostname = 'localhost'
        mock_result.ntp_servers = ['something']
        mock_result.node_type = 'Node'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user', 'pass', 'localhost', ['something'], 'Node'))

    def test_generate_uuid(self):
        self.mock_uuid.uuid1.return_value = 1
        self.assertEqual(self.module.generate_uuid(), '1')


    def test_get_neb_ip(self):
        mock_localhost_obj = MagicMock(name='LocalHost')
        mock_localhost_obj.__setattr__('ip_address', '1.2.3.4')
        self.mock_scanline.component.localhost.LocalHost.return_value = mock_localhost_obj
        self.assertEqual(self.module.get_neb_ip(), '1.2.3.4')

    def test_get_neb_ip_RuntimeError(self):
        self.mock_scanline.component.localhost.LocalHost.side_effect = RuntimeError
        self.assertEqual(self.module.get_neb_ip(), None)


    def test_format_query(self):
        self.assertEqual(self.module.format_query("({ntpservers}):list,({passive_url}):url,({uid}):uuid,({addr}):hostname"
            ,["1","2"],'1.1.1.1','1','0.0.0.0'),('(1,2):list,(https://1.1.1.1/check_result):url,(1):uuid,(0.0.0.0):hostname'))


    def test_get_domain_name(self):
        self.assertEqual(self.module.get_domain_name('idm01mg1.idm01.isyntax.net'), 'idm01.isyntax.net')


    def test_set_pdc_role_owner(self):
        mock_domain_name = MagicMock(name='domain_name')
        mock_domain_name = 'idm01.isyntax.net'
        mock_win_rm = MagicMock(name='win_rm')
        mock_win_rm.execute_ps_script.return_value.std_out = 'idm01mg1.idm01.isyntax.net\n'
        self.module.write_cache = MagicMock(name='write_cache')
        self.module.set_pdc_role_owner(mock_win_rm , mock_domain_name)
        self.module.write_cache.assert_called_once_with(mock_domain_name, 'idm01mg1.idm01.isyntax.net', 7200)


    def test_get_dc_status_ok(self):
        self.module.read_cache = MagicMock(name='read_cache', return_value='idm01mg1.idm01.isyntax.net')
        self.mock_scanline.utilities.config_reader.read_from_tbconfig.return_value={'URL': 'https://localhost/thruk/'}
        self.mock_scanline.utilities.config_reader.get_thruk_response.return_value=[{'display_name': 'Product__IntelliSpace__TimeSync__Service__Status','state': 0}]
        self.assertEqual(self.module.get_dc_status('idm01.isyntax.net'),(0,''))


    def test_get_dc_status_unknown_thruk_api_empty(self):
        self.module.read_cache = MagicMock(name='read_cache', return_value='idm01mg1.idm01.isyntax.net')
        self.mock_scanline.utilities.config_reader.read_from_tbconfig.return_value={'URL': 'https://localhost/thruk/'}
        self.mock_scanline.utilities.config_reader.get_thruk_response.return_value={}
        self.assertEqual(self.module.get_dc_status('idm01.isyntax.net'),(3,'Empty response from Thruk API'))


    def test_get_dc_status_unknown_failed_activedc(self):
        self.module.read_cache = MagicMock(name='read_cache', return_value='idm01mg1.idm01.isyntax.net')
        self.mock_scanline.utilities.config_reader.read_from_tbconfig.return_value={'URL': 'https://localhost/thruk/'}
        self.mock_scanline.utilities.config_reader.get_thruk_response.return_value=[{'display_name': 'Product__IntelliSpace__TimeSync__Service__Status','state': 2}]
        self.assertEqual(self.module.get_dc_status('idm01.isyntax.net'),(3,'The script execution failed in Active MgNode = idm01mg1.idm01.isyntax.net'))

    def test_get_dc_status_unknown_condition_not_met(self):
        self.module.read_cache = MagicMock(name='read_cache', return_value='idm01mg1.idm01.isyntax.net')
        self.mock_scanline.utilities.config_reader.read_from_tbconfig.return_value={'URL': 'https://localhost/thruk/'}
        self.mock_scanline.utilities.config_reader.get_thruk_response.return_value=[{'display_name': 'Product__IntelliSpace__TimeSync__Service__Status','state': 1}]
        self.assertEqual(self.module.get_dc_status('idm01.isyntax.net'),(3,'Condition to run not met'))


    @mock.patch('scanline.utilities.cache.SafeCache.do')
    def test_write_cache(self, mock_safecache_do):
        mock_safecache_do.return_value = None
        self.assertEqual(self.module.write_cache('key', 'json', 540), None)

    @mock.patch('scanline.utilities.cache.SafeCache.do')
    def test_read_cache(self, mock_safecache_do):
        mock_safecache_do.return_value = 'json'
        self.assertEqual(self.module.read_cache('key'), 'json')


    def test_submit_timesync_job_ok_dc(self):
        self.module.generate_uuid = MagicMock(name = 'generate_uuid', return_value = '1')
        self.module.get_neb_ip = MagicMock(name= 'get_neb_ip', return_value = '1.1.1.1')
        self.module.get_domain_name = MagicMock(name = 'get_domain_name', return_value = 'idm01.isyntax.net')
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        self.mock_set_pdc_role_owner = MagicMock(name = 'set_pdc_role_owner', return_value = 'idm01mg1.idm01.isyntax.net')
        self.module.format_query = MagicMock(name = 'format_query', return_value = 'something')
        mock_winrm = MagicMock(name='WinRM')
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 0)
        exp_msg = "OK : Submitted the Powershell script with trigger_id : 1"
        self.assertEqual(msg, exp_msg)


    def test_submit_timesync_job_ok_nondc(self):
        self.module.generate_uuid = MagicMock(name = 'generate_uuid', return_value = '1')
        self.module.get_neb_ip = MagicMock(name= 'get_neb_ip', return_value = '1.1.1.1')
        self.module.get_domain_name = MagicMock(name = 'get_domain_name', return_value = 'idm01.isyntax.net')
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        mock_get_dc_status = MagicMock(name = 'get_dc_status')
        mock_get_dc_status.return_value = 0,''
        self.module.get_dc_status = mock_get_dc_status
        self.module.format_query = MagicMock(name = 'format_query', return_value = 'something')
        mock_winrm = MagicMock(name='WinRM')
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'NONDC')
        self.assertEqual(status, 0)
        exp_msg = "OK : Submitted the Powershell script with trigger_id : 1"
        self.assertEqual(msg, exp_msg)

    def test_submit_timesync_job_ok_not_ntp_servers(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value=[]
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"[]", 'NONDC')
        self.assertEqual(status, 0)
        exp_msg = "set NTP_SERVERS and try again"
        self.assertEqual(msg, exp_msg)


    def test_submit_timesync_job_activeDC_failure(self):
        self.module.generate_uuid = MagicMock(name = 'generate_uuid', return_value = '1')
        self.module.get_neb_ip = MagicMock(name= 'get_neb_ip', return_value = '1.1.1.1')
        self.module.get_domain_name = MagicMock(name = 'get_domain_name', return_value = 'idm01.isyntax.net')
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        mock_get_dc_status = MagicMock(name = 'get_dc_status')
        mock_get_dc_status.return_value = 3,'The script execution failed in Active MgNode = activeDC'
        self.module.get_dc_status = mock_get_dc_status
        status, msg = self.module.submit_timesync_job('activeDC', "user", "pass", 'localhost',"['something']", 'NONDC')
        self.assertEqual(status, 3)
        exp_msg = "The script execution failed in Active MgNode = activeDC"
        self.assertEqual(msg, exp_msg)


    def test_submit_timesync_job_exception(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 2)
        exp_msg = 'CRITICAL : Exception e'
        self.assertEqual(msg, exp_msg)

    def test_get_job_status_timeout_error(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError('WinRMOperationTimeoutError')
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 2)
        exp_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMOperationTimeoutError')
        self.assertEqual(msg, exp_msg)

    def test_get_job_status_authentication_error(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('AuthenticationError')
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 2)
        exp_msg = 'CRITICAL : Authentication Error - AuthenticationError'
        self.assertEqual(msg, exp_msg)

    def test_get_job_status_winrm_error(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('WinRMError')
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 2)
        exp_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMError')
        self.assertEqual(msg, exp_msg)

    def test_submit_timesync_job_typeerror(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value=[1,2]
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 2)
        exp_msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format('localhost')
        self.assertEqual(msg, exp_msg)

    def test_submit_timesync_job_type_error_submit_ps_script(self):
        self.module.generate_uuid = MagicMock(name = 'generate_uuid', return_value = '1')
        self.mock_scanline.utilities.utils.str_to_list.str_to_list.return_value=[1,2]
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('find() takes exactly 2')
        status, msg = self.module.submit_timesync_job('localhost', "user", "pass", 'localhost',"['something']", 'DC')
        self.assertEqual(status, 0)
        exp_msg = 'OK : Submitted the Powershell script with trigger_id : 1'
        self.assertEqual(msg, exp_msg)

    def test_main(self):
        self.module.submit_timesync_job = MagicMock(name='submit_timesync_job', return_value=(0, 'something'))
        self.module.check_arg  = MagicMock(name='check_arg', return_value='')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
