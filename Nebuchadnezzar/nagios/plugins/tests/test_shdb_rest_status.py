import unittest

from mock import MagicMock, patch


class CheckLogstashStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_request,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_shdb_rest_status
        self.check_shdb_rest_status = check_shdb_rest_status
        from check_shdb_rest_status import get_rest_status
        self.get_rest_status = get_rest_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.hostname ='h'
        obj2.proto='pr'
        obj2.port ='p'
        obj2.subpath ='s'
        obj2.servicename ='se'
        obj2.timeout ='t'
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.check_shdb_rest_status.check_arg(),('h','pr','p','s','se','t'))

    def test_rest_ok_status_no_host(self):
        resp = self.get_rest_status(None, 'http',9600, '', 'Logstash', 10)
        self.assertEqual(resp, (2, 'Could not get due to missing arguments'))

    def test_main(self):
        self.check_shdb_rest_status.exit = MagicMock()
        self.check_shdb_rest_status.get_rest_status = MagicMock(return_value=('1','2'))
        self.check_shdb_rest_status.main()

    def test_rest_ok_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'host': u'localhost', u'version': u'6.2.2'};
        self.mock_request.get.return_value = resp_mock
        resp = self.get_rest_status('localhost', 'http',9600, '', 'Logstash', 10)
        self.mock_request.get.assert_called_once_with(
            'http://localhost:9600', headers={'accept': 'application/v1+json'}, verify=False, timeout=10.0)
        resp_mock.raise_for_status.assert_called_once_with()
        self.assertEqual(resp, (0, "OK - Logstash is running; version:6.2.2"))

    def test_rest_critical_status(self):
        resp_mock = MagicMock(name='response')
        resp_mock.json.return_value = {u'host': u'localhost', u'version': u'6.2.2'};
        self.mock_request.get.return_value = resp_mock
        resp_mock.raise_for_status.side_effect = Exception('Exception')
        resp = self.get_rest_status('localhost', 'http',9600, '', 'Logstash', 10)
        self.mock_request.get.assert_called_once_with(
            'http://localhost:9600', headers={'accept': 'application/v1+json'}, verify=False, timeout=10.0)
        self.assertEqual(resp, (2, 'CRITICAL - Logstash is not running'))


if __name__ == '__main__':
    unittest.main()
