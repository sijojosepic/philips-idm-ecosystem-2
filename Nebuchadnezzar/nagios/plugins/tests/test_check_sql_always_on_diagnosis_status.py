from StringIO import StringIO
import unittest
import datetime

from mock import MagicMock, patch, Mock


class CheckSqlAlwaysOnDiagnosisTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dbutils': self.mock_scanline.utilities.dbutils
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_always_on_diagnosis_status
        self.module = check_sql_always_on_diagnosis_status

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.logsnap = 10
        mock_result.vigilant_url = 'http://vigilant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 10, 'http://vigilant/'))

    def test_get_diagnosis_status_1(self):
        self.module.get_connection = MagicMock(return_value="")
        self.module.get_formatted_data = MagicMock(return_value="mocked")
        self.assertEqual(self.module.get_diagnosis_status('localhost', 'test1', 'pass1', 'db1', 10, 'http://vigilant/'), (2,
                                                                                                      'CRITICAL: Cannot connect to database due to an incorrect username/password, or service check could not resolve db nodes.'))

    def test_get_diagnosis_status_2(self):
        self.module.get_formatted_data = MagicMock(return_value="Mocked")
        self.module.get_diagnosis_status('localhost', 'test1', 'pass1', 'db1', 10, 'http://vigilant/')
        self.assertEqual(self.mock_pymssql.connect.call_count, 1)

    def test_get_formatted_data_1(self):
        self.module.log_result_head = MagicMock(return_value=["mocked"])
        self.module.log_result_detail = MagicMock(return_value=[("mocked1", "mocked2", "mocked3", "mocked4")])
        self.assertEqual(self.module.get_formatted_data('cursor', 10),
                         (1,
                          'WARNING: Cluster Database diagnosed unhealthy :\nError : 19407\nmocked1 : mocked3\n\n For more information, please check SQL Server Logs.'))

    def test_get_formatted_data_2(self):
        self.module.log_result_head = MagicMock(return_value=[])
        self.assertEqual(self.module.get_formatted_data('cursor', 10),
                         (0, 'OK: Internal Database health is OK.'))

    def test_log_result_head(self):
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.return_value = True
        mock_cursor_obj.conn.return_value = True
        mock_cursor_obj.execute.return_value = True
        mock_cursor_obj.fetchall.return_value = ['']
        self.module.log_result_head(1, mock_cursor_obj)

    def test_log_result_detail(self):
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.return_value = True
        mock_cursor_obj.conn.return_value = True
        mock_cursor_obj.execute.return_value = True
        mock_cursor_obj.fetchall.return_value = ['']
        self.module.log_result_detail(1, mock_cursor_obj, 10, [datetime.datetime.now()])

    def test_get_connection(self):
        self.module.get_connection('localhost', 'test1', 'pass1', 'db1')
        self.assertEqual(self.mock_pymssql.connect.call_count, 1)

    def test_get_connection_exception(self):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(self.module.get_connection('localhost', 'test1', 'pass1', 'db1'), False)

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_diagnosis_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_diagnosis_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_diagnosis_status = MagicMock(name='get_diagnosis_status')
        self.module.get_diagnosis_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_diagnosis_status = MagicMock(name='get_diagnosis_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_diagnosis_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
