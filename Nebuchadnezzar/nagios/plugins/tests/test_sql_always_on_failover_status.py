from StringIO import StringIO
import unittest
import mock
import datetime
from mock import MagicMock, patch, Mock


class CheckSqlFailoverTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_always_on_failover_status
        self.module = check_sql_always_on_failover_status

    def test_get_dbnodes(self):
        self.module.get_dbnodes('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb')
        self.mock_pymssql.connect.assert_called_once_with(
            server='zvm01_cluster.zvm01.isyntax.net', user='phiadmin', password='philtor', database='msdb',
            login_timeout=20)

    def test_get_dbnodes_exception(self):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(self.module.get_dbnodes('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb'),
                         False)

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=False)
    def test_get_failover_status(self, mock_dbnodes):
        self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2, 5)
        self.mock_pymssql.connect.assert_called_once_with(
            server='zvm01_cluster.zvm01.isyntax.net', user='phiadmin', password='philtor', database='msdb',
            login_timeout=10)

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_with_nodes(self, mock_dbnodes):
        self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2, 5)
        self.assertEqual(self.mock_pymssql.connect.call_count, 3)

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_exception(self, mock_dbnodes):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2, 5),
            (2,
             'CRITICAL: Cannot connect to database due to an incorrect username/password, or service check could not resolve db nodes.'))

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_warning(self, mock_dbnodes):
        self.module.FAILOVER_COUNT = [(datetime.datetime(2018, 12, 14, 3, 6, 43, 610000), u'spid99s')]
        self.module.validate_query = Mock(return_value=[])
        self.assertEqual(
            self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2, 5),
            (1, 'WARNING - Database failover occurred 1 times in last 6 hours.'))

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_critical(self, mock_dbnodes):
        self.module.FAILOVER_COUNT = [(datetime.datetime(2018, 12, 14, 3, 6, 43, 610000), u'spid99s'),
                                      (datetime.datetime(2018, 12, 14, 2, 53, 53, 300000), u'spid28s')]
        self.module.validate_query = Mock(return_value=[])
        self.assertEqual(
            self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2, 5),
            (2,
             'CRITICAL - Database failover occurred 2 times in last 6 hours. This could cause database unavailability.'))

    @mock.patch('check_sql_always_on_failover_status.get_dbnodes', return_value=['db1', 'db2'])
    def test_get_failover_status_ok(self, mock_dbnodes):
        self.module.FAILOVER_COUNT = []
        self.module.validate_query = Mock(return_value=[])
        self.assertEqual(
            self.module.get_failover_status('zvm01_cluster.zvm01.isyntax.net', 'phiadmin', 'philtor', 'msdb', 6, 2, 5),
            (0, 'OK - There is no database failover occurred in last 6 hours.'))

    @mock.patch('check_sql_always_on_failover_status.pymssql.connect.cursor')
    def test_validate_query(self, mock_cursor):
        self.module.FAILOVER_COUNT = []
        self.module.validate_query(mock_cursor, 6)
        self.assertEqual(mock_cursor.execute.call_count, 4)

    @mock.patch('check_sql_always_on_failover_status.pymssql.connect.cursor')
    def test_validate_query_FAILCOUNT(self, mock_cursor):
        self.module.FAILOVER_COUNT = []
        mock_cursor.fetchall.return_value = ["(datetime.datetime(2018, 10, 20, 2, 49, 11, 90000), u'spid22s'"]
        self.module.validate_query(mock_cursor, 6)
        self.assertEqual(len(self.module.FAILOVER_COUNT), 4)

    @mock.patch('check_sql_always_on_failover_status.pymssql.connect.cursor')
    def test_validate_query_exception(self, mock_cursor):
        self.module.FAILOVER_COUNT = []
        mock_cursor.execute.side_effect = Exception('e')
        self.module.validate_query(mock_cursor, 6)
        self.assertEqual(len(self.module.FAILOVER_COUNT), 0)

    def test_get_failover_count_duplicate(self):
        self.module.FAILOVER_COUNT = [(datetime.datetime(2018, 12, 14, 3, 6, 43, 610000), u'spid99s'),
                                      (datetime.datetime(2018, 12, 14, 2, 53, 53, 300000), u'spid28s'),
                                      (datetime.datetime(2018, 12, 14, 3, 6, 43, 610000),
                                       u'spid99s')]
        self.assertEqual(self.module.get_failover_count(5), 2)

    def test_get_failover_count_empty(self):
        self.module.FAILOVER_COUNT = []
        self.assertEqual(self.module.get_failover_count(5), 0)

    def test_get_failover_count(self):
        self.module.FAILOVER_COUNT = [(datetime.datetime(2018, 12, 14, 3, 6, 43, 610000), u'spid99s'),
                                      (datetime.datetime(2018, 12, 14, 2, 53, 53, 300000), u'spid28s'),
                                      (datetime.datetime(2018, 12, 14, 3, 6, 42, 610000),
                                       u'spid99s')]
        self.assertEqual(self.module.get_failover_count(5), 2)

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.timediff = 6
        mock_result.failovercount = 2
        mock_result.logdiff = 5
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 6, 2, 5))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_failover_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_failover_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_failover_status = MagicMock(name='get_failover_status')
        self.module.get_failover_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_failover_status = MagicMock(name='get_failover_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_failover_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
