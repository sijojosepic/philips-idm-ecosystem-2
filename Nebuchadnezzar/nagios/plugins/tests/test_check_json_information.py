import unittest
from mock import MagicMock, patch


class CheckJSONInformationTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_jsonpath_rw = MagicMock(name='jsonpath_rw')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'requests': self.mock_requests,
            'jsonpath_rw': self.mock_jsonpath_rw,
            'phimutils': self.mock_phimutils,
            'phimutils.perfdata': self.mock_phimutils.perfdata,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_json_information
        self.module = check_json_information
        self.module.INFO_MARKER = '++I#'

        self.module.get_json = MagicMock(name='get_json')
        self.module.get_query = MagicMock(name='get_query')

        self.get_query_mock = self.module.get_query

        self.module.get_query.return_value.find.return_value = [MagicMock(value={'an': 'xt', 'b': 'yy'})]
        self.find_return_value = self.module.get_query.return_value.find.return_value

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_json_information_one_value(self):
        # to get a mock object with a value field
        self.find_return_value[:] = [MagicMock(value={'a': 'xt', 'b': 'yy'})]
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'a=b', False),
            '++I# xt=yy'
        )

    def test_check_json_information_one_value_no_label(self):
        # to get a mock object with a value field
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'b', False),
            '++I# b=yy'
        )

    def test_check_json_information_two_values(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'an': 'ct', 'b': 'ii', 'not': 'to use'}))
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'an=b', False),
            '++I# xt=yy; ct=ii'
        )

    def test_check_json_information_two_values_no_label(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'an': 'ct', 'b': 'ii', 'not': 'to use'}))
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'an', False),
            '++I# an=xt; an=ct'
        )

    def test_check_json_information_one_value_one_notall_data(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'an': 'ct', 'not': 'to use'}))
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'an=b', False),
            '++I# xt=yy'
        )

    def test_check_json_information_one_value_two_notall_data(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'an': 'ct', 'not': 'to use'}))
        self.find_return_value.append(MagicMock(value={'not': 'to use'}))
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'an=b', False),
            '++I# xt=yy'
        )

    def test_check_json_information_empty_result(self):
        self.find_return_value[:] = []
        self.assertEqual(
            self.module.check_json_information('nourl', '', 'an=b', False),
            '++I# '
        )

    def test_check_json_information_empty_val(self):
        # to get a mock object with a value field
        self.find_return_value[:] = [MagicMock(value={})]
        msg = self.module.check_json_information('nourl', '', 'an=b', False)
        self.assertEqual(msg, '++I# ')


if __name__ == '__main__':
    unittest.main()
