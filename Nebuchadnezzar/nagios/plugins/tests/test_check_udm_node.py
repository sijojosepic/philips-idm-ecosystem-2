import unittest
from mock import MagicMock, patch, call


class CheckUDMNodeTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_redis = MagicMock(name='redis')
        self.mock_json = MagicMock(name='json')
        self.mock_args = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name="scanline")
        modules = {
            'redis': self.mock_redis,
            'json': self.mock_json,
            'argparse': self.mock_args,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.utilities.http.HTTPRequester': self.mock_scanline.utilities.http.HTTPRequester,
            'scanline.utilities.config_reader': self.mock_scanline.utilities.config_reader
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_udm_node
        self.module = check_udm_node

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        results = MagicMock(name='mock_result')
        results.vigilant_url = 'vigilant_url'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = results
        self.module.argparse.ArgumentParser = self.mock_args
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, 'vigilant_url')

    def test_check_in_facts(self):
        mock_obj = MagicMock(name='response')
        mock_obj.content = '{"result": [{"ISP": {"module_type": 1024}, "hostname": "idm10ap1"}]}'
        self.mock_json.loads.return_value = {'result': [{'ISP': {'module_type': 1024}, 'hostname': 'idm10ap1'}]}
        self.mock_scanline.utilities.http.HTTPRequester.return_value.suppressed_get.return_value = mock_obj
        output = self.module.check_in_facts('siteid', 'vigilant_url')
        self.assertEqual(output, True)

    def test_check_in_redis(self):
        mock_obj = MagicMock(name='redis_con')
        mock_obj.keys.return_value = ['#idm10fc1.idm10.isyntax.net#ISPACS#16', '#idm04if1.idm04.isyntax.net#ISPACS#1024']
        self.mock_redis.StrictRedis.return_value = mock_obj
        output = self.module.check_in_redis()
        self.assertEqual(output, True)

    def test_main(self):
        self.module.check_in_redis = MagicMock(name='check_in_redis', return_value=True)
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)
            self.module.check_in_redis.assert_called_once_with()


if __name__ == '__main__':
    unittest.main()
