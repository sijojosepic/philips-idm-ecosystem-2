import unittest
from mock import MagicMock, patch


class CheckSHDBClientStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_redis = MagicMock(name='redis')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,            
            'argparse': self.mock_argparse,
            'redis': self.mock_redis
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_shdb_client_status
        self.check_shdb_client_status = check_shdb_client_status
        from check_shdb_client_status import get_process_status
        self.get_process_status = get_process_status


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        obj = MagicMock()
        obj2 = MagicMock()
        
        obj2.host = 'u'
        obj2.username = 'h'
        obj2.password = 'ha'
        obj2.process = 'd'
        obj2.versionpath = 'n'
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.check_shdb_client_status.check_arg(),('u','h','ha','d','n'))


    def test_process_ok_status(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_cmd.return_value.std_out = '\r\nfilebeat.exe'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.get_process_status('192.168.59.45', 'user', 'pass', 'filebeat.exe',''),\
                         (0, 'OK - filebeat.exe is running')
                        )

    def test_process_critical_status(self):
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_cmd.return_value.std_out = '\r\nfileseat.exe'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.assertEqual(self.get_process_status('192.168.59.45', 'user', 'pass', 'filebeat.exe',''),\
                         (2, 'CRITICAL - filebeat.exe is not running')
                        )

    def test_process_status_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        self.assertEqual(self.get_process_status('192.168.59.45', 'user', 'pass', 'filebeat.exe',''),\
                         (2, 'CRITICAL : Exception e')
                        )

    def test_is_shdb_configured(self):
        self.mock_redis.StrictRedis = MagicMock()
        self.mock_redis.StrictRedis().sismember = MagicMock(return_value='val')
        self.assertEqual( self.check_shdb_client_status.is_shdb_configured(), 'val')

    def test_is_shdb_configured_exception(self):
        self.mock_redis.StrictRedis = MagicMock(side_effect=Exception)
        self.assertEqual( self.check_shdb_client_status.is_shdb_configured(), False)

    def test_main(self):
        self.check_shdb_client_status.exit = MagicMock()
        self.check_shdb_client_status.is_shdb_configured = MagicMock(return_value=True)
        self.check_shdb_client_status.get_process_status = MagicMock(return_value=('1','2'))
        self.check_shdb_client_status.main()




if __name__ == '__main__':
    unittest.main()
