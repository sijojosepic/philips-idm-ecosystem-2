import sys
import unittest
from mock import MagicMock, patch



class CheckPoolStatusTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'winrm': self.mock_winrm,
            'argparse': self.mock_argparse,
            'sys': self.mock_sys,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_pool_status
        self.check_pool_status = check_pool_status
        from check_pool_status import get_pool_status
        self.get_pool_status = get_pool_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('check_pool_status.winrm.Session')
    def test_check_args(self, mock_winrm):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.hostaddress ='h'
        obj2.username='u'
        obj2.password ='p'
        obj2.poolname ='pn'
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.check_pool_status.check_args(),('h','u','p','pn'))

    @patch('check_pool_status.winrm.Session')
    def test_get_pool_status_ok(self, mock_winrm):
        mock_winrm.return_value.run_ps.return_value.std_out = 'Started'
        self.assertEqual(self.get_pool_status('127.0.0.1', 'admin', 'admin', 'testpool'), (0, 'testpool is up and running.'))

    @patch('check_pool_status.winrm.Session')
    def test_get_pool_status_stopped(self, mock_winrm):
        mock_winrm.return_value.run_ps.return_value.std_out = 'Stopped'
        self.assertEqual(self.get_pool_status('127.0.0.1', 'admin', 'admin', 'testpool'), (2, 'testpool is stopped.'))

    @patch('check_pool_status.winrm.Session')
    def test_get_pool_status_not_found(self, mock_winrm):
        self.assertEqual(self.get_pool_status('127.0.0.1', 'admin', 'admin', 'testpool'), (1, 'testpool not found.'))

    @patch('check_pool_status.winrm.Session')
    def test_main(self,mock_winrm):
        self.check_pool_status.get_pool_status = MagicMock(return_value=('state','val'))
        self.check_pool_status.main()


if __name__ == '__main__':
    unittest.main()
