from StringIO import StringIO

import unittest
from mock import MagicMock, patch

import requests
import winrm


class WindowsClusterStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'requests': self.mock_request,
            'requests.exceptions': self.mock_request.exceptions,
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_winrm.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_windows_cluster_status
        self.module = check_windows_cluster_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_ps_script(self):
        self.assertEqual(self.module.ps_script('cluster'), 'Get-cluster | select Name,State')

    def test_cluster_status_online(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n----  -----\r\nCluster IP Address  Online\r\nCluster Name Online\r\n'
        result = self.module.cluster_status(mock_out_put)
        exp_result = (0, 'OK : Cluster is up and running')
        self.assertEqual(result, exp_result)

    def test_cluster_status_offline(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n----  -----\r\nCluster IP Address  Online\r\nCluster Name Offline\r\n'
        result = self.module.cluster_status(mock_out_put)
        exp_result = (2, 'CRITICAL : Cluster is Offline')
        self.assertEqual(result, exp_result)

    def test_file_share_status_online(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n----  -----\r\nFile Share Witness  Online\r\n'
        result = self.module.file_share_status(mock_out_put)
        exp_result = (0, 'OK : File Share Witness is Online')
        self.assertEqual(result, exp_result)

    def test_file_share_status_offline(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n----  -----\r\nFile Share Witness  Offline\r\n'
        result = self.module.file_share_status(mock_out_put)
        exp_result = (2, 'CRITICAL : File Share Witness is Offline')
        self.assertEqual(result, exp_result)

    def test_file_share_status_failed(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n----  -----\r\nFile Share Witness  Failed\r\n'
        result = self.module.file_share_status(mock_out_put)
        exp_result = (2, 'CRITICAL : File Share Witness is Failed to start')
        self.assertEqual(result, exp_result)

    def test_cluster_node_status_critical(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n-- --\r\nDRZ01HL7  Down\r\nDRZ01HL8   Up\r\n\r\n\r\n'
        result = self.module.cluster_node_status(mock_out_put)
        exp_result = (2, 'CRITICAL : Following node in the cluster is Down - DRZ01HL7')
        self.assertEqual(result, exp_result)

    def test_cluster_node_status_warning(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n-- --\r\nDRZ01HL7  Paused\r\nDRZ01HL8   Up\r\n\r\n\r\n'
        result = self.module.cluster_node_status(mock_out_put)
        exp_result = (1, 'WARNING : Following node in the cluster is Paused - DRZ01HL7')
        self.assertEqual(result, exp_result)

    def test_cluster_node_status_ok(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n-- --\r\nDRZ01HL7  Up\r\nDRZ01HL8   Up\r\n\r\n\r\n'
        result = self.module.cluster_node_status(mock_out_put)
        exp_result = (0, 'OK : All nodes in the cluster are up and running')
        self.assertEqual(result, exp_result)

    def test_cluster_node_status_critical_warning(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.std_out = '\r\nName  State\r\n-- --\r\nDRZ01HL7  Down\r\nDRZ01HL8   Paused\r\n\r\n\r\n'
        result = self.module.cluster_node_status(mock_out_put)
        exp_result = (2, 'CRITICAL : Following node in the cluster is Down - DRZ01HL7\nFollowing node in the cluster '
                         'is in Paused stateDRZ01HL8')
        self.assertEqual(result, exp_result)

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = requests.exceptions.RequestException()
        self.module.main('host', 'username', 'password', 'command', 'subcommand')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception \n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.main('host', 'username', 'password', 'command', 'subcommand')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Issue in connecting to cluster IP')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = TypeError('e')
        self.module.main('host', 'username', 'password', 'command', 'subcommand')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Typeerror(May be Issue in connecting to cluster IP)')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = winrm.exceptions.AuthenticationError()
        self.module.main('host', 'username', 'password', 'command', 'subcommand')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception \n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.WinRM.side_effect = Exception('e')
        self.module.main('host', 'username', 'password', 'command', 'subcommand')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception e\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_cluster_status_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 'mock_ps_output'
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_ps_script = MagicMock(name='ps_script')
        mock_ps_script.return_value = 'script'
        self.module.ps_script = mock_ps_script
        mock_cluster_status = MagicMock(name='cluster_status')
        mock_cluster_status.return_value = 0, 'msg'
        self.module.cluster_status = mock_cluster_status
        self.module.main('host', 'username', 'password', 'ClusterResource', 'Cluster Name')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_winrm.utilities.WinRM.assert_called_once_with('host', 'username', 'password')
        mock_WinRM.execute_ps_script.assert_called_once_with('script')
        mock_ps_script.assert_called_once_with('ClusterResource')
        mock_cluster_status.assert_called_once_with('mock_ps_output')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_file_share_status_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 'mock_ps_output'
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_ps_script = MagicMock(name='ps_script')
        mock_ps_script.return_value = 'script'
        self.module.ps_script = mock_ps_script
        mock_file_share_status = MagicMock(name='file_share_status')
        mock_file_share_status.return_value = 0, 'msg'
        self.module.file_share_status = mock_file_share_status
        self.module.main('host', 'username', 'password', 'ClusterResource', 'File Share Witness')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_winrm.utilities.WinRM.assert_called_once_with('host', 'username', 'password')
        mock_WinRM.execute_ps_script.assert_called_once_with('script')
        mock_ps_script.assert_called_once_with('ClusterResource')
        mock_file_share_status.assert_called_once_with('mock_ps_output')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_cluster_node_status_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 'mock_ps_output'
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        mock_ps_script = MagicMock(name='ps_script')
        mock_ps_script.return_value = 'script'
        self.module.ps_script = mock_ps_script
        mock_cluster_node_status = MagicMock(name='cluster_node_status')
        mock_cluster_node_status.return_value = 0, 'msg'
        self.module.cluster_node_status = mock_cluster_node_status
        self.module.main('host', 'username', 'password', 'ClusterNode', 'Node')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_winrm.utilities.WinRM.assert_called_once_with('host', 'username', 'password')
        mock_WinRM.execute_ps_script.assert_called_once_with('script')
        mock_ps_script.assert_called_once_with('ClusterNode')
        mock_cluster_node_status.assert_called_once_with('mock_ps_output')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_unknown(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        self.mock_winrm.utilities.WinRM.return_value = mock_WinRM
        self.module.main('host', 'username', 'password', 'command', 'Node')
        self.sys_mock.exit.assert_called_once_with(3)
        exp_msg = 'UNKNOWN: Command or sub-command is invalid\n'
        self.assertEqual(std_out.getvalue(), exp_msg)


if __name__ == '__main__':
    unittest.main()
