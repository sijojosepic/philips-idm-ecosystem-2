import unittest
from mock import MagicMock, patch
from datetime import datetime


class CheckSSHCommandTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.ssh': self.mock_scanline.utilities.ssh,
            'scanline.utilities.ssh.SSHConnection': self.mock_scanline.utilities.ssh.SSHConnection,
            'scanline.utilities.utils': self.mock_scanline.utilities.utils,
            'scanline.utilities.utils.format_status': self.mock_scanline.utilities.utils.format_status,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_ssh_command
        self.module = check_ssh_command

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_result.command = 'partition'
        mock_result.warning = 80
        mock_result.critical = 90
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('localhost', 'user', 'pass', 'partition', 80, 90))

    def test_check_partition(self):
        raw_output = """test / 40% \ntest1 /var 100%"""
        self.module.check_partition_used_percent = MagicMock(name="check_partition_used_percent",
                                                             return_value=('warn_msg', 'crt_msg', 'ok_msg', 'perf_msg'))
        self.module.format_status = MagicMock(name="format_status", return_value=(0, 'OK'))
        self.assertEquals((0, 'OK'), self.module.check_partition(raw_output, 80, 85))
        self.module.check_partition_used_percent.assert_called_once_with([['test1', '/var', '100%']], 80, 85)
        self.module.format_status.assert_called_once_with('warn_msg', 'crt_msg', 'ok_msg', 'perf_msg')

    def test_check_partition_used_percent_ok(self):
        ok_msg = 'OK: Partitions: Mounted on: /, Used%: 40.0 \nMounted on: /var, Used%: 60.0 \n'
        perf_msg = '/=40%;80;85;\n/var=60%;80;85;\n'
        self.assertEquals(('', '', ok_msg, perf_msg),
                          self.module.check_partition_used_percent([['test', '40%', '/'], ['test1', '60%', '/var']], 80,
                                                                   85))

    def test_check_partition_used_percent_critical(self):
        crit_msg = 'CRITICAL: Threshold breached for partitions: Mounted on: /var, Used%: 60.0 \nOK: Partitions: ' \
                   'Mounted on: /, Used%: 40.0 \n'
        ok_msg = 'OK: Partitions: Mounted on: /, Used%: 40.0 \n'
        perf_msg = '/=40%;100;50;\n/var=60%;100;50;\n'
        self.assertEquals(('', crit_msg, ok_msg, perf_msg),
                          self.module.check_partition_used_percent([['test', '40%', '/'], ['test1', '60%', '/var']],
                                                                   100, 50))

    def test_check_partition_used_percent_warning(self):
        warn_msg = 'WARNING: Threshold breached for partitions: Mounted on : /var, Used%: 60.0 \nOK: Partitions: ' \
                   'Mounted on: /, Used%: 40.0 \n'
        ok_msg = 'OK: Partitions: Mounted on: /, Used%: 40.0 \n'
        perf_msg = '/=40%;50;100;\n/var=60%;50;100;\n'
        self.assertEquals((warn_msg, '', ok_msg, perf_msg),
        self.module.check_partition_used_percent([['test','40%', '/'],['test1','60%', '/var']], 50, 100))

    def test_check_partition_used_percent_warn_crit(self):
        warn_msg = 'WARNING: Threshold breached for partitions: Mounted on : /, Used%: 40.0 \n'
        crit_msg = 'CRITICAL: Threshold breached for partitions: Mounted on: /var, Used%: 60.0 \nWARNING: ' \
                   'Threshold breached for partitions: Mounted on : /, Used%: 40.0 \n'
        perf_msg = '/=40%;39;59;\n/var=60%;39;59;\n'
        self.assertEquals((warn_msg, crit_msg, '', perf_msg),
                          self.module.check_partition_used_percent([['test', '40%', '/'], ['test1', '60%', '/var']],
                                                                   39, 59))

    def test_check_status_partition_invalid_command(self):
        self.assertEqual((2, 'CRITICAL : Invalid command - invalid_command'),
                         self.module.check_status('host', 'usr', 'pass', 'invalid_command', 80, 90))

    def test_check_status_partition_error(self):
        ssh_obj = MagicMock(name='ssh_obj')
        self.mock_scanline.utilities.ssh.SSHConnection.return_value = ssh_obj
        ssh_obj.execute_command.return_value = ("SSH ERROR", None)
        self.assertEqual((2, 'CRITICAL : Error - SSH ERROR'),
                         self.module.check_status('host', 'usr', 'pass', 'partition', 80, 90))
        ssh_obj.execute_command.assert_called_once_with('df -h')

    def test_check_status_partition_exception(self):
        ssh_obj = MagicMock(name='ssh_obj')
        self.mock_scanline.utilities.ssh.SSHConnection.return_value = ssh_obj
        ssh_obj.execute_command.side_effect = Exception('e')
        self.assertEqual((2, 'CRITICAL : Error while retrieving data e'),
                         self.module.check_status('host', 'usr', 'pass', 'partition', 80, 90))
        ssh_obj.execute_command.assert_called_once_with('df -h')

    def test_check_status_partition_ok(self):
        ssh_obj = MagicMock(name='ssh_obj')
        self.mock_scanline.utilities.ssh.SSHConnection.return_value = ssh_obj
        ssh_obj.execute_command.return_value = (None, "ssh_output")
        self.module.check_partition = MagicMock(name='check_partition', return_value=(0, 'OK'))
        self.assertEqual((0, 'OK'), self.module.check_status('host', 'usr', 'pass', 'partition', 80, 90))
        self.module.check_partition.assert_called_once_with('ssh_output', 80, 90)
        ssh_obj.execute_command.assert_called_once_with('df -h')

    def test_check_license_ok(self):
        ssh_output = """Sys::Licse\nLicensed Version 12.1.1\nLicensed On 2017/04/17\nDNS Licensed Objects, Unlimited"""
        self.mock_scanline.utilities.utils.format_status.return_value = (0, 'OK')
        self.assertEquals((0, 'OK'), self.module.check_license(ssh_output, 0, 5))
        self.mock_scanline.utilities.utils.format_status.assert_called_once_with('', '',
                                                                                 'OK: Permanent License in '
                                                                                 'use: \n'+ssh_output, '')

    def test_check_license_expired(self):
        ssh_output = """Sys::License\nLicensed Version 12.1.2\nLicensed On 2019/11/18\nLicense Start Date 2019/11/17\nLicense End Date 2020/01/03\n   """
        self.mock_scanline.utilities.utils.format_status.return_value = (2, 'CRITICAL')
        self.assertEquals((2, 'CRITICAL'), self.module.check_license(ssh_output, 0, 5))
        self.mock_scanline.utilities.utils.\
            format_status.assert_called_once_with('', 'CRITICAL: F5 License is expired. License end date: '
                                                      '2020-01-03 00:00:00 \n '+ssh_output, '', '')

    def test_check_license_critical_expiring(self):
        self.module.datetime = MagicMock(name='datetime')
        now_date = datetime.strptime('2020/05/30', '%Y/%m/%d')
        self.module.datetime.now.return_value = now_date
        self.module.datetime.strptime.return_value = datetime.strptime('2020/06/03', '%Y/%m/%d')
        output = """Sys::License\nLicensed Version 12.1.2\nLicensed On 2019/11/18\nLicense Start Date 2019/11/17\nLicense End Date 2020/06/03\n   """
        self.mock_scanline.utilities.utils.format_status.return_value = (2, 'CRITICAL')
        self.assertEquals((2, 'CRITICAL'), self.module.check_license(output, 0, 5))
        self.mock_scanline.utilities.utils.\
            format_status.assert_called_once_with('', 'CRITICAL: Temporary License is in use. 4 days left for license '
                                                      'expiry and license end date is 2020-06-03 00:00:00 \n '+output,
                                                  '', '')

    def test_check_license_ok_expiring(self):
        self.module.datetime = MagicMock(name='datetime')
        now_date = datetime.strptime('2020/05/29', '%Y/%m/%d')
        self.module.datetime.now.return_value = now_date
        self.module.datetime.strptime.return_value = datetime.strptime('2020/07/03', '%Y/%m/%d')
        output = """Sys::License\nLicensed Version 12.1.2\nLicensed On 2019/11/18\nLicense Start Date 2019/11/17\nLicense End Date 2020/07/03\n   """
        self.mock_scanline.utilities.utils.format_status.return_value = (0, 'OK')
        self.assertEquals((0, 'OK'), self.module.check_license(output, 0, 5))
        self.mock_scanline.utilities.utils.\
            format_status.assert_called_once_with('', '', 'OK: Temporary License is in use. 35 days left for license '
                                                          'expiry and license end date is 2020-07-03 00:00:00 \n'
                                                          ' '+output, '')

    def test_main(self):
        self.module.check_args = MagicMock(name='check_args')
        self.module.check_args.return_value = ('localhost', 'user', 'pass', 'partition', 80, 90)
        self.module.check_status = MagicMock(name='check_status')
        self.module.check_status.return_value = (0, 'OK')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
