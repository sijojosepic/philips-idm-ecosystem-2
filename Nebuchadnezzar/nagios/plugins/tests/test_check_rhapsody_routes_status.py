import unittest
from requests.exceptions import RequestException
from mock import MagicMock, patch, Mock


class CheckRhapsodyRouteDataTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        modules = {
            'argparse': self.mock_argparse,
            'requests': self.mock_request,
            'sys': self.mock_sys
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rhapsody_routes_status
        self.check_rhapsody_routes_status = check_rhapsody_routes_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.port = '80'
        mock_result.rhapsodyusername = 'test1'
        mock_result.rhapsodypassword = 'pass1'
        mock_result.protocol = 'https'
        mock_result.critical = ['CRITICAL']
        mock_result.warning = ['WARNING']
        mock_result.ok = ['OK']
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.check_rhapsody_routes_status.argparse.ArgumentParser = MagicMock(name='argparse')
        self.check_rhapsody_routes_status.argparse.ArgumentParser.return_value = mock_parser
        response = self.check_rhapsody_routes_status.check_arg()
        self.assertEqual(response, ('localhost', '80', 'test1', 'pass1', 'https', ['CRITICAL'], ['WARNING'], ['OK']))

    def test_check_route_status_case1(self):
        mock_get = MagicMock(name='get_request')
        mock_get = True
        self.mock_request.get.return_value = mock_get
        response = self.check_rhapsody_routes_status.check_route_status("localhost", "80", "test1", "pass1", "https")
        self.assertEqual(response, True)

    def test_check_json_extractor_case1(self):
        data = {}
        response = list(self.check_rhapsody_routes_status.json_extractor(data))
        self.assertEqual(response, [])

    def test_check_json_extractor_case2(self):
        data = {
            'id': 1234,
            'type': 'ROUTE',
            'childComponents': [{
                "id": 113,
                "name": "iSite\u0020Flat\u0020File\u0020Xml\u0020Filing\u0020Service\u0020",
                "state": "STOPPED",
                "type": "ROUTE"
            }],
            'child': {}
        }
        response = list(self.check_rhapsody_routes_status.json_extractor(data))
        self.assertEqual(response, [{'state': 'STOPPED', 'type': 'ROUTE', 'id': 113,
                                     'name': 'iSite\\u0020Flat\\u0020File\\u0020Xml\\u0020Filing\\u0020Service\\u0020'}])

    def test_compose_status_message_1(self):
        response = self.check_rhapsody_routes_status.compose_status_message(
            {u'STOPPED': {'count': 2, 'id': [204, 203]}}, ['STOPPED', 'ERROR', 'DELETED'], ['STARTING', 'STOPPING'],
            ['RUNNING'])
        self.assertEqual(response, (2, 'CRITICAL - 2 route status is unhealthy. STOPPED; Count 2; IDs [204, 203]. \n'))

    def test_compose_status_message_2(self):
        response = self.check_rhapsody_routes_status.compose_status_message(
            {u'STOPPING': {'count': 2, 'id': [204, 203]}}, ['STOPPED', 'ERROR', 'DELETED'], ['STARTING', 'STOPPING'],
            ['RUNNING'])
        self.assertEqual(response, (1, 'WARNING - 2 route status is unhealthy. STOPPING; Count 2; IDs [204, 203]. \n'))

    def test_compose_status_message_3(self):
        response = self.check_rhapsody_routes_status.compose_status_message(
            {u'RUNNING': {'count': 2, 'id': [204, 203]}}, ['STOPPED', 'ERROR', 'DELETED'], ['STARTING', 'STOPPING'],
            ['RUNNING'])
        self.assertEqual(response, (0, 'OK - 2 route status is healthy. RUNNING; Count 2; IDs [204, 203]. \n'))

    def test_check_json_extractor_case3(self):
        data = {
            'id': 1234,
            'type': 'ROUTE',
            'childComponents': {
                "id": 113,
                "name": "iSite",
                "state": "STOPPED",
                "type": "ROUTE"
            },
            'child': {}
        }
        response = list(self.check_rhapsody_routes_status.json_extractor(data))
        self.assertEqual(response, [{'state': 'STOPPED', 'type': 'ROUTE', 'id': 113, 'name': 'iSite'}])

    def test_aggregate_status_1(self):
        response = self.check_rhapsody_routes_status.aggregate_status(
            [{u'state': u'STOPPED', u'type': u'ROUTE', u'id': 204, u'name': u'FlatFile-CSV'}])
        self.assertEqual(response, {u'STOPPED': {'count': 1, 'id': [204]}})

    def test_aggregate_status_2(self):
        response = self.check_rhapsody_routes_status.aggregate_status(
            [{u'state': u'STOPPED', u'type': u'ROUTE', u'id': 204, u'name': u'FlatFile-CSV'},
             {u'state': u'STOPPED', u'type': u'ROUTE', u'id': 203, u'name': u'FlatFile-CSV'}])
        self.assertEqual(response, {u'STOPPED': {'count': 2, 'id': [204, 203]}})

    def test_process_data_case1(self):
        data = {
            'id': 1234,
            'type': 'ROUTE',
            'childComponents': {
                "id": 113,
                "name": "iSite",
                "state": "STOPPED",
                "type": "ROUTE"
            },
            'child': {}
        }
        self.check_rhapsody_routes_status.json_extractor = MagicMock(name='json_extractor',
                                                                     return_value=[{'state': 'STOPPED', 'type': 'ROUTE',
                                                                                    'id': 113, 'name': 'iSite'}])
        self.check_rhapsody_routes_status.aggregate_status = MagicMock(name='aggregate_status',
                                                                       return_value={
                                                                           'STOPPED': {'count': 1, 'id': [113]}})
        self.check_rhapsody_routes_status.compose_status_message = MagicMock(name='compose_status_message',
                                                                             return_value=(2,
                                                                                           'CRITICAL - 1 route status is unhealthy. STOPPED; Count 1; IDs [113]. \n'))
        response = self.check_rhapsody_routes_status.process_data(data, '["STOPPED","ERROR","DELETED"]',
                                                                  '["STARTING","STOPPING"]', '["RUNNING"]')
        self.assertEqual(response, (2, 'CRITICAL - 1 route status is unhealthy. STOPPED; Count 1; IDs [113]. \n'))

    def test_unknown_message(self):
        response = self.check_rhapsody_routes_status.unknown_message(['STOPPppED', 'ERRppOR', 'DELETEppD'],
                                                                     ['STARTIppNG', 'STOPPINpG'], ['RUNNINGp'])
        self.assertEqual(response,
                         "No Route data found with given configuration CRITICAL - ['STOPPppED', 'ERRppOR', 'DELETEppD'], WARNING : ['STARTIppNG', 'STOPPINpG'],OK: ['RUNNINGp']")

    def test_process_data_case2(self):
        data = {
            'id': 1234,
            'type': 'ROUTE',
            'childComponents': {
                "id": 113,
                "name": "iSite",
                "state": "RUNNING",
                "type": "ROUTE"
            },
            'child': {}
        }
        self.check_rhapsody_routes_status.json_extractor = MagicMock(name='json_extractor',
                                                                     return_value=[{'state': 'RUNNING', 'type': 'ROUTE',
                                                                                    'id': 113, 'name': 'iSite'}])
        self.check_rhapsody_routes_status.aggregate_status = MagicMock(name='aggregate_status',
                                                                       return_value={
                                                                           'RUNNING': {'count': 1, 'id': [113]}})
        self.check_rhapsody_routes_status.compose_status_message = MagicMock(name='compose_status_message',
                                                                             return_value=(0,
                                                                                           'OK - 1 route status is healthy. RUNNING; Count 1; IDs [113]. \n'))
        response = self.check_rhapsody_routes_status.process_data(data, '["RUNNING","ERROR","DELETED"]',
                                                                  '["STARTING","STOPPING"]', '["RUNNING"]')
        self.assertEqual(response, (0, 'OK - 1 route status is healthy. RUNNING; Count 1; IDs [113]. \n'))

    def test_str_to_list(self):
        response = self.check_rhapsody_routes_status.str_to_list('["STOPPED","ERROR","DELETED"]')
        self.assertEqual(response, ["STOPPED", "ERROR", "DELETED"])

    def test_main_case1(self):
        self.check_rhapsody_routes_status.check_arg = MagicMock(name='check_arg', return_value=(
            'arg1', 'arg2', 'arg3', 'arg4', 'arg5', 'arg6', 'arg7', 'arg8'))
        mock_response = MagicMock(name='mock_response')
        mock_response.status_code = 401
        mock_response.content = {}
        self.check_rhapsody_routes_status.check_route_status = MagicMock(name='check_route_status',
                                                                         return_value=mock_response)
        self.mock_sys.exit = MagicMock(name='exit', return_value=True)
        response = self.check_rhapsody_routes_status.main()
        self.mock_sys.exit.assert_called_once_with(2)

    def test_main_case2(self):
        self.check_rhapsody_routes_status.check_arg = MagicMock(name='check_arg', return_value=(
            'arg1', 'arg2', 'arg3', 'arg4', 'arg5', 'arg6', 'arg7', 'arg8'))
        mock_response = MagicMock(name='mock_response')
        mock_response.status_code = 500
        mock_response.content = {}
        self.check_rhapsody_routes_status.check_route_status = MagicMock(name='check_route_status',
                                                                         return_value=mock_response)
        self.mock_sys.exit = MagicMock(name='exit', return_value=True)
        self.check_rhapsody_routes_status.main()
        self.mock_sys.exit.assert_called_once_with(2)

    def test_main_case3(self):
        self.check_rhapsody_routes_status.check_arg = MagicMock(name='check_arg', return_value=(
            'arg1', 'arg2', 'arg3', 'arg4', 'arg5', '[]', '[]', '[]'))
        mock_response = MagicMock(name='mock_response')
        mock_response.status_code = 200
        self.check_rhapsody_routes_status.check_route_status = MagicMock(name='check_route_status',
                                                                         return_value=mock_response)
        self.check_rhapsody_routes_status.process_data = MagicMock(name='process_data',
                                                                   return_value=(0,
                                                                                 'OK - 1 route status is healthy. RUNNING; Count 1; IDs [204].'))
        response = self.check_rhapsody_routes_status.main()
        self.mock_sys.exit.assert_called_once_with(0)

    def test_main_case4(self):
        self.check_rhapsody_routes_status.check_arg = MagicMock(name='check_arg', return_value=(
            'arg1', 'arg2', 'arg3', 'arg4', 'arg5', 'arg6', 'arg7', 'arg8'))
        self.check_rhapsody_routes_status.check_route_status = MagicMock(name='check_route_status',
                                                                         side_effect=Exception)
        response = self.check_rhapsody_routes_status.main()
        self.mock_sys.exit.assert_called_once_with(2)

    def test_main_case5(self):
        self.check_rhapsody_routes_status.check_arg = MagicMock(name='check_arg', return_value=(
            'arg1', 'arg2', 'arg3', 'arg4', 'arg5', 'arg6', 'arg7', 'arg8'))
        self.check_rhapsody_routes_status.check_route_status = MagicMock(name='check_route_status',
                                                                         side_effect=RequestException())
        self.check_rhapsody_routes_status.requests.exceptions.RequestException = RequestException
        response = self.check_rhapsody_routes_status.main()
        self.mock_sys.exit.assert_called_once_with(2)


if __name__ == '__main__':
    unittest.main()
