from StringIO import StringIO
import unittest
from mock import MagicMock, patch, call
from winrm.exceptions import WinRMError, AuthenticationError


class WindowsServiceStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_windows_service_status
        self.module = check_windows_service_status

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.service = 'Dnscache'
        mock_result.argument = 'INSTALLED'
        mock_result.state = 'CRITICAL'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'Dnscache', 'INSTALLED', 'CRITICAL'))

    def test_ps_script(self):
        self.assertEqual(self.module.ps_script('Dnscache'), 'Get-WmiObject win32_service -Filter "name=\'Dnscache\'"')

    def test_check_status(self):
        result = self.module.check_status('CRITICAL', 'Service is not installed')
        exp_result = (2, 'CRITICAL : Service is not installed')
        self.assertEqual(result, exp_result)

    def test_check_mode_with_output(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = '\r\n\r\nExitCode  : 0\r\nName      : Dnscache\r\nProcessId : 336' \
                               '\r\nStartMode : Auto\r\nState     : Running\r\nStatus    : OK\r\n\r\n\r\n\r\n'
        formatted_output = ['', '', 'ExitCode  : 0', 'Name      : Dnscache', 'ProcessId : 336', 'StartMode : Auto',
                            'State     : Running', 'Status    : OK', '', '', '', '']
        mock_get_service_mode_state = MagicMock(name='get_service_mode_state')
        mock_get_service_mode_state.return_value = 'status', 'msg'
        self.module.get_service_mode_state = mock_get_service_mode_state
        result = self.module.check_mode(mock_out_put, 'INSTALLED', 'WARNING')
        self.assertEqual(result, ('status', 'msg'))
        mock_get_service_mode_state.assert_called_once_with(formatted_output)

    def test_check_mode_with_notinstalled_argument(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = []
        result = self.module.check_mode(mock_out_put, 'NOTINSTALLED', 'WARNING')
        exp_result = (0, 'OK : Service is not installed| service status=0;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_mode_with_installed_argument(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = []
        mock_check_status = MagicMock(name='check_status')
        mock_check_status.return_value = 'status', 'msg'
        self.module.check_status = mock_check_status
        result = self.module.check_mode(mock_out_put, 'INSTALLED', 'WARNING')
        exp_result = 'status', 'msg'
        self.assertEqual(result, exp_result)

    def test_check_mode_with_invalid_argument(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = []
        self.assertEqual(self.module.check_mode(mock_out_put, 'INVALID', 'WARNING'), (1, 'WARNING: Argument or state supplied to plugin is invalid'))

    def test_get_service_mode_state(self):
        out_put = ['', '', 'ExitCode  : 0', 'Name      : Dnscache', 'ProcessId : 336', 'StartMode : Auto',
                   'State     : Running', 'Status    : OK']
        mock_check_state = MagicMock(name='check_state')
        mock_check_state.return_value = 'status', 'msg'
        self.module.check_state = mock_check_state
        result = self.module.get_service_mode_state(out_put)
        self.assertEqual(result, ('status', 'msg'))
        mock_check_state.assert_called_once_with(['Auto', 'Running'])

    def test_check_state_disabled_stopped(self):
        state = ['Disabled', 'Stopped']
        result = self.module.check_state(state)
        exp_result = (0, 'OK : Service startup type is set to Disabled and Status is Stopped| service status=0;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_disabled_running(self):
        state = ['Disabled', 'Running']
        result = self.module.check_state(state)
        exp_result = (0, 'OK : Service startup type is set to Disabled and Status is Running| service status=0;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_manual_stopped(self):
        state = ['Manual', 'Stopped']
        result = self.module.check_state(state)
        exp_result = (2, 'CRITICAL : Service startup type is set to Manual and Status is Stopped| service status=2;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_manual_paused(self):
        state = ['Manual', 'Paused']
        result = self.module.check_state(state)
        exp_result = (1, 'WARNING : Service startup type is set to Manual and Status is Paused| service status=1;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_manual_running(self):
        state = ['Manual', 'Running']
        result = self.module.check_state(state)
        exp_result = (0, 'OK : Service startup type is set to Manual and Status is Running| service status=0;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_auto_running(self):
        state = ['Auto', 'Running']
        result = self.module.check_state(state)
        exp_result = (0, 'OK : Service startup type is set to Automatic and Status is Running| service status=0;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_auto_paused(self):
        state = ['Auto', 'Paused']
        result = self.module.check_state(state)
        exp_result = (1, 'WARNING : Service startup type is set to Automatic and Status is Paused| service status=1;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_auto_start_pending(self):
        state = ['Auto', 'Start Pending']
        result = self.module.check_state(state)
        exp_result = (1, 'WARNING : Service startup type is set to Automatic and Status is Start Pending| service status=1;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_auto_stopped(self):
        state = ['Auto', 'Stopped']
        result = self.module.check_state(state)
        exp_result = (2, 'CRITICAL : Service startup type is set to Automatic and Status is not Running| service status=2;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_check_state_unknown_state(self):
        state = ['Auto', 'topped']
        result = self.module.check_state(state)
        exp_result = (2, 'Service startup type or status unknown')
        self.assertEqual(result, exp_result)

    def test_process_output(self):
        mock_check_mode = MagicMock(name='check_mode')
        mock_check_mode.return_value = 'status', 'msg'
        self.module.check_mode = mock_check_mode
        result = self.module.process_output('output', 'INSTALLED', 'CRITICAL')
        self.assertEqual(result, ('status', 'msg'))
        mock_check_mode.assert_called_once_with('output', 'INSTALLED', 'CRITICAL')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 'out_put'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        mock_ps_script = MagicMock(name='ps_script')
        mock_ps_script.return_value = 'script'
        self.module.ps_script = mock_ps_script
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 0, 'msg'
        self.module.process_output = mock_process_output
        self.module.main('host', 'username', 'password', 'DNS', 'INSTALLED', 'CRITICAL')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_scanline.utilities.win_rm.WinRM.assert_called_once_with('host', 'username', 'password')
        mock_WinRM.execute_ps_script.assert_called_once_with('script')
        mock_ps_script.assert_called_once_with('DNS')
        mock_process_output.assert_called_once_with('out_put', 'INSTALLED', 'CRITICAL')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_authentication_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('e')
        self.module.AuthenticationError = AuthenticationError
        self.module.main('host', 'username', 'password', 'DNS', 'INSTALLED', 'CRITICAL')
        self.sys_mock.exit.assert_called_once_with(3)
        self.assertEqual(std_out.getvalue(), 'UNKNOWN : WinRM Error e| service status=3;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_winrm_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('e')
        self.module.WinRMError = WinRMError
        self.module.main('host', 'username', 'password', 'DNS', 'INSTALLED', 'CRITICAL')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : WinRM Error e| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error_takes_exactly_2(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.main('host', 'username', 'password', 'DNS', 'INSTALLED', 'CRITICAL')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Issue in connecting to node - host| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        self.module.main('host', 'username', 'password', 'DNS', 'INSTALLED', 'CRITICAL')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Typeerror(May be Issue in connecting to node - host)| '
                                             'service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        self.module.main('host', 'username', 'password', 'DNS', 'INSTALLED', 'CRITICAL')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception e| service status=2;1;2;0;2\n')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()

