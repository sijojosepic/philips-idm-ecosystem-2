from StringIO import StringIO
import unittest
from mock import MagicMock, patch, Mock


class CheckSqlJobTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dbutils': self.mock_scanline.utilities.dbutils
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sqljob_status
        self.module = check_sqljob_status
        self.module.QUERY = "select * from test_sql_job;"

    def test_check_arg_1(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.job = 'AdminDBfullBackup'
        mock_result.vigilant_url = 'http://vigliant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'AdminDBfullBackup', 'http://vigliant/'))

    def test_check_arg_2(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.job = 'AdminDBTrnBackup'
        mock_result.vigilant_url = 'http://vigliant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'AdminDBTrnBackup', 'http://vigliant/'))

    def test_check_arg_3(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.job = 'AdminDBdiffBackup'
        mock_result.vigilant_url = 'https://vigliant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'AdminDBdiffBackup', 'https://vigliant/'))

    def test_check_arg_4(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.job = 'AdminDBDBCCCheck'
        mock_result.vigilant_url = 'https://vigliant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'AdminDBDBCCCheck', 'https://vigliant/'))

    def test_get_job_status_single(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'AdminDBDBCCCheck', 'https://vigliant/'),
                         (2, 'CRITICAL: Database consistency check job is not running on localhost'))

    @patch('scanline.utilities.dbutils.get_mirroring_primary_check', return_value=False)
    def test_get_job_status_both(self, primary_check):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(
            return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']])
        self.assertEqual(
            self.module.get_job_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'AdminDBDBCCCheck', 'https://vigliant/'),
            (0,
             'OK: This service check only applicable for primary node. idm04db1.idm04.isyntax.net is not the primary node.'))

    @patch('scanline.utilities.dbutils.get_dbnodes',
           return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'], ['shd03db.shd03.isyntax.net']])
    def test_get_job_status_both_exception(self, mock_db_hosts):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_job_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'AdminDBDBCCCheck', 'https://vigliant/'),
            (2, 'CRITICAL - Connection error with idm04db1.idm04.isyntax.net. '))

    def test_get_job_status_is_enabled(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchone.return_value = "Enable"
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'Maint_Backup_Diff', 'https://vigliant/'),
                         (2, 'CRITICAL: Database Maint_Backup_Diff job Failed on the node localhost'))

    def test_get_job_status_is_success(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchone.return_value = "Sucessful"
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'PhilipsDatacenter.Daily', 'https://vigliant/'),
                         (2, 'CRITICAL: Database PhilipsDatacenter Daily job is not running on localhost'))

    def test_get_job_status_is_enabled_and_is_success(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchone.return_value = "Enabled and Successful"
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'PhilipsDatacenter.Hourly', 'https://vigliant/'),
                         (0, 'OK: Database PhilipsDatacenter hourly job is successful on localhost.'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_job_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_job_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(
            std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_job_status = MagicMock(name='get_job_status')
        self.module.get_job_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(
            std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_job_status = MagicMock(name='get_job_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_job_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
