import unittest
from mock import MagicMock, patch, call
from requests.exceptions import ConnectTimeout, HTTPError, RequestException


class CheckRabbitmqAccessTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_json = MagicMock(name='json')
        self.mock_args = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_request,
            'json': self.mock_json,
            'argparse': self.mock_args,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_access
        self.module = check_rabbitmq_access

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        results = MagicMock(name='mock_result')
        results.hostname = 'localhost'
        results.port = 15672
        results.username = 'user'
        results.password = 'password'
        results.protocol = 'https'
        results.timeout = 15
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = results
        self.module.argparse.ArgumentParser = self.mock_args
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 15672, 'user', 'password', 'https', 15))

    def test_get_rabbitmq_status_ok(self):
        self.mock_request.get.return_value.status_code = 200
        state, output = self.module.get_rabbitmq_status('localhost', 15671, 'guest', 'guest', 'https', 15)
        msg = 'OK : RabbitMq Alive'
        self.assertEqual(output, msg)
        url = 'https://localhost:15671/api/aliveness-test/%2F'
        self.mock_request.get.assert_called_once_with(url, auth=('guest', 'guest'), verify=False, timeout=15)

    def test_get_rabbitmq_status_exception(self):
        self.mock_request.get.side_effect = Exception('e')
        state, output = self.module.get_rabbitmq_status('localhost', 15671, 'guest', 'guest', 'https', 15)
        msg = 'CRITICAL : Error while connecting to localhost - e'
        self.assertEqual(output, msg)
        url = 'https://localhost:15671/api/aliveness-test/%2F'
        self.mock_request.get.assert_called_once_with(url, auth=('guest', 'guest'), verify=False, timeout=15)

    def test_get_rabbitmq_status_connectionerror(self):
        self.mock_request.get.side_effect = ConnectTimeout('e')
        state, output = self.module.get_rabbitmq_status('localhost', 15671, 'guest', 'guest', 'https', 15)
        msg = 'CRITICAL : Connection Error - RabbitMQ server is not accessible'
        self.assertEqual(output, msg)
        url = 'https://localhost:15671/api/aliveness-test/%2F'
        self.mock_request.get.assert_called_once_with(url, auth=('guest', 'guest'), verify=False, timeout=15)

    def test_get_rabbitmq_status_requestexp(self):
        self.mock_request.get.side_effect = RequestException('e')
        state, output = self.module.get_rabbitmq_status('localhost', 15671, 'guest', 'guest', 'https', 15)
        msg = 'CRITICAL : Request Exception  - e'
        self.assertEqual(output, msg)
        url = 'https://localhost:15671/api/aliveness-test/%2F'
        self.mock_request.get.assert_called_once_with(url, auth=('guest', 'guest'), verify=False, timeout=15)

    def test_get_rabbitmq_status_httperror(self):
        self.mock_request.get.side_effect = HTTPError('e')
        state, output = self.module.get_rabbitmq_status('localhost', 15671, 'guest', 'guest', 'https', 15)
        msg = 'CRITICAL : HTTPError - e'
        self.assertEqual(output, msg)
        url = 'https://localhost:15671/api/aliveness-test/%2F'
        self.mock_request.get.assert_called_once_with(url, auth=('guest', 'guest'), verify=False, timeout=15)

    def test_get_rabbitmq_status_httperror_unauthorized(self):
        self.mock_request.get.side_effect = HTTPError('Unauthorized')
        state, output = self.module.get_rabbitmq_status('localhost', 15671, 'guest', 'guest', 'https', 15)
        msg = 'CRITICAL : Authentication Error - RabbitMQ Username or Password is not valid'
        self.assertEqual(output, msg)
        url = 'https://localhost:15671/api/aliveness-test/%2F'
        self.mock_request.get.assert_called_once_with(url, auth=('guest', 'guest'), verify=False, timeout=15)

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg', return_value=('localhost', 15671, 'guest', 'guest', 'https'
                                                                          , 15))
        self.module.get_rabbitmq_status = MagicMock(name='get_rabbitmq_status', return_value=(0, 'OK'))
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)
            self.module.check_arg.assert_called_once_with()
            self.module.get_rabbitmq_status.assert_called_once_with('localhost', 15671, 'guest', 'guest', 'https', 15)


if __name__ == '__main__':
    unittest.main()
