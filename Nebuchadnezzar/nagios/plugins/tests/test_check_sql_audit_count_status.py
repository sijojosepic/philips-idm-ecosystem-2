import sys
import unittest
import mock
from mock import MagicMock, patch


class CheckSqlAuditCountTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_redis = MagicMock(name='redis')
        self.mock_json = MagicMock(name='json')
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            'radis': self.mock_redis,
            'json': self.mock_json,
            'linecache': self.mock_linecache,
            'pymssql': self.mock_pymssql,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.utilities.http.HTTPRequester': self.mock_scanline.utilities.http.HTTPRequester,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_audit_count_status
        self.module = check_sql_audit_count_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_siteid(self):
        self.mock_linecache.getline.return_value = "test1"
        self.assertEqual(self.module.get_siteid(), "test1")

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.critical = '10000'
        mock_result.warning = '5000'
        mock_result.vigilanturl = 'https:/vigilant'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', '10000', '5000', 'https:/vigilant'))

    def test_get_backoffice_data(self):
        mock_site_id = MagicMock(name="site_id")
        mock_site_id.return_value = 'test1'
        self.module.get_siteid = mock_site_id
        mock_content = MagicMock(name='content')
        mock_content.content.return_value = {'a': 'b'}
        mock_http_obj = MagicMock(name='http_obj')
        mock_http_obj.suppressed_get.return_value = mock_content
        self.module.HTTPRequester = MagicMock(name='httprequester')
        self.module.HTTPRequester.return_value = mock_http_obj
        self.module.json.loads = MagicMock(name='json_load')
        self.module.json.loads.return_value = {'a': 'b'}
        response = self.module.get_backoffice_data(fact_type='host',
                                                   MODULE_URL='pma/facts/{site_id}/modules/ISP/keys/module_type',
                                                   vig_url='http://vigilant/')
        self.assertEqual(response, {'a': 'b'})

    def test_get_db_hosts_redis_connection(self):
        mock_redis_obj = MagicMock(name="redis_obj")
        mock_redis_obj.keys.return_value = ['phistate#Database']
        self.module.redis.StrictRedis = MagicMock(name='redis_keys')
        self.module.redis.StrictRedis.return_value = mock_redis_obj
        response = self.module.get_db_hosts('http://vigilant/')
        self.assertEqual(response, ['Database'])

    def test_get_db_hosts_redis_connection_none(self):
        mock_redis_obj = MagicMock(name="redis_obj")
        mock_redis_obj.keys.return_value = ['phistate#DB']
        self.module.redis.StrictRedis = MagicMock(name='redis_keys')
        self.module.redis.StrictRedis.return_value = mock_redis_obj
        facts = {'result': [
            {
                'ISP': {'module_type': 'DB'},
                'hostname': 'localhost'
            }
        ]}
        self.module.get_backoffice_data = MagicMock(name='get_backoffice_data')
        self.module.get_backoffice_data.return_value = facts
        response = self.module.get_db_hosts('http://vigilant/')
        self.assertEqual(response, ['localhost'])

    def test_get_primary_db_obj_connection(self):
        self.mock_pymssql.connect.return_value.__enter__.return_value = 'pysql_connect'
        response = self.module.get_primary_db_obj(db_hosts=['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'],
                                                  username='root',
                                                  password='something@121i', database='mydb')
        self.assertEqual(self.mock_pymssql.connect.call_count, 2)

    def test_get_primary_db_obj(self):
        self.mock_pymssql.connect.side_effect = Exception
        response = self.module.get_primary_db_obj(db_hosts=['idm04db1.idm04.isyntax.net'],
                                                  username='root',
                                                  password='something@121i', database='mydb')
        self.assertEqual(response, (False, None))

    def test_get_primary_db_case_1(self):
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.return_value = True
        mock_cursor_obj.conn.return_value = True
        mock_cursor_obj.execute.return_value = True
        mock_cursor_obj.fetchall.return_value = ['']
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_cursor_obj
        response = self.module.get_primary_db_obj(db_hosts=['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'],
                                                  username='root',
                                                  password='something@121i', database='mydb')
        self.assertEqual(response, (False, None))

    def test_get_primary_db_case_2(self):
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchall.return_value = [('OK',)]
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        response = self.module.get_primary_db_obj(db_hosts=['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'],
                                                  username='root',
                                                  password='something@121i', database='mydb')
        self.assertEqual(response, (mock_cursor, 'idm04db1.idm04.isyntax.net'))

    def test_get_aduit_count_status_CRITICAL_1(self):
        self.module.get_db_hosts = MagicMock(name="get_db_hosts")
        self.module.get_db_hosts.return_value = ['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']
        self.module.get_primary_db_obj.return_value = MagicMock(return_value=False)
        response = self.module.get_aduit_count_status(hostname='idm04db1.idm04.isyntax.net', username='root',
                                                      password='something@121i', database='mydb', critical=10000,
                                                      warning=5000)
        self.assertEqual(response, (2, 'CRITICAL - Cannot establish a connection to primary database.'))

    def test_get_aduit_count_status_CRITICAL_2(self):
        mock_db_hosts = MagicMock(name="db_hosts")
        mock_db_hosts.return_value = ['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']
        self.module.get_db_hosts = mock_db_hosts
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.fetchone.return_value = [11000]
        self.module.get_primary_db_obj = MagicMock(name="get_primary_db_obj")
        self.module.get_primary_db_obj.return_value = (mock_cursor_obj, 'idm04db1.idm04.isyntax.net')
        response = self.module.get_aduit_count_status(hostname='idm04db1.idm04.isyntax.net', username='root',
                                                      password='1nf0M@t1csph!lt0r', database='PiiMainJobService',
                                                      critical=10000,
                                                      warning=5000, vig_url='http://vigilant/')
        self.assertEqual(response, (2,
                                    'CRITICAL - The Number of audit entries is 11000, which has crossed the threshold value provided 10000. This may result in audit job failure.'))

    def test_get_aduit_count_status_CRITICAL_3(self):
        mock_db_hosts = MagicMock(name="db_hosts")
        mock_db_hosts.return_value = ['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']
        self.module.get_db_hosts = mock_db_hosts
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.fetchone.return_value = [None]
        self.module.get_primary_db_obj = MagicMock(name="get_primary_db_obj")
        self.module.get_primary_db_obj.return_value = (mock_cursor_obj, 'idm04db1.idm04.isyntax.net')
        response = self.module.get_aduit_count_status(hostname='idm04db1.idm04.isyntax.net', username='root',
                                                      password='1nf0M@t1csph!lt0r', database='PiiMainJobService',
                                                      critical=10000,
                                                      warning=5000, vig_url='http://vigilant/')
        self.assertEqual(response, (
        2, "CRITICAL - The Number of audit entries is None. Unexpected result from audit job table."))

    def test_get_aduit_count_status_Warning(self):
        mock_db_hosts = MagicMock(name="db_hosts")
        mock_db_hosts.return_value = ['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']
        self.module.get_db_hosts = mock_db_hosts
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.fetchone.return_value = [6000]
        self.module.get_primary_db_obj = MagicMock(name="get_primary_db_obj")
        self.module.get_primary_db_obj.return_value = (mock_cursor_obj, "idm04db1.idm04.isyntax.net")
        response = self.module.get_aduit_count_status(hostname='idm04db1.idm04.isyntax.net', username='root',
                                                      password='1nf0M@t1csph!lt0r', database='PiiMainJobService',
                                                      critical=10000,
                                                      warning=5000, vig_url='http://vigilant/')
        self.assertEqual(response, (1,
                                    'WARNING - The Number of audit entries is 6000, which has crossed the threshold value provided 5000.'))

    def test_get_aduit_count_status_Ok(self):
        mock_db_hosts = MagicMock(name="db_hosts")
        mock_db_hosts.return_value = ['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']
        self.module.get_db_hosts = mock_db_hosts
        mock_cursor_obj = MagicMock(name="cursor_obj")
        mock_cursor_obj.fetchone.return_value = [3000]
        self.module.get_primary_db_obj = MagicMock(name="get_primary_db_obj")
        self.module.get_primary_db_obj.return_value = (mock_cursor_obj, "idm04db1.idm04.isyntax.net")
        response = self.module.get_aduit_count_status(hostname='idm04db1.idm04.isyntax.net', username='root',
                                                      password='1nf0M@t1csph!lt0r', database='PiiMainJobService',
                                                      critical=10000,
                                                      warning=5000, vig_url='http://vigilant/')
        self.assertEqual(response, (0, 'OK - The Number of audit entries is 3000, audit job is healthy.'))

    def test_get_aduit_count_status_Ok_2(self):
        mock_db_hosts = MagicMock(name="db_hosts")
        mock_db_hosts.return_value = ['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']
        self.module.get_db_hosts = mock_db_hosts
        mock_cursor_obj = MagicMock(name="cursor_obj")
        self.module.get_primary_db_obj = MagicMock(name="get_primary_db_obj")
        self.module.get_primary_db_obj.return_value = (mock_cursor_obj, "localhost")
        response = self.module.get_aduit_count_status(hostname='idm04db1.idm04.isyntax.net', username='root',
                                                      password='1nf0M@t1csph!lt0r', database='PiiMainJobService',
                                                      critical=10000,
                                                      warning=5000, vig_url='http://vigilant/')
        self.assertEqual(response, (
        0, 'OK - Audit job is only applicable for Primary Node. idm04db1.idm04.isyntax.net is not the primary node.'))

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('arg1', 'arg2')
        self.module.get_aduit_count_status = MagicMock(name='get_audit_count_status')
        self.module.get_aduit_count_status.return_value = (0, 'Ok')
        self.module.exit = MagicMock(name='exit')
        self.module.exit.return_value = True
        self.module.main()
        self.module.get_aduit_count_status.assert_called_once_with('arg1', 'arg2')

    def test_main_case_2(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('arg1', 'arg2')
        self.module.get_aduit_count_status = MagicMock(name='get_audit_count_status')
        self.module.get_aduit_count_status.side_effect = ValueError
        self.module.exit = MagicMock(name='exit')
        response = self.module.main()
        self.module.exit.assert_called_once_with(2)


if __name__ == '__main__':
    unittest.main()
