from StringIO import StringIO
import unittest
from mock import MagicMock, patch, Mock


class CheckSqlAlwaysOnAvailabilityTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dbutils': self.mock_scanline.utilities.dbutils
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_always_on_availability_status
        self.module = check_sql_always_on_availability_status

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.vigilant_url = 'https://vigilant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'https://vigilant/'))

    def test_get_availability_status_1(self):
        self.module.get_formatted_data = MagicMock(return_value="mocked")
        self.assertEqual(self.module.get_availability_status('localhost', 'test1', 'pass1', 'db1', 'https://vigilant/'),
                         'mocked')

    def test_get_availability_status_2(self):
        self.module.get_formatted_data = MagicMock(return_value="Mocked")
        self.module.get_availability_status('localhost', 'test1', 'pass1', 'db1', 'https://vigilant/')
        self.assertEqual(self.mock_pymssql.connect.call_count, 1)

    @patch('scanline.utilities.dbutils.get_dbnodes',
           return_value=[['zvm01db1.zvm01.isyntax.net', 'zvm01db2.zvm01.isyntax.net']])
    def test_get_availability_status_3(self, db_nodes):
        self.module.get_formatted_data = MagicMock(return_value="mocked")
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(self.module.get_availability_status('zvm01db1.zvm01.isyntax.net', 'test1', 'pass1', 'db1',
                                                             'https://vigilant/'), 'mocked')

    def test_get_formatted_data_1(self):
        results = [[('DB1', 'PRIMARY', 'ONLINE', 'CONNECTED'), ('DB2', 'SECONDARY', 'ONLINE', 'CONNECTED')]]
        self.assertEqual(self.module.get_formatted_data(results, ''),
                         (0, 'OK: All the databases are available. Primary up on DB1.'))

    def test_get_formatted_data_2(self):
        results = [[('DB1', 'RESOLVING', 'ONLINE', 'CONNECTED'), ('DB2', 'RESOLVING', 'ONLINE', 'CONNECTED')]]
        self.assertEqual(self.module.get_formatted_data(results, ''), (
            2,
            'CRITICAL: Database with resolving state detected, this could cause potentially database unavailability.'))

    def test_get_formatted_data_3(self):
        results = [[('DB1', 'PRIMARY', 'ONLINE', 'CONNECTED'), ('DB2', 'SECONDARY', 'OFFLINE', 'DISCONNECTED')]]
        self.assertEqual(self.module.get_formatted_data(results, ''),
                         (1, 'WARNING: Database disconnection identified on DB2. Primary up on DB1.'))

    def test_get_formatted_data_4(self):
        results = [[('DB1', 'PRIMARY', 'OFFLINE', 'CONNECTED'), ('DB2', 'SECONDARY', 'OFFLINE', 'DISCONNECTED')]]
        self.assertEqual(self.module.get_formatted_data(results, ''),
                         (2, 'CRITICAL: Database Replica unexpected dropdown or not available or disconnected.'))

    def test_get_formatted_data_5(self):
        self.assertEqual(self.module.get_formatted_data('', ['DB1', 'DB2']), (2,
                                                                              'CRITICAL: Cannot connect to database due to an incorrect username/password, or service check could not resolve db nodes.'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_availability_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_availability_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_availability_status = MagicMock(name='check_arg')
        self.module.get_availability_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_availability_status = MagicMock(name='get_availability_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_availability_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
