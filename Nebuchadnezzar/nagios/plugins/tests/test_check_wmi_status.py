import unittest
from mock import MagicMock, patch
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)


class CPUStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_wmi_status
        self.module = check_wmi_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user', 'pass'))

    def test_wmi_status_ok(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = """Windows 2008"""
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 0)
        test_msg = "OK : " + "Windows 2008"
        self.assertEqual(msg, test_msg)

    def test_wmi_check_crit(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = ''
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = "CRITICAL : The WMI query had problems. WMI repository might be corrupted."
        self.assertEqual(msg, test_msg)

    def test_wmi_check_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Exception {0}'.format('e')
        self.assertEqual(msg, test_msg)

    def test_wmi_check_authentication_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('AuthenticationError')
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = "CRITICAL : Authentication Error. You might have your username/password wrong or the user's " \
                   "access level is too low"
        self.assertEqual(msg, test_msg)

    def test_wmi_check_winrm_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('WinRMError')
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMError')
        self.assertEqual(msg, test_msg)

    def test_wmi_check_typeerror(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Typeerror {0}'.format('e')
        self.assertEqual(msg, test_msg)

    def test_wmi_check_type_error1(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('find() takes exactly 2')
        status, msg = self.module.wmi_status('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Issue in connecting to node - {0}'.format('localhost')
        self.assertEqual(msg, test_msg)

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('localhost','user', 'pass')
        self.module.wmi_status = MagicMock(name='wmi_status')
        self.module.wmi_status.return_value = (0, 'OK')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
