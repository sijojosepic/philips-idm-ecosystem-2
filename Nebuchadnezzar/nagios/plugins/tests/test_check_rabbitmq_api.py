import unittest
from mock import MagicMock, patch, PropertyMock, call

class BaseTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_scanline = MagicMock(name='scanline')
        self.datetime = MagicMock(name='datetime')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        self.mock_json = MagicMock(name='json')
        self.mock_requests = MagicMock(name='requests')
        self.mock_ast = MagicMock(name='ast')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.product': self.mock_scanline.product,
            'scanline.product.isp': self.mock_scanline.product.isp,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.back_office': self.mock_scanline.back_office,
            'scanline.utilities.cache': self.mock_scanline.cache,
            'argparse': self.mock_argparse,
            'json': self.mock_json,
            'requests': self.mock_requests,
            'requests.exceptions': self.mock_requests.exceptions,
            'ast': self.mock_ast,
            'sys': self.mock_sys
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_api
        self.module = check_rabbitmq_api

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


class CacheTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        self.cobj = self.module.Cache()

    def test_read_cache(self):
        self.assertEqual(self.cobj.read_cache('abc'),
                         self.mock_scanline.cache.SafeCache.do.return_value)
        self.mock_scanline.cache.SafeCache.do.assert_called_once_with(
            'get', 'abc')

    def test_write_cache(self):
        self.cobj.write_cache('k', 'v', '50')
        self.mock_scanline.cache.SafeCache.do.assert_called_once_with(
            'setex', 'k', 'v', '50')


class HostsInfoTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        self.hobj = self.module.HostsInfo('s1', 'vurl', 'ser')
        self.hobj.cache = MagicMock()

    def test_attr(self):
        HOSTS_LEN_KEY = 'hosts_len'
        CACHE_TTL = 60 * 60
        self.assertEqual(HOSTS_LEN_KEY, self.module.HostsInfo.HOSTS_LEN_KEY)
        self.assertEqual(CACHE_TTL, self.module.HostsInfo.CACHE_TTL)

    def test_bo(self):
        BO = self.mock_scanline.back_office.BackOffice
        self.assertEqual(self.hobj.bo, BO.return_value)
        BO.assert_called_once_with('vurl', 's1')

    def test_isp_module_with_isite_server(self):
        ISPP = self.mock_scanline.product.isp.ISPProductScanner
        self.assertEqual(self.hobj.isp_module, ISPP.create_isp.return_value)
        ISPP.create_isp.assert_called_once_with('ser')

    def test_isp_module_no_isite_server(self):
        self.hobj.isite_server = None
        ISPP = self.mock_scanline.product.isp.ISPProductScanner
        self.assertEqual(self.hobj.isp_module, None)
        self.assertEqual(ISPP.create_isp.call_count, 0)

    def test_from_pacs_when_isp_module(self):
        with patch('check_rabbitmq_api.HostsInfo.isp_module', new_callable=PropertyMock) as mk_isp_mod:
            mk_isp_mod.return_value.get_hosts.return_value = ['h1', 'h2', 'h3']
            self.assertEqual(self.hobj.from_pacs(), 3)

    def test_from_pacs_when_not_isp_module(self):
        with patch('check_rabbitmq_api.HostsInfo.isp_module', new_callable=PropertyMock) as mk_isp_mod:
            mk_isp_mod.return_value = None
            self.assertEqual(self.hobj.from_pacs(), None)

    def test_back_office_when_response(self):
        ISPDB = self.mock_scanline.product.isp.ISPDBConfiguration
        ISPDB.EXTENDEDNODES = ('pqr')
        with patch('check_rabbitmq_api.HostsInfo.bo', new_callable=PropertyMock) as mk_bo:
            resp_mock = MagicMock()
            result = [{'hostname': 'host_should_come',
                       'ISP': {'module_type': 'abc'}}]
            result.append({'hostname': 'empty_mod',
                           'ISP': {'module_type': ''}})
            result.append({'hostname': 'excludedhost',
                           'ISP': {'module_type': 'pqr'}})
            resp_mock.json.return_value = {'result': result}
            mk_bo.return_value.hosts_with_mod_type.return_value = resp_mock
            self.assertEqual(self.hobj.from_backoffice(), 1)
            mk_bo.return_value.hosts_with_mod_type.assert_called_once_with()
            resp_mock.json.assert_called_once_with()

    def test_bac_office_no_response(self):
        with patch('check_rabbitmq_api.HostsInfo.bo', new_callable=PropertyMock) as mk_bo:
            mk_bo.return_value.hosts_with_mod_type.return_value = None
            self.assertEqual(self.hobj.from_backoffice(), 0)

    def test_from_cache_when_value_in_cache(self):
        self.hobj.cache.read_cache.return_value = '12'
        self.assertEqual(self.hobj.from_cache('key'), 12)
        self.hobj.cache.read_cache.assert_called_once_with('key')

    def test_from_cache_when_value_not_in_cache(self):
        self.hobj.cache.read_cache.return_value = ''
        self.assertEqual(self.hobj.from_cache('key'), 0)
        self.hobj.cache.read_cache.assert_called_once_with('key')

    def test_get_hosts_len_from_cache(self):
        self.hobj.from_cache = MagicMock()
        self.hobj.from_pacs = MagicMock()
        self.hobj.from_backoffice = MagicMock()
        self.assertEqual(self.hobj.get_hosts_len(),
                         self.hobj.from_cache.return_value)
        self.hobj.from_cache.assert_called_once_with(self.hobj.HOSTS_LEN_KEY)
        self.assertEqual(self.hobj.from_pacs.call_count, 0)
        self.assertEqual(self.hobj.from_backoffice.call_count, 0)

    def test_get_hosts_len_from_pacs(self):
        self.hobj.CACHE_TTL = 'cache'
        self.hobj.HOSTS_LEN_KEY = 'hst1'
        self.hobj.from_cache = MagicMock()
        self.hobj.from_cache.return_value = None
        self.hobj.from_pacs = MagicMock()
        self.hobj.from_backoffice = MagicMock()
        self.assertEqual(self.hobj.get_hosts_len(),
                         self.hobj.from_pacs.return_value)
        self.hobj.cache.write_cache('hst1',
                                    self.hobj.from_pacs.return_value,
                                    'cache')
        self.hobj.from_cache.assert_called_once_with('hst1')
        self.hobj.from_pacs.assert_called_once_with()
        self.assertEqual(self.hobj.from_backoffice.call_count, 0)

    def test_get_hosts_len_from_back_office(self):
        self.hobj.CACHE_TTL = 'cache'
        self.hobj.HOSTS_LEN_KEY = 'hst1'
        self.hobj.from_cache = MagicMock()
        self.hobj.from_cache.return_value = None
        self.hobj.from_pacs = MagicMock()
        self.hobj.from_pacs.return_value = None
        self.hobj.from_backoffice = MagicMock()
        self.assertEqual(self.hobj.get_hosts_len(),
                         self.hobj.from_backoffice.return_value)
        self.hobj.cache.write_cache('hst1',
                                    self.hobj.from_pacs.return_value,
                                    'cache')
        self.hobj.from_cache.assert_called_once_with('hst1')
        self.hobj.from_pacs.assert_called_once_with()
        self.hobj.from_backoffice.assert_called_once_with()

    def test_number_of_core_nodes(self):
        self.hobj.get_hosts_len = MagicMock()
        self.assertEqual(self.hobj.num_core_nodes,
                         self.hobj.get_hosts_len.return_value)


class QueueStatusTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        self.m1 = MagicMock()
        self.m2 = MagicMock()
        self.qobj = self.module.QueueStatus([self.m1, self.m2],
                                            '10', '20', 'dqueus',
                                            'cqueues')

    def test_QueueStatus_attrs(self):
        status_dct = {'CRITICAL': [], 'WARNING': []}
        mapping = {}
        sts_set = set([0])
        warning, critical = 10, 20
        self.assertEqual(self.qobj.status_dct, status_dct)
        self.assertEqual(self.qobj.mapping, mapping)
        self.assertEqual(self.qobj.sts_set, sts_set)
        self.assertEqual(self.qobj.warning, warning)
        self.assertEqual(self.qobj.critical, critical)

    def test_format_queue_when_list_type(self):
        self.mock_ast.literal_eval.return_value = ['a', 'b']
        self.assertEqual(self.qobj.format_queue('abc'), ['a', 'b'])

    def test_format_queue_when_nost_list_type(self):
        self.assertEqual(self.qobj.format_queue('abc'), [])

    def test_form_queue_mapping(self):
        self.qobj.format_queue = MagicMock()
        self.qobj.format_queue.side_effect = [
            ['Q1,34,50', 'Q2,34,50'], ['Q3,343,504', 'Q2,340,500']]
        exp_mapping = {'Q1': {'WAR': 34, 'CRT': 50},
                       'Q3': {'WAR': 343, 'CRT': 504},
                       'Q2': {'WAR': 340, 'CRT': 500}}
        self.qobj.form_queue_mapping()
        self.assertEqual(self.qobj.mapping, exp_mapping)
        self.assertEqual(self.qobj.format_queue.call_args_list,
                         [call('dqueus'), call('cqueues')])

    def test_thresholds_when_queue_in_mapping(self):
        self.qobj.mapping = {'Q1': {'WAR': 34, 'CRT': 50}}
        self.assertEqual(self.qobj.thresholds('Q1'), (34, 50))
        # Default Thresholds
        self.assertEqual(self.qobj.thresholds('Q2'), (10, 20))

    def test_compose_status(self):
        q1 = {'name': 'DEF_Q_CRIT', 'messages': 20}
        q2 = {'name': 'DEF_Q_WARN', 'messages': 10}
        q3 = {'name': 'EXISTING_Q_CRIT', 'messages': 75}
        q4 = {'name': 'EXISTING_Q_WARN', 'messages': 312}
        q5 = {'name': 'empyt_q', 'messages': ''}
        self.qobj.api_response = [q1, q2, q3, q4, q5]
        self.qobj.mapping = {'EXISTING_Q_CRIT': {'WAR': 50, 'CRT': 60},
                             'EXISTING_Q_WARN': {'WAR': 300, 'CRT': 350},
                             'Q2': {'WAR': 340, 'CRT': 500}}
        self.qobj.compose_status()

        exp_resp = {'CRITICAL': ['Messages in DEF_Q_CRIT queue has breached the critical threshold (actual - 20, Thresholds: warning-10, critical-20),\n',
                                 'Messages in EXISTING_Q_CRIT queue has breached the critical threshold (actual - 75, Thresholds: warning-50, critical-60),\n'],
                    'WARNING': ['Messages in DEF_Q_WARN queue has breached the warning threshold (actual - 10, Thresholds: warning-10, critical-20),\n',
                                'Messages in EXISTING_Q_WARN queue has breached the warning threshold (actual - 312, Thresholds: warning-300, critical-350),\n']}
        self.assertEqual(self.qobj.status_dct, exp_resp)

    def test_st_msg_when_final_msg(self):
        self.assertEqual(self.qobj.st_msg('c1', 'c2'), 'c1c2')

    def test_st_msg_when_q_len_0(self):
        self.qobj.q_len = 0
        self.assertEqual(self.qobj.st_msg('', ''), self.qobj.Msgs.NO_Q)

    def test_st_msg_when_q_len_1(self):
        self.qobj.q_len = 1
        self.assertEqual(self.qobj.st_msg('', ''), self.qobj.Msgs.ONE_Q)

    def test_st_msg_when_q_len_more_than_1(self):
        self.qobj.q_len = 10
        ok_msg = 'OK :: Found 10 Queues, None of them has breached the threshold defined for number of messages'
        self.assertEqual(self.qobj.st_msg('', ''), ok_msg)

    def test_get_status_when_warning(self):
        self.qobj.compose_status = MagicMock()
        self.qobj.st_msg = MagicMock()
        self.assertEqual(self.qobj.get_status(), (0, self.qobj.st_msg.return_value))
        self.qobj.compose_status.assert_called_once_with()

    def test_get_status_when_critical(self):
        self.qobj.compose_status = MagicMock()
        self.qobj.st_msg = MagicMock()
        self.qobj.status_dct = {'WARNING': [], 'CRITICAL': ['critical']}
        self.assertEqual(self.qobj.get_status(), (2, self.qobj.st_msg.return_value))
        self.qobj.compose_status.assert_called_once_with()

    def test_get_status_when_warning2(self):
        self.qobj.compose_status = MagicMock()
        self.qobj.st_msg = MagicMock()
        self.qobj.status_dct = {'CRITICAL': [], 'WARNING': ['warning']}
        self.assertEqual(self.qobj.get_status(), (1, self.qobj.st_msg.return_value))
        self.qobj.compose_status.assert_called_once_with()


class QueueMsgsTestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)
        self.cls = self.module.QueueMsgs

    def test_QueueMsgs_attrs(self):
        CRITICAL = 'Messages in {0} queue has breached the critical threshold (actual - {1}, Thresholds: ' \
                   'warning-{2}, critical-{3}),\n'
        WARNING = 'Messages in {0} queue has breached the warning threshold (actual - {1}, Thresholds: warning-{2}, ' \
                  'critical-{3}),\n'
        ONE_Q = 'OK :: Found one Queue, which has not breached the threshold defined for number of messages'
        NO_Q = 'No Queues Found'
        OK = 'OK :: Found {0} Queues, None of them has breached the threshold defined for number of messages'
        C_PRFX = 'CRITICAL:: '
        W_PRFX = 'WARNING:: '
        self.assertEqual(self.cls.CRITICAL, CRITICAL)
        self.assertEqual(self.cls.WARNING, WARNING)
        self.assertEqual(self.cls.ONE_Q, ONE_Q)
        self.assertEqual(self.cls.NO_Q, NO_Q)
        self.assertEqual(self.cls.OK, OK)
        self.assertEqual(self.cls.C_PRFX, C_PRFX)
        self.assertEqual(self.cls.W_PRFX, W_PRFX)


class RabbitMQAPITestCase(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)

    def test_check_arg(self):
        results = MagicMock(name='mock_result')
        results.site_id = 'IDM06'
        results.vig_url = 'https//vigilanturl/'
        results.isite_address = '10.10.10.10'
        results.hostname = 'localhost'
        results.port = 15672
        results.username = 'user'
        results.password = 'password'
        results.protocol = 'https'
        results.api = 'api'
        results.req_timeout = 15
        results.warning = 95
        results.critical = 100
        results.nodes = 10
        results.default_queue = ''
        results.queues = ''
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = results
        self.module.argparse.ArgumentParser = self.mock_argparse
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('IDM06', 'https//vigilanturl/', '10.10.10.10', 'localhost', 15672, 'user',
                                    'password', 'https', 'api', 15, 95, 100, 10, '', ''))

    def test_get_url(self):
        output = self.module.get_url("localhost", 15672, 'https', 'queues')
        msg = 'https://localhost:15672/api/queues'
        self.assertEqual(output, msg)

    def test_get_api_response(self):
        mock_response = MagicMock(name='response')
        mock_response.content = "{'key': 'data'}"
        self.mock_requests.get.return_value = mock_response
        self.mock_json.loads.return_value = {'json': 'data'}
        output = self.module.get_api_response("url", 'user', 'pass', 15)
        self.assertEqual(output, {'json': 'data'})
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'connection': 'keep-alive',
            'content-type': 'application/json',
            'upgrade-insecure-requests': '1',
        }
        self.mock_requests.get.assert_called_once_with('url', headers=headers, auth=('user', 'pass'), timeout=15,
                                                       verify=False)
        mock_response.raise_for_status.assert_called_once_with()
        self.mock_json.loads.assert_called_once_with("{'key': 'data'}")

    def test_do_request(self):
        self.module.get_url = MagicMock(name='get_url', return_value='url')
        self.module.get_api_response = MagicMock(name='get_api_response', return_value='json')
        output = self.module.do_request('host', 'subpath', 15671, 'https', 'user', 'pass', 10)
        self.assertEqual(output, 'json')
        self.module.get_url.assert_called_once_with('host', 15671, 'https', 'subpath')
        self.module.get_api_response.assert_called_once_with('url', 'user', 'pass', 10)

    def test_get_threshold_values(self):
        self.assertEqual(['1', '2', '3', '4'], self.module.get_threshold_values('1,2,3,4'))

    def test_get_nodes_threshold(self):
        self.assertEqual(['1', '2', '3', '4'], self.module.get_nodes_threshold('1,2,3,4'))

    def test_get_status_data(self):
        self.module.do_request = MagicMock(name='do_request', return_value='json')
        self.assertEqual('json', self.module.get_status_data('host', 'api', 15672, 'https', 'user', 'pass', 10))
        self.module.do_request.assert_called_once_with('host', 'api', 15672, 'https', 'user', 'pass', 10)

    def test_check_critical_threshold(self):
        output = self.module.check_critical_threshold(10, 5, {'critical_msg': 'CRITICAL {0} {1}'})
        self.assertEqual(output, 'CRITICAL 10 5')

    def test_check_warning_threshold(self):
        output = self.module.check_warning_threshold(7, 5, 10, {'warning_msg': 'WARNING {0} {1}'})
        self.assertEqual(output, 'WARNING 7 5')

    def test_get_overview_status_ok(self):
        response = {'object_totals': {'connections': 5, 'channels': 4, 'consumers': 5},
                    'queue_totals': {'messages_unacknowledged': 0}}
        overview_list = [100, 100, 100, 100]
        self.module.get_threshold_values = MagicMock(name='get_threshold_values', return_value=overview_list)
        self.module.check_critical_threshold = MagicMock(name='check_critical_threshold', return_value='')
        self.module.check_warning_threshold = MagicMock(name='check_warning_threshold', return_value='')
        output_msg = 'OK: Total number of connections-5, channels-4, consumers-5, unacknowledged messages-0 ' \
                     'and the number of nodes used for threshold calculation - 10'
        self.assertEqual(self.module.get_overview_status(response, 10, 20, 10), (0, output_msg))

    def test_get_overview_status_warning(self):
        response = {'object_totals': {'connections': 150, 'channels': 4, 'consumers': 5},
                    'queue_totals': {'messages_unacknowledged': 0}}
        overview_warn = '10,10,10,10'
        overview_critical = '20,20,20,20'
        output_msg = 'WARNING : Warning threshold breached for total number of connection.' \
                     '(Total Connections-150, Threshold-100)\n'
        self.assertEqual(self.module.get_overview_status(response, overview_warn, overview_critical, 10),
                         (1, output_msg))

    def test_get_overview_status_critical(self):
        response = {'object_totals': {'connections': 150, 'channels': 4, 'consumers': 5},
                    'queue_totals': {'messages_unacknowledged': 0}}
        overview_warn = '5,5,5,5'
        overview_critical = '10,10,10,10'
        output_msg = 'CRITICAL : Critical threshold breached for total number of connection.' \
                     '(Total Connections-150, Threshold-100)\n'
        self.assertEqual(self.module.get_overview_status(response, overview_warn, overview_critical, 10),
                         (2, output_msg))

    def test_convert_size_0(self):
        self.assertEqual(self.module.convert_size(0), "0B")

    def test_convert_size_MB(self):
        self.assertEqual(self.module.convert_size(1500000), '1.43 MB')

    def test_convert_size_error(self):
        self.assertEqual(self.module.convert_size(-1), '0B')

    def test_check_status_overview(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        mock_obj = MagicMock(name='mock_obj')
        mock_obj.num_core_nodes = 10
        self.module.HostsInfo = MagicMock(name='HostsInfo', return_value=mock_obj)
        self.module.get_overview_status = MagicMock(name='get_overview_status', return_value=(0, 'OK'))
        self.assertEqual((0, 'OK'), self.module.check_status('idm06', 'vig_url', '1.1.1.1', 'host', 'port', 'user',
                                                             'pass', 'https', 'overview', 10, '', '', 10, '', ''))
        self.module.get_overview_status.assert_called_once_with('json', '', '', 20)
        self.module.get_status_data.assert_called_once_with('host', 'overview', 'port', 'https', 'user', 'pass', 10)

    def test_check_status_overview_critical(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        mock_obj = MagicMock(name='mock_obj')
        mock_obj.num_core_nodes = ''
        self.module.HostsInfo = MagicMock(name='HostsInfo', return_value=mock_obj)
        self.assertEqual((2, 'Unable to retrieve UDM core nodes information'),
                         self.module.check_status('idm06', 'vig_url', '1.1.1.1', 'host', 'port', 'user', 'pass',
                                                  'https', 'overview', 10, '', '', 10, '', ''))
        self.module.get_status_data.assert_called_once_with('host', 'overview', 'port', 'https', 'user', 'pass', 10)

    def test_get_disk_free_response(self):
        self.module.convert_size = MagicMock(name='convert_size', return_value='5GB')
        data = {'key': 5, 'name': 'abc'}
        msg = "Critical threshold breached for disk free space-node: abc(Total-5GB, Threshold-1GB)\n"
        self.assertEqual((True, False, msg), self.module.get_disk_free_response(data, 'key', 1, 2))

    def test_get_disk_free_response_warning(self):
        self.module.convert_size = MagicMock(name='convert_size', return_value='10GB')
        data = {'key': 5, 'name': 'abc'}
        msg = "Warning threshold breached for disk free space-node: abc(Total-10GB, Threshold-1GB)\n"
        self.assertEqual((False, True, msg), self.module.get_disk_free_response(data, 'key', 0, 1))

    def test_get_nodes_status_critical(self):
        self.module.get_nodes_details = MagicMock(name='get_nodes_details', return_value=('CRITICAL', '', 'CRITICAL'))
        self.assertEqual((2, 'CRITICAL'), self.module.get_nodes_status('json', 'warning', 'critical'))
        self.module.get_nodes_details.assert_called_once_with('json', 'warning', 'critical')

    def test_get_memory_used_response(self):
        data = {'name': 'abc', 'key': 10, 'mem_limit': 50}
        msg = "Critical threshold breached for memory used-node: abc, (Total-20.0%, Threshold-1%)\n"
        self.assertEqual((True, False, msg), self.module.get_memory_used_response(data, 'key', 1, 0))

    def test_get_memory_used_response_warning(self):
        data = {'name': 'abc', 'key': 10, 'mem_limit': 50}
        msg = "Warning threshold breached for memory used-node: abc(Total-20.0%, Threshold-10%)\n"
        self.assertEqual((False, True, msg), self.module.get_memory_used_response(data, 'key', 50, 10))

    def test_get_fd_used_response(self):
        data = {'name': 'abc', 'key': 10}
        msg = "Critical threshold breached for file descriptors used-node: abc(Total-10, Threshold-5)\n"
        self.assertEqual(self.module.get_fd_used_response(data, 'key', 5, 2), (True, False, msg))

    def test_get_fd_used_response_warning(self):
        data = {'name': 'abc', 'key': 10}
        msg = "Warning threshold breached for file descriptors used-node: abc(Total-10, Threshold-9)\n"
        self.assertEqual(self.module.get_fd_used_response(data, 'key', 20, 9), (False, True, msg))

    def test_get_alarm_status(self):
        data = {'mem_alarm': True, 'name': 'abc'}
        msg = "Critical: Memory alarm is true for node: abc\n"
        self.assertEqual(msg, self.module.get_alarm_status(data))

    def test_get_alarm_status_disk(self):
        data = {'disk_free_alarm': True, 'name': 'abc'}
        msg = "Critical: Disk Alarm is true for node: abc\n"
        self.assertEqual(msg, self.module.get_alarm_status(data))

    def test_get_alarm_status_partition(self):
        data = {'partitions': ['node', 'node1'], 'name': 'abc'}
        msg = "Critical: Partition detected for nodes: abc and partitions: ['node', 'node1']\n"
        self.assertEqual(msg, self.module.get_alarm_status(data))

    def test_get_nodes_result(self):
        self.module.get_nodes_threshold = MagicMock(name='get_nodes_threshold', return_value=[10, 10, 10])
        self.module.get_memory_used_response = MagicMock(name='get_memory_used_response',
                                                         return_value=(True, False, ''))
        self.module.get_disk_free_response = MagicMock(name='get_disk_free_response', return_value=(True, False, ''))
        self.module.get_fd_used_response = MagicMock(name='get_fd_used_response', return_value=(True, False, ''))
        self.module.get_alarm_status = MagicMock(name='get_alarm_status', return_value='yes')
        self.assertEqual((True, False, 'yes'), self.module.get_nodes_result('data', 'warn', 'crit'))

    def test_get_nodes_status_warning(self):
        self.module.get_nodes_details = MagicMock(name='get_nodes_details', return_value=('', 'WARNING', 'WARNING'))
        self.assertEqual((1, 'WARNING'), self.module.get_nodes_status('json', 'warning', 'critical'))
        self.module.get_nodes_details.assert_called_once_with('json', 'warning', 'critical')

    def test_get_nodes_details(self):
        config_json = [{}, {'mem_used': 10, 'disk_free': 10, 'fd_used': 10}]
        self.module.get_nodes_result = MagicMock(name='get_nodes_result', return_value=(True, False, 'msg'))
        self.assertEqual((True, False, 'msg'), self.module.get_nodes_details(config_json, 'warn', 'criti'))

    def test_get_nodes_details_warning(self):
        config_json = [{'mem_used': 10, 'disk_free': 10, 'fd_used': 10}]
        self.module.get_nodes_result = MagicMock(name='get_nodes_result', return_value=(False, True, 'msg'))
        self.assertEqual((False, True, 'msg'), self.module.get_nodes_details(config_json, 'warn', 'criti'))

    def test_get_nodes_details_ok(self):
        config_json = [{'mem_used': 10, 'disk_free': 10, 'fd_used': 10}]
        self.module.get_nodes_result = MagicMock(name='get_nodes_result', return_value=(False, False, 'msg'))
        self.assertEqual((False, False, "All Nodes are OK state"),
                         self.module.get_nodes_details(config_json, 'warn', 'criti'))

    def test_get_nodes_status_ok(self):
        self.module.get_nodes_details = MagicMock(name='get_nodes_details', return_value=('', '', 'OK'))
        self.assertEqual((0, 'OK'), self.module.get_nodes_status('json', 'warning', 'critical'))
        self.module.get_nodes_details.assert_called_once_with('json', 'warning', 'critical')

    def test_check_status_queues(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        mock_obj = MagicMock(name='mock_obj')
        mock_obj.get_status.return_value = (0, 'OK')
        self.module.QueueStatus = MagicMock(name='QueueStatus', return_value=mock_obj)
        self.assertEqual((0, 'OK'),
                         self.module.check_status('idm06', 'vig_url', '1.1.1.1', 'host', 'port', 'user', 'pass',
                                                  'https', 'queues', 10, '', '', 10, '', ''))
        self.module.get_status_data.assert_called_once_with('host', 'queues', 'port', 'https', 'user', 'pass', 10)

    def test_check_status_nodes(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_nodes_status = MagicMock(name='get_nodes_status', return_value=(0, 'OK'))
        self.assertEqual((0, 'OK'),
                         self.module.check_status('idm06', 'vig_url', '1.1.1.1', 'host', 'port', 'user', 'pass',
                                                  'https', 'nodes', 10, '', '', 10, '', ''))
        self.module.get_status_data.assert_called_once_with('host', 'nodes', 'port', 'https', 'user', 'pass', 10)
        self.module.get_nodes_status.assert_called_once_with('json', '', '')

    def test_check_status_unknown(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.assertEqual((3, 'UNKNOWN API - abc'),
                         self.module.check_status('idm06', 'vig_url', '1.1.1.1', 'host', 'port', 'user', 'pass',
                                                  'https', 'abc', 10, '', '', 10, '', ''))
        self.module.get_status_data.assert_called_once_with('host', 'abc', 'port', 'https', 'user', 'pass', 10)

    def test_check_status_httperror(self):
        self.module.get_status_data = MagicMock(name='get_status_data', side_effect=Exception('e'))
        self.assertEqual((2, 'CRITICAL : Error while retrieving response data e'),
                         self.module.check_status('idm06', 'vig_url', '1.1.1.1', 'host', 'port', 'user', 'pass',
                                                  'https', 'abc', 10, '', '', 10, '', ''))
        self.module.get_status_data.assert_called_once_with('host', 'abc', 'port', 'https', 'user', 'pass', 10)

    def test_main(self):
        self.module.check_args = MagicMock(name='check_arg', return_value=())
        self.module.check_status = MagicMock(name='check_status', return_value=(0, 'OK'))
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
