import unittest
from mock import MagicMock, patch


class CheckJSONInformationTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_jsonpath_rw = MagicMock(name='jsonpath_rw')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_requests,
            'jsonpath_rw': self.mock_jsonpath_rw,
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_json_status
        self.module = check_json_status

        self.module.get_json = MagicMock(name='get_json')
        self.module.get_query = MagicMock(name='get_query')

        self.module.get_query.return_value.find.return_value = [
            MagicMock(value={'Status': 'OK', 'Message': 'everything is extremely well', 'ApplicationName': 'Happy.App'})
        ]
        self.find_return_value = self.module.get_query.return_value.find.return_value

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        obj = MagicMock()
        obj2 = MagicMock()
        
        obj2.url = 'u'
        obj2.query = 'h'
        obj2.state_key = 'ha'
        obj2.message_key = 'd'
        obj2.label_key = 'n'
        obj2.insecure = 'i'
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        obj.parse_args.return_value = obj2
        self.assertEqual(self.module.check_arg(),('u','h','ha','d','n','i'))

    def test_check_json_status_one_value(self):
        # to get a mock object with a value field
        out = self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False)
        self.assertEqual(out, (0, '[OK] Happy.App: everything is extremely well'))

    def test_check_json_status_one_value_no_message_label(self):
        # to get a mock object with a value field
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', '', '', False),
            (0, '[OK] : ')
        )

    def test_check_json_status_two_values(self):
        # to get a mock object with a value field
        self.find_return_value.append(
            MagicMock(value={'Status': 'OK', 'Message': 'no error', 'ApplicationName': 'NotErroredApp'})
        )
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (0, '[OK] Happy.App: everything is extremely well; [OK] NotErroredApp: no error')
        )

    def test_check_json_status_ok_critical_and_unk(self):
        # to get a mock object with a value field
        self.find_return_value.append(
            MagicMock(value={'Status': 'CRITICAL', 'Message': 'error', 'ApplicationName': 'ErroredApp'})
        )
        self.find_return_value.append(
            MagicMock(value={'Status': 'UNKNOWN', 'Message': 'who am I?', 'ApplicationName': 'SentientApp'})
        )
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (
                2,
                '[OK] Happy.App: everything is extremely well; '
                '[CRITICAL] ErroredApp: error; '
                '[UNKNOWN] SentientApp: who am I?'
            )
        )

    def test_check_json_status_warn_ok(self):
        # to get a mock object with a value field
        self.find_return_value.append(
            MagicMock(value={'Status': 'warning', 'Message': 'maybe error', 'ApplicationName': 'MaybeErroredApp'})
        )
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (1, '[OK] Happy.App: everything is extremely well; [WARNING] MaybeErroredApp: maybe error')
        )

    def test_check_json_status_missing_state(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'Message': 'maybe error', 'ApplicationName': 'MaybeErroredApp'}))
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (3, '[OK] Happy.App: everything is extremely well; [UNKNOWN] MaybeErroredApp: maybe error')
        )

    def test_check_json_status_missing_message_specified(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'Status': 'OK', 'ApplicationName': 'MaybeErroredApp'}))
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (3, '[OK] Happy.App: everything is extremely well; [OK] : missing key')
        )

    def test_check_json_status_missing_message_specified_state_critical(self):
        # to get a mock object with a value field
        self.find_return_value.append(MagicMock(value={'Status': 'critical', 'ApplicationName': 'MaybeErroredApp'}))
        self.assertEqual(
            self.module.check_json_status('nourl', '', 'Status', 'Message', 'ApplicationName', False),
            (2, '[OK] Happy.App: everything is extremely well; [CRITICAL] : missing key')
        )

class CheckJSONInformationTests(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        self.mock_jsonpath_rw = MagicMock(name='jsonpath_rw')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'requests': self.mock_requests,
            'jsonpath_rw': self.mock_jsonpath_rw,
            'argparse': self.mock_argparse,
            'sys': MagicMock()
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_json_status
        self.module = check_json_status


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_json(self):
        self.assertEqual( self.module.get_json('url','v'), self.mock_requests.get().json())
    
    def test_get_query(self):
        self.module.parse = MagicMock(return_value='val')
        self.assertEqual(self.module.get_query('query'), 'val')

    def test_main(self):
        self.module.check_json_status = MagicMock(return_value=(1,'2'))
        self.module.main()

    def test_main_exception(self):
        self.module.check_json_status = MagicMock(side_effect=Exception)
        self.module.main()

    def test_main_exception_request(self):
        import requests
        self.module.check_json_status = MagicMock(side_effect=requests.exceptions.RequestException)
        self.module.main()



if __name__ == '__main__':
    unittest.main()
