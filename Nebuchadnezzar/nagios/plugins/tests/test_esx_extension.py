import unittest
from mock import MagicMock, patch, call
from StringIO import StringIO


class CheckESXEXTENSIONTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_subprocess = MagicMock(name='subprocess')
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            'argparse': self.mock_argparse,
            'subprocess': self.mock_subprocess,
            'subprocess.Popen': self.mock_subprocess.Popen,
            'subprocess.PIPE': self.mock_subprocess.PIPE,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import esx_extension
        self.module = esx_extension

    def test_check_args(self):
        result = self.module.check_args()
        exp_result = (self.mock_argparse.ArgumentParser.return_value.parse_args().condition,
                      self.mock_argparse.ArgumentParser.return_value.parse_args().command)
        self.assertEqual(result, exp_result)

    def test_esx_extension_status(self):
        mock_popen = MagicMock(name='popen')
        mock_popen.communicate.return_value = 'stdout', 'stderr'
        mock_popen.returncode = 0
        self.mock_subprocess.Popen.return_value = mock_popen
        resp = self.module.esx_extension('OK', 'command')
        self.assertEqual(resp, (0, 'stdout', 'stderr'))

    def test_esx_extension_status_1(self):
        mock_popen = MagicMock(name='popen')
        vmware_stdout = "CHECK_VMWARE_API.PL UNKNOWN - 2 health issue(s) found in 245 checks:\n1) UNKNOWN[fan] Status of Fan Device 8 Fans - unknown: Cannot report on the current health state of the element\n2) UNKNOWN[Battery] Status of Battery 1 Megacell Status: Failed - unknown: Cannot report on the current health state of the element"
        mock_popen.communicate.return_value = vmware_stdout, 'stderr'
        mock_popen.returncode = 3
        self.mock_subprocess.Popen.return_value = mock_popen
        resp = self.module.esx_extension('OK', 'command')
        plugin_stdout = (1,
                         'CHECK_VMWARE_API.PL WARNING - Reset of ESX sensor and CIM service restart might be required to get right status.\n Results - 2 health issue(s) found in 245 checks:\n1) UNKNOWN[fan] Status of Fan Device 8 Fans - unknown: Cannot report on the current health state of the element\n2) UNKNOWN[Battery] Status of Battery 1 Megacell Status: Failed - unknown: Cannot report on the current health state of the element',
                         'stderr')
        self.assertEqual(resp, plugin_stdout)

    def test_esx_extension_status_2(self):
        mock_popen = MagicMock(name='popen')
        vmware_stdout = "CHECK_VMWARE_API.PL UNKNOWN - 2 health issue(s) found in 245 checks:\n1) WARNING[fan] Status of Fan Device 8 Fans - unknown: Cannot report on the current health state of the element\n2) UNKNOWN[Battery] Status of Battery 1 Megacell Status: Failed - unknown: Cannot report on the current health state of the element"
        mock_popen.communicate.return_value = vmware_stdout, 'stderr'
        mock_popen.returncode = 3
        self.mock_subprocess.Popen.return_value = mock_popen
        resp = self.module.esx_extension('OK', 'command')
        plugin_stdout = (1,
                         'CHECK_VMWARE_API.PL WARNING - Reset of ESX sensor and CIM service restart might be required to get right status.\n Results - 2 health issue(s) found in 245 checks:\n1) WARNING[fan] Status of Fan Device 8 Fans - unknown: Cannot report on the current health state of the element\n2) UNKNOWN[Battery] Status of Battery 1 Megacell Status: Failed - unknown: Cannot report on the current health state of the element',
                         'stderr')
        self.assertEqual(resp, plugin_stdout)

    def test_esx_extension_status_3(self):
        mock_popen = MagicMock(name='popen')
        vmware_stdout = "CHECK_VMWARE_API.PL UNKNOWN - 2 health issue(s) found in 245 checks:\n1) CRITICAL[fan] Status of Fan Device 8 Fans - unknown: Cannot report on the current health state of the element\n2) UNKNOWN[Battery] Status of Battery 1 Megacell Status: Failed - unknown: Cannot report on the current health state of the element"
        mock_popen.communicate.return_value = vmware_stdout, 'stderr'
        mock_popen.returncode = 3
        self.mock_subprocess.Popen.return_value = mock_popen
        resp = self.module.esx_extension('OK', 'command')
        plugin_stdout = (2,
                         'CHECK_VMWARE_API.PL CRITICAL - Reset of ESX sensor and CIM service restart might be required to get right status.\n Results - 2 health issue(s) found in 245 checks:\n1) CRITICAL[fan] Status of Fan Device 8 Fans - unknown: Cannot report on the current health state of the element\n2) UNKNOWN[Battery] Status of Battery 1 Megacell Status: Failed - unknown: Cannot report on the current health state of the element',
                         'stderr')
        self.assertEqual(resp, plugin_stdout)

    def test_esx_extension_status_exception(self):
        self.mock_subprocess.Popen.side_effect = Exception("Exception")
        resp = self.module.esx_extension('OK', 'command')
        self.assertEqual(resp, (3, 'UNKNOWN - Exception', None))

    def test_get_unknown_output(self):
        self.assertEqual(self.module.get_unknown_output('message'), (3, 'UNKNOWN - message', None))

    def test_scrub_output(self):
        self.assertEqual(self.module.scrub_output('$#message'), '##message')

    def test_set_outmsg(self):
        self.assertEqual(self.module.set_outmsg('WARNING-msg, Results', 'UNKNOWN-out_msg'),
                         'WARNING-msg, Results-out_msg')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_check_args = MagicMock(name='check_args')
        mock_check_args.return_value = 'a', 'b', 'c'
        self.module.check_args = mock_check_args
        mock_esx_extension = MagicMock(name='esx_extension')
        mock_esx_extension.return_value = 'state', 'msg', 'err_msg'
        self.module.esx_extension = mock_esx_extension
        self.module.main()
        self.assertEqual(std_out.getvalue(), 'msg')
        self.sys_mock.exit.assert_called_once_with('state')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
