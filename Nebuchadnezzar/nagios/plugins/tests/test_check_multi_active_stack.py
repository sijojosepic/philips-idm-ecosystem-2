import unittest
from mock import MagicMock, patch


class CheckTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.product': self.mock_scanline.product,
            'scanline.product.isp': self.mock_scanline.product.isp,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.token_mgr': self.mock_scanline.utilities.token_mgr,
            'scanline.utilities.http': self.mock_scanline.utilities.http,
            'scanline.utilities.dns': self.mock_scanline.utilities.dns,
            'scanline.utilities.wmi': self.mock_scanline.utilities.wmi,
            'phimutils.resource': self.mock_phimutils.resource,

        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_multi_active_stack
        self.check_multi_active_stack = check_multi_active_stack
        from check_multi_active_stack import get_active_stack
        self.get_active_stack = get_active_stack
        from scanline.utilities.http import HTTPRequester
        self.HTTPRequester = HTTPRequester()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_xml_blob(self):
        data = '''YAsAAHic7VbbbqMwEH2v1H/IDzgxYG4SQoo2feCh0SppP8CXoWstMbtcou1+fc0thpC0b6uNlB
                   fAZ449x2cGQ7SvClrB2/u3XKXyrdYDmav48WGxiNZ1lW8ggwoGUovryB6KIxTPuYAYR6vRaIj3
                   /Jf3XxATzRiPe86mz5WoZ6nqCspmqTnYKlldkRKtj1RmlGUDLPsZIw39eIRs6QHiPfyuQVWSZk
                   ZeG5jRn1STQMSWIQ7QjNtssFu+0ovnxbLzZmnkLU3ii9ZPzbqsaJNlXyQR2WhbA322TqIqKFLK
                   IX76A1zbbeaYUG/natYDn/u7y2sldjmT6h/7axLfsL+646909rjlKf85a/cWHUvRw1cpYmAEYy
                   sIEA1Chkhq24i5boBw4PKUOT6EqdeI6OmnBZKyxRLFs1qASE6uNvW6Hjyfv+aVPLanxRQ45+2g
                   koWuvCEOyDnzVdVl1zVTwGy9fdE4bOsDg6I7qCbI1KRWkBYvNvryIg+wWJ0xeiWX4klpPDBnVb
                   eLi5Hzqo/L9nUtsc9cwjgS2LMQ8UOMQsApIhbBKQUvxVzca3kjtbRJanmefi+twEbEwUw/uT7i
                   YeAEPEwxDZ17LW+jllgwHHicId/FApHQc1BoE0DYJpbgoeu7nnuv5X9Qy8nn9fQVjTq/1gX/oW
                   9b/T9bfs/zbC//QmPBJ9HHB5Nl+iP9AQPapzo='''
        xml_obj = self.check_multi_active_stack.get_xml_blob(data)
        self.assertEqual(xml_obj.ActiveArchiveNodesPoolSize.text, '1')

    def test_waiting_stack(self):
        data = [{'SequenceNumber': 0, 'IsStackRetired': 0, 'IsStackIncludedInStrategy': 1,
                 'IsStackActive': 0, 'StackUid': 'eb400188-a89b-4f22-b558-085cfb37e9f6', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0,
                 'IsStackIncludedInStrategy': 1, 'IsStackActive': 0,
                 'StackUid': 'e07b54bc-d061-4790-9e0f-4140fae6f0cd', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0, 'IsStackIncludedInStrategy': 1,
                 'IsStackActive': 0, 'StackUid': 'e24f1668-a182-430b-a157-c9838c9f0a93', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0,
                 'IsStackIncludedInStrategy': 1, 'IsStackActive': 0,
                 'StackUid': '0db086cb-750d-4963-924e-0241dc957565', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0, 'IsStackIncludedInStrategy': 1,
                 'IsStackActive': 0, 'StackUid': 'ff5c812c-0427-4734-98ee-3832463736e0', 'IsStackUnused': 1}]
        self.check_multi_active_stack.get_threshold = MagicMock('get_threshold', return_value=(2, 1, 3))
        self.assertEqual(self.check_multi_active_stack.waiting_stack(data, '1M'),
                         (0, 'OK - Found 5 number of waiting stacks meeting threshold.'))

    def test_waiting_stack_warning(self):
        data = [{'SequenceNumber': 0, 'IsStackRetired': 0, 'IsStackIncludedInStrategy': 1,
                 'IsStackActive': 0, 'StackUid': 'eb400188-a89b-4f22-b558-085cfb37e9f6', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0,
                 'IsStackIncludedInStrategy': 1, 'IsStackActive': 0,
                 'StackUid': 'e07b54bc-d061-4790-9e0f-4140fae6f0cd', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0, 'IsStackIncludedInStrategy': 1,
                 'IsStackActive': 0, 'StackUid': 'e24f1668-a182-430b-a157-c9838c9f0a93', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0,
                 'IsStackIncludedInStrategy': 1, 'IsStackActive': 0,
                 'StackUid': '0db086cb-750d-4963-924e-0241dc957565', 'IsStackUnused': 1},
                {'SequenceNumber': 0, 'IsStackRetired': 0, 'IsStackIncludedInStrategy': 1,
                 'IsStackActive': 0, 'StackUid': 'ff5c812c-0427-4734-98ee-3832463736e0', 'IsStackUnused': 1}]
        self.check_multi_active_stack.get_threshold = MagicMock('get_threshold', return_value=(8, 4, 12))
        self.assertEqual(self.check_multi_active_stack.waiting_stack(data, '5M'),
                         (1, 'WARNING - Found 5 waiting stacks not meeting threshold of 12'))

    def test_get_data(self):
        self.mock_scanline.utilities.token_mgr.get_formatted_token.return_value = '123'
        self.HTTPRequester.suppressed_post = MagicMock('suppressed_post', return_value='112')
        self.assertEqual(self.check_multi_active_stack.get_data('http://1.com', ''), '112')

    @patch('check_multi_active_stack.get_data')
    def test_get_machine_map(self, mock_data):
        self.check_multi_active_stack.get_data.return_value.status_code = 200
        self.check_multi_active_stack.get_data.return_value.content = 'test'
        self.assertEqual(self.check_multi_active_stack.get_machine_map('http://1.com'), 'test')

    @patch('check_multi_active_stack.get_data')
    def test_get_stratagies(self, mock_data):
        url = '1.1.1.1'
        hostname = 'test_host'
        study = '123'
        self.check_multi_active_stack.get_data.return_value = 'test'
        self.assertEqual(self.check_multi_active_stack.get_stratagies(url, hostname, study), 'test')

if __name__ == '__main__':
    unittest.main()
