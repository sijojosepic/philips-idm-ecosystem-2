from StringIO import StringIO
import unittest

from mock import MagicMock, patch
from requests.exceptions import RequestException


class HealthStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'requests': self.mock_request,
            'requests.exceptions': self.mock_request.exceptions,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_health_status
        self.module = check_health_status

    def test_get_url(self):
        url = self.module.get_url('host', '/service_url', 'protocol', 80)
        self.assertEqual(url, 'protocol://host:80/service_url')

    def test_parse_cmd_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.service_url = 'service_url'
        mock_result.protocol = 'protocol'
        mock_result.hmac = 'True'
        mock_result.port = 80
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.parse_cmd_args()
        self.assertEqual(response, ('localhost', 'service_url',
                                    'protocol', True, 80))

    def test_process_output_ok_status(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "OK",
                                           "data": {
                                               "message": "Cumulative Calls : 0 , Success Calls : 0, Failed Calls : 0"
                                           }
                                           }
        expected_response = (
            0, 'OK - Cumulative Calls : 0 , Success Calls : 0, Failed Calls : 0| service status=0;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_critical_status(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "CRITICAL",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        expected_response = (2, 'CRITICAL - Error| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_proper_response(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "CRITICAL",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        expected_response = (2, 'CRITICAL - Error| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_invalid_status(self):
        mock_response = MagicMock(name='response')
        mock_response.json.return_value = {"status": "INVALID",
                                           "data": {
                                               "message": "Error"
                                           }
                                           }
        expected_response = (
            2, 'CRITICAL : Invalid status `INVALID` received, status should be any of `OK,WARNING,CRITICAL,UNKNOWN`| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_process_output_verify_json_false(self):
        mock_response = MagicMock(name='response')
        mock_response.status_code = 500
        del mock_response.json
        # mock_response.json.return_value = {"status1": "INVALID",
        #                                    "data": {
        #                                        "message": "Error"
        #                                    }
        #                                    }
        mock_verify_response = MagicMock(name='verify_response')
        mock_verify_response.__nonzero__.return_value = False
        self.module.mock_verify_response = mock_verify_response
        expected_response = (2, 'CRITICAL : did not get, application status from the URL url, status_code - 500| service status=2;1;2;0;2')
        self.assertEqual(expected_response,
                         self.module.process_output(mock_response, 'url'))

    def test_health_status(self):
        mock_resp = MagicMock(name='repsonse')
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 'h_s'
        self.module.process_output = mock_process_output
        self.mock_request.get.return_value = mock_resp
        self.assertEqual(self.module.health_status('url', False, False), 'h_s')
        mock_process_output.assert_called_once_with(mock_resp, 'url')

    def test_health_status_hmac(self):
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 'h_s'
        self.module.process_output = mock_process_output
        self.assertEqual(self.module.health_status('url', True, False), 'h_s')
        mock_process_output.assert_called_once_with(
            self.mock_scanline.utilities.token_mgr.do_hmac_request.return_value, 'url')
        self.mock_scanline.utilities.token_mgr.do_hmac_request.assert_called_once_with('url', timeout=5)

    def test_health_status_verify_arg(self):
        mock_resp = MagicMock(name='repsonse')
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 'h_s'
        self.module.process_output = mock_process_output
        self.mock_request.get.return_value = mock_resp
        self.assertEqual(self.module.health_status('url', False, False), 'h_s')
        mock_process_output.assert_called_once_with(mock_resp, 'url')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_health_status = MagicMock(name='health_status')
        mock_health_status.return_value = 1, 'Msg'
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.health_status = mock_health_status
        self.module.get_url = mock_get_url
        self.module.main('host', 'url', 'protocol', False, 80)
        mock_health_status.assert_called_once_with('url', False)
        mock_get_url.assert_called_once_with('host', 'url', 'protocol',80)
        self.sys_mock.exit.assert_called_once_with(1)
        self.assertEqual(std_out.getvalue(), 'Msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_health_status = MagicMock(name='health_status')
        mock_health_status.side_effect = RequestException(
            'exe')
        self.module.RequestException = RequestException
        self.module.health_status = mock_health_status
        self.module.main('host', 'url', 'protocol', False, 80)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL : Request Error - exe| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_health_status = MagicMock(name='health_status')
        mock_health_status.side_effect = Exception('Exception')
        self.module.health_status = mock_health_status
        self.module.main('host', 'url', 'protocol', False, 80)
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(
            std_out.getvalue(), 'CRITICAL : Error - Exception| service status=2;1;2;0;2\n')

    def test_search_response_status(self):
        json_data = {"status": "CRITICAL", "data": {"message": "Error"}}
        self.assertEqual(self.module.search_response(
            json_data, 'status'), 'CRITICAL')

    def test_search_response_message(self):
        json_data = {"status": "CRITICAL", "data": {"message": "Error"}}
        self.assertEqual(self.module.search_response(
            json_data, 'message'), 'Error')

    def test_search_response_message_list(self):
        json_data = [{"status": "CRITICAL", "data": {"message": "Error"}}]
        self.assertEqual(self.module.search_response(
            json_data, 'message'), 'Error')

    def test_search_response_wrong(self):
        json_data = {"status": "CRITICAL", "data": {"message": "Error"}}
        self.assertEqual(self.module.search_response(json_data, 'stat'), None)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
