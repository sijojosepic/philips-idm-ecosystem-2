import unittest
from mock import MagicMock, patch, call
import mock
from requests.exceptions import ConnectionError, HTTPError, ConnectTimeout


class CheckRabbitmqDataTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_json = MagicMock(name='json')
        self.mock_args = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name="scanline")
        modules = {
            'requests': self.mock_request,
            'json': self.mock_json,
            'argparse': self.mock_args,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.cache': self.mock_scanline.utilities.cache
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_rabbitmq_data
        self.module = check_rabbitmq_data

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        results = MagicMock(name='mock_result')
        results.hostname = 'localhost'
        results.port = 15672
        results.username = 'user'
        results.password = 'password'
        results.protocol = 'https'
        results.subpath = 'subpath'
        results.service = 'service'
        results.warning_val = 95
        results.critical_val = 100
        results.req_timeout = 15
        results.previous_state = 'OK'
        results.enable_cache = 'yes'
        results.cache_timeout = 540
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = results
        self.module.argparse.ArgumentParser = self.mock_args
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('localhost', 15672, 'user',
                                    'password', 'https', 'subpath', 'service', 95, 100, 15, 'OK', 'yes', 540))

    def test_get_url(self):
        output = self.module.get_url("localhost", 15672, 'https', 'queues')
        msg = '{0}://{1}:{2}/api/{3}'.format('https',
                                             "localhost", 15672, 'queues')
        self.assertEqual(output, msg)

    def test_get_api_response(self):
        mock_response = MagicMock(name='response')
        mock_response.content = "{'key': 'data'}"
        self.mock_request.get.return_value = mock_response
        self.mock_json.loads.return_value = {'json': 'data'}
        output = self.module.get_api_response(
            "url", 'user', 'pass', 15)
        self.assertEqual(output, {'json': 'data'})
        headers = {
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
            'connection': 'keep-alive',
            'content-type': 'application/json',
            'upgrade-insecure-requests': '1',
        }
        self.mock_request.get.assert_called_once_with(
            'url', headers=headers, auth=('user', 'pass'), timeout=15, verify=False)
        mock_response.raise_for_status.assert_called_once_with()
        self.mock_json.loads.assert_called_once_with("{'key': 'data'}")

    @mock.patch('scanline.utilities.cache.SafeCache.do')
    def test_write_cache(self, mock_safecache_do):
        mock_safecache_do.return_value = None
        self.assertEqual(self.module.write_cache('key', 'json', 540), None)

    @mock.patch('scanline.utilities.cache.SafeCache.do')
    def test_read_cache(self, mock_safecache_do):
        mock_safecache_do.return_value = 'json'
        self.assertEqual(self.module.read_cache('key'), 'json')

    def test_do_request(self):
        self.module.get_url = MagicMock(name='get_url', return_value='url')
        self.module.get_api_response = MagicMock(name='get_api_response', return_value='json')
        output = self.module.do_request('host', 'subpath', 15671, 'https', 'user', 'pass', 10)
        self.assertEqual(output, 'json')
        self.module.get_url.assert_called_once_with('host', 15671, 'https', 'subpath')
        self.module.get_api_response.assert_called_once_with('url', 'user', 'pass', 10)

    def test_get_cofig_data_enable_cache(self):
        self.module.read_cache = MagicMock(name='read_cache', return_value='json')
        self.mock_json.loads.return_value = {'json': 'data'}
        output = self.module.get_status_data('host', 'subpath', 15672, 'https', 'user', 'pass', 10, 'yes', 540, 'OK')
        self.assertEqual(output, {'json': 'data'})
        self.module.read_cache.assert_called_once_with('host_rabbitmq_subpath')

    def test_get_cofig_data_enable_cache2(self):
        self.module.read_cache = MagicMock(name='read_cache', return_value='')
        self.module.do_request = MagicMock(name='do_request', return_value='json')
        self.module.write_cache = MagicMock(name='write_cache')
        output = self.module.get_status_data('host', 'subpath', 15672, 'https', 'user', 'pass', 10, 'yes', 540, 'OK')
        self.assertEqual(output, 'json')
        self.module.read_cache.assert_called_once_with('host_rabbitmq_subpath')
        self.module.do_request.assert_called_once_with('host', 'subpath', 15672, 'https', 'user', 'pass', 10)
        self.module.write_cache.assert_called_once_with('host_rabbitmq_subpath', 'json', 540)

    def test_get_cofig_data_previous_state(self):
        self.module.do_request = MagicMock(name='do_request', return_value='json')
        self.module.write_cache = MagicMock(name='write_cache')
        output = self.module.get_status_data('host', 'subpath', 15672, 'http', 'user', 'pass', 10, 'yes', 540,
                                             'CRITICAL')
        self.assertEqual(output, 'json')
        self.module.do_request.assert_called_once_with('host', 'subpath', 15672, 'http', 'user', 'pass', 10)
        self.module.write_cache.assert_called_once_with('host_rabbitmq_subpath', 'json', 540)

    def test_get_cofig_data_disable_cache(self):
        self.module.do_request = MagicMock(name='do_request', return_value='json')
        output = self.module.get_status_data('host', 'subpath', 15672, 'https', 'user', 'pass', 10, 'no', 540, 'OK')
        self.assertEqual(output, 'json')
        self.module.do_request.assert_called_once_with('host', 'subpath', 15672, 'https', 'user', 'pass', 10)

    def test_check_status_queues(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_queues_status = MagicMock(name='get_queues_status', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'queues', 'service',
                                                  'warning_val', 'critical_val', 'timeout', 'OK', 'yes', 540),
                         (0, 'msg')
                         )
        self.module.get_status_data.assert_called_once_with('hostname', 'queues', 'port', 'protocol', 'user', 'pass',
                                                            'timeout', 'yes', 540, 'OK')
        self.module.get_queues_status.assert_called_once_with('json', 'warning_val', 'critical_val')

    def test_check_status_channel(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_channel_obj_status = MagicMock(name='get_channel_obj_status', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'channels',
                                                  'service', 'warning_val', 'critical_val', 'timeout', 'OK', 'yes',
                                                  540), (0, 'msg'))
        self.module.get_status_data.assert_called_once_with('hostname', 'channels', 'port', 'protocol', 'user', 'pass',
                                                            'timeout', 'yes', 540, 'OK')
        self.module.get_channel_obj_status.assert_called_once_with('json', 'warning_val', 'critical_val')

    def test_check_status_connections_count(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_connection_count = MagicMock(name='get_connection_count', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'connections',
                                                  'count', 'warning_val', 'critical_val', 'timeout', 'OK', 'yes', 540),
                         (0, 'msg'))
        self.module.get_status_data.assert_called_once_with('hostname', 'connections', 'port', 'protocol', 'user',
                                                            'pass',
                                                            'timeout', 'yes', 540, 'OK')
        self.module.get_connection_count.assert_called_once_with('json', 'critical_val')

    def test_check_status_watermark_mem(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_watermark_status = MagicMock(name='get_watermark_status', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'nodes',
                                                  'watermark', 'warning_val', 'critical_val', 15, 'OK', 'yes', 540),
                         (0, 'msg'))
        self.module.get_status_data.assert_called_once_with('hostname', 'nodes', 'port', 'protocol', 'user', 'pass',
                                                            15, 'yes', 540, 'OK')
        self.module.get_watermark_status.assert_called_once_with('json')

    def test_check_status_watermark_disk(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_disk_status = MagicMock(name='get_disk_status', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'nodes',
                                                  'disk', 'warning_val', 'critical_val', 15, 'OK', 'yes', 540),
                         (0, 'msg'))
        self.module.get_status_data.assert_called_once_with('hostname', 'nodes', 'port', 'protocol', 'user', 'pass',
                                                            15, 'yes', 540, 'OK')
        self.module.get_disk_status.assert_called_once_with('json')

    def test_check_status_partitions(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_partitions_status = MagicMock(name='get_partitions_status', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'nodes',
                                                  'partitions', 'warning_val', 'critical_val', 'timeout', 'OK', 'yes',
                                                  540), (0, 'msg'))
        self.module.get_status_data.assert_called_once_with('hostname', 'nodes', 'port', 'protocol', 'user', 'pass',
                                                            'timeout', 'yes', 540, 'OK')
        self.module.get_partitions_status.assert_called_once_with('json')

    def test_check_status_connections_state(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_connection_state = MagicMock(name='get_connection_state', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('hostname', 'port', 'user', 'pass', 'protocol', 'connections',
                                                  'state', 'warning_val', 'critical_val', 'timeout', 'OK', 'yes', 540),
                         (0, 'msg'))
        self.module.get_status_data.assert_called_once_with('hostname', 'connections', 'port', 'protocol', 'user',
                                                            'pass',
                                                            'timeout', 'yes', 540, 'OK')
        self.module.get_connection_state.assert_called_once_with('json')

    def test_check_status_cluster(self):
        self.module.get_status_data = MagicMock(name='get_status_data', return_value='json')
        self.module.get_cluster_status = MagicMock(name='get_cluster_status', return_value=(0, 'msg'))
        self.assertEqual(self.module.check_status('host', 'port', 'user', 'pass', 'https', 'nodes',
                                                  'cluster', 'warning_val', 'critical_val', 'timeout', 'OK', 'yes',
                                                  540), (0, 'msg'))

        self.module.get_status_data.assert_has_calls([call('host', 'nodes', 'port', 'https', 'user', 'pass', 'timeout',
                                                           'yes', 540, 'OK'),
                                                      call('host', 'overview', 'port', 'https', 'user', 'pass',
                                                           'timeout',
                                                           'yes', 540, 'OK')])
        self.module.get_cluster_status.assert_called_once_with('json', 'json')

    def test_check_status_authexception(self):
        self.module.get_status_data = MagicMock(name='get_status_data', side_effect=HTTPError)
        self.assertEqual(self.module.check_status('localhost', 15671, 'guest', 'guest', 'https', 'nodes', 'partitions',
                                                  10, 20, 15, 'OK', 'yes', 540),
                         (3, 'UNKNOWN : HTTPError - {0}'.format('')))

    def test_check_status_connectionexception(self):
        self.module.get_status_data = MagicMock(name='get_status_data', side_effect=ConnectTimeout)
        self.assertEqual(self.module.check_status('localhost', 15671, 'guest', 'guest', 'https', 'nodes', 'partitions',
                                                  10, 20, 15, 'OK', 'yes', 540),
                         (3, 'UNKNOWN : Connection Error - RabbitMQ server is not accessible'))

    def test_check_status_requestexception(self):
        self.module.get_status_data = MagicMock(name='get_status_data', side_effect=ConnectionError)
        self.assertEqual(self.module.check_status('localhost', 15671, 'guest', 'guest', 'https', 'nodes', 'partitions',
                                                  10, 20, 15, 'OK', 'yes', 540),
                         (3, 'UNKNOWN : Request Error {0}'.format('')))

    def test_check_status_exception(self):
        self.module.get_status_data = MagicMock(name='get_status_data', side_effect=Exception('e'))
        self.assertEqual(self.module.check_status('localhost', 15671, 'guest', 'guest', 'https', 'nodes', 'partitions',
                                                  10, 20, 15, 'OK', 'yes', 540),
                         (2, 'CRITICAL : Error while retrieving response data e'))

    def test_get_connection_state_warning(self):
        content = [{"name": "connection1", "state": "running"}, {"name": "connection2", "state": "flow"}]
        self.assertEqual(self.module.get_connection_state(content),
                         (2, 'CRITICAL : Connection(s) in Flow state \n Name: connection2, State: flow'))

    def test_get_connection_state_ok(self):
        content = [{"name": "connection1", "state": "running"}, {"name": "connection2", "state": "running"}]
        self.assertEqual(self.module.get_connection_state(content), (0, 'OK : RabbitMQ connections state are OK.'))

    def test_get_watermark_memory_status_critical(self):
        content = [
            {
                "mem_alarm": True,
                "disk_free_alarm": False,
                "name": "host1",
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
            },
            {
                "mem_alarm": True,
                "disk_free_alarm": False,
                "name": "host2",
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
            }
        ]
        self.assertEqual(self.module.get_watermark_status(content),
                         (2,
                          'CRITICAL : Watermark is critical, memory alarm is True. \n  host1 - Memory limit: 3301928140,'
                          ' Memory used: 32176800 \n host2 - Memory limit: 3301928140, Memory used: 32176800 \n'))

    def test_get_disk_status_critical(self):
        content = [
            {
                "mem_alarm": False,
                "disk_free_alarm": True,
                "name": "host1",
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
            },
            {
                "mem_alarm": False,
                "disk_free_alarm": True,
                "name": "host2",
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
            }
        ]
        self.assertEqual(self.module.get_disk_status(content),
                         (2, 'CRITICAL : Disk is critical, disk alarm is True. \n  host1 - Disk free limit: 1000000000, '
                             'Disk free: 135471878144 \n host2 - Disk free limit: 1000000000, '
                             'Disk free: 135471878144 \n.'))

    def test_get_watermark_status_ok(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_watermark_status(content),
                         (0, 'OK : RabbitMQ Memory utilization is OK.'))

    def test_get_watermark_disk_status_ok(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_disk_status(content),
                         (0, 'OK : RabbitMQ Disk utilization is OK.'))

    def test_get_channel_obj_status_ok(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_channel_obj_status(content, 5, 10),
                         (0, 'OK : The Channel Object Count is 1'))

    def test_get_connection_count_ok(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_connection_count(content, 5),
                         (0, 'OK : Total Connection Count: 1'))

    def test_get_connection_count_critical(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_connection_count(content, 0),
                         (2, 'CRITICAL : Total Connection Count is 1, which has breached the CRITICAL threshold of 0'))

    def test_get_channel_obj_status_warn(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_channel_obj_status(content, 0, 5),
                         (1, 'WARNING : The Channel Object Count is 1, which has breached the WARNING threshold of 0'))

    def test_get_channel_obj_status_critical(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_channel_obj_status(content, 0, 1),
                         (2, 'CRITICAL : The Channel Object Count is 1, which has breached the CRITICAL threshold of 1'))

    def test_get_partitions_status_ok(self):
        content = [
            {
                "partitions": [],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200
            }
        ]
        self.assertEqual(self.module.get_partitions_status(content),
                         (0, 'OK : No network partitions.'))

    def test_get_partitions_status_critical(self):
        content = [
            {
                "partitions": [1, 2],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200,
                "name": "host1"
            },
            {
                "partitions": [3],
                "mem_used": 32176800,
                "mem_limit": 3301928140,
                "mem_alarm": False,
                "disk_free_limit": 1000000000,
                "disk_free": 135471878144,
                "disk_free_alarm": False,
                "proc_used": 200,
                "name": "host2"
            }
        ]
        self.assertEqual(self.module.get_partitions_status(content),
                         (2,
                          "CRITICAL : Network partitions detected: \n  host1: Partition- ['1', '2'] \n "
                          " host2: Partition- ['3'] \n "))

    def test_get_queues_status_ok(self):
        content = [
            {
                "messages": 100,
                "name": "Q1",
            },
            {
                "messages": 40,
                "name": "Q2",
            },
            {
                "messages": 105,
                "name": "Q3",
            },
        ]
        exp_msg = 'OK : Found 3 Queues, None of them has breached the threshold defined for number of messages (Warning-950, Critical-1000)'
        self.assertEqual(self.module.get_queues_status(content, 950, 1000),
                         (0, exp_msg))

    def test_get_queues_status_warn(self):
        content = [
            {
                "messages": 955,
                "name": "Q1",
            },
            {
                "messages": 40,
                "name": "Q2",
            },
            {
                "messages": 105,
                "name": "Q3",
            },
        ]
        warning_queues = '{0} {1} - {2} Messages \n'.format('', 'Q1', 955)
        msg = 'WARNING : Out of {0} queues, {1} queues has breached  WARNING threshold of 950. \n {2}' \
            .format(3, 1, warning_queues)
        self.assertEqual(self.module.get_queues_status(
            content, 950, 1000), (1, msg))

    def test_get_queues_status_crit(self):
        content = [
            {
                "messages": 955,
                "name": "Q1",
            },
            {
                "messages": 2000,
                "name": "Q2",
            },
            {
                "messages": 105,
                "name": "Q3",
            },
        ]
        warning_queues = '{0} {1} - {2} Messages \n'.format('', 'Q1', 955)
        critical_queues = '{0} {1} - {2} Messages \n'.format('', 'Q2', 2000)
        msg = "CRITICAL : Out of {0} queues, {1} queues has breached CRITICAL threshold of 1000, {2} queues has " \
              "breached WARNING threshold of 950. \n CRITICAL : \n {3} \n WARNING : \n {4}" \
            .format(3, 1, 1, critical_queues, warning_queues)
        self.assertEqual(self.module.get_queues_status(
            content, 950, 1000), (2, msg))

    def test_get_queues_status_crit_only(self):
        content = [
            {
                "messages": 0,
                "name": "Q1",
            },
            {
                "messages": 2000,
                "name": "Q2",
            },
            {
                "messages": 0,
                "name": "Q3",
            },
        ]
        critical_queues = '{0} {1} - {2} Messages \n'.format('', 'Q2', 2000)
        msg = "CRITICAL : Out of {0} queues, {1} queues has breached  CRITICAL threshold of 1000. \n " \
              "CRITICAL : \n  {2}".format(3, 1, critical_queues)
        self.assertEqual(self.module.get_queues_status(
            content, 950, 1000), (2, msg))

    def test_get_queues_status_keyerror(self):
        content = [
            {
                "name": "Q1",
            },
            {
                "messages": 2,
                "name": "Q2",
            },
            {
                "messages": 3,
                "name": "Q3",
            },
        ]
        exp_msg = 'OK : Found 3 Queues, None of them has breached the threshold defined for number of messages (Warning-950, Critical-1000)'
        self.assertEqual(self.module.get_queues_status(content, 950, 1000),
                         (0, exp_msg))

    def test_get_cluster_status_network_critical(self):
        node_data = [{'name': 'node1', 'running': True}, {'name': 'node2', 'running': False}]
        overview_data = {}
        state, msg = self.module.get_cluster_status(node_data, overview_data)
        self.assertEqual(state, 2)
        self.assertEqual(msg, "CRITICAL : Node is out of network/not reachable ['node2']")

    def test_get_cluster_status_cluster_critical(self):
        node_data = [{'name': 'node1', 'running': True}, {'name': 'node2', 'running': True}]
        listeners = [{'protocol': 'clustering', 'node': 'node1'}, {'protocol': 'clustering', 'node': 'node2'}]
        contexts = [{'node': 'node1'}]
        overview_data = {'listeners': listeners, 'contexts': contexts}
        state, msg = self.module.get_cluster_status(node_data, overview_data)
        self.assertEqual(state, 2)
        self.assertEqual(msg, "CRITICAL : Node is out of cluster ['node2']")

    def test_get_cluster_status_cluster_warning(self):
        node_data = [{'name': 'node1', 'running': True}, {'name': 'node2', 'running': True}]
        listeners = [{'protocol': 'clustering', 'node': 'node1'}]
        contexts = [{'node': 'node1'}, {'node': 'node2'}]
        overview_data = {'listeners': listeners, 'contexts': contexts}
        state, msg = self.module.get_cluster_status(node_data, overview_data)
        self.assertEqual(state, 1)
        self.assertEqual(msg, "WARNING : All nodes are not part of cluster, may be due to network partition")

    def test_get_cluster_status_ok(self):
        node_data = [{'name': 'node1', 'running': True}, {'name': 'node2', 'running': True}]
        listeners = [{'protocol': 'clustering', 'node': 'node1'}, {'protocol': 'clustering', 'node': 'node2'}]
        contexts = [{'node': 'node1'}, {'node': 'node2'}]
        overview_data = {'listeners': listeners, 'contexts': contexts}
        state, msg = self.module.get_cluster_status(node_data, overview_data)
        self.assertEqual(state, 0)
        self.assertEqual(msg, "OK : All nodes are up and running ['node1', 'node2']")

    def test_main(self):
        self.module.check_args = MagicMock(name='check_arg', return_value=(
            'localhost', 15671, 'guest', 'guest', 'https', 'queues', '', 10, 20, 15, 'OK', 'yes', 540))
        self.module.get_service_status = MagicMock(name='get_service_status', return_value=(0, 'OK'))
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
