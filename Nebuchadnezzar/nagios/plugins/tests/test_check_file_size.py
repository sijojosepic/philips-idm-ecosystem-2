import unittest
from mock import MagicMock, patch
from winrm.exceptions import AuthenticationError, WinRMOperationTimeoutError, WinRMError


class CheckSizeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_file_size
        self.module = check_file_size

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg_1(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_result.type = 'folder'
        mock_result.path = 'path'
        mock_result.warning = 450
        mock_result.critical = 500
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user',
                                    'pass', 'folder', 'path', 500, 450))

    def test_check_arg_2(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_result.type = 'file'
        mock_result.path = 'path'
        mock_result.warning = 450
        mock_result.critical = 500
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user',
                                    'pass', 'file', 'path', 500, 450))

    def test_get_status_file_ok(self):
        self.assertEqual(self.module.get_status(
            20, 500, 450, 'file'), (0, 'OK: Current File Size is 20MB.'))

    def test_get_status_file_Warning(self):
        self.assertEqual(self.module.get_status(
            468, 500, 450, 'file'), (1, 'WARNING: Current File Size is 468MB.'))

    def test_get_status_file_Critical(self):
        self.assertEqual(self.module.get_status(
            555, 500, 450, 'file'), (2, 'CRITICAL: Current File Size is 555MB.'))

    def test_get_status_folder_ok(self):
        self.assertEqual(self.module.get_status(
            20, 500, 450, 'folder'), (0, 'OK: Current Folder Size is 20MB.'))

    def test_get_status_folder_Warning(self):
        self.assertEqual(self.module.get_status(
            468, 500, 450, 'folder'), (1, 'WARNING: Current Folder Size is 468MB.'))

    def test_get_status_folder_Critical(self):
        self.assertEqual(self.module.get_status(555, 500, 450, 'folder'),
                         (2, 'CRITICAL: Current Folder Size is 555MB.'))

    def test_get_status_rabbitmq(self):
        mock_rbq_status = MagicMock(name='rabbitmq_status')
        mock_rbq_status.return_value = 2, 'msg'
        self.module.rabbitmq_status = mock_rbq_status
        result = self.module.get_status(1000, 450, 400, 'rabbitmq')
        self.assertEqual(result, (2, 'msg'))
        mock_rbq_status.assert_called_once_with(1000, 450, 400)

    def test_rabbitmq_status_OK(self):
        self.assertEqual(self.module.rabbitmq_status(
            10, 450, 400), (0, 'The RabbitMQ log file size is 10MB'))

    def test_rabbitmq_status_WARNING(self):
        self.assertEqual(self.module.rabbitmq_status(
            401, 450, 400), (1, 'The RabbitMQ log file size is 401MB, which has breached the WARNING threshold of 400MB.'))

    def test_rabbitmq_status_CRITICAL(self):
        self.assertEqual(self.module.rabbitmq_status(
            1000, 450, 400), (2, 'The RabbitMQ log file size is 1000MB, which has breached the CRITICAL threshold of 450MB.'))

    def test_get_file_size(self):
        self.mock_winrm.execute_ps_script.return_value = 130
        self.assertEqual(self.module.get_file_size(
            self.mock_winrm, 'path'), 130)

    def test_get_folder_size(self):
        self.mock_winrm.execute_ps_script.return_value = 131
        self.assertEqual(self.module.get_folder_size(
            self.mock_winrm, 'path'), 131)

    def test_format_output(self):
        self.assertEqual(self.module.format_output(
            '100.04355347', '', '', 'folder'), 100.04)

    def test_format_output_exception(self):
        try:
            self.module.format_output('0', '', 'Path Not Found', 'file')
        except Exception as e:
            self.assertEqual(str(e), 'Path Not Found')

    def test_format_output_rabbitmq(self):
        self.assertRaises(RuntimeError, self.module.format_output,
                          '', 'cannot find path', 'p1', 'rabbitmq')

    def test_format_output_file(self):
        self.assertRaises(RuntimeError, self.module.format_output,
                          '', 'cannot find path', 'p1', 'file')

    def test_clean_path(self):
        self.assertEqual(self.module.clean_path('abc'), 'abc/')

    def test_clean_path_non_slash(self):
        self.assertEqual(self.module.clean_path('abc/'), 'abc/')

    def test_get_rabbitmq_log_file_size(self):
        mock_wim_rm = MagicMock(name='win_rm')
        result = self.module.get_rabbitmq_log_file_size(mock_wim_rm, 'p1')
        self.assertEqual(result, mock_wim_rm.execute_ps_script.return_value)
        arg_val = '$hostName = hostname;(Get-ChildItem -Path p1rabbit@$hostName.log).Length/1mb'
        mock_wim_rm.execute_ps_script.assert_called_once_with(arg_val)

    def test_check_size_ok(self):
        self.module.format_output = MagicMock(return_value=313)
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(response, (0, 'OK: Current File Size is 313MB.'))

    def test_check_size_critical(self):
        self.module.format_output = MagicMock(return_value=813)
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(
            response, (2, 'CRITICAL: Current File Size is 813MB.'))

    def test_check_size_warning(self):
        self.module.format_output = MagicMock(return_value=467)
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(response, (1, 'WARNING: Current File Size is 467MB.'))

    def test_check_size_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(response, (1, 'WARNING : Exception e'))

    def test_check_size_WinRMOperationTimeoutError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError(
            'WinRMOperationTimeoutError')
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(
            response, (2, 'CRITICAL : WinRM Error WinRMOperationTimeoutError'))

    def test_check_size_AuthenticationError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError(
            'AuthenticationError')
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(
            response, (3, 'UNKNOWN : Authentication Error - AuthenticationError'))

    def test_check_size_WinRMError(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError(
            'WinRMError')
        response = self.module.check_size(
            'localhost', 'user', 'pass', 'file', 'path', 500, 450)
        self.assertEqual(response, (2, 'CRITICAL : WinRM Error WinRMError'))

    def test_check_size_rabbitmq(self):
        mock_format_output = MagicMock(name='format_output', return_value=540)
        self.module.format_output = mock_format_output
        mock_log_file_size = MagicMock(name='get_rabbitmq_log_file_size')
        self.module.get_rabbitmq_log_file_size = mock_log_file_size
        mock_clean_path = MagicMock(name='clean_path', return_value='p1')
        self.module.clean_path = mock_clean_path
        mock_get_status = MagicMock(name='get_status')
        mock_get_status.return_value = 0, 'msg'
        self.module.get_status = mock_get_status
        result = self.module.check_size(
            'localhost', 'user', 'pass', 'rabbitmq', 'path', 500, 450)
        self.assertEqual(result, (0, 'msg'))
        mock_log_file_size.assert_called_once_with(
            self.mock_scanline.utilities.win_rm.WinRM.return_value, 'p1')
        mock_clean_path.assert_called_once_with('path')
        mock_get_status.assert_called_once_with(540, 500, 450, 'rabbitmq')
        mock_format_output.assert_called_once_with(
            mock_log_file_size.return_value.std_out, mock_log_file_size.return_value.std_err, 'path', 'rabbitmq')

    def test_check_size_folder(self):
        mock_format_output = MagicMock(name='format_output', return_value=540)
        self.module.format_output = mock_format_output
        mock_get_folder_size = MagicMock(name='get_folder_size')
        self.module.get_folder_size = mock_get_folder_size
        mock_clean_path = MagicMock(name='clean_path', return_value='p1')
        self.module.clean_path = mock_clean_path
        mock_get_status = MagicMock(name='get_status')
        mock_get_status.return_value = 0, 'msg'
        self.module.get_status = mock_get_status
        result = self.module.check_size(
            'localhost', 'user', 'pass', 'folder', 'path', 500, 450)
        self.assertEqual(result, (0, 'msg'))
        mock_get_folder_size.assert_called_once_with(
            self.mock_scanline.utilities.win_rm.WinRM.return_value, 'p1')
        mock_clean_path.assert_called_once_with('path')
        mock_get_status.assert_called_once_with(540, 500, 450, 'folder')
        mock_format_output.assert_called_once_with(
            mock_get_folder_size.return_value.std_out, mock_get_folder_size.return_value.std_err, 'path', 'folder')

    def test_check_size_unknown_type(self):
        result = self.module.check_size(
            'localhost', 'user', 'pass', 'invalid', 'path', 500, 450)
        self.assertEqual(
            result, (2, 'CRITICAL: Unknown type invalid, type should be either file or folder'))

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = (
            'localhost', 'user', 'pass', 'folder', 'path', 500, 450)
        self.module.check_size = MagicMock(name='check_size')
        self.module.check_size.return_value = (0, 'OK')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
