import unittest
from mock import MagicMock, patch, call
from StringIO import StringIO


class CheckHPSANTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_json = MagicMock(name='json')
        self.mock_subprocess = MagicMock(name='subprocess')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_xml = MagicMock(name='xml')
        self.mock_shlex = MagicMock(name='shlex')
        self.mock_phimutils.collection.DictNoEmpty = dict
        modules = {
            'requests': self.mock_request,
            'phimutils': self.mock_phimutils,
            'argparse': self.mock_argparse,
            'json': self.mock_json,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'subprocess': self.mock_subprocess,
            'subprocess.Popen': self.mock_subprocess.Popen,
            'subprocess.PIPE': self.mock_subprocess.PIPE,
            'xml': self.mock_xml,
            'xml.etree': self.mock_xml.etree,
            'xml.etree.ElementTree': self.mock_xml.etree.ElementTree,
            'shlex': self.mock_shlex
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_hp_san
        self.module = check_hp_san

    def test_XPATH_STR(self):
        exp_str = ".//OBJECT[@basetype='{0}']/PROPERTY[@name='health']"
        self.assertEqual(self.module.XPATH_STR, exp_str)

    def test_supplies_template(self):
        exp_msg = {'part_attr': 'position',
                   'part_msg': 'Power Supply at the position - {0},',
                   'plural': 'Power Supplies',
                   'req_attrs': ('health', 'health-reason', 'position'),
                   'singular': 'Power Supply'}
        self.assertEqual(exp_msg, self.module.supplies_template)

    def test_drives_template(self):
        exp_msg = {'req_attrs': ('health', 'health-reason', 'location'),
                   'part_msg': 'In Enclousure {0} - Slot {1}, disk ',
                   'plural': 'Disks', 'part_attr': 'location',
                   'singular': 'Disk'}
        self.assertEqual(exp_msg, self.module.drives_template)

    def test_controller_template(self):
        exp_msg = {'req_attrs': ('health', 'health-reason', 'position'),
                   'part_msg': 'Controller at the position - {0},',
                   'plural': 'Controllers', 'part_attr': 'position',
                   'singular': 'Controller'}
        self.assertEqual(exp_msg, self.module.controller_template)

    def test_volume_template(self):
        exp_msg = {'req_attrs': ('health', 'health-reason', 'volume-name'),
                   'part_msg': 'Volume with name - {0},', 'plural': 'Volumes',
                   'part_attr': 'volume-name', 'singular': 'Volume'}
        self.assertEqual(exp_msg, self.module.volume_template)

    def test_san_template(self):
        exp_msg = {'controllers': {'part_attr': 'position',
                                   'part_msg': 'Controller at the position - {0},',
                                   'plural': 'Controllers',
                                   'req_attrs': ('health', 'health-reason', 'position'),
                                   'singular': 'Controller'},
                   'critical_msg': 'CRITICAL - Out of {0}, {1} {2} Failed',
                   'drives': {'part_attr': 'location',
                              'part_msg': 'In Enclousure {0} - Slot {1}, disk ',
                              'plural': 'Disks',
                              'req_attrs': ('health', 'health-reason', 'location'),
                              'singular': 'Disk'},
                   'power-supplies': {'part_attr': 'position',
                                      'part_msg': 'Power Supply at the position - {0},',
                                      'plural': 'Power Supplies',
                                      'req_attrs': ('health', 'health-reason', 'position'),
                                      'singular': 'Power Supply'},
                   'status_msg': '\n{cnt}. {part_msg}failed with Status - {health},Health Reason - {health_reason}',
                   'succes_msg': 'OK: HP SAN - {0} health is bright and sunny, found {1} {2}',
                   'volumes': {'part_attr': 'volume-name',
                               'part_msg': 'Volume with name - {0},',
                               'plural': 'Volumes',
                               'req_attrs': ('health', 'health-reason', 'volume-name'),
                               'singular': 'Volume'}}
        self.assertEqual(self.module.SAN_TEMPLATE, exp_msg)

    def test_get_part_msg_drives(self):
        part_msg_raw = 'In Enclousure {0} - Slot {1}, disk '
        part_msg = self.module.get_part_msg(
            part_msg_raw, 'drives', {'attr': '1.23'}, 'attr')
        exp_msg = 'In Enclousure 1 - Slot 23, disk '
        self.assertEqual(part_msg, exp_msg)

    def test_get_part_msg_non_drives(self):
        part_msg_raw = 'Controller at the position - {0},'
        part_msg = self.module.get_part_msg(
            part_msg_raw, 'nondrive', {'attr': 'top'}, 'attr')
        exp_msg = 'Controller at the position - top,'
        self.assertEqual(part_msg, exp_msg)

    def test_plural(self):
        plural = self.module.plural_or_singular(10)
        self.assertEqual(plural, 'plural')

    def test_singular(self):
        plural = self.module.plural_or_singular(1)
        self.assertEqual(plural, 'singular')

    def test_get_critical_msg(self):
        critical_msg = 'CRITICAL - Out of {0}, {1} {2} Failed'
        self.module.SAN_TEMPLATE = {'critical_msg': critical_msg}
        mock_plural = MagicMock(name='plural_or_singular')
        mock_plural.return_value = 'plural'
        self.module.plural_or_singular = mock_plural
        template = {'plural': 'plural'}
        crit_msg = self.module.get_critical_msg(
            'san_type', template, [1, 2], 3)
        self.assertEqual(crit_msg, 'CRITICAL - Out of 3, 2 plural Failed')

    def test_compose_err_status(self):
        mock_critical_msg = MagicMock(name='get_critical_msg')
        mock_critical_msg.return_value = 'err_msg'
        self.module.get_critical_msg = mock_critical_msg
        mock_part_msg = MagicMock(name='get_part_msg')
        mock_part_msg.return_value = 'part_msg '
        self.module.get_part_msg = mock_part_msg
        err_list = [{'health': 'health', 'health-reason': 'hr'}]
        status_msg = ('\n{cnt}. {part_msg}'
                      'failed with Status - {health},'
                      'Health Reason - {health_reason}')

        self.module.SAN_TEMPLATE['status_msg'] = status_msg
        template = {'part_msg': 'part_msg', 'part_attr': 'part_attr'}
        err_status = self.module.compose_err_status(
            'san_type', err_list, 10, template)
        exp_response = 'err_msg\n1. part_msg failed with Status - health,Health Reason - hr'
        self.assertEqual(err_status, exp_response)

    def test_get_xpath_str(self):
        self.module.XPATH_STR = ' yes {0}'
        self.assertEqual(self.module.get_xpath_str('replace_str'),
                         ' yes replace_str')

    def test_clean_xml(self):
        mock_in_xml = MagicMock(name='in_xml')
        mock_in_xml.splitlines.return_value = ['removed', 'removed', 'not_removed1',
                                               'not_removed2', 'removed']
        clean_xml = self.module.cleanup_xml(mock_in_xml)
        self.assertEqual(clean_xml, 'not_removed1not_removed2')

    def test_ok_status_True(self):
        mock_root = MagicMock(name='root')
        mock_o_nodes = MagicMock(name='o_nodes')
        mock_o_nodes.attrib = {'basetype': 'xpath'}
        mock_p_nodes = MagicMock(name='p_node')
        mock_p_nodes.attrib = {'name': 'health'}
        mock_p_nodes.text = 'OK'
        mock_o_nodes.findall.return_value = [mock_p_nodes]
        mock_root.findall.return_value = [mock_o_nodes]
        self.assertEqual(self.module.ok_status(mock_root, 'xpath'),
                         (True, [], 1))
        mock_root.findall.assert_called_once_with('OBJECT')
        mock_o_nodes.findall.assert_called_once_with('PROPERTY')

    def test_ok_status_False(self):
        mock_root = MagicMock(name='root')
        mock_o_nodes = MagicMock(name='o_nodes')
        mock_o_nodes.attrib = {'basetype': 'xpath'}
        mock_p_nodes = MagicMock(name='p_node')
        mock_p_nodes.attrib = {'name': 'health'}
        mock_p_nodes.text = 'Fault'
        mock_o_nodes.findall.return_value = [mock_p_nodes]
        mock_root.findall.return_value = [mock_o_nodes]
        self.assertEqual(self.module.ok_status(mock_root, 'xpath'),
                         (False, [mock_o_nodes], 1))
        mock_root.findall.assert_called_once_with('OBJECT')
        mock_o_nodes.findall.assert_called_once_with('PROPERTY')

    def test_get_formatted_status(self):
        mock_compose_err_status = MagicMock(name='compose_err_status')
        mock_compose_err_status.return_value = 'err'
        mock_err_item = MagicMock(name='err_item')
        mock_node = MagicMock(name='node')
        # details is not there in template's req_attrs
        # so it won't be added
        self.module.compose_err_status = mock_compose_err_status
        mock_node.attrib = {'name': 'health', 'details': 'd1'}
        mock_node.text = 'healthy'
        mock_err_item.findall.return_value = [mock_node]
        err_items = [mock_err_item]
        template = {'req_attrs': ['health']}
        formed_status = self.module.get_formatted_status(
            'san_type', err_items, 3, template)
        self.assertEqual(formed_status, 'err')
        mock_compose_err_status.assert_called_once_with('san_type',
                                                        [{'health': 'healthy'}],
                                                        3, template)

    def test_get_formatted_status_node_text_None(self):
        mock_compose_err_status = MagicMock(name='compose_err_status')
        mock_compose_err_status.return_value = 'err'
        mock_err_item = MagicMock(name='err_item')
        mock_node = MagicMock(name='node')
        mock_node.attrib = {'name': 'health'}
        mock_node.text = None  # empty string will be added
        mock_err_item.findall.return_value = [mock_node]
        err_items = [mock_err_item]
        template = {'req_attrs': ['health']}
        self.module.compose_err_status = mock_compose_err_status
        formed_status = self.module.get_formatted_status(
            'san_type', err_items, 3, template)
        self.assertEqual(formed_status, 'err')
        mock_compose_err_status.assert_called_once_with('san_type',
                                                        [{'health': ''}],
                                                        3, template)

    def test_success_msg(self):
        template = {'drives': {'plural': 'plu'}}
        template['succes_msg'] = '{0} {1} {2}'
        self.module.SAN_TEMPLATE = template
        mock_plu_sing = MagicMock(name='plural_or_singular')
        mock_plu_sing.return_value = 'plural'
        self.module.plural_or_singular = mock_plu_sing
        msg = self.module.success_msg('drives', 2)
        self.assertEqual(msg, 'drives 2 plu')

    def test_get_san_status_invalid_cred(self):
        mock_popen = MagicMock(name='popen')
        mock_popen.communicate.side_effect = [('', ''), ('', '')]
        self.mock_subprocess.Popen.return_value = mock_popen
        resp = self.module.get_san_status('h1', 'u1', 'p1', 'c1', 'x1')
        exp_resp = (2, 'Unable to connect to SAN. Invalid credentials.')
        self.assertEqual(resp, exp_resp)
        self.assertEqual(mock_popen.communicate.call_count, 2)
        exp_shlex_calls = [call('sshpass -p p1! ssh -o StrictHostKeyChecking=no u1@h1'),
                           call('sshpass -p p1 ssh -o StrictHostKeyChecking=no u1@h1')]
        self.assertEqual(self.mock_shlex.split.call_args_list, exp_shlex_calls)
        add_call_args = [call(['c1']), call(['c1'])]
        self.assertEqual(self.mock_shlex.split(
        ).__add__.call_args_list, add_call_args)
        popen_call_args = [call(self.mock_shlex.split.return_value.__add__.return_value, stderr=self.mock_subprocess.PIPE, stdout=self.mock_subprocess.PIPE
                                )] * 2
        self.assertEqual(
            self.mock_subprocess.Popen.call_args_list, popen_call_args)

    def test_get_san_status_status_False(self):
        mock_popen = MagicMock(name='popen')
        mock_popen.communicate.return_value = 'xml_out', ''
        self.mock_subprocess.Popen.return_value = mock_popen
        self.module.SAN_TEMPLATE = {'x1': 'x1'}
        mock_cleanup_xml = MagicMock(name='cleanup_xml')
        mock_cleanup_xml.return_value = "clean_xml"
        self.module.cleanup_xml = mock_cleanup_xml
        mock_ok_status = MagicMock(name='ok_status')
        mock_ok_status.return_value = False, "unhealthy_list", "total_count"
        self.module.ok_status = mock_ok_status
        mock_get_f_sts = MagicMock(name='get_formatted_status')
        mock_get_f_sts.return_value = 'Error'
        self.module.get_formatted_status = mock_get_f_sts
        resp = self.module.get_san_status('h1', 'u1', 'p1', 'c1', 'x1')
        self.assertEqual(resp, (2, 'Error'))
        self.mock_xml.etree.ElementTree.fromstring.assert_called_once_with(
            'clean_xml')
        mock_ok_status.assert_called_once_with(
            self.mock_xml.etree.ElementTree.ElementTree.return_value.getroot.return_value, 'x1')
        mock_get_f_sts.assert_called_once_with(
            'x1', 'unhealthy_list', 'total_count', 'x1')
        mock_cleanup_xml.assert_called_once_with('xml_out')

    def test_get_san_status_status_True(self):
        mock_popen = MagicMock(name='popen')
        mock_popen.communicate.return_value = 'xml_out', ''
        self.mock_subprocess.Popen.return_value = mock_popen
        self.module.SAN_TEMPLATE = {'x1': 'x1'}
        mock_cleanup_xml = MagicMock(name='cleanup_xml')
        mock_cleanup_xml.return_value = "clean_xml"
        self.module.cleanup_xml = mock_cleanup_xml
        mock_ok_status = MagicMock(name='ok_status')
        mock_ok_status.return_value = True, "unhealthy_list", "total_count"
        self.module.ok_status = mock_ok_status
        mock_get_f_sts = MagicMock(name='get_formatted_status')
        mock_get_f_sts.return_value = 'Error'
        self.module.get_formatted_status = mock_get_f_sts
        mock_success_msg = MagicMock(name='success_msg')
        mock_success_msg.return_value = 'success'
        self.module.success_msg = mock_success_msg
        resp = self.module.get_san_status('h1', 'u1', 'p1', 'c1', 'x1')
        self.assertEqual(resp, (0, 'success'))
        self.mock_xml.etree.ElementTree.fromstring.assert_called_once_with(
            'clean_xml')
        mock_ok_status.assert_called_once_with(
            self.mock_xml.etree.ElementTree.ElementTree.return_value.getroot.return_value, 'x1')
        self.assertEqual(mock_get_f_sts.call_count, 0)
        mock_cleanup_xml.assert_called_once_with('xml_out')
        mock_success_msg.assert_called_once_with('x1', 'total_count')

    def test_get_san_status_exception(self):
        mock_popen = MagicMock(name='popen')
        mock_popen.communicate.side_effect = AttributeError('err')
        self.mock_subprocess.Popen.return_value = mock_popen
        resp = self.module.get_san_status('h1', 'u1', 'p1', 'c1', 'x1')
        self.assertEqual(resp, (2, 'HP SAN - Critical error. err'))

    def test_check_args(self):
        result = self.module.check_args()
        exp_result = (self.mock_argparse.ArgumentParser.return_value.parse_args().hostname,
                      self.mock_argparse.ArgumentParser.return_value.parse_args().username,
                      self.mock_argparse.ArgumentParser.return_value.parse_args().password,
                      self.mock_argparse.ArgumentParser.return_value.parse_args().command,
                      self.mock_argparse.ArgumentParser.return_value.parse_args().xpath)
        self.assertEqual(result, exp_result)

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_check_args = MagicMock(name='check_args')
        mock_check_args.return_value = 'a', 'b'
        self.module.check_args = mock_check_args
        mock_get_san_status = MagicMock(name='get_san_status')
        mock_get_san_status.return_value = 'state', 'msg'
        self.module.get_san_status = mock_get_san_status
        self.module.main()
        self.assertEqual(std_out.getvalue(), 'msg\n')
        self.sys_mock.exit.assert_called_once_with('state')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
