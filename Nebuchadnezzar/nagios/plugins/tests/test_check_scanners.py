import unittest
from mock import MagicMock, patch

class CheckScannersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.config_reader': self.mock_scanline.utilities.config_reader,
            'scanline.product': self.mock_scanline.product,
            'scanline.product.isp': self.mock_scanline.product.isp
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_scanners
        self.module = check_scanners

    def test_get_status(self):
        state, msg = self.module.get_status('1.1.1.1',True)
        self.module.get_isp_version = MagicMock(name='get_isp_version',return_value='4,4,553,20')
        self.assertEqual(msg, 'OK: ISP and vCenter Scanners are present in discovery.yml and current ISP version(4,4,553,20) is greater than 4.4')
        self.assertEqual(state, 0)   
        state, msg = self.module.get_status('1.1.1.1',False)
        self.assertEqual(msg, 'CRITICAL: ISP Scanner is present but vCenter Scanner is not available in discovery.yml')
        self.assertEqual(state, 2)
        state, msg = self.module.get_status(None,True)
        self.assertEqual(msg, 'CRITICAL: vCenter Scanner is present but ISP Scanner is not available in discovery.yml')
        self.assertEqual(state, 2)
        state, msg = self.module.get_status(None,False)
        self.assertEqual(msg, 'CRITICAL: ISP and vCenter Scanners are not present in discovery.yml')
        self.assertEqual(state, 2)

    def test_get_status1(self):
        self.module.get_isp_version = MagicMock(name='get_isp_version',return_value='4,1,553,20')
        state, msg = self.module.get_status('1.1.1.1',True)
        self.assertEqual(msg, 'CRITICAL: ISP and vCenter Scanners are present in discovery.yml but current ISP version(4,1,553,20) is less than 4.4')
        self.assertEqual(state, 2)

    def test_get_status2(self):
        self.module.get_isp_version = MagicMock(name='get_isp_version',return_value='3,3,553,20')
        state, msg = self.module.get_status('1.1.1.1',True)
        self.assertEqual(msg, 'CRITICAL: ISP and vCenter Scanners are present in discovery.yml but current ISP version(3,3,553,20) is less than 4.4')
        self.assertEqual(state, 2)

    def test_get_status3(self):
        self.module.get_isp_version = MagicMock(name='get_isp_version',return_value=None)
        state, msg = self.module.get_status('1.1.1.1',True)
        self.assertEqual(msg, 'CRITICAL: ISP and vCenter Scanners are present but ISP version cannot be fetched')
        self.assertEqual(state, 2)


    def test_check_scanners(self):
        self.mock_scanline.utilities.config_reader.discovery_yml_contents.return_value = [{'scanner': 'vcenter', 'tags': 'vcenter', 'address': 'address',
                                      'username': 'username', 'password': 'password'},{'scanner': 'isp', 'tags': 'isp', 'address': 'address1',
                                      'username': 'username1', 'password': 'password1'}]
        self.module.get_isp_version = MagicMock(name='get_isp_version',return_value='4,4,553,20') 
        status, msg = self.module.check_scanners()
        self.assertEqual(status, 0)
        test_msg = 'OK: ISP and vCenter Scanners are present in discovery.yml and current ISP version(4,4,553,20) is greater than 4.4'
        self.assertEqual(msg,test_msg)

    def test_main(self):
        self.module.check_scanners = MagicMock(name='check_scanners',return_value=(0,'xyz'))
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
