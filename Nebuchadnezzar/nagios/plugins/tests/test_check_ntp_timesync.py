import unittest
from mock import MagicMock, patch
from requests.exceptions import RequestException
from StringIO import StringIO


class NtpTimeSyncTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_sys = MagicMock(name='sys')
        self.mock_subprocess = MagicMock(name='subprocess')
        modules = {
            'subprocess': self.mock_subprocess,
            'requests': self.mock_request,
            'sys': self.mock_sys
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_ntp_timesync
        self.module = check_ntp_timesync

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_lb_time_ok(self):
        mock_get = MagicMock(name='get_request')
        mock_get.headers = {'Date': 'Tue, 31 Mar 2020 15:11:25 GMT', 'Connection': 'keep-alive',
                            'Content-Type': 'text/html; charset=UTF-8', 'Content-Length': '9', 'Server': 'nginx/1.0.15'}
        self.mock_request.get.return_value = mock_get
        response = self.module.get_lb_time()
        self.assertEqual(response, 'Tue, 31 Mar 2020 15:11:25')

    def test_get_lb_time_exception(self):
        self.mock_request.get.side_effect = Exception('Error msg')
        response = self.module.get_lb_time()
        self.assertEqual(response, (2, 'Error msg'))

    def test_sync_time(self):
        response = 'Tue, 31 Mar 2020 15:11:25'
        self.module.sync_time(response)
        self.assertEqual(self.mock_subprocess.Popen.call_count, 2)

    def test_main_ok(self):
        response = 'Tue, 31 Mar 2020 15:11:25'
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.get_lb_time = MagicMock(name='get_lb_time', return_value=response)
        self.module.sync_time = MagicMock(name='sync_time', return_value=True)
        self.module.main()
        self.sys_mock.exit.assert_called_once_with(0)
        self.assertEqual(self.module.sync_time.call_count, 1)

    def test_main_exception(self):
        response = 'Tue, 31 Mar 2020 15:11:25'
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.get_lb_time = MagicMock(name='get_lb_time', return_value=response)
        self.module.sync_time = MagicMock(name="sync_time")
        self.module.sync_time.side_effect= Exception('Error msg')
        self.module.main()
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(self.module.get_lb_time.call_count, 1)

    def test_main_else(self):
        response = (0, 'Successfully Updated Time')
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.get_lb_time = MagicMock(name='get_lb_time', return_value=response)
        self.module.main()
        self.sys_mock.exit.assert_called_once_with(0)
        self.assertEqual(self.module.get_lb_time.call_count, 1)


if __name__ == '__main__':
    unittest.main()