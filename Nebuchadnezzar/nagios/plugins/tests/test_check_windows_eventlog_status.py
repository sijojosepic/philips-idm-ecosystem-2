from StringIO import StringIO
import unittest
from mock import MagicMock, patch, Mock


class WinEventLogParserTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            'sys': self.mock_sys,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
            'scanline.utilities.utils': self.mock_scanline.utilities
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        import check_windows_eventlog_status
        self.module = check_windows_eventlog_status
        self.WinEventLogParser = check_windows_eventlog_status.WinEventLogParser
        hostname = 'localhost'
        username = 'user1'
        password = 'pass'
        source = 'DFSR'
        hours = 10
        eventid = "['5002']"
        self.win_eventlog_parser = self.WinEventLogParser(hostname, username, password, source, hours, eventid)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.source = 'DFSR'
        mock_result.hours = '10'
        mock_result.eventid = "['5002']"
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_args()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'DFSR', '10', "['5002']"))

    def test_from_time_query(self):
        response = "$from_time = (Get-Date).AddHours(-10)"
        self.assertEqual(self.win_eventlog_parser.from_time_query(), response)

    def test_event_query(self):
        response = "$result = Get-WinEvent -ProviderName  DFSR | where-object {$_.Id -In  -And $_.LevelDisplayName -eq 'Error' -And $_.TimeCreated -ge  $from_time }| Sort-Object TimeCreated -Descending |ForEach-Object {($_.Message -split '\n')[0].Trim() +'|'+ $_.Id}; $result"
        self.assertEqual(self.win_eventlog_parser.event_query(), response)

    def test_ps_query(self):
        response = "$from_time = (Get-Date).AddHours(-10);$result = Get-WinEvent -ProviderName  DFSR | where-object {$_.Id -In  -And $_.LevelDisplayName -eq 'Error' -And $_.TimeCreated -ge  $from_time }| Sort-Object TimeCreated -Descending |ForEach-Object {($_.Message -split '\n')[0].Trim() +'|'+ $_.Id}; $result"
        self.assertEqual(self.win_eventlog_parser.ps_query(), response)

    def test_format_msg_critical(self):
        eventid = 5002
        count = 10
        err_msg = 'EventId-5002,Count-17, The DFS Replication Error'
        response = 'EventId-5002,Count-10, EventId-5002,Count-17, The DFS Replication Error\n'
        self.assertEqual(self.win_eventlog_parser.format_msg(eventid, count, err_msg), response)

    def test_format_msg_ok(self):
        eventid = 1000
        count = None
        err_msg = None
        response = 'EventId-1000,No Error Log Found.\n'
        self.assertEqual(self.win_eventlog_parser.format_msg(eventid, count, err_msg), response)

    def test_compose_status_critical(self):
        self.mock_scanline.utilities.utils.str_to_list.return_value = ['5002', '5008']
        self.win_eventlog_parser.event_ids = ['5002', '5008']
        response = (2,
                    'CRITICAL : EventId-5008,Count-1, The DFS Replication service error\nOK : EventId-5002,No Error Log Found.\n')
        status_dict = {'5008': {'count': 1,
                                'err_msg': 'The DFS Replication service error'}}
        self.assertEqual(self.win_eventlog_parser.compose_status(status_dict), response)

    def test_aggregate_log(self):
        self.win_eventlog_parser.eventid = MagicMock(name='eventid', return_value="['5002']")
        event_log = "The DFS Replication service error| 5008"
        response = {' 5008': {'count': 1,
                              'err_msg': 'The DFS Replication service error'}}
        self.assertEqual(self.win_eventlog_parser.aggregate_log(event_log), response)

    def test_compose_msg(self):
        ok_msg = 'EventId-1000,No Error Log Found.\n'
        crt_msg = 'EventId-5002,Count-17, The DFS Replication error'
        response = 'CRITICAL : EventId-5002,Count-17, The DFS Replication errorOK : EventId-1000,No Error Log Found.\n'
        self.assertEqual(self.win_eventlog_parser.compose_msg(ok_msg, crt_msg), response)

    def test_get_status_ok(self):
        event_log = ""
        self.win_eventlog_parser.aggregate_log = MagicMock(name='aggregate_log')
        self.win_eventlog_parser.compose_status = MagicMock(name='compose_status',
                                                            return_value=(0, 'OK : EventId-5002,No Error Log Found.'))
        self.assertEqual(self.win_eventlog_parser.get_status(event_log),
                         (0, 'OK : EventId-5002,No Error Log Found.'))

    def test_get_status_critical(self):
        event_log = "The DFS Replication error.|5008"
        response = (2, 'CRITICAL : EventId-5008,Count-1, The DFS Replication error.')
        self.win_eventlog_parser.aggregate_log = MagicMock(name='aggregate_log')
        self.win_eventlog_parser.compose_status = MagicMock(name='compose_status', return_value=response)
        self.assertEqual(self.win_eventlog_parser.get_status(event_log), response)

    def test_execute_powershell_ok_case1(self):
        mock_output = MagicMock(name="output")
        mock_output.status_code = 1
        mock_output.std_out = 'Exception Error'
        status = 0
        self.mock_scanline.utilities.win_rm.exec_ps_script.return_value = status, mock_output
        self.assertEqual(self.win_eventlog_parser.execute_powershell(),
                         (0, 'CRITICAL : Exception Error| service status=2;1;2;0;2'))

    def test_execute_powershell_ok_case2(self):
        mock_output = MagicMock(name="output")
        mock_output.status_code = 0
        mock_output.std_out = True
        status = 0
        self.mock_scanline.utilities.win_rm.exec_ps_script.return_value = status, mock_output
        msg = 'CRITICAL : EventId-5008,Count-1, The DFS Replication error.'
        status = 2
        self.win_eventlog_parser.get_status = MagicMock(name='status', return_value=(status, msg))
        self.assertEqual(self.win_eventlog_parser.execute_powershell(),
                         (2, 'CRITICAL : EventId-5008,Count-1, The DFS Replication error.| service status=2;1;2;0;2'))

    def test_execute_powershell_ok_case3(self):
        mock_output = MagicMock(name="output")
        mock_output.status_code = 0
        mock_output.std_out = True
        status = 0
        self.mock_scanline.utilities.win_rm.exec_ps_script.return_value = status, mock_output
        msg = 'OK : EventId-5002,No Error Log Found.'
        status = 0
        self.win_eventlog_parser.get_status = MagicMock(name='status', return_value=(status, msg))
        self.assertEqual(self.win_eventlog_parser.execute_powershell(),
                         (0, 'OK : EventId-5002,No Error Log Found.| service status=0;1;2;0;2'))

    def test_execute_powershell_ok_else(self):
        err_msg = 'Exception Error'
        status = 2
        self.mock_scanline.utilities.win_rm.exec_ps_script.return_value = status, err_msg
        self.assertEqual(self.win_eventlog_parser.execute_powershell(),
                         (2, 'Exception Error| service status=2;1;2;0;2'))

    def test_main_ok(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_obj = MagicMock(name='execute_powershell')
        mock_obj.execute_powershell.return_value = 0, 'ok_msg'
        self.module.WinEventLogParser = MagicMock(name="WinEventLogParser", return_value=mock_obj)
        hostname = 'localhost'
        username = 'test1'
        password = 'pass1'
        source = 'DFSR'
        hours = '10'
        eventid = "['5002']"
        self.module.main(hostname, username, password, source, hours, eventid)
        self.sys_mock.exit.assert_called_once_with(0)
        mock_obj.execute_powershell.assert_called_once_with()

    def test_main_critical(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_obj = MagicMock(name='execute_powershell')
        mock_obj.execute_powershell.return_value = 1, 'crt_msg'
        self.module.WinEventLogParser = MagicMock(name="WinEventLogParser", return_value=mock_obj)
        hostname = 'localhost'
        username = 'test1'
        password = 'pass1'
        source = 'DFSR'
        hours = '10'
        eventid = "['5002']"
        self.module.main(hostname, username, password, source, hours, eventid)
        self.sys_mock.exit.assert_called_once_with(1)
        mock_obj.execute_powershell.assert_called_once_with()

    def test_main_else(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_obj = MagicMock(name='execute_powershell')
        mock_obj.execute_powershell.return_value = (0, 'Try after setting this variable - $DOMAIN_EVENT_ID$')
        self.module.WinEventLogParser = MagicMock(name="WinEventLogParser", return_value=mock_obj)
        hostname = 'localhost'
        username = 'test1'
        password = 'pass1'
        source = 'DFSR'
        hours = '10'
        eventid = None
        self.module.main(hostname, username, password, source, hours, eventid)
        self.sys_mock.exit.assert_called_once_with(0)

    def test_main_Exception(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_obj = MagicMock(name='execute_powershell')
        mock_obj.execute_powershell.return_value = (0, 'Try after setting this variable - $DOMAIN_EVENT_ID$')
        self.module.WinEventLogParser = MagicMock(name="WinEventLogParser", return_value=mock_obj)
        self.module.WinEventLogParser.side_effect = Exception()
        hostname = 'localhost'
        username = 'test1'
        password = 'pass1'
        source = 'DFSR'
        hours = '10'
        eventid = "['5002']"
        self.module.main(hostname, username, password, source, hours, eventid)
        self.sys_mock.exit.assert_called_once_with(2)


if __name__ == '__main__':
    unittest.main()
