import unittest
from mock import patch, MagicMock
from collections import defaultdict






class CheckClusterLHOverLevelTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_sys = MagicMock(name='sys')
        modules = {
            
            'argparse': self.mock_argparse,
            'sys': self.mock_sys
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.items = defaultdict(list)
        import check_cluster_lh
        self.check_cluster_lh = check_cluster_lh

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        obj = MagicMock()
        obj2 = MagicMock()
        obj2.label = 'del'
        obj2.warning = 'ho'
        obj2.critical = 'hn'
        obj2.level = 's'
        obj2.data = 'd'
        obj.parse_args = MagicMock(return_value=obj2)
        self.mock_argparse.ArgumentParser = MagicMock(return_value = obj)
        self.assertEqual(self.check_cluster_lh.check_arg(),('del','ho', 'hn', 's', 'd'))

    def test_main(self):
        self.check_cluster_lh.check_cluster_lh = MagicMock(return_value=(2,2))
        self.mock_sys.exit = MagicMock()
        self.check_cluster_lh.main()
    
    def test_empty(self):
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 1), (0, ''))

    def test_empty_2(self):
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 2), (0, ''))

    def test_one_warning_level_critical(self):
        self.items[1].append('service1')
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 2), (0, ''))

    def test_one_warning_level_warning(self):
        self.items[1].append('service1')
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 1), (1, '[ service1 ]'))

    def test_one_critical_level_warning(self):
        self.items[2].append('service1')
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 1), (1, '[ service1 ]'))

    def test_two_critical_level_warning(self):
        self.items[2] = ['service1', 'service2']
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 1), (2, '[ service1, service2 ]'))

    def test_two_critical_level_critical(self):
        self.items[2] = ['service1', 'service2']
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 2), (2, '[ service1, service2 ]'))

    def test_two_each_level_critical(self):
        self.items[0] = ['service1', 'service2']
        self.items[1] = ['service3', 'service4']
        self.items[2] = ['service5', 'service6']
        self.items[3] = ['service7', 'service8']
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 2), (2, '[ service5, service6 ]'))

    def test_two_each_level_warning(self):
        self.items[0] = ['service1', 'service2']
        self.items[1] = ['service3', 'service4']
        self.items[2] = ['service5', 'service6']
        self.items[3] = ['service7', 'service8']
        self.assertEqual(self.check_cluster_lh.over_level(self.items, 1), (4, '[ service5, service6, service3, service4 ]'))

class CheckClusterLHLevelGroupsFromListTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        modules = {
            
            'argparse': self.mock_argparse,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        self.items = defaultdict(list)
        import check_cluster_lh
        self.check_cluster_lh = check_cluster_lh

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    def test_empty(self):
        self.assertEqual(self.check_cluster_lh.level_groups_from_list([]), {})

    def test_two_groups(self):
        self.assertEqual(
            self.check_cluster_lh.level_groups_from_list([(0, 'service1'), (3, 'service2')]),
            {0: ['service1'], 3: ['service2']}
        )

    def test_one_of_each_groups(self):
        self.assertEqual(
            self.check_cluster_lh.level_groups_from_list(
                [(0, 'service1'), (3, 'service2'), (1, 'service3'), (2, 'service4')]
            ),
            {0: ['service1'], 1: ['service3'], 2: ['service4'], 3: ['service2']}
        )

    def test_two_of_each_groups(self):
        self.assertEqual(
            self.check_cluster_lh.level_groups_from_list(
                [(0, 'service1'), (3, 'service2'), (1, 'service3'), (2, 'service4'),
                 (0, 'service5'), (3, 'service6'), (1, 'service7'), (2, 'service8')]
            ),
            {0: ['service1', 'service5'], 1: ['service3', 'service7'],
             2: ['service4', 'service8'], 3: ['service2', 'service6']}
        )


class CheckClusterLHOverviewMessageTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.items = defaultdict(list)
        import check_cluster_lh
        self.check_cluster_lh = check_cluster_lh


    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_empty(self):
        self.assertEqual(self.check_cluster_lh.overview_msg(self.items), '0 ok, 0 warning, 0 unknown, 0 critical')

    def test_one(self):
        self.items[1].append('service1')
        self.assertEqual(self.check_cluster_lh.overview_msg(self.items), '0 ok, 1 warning, 0 unknown, 0 critical')

    def test_two_of_each(self):
        self.items[0] = ['service1', 'service2']
        self.items[1] = ['service3', 'service4']
        self.items[2] = ['service5', 'service6']
        self.items[3] = ['service7', 'service8']
        self.assertEqual(self.check_cluster_lh.overview_msg(self.items), '2 ok, 2 warning, 2 unknown, 2 critical')


class CheckClusterLHTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.patch_over_level = patch('check_cluster_lh.over_level')
        self.filter_over_mock = self.patch_over_level.start()
        self.patch_overview = patch('check_cluster_lh.overview_msg')
        self.filter_overview = self.patch_overview.start()
        self.filter_overview.return_value = '#OVERVIEW#'
        self.patch_level_groups = patch('check_cluster_lh.level_groups_from_list')
        self.filter_level_groups = self.patch_level_groups.start()
        self.filter_level_groups.return_value = '#LEVEL_GROUPS#'
        import check_cluster_lh
        self.check_cluster_lh = check_cluster_lh


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.patch_over_level.stop()
        self.patch_overview.stop()
        self.patch_level_groups.stop()

    def test_no_thresholds(self):
        self.filter_over_mock.return_value = 8, 'CRITICAL'
        out = self.check_cluster_lh.check_cluster_lh('label x', None, None, 1, [])
        self.assertEqual(out, (0, 'label x: #OVERVIEW# CRITICAL'))

    def test_no_critical(self):
        self.filter_over_mock.return_value = 0, ''
        out = self.check_cluster_lh.check_cluster_lh('label x', 0, 2, 1, [])
        self.assertEqual(out, (0, 'label x: #OVERVIEW#'))

    def test_critical_not_greater_warning_greater(self):
        self.filter_over_mock.return_value = 2, 'CRITICAL'
        out = self.check_cluster_lh.check_cluster_lh('', 0, 2, 1, [])
        self.assertEqual(out, (1, '#OVERVIEW# CRITICAL'))

    def test_critical_greater(self):
        self.filter_over_mock.return_value = 4, 'CRITICAL'
        out = self.check_cluster_lh.check_cluster_lh('', None, 3, 1, [])
        self.assertEqual(out, (2, '#OVERVIEW# CRITICAL'))

    def test_warning(self):
        self.filter_over_mock.return_value = 3, 'CRITICAL'
        out = self.check_cluster_lh.check_cluster_lh('', 2, None, 1, [])
        self.assertEqual(out, (1, '#OVERVIEW# CRITICAL'))

    def test_critical_over_warning(self):
        self.filter_over_mock.return_value = 5, 'CRITICAL'
        out = self.check_cluster_lh.check_cluster_lh('', 2, 4, 1, [])
        self.assertEqual(out, (2, '#OVERVIEW# CRITICAL'))


class CheckClusterLevelListActionTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.items = defaultdict(list)
        import check_cluster_lh
        self.check_cluster_lh = check_cluster_lh


    def tearDown(self):
        unittest.TestCase.tearDown(self)

    def test_length_more_than_2(self):
        import argparse
        self.LevelListAction = self.check_cluster_lh.LevelListAction('parser', 'namespace', 'values')
        with self.assertRaises(argparse.ArgumentError): self.LevelListAction.__call__( 'parser', 'namespace', 'values')

    def test_length_more_than_2(self):
        import argparse
        self.LevelListAction = self.check_cluster_lh.LevelListAction('pr', 'namespace', 'va')
        self.LevelListAction.LEVELS = '1'
        obj = MagicMock()
        obj.namespace = MagicMock()
        self.LevelListAction.__call__( 'pr', obj, '1a')


    # def test_one(self):
    #     self.items[1].append('service1')
    #     self.assertEqual(self.check_cluster_lh.overview_msg(self.items), '0 ok, 1 warning, 0 unknown, 0 critical')


if __name__ == '__main__':
    unittest.main()
