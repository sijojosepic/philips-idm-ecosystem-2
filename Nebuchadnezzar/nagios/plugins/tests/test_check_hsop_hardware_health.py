#!/usr/bin/env python

import unittest
import json
from argparse import Namespace
from mock import MagicMock, patch, Mock
from requests.auth import HTTPBasicAuth

class TestCheckHSOPHardwareHealth(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_requests = MagicMock(name='requests')
        modules = {
            'requests': self.mock_requests
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_hsop_hardware_health
        from check_hsop_hardware_health import get_hardware_device_status, check_args
        self.check_args = check_args
        self.get_hardware_device_status = get_hardware_device_status
        self.USERNAME = 'administrator'
        self.PASSWORD = '1nf0M@t1cs'
        self.HOSTNAME = '172.17.19.29'
        self.HARDWARE_DEVICE = 'FAN'
        self.OK_STATUS = 0
        self.WARNING_STATUS = 1
        self.CRITICAL_STATUS = 2
        self.stubbed_ilo_system_response_ok = {"Oem": {"Hpe":{"AggregateHealthStatus": {"AgentlessManagementService": "Unavailable","BiosOrHardwareHealth": {"Status": {"Health": "OK"}},"FanRedundancy": "Redundant","Fans": {"Status": {"Health": "OK"}},
                                "Memory": {"Status": {"Health": "OK"}},"Network": {"Status": {"Health": "OK"}},"PowerSupplies": {"Status": {"Health": "OK"}},"PowerSupplyRedundancy": "Redundant",
                                "Processors": {"Status": {"Health": "OK"}},"SmartStorageBattery": {"Status": {"Health": "OK"}},"Storage": {"Status": {"Health": "OK"}},"Temperatures": {"Status": {"Health": "OK"}}}}}}
        
        self.stubbed_ilo_system_response_critical = {"Oem": {"Hpe":{"AggregateHealthStatus": {"AgentlessManagementService": "Unavailable","BiosOrHardwareHealth": {"Status": {"Health": "OK"}},"FanRedundancy": "Redundant","Fans": {"Status": {"Health": "CRITICAL"}},
                                "Memory": {"Status": {"Health": "CRITICAL"}},"Network": {"Status": {"Health": "OK"}},"PowerSupplies": {"Status": {"Health": "CRITICAL"}},"PowerSupplyRedundancy": "Redundant",
                                "Processors": {"Status": {"Health": "OK"}},"SmartStorageBattery": {"Status": {"Health": "CRITICAL"}},"Storage": {"Status": {"Health": "OK"}},"Temperatures": {"Status": {"Health": "CRITICAL"}}}}}}


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    @patch('argparse.ArgumentParser.parse_args')
    def test_check_arg(self, mock_argparse):
        mock_argparse.return_value = Namespace(hostname=self.HOSTNAME,username=self.USERNAME, password=self.PASSWORD,
                                                        hardware_device=self.HARDWARE_DEVICE)
        results = self.check_args()
        #print results
        assert results == (
            self.HOSTNAME, self.USERNAME, self.PASSWORD, self.HARDWARE_DEVICE)

    def test_get_fan_status(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_ok
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'FAN')        
        assert status ==  self.OK_STATUS
    
    def test_get_fan_status_critical(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_critical
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'FAN')        
        assert status ==  self.CRITICAL_STATUS
    
        
    def test_get_temperature_status(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_ok
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'TEMPERATURE')
        assert status ==  self.OK_STATUS
    
    def test_get_temperature_status_critical(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_critical
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'TEMPERATURE')
        assert status ==  self.CRITICAL_STATUS
        
    def test_get_voltage_status(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_ok
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'VOLTAGE')
        assert status ==  self.OK_STATUS
    
    def test_get_voltage_status_critical(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_critical
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'VOLTAGE')
        assert status ==  self.CRITICAL_STATUS
        
    def test_get_battery_status(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_ok
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'BATTERY')
        assert status ==  self.OK_STATUS
        
    def test_get_battery_status_critical(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_critical
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'BATTERY')
        assert status ==  self.CRITICAL_STATUS
        
    def test_get_memory_status(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_ok
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'MEMORY')
        assert status ==  self.OK_STATUS
        
    def test_get_memory_status_critical(self):        
        mock_get = Mock(ok=True, status_code=200)
        self.mock_requests.get.return_value = mock_get
        mock_get.json.return_value = self.stubbed_ilo_system_response_critical
        status, message = self.get_hardware_device_status(self.HOSTNAME, self.USERNAME, self.PASSWORD, 'MEMORY')
        assert status ==  self.CRITICAL_STATUS
        

if __name__ == '__main__':
    unittest.main()

