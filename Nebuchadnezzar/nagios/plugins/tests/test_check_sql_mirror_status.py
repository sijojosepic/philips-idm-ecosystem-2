from StringIO import StringIO
import unittest
import datetime

from mock import MagicMock, patch, Mock


class CheckSqlMirrorTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dbutils': self.mock_scanline.utilities.dbutils
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_mirror_status
        self.module = check_sql_mirror_status
        self.failover_result1 = [(datetime.datetime(2018, 8, 21, 13, 32, 32, 360000), u'spid6s',
                                  u'The mirrored database "StentorOperation" is changing roles from "MIRROR" to "PRINCIPAL" because the mirroring session or availability group failed over due to failover from partner. This is an informational message only. No user action is required.'),
                                 (datetime.datetime(2018, 8, 21, 13, 32, 32, 460000), u'spid38s',
                                  u'The mirrored database "StentorOperation" is changing roles from "PRINCIPAL" to "MIRROR" because the mirroring session or availability group failed over due to manual failover. This is an informational message only. No user action is required.')]
        self.failover_result2 = [(datetime.datetime(2018, 8, 21, 13, 32, 32, 460000), u'spid38s',
                                  u'The mirrored database "StentorOperation" is changing roles from "PRINCIPAL" to "MIRROR" because the mirroring session or availability group failed over due to manual failover. This is an informational message only. No user action is required.'),
                                 (datetime.datetime(2018, 8, 21, 13, 32, 32, 360000), u'spid6s',
                                  u'The mirrored database "StentorOperation" is changing roles from "MIRROR" to "PRINCIPAL" because the mirroring session or availability group failed over due to failover from partner. This is an informational message only. No user action is required.')]
        self.failover_result3 = [(datetime.datetime(2018, 8, 21, 13, 32, 32, 360000), u'spid6s',
                                  u'The mirrored database "StentorOperation" is changing roles from "MIRROR" to "PRINCIPAL" because the mirroring session or availability group failed over due to failover from partner. This is an informational message only. No user action is required.'),
                                 ('FLAG',)]
        self.failover_result4 = [('FLAG',), (datetime.datetime(2018, 8, 21, 13, 32, 32, 360000), u'spid6s',
                                             u'The mirrored database "StentorOperation" is changing roles from "MIRROR" to "PRINCIPAL" because the mirroring session or availability group failed over due to failover from partner. This is an informational message only. No user action is required.')]
        self.failover_result5 = [(datetime.datetime(2018, 8, 21, 13, 32, 32, 460000), u'spid38s',
                                  u'The mirrored database "StentorOperation" is changing roles from "PRINCIPAL" to "MIRROR" because the mirroring session or availability group failed over due to manual failover. This is an informational message only. No user action is required.'),
                                 ('FLAG',)]
        self.failover_result6 = [('FLAG',), (datetime.datetime(2018, 8, 21, 13, 32, 32, 460000), u'spid38s',
                                             u'The mirrored database "StentorOperation" is changing roles from "PRINCIPAL" to "MIRROR" because the mirroring session or availability group failed over due to manual failover. This is an informational message only. No user action is required.')]
        self.failover_result7 = [('FLAG',), ('FLAG',)]

        self.mock_db_hosts = MagicMock("DB_hosts")
        self.mock_state_result = MagicMock(name='state_result')
        self.mock_db_hosts.return_value = ['db1', 'db2']

    def test_check_arg_1(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.service = 'failover'
        mock_result.vigilant_url = 'http://vigilant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'failover', 'http://vigilant/'))

    def test_check_arg_2(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.service = 'splitbrain'
        mock_result.vigilant_url = 'https://vigilant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'splitbrain', 'https://vigilant/'))

    @patch('scanline.utilities.dbutils.get_mirroring_primary_check', return_value=False)
    def test_get_db_mirror_status_primary_check(self, primary_check):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(
            return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']])
        self.assertEqual(
            self.module.get_db_mirror_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'failover',
                                             'http://vigilant/'), (0,
                'OK: This service check only applicable for primary node. idm04db1.idm04.isyntax.net is not the primary node.'))

    @patch('scanline.utilities.dbutils.get_dbnodes', return_value=[])
    def test_get_db_mirror_status_no_dbnodes(self, mock_db_hosts):
        self.assertEqual(
            self.module.get_db_mirror_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'failover',
                                             'http://vigilant/'),(2,
             'CRITICAL - Unable to retrieve DB nodes due to connection issue with vigilant or DB nodes not reachable.'))

    @patch('scanline.utilities.dbutils.get_dbnodes', return_value=[['idm04db1.idm04.isyntax.net']])
    def test_get_db_mirror_status_single_dbnode(self, mock_db_hosts):
        self.assertEqual(
            self.module.get_db_mirror_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'splitbrain',
                                             'http://vigilant/'),(0, 'OK - This environment contains single DB node.'))

    @patch('scanline.utilities.dbutils.get_dbnodes',
           return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'], ['shd03db.shd03.isyntax.net']])
    def test_get_db_mirror_status_dbnode_exception(self, mock_db_hosts):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_db_mirror_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'splitbrain',
                                             'http://vigilant/'),
            (2, "CRITICAL - Connection error with 'idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'."))

    @patch('scanline.utilities.dbutils.get_dbnodes',
           return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']])
    def test_get_db_mirror_status_dbnode(self, mock_db_hosts):
        self.module.get_formatted_data = MagicMock(return_value="mocked")
        self.module.get_db_mirror_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'splitbrain',
                                         'http://vigilant/')
        self.assertEqual(self.mock_pymssql.connect.call_count, 2)

    def test_get_db_mirror_status_WARNING1_failover_1(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result1, 'failover'),
            (1, 'WARNING - Detected successful failover, db1 is now the primary.'))

    def test_get_db_mirror_status_WARNING2_failover_1(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result1, 'failover'),
            (1, 'WARNING - Detected successful failover, db1 is now the primary.'))

    def test_get_db_mirror_status_WARNING_failover_at_splitbrain_1(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result1, 'failover'),
            (1, 'WARNING - Failover transformation in progress.'))

    def test_get_db_mirror_status_WARNING1_failover_2(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result2, 'failover'),
            (1, 'WARNING - Detected successful failover, db2 is now the primary.'))

    def test_get_db_mirror_status_WARNING2_failover_2(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result2, 'failover'),
            (1, 'WARNING - Detected successful failover, db2 is now the primary.'))

    def test_get_db_mirror_status_WARNING_failover_at_splitbrain_2(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result2, 'failover'),
            (1, 'WARNING - Failover transformation in progress.'))

    def test_get_db_mirror_status_WARNING1_failover_3(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result3, 'failover'),
            (1, 'WARNING - Detected successful failover, db1 is now the primary.'))

    def test_get_db_mirror_status_WARNING2_failover_3(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result3, 'failover'),
            (1, 'WARNING - Detected successful failover, db1 is now the primary.'))

    def test_get_db_mirror_status_WARNING_failover_at_splitbrain_3(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result3, 'failover'),
            (1, 'WARNING - Failover transformation in progress.'))

    def test_get_db_mirror_status_WARNING1_failover_4(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result4, 'failover'),
            (1, 'WARNING - Detected successful failover, db2 is now the primary.'))

    def test_get_db_mirror_status_WARNING2_failover_4(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result4, 'failover'),
            (1, 'WARNING - Detected successful failover, db2 is now the primary.'))

    def test_get_db_mirror_status_WARNING_failover_at_splitbrain_4(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result4, 'failover'),
            (1, 'WARNING - Failover transformation in progress.'))

    def test_get_db_mirror_status_WARNING1_failover_5(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result5, 'failover'),
            (1, 'WARNING - Detected successful failover, db2 is now the primary.'))

    def test_get_db_mirror_status_WARNING2_failover_5(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result5, 'failover'),
            (1, 'WARNING - Detected successful failover, db2 is now the primary.'))

    def test_get_db_mirror_status_WARNING_failover_at_splitbrain_5(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result5, 'failover'),
            (1, 'WARNING - Failover transformation in progress.'))

    def test_get_db_mirror_status_WARNING1_failover_6(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result6, 'failover'),
            (1, 'WARNING - Detected successful failover, db1 is now the primary.'))

    def test_get_db_mirror_status_WARNING2_failover_6(self):
        self.mock_state_result.return_value = ['Flag Error', 'OK']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result6, 'failover'),
            (1, 'WARNING - Detected successful failover, db1 is now the primary.'))

    def test_get_db_mirror_status_WARNING_failover_at_splitbrain_6(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result6, 'failover'),
            (1, 'WARNING - Failover transformation in progress.'))

    def test_get_db_mirror_status_OK_failover(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result7, 'failover'),
            (0, 'OK - Primary database is up and running fine on db1'))

    def test_get_db_mirror_status_OK_splitbrain(self):
        self.mock_state_result.return_value = ['OK', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result7, 'splitbrain'),
            (0, 'OK - Primary database is up and running fine on db1'))

    def test_get_db_mirror_status_CRITICAL_splitbrain(self):
        self.mock_state_result.return_value = ['Flag Error', 'Flag Error']
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_state_result.return_value,
                                           self.failover_result7, 'splitbrain'), (2,
                                                                                  'CRITICAL - Detected split brain state.All the database needs to be made Primary on Single Database Server.'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_db_mirror_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_db_mirror_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_db_mirror_status = MagicMock(name='get_db_mirror_status')
        self.module.get_db_mirror_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_db_mirror_status = MagicMock(name='get_db_mirror_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_db_mirror_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
