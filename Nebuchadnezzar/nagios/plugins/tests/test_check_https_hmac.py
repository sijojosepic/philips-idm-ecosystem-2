import unittest
from StringIO import StringIO

from mock import MagicMock, patch
from requests.exceptions import RequestException


class CheckHttpsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_utilities = MagicMock(name='utilities')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'requests': self.mock_request,
            'requests.exceptions': self.mock_request.exceptions,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_utilities,
            'scanline.utilities.token_mgr': self.mock_utilities.token_mgr,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_https_hmac
        self.module = check_https_hmac

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_url(self):
        url = self.module.get_url('host', '/service_url', 'protocol')
        self.assertEqual(url, 'protocol://host/service_url')

    def test_parse_cmd_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.service_url = 'service_url'
        mock_result.protocol = 'protocol'
        mock_result.hmac = 'hmac'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.parse_cmd_args()
        self.assertEqual(response, ('localhost', 'service_url',
                                    'protocol', 'hmac'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_hmac(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        self.mock_utilities.token_mgr.do_hmac_request.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', True)
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_utilities.token_mgr.do_hmac_request.assert_called_once_with(
            'url', timeout=5)
        mock_get_url.assert_called_once_with('host', 'service_url', 'protocol')
        self.assertEqual(std_out.getvalue(), 'OK - 200 :OK\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_non_hmac(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        self.mock_request.get.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_request.get.assert_called_once_with('url', timeout=5, verify=False)
        mock_get_url.assert_called_once_with('host', 'service_url', 'protocol')
        self.assertEqual(std_out.getvalue(), 'OK - 200 :OK\n')
        mock_response.raise_for_status.assert_called_once_with()

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_no_respone(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 400
        mock_response.content = ''
        mock_response.__nonzero__.return_value = False
        self.mock_request.get.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(2)
        self.mock_request.get.assert_called_once_with('url', timeout=5, verify=False)
        mock_get_url.assert_called_once_with('host', 'service_url', 'protocol')
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Non 200 HTTP response status\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.side_effect = Exception('exe')
        self.module.get_url = mock_get_url
        mock_response = MagicMock(name='response_mock')
        mock_response.status_code = 200
        mock_response.content = 'OK'
        self.mock_request.get.return_value = mock_response
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception - exe\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_request_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.RequestException = RequestException
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.side_effect = RequestException('exe')
        self.module.get_url = mock_get_url
        self.module.main('host', 'service_url', 'protocol', '')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Request Error - exe\n')


if __name__ == '__main__':
    unittest.main()
