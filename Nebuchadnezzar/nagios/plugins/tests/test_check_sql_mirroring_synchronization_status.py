from StringIO import StringIO
import unittest
import mock

from mock import MagicMock, patch, Mock


class CheckSqlSynchronizationTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dbutils': self.mock_scanline.utilities.dbutils
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_mirroring_synchronization_status
        self.module = check_sql_mirroring_synchronization_status
        self.mock_db_hosts = MagicMock("DB_hosts")
        self.mock_sync_result = MagicMock(name='sync_result')
        self.mock_sync_health_result = MagicMock(name='sync_health_result')
        self.mock_db_hosts.return_value = ['db1', 'db2']

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.counterval = '102400'
        mock_result.vigilant_url = 'https://vigilant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', '102400', 'https://vigilant/'))

    @mock.patch('scanline.utilities.dbutils.get_mirroring_primary_check', return_value=False)
    def test_get_db_synchronization_status_primary_check(self, primary_check):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(
            return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']])
        self.assertEqual(
            self.module.get_db_mirroring_synchronization_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor',
                                                                'msdb', 102400, 'https://vigilant/'), (0,
                                                                                  'OK: This service check only applicable for primary node. idm04db1.idm04.isyntax.net is not the primary node.'))

    @mock.patch('scanline.utilities.dbutils.get_dbnodes', return_value=[])
    def test_get_db_synchronization_status_no_dbnodes(self, mock_db_hosts):
        self.assertEqual(
            self.module.get_db_mirroring_synchronization_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor',
                                                                'msdb', 102400, 'https://vigilant/'),
            (2,
             'CRITICAL - Unable to retrieve DB nodes due to connection issue with vigilant or DB nodes not reachable.'))

    @mock.patch('scanline.utilities.dbutils.get_dbnodes', return_value=[['idm04db1.idm04.isyntax.net']])
    def test_get_db_synchronization_status_single_dbnode(self, mock_db_hosts):
        self.assertEqual(
            self.module.get_db_mirroring_synchronization_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor',
                                                                'msdb', 102400, 'https://vigilant/'),
            (0, 'OK - This environment contains single DB node.'))

    @mock.patch('scanline.utilities.dbutils.get_dbnodes',
                return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']])
    def test_get_db_synchronization_status_dbnode_exception(self, mock_db_hosts):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_db_mirroring_synchronization_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor',
                                                                'msdb', 'splitbrain', 'https://vigilant/'),
            (2, "CRITICAL - Connection error with 'idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'."))

    @mock.patch('scanline.utilities.dbutils.get_dbnodes',
                return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net'],
                              ['shd03db.shd03.isyntax.net']])
    def test_get_db_mirror_status_dbnode(self, mock_db_hosts):
        self.module.get_formatted_data = MagicMock(return_value="mocked")
        self.module.get_db_mirroring_synchronization_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb',
                                                            102400, 'https://vigilant/')
        self.assertEqual(self.mock_pymssql.connect.call_count, 2)

    def test_get_db_synchronization_status_OK_backlog(self):
        self.mock_sync_result.return_value = []
        self.mock_sync_health_result.return_value = []
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_sync_result.return_value, 102400,
                                           self.mock_sync_health_result.return_value),
            (0, 'OK : Data synchronization is healthy'))

    def test_get_db_synchronization_status_CRITICAL1_backlog(self):
        self.mock_sync_result.return_value = [(u'Audit      ', 204800L), (u'Audit                ', 0L)]
        self.mock_sync_health_result.return_value = []
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_sync_result.return_value, 102400,
                                           self.mock_sync_health_result.return_value), (2,
                                                                                        'CRITICAL: Detected slow data synchronization between db1 and db2. Current backlog 200MB has reached threshold of 100MB. This could cause data loss on mirror node.'))

    def test_get_db_synchronization_status_CRITICAL2_backlog(self):
        self.mock_sync_result.return_value = [(u'Audit      ', 0L), (u'Audit                ', 222222L)]
        self.mock_sync_health_result.return_value = []
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_sync_result.return_value, 102400,
                                           self.mock_sync_health_result.return_value), (2,
                                                                                        'CRITICAL: Detected slow data synchronization between db1 and db2. Current backlog 217MB has reached threshold of 100MB. This could cause data loss on mirror node.'))

    def test_get_db_synchronization_status_CRITICAL3_backlog(self):
        self.mock_sync_result.return_value = [(u'Audit      ', 204800L), (u'Audit                ', 222222L)]
        self.mock_sync_health_result.return_value = []
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_sync_result.return_value, 102400,
                                           self.mock_sync_health_result.return_value), (2,
                                                                                        'CRITICAL: Detected slow data synchronization between db1 and db2. Current backlog 217MB has reached threshold of 100MB. This could cause data loss on mirror node.'))

    def test_get_db_synchronization_status_CRITICAL_health(self):
        self.mock_sync_result.return_value = []
        self.mock_sync_health_result.return_value = ['StentorExport', 'StentorDba', ]
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_sync_result.return_value, 102400,
                                           self.mock_sync_health_result.return_value), (2,
                                                                                        "CRITICAL: Problem in data synchronization. Synchronization issues found in 'StentorExport', 'StentorDba' databases."))

    def test_get_db_synchronization_status_CRITICAL_health_backlog(self):
        self.mock_sync_result.return_value = [(u'Audit      ', 204800L), (u'Audit                ', 222222L)]
        self.mock_sync_health_result.return_value = ['StentorExport', 'StentorDba', ]
        self.assertEqual(
            self.module.get_formatted_data(self.mock_db_hosts.return_value, self.mock_sync_result.return_value, 102400,
                                           self.mock_sync_health_result.return_value), (2,
                                                                                        "CRITICAL: Problem in data synchronization. Synchronization issues found in 'StentorExport', 'StentorDba' databases."))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_db_mirroring_synchronization_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_db_mirroring_synchronization_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_db_mirroring_synchronization_status = MagicMock(name='get_db_mirroring_synchronization_status')
        self.module.get_db_mirroring_synchronization_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_db_mirroring_synchronization_status = MagicMock(name='get_db_mirroring_synchronization_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_db_mirroring_synchronization_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
