import unittest
from mock import MagicMock, patch
from winrm.exceptions import AuthenticationError, WinRMOperationTimeoutError, WinRMError
from requests.exceptions import RequestException

class CheckInternetTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_internet
        self.module = check_internet

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user', 'pass'))

    def test_get_status_ok(self):
        status, msg = self.module.get_status('0','0')
        self.assertEqual(msg, 'OK: Internet access in not available in the Node')
        self.assertEqual(status, 0)

    def test_get_output(self):
        self.assertEqual(self.module.get_output('2\n'),'2')

    def test_get_status_crit(self):
        status, msg = self.module.get_status('2','2')
        self.assertEqual(msg, 'CRITICAL: Internet access is available in the Node')
        self.assertEqual(status, 2)

    def test_get_status_unk(self):
        status, msg = self.module.get_status('3','3')
        self.assertEqual(msg, 'UNKNOWN: Please check powershell script')
        self.assertEqual(status, 3)

    def test_check_internet_ok(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_err = ''
        mock_winrm.execute_ps_script.return_value.std_out = """0\n"""
        mock_winrm.execute_ps_script.return_value.std_out1 = """0\n"""
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 0)
        test_msg = "OK: Internet access in not available in the Node"
        self.assertEqual(msg, test_msg)


    def test_check_internet_crit(self):
        mock_winrm = MagicMock(name='WinRM')
        mock_winrm.execute_ps_script.return_value.std_out = "2\n"
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_winrm
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = "CRITICAL: Internet access is available in the Node"
        self.assertEqual(msg, test_msg)

    def test_check_internet_req_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = RequestException('RequestException')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 3)
        test_msg = 'UNKNOWN : Authentication Error - {0}'.format('RequestException')
        self.assertEqual(msg, test_msg)

    def test_check_internet_exception(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Exception e'
        self.assertEqual(msg, test_msg)

    def test_get_job_status_timeout_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMOperationTimeoutError('WinRMOperationTimeoutError')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMOperationTimeoutError')
        self.assertEqual(msg, test_msg)

    def test_get_job_status_authentication_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('AuthenticationError')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 3)
        test_msg = 'UNKNOWN : Authentication Error - AuthenticationError'
        self.assertEqual(msg, test_msg)

    def test_get_job_status_winrm_error(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('WinRMError')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : WinRM Error {0}'.format('WinRMError')
        self.assertEqual(msg, test_msg)

    def test_check_internet_typeerror(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format('localhost')
        self.assertEqual(msg, test_msg)

    def test_check_internet_type_error1(self):
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('find() takes exactly 2')
        status, msg = self.module.check_internet('localhost', "user", "pass")
        self.assertEqual(status, 2)
        test_msg = 'CRITICAL : Issue in connecting to node - {0}'.format('localhost')
        self.assertEqual(msg, test_msg)

    def test_main(self):
        self.module.check_arg = MagicMock(name='check_arg')
        self.module.check_arg.return_value = ('localhost', 'user', 'pass')
        self.module.wmi_status = MagicMock(name='check_internet')
        self.module.wmi_status.return_value = (0, 'OK')
        try:
            self.module.main()
        except SystemExit as e:
            self.assertEqual(e.code, 0)


if __name__ == '__main__':
    unittest.main()
