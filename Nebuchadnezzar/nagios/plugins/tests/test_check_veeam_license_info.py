import unittest
from mock import MagicMock, patch
from requests.exceptions import RequestException
from StringIO import StringIO
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)


class VeeamLicenseInfoTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_veeam_license_info
        self.module = check_veeam_license_info
        self.module.WinRM = MagicMock(name="WinRM")

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.critical_val = 30
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 30))

    def test_process_output_err(self):
        out_put = "couldn't get the license expirationdate"
        result = self.module.process_output(out_put, 30)
        exp_result = (
            2,
            'CRITICAL : Veeam Backup and Replication license expiration date in not available| service status=2;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_process_output_license_expired(self):
        out_put = '20'
        result = self.module.process_output(out_put, 30)
        exp_result = (
            2,
            'CRITICAL : 20 days remaining for Veeam Backup and Replication license to expire| service status=2;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_process_output_license_not_expired(self):
        out_put = '40'
        result = self.module.process_output(out_put, 30)
        exp_result = (
            0, 'OK: 40 days remaining for Veeam Backup and Replication license to expire| service status=0;1;2;0;2')
        self.assertEqual(result, exp_result)

    def test_process_output_license_unknown_expiry_date(self):
        out_put = '-20'
        result = self.module.process_output(out_put, 30)
        exp_result = (2, 'CRITICAL : -20 days before Veeam Backup and Replication license expired or unknown response '
                         'of expiration date| service status=2;1;2;0;2')
        self.assertEqual(result, exp_result)

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_output = MagicMock(name="output")
        mock_output.std_out = 40
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        self.module.WinRM.return_value = mock_WINRM
        self.module.process_output = MagicMock(name='process_output', return_value=(
        0, 'OK: 40 days remaining for Veeam Backup and Replication license to expire'))
        self.module.main('host', 'username', 'password', '20')
        self.sys_mock.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(),
                         'OK: 40 days remaining for Veeam Backup and Replication license to expire\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_critical(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_output = MagicMock(name="output")
        mock_output.std_out = 10
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        self.module.WinRM.return_value = mock_WINRM
        self.module.process_output = MagicMock(name='process_output', return_value=(
        2, 'CRITICAL: 10 days remaining for Veeam Backup and Replication license to expire'))
        self.module.main('host', 'username', 'password', '20')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL: 10 days remaining for Veeam Backup and Replication license to expire\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok_std_err(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        error = 'Error'
        mock_output = MagicMock(name="output")
        mock_output.std_err = error
        mock_output.std_out = ''
        mock_WINRM = MagicMock(name="WinRM")
        mock_WINRM.execute_ps_script.return_value = mock_output
        self.module.WinRM.return_value = mock_WINRM
        data = self.module.main('host', 'username', 'password', 'critical_val')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Error| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_authentication_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.WinRM.side_effect = AuthenticationError('e')
        self.module.main('host', 'username', 'password', 'critical_val')
        self.sys_mock.exit.assert_called_once_with(3)
        self.assertEqual(std_out.getvalue(), 'UNKNOWN : Authentication Error - e| service status=3;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_winrm_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.WinRM.side_effect = WinRMError('e')
        self.module.WinRMError = WinRMError
        self.module.main('host', 'username', 'password', 'critical_val')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : WinRM Error e| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_RequestException(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.WinRM = MagicMock(name="WinRM")
        self.module.WinRM.side_effect = RequestException
        data = self.module.main('host', 'username', 'password', 'critical_val')
        self.module.sys.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Request Error | service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error_takes_exactly_2(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.main('host', 'username', 'password', 'critical_val')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL : Issue in connecting to node - host| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.WinRM.side_effect = TypeError('e')
        self.module.main('host', 'username', 'password', 'critical_val')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL : Typeerror(May be Issue in connecting to node - host)| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.module.WinRM.side_effect = Exception('e')
        self.module.main('host', 'username', 'password', 'critical_val')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception e| service status=2;1;2;0;2\n')


if __name__ == '__main__':
    unittest.main()
