from StringIO import StringIO
import unittest
from mock import MagicMock, patch, Mock


class CheckSqlJobsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_pymssql = MagicMock(name='pymssql')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_datetime = MagicMock(name='datetime')
        modules = {
            'pymssql': self.mock_pymssql,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.dbutils': self.mock_scanline.utilities.dbutils,
            'datetime': self.mock_datetime
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_sql_jobs_status
        self.module = check_sql_jobs_status
        self.module.QUERY = "select * from test_sql_job;"

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg_1(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.database = 'db1'
        mock_result.env = 'cluster'
        mock_result.vigilant_url = 'http://vigilant/'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'db1', 'cluster', 'http://vigilant/'))

    @patch('scanline.utilities.dbutils.get_mirroring_primary_check', return_value=False)
    def test_get_job_status_secondary_node(self, primary_check):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(
            return_value=[['idm04db1.idm04.isyntax.net', 'idm04db2.idm04.isyntax.net']])
        self.assertEqual(
            self.module.get_job_status('idm04db1.idm04.isyntax.net', 'admin', 'phipass', 'msdb', 'mirroring', 'http://vigilant/'),
            (0, 'OK: This service check only applicable for primary node. idm04db1.idm04.isyntax.net '
                'is not the primary node.'))

    @patch('scanline.utilities.dbutils.get_dbnodes', return_value=[['idm04db1.idm04.isyntax.net',
                                                                    'idm04db2.idm04.isyntax.net']])
    def test_get_job_status_both_exception(self, mock_db_hosts):
        self.mock_pymssql.connect.side_effect = Exception('e')
        self.assertEqual(
            self.module.get_job_status('idm04db1.idm04.isyntax.net', 'phiadmin', 'philtor', 'msdb', 'mirroring', 'http://vigilant/'),
            (2, 'CRITICAL - Connection error with idm04db1.idm04.isyntax.net. '))

    def test_get_job_status_ok(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        self.module.get_message = MagicMock(name='get_message')
        self.module.get_message.return_value = 'Job:job1 Last execution on date/time:30/7/2019 state:Succeeded\n' \
                                               'Job:job2 Last execution on date/time:29/7/2019 state:Succeeded'
        self.mock_datetime.datetime.strftime.side_effect = ['30/7/2019', '29/7/2019']
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchall.return_value = [('job1', '30/7/2019', 'Succeeded'), ('job2', '29/7/2019', 'Succeeded')]
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'mirroring', 'http://vigilant/'),
                         (0, 'OK - All the SQL jobs 2/2 are Success\n'
                             'Job:job1 Last execution on date/time:30/7/2019 state:Succeeded\n'
                             'Job:job2 Last execution on date/time:29/7/2019 state:Succeeded'))

    def test_get_job_status_ok_cluster(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        self.module.get_message = MagicMock(name='get_message')
        self.module.get_message.return_value = 'Job:job1 Last execution on date/time:30/7/2019 state:Succeeded\n' \
                                               'Job:job2 Last execution on date/time:29/7/2019 state:Succeeded'
        self.mock_datetime.datetime.strftime.side_effect = ['30/7/2019', '29/7/2019']
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchall.return_value = [('job1', '30/7/2019', 'Succeeded'), ('job2', '29/7/2019', 'Succeeded')]
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'cluster', 'http://vigilant/'),
                         (0, 'OK - All the SQL jobs 2/2 are Success\n'
                             'Job:job1 Last execution on date/time:30/7/2019 state:Succeeded\n'
                             'Job:job2 Last execution on date/time:29/7/2019 state:Succeeded'))

    def test_get_job_status_critical(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        self.module.get_message = MagicMock(name='get_message')
        self.module.get_message.return_value = 'Job:job1 Last execution on date/time:30/7/2019 state:Failed\n' \
                                               'Job:job2 Last execution on date/time:29/7/2019 state:Canceled\n' \
                                               'Job:job3 Last execution on date/time:31/7/2019 state:Succeeded'
        self.mock_datetime.datetime.strftime.side_effect = ['30/7/2019', '29/7/2019', '31/7/2019']
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchall.return_value = [('job1', '30/7/2019', 'Failed'), ('job2', '29/7/2019', 'Canceled'),
                                             ('job3', '31/7/2019', 'Succeeded')]
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'mirroring', 'http://vigilant/'),
                         (2, 'CRITICAL - There are 1/3 SQL jobs in Failed state and 1/3 SQL jobs in Canceled state\n'
                             'Job:job1 Last execution on date/time:30/7/2019 state:Failed\n'
                             'Job:job2 Last execution on date/time:29/7/2019 state:Canceled\n'
                             'Job:job3 Last execution on date/time:31/7/2019 state:Succeeded'))

    def test_get_job_status_warning(self):
        self.mock_scanline.utilities.dbutils.get_dbnodes = MagicMock(return_value=[['idm04db1.idm04.isyntax.net']])
        self.mock_scanline.utilities.dbutils.dbutils.get_mirroring_primary_check = MagicMock(return_value=True)
        self.module.get_message = MagicMock(name='get_message')
        self.module.get_message.return_value = 'Job:job1 Last execution on date/time:30/7/2019 state:Canceled\n' \
                                               'Job:job2 Last execution on date/time:29/7/2019 state:Canceled\n' \
                                               'Job:job3 Last execution on date/time:31/7/2019 state:Succeeded'
        self.mock_datetime.datetime.strftime.side_effect = ['30/7/2019', '29/7/2019', '31/7/2019']
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchall.return_value = [('job1', '30/7/2019', 'Canceled'), ('job2', '29/7/2019', 'Canceled'),
                                             ('job3', '31/7/2019', 'Succeeded')]
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.assertEqual(self.module.get_job_status('localhost', 'test1', 'pass1', 'db1', 'mirroring', 'http://vigilant/'),
                         (1, 'WARNING - There are 2/3 SQL jobs in Canceled state\n'
                             'Job:job1 Last execution on date/time:30/7/2019 state:Canceled\n'
                             'Job:job2 Last execution on date/time:29/7/2019 state:Canceled\n'
                             'Job:job3 Last execution on date/time:31/7/2019 state:Succeeded'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main(self, std_out):
        self.module.sys = MagicMock(name='sys')
        self.module.check_arg = MagicMock(return_value=('arg1', 'arg2'))
        self.module.get_job_status = MagicMock(return_value=(0, 'Ok'))
        self.module.exit = MagicMock(return_value=True)
        self.module.main()
        self.module.get_job_status.assert_called_once_with('arg1', 'arg2')
        self.module.sys.exit.assert_called_once_with(0)
        self.assertEqual(
            std_out.getvalue(), 'Ok\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception(self, std_out):
        self.module.get_job_status = MagicMock(name='get_job_status')
        self.module.get_job_status.side_effect = Exception('Exception')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(
            std_out.getvalue(), 'CRITICAL - Exception.\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_exception_permissions(self, std_out):
        self.module.get_job_status = MagicMock(name='get_job_status')
        self.module.check_arg = MagicMock(return_value=('hostname', 'phisqluser'))
        self.module.get_job_status.side_effect = Exception(
            "The EXECUTE permission was denied on the object 'xp_readerrorlog'")
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        self.module.main()
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         "CRITICAL - SQL access permissions denied, access level low for 'phisqluser' user.\n")


if __name__ == '__main__':
    unittest.main()
