from StringIO import StringIO
import unittest

from mock import MagicMock, patch
from requests.exceptions import RequestException


class PingStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_request = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'requests': self.mock_request,
            'requests.exceptions': self.mock_request.exceptions,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_ping_status
        self.module = check_ping_status

    def test_get_url(self):
        url = self.module.get_url('host', '/service_url', 'protocol','443')
        self.assertEqual(url, 'protocol://host:443/service_url')

    def test_get_url_path(self):
        url = self.module.get_url('host', '', 'protocol','443')
        self.assertEqual(url, 'protocol://host:443')

    @patch('check_ping_status.requests')
    def test_ping_status(self, mock_request):
        mock_request.get.return_value.status_code = 200
        self.assertEqual(self.module.ping_status('url', False, False),
            (0,'200 - Service is up'))
        
    @patch('check_ping_status.token_mgr')
    def test_ping_status_hmac(self, mock_request):
        mock_request.do_hmac_request.return_value.status_code = 200
        self.assertEqual(self.module.ping_status('url', True, False),
            (0,'200 - Service is up'))

    def test_parse_cmd_args(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.service_url = '/service/url'
        mock_result.protocol = 'https'
        mock_result.hmac = 'True'
        mock_result.port = '443'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.parse_cmd_args()
        self.assertEqual(response, ('localhost', '/service/url', 'https', True, '443'))

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.module.ping_status.return_value = (0, '200 - Service is up')
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        mock_ping_status = MagicMock(name='ping_status')
        mock_ping_status.return_value = 0,'200 - Service is up'
        mock_get_url = MagicMock(name='get_url')
        mock_get_url.return_value = 'url'
        self.module.ping_status = mock_ping_status
        self.module.get_url = mock_get_url
        self.module.main('host', 'url', 'protocol', False, 80)
        mock_ping_status.assert_called_once_with('url', False)
        mock_get_url.assert_called_once_with('host', 'url', 'protocol', 80)
        sys_mock.exit.assert_called_once_with(0)
        self.assertEqual(std_out.getvalue(), '200 - Service is up\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_requestException(self, std_out):
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        mock_ping_status = MagicMock(name='health_status')
        mock_ping_status.side_effect = RequestException(
            '404')
        self.module.RequestException = RequestException
        self.module.ping_status = mock_ping_status
        self.module.main('host', 'url', 'protocol', False,80)
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(),
                         'CRITICAL : Service is down - Request Error - 404| service status=2;1;2;0;2\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_Exception(self, std_out):
        sys_mock = MagicMock(name='sys')
        self.module.sys = sys_mock
        mock_ping_status = MagicMock(name='health_status')
        mock_ping_status.side_effect = Exception('Exception')
        self.module.ping_status = mock_ping_status
        self.module.main('host', 'url', 'protocol', False, 80)
        sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(
            std_out.getvalue(), 'CRITICAL : Service is down - Error - Exception| service status=2;1;2;0;2\n')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

if __name__ == '__main__':
    unittest.main()