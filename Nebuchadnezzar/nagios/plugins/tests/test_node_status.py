import unittest
from mock import MagicMock, patch
from requests.exceptions import RequestException
from pymssql import Error as pyError

class NodeStatus(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_requests = MagicMock(name='requests')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_json = MagicMock(name='json')
        self.mock_pymssql = MagicMock(name='pymssql')
        modules = {
            'winrm': self.mock_winrm,
            'argparse': self.mock_argparse,
            'json': self.mock_json,
            'pymssql': self.mock_pymssql,
            'requests': self.mock_requests,
            'phimutils': self.mock_phimutils,
            'phimutils.message': self.mock_phimutils.message,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.config_reader': self.mock_scanline.utilities.config_reader,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import node_status
        self.module = node_status

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.username = 'user'
        mock_result.password = 'pass'
        mock_result.module_type = 'module_type'
        mock_result.protocol = 'protocol'
        mock_result.port = 'port'
        mock_result.timeout = 'timeout'
        mock_result.req_url = 'req_url'
        mock_result.database = 'db'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'user',
                                    'pass', 'module_type', 'protocol', 'port', 'timeout', 'req_url', 'db'))

    def test_check_arg_default(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.hostname = 'localhost'
        mock_result.module_type = 'module_type'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', mock_result.username,
                                    mock_result.password, 'module_type',
                                    mock_result.protocol, mock_result.port,
                                    mock_result.timeout, mock_result.req_url, mock_result.database))

    def test_get_url(self):
        template_mock = MagicMock()
        result = self.module.get_url(template_mock, 'a', 'b')
        self.assertEqual(result, template_mock.format.return_value)
        template_mock.format.assert_called_once_with('a', 'b')

    def test_update_status(self):
        endpoint = {'ENDPOINT': 'enp'}
        self.mock_scanline.utilities.config_reader.get_site_id.return_value = 'siteid'
        self.mock_scanline.utilities.config_reader.read_from_tbconfig.return_value = endpoint
        self.module.update_status('data')
        self.mock_scanline.utilities.config_reader.read_from_tbconfig.assert_called_once_with('VERSION')
        self.mock_scanline.utilities.config_reader.get_site_id.assert_called_once_with()
        data = {'facts': 'data'}
        self.mock_phimutils.message.Message.assert_called_once_with(siteid='siteid', payload=data, **{})
        self.mock_requests.post.assert_called_once_with('enp', json=self.mock_phimutils.message.Message.return_value.to_dict.return_value, verify=False)
        self.mock_requests.post.return_value.raise_for_status.assert_called_once_with()

    def test__request(self):
        response = self.module._request('url', 'user', 'pass', 10, 'header', 'verify')
        self.assertEqual(response, self.mock_json.loads.return_value)
        self.mock_json.loads.assert_called_once_with(self.mock_requests.get.return_value.content)
        self.mock_requests.get.assert_called_once_with('url', headers='header', auth=('user', 'pass'), timeout=10, verify='verify')
        self.mock_requests.get.return_value.raise_for_status.assert_called_once_with()

    def test__format_status_info(self):
        response = self.module._format_status_info('hostname', {'rabbitmq_version': 1})
        exp_response = {'hostname': {'Components': [{'name': 'Rabbitmq', 'version': 1}]}}
        self.assertEqual(response, exp_response)

    def test_update_rabbitmq_status(self):
        self.module.get_url = MagicMock(name='get_url')
        self.module._request = MagicMock(name='_request')
        self.module._format_status_info = MagicMock(name='_format_status_info')
        self.module.update_status = MagicMock(name='update_status')
        res = self.module.update_rabbitmq_status('host', 'u', 'p', 'protocol', 'port', 'timeout')
        self.assertEqual(res, (0, 'Status Updated'))
        self.module.get_url.assert_called_once_with(self.module.RBQ_URL_TEMPLATE, 'protocol', 'host', 'port', 'overview')
        self.module._request.assert_called_once_with(self.module.get_url.return_value, 'u', 'p', 'timeout', {})
        self.module._format_status_info.assert_called_once_with('host', self.module._request.return_value)
        self.module.update_status.assert_called_once_with(self.module._format_status_info.return_value)

    def test_main(self):
        self.module.update_rabbitmq_status = MagicMock(name='update_rabbitmq_status')
        response = self.module.main('host', 'u', 'p', 'rabbitmq', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, self.module.update_rabbitmq_status.return_value)

    def test_main_stratoscale(self):
        self.module.update_stratoscale_status = MagicMock(name='update_stratoscale_status')
        response = self.module.main('host', 'u', 'p', 'stratoscale', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, self.module.update_stratoscale_status.return_value)

    def test_main_sql_version(self):
        self.module.update_sql_version = MagicMock(name='update_sql_version')
        response = self.module.main('host', 'u', 'p', 'sql_version', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, self.module.update_sql_version.return_value)

    def test_main_unknown(self):
        self.module.update_rabbitmq_status = MagicMock(name='update_rabbitmq_status')
        response = self.module.main('host', 'u', 'p', 'unknown', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, (1, 'WARNING : UNKNOWN module_type - unknown'))

    def test_main_RequestException(self):
        self.module.update_rabbitmq_status = MagicMock(name='update_rabbitmq_status')
        self.module.update_rabbitmq_status.side_effect = RequestException
        self.module.RequestException = RequestException
        response = self.module.main('host', 'u', 'p', 'rabbitmq', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, (1, 'WARNING : Request Error '))

    def test_main_Exception(self):
        self.module.update_rabbitmq_status = MagicMock(name='update_rabbitmq_status')
        self.module.update_rabbitmq_status.side_effect = Exception
        response = self.module.main('host', 'u', 'p', 'rabbitmq', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, (1, 'WARNING : Error '))

    def test_main_pyException(self):
        self.module.update_sql_version = MagicMock(name='update_sql_version')
        self.module.update_sql_version.side_effect = pyError
        response = self.module.main('host', 'u', 'p', 'sql_version', 'prt', 'port', 'tm', 'url', 'db')
        self.assertEqual(response, (1, 'WARNING : Error '))

    def test_update_stratoscale_status(self):
        self.module._request = MagicMock(name='_request')
        self.module._format_status_info_stratoscale = MagicMock(name='_format_status_info_stratoscale')
        self.module.update_status = MagicMock(name='update_status')
        res = self.module.update_stratoscale_status('host', 'http://abc','15')
        self.assertEqual(res, (0, 'OK - Consul services version Updated'))

    def test__format_status_info_stratoscale(self):
        data = {"elasticsearch-161-92-250-50": ["Version-6.8.2"],"fluentd-161-92-250-55": ["Version-3"]}
        response = self.module._format_status_info_stratoscale('hostname', data)
        exp_response = {'hostname': {'Components': [{'version': '3', 'name': 'fluentd-161-92-250-55'},
         {'version': '6.8.2', 'name': 'elasticsearch-161-92-250-50'}]}}
        self.assertEqual(response, exp_response)

    def test__format_status_info_stratoscale_no_version(self):
        data = {"elasticsearch-161-92-250-50": ["Version-6.8.2"],"fluentd-161-92-250-55": ["Version-3"], "fluentd-161-92-250-56": ["Vault"]}
        response = self.module._format_status_info_stratoscale('hostname', data)
        exp_response = {'hostname': {'Components': [{'version': '3', 'name': 'fluentd-161-92-250-55'},
         {'version': '6.8.2', 'name': 'elasticsearch-161-92-250-50'}]}}
        self.assertEqual(response, exp_response)

    def test_update_sql_version_if(self):
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchone.return_value = "data"
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        self.module._format_sql_version_info = MagicMock(name='_format_sql_version_info', return_value='status')
        self.module.update_status = MagicMock(name='update_status')
        state, msg = self.module.update_sql_version('host', 'user', 'pass', 'db')
        self.assertEqual(0, state)
        self.assertEqual(msg, 'SQL Version Updated')
    
    def test_update_sql_version(self):
        mock_cursor = MagicMock(name='cursor')
        mock_cursor.fetchone.return_value = ""
        mock_con = MagicMock(name='conn')
        mock_con.cursor.return_value = mock_cursor
        self.module.pymssql.connect = MagicMock(name="pymssql.connect")
        self.module.pymssql.connect.return_value = mock_con
        state, msg = self.module.update_sql_version('host', 'user', 'pass', 'db')
        self.assertEqual(1, state)
        self.assertEqual(msg, 'Failed to retrieve SQL Version')

    def test__format_sql_version_info(self):
        output = self.module._format_sql_version_info('hostname', ('SP1', 'Std Edition (64-bit)', '10.50.2500.0',
                                                                   u'SQL (SP1)'))
        version_info = {'hostname': {'Components': [{'version': '10.50.2500.0', 'name': 'SQL Std Edition (64-bit)'}]}}
        self.assertEqual(output, version_info)


if __name__ == '__main__':
    unittest.main()
