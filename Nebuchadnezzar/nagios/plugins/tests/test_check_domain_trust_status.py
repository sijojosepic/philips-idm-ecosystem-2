from StringIO import StringIO
import unittest
from mock import MagicMock, patch, call
from winrm.exceptions import WinRMError, AuthenticationError


class DomainTrustStatusTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_winrm = MagicMock(name='winrm')
        self.mock_scanline = MagicMock(name='scanline')
        modules = {
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,
            'argparse': self.mock_argparse,
            'scanline': self.mock_scanline,
            'scanline.utilities': self.mock_scanline.utilities,
            'scanline.utilities.win_rm': self.mock_scanline.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_domain_trust_status
        self.module = check_domain_trust_status

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.trusteddomainname = 'hospital.org'
        mock_result.trusteddomainuser = 'test1'
        mock_result.trusteddomainpassword = 'pass1'
        mock_result.trustingdomainname = 'HMG01.iSyntax.net'
        mock_result.trustingdomainuser = 'test1'
        mock_result.trustingdomainpassword = 'pass1'
        mock_result.trusttype = 'one_way'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1',
                                    'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way'))

    def test_get_one_way_trust_status(self):
        one_way_trust_script = "$remotecontext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', '{0}', '{1}', '{2}')\n     $remoteForest = [System.DirectoryServices.ActiveDirectory.Forest]::getForest($remotecontext)\n     $localcontext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', '{3}', '{4}', '{5}')\n     $localForest = [system.DirectoryServices.ActiveDirectory.Forest]::GetForest($localcontext)\n     $localForest.VerifyTrustRelationship($remoteForest, 'Outbound')"
        self.assertEqual(self.module.get_one_way_trust_script(), one_way_trust_script)

    def test_two_way_trust_status(self):
        two_way_trust_script = "$remotecontext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Domain', '{0}', '{1}', '{2}')\n     $remoteForest = [System.DirectoryServices.ActiveDirectory.Domain]::getDomain($remotecontext)\n     $localForest = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()\n     $localForest.VerifyTrustRelationship($remoteForest,'Bidirectional')"
        self.assertEqual(self.module.get_two_way_trust_script(), two_way_trust_script)

    def test_format_err_username_password(self):
        std_err = 'the user name or password \nis incorrect.\n'
        exp_result = 'CRITICAL: The credentials of trusted or trusting domain is incorrect'
        self.assertEqual(self.module.format_err(std_err), exp_result)

    def test_format_err_domain_cannot_contact(self):
        std_err = 'the specified forest does \nnot exist or cannot be contacted.'
        exp_result = 'CRITICAL: The trust cannot be validate for the following reasons: The trusted or' \
                     ' trusting domain could not be contacted or does not exist.The trust verification failed between ' \
                     'trusted or trusting domain.'
        self.assertEqual(self.module.format_err(std_err), exp_result)

    def test_format_err(self):
        mock_parse_err_string = MagicMock(name='parse_err_string')
        mock_parse_err_string.return_value = 'err'
        self.module.parse_err_string = mock_parse_err_string
        result = self.module.format_err('std_err')
        self.assertEqual(result, 'err')
        mock_parse_err_string.assert_called_once_with('std_err')

    def test_parse_err_string(self):
        std_err = 'Exception calling "VerifyTrustRelationship" with "2" argument(s): "The trust \nrelationship between "NYP06CHILD.NYP02.iSyntax.net" and "NYP02.iSyntax.net" is \nnot "Bidirectional"."\nAt line:4 char:6\n+      $localForest.VerifyTrustRelationship($remoteForest,\'Bidirectiona ...\n+      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n    + CategoryInfo          : NotSpecified: (:) [], MethodInvocationException\n    + FullyQualifiedErrorId : ActiveDirectoryObjectNotFoundException'
        exp_result = 'The trust relationship between NYP06CHILD.NYP02.iSyntax.net and NYP02.iSyntax.net is not Bidirectional.'
        self.assertEqual(self.module.format_err(std_err), exp_result)

    def test_parse_err_index_error(self):
        std_err = 'Exception calling'
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = IndexError
        self.assertEqual(self.module.parse_err_string(std_err), ('CRITICAL: Error while verifying the domain trust '
                                                                 'relationship'))

    def test_process_output_ok(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = ''
        mock_compose_status_msg = MagicMock(name='compose_status_msg')
        mock_compose_status_msg.return_value = 'status', 'msg'
        mock_format_err = MagicMock(name='format_err')
        mock_format_err.return_value = 'err'
        self.module.compose_status_msg = mock_compose_status_msg
        self.module.format_err = mock_format_err
        result = self.module.process_output(mock_out_put)
        self.assertEqual(result, ('status', 'msg'))
        mock_compose_status_msg.assert_called_once_with(mock_out_put)
        self.assertEqual(mock_format_err.call_count, 0)

    def test_process_output_not_ok(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 1
        mock_out_put.std_err = 'std_err'
        mock_compose_status_msg = MagicMock(name='compose_status_msg')
        mock_format_err = MagicMock(name='format_err')
        mock_format_err.return_value = 'err'
        self.module.compose_status_msg = mock_compose_status_msg
        self.module.format_err = mock_format_err
        result = self.module.process_output(mock_out_put)
        self.assertEqual(result, (2, 'err'))
        mock_format_err.assert_called_once_with('std_err')

    def test_compose_status_msg_ok(self):
        mock_out_put = MagicMock(name='out_put')
        mock_out_put.status_code = 0
        mock_out_put.std_out = ''
        result = self.module.process_output(mock_out_put)
        exp_result = 'The trust has been validated. It is in place and active.'
        self.assertEqual(result, (0, exp_result))

    def test_get_trust_type_one_way(self):
        mock_get_one_way_trust_script = MagicMock(name='get_one_way_trust_script')
        mock_get_one_way_trust_script.return_value = 'one_way_trust_script'
        self.module.get_one_way_trust_script = mock_get_one_way_trust_script
        self.assertEqual(self.module.get_trust_type('hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way'), 'one_way_trust_script')
        mock_get_one_way_trust_script.assert_called_once_with()

    def test_get_trust_type_two_way(self):
        mock_get_two_way_trust_script = MagicMock(name='get_two_way_trust_script')
        mock_get_two_way_trust_script.return_value = 'two_way_trust_script'
        self.module.get_two_way_trust_script = mock_get_two_way_trust_script
        self.assertEqual(self.module.get_trust_type('hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'two_way'), 'two_way_trust_script')
        mock_get_two_way_trust_script.assert_called_once_with()

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_ok(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = 'out_put'
        self.mock_scanline.utilities.win_rm.WinRM.return_value = mock_WinRM
        mock_get_trust_type = MagicMock(name='get_trust_type')
        mock_get_trust_type.return_value = 'trust_script'
        self.module.get_trust_type = mock_get_trust_type
        mock_process_output = MagicMock(name='process_output')
        mock_process_output.return_value = 0, 'msg'
        self.module.process_output = mock_process_output
        self.module.main('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        self.sys_mock.exit.assert_called_once_with(0)
        self.mock_scanline.utilities.win_rm.WinRM.assert_called_once_with('localhost', 'test1', 'pass1')
        mock_get_trust_type.assert_called_once_with('hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        mock_WinRM.execute_ps_script.assert_called_once_with('trust_script')
        mock_process_output.assert_called_once_with('out_put')
        self.assertEqual(std_out.getvalue(), 'msg\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_authentication_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = AuthenticationError('e')
        self.module.AuthenticationError = AuthenticationError
        self.module.main('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        self.sys_mock.exit.assert_called_once_with(3)
        self.assertEqual(std_out.getvalue(), 'UNKNOWN : WinRM Error e\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_winrm_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = WinRMError('e')
        self.module.WinRMError = WinRMError
        self.module.main('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : WinRM Error e\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error_takes_exactly_2(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.main('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Issue in connecting to the specified domain node(s) from the node - localhost\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = TypeError('e')
        self.module.main('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Typeerror(May be Issue in connecting to node - localhost)\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_scanline.utilities.win_rm.WinRM.side_effect = Exception('e')
        self.module.main('localhost', 'test1', 'pass1', 'hospital.org', 'test1', 'pass1', 'HMG01.iSyntax.net', 'test1', 'pass1', 'one_way')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception e\n')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
