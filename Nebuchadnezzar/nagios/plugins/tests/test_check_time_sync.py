from StringIO import StringIO

import unittest
from mock import MagicMock, patch, call

import requests
import winrm
from winrm.exceptions import WinRMError, AuthenticationError


class TimeSyncTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_winrm = MagicMock(name='winrm')
        modules = {
            'winrm': self.mock_winrm,
            'winrm.exceptions': self.mock_winrm.exceptions,
            'argparse': self.mock_argparse,
            'scanline.utilities.win_rm': self.mock_winrm.utilities.win_rm
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import check_time_sync
        self.module = check_time_sync
        self.mock_format_time = MagicMock(name='format_time')

    def test_check_arg(self):
        mock_result = MagicMock(name='mock_result')
        mock_result.host = 'localhost'
        mock_result.username = 'test1'
        mock_result.password = 'pass1'
        mock_result.minutes = 5
        mock_result.domain = 'test.iSyntax.net'
        mock_parser = MagicMock(name='parser')
        mock_parser.add_argument.return_value = True
        mock_parser.parse_args.return_value = mock_result
        self.module.argparse.ArgumentParser = MagicMock(name='argparse')
        self.module.argparse.ArgumentParser.return_value = mock_parser
        response = self.module.check_arg()
        self.assertEqual(response, ('localhost', 'test1', 'pass1', 5, 'test.iSyntax.net'))


    def test_get_time_offset(self):
        std_out = '04:37:57, +00.5911634s\n'
        result = self.module.get_time_offset(std_out)
        exp_result = '+00.5911634'
        self.assertEqual(result, exp_result)

    def test_convert_minutes_to_seconds(self):
        self.assertEqual(self.module.convert_minutes_to_seconds(5), 300)

    def test_check_time_diff_status_ok(self):
        exp_result = (0, 'OK : The current time of the node is in sync with primary domain node.')
        self.assertEqual(self.module.check_time_diff('+00.0428816', 300), exp_result)

    def test_check_time_diff_status_warning(self):
        exp_result = (1, 'WARNING : The node is not part of the domain or error in time offset response.')
        self.assertEqual(self.module.check_time_diff('error: 0x800705B', 300), exp_result)

    def test_check_time_diff_status_critical_slower(self):
        self.mock_format_time.return_value = '1 hour, 2 seconds'
        exp_result = (2, 'CRITICAL : Time not synchronized. The current time of the node is 1 hour, 2 seconds slower '
                         'than primary domain node.')
        self.assertEqual(self.module.check_time_diff('+3602.7931483', 300), exp_result)

    def test_check_time_diff_status_critical_faster(self):
        self.mock_format_time.return_value = '59 minutes, 53 seconds'
        exp_result = (2, 'CRITICAL : Time not synchronized. The current time of the node is 59 minutes, 53 seconds'
                         ' faster than primary domain node.')
        self.assertEqual(self.module.check_time_diff('-3593.0927218', 300), exp_result)

    def test_check_time_diff_status_critical_unknown_response(self):
        exp_result = (2, 'CRITICAL : Unknown time offset response from primary domain node.')
        self.assertEqual(self.module.check_time_diff('4557.0428816', 300), exp_result)

    def test_format_time_seconds(self):
        self.assertEqual(self.module.format_time(50), '50 seconds')

    def test_format_time_minutes(self):
        self.assertEqual(self.module.format_time(500), '8 minutes, 20 seconds')

    def test_format_time_hours(self):
        self.assertEqual(self.module.format_time(5000), '1 hour, 23 minutes')

    def test_format_time_days(self):
        self.assertEqual(self.module.format_time(500000), '5 days, 18 hours')

    def test_error_check_non_authoritative_error(self):
        err = 'Non-authoritative answer\n'
        result = self.module.error_check(err)
        exp_result = (1, 'WARNING: The node is not part of domain.')
        self.assertEqual(result, exp_result)

    def test_error_check_primary_name_server_error(self):
        err = '\'primary name server\' found\n'
        result = self.module.error_check(err)
        exp_result = (2, 'CRITICAL: Error fetching domain name or DNS request timed out.')
        self.assertEqual(result, exp_result)

    def test_error_check_error_time_offset_response(self):
        err = 'Error getting time offset\n'
        result = self.module.error_check(err)
        exp_result = (2, 'CRITICAL: Error in getting the time offset response value.')
        self.assertEqual(result, exp_result)

    def test_error_check_time_offset_response_not_available(self):
        err = '\primary name server\n'
        result = self.module.error_check(err)
        exp_result = (2, 'CRITICAL: Unable to retrieve time offset response value from the server.')
        self.assertEqual(result, exp_result)

    def test_check_time_offset(self):
        mock_get_time_offset = MagicMock(name='get_time_offset')
        mock_get_time_offset.return_value = '+00.5911634'
        self.module.get_time_offset = mock_get_time_offset
        mock_convert_minutes_to_seconds = MagicMock(name='convert_minutes_to_seconds')
        mock_convert_minutes_to_seconds.return_value = 300
        self.module.convert_minutes_to_seconds = mock_convert_minutes_to_seconds
        mock_check_time_diff = MagicMock(name='check_time_diff')
        mock_check_time_diff.return_value = 'status', 'msg'
        self.module.check_time_diff = mock_check_time_diff
        self.assertEqual(self.module.check_time_offset('+00.5911634', 5), ('status', 'msg'))
        mock_get_time_offset.assert_called_once_with('+00.5911634')
        mock_convert_minutes_to_seconds.assert_called_once_with(5)
        mock_check_time_diff.assert_called_once_with('+00.5911634', 300)

    def test_check_time_offset_index_error(self):
        mock_get_time_offset = MagicMock(name='get_time_offset')
        mock_get_time_offset.side_effect = IndexError
        self.module.get_time_offset = mock_get_time_offset
        self.assertEqual(self.module.check_time_offset('std_out', 300), (2, 'CRITICAL: IndexError while getting time offset value.'))

    def test_main_ok_std_err(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_output =  MagicMock(name='out_put')
        mock_output.std_err =(2, 'CRITICAL: Unable to retrieve time offset response value from the server.')
        mock_output.std_out = False
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = mock_output
        self.mock_winrm.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.module.error_check =  MagicMock(name='error_check',return_value=(2,'msg'))
        data = self.module.main('host', 'username', 'password', 60,'primary_domain')
        self.module.error_check.assert_called_once_with(mock_output.std_err)
        self.assertEqual(self.module.error_check.call_count,1)


    def test_main_ok(self):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        mock_output =  MagicMock(name='out_put')
        mock_output.std_err = False
        mock_output.std_out = True
        mock_WinRM = MagicMock(name='WinRM')
        mock_WinRM.execute_ps_script.return_value = mock_output
        self.mock_winrm.utilities.win_rm.WinRM.return_value = mock_WinRM
        self.module.check_time_offset =  MagicMock(name='check_time_offset' ,return_value = (2,'msg'))
        self.module.main('host', 'username', 'password', 60,'primary_domain')
        self.module.check_time_offset.assert_called_once_with(mock_output.std_out,60)
        self.assertEqual(self.module.check_time_offset.call_count,1)


    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_authentication_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.win_rm.WinRM.side_effect = AuthenticationError('e')
        self.module.AuthenticationError = AuthenticationError
        self.module.main('host', 'username', 'password', 'minutes', 'primary_domain')
        self.sys_mock.exit.assert_called_once_with(3)
        self.assertEqual(std_out.getvalue(), 'UNKNOWN : Authentication Error - e\n')



    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_winrm_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.win_rm.WinRM.side_effect = WinRMError('e')
        self.module.WinRMError = WinRMError
        self.module.main('host', 'username', 'password', 'minutes', 'primary_domain')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : WinRM Error e\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error_takes_exactly_2(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.win_rm.WinRM.side_effect = TypeError('takes exactly 2')
        self.module.main('host', 'username', 'password', 'minutes', 'primary_domain')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Issue in connecting to node - host\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception_type_error(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.win_rm.WinRM.side_effect = TypeError('e')
        self.module.main('host', 'username', 'password', 'minutes', 'primary_domain')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Typeerror(May be Issue in connecting to node - host)\n')

    @patch('sys.stdout', new_callable=StringIO)
    def test_main_winrm_exception(self, std_out):
        self.sys_mock = MagicMock(name='sys')
        self.module.sys = self.sys_mock
        self.mock_winrm.utilities.win_rm.WinRM.side_effect = Exception('e')
        self.module.main('host', 'username', 'password', 'minutes', 'primary_domain')
        self.sys_mock.exit.assert_called_once_with(2)
        self.assertEqual(std_out.getvalue(), 'CRITICAL : Exception e\n')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


if __name__ == '__main__':
    unittest.main()
