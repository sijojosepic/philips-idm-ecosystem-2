#!/usr/bin/env python
############################################
# Shinken Plugin - Time SyncMonitoring using WinRM.
#
# The aim of this plugin is to monitor the time synchronization
# between primary Domain node and its client node.
# Plugin uses WinRM, to execute PowerShell scripts
# Plugin exit with OK or CRITICAL status.
# CRITICAL Status will be triggered
#   1) When the time of the node is faster or slower than the
#      provided threshold against Domain node.
#   2) When the node is not part of the Domain.
#   3) when there is an error in the time offset response value.
################################################################

from __future__ import print_function
import sys
import argparse
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)
from scanline.utilities.win_rm import WinRM

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

intervals = (
    ('weeks', 604800),  # 60 seconds * 60 seconds * 24 hours  * 7 days
    ('days', 86400),  # 60 seconds * 60 seconds * 24 hours
    ('hours', 3600),  # 60 seconds * 60 seconds
    ('minutes', 60),
    ('seconds', 1)
)

QUERY_WHEN_NO_DOMAIN = '''
$Domain = Get-WmiObject Win32_ComputerSystem | Select-Object -ExpandProperty Domain
if([String]::IsNullOrEmpty($Domain)){
    Throw "System $env:COMPUTERNAME is not part of a domain"
}
$NslookupOutput = nslookup -type=soa $Domain
$MatchOutput = ($NslookupOutput | Out-String) -match 'primary name server = (.+)'
if(-not $MatchOutput){
    Throw "Error fetching 'primary name server' found"
}
$PrimaryNameServer = $Matches[1]
$W32Command = "w32tm /stripchart /computer:{0} /samples:1 /dataonly" -f $PrimaryNameServer
$W32Output = cmd /c  $W32Command
$Success = "$W32Output" -match 'The current time is'
if(-not $Success){
    Throw "Error getting time offset"
}
$W32Output
'''

QUERY_WHEN_DOMAIN = '''
$W32Command = "w32tm /stripchart /computer:%s /samples:1 /dataonly" 
$W32Output = cmd /c  $W32Command
$Success = "$W32Output" -match 'The current time is'
if(-not $Success){
    Throw "Error getting time offset"
}
$W32Output
'''


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Monitor Time difference between MG node and other nodes')
    parser.add_argument('-H', '--host', required=True, help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True, help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-M', '--minutes', required=True, type=int, help='The time difference to check in minutes')
    parser.add_argument('-D', '--domain', required=False, help='The domain name', default='')
    results = parser.parse_args(args)
    return results.host, results.username, results.password, results.minutes, results.domain


def get_time_offset(std_out):
    time_offset = std_out.split(',')[-1].strip()[:-1]
    return time_offset


def convert_minutes_to_seconds(minutes):
    seconds = 60 * int(minutes)
    return seconds


def check_time_diff(time_offset, threshold):
    status = CRITICAL
    if 'error' in time_offset:
        status, msg = WARNING, 'WARNING : The node is not part of the domain or error in time offset response.'
        return status, msg
    decimal_offset = time_offset[1:]
    offset = float(decimal_offset)
    if offset > threshold:
        time_difference = format_time(offset)
        if time_offset.startswith('+'):
            msg = 'CRITICAL : Time not synchronized. The current time of the node is {0} slower than primary domain node.' \
                .format(time_difference)
        elif time_offset.startswith('-'):
            msg = 'CRITICAL : Time not synchronized. The current time of the node is {0} faster than primary domain node.' \
                .format(time_difference)
        else:
            msg = 'CRITICAL : Unknown time offset response from primary domain node.'
    else:
        status, msg = OK, 'OK : The current time of the node is in sync with primary domain node.'
    return status, msg


def format_time(seconds, granularity=2):
    """This method converts seconds into days, hours and minutes"""
    result = []
    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{0} {1}".format(int(value), name))
    return ', '.join(result[:granularity])


def error_check(err):
    status = CRITICAL
    if 'Non-authoritative answer' in err:
        status, msg = WARNING, 'WARNING: The node is not part of domain.'
        return status, msg
    if '\'primary name server\' found' in err:
        msg = 'CRITICAL: Error fetching domain name or DNS request timed out.'
        return status, msg
    if 'Error getting time offset' in err:
        msg = 'CRITICAL: Error in getting the time offset response value.'
        return status, msg
    msg = 'CRITICAL: Unable to retrieve time offset response value from the server.'
    return status, msg


def check_time_offset(std_out, minutes):
    try:
        time_offset = get_time_offset(std_out)
        threshold = convert_minutes_to_seconds(minutes)
        return check_time_diff(time_offset, threshold)
    except IndexError:
        return CRITICAL, 'CRITICAL: IndexError while getting time offset value.'


def main(host, username, password, minutes, primary_domain):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        ps_query = QUERY_WHEN_DOMAIN % primary_domain if primary_domain else QUERY_WHEN_NO_DOMAIN
        out_put = win_rm.execute_ps_script(ps_query)
        if out_put.std_out:
            status, msg = check_time_offset(out_put.std_out, minutes)
        else:
            status, msg = error_check(out_put.std_err)
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : Authentication Error - {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(host)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(host)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_arg())
