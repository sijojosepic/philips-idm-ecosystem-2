#!/usr/bin/env python
'''
Created by Vijender.singh@philips.com on 7/24/18
'''
import json
import sys
import argparse
import requests
import shlex
import logging
import winrm
import redis
from collections import namedtuple
from subprocess import Popen, PIPE
from winrm.exceptions import AuthenticationError

logger = logging.getLogger(__name__)

VM_STATUS_QUERY_PARAM = {
    "cpu__used__of__vm__in__percent": ["query_top", {"metric_name": "cpu__used__of__vm__in__percent"}],
    "memory__used_by_guest_os__of__vm__in__percent": ["query_top",
                                                      {"metric_name": "memory__used_by_guest_os__of__vm__in__percent"}],
    "network__rx_kbps__of__vm__in__kbps": ["query_top", {"metric_name": "network__rx_kbps__of__vm__in__kbps"}],
    "network__tx_kbps__of__vm__in__kbps": ["query_top", {"metric_name": "network__tx_kbps__of__vm__in__kbps"}]}

MONITORED_STORAGE_LABELS = ['SYSTEM', 'DATA']
LSBLK_CMD = 'lsblk -f'
DF_CMD = 'df -h'

redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check StratoScale infra health')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of StratoScale instance')
    parser.add_argument('-d', '--domainname', required=True,
                        help='Domain name on Stratoscale')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the Stratoscale')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the Stratoscale')
    parser.add_argument('-i', '--vmid', required=False,
                        help='The password of the Stratoscale')
    parser.add_argument('-w', '--warningthreshold', required=False, type=int,
                        help='The threshold value for warning status')
    parser.add_argument('-c', '--criticalthreshold', required=False, type=int,
                        help="The threshold value for critical status")
    parser.add_argument('-r', '--resourceType', required=False,
                        choices=['MEMORY', 'CPU', 'STORAGE', 'NETWORK_TX', 'NETWORK_RX'],
                        help="Resource Type for which status needs to be checked")
    parser.add_argument('-f', '--floatingip', required=False,
                        help="floating ip of the VM")
    parser.add_argument('-o','--ostype',required=False, type=str,
                        help="os type of the VM ")
    parser.add_argument('-vmu', '--vmusername', required=False,
                        help="username  of the VM")
    parser.add_argument('-vmp','--vmpassword',required=False,
                        help="password of the VM ")
    parser.add_argument('-s','--servicetype',required=False,
                        help="service type for vm either precheck or other services", default=None)

    results = parser.parse_args(args)
    return (results.hostname,
            results.domainname,
            results.username,
            results.password,
            results.vmid,
            results.warningthreshold,
            results.criticalthreshold,
            results.resourceType,
            results.floatingip,
            results.ostype,
            results.vmusername,
            results.vmpassword,
            results.servicetype)

def get_token_from_redis():
    token = redis_con.hgetall("hsopToken")
    return token['token'] if token else False

def insert_token_to_redis(token):
    token = {'token': token}
    redis_con.hmset("hsopToken", token)

def generate_token(hostname, domainname, username, password):
    token_payload = get_token_payload(domainname, username, password)
    token = get_token(hostname, token_payload)
    product_id = get_product_id(hostname, token, domainname)
    token_payload = get_product_token_payload(token, product_id)
    token = get_token(hostname, token_payload)
    insert_token_to_redis(token)
    return token

def get_stratoscale_vm_resources_status(hostname, domainname, username, password, vmid, warn_threshold, crit_threshold,
                                        resource_type, floatingip=None, ostype=None, vmusername=None, vmpassword=None):
    try:
        resource_mths = {
            'MEMORY': get_memory_status,
            'CPU': get_cpu_status,
            'NETWORK_TX': get_network_tx,
            'NETWORK_RX': get_network_rx,
            'STORAGE': get_storage_status
        }
        if resource_type != 'STORAGE':
            token = get_token_from_redis()
            if not token:
                token = generate_token(hostname, domainname, username, password)
            stratoscale_vms_info = get_stratoscale_vms_info(hostname, token)
            if not stratoscale_vms_info:
                token = generate_token(hostname, domainname, username, password)
                stratoscale_vms_info = get_stratoscale_vms_info(hostname, token)
            status, message = resource_mths[resource_type](stratoscale_vms_info, vmid, warn_threshold, crit_threshold)
        else:
            status, message = resource_mths[resource_type](ostype, floatingip, vmusername, vmpassword, warn_threshold, crit_threshold)
        return status, message
    except Exception as e:
        return (2, 'Exception Occurred: ' + str(e))


def get_token_payload(domainname, username, password):
    payload = {'auth': {'identity': {'methods': ['password'], 'password': {
        'user': {'name': username, 'password': password, 'domain': {'name': domainname}}}},
                        'scope': {'domain': {'name': domainname}}}}

    headers = {'content-type': 'application/json'}
    body = json.dumps(payload)
    TokenPayload = namedtuple('TokenPayload', 'headers body')
    token_payload = TokenPayload(headers, body)
    return token_payload

def create_token_payload(domainname, username, password):
    payload = {'auth': {'identity': {'methods': ['password'], 'password': {
        'user': {'name': username, 'password': password, 'domain': {'name': domainname}}}},
                        'scope': {'domain': {'name': domainname}}}}
    headers = {'content-type': 'application/json'}
    body = json.dumps(payload)
    TokenPayload = namedtuple('TokenPayload', 'headers body')
    token_payload = TokenPayload(headers, body)
    return token_payload

def get_token(hostname, token_payload):
    try:
        url = 'https://{0}/api/v2/identity/auth'.format(hostname)
        response = requests.post(url, verify=False, headers=token_payload.headers, data=token_payload.body)
        token = response.headers['x-subject-token']
        return token
    except Exception as e:
        raise Exception('Token generation failed : Please check credentials')


def get_product_id(hostname, token, domainname):
    try:
        url = 'https://{0}/api/v2/identity/users/myself/projects'.format(hostname)
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, headers=headers, verify=False)
        result = response.json()
        product_id = None
        for product_details in result:
            if product_details['domain_name'] == domainname:
                product_id = product_details['id']

        if not product_id:
            raise Exception('No corresponding product found')
        return product_id
    except Exception:
        raise Exception('No corresponding product found')


def get_product_token_payload(token, product_id):
    payload = {
        'auth': {'identity': {'methods': ['token'], 'token': {'id': token}},
                 'scope': {'project': {'id': product_id}}}}
    headers = {'content-type': 'application/json'}
    body = json.dumps(payload)
    TokenPayload = namedtuple('TokenPayload', 'headers body')
    token_payload = TokenPayload(headers, body)
    return token_payload


def get_stratoscale_vms_info(hostname, token):
    url = 'https://{0}/api/v2/metrics/queries'.format(hostname)
    try:
        param_val = VM_STATUS_QUERY_PARAM
        param_val_json_str = json.dumps(param_val)
        param_pay_load = {'queries': param_val_json_str}
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, verify=False, params=param_pay_load, headers=headers)
        result = response.json()
        return result
    except Exception as e:
        return {}


def get_memory_status(stratoscale_vms_info, vmid, warn_threshold, crit_threshold):
    vms_memory = stratoscale_vms_info['memory__used_by_guest_os__of__vm__in__percent']
    mem_used_percentage = None
    for id, mem in vms_memory:
        if id == vmid:
            mem_used_percentage = mem
    if mem_used_percentage is None:
        status = 3
        message = 'VM maybe ShutOff or may not exist anymore'
    else:
        status, message = get_health_status(mem_used_percentage, warn_threshold,
                                            crit_threshold, 'MEMORY')
    return (status, message)


def get_cpu_status(stratoscale_vms_info, vmid, warn_threshold, crit_threshold):
    vms_cpu = stratoscale_vms_info['cpu__used__of__vm__in__percent']
    cpu_used_percentage = None
    for id, cpu in vms_cpu:
        if id == vmid:
            cpu_used_percentage = cpu
    if cpu_used_percentage is None:
        status = 3
        message = 'VM maybe ShutOff or may not exist anymore'
    else:
        status, message = get_health_status(cpu_used_percentage, warn_threshold,
                                            crit_threshold, 'CPU')
    return (status, message)


def get_network_tx(stratoscale_vms_info, vmid, warn_threshold, crit_threshold):
    vms_network_tx = stratoscale_vms_info['network__tx_kbps__of__vm__in__kbps']
    network_tx_kbps = None
    for id, network_tx in vms_network_tx:
        if id == vmid:
            network_tx_kbps = network_tx
    '''
    TODO: 10MB is just a dummy value.
    Will need to change when StratoScale gives the threshold
    '''
    if network_tx_kbps is None:
        status = 3
        message = 'VM maybe ShutOff or may not exist anymore'
    else:
        network_utilization_percentage = (float(network_tx_kbps) / 100000) * 100
        status, message = get_health_status(network_utilization_percentage,
                                            warn_threshold, crit_threshold,
                                            'NETWORK_TX')
    return (status, message)


def get_network_rx(stratoscale_vms_info, vmid, warn_threshold, crit_threshold):
    vms_network_rx = stratoscale_vms_info['network__rx_kbps__of__vm__in__kbps']
    network_rx_kbps = None
    for id, network_rx in vms_network_rx:
        if id == vmid:
            network_rx_kbps = network_rx
    '''
    TODO: 10MB is just a dummy value.
    Will need to change when StratoScale gives the threshold
    '''
    if network_rx_kbps is None:
        status = 3
        message = 'VM maybe ShutOff or may not exist anymore'
    else:
        network_utilization_percentage = (float(network_rx_kbps)/100000)*100
        status, message = get_health_status(network_utilization_percentage,
                                            warn_threshold, crit_threshold,
                                            'NETWORK_RX')
    return (status, message)

def get_storage_status(os_type, floating_ip, username, password,
                       warn_threshold, crit_threshold):
    if floating_ip == 'None' or os_type == 'None':
        status = 1
        message = 'Check parameters for Floating IP and OS type'

    elif os_type.upper() == 'LINUX':
        try:
            usage, usage_breakup_message = get_linux_storage_usage(floating_ip,
                                                            username, password)
            if usage == None:
                status = 3
                message= usage_breakup_message
            else:
                status, message = get_health_status(usage, warn_threshold,
                                crit_threshold, 'STORAGE', usage_breakup_message)
        except ValueError as exc:
            status = 1
            message = str(exc)
    elif os_type.upper() == 'WINDOWS':
        try:
            usage, usage_breakup_message = check_windows_storage_usage(floating_ip,
                                                                   username, password)
            if usage == None:
                status = 3
                message = usage_breakup_message
            else:
                status, message = get_health_status(usage, warn_threshold,
                                                crit_threshold, 'STORAGE', usage_breakup_message)
        except ValueError as exc:
            status = 3
            message = str(exc)
    else:
        status = 3
        message = str(os_type) + " not yet supported."
    return status, message


def get_linux_storage_usage(ip, user, pwd):
    label_wise_usage = _get_label_wise_usage(ip, user, pwd)
    if _is_storage_monitored(label_wise_usage):
        highest_used_percentage = _get_highest_storage_usage(label_wise_usage)
        usage_breakup_message = label_wise_usage.__str__()
    else:
        highest_used_percentage = None
        usage_breakup_message = 'No drives to be monitored as ' \
                                'no suitable labels found'
    return highest_used_percentage, usage_breakup_message

def ssh_util(ip, username, pwd, command):
    shell_command = 'sshpass -p ' + pwd + ' ssh -o StrictHostKeyChecking=no '
    shell_command += username + '@' + ip
    shell_command = shlex.split(shell_command) + [command]
    logger.info("Executing cmd: " + str(shell_command))
    popen_object = Popen(shell_command, stdout=PIPE, stderr=PIPE)
    stdout, stderr = popen_object.communicate()
    if 'Permission denied' in stderr:
        raise ValueError('Login to VM failed. '
                         'Please check the IP and credentials')
    splitlines = [line.split() for line in stdout.split('\n') if line]
    return splitlines


def _get_label_wise_usage(host_ip, username, password):
    label_usage_map = {}
    for label in MONITORED_STORAGE_LABELS:
        label_usage_map[label] = {}
        # extract mount points for label
        for line in ssh_util(host_ip, username, password, LSBLK_CMD):
            if label in line:
                label_usage_map[label].update({line[-1]: None})
        # find usage for each mount point
        for line in ssh_util(host_ip, username, password, DF_CMD):
            mount_point = line[-1]
            if mount_point in label_usage_map[label].keys():
                label_usage_map[label][mount_point] = int(line[-2][:-1])
    return label_usage_map


def _is_storage_monitored(label_wise_usage):
    for drive in label_wise_usage.values():
        if drive:
            return True
    return False


def _get_highest_storage_usage(label_wise_usage):
    candidates = []
    for drive_usage in label_wise_usage.values():
        try:
            candidates.append(max(drive_usage.values()))
        except ValueError:
            pass
    return max(candidates)


def check_windows_storage_usage(ip, user, pwd):
    win_label_wise_usage = _get_win_label_wise_usage(ip, user, pwd)
    if win_label_wise_usage == 'Authentication Error':
        highest_used_percentage = None
        usage_breakup_message = 'Authentication Error please check username and password'
    elif win_label_wise_usage == 'VM Connection Error':
        highest_used_percentage = None
        usage_breakup_message = 'VM maybe ShutOff or Winrm service is not running'
    elif _is_storage_monitored(win_label_wise_usage):
        highest_used_percentage = _get_highest_storage_usage(win_label_wise_usage)
        usage_breakup_message = win_label_wise_usage.__str__()
    else:
        highest_used_percentage = None
        print(highest_used_percentage)
        usage_breakup_message = 'No drives to be monitored as ' \
                                'no suitable labels found'
    return highest_used_percentage, usage_breakup_message


def _get_win_label_wise_usage(ip, user, pwd):
    try:
        session = winrm.Session(ip, auth=(user, pwd), transport="ntlm")
        result = session.run_cmd("wmic logicaldisk where drivetype=3 get caption,volumename /format:value")
        drives_list = result.std_out.replace("\r", "").replace("\n", " ").split(' ')
        drive_usage_map = {}
        for each in drives_list:
            if "Caption" in each:
                caption = each.split("=")[1]
            if "VolumeName" in each:
                label = each.split("=")[1]
                if label == MONITORED_STORAGE_LABELS[0] or label == MONITORED_STORAGE_LABELS[1]:
                    if label not in drive_usage_map:
                        drive_usage_map[label] = {}
                    usage_drive = get_drive_usage_percentage(session, caption)
                    drive_usage_map[label].update({caption: usage_drive})
        return drive_usage_map
    except AuthenticationError as e:
        return 'Authentication Error'
    except Exception as e:
        return 'VM Connection Error'


def get_drive_usage_percentage(session, caption):
    result = session.run_cmd("wmic logicaldisk where DeviceID=" + '"' + caption + '"' + " get size,freespace /format:value")
    sizes_list = result.std_out.replace("\r", "").replace("\n", " ").split(' ')
    for each in sizes_list:
        if "FreeSpace" in each:
            free_space = each.split("=")[1]
        if "Size" in each:
            total_space = each.split("=")[1]
    used_space = int(total_space) - int(free_space)
    usage_percentage = round((float(used_space) / float(total_space) * 100), 2)
    return int(usage_percentage)


def get_max_storage_usage(drive_usage_map):
    usage_drive_wise = []
    for drive_usage in drive_usage_map.values():
        try:
            usage_drive_wise.append(drive_usage)
        except ValueError:
            pass
    return max(usage_drive_wise)


def get_health_status(currentvalue, warn_threshold, crit_threshold, resource_type, custom_message=''):
    try:
        currentvalue = int(currentvalue)
        warn_threshold = int(warn_threshold)
        crit_threshold = int(crit_threshold)
        grapher = '|message info={0};{1};{2};1;100'

        if warn_threshold <= currentvalue < crit_threshold:
            return 1, '{0} usage = {1}%, {2}. Status is Warning'.format(resource_type, currentvalue, custom_message) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)
        elif currentvalue >= crit_threshold:
            return 2, '{0} usage = {1}%, {2}. Status is Critical'.format(resource_type, currentvalue, custom_message) + grapher.format(
                currentvalue, warn_threshold, crit_threshold)
        else:
            return 0, '{0} usage = {1}%, {2}. Status is Ok.'.format(resource_type, currentvalue, custom_message) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)
    except TypeError as excep:
        usage_breakup_message = 'Check the parameters for Floating IP, OS type, and Drive labels'
        return(1, usage_breakup_message)

def get_reachability_status_vm(hostname, domainname, username, password):
    try:
        token_payload = get_token_payload(domainname, username, password)
        token = get_token(hostname, token_payload)
        redis_con.hset('hsop','hsopPrecheck', 1)
        return 0, 'Ok - Token generation passed for {hostname}'.format(hostname=hostname)
    except Exception as e:
        redis_con.hset('hsop','hsopPrecheck', 0)
        return 2, 'Critical - Token generation failed for {hostname}, Please check credentials'.format(
            hostname=hostname)

def get_reachability_status(hostname, domainname, username, password):
    try:
        valid = redis_con.hget('hsop','hsopPrecheck')
        if int(valid):
           return 0, 'Ok - Token generation passed for {hostname}'.format(hostname=hostname)
        else:
           return 2, 'Critical - Token generation failed for {hostname}, Please check credentials'.format(
            hostname=hostname)
    except Exception as e:
        return get_reachability_status_vm(hostname, domainname, username, password)

def main():
    hostname, domainname, username, password, vmid, warn_threshold,\
     crit_threshold,resource_type, floatingip,ostype, vmusername, vmpassword, service_type = check_arg()
    status, message = 0, ''
    if service_type == 'precheck':
        status, message = get_reachability_status(hostname, domainname, username, password)
    elif service_type == 'precheckcluster':
        status, message = get_reachability_status_vm(hostname, domainname, username, password)
    else: 
        status, message = get_stratoscale_vm_resources_status(hostname, domainname, username, password, vmid, warn_threshold, 
            crit_threshold,resource_type, floatingip,vmusername, vmpassword)
    print message
    sys.exit(status)

if __name__ == '__main__':
    main()
