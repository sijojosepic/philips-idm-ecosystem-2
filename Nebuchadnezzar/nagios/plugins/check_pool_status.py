#!/usr/bin/env python
import argparse
import winrm
import sys


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of IIS Application Pool')
    parser.add_argument('-H', '--hostaddress', required=True,
                        help='The host address of the iis application server')
    parser.add_argument('-u', '--username',
                        help='The username of the iis application server', required=True)
    parser.add_argument('-p', '--password',
                        help='The  password of the iis application server', required=True)
    parser.add_argument('-n', '--poolname', required=True,
                        help='The application pool name')
    results = parser.parse_args(args)

    return (
        results.hostaddress,
        results.username,
        results.password,
        results.poolname)


def get_pool_status(hostaddress, username, password, poolname):
    ps_script = "Import-Module WebAdministration;Get-ItemProperty -Path " \
                "IIS:\AppPools\{0} | select name, state".format(poolname)
    session = winrm.Session(hostaddress, auth=(username, password))
    run = session.run_ps(ps_script)
    if 'Started' in run.std_out.split():
        state = 0
        msg = '{0} is up and running.'.format(poolname)
    elif 'Stopped' in run.std_out.split():
        state = 2
        msg = '{0} is stopped.'.format(poolname)
    else:
        state = 1
        msg = '{0} not found.'.format(poolname)
    return state, msg


def main():
    state, msg = get_pool_status(*check_args())
    print(msg)
    sys.exit(state)

if __name__ == '__main__':
    main()
