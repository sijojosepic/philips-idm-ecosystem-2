#!/usr/bin/env python
# This plugin is useful for checking heartbeat of RabbitMQ.
# It creates a queue, post a message to it.
# Checks if message is posted successfully and then fetches that message.
# It deletes the queue, if this complete workflow happens properly
# then this plugin returns 'OK' status with message.
# If anything wrong happens, if throws status code with relevent message.

import sys
import argparse
import json
import requests
import time


PAYLOAD = """<?xml version="1.0"?>
<data xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <transaction_data>
    <id>17f9aaad-bda3-ecbb-c051-39da7764b5c4</id>
    <type>Prefetch</type>
    <sub-type>EC</sub-type>
    <requesting_node>HEARTBEAT.COM</requesting_node>
  </transaction_data>
  <business_data>
    <patient id="000-00-000-0000" patientKey="00000000-0000-0000-0000-000000000000" personKey="00000000-0000-0000-0000-000000000000" />
    <exam key="00000000-0000-0000-0000-000000000000" scheduledDateTime="2016-09-26T06:38:49" />
    <rule_execution>
      <request>
        <fact_attributes>
          <fact_attribute name="Modality">HEARTBEAT</fact_attribute>
          <fact_attribute name="BodyPart">HEARTBEAT</fact_attribute>
          <fact_attribute name="ExamCode">HEARTBEAT</fact_attribute>
          <fact_attribute name="OrganizationId">HEARTBEAT</fact_attribute>
          <fact_attribute name="ExamDescription" />
          <fact_attribute name="SubSpecialtyCode" />
          <fact_attribute name="TriggerType">NEWORDERARRIVAL</fact_attribute>
        </fact_attributes>
        <execution_specifications>
          <context>prefetch</context>
          <type>SINGLE_MATCH</type>
          <group>prefetch_general</group>
        </execution_specifications>
        <response_queue>
          <broker>rabbitMq</broker>
          <exchange>amq.direct</exchange>
          <queueName>IDM_MONITOR_Q</queueName>
          <routingKey>IDM_MONITOR_RK</routingKey>
        </response_queue>
      </request>
    </rule_execution>
  </business_data>
</data>"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check heartbeat status of RabbitMQ server')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address to post to')
    parser.add_argument('-P', '--port', required=True,
                        help='The port to post to')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-i', '--in_queue', required=True,
                        help='The queue_name of the rabbitmq server to post message')
    parser.add_argument('-q', '--out_queue', required=True,
                        help='The queue_name of the rabbitmq server by IDM')
    parser.add_argument('-r', '--in_routing_key', required=True,
                        help='The routing_key of the rabbitmq server to be used')
    parser.add_argument('-x', '--in_exchange', required=True,
                        help='The exchange of the rabbitmq server to be used')
    parser.add_argument('-t', '--timeout', required=True,
                        help='The timeouqt of the rabbitmq server while fetching message')
    results = parser.parse_args(args)
    return (results.hostname,
            results.port,
            results.username,
            results.password,
            results.in_queue,
            results.out_queue,
            results.in_routing_key,
            results.in_exchange,
            results.timeout)


def form_header(username, password):
    headers = {'content-type': 'text/plain', }
    auth = (username, password)
    return headers, auth


def check_rabbimq_availability(headers, auth, hostname, port):
    url = 'https://{0}:{1}/api/queues'.format(hostname, port)
    try:
        response = requests.get(url, headers=headers, auth=auth, timeout=10,
                                verify=False)
    except Exception:
        print 'CRITICAL: Could not connect to RabbitMQ node at {0}'.format(hostname)
        sys.exit(2)
    else:
        if response.status_code != 200:
            msg = 'CRITICAL: Could not connect to RabbitMQ node at {0}, status code: {1}'
            print msg.format(hostname, response.status_code)
            sys.exit(2)


def form_body(exchange, routing_key):
    data = {'vhost':'/','name': exchange,'properties':{'delivery_mode':1,
                                                       'headers':{}},
            'routing_key':routing_key,'delivery_mode':'1','headers':{},
            'props':{},'payload_encoding':'string', 'payload':PAYLOAD}
    return data


def delete_queue(hostname, port, auth, out_queue):
    url = 'https://{0}:{1}/api/queues/%2F/{2}'.format(hostname, port, out_queue)
    try:
        requests.delete(url, auth=auth, verify=False)
    except Exception:
        print 'CRITICAL: Could not delete output queue {0}'.format(out_queue)
        sys.exit(2)


def publish_message(headers, auth, hostname, port, in_exchange, in_queue, body):
    url = 'https://{0}:{1}/api/exchanges/%2f/{2}/publish'.format(hostname, port, in_exchange)
    try:
        response = requests.post(url, data=json.dumps(body), auth=auth,
                                 headers=headers, timeout=60, verify=False)
        if response.status_code != 200:
            msg = 'Could not post message to {0} queue, status code: {1}'
            print msg.format(in_queue, response.status_code)
            sys.exit(2)
    except Exception:
        print 'CRITICAL: Invalid post request for {0} queue.'.format(in_queue)
        sys.exit(2)


def get_message(headers, auth, hostname, port, out_queue, timeout):
    url = 'https://{0}:{1}/api/queues/%2f/{2}/get'.format(hostname, port, out_queue)
    data = {'count': 1, 'requeue': 'false', 'encoding': 'auto'}
    wait_time = 3
    try:
        while timeout >= wait_time:
            time.sleep(wait_time)
            response = requests.post(url, headers=headers, auth=auth,
                                     data=json.dumps(data),
                                     timeout=float(wait_time), verify=False)
            wait_time += 3
            if response.status_code == 200:
                print 'OK: RabbitMQ Heartbeat is up.'
                sys.exit(0)
            else:
                msg = 'CRITICAL: Could not fetch message from {0} queue, status code: {1}'
                print msg.format(out_queue, response.status_code)
                sys.exit(2)
    except Exception:
        print 'CRITICAL: Invalid post(get message) request for {0} queue.'.format(out_queue)
        sys.exit(2)


def main():
    hostname, port, username, password, in_queue, out_queue, in_routing_key,\
    in_exchange, timeout = check_arg()
    headers, auth = form_header(username, password)
    body = form_body(in_exchange, in_routing_key)
    check_rabbimq_availability(headers, auth, hostname, port)
    delete_queue(hostname, port, auth, out_queue)
    publish_message(headers, auth, hostname, port, in_exchange, in_routing_key, body)
    get_message(headers, auth, hostname, port, out_queue, int(timeout))


if __name__ == '__main__':
    main()
