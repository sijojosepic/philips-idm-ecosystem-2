#!/usr/bin/env python

################################################################################################################################################################
# This plugin is to submit the detect and repair timesync script to the ISPACS windows nodes.
# It will execute a power shell script name 'PS_QUERY' below, shared by CE Team from R&D
# The powershell script will detect and repair the change in time sync with NTP server.
# The plugin will be called from a trigger service with namespace :'Product__IntelliSpace__TimeSync__Service__Trigger'.
# The NTP Server list will be provided as an argument upon plugin execution which will be incorporated with the powershell script.
# Upon execution of this plugin aganist any node it will invoke the powershell script which will detect and repair the descrenpencies with respect to timesync.
# The powershell script will rectify the timesync issue of the Domain Controller Nodes to NTP servers provided.
# Here winrm is used to submit timesync powershell script as a fire and forget job, where sleep time for script is 5mins, which is required to continue the
# script execution at the target node even after winrm session expiry.
# For the Domain Computer Nodes the script will rectify the timesync issue with its respective Domain Controller Nodes.
# Also the powershell script will post the output to the passive service :'Product__IntelliSpace__TimeSync__Service__Status', with the correct status change.
# The status message of the plugin will contain a trigger_id which will be matched with the trigger_id of the script response to passive service.
# A sample respose of the script to passive service:
# {
#    "service": "Product__IntelliSpace__TimeSync__Service__Status",
#    "return_code": 1,
#    "output": "{'message': 'The system is found to be not in desired state. Fix applied and recheck passed','DateTime': '04/17/2020 03:06:12','TimeZome': 
#           'Coordinated Universal Time','NTPSource': 'IDM01.ISYNTAX.NET,0x1','TriggerId': 'bcb427b2-8084-11ea-b656-0050568db0bf'}",
#    "host": "idm01if1.idm01.isyntax.net"
# }
# eg running the plugin
# ./check_isp_timesync.py -H 'hostaddress' -U 'username' -P 'password' -a 'NTP Server'
################################################################################################################################################################

from __future__ import print_function
import argparse
import sys
import uuid
from scanline.utilities.win_rm import WinRM
from scanline.utilities.utils import str_to_list
from scanline.utilities.config_reader import get_thruk_response
from scanline.utilities.cache import SafeCache
from scanline.component.localhost import LocalHost
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from timesync_ps_script import PS_QUERY, PDC_ROLE_OWNER_QUERY


OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-H', '--hostaddress', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server')
    parser.add_argument('-N', '--hostname', required=True, help='Hostname of the server')
    parser.add_argument('-a', '--ntp_servers', required=True, help='NTP server list')
    parser.add_argument('-T', '--node_type', required=False, default=2, help='node type')
    results = parser.parse_args(args)
    return results.hostaddress, results.username, results.password, results.hostname, results.ntp_servers, results.node_type

def generate_uuid():
    return str(uuid.uuid1())

def get_neb_ip():
    try:
        return LocalHost().ip_address
    except RuntimeError:
        pass

def format_query(query, ntp_servers, neb_ip, u_uid, hostaddress):
    post_url = "https://"+ neb_ip +"/check_result"
    server_str = ','.join(ntp_servers)
    return query.format(ntpservers = server_str, passive_url = post_url, uid = u_uid, addr = hostaddress)

def get_domain_name(hostname):
    return '.'.join(hostname.split('.')[1:])

def set_pdc_role_owner(win_rm, domain_name):
    QUERY = PDC_ROLE_OWNER_QUERY.format(domain = domain_name)
    out_put = win_rm.execute_ps_script(QUERY)
    domain_pdc_role_owner = out_put.std_out.replace("\n","")
    write_cache(domain_name, domain_pdc_role_owner, 7200)

def read_cache(key):
    return SafeCache.do('get', key)

def write_cache(key, data, cache_timeout):
    SafeCache.do('setex', key, data, cache_timeout)

def get_dc_status(domain_name):
    domain_controller = read_cache(domain_name)
    #TO DO: Get the exact url of thruk api to get service details for a host
    thruk_url = 'cgi-bin/status.cgi?host={dc}'.format(dc = domain_controller)
    thruk_response = get_thruk_response(thruk_url)
    dc_status, msg = UNKNOWN, 'Condition to run not met'
    if thruk_response:
        for item in thruk_response:
            if item.get('display_name') == "Product__IntelliSpace__TimeSync__Service__Status":
                if item.get('state') == OK:
                    dc_status, msg = OK, ''
                elif item.get('state') == CRITICAL:
                    msg = 'The script execution failed in Active MgNode = {0}'.format(domain_controller)
                break         
    else:
        msg = 'Empty response from Thruk API'
    return dc_status, msg


def submit_timesync_job(hostaddress, username, password, hostname, ntp_servers, node_type):
    ntp_servers = str_to_list(ntp_servers)
    if not ntp_servers:
        return OK, 'set NTP_SERVERS and try again'
    neb_ip = get_neb_ip() or ''
    u_uid = generate_uuid()
    domain_name = get_domain_name(hostname)
    try:
        win_rm = WinRM(hostaddress, username, password)
        #'DC' stands for Domain Controller and 'NONDC' is NON Domain Controller
        if node_type == 'DC':
            set_pdc_role_owner(win_rm, domain_name)
        elif node_type == 'NONDC':
            state, msg = get_dc_status(domain_name)
            if state != OK:
                return state, msg
        query = format_query(PS_QUERY, ntp_servers, neb_ip, u_uid, hostaddress)
        out_put = win_rm.execute_long_script(query)
        state = OK
        msg = "OK : Submitted the Powershell script with trigger_id : {0}".format(u_uid)
        return state, msg
    except AuthenticationError as e:
        state, msg = CRITICAL, 'CRITICAL : Authentication Error - {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        state, msg = CRITICAL, 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            #TypeError is a pywinrm bug due to timeout, here win rm is used to submit the power shell job, so response is not mandatory.
            state= OK
            msg = "OK : Submitted the Powershell script with trigger_id : {0}".format(u_uid)
        else:
            state, msg = CRITICAL, 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(hostaddress)
    except Exception as e:
        state, msg = CRITICAL,  'CRITICAL : Exception {0}'.format(e)
    return state, msg

def main():
    state, output = submit_timesync_job(*check_arg())
    print(output, end='')
    sys.exit(state)

if __name__ == '__main__':
    main()
