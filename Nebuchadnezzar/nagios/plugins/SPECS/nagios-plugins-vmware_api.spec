# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_vmware_api

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-vmware_api
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor vmware ESX and vSphere servers

Group:     none
License:   GPL
URL:       http://op5.com
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  perl-Nagios-Plugin
Requires:  VMware-vSphere-CLI
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(HTTP::Date) perl(File::Basename)

Provides:  nagios-plugins-check-vmware_api = %{version}-%{release}

%description
This plugin is used to monitor VMware virtualization environments.
You may monitor DCs, single esx hosts, virtual machines and so on.
To be able to use the plugin you need to install VMware vSphere SDK for Perl.


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_vmware_api.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_vmware_api.pl

%changelog
* Thu Jul 26 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File