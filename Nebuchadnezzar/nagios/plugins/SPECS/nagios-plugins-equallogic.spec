# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_equallogic

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-equallogic
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios plugin to monitor Dell Equallogic.

Group:     none
License:   GPL
URL:       http://www.claudiokuenzler.com/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  net-snmp-utils
Requires:  gawk
Requires:  grep

Provides:  nagios-plugins-check-equallogic = %{version}-%{release}

%description
Checks Dell Equallogic via SNMP.
Can be used to query status and performance info

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_equallogic.sh %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_equallogic.sh

%changelog
* Fri Jun 14 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Updated to version 20121228
* Wed Jul 25 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
