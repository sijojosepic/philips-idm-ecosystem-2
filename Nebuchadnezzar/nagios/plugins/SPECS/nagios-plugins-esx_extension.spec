# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# This is a work around for rhel 5, where pyo pyc were being created and not packaged
# prevent invocation of brp-python-bytecompile
# not needed as targeting rhel 6
# %define __os_install_post %{___build_post}

# Name of the plugin
%global plugin esx_extension
%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-esx_extension
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Extension for esx hardware and temperature check plugin.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Shital Patil<shital.patil@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
Requires:  python >= 2.6
Requires:  python-argparse

Provides:  nagios-plugins-esx_extension = %{version}-%{release}

%description
Plugin to change state and message for esx hardware/temperature.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/esx_extension.py %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/esx_extension.py

%changelog
* Mon Jul 01 2019 Shital Patil <shital.patil@philips.com>
- Initial Spec File