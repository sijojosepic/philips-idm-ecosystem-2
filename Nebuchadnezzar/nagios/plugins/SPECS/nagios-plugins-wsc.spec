# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_wsc

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-wsc
Version:   %{_phiversion}
Release:   2%{?dist}
Summary:   Nagios - web service checker plugin.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(Net::SNMP)
Requires:  perl(Getopt::Long)

Provides:  nagios-plugins-check-wsc = %{version}-%{release}

%description
Nagios talks via this plugin to a VB.NET web service.
Different types of query can be passed which the service performs 
on behalf of Nagios, compiles the results and then passes back to 
Nagios to display. If the service detects problems, it passes back 
sufficient information for Nagios to alert as per normal. 


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_wsc_v14.pl %{buildroot}%{nagiospluginsdir}/check_wsc.pl
%{__sed} -i 's#\(use\s*lib\).*plugins[^;]*#\1 "%{nagiospluginsdir}"#g' %{buildroot}%{nagiospluginsdir}/check_wsc.pl

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_wsc.pl

%changelog
* Wed Jul 25 2012 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
