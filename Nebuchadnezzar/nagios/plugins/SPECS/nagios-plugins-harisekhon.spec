# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin harisekhon

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-%{plugin}
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - Harisekhon Library used by other plugins

Group:     none
License:   GPL
URL:       https://github.com/harisekhon
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
Requires:  perl-JSON

Provides:  nagios-plugins-%{plugin} = %{version}-%{release}

%description
This is a library used by other plugins by harisekhon.

HARI SEKHON:

Library of personal stuff I use a lot, cobbled together from bits of my own
scripts over the last few years and a Nagios library I started in Python years ago

I welcome feedback on this. Currently this lib isn't designed for purity but rather convenience
and ease of maintenance.  If you have a better way of doing anything in this library that will
not significantly inconvenience me then let me know!

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}/harisekhon/lib
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/HariSekhonUtils.pm %{buildroot}%{nagiospluginsdir}/harisekhon/lib/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%dir %{nagiospluginsdir}/harisekhon/
%attr(0755,root,root) %{nagiospluginsdir}/harisekhon/lib

%changelog
* Wed Feb 18 2015 Jason Malobicky <jason.malobicky@philips.com>
- Changed file permissions on lib directory so the Nagios user would have access to the necessary library

* Fri Jan 02 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File