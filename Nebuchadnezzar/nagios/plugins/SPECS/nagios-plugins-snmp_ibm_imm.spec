# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_ibm_imm

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-snmp_ibm_imm
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - script to monitor IBM XSeries IMMs via SNMP.

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Jason Malobicky <jason.malobicky@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins 
# Rpmbuild doesn't find these perl dependencies
Requires:  perl(Net::SNMP)

Provides:  nagios-plugins-check-snmp_ibm_imm = %{version}-%{release}

%description
This is a script to monitor IBM XSeries IMMs via SNMP.
It will do the following Following: 
* Temperature 
* Fans
* Health
* Voltage


%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_snmp_ibm_imm.sh %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_snmp_ibm_imm.sh

%changelog
* Mon Apr 11 2014 Jason Malobicky <jason.malobicky@philips.com>
- Initial Spec File