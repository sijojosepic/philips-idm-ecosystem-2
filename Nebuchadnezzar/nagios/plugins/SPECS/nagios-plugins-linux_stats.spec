# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_linux_stats

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-linux_stats
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - Plugin to monitor linux stats

Group:     none
License:   GPL
URL:       http://exchange.nagios.org/directory/Plugins/Operating-Systems/Linux/check_linux_stats/details
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins
Requires:  perl-Sys-Statistics-Linux

Provides:  nagios-plugins-linux_stats = %{version}-%{release}

%description
Plugin to check linux system performance (cpu, mem, load, disk usage, disk io, network usage, open files and processes).
A perl plugin using Sys::Statistics::Linux

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_linux_stats.pl %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_linux_stats.pl

%changelog
* Thu Jan 29 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File