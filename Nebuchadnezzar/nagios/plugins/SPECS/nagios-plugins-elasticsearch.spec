# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Name of the plugin
%global plugin check_elasticsearch

%global nagiospluginsdir %{_libdir}/nagios/plugins

# Basic Information
Name:      nagios-plugins-elasticsearch
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   Nagios - script to monitor ElasticSearch.

Group:     none
License:   GPL
URL:       https://github.com/orthecreedence/check_elasticsearch/blob/master/check_elasticsearch
# Packager Information
Packager:  Jason Malobicky <jason.malobicky@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: i386 x86_64

# Source Information
Source0:   %{plugin}-%{version}.tar.gz
#Patch0:

# Owns the nagios plugins directory
Requires:  nagios-plugins

Provides:  nagios-plugins-elasticsearch = %{version}-%{release}

%description
This is a script to monitor ElasticSearch.

%prep
%setup -q -n %{plugin}-%{version}

%build
echo OK

%install
%{__rm} -rf %{buildroot}

%{__install} -d -m 0755 %{buildroot}%{nagiospluginsdir}
%{__install} -Dp -m 0755 %{_builddir}/%{plugin}-%{version}/check_elasticsearch.sh %{buildroot}%{nagiospluginsdir}/

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root, -)
%attr(0755,root,root) %{nagiospluginsdir}/check_elasticsearch.sh

%changelog
* Tue Aug 05 2014 Jason Malobicky <jason.malobicky@philips.com>
- Initial Spec File