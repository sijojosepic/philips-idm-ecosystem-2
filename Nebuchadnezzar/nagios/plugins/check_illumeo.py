#!/usr/bin/env python
import sys
import requests
import argparse
import redis

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check illumeo nodes')
    parser.add_argument('-P', '--protocol', default='http',
                        help='web protocol')
    results = parser.parse_args(args)
    return (results.protocol)

def get_illumeo_status():
    try:
        redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
        noncore_nodes = ['Rviewer','Eviewer','I4Processing']
        status_message = 'Either one {0} is available'
        for key in redis_con.keys(pattern="*ISPACS*"):
            if any(module_type in key for module_type in noncore_nodes):
                return 0, status_message.format(','.join(noncore_nodes))
        return get_illumeo_status_from_restapi(check_arg())
    except:
        return get_illumeo_status_from_restapi(check_arg())


def get_siteid():
    file_path = '/etc/siteid'
    with open(file_path) as fd:
        return fd.read().strip()

def get_illumeo_status_from_restapi(protocol):
    try:
        siteid = get_siteid()
        url = '{0}://vigilant/pma/facts/{1}/nodes'.format(protocol, siteid)
        response = requests.get(url, verify=False)
        data = response.json()
        results = data['result']
        number_of_hosts = len(results)
        noncore_nodes = ['Rviewer','Eviewer','I4Processing']
        status_message = 'Either one {0} is available'
        for host in range(number_of_hosts):
            module_type = results[host]['module_type']
            if module_type in noncore_nodes:
                return 0, status_message.format(','.join(noncore_nodes))
        status_message = ','.join(noncore_nodes) + ' not available'
        return 3, status_message
    except:
        return 3, 'could not connect to Fact rest api'

def main():
    status, message = get_illumeo_status()
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()

