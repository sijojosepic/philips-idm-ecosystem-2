#!/bin/bash
#Version:0.0.0.0
#set -x
#This script will check for a version number in a web page
#  The version is expected in 0.0.0.0 format within a node with property id="APP_VERSION"
#  The first string matching this pattern in the document will be used.
#  The check will be done via https, certificate validity is ignored.
#  This script is not smart enough to handle complex variations on the node for this reason:
#    The node must be childless.
#    The opening and closing tag should avoid spannig multiple lines.

STATE_OK=0              # define the exit code if status is OK
STATE_WARNING=1         # define the exit code if status is Warning
STATE_CRITICAL=2        # define the exit code if status is Critical
STATE_UNKNOWN=3         # define the exit code if status is Unknown

BASENAME="/bin/basename"
SED="/bin/sed"
CURL="/usr/bin/curl"
GREP="/bin/grep"
HEAD="/usr/bin/head"

PROGNAME="$BASENAME $0"
PROGPATH=$( echo $0 | $SED -e 's,[\\/][^\\/][^\\/]*$,,' )
REVISION="[CE#!VersionNumber!#]"
AUTHOR="2013 Leonardo Ruiz"

#GLOBALS
HOSTNAME="" 
URL=""
VERSION_STRING=""
PROTO[0]="https://"
PROTO[1]="k"


ShowUsageAndExit()
{
    EXIT_CODE=0
    [ -z "$1" ] || EXIT_CODE="$1"
    echo
    echo "Usage: ${PROGNAME} [OPTIONS]"
    echo
    echo "-H | --hostname ; hostname or ip address of the remote server"
    echo "-u | --url ; url of the version.html location"
    echo "-h | --help ; Display this message"
    echo "-v | --version ; Display the version of the script"
    echo "-n | --no-ssl ; Use http"
    echo
    exit $EXIT_CODE
}

ShowVersion()
{
    echo
    echo "Script Version:" $VERSION
    echo "Author:" $AUTHOR
    echo
}

SetNoSSL()
{
    PROTO[0]="http://"
    PROTO[1]=""
}

# MAIN

if [ "$#" -lt 1 ]; then
    ShowUsageAndExit $STATE_UNKNOWN
fi

while [ "$1" != "" ]; do
    case $1 in
        -H | --hostname )
            shift
            HOSTNAME=$1
            ;;
        -u | --url )
            shift
            URL=$1
            ;;
        -h | --help )
            ShowUsageAndExit
            ;;
        -v | --version )
            ShowVersion
            exit
            ;;
        -n | --no-ssl )
            SetNoSSL
            ;;
        * )
            ShowUsageAndExit $STATE_UNKNOWN
    esac
    shift
done


[ -z "$HOSTNAME" ] && ShowUsageAndExit $STATE_UNKNOWN

_ADDRESS="${PROTO[0]}${HOSTNAME}/${URL}"

_HTML="$( $CURL -sf${PROTO[1]} "${_ADDRESS}" )"
if [ $? -gt 0 ]
then
    echo "CRITICAL - Address '${_ADDRESS}' could not be read"
    exit $STATE_CRITICAL
fi

_APP_VERSION="$( echo "$_HTML" | $GREP -Po '<.*\s*id="APP_VERSION".*?>[^<]*<\/.*?>' )"
if [ $? -gt 0 ]
then
    echo "CRITICAL - APP_VERSION not found"
    exit $STATE_CRITICAL
fi

_VERSION_MATCHES="$( echo "$_APP_VERSION" | $GREP -Po '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' )"
if [ $? -gt 0 ]
then
    echo "CRITICAL - Version not found or in incorrect format"
    exit $STATE_CRITICAL
fi

VERSION_STRING="$( echo "$_VERSION_MATCHES" | $HEAD -1 )"

echo "OK - Version in correct format ++ ${VERSION_STRING}"
exit $STATE_OK
