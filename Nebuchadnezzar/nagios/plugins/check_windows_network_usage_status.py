#!/usr/bin/env python

"""
Shinken Plugin - Windows network usage monitering using WinRM.

This plugin capture the present network usage and report using power shell query.

example:

python check_windows_network_usage_status.py -H 192.168.xx.xxx -U 'dmin' -P 'pa55w0rd' -C 90 -W 75
OK - The network utlization is 10 percentage

The service will alert depends upon the percentage of network usage in a system.(Also configurable)
     CRITICAL - 90
     WARNING - 75
     OK - below 75

"""
import argparse
import sys
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException
from scanline.utilities.win_rm import WinRM

GRAPHER = '| network usage={0};{1};{2};0;100'

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

PSCRIPT = """$interface = Get-WmiObject -class Win32_PerfFormattedData_Tcpip_NetworkInterface |
select BytesTotalPersec, CurrentBandwidth,PacketsPersec|where {$_.PacketsPersec -gt 0} ;
$bitsPerSec = $interface.BytesTotalPersec*8 ; $totalBandwidth = $interface.CurrentBandwidth ;
$network_usage = (( $bitsPerSec / $totalBandwidth) * 100); $rounded_network_usage = [math]::Round($network_usage);
Write-host $rounded_network_usage """


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Monitor the windows network usage.')
    parser.add_argument('-H', '--hostname', required=True, help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True, help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-C', '--critical', required=True, help='The Critical threshold value')
    parser.add_argument('-W', '--warning', required=True, help='The Warning threshold value')
    results = parser.parse_args(args)
    return (results.hostname, results.username,
            results.password, results.critical, results.warning)


def process_output(network_usage, critical, warning):
    """ The messages related with network utilization constructed here."""
    if network_usage.status_code == OK:
        count_network_usage = int(network_usage.std_out)
        text_msg = "{0} - The network usage is {1} percentage which has breached the threshold value of {2} percentage"
        if count_network_usage >= int(critical):
            status, msg = CRITICAL, text_msg.format("CRITICAL", count_network_usage, critical) + GRAPHER.format(
                CRITICAL, warning, critical)
        elif count_network_usage >= int(warning):
            status, msg = WARNING, text_msg.format("WARNING", count_network_usage, warning) + GRAPHER.format(
                WARNING, warning, critical)
        else:
            text_msg = "{0} - The network usage is {1} percentage"
            status, msg = OK, text_msg.format("OK", count_network_usage) + GRAPHER.format(OK, warning, critical)
    else:
        status, msg = CRITICAL, 'WinRM Error {0}'.format(network_usage.std_err)
    return status, msg


def main(host, username, password, critical, warning):
    """ This method is used to handling WINRM requests. """
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        network_usage = win_rm.execute_ps_script(PSCRIPT)
        status, msg = process_output(network_usage, critical, warning)
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : Invalid login credentials'
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(host)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(host)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)

    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*parse_cmd_args())
