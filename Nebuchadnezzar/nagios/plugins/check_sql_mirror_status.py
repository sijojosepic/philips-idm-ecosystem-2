#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

from __future__ import print_function
import sys
import argparse
import pymssql
from scanline.utilities import dbutils

DB_STATE_QUERY = """DECLARE @dbcount int;
     DECLARE @dbcountgood int;
     SET @dbcount = (select count(*) from master.sys.database_mirroring where mirroring_partner_instance <> 'NULL');
     SET @dbcountgood = (select count(*) from master.sys.database_mirroring where mirroring_role = '1');
     IF (@dbcount <> @dbcountgood)
     Select 'Flag Error'
     ELSE Select 'OK'"""

FAILOVER_QUERY = """DECLARE @dbdate_start datetime;
            DECLARE @dbdate_end datetime;
            SET @dbdate_end = CURRENT_TIMESTAMP;
            SET @dbdate_start = CURRENT_TIMESTAMP-0.003473;
            EXEC master.dbo.xp_readerrorlog 0, 1, N'"Audit" is changing roles', NULL, @dbdate_start, @dbdate_end, N'desc'
            SELECT 'FLAG';"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-S', '--service', required=True, help='Service of the server')
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.service, results.vigilant_url)


def get_formatted_data(db_hosts, state_result, failover_result, service):
    """ This method returns formatted message with service status for sql mirroring."""
    ok_msg = 'OK - Primary database is up and running fine on {DB_node}'
    failover_node_msg = '"PRINCIPAL" to "MIRROR"'
    takeover_node_msg = '"MIRROR" to "PRINCIPAL"'
    if state_result[0] == 'Flag Error' and state_result[1] == 'Flag Error':
        if service == 'splitbrain':
            status, outmsg = 2, 'CRITICAL - Detected split brain state.' \
                                'All the database needs to be made Primary on Single Database Server.'
        else:
            status, outmsg = 1, 'WARNING - Failover transformation in progress.'
    else:
        status, outmsg = 0, ok_msg.format(DB_node=db_hosts[state_result.index('OK')])
        if service == 'failover':
            if failover_result[0][0] != 'FLAG':
                if failover_node_msg in failover_result[0][2]:
                    status, outmsg = 1, 'WARNING - Detected successful failover, {dnode} is now the primary.'.format(
                        dnode=db_hosts[1])
                if takeover_node_msg in failover_result[0][2]:
                    status, outmsg = 1, 'WARNING - Detected successful failover, {dnode} is now the primary.'.format(
                        dnode=db_hosts[0])

            if failover_result[1][0] != 'FLAG':
                if failover_node_msg in failover_result[1][2]:
                    status, outmsg = 1, 'WARNING - Detected successful failover, {dnode} is now the primary.'.format(
                        dnode=db_hosts[0])
                if takeover_node_msg in failover_result[1][2]:
                    status, outmsg = 1, 'WARNING - Detected successful failover, {dnode} is now the primary.'.format(
                        dnode=db_hosts[1])

    return status, outmsg


def get_db_mirror_status(hostname, username, password, database, service, vigilant_url):
    """ This method returns the DB Mirroring status."""
    db_hosts = filter(lambda x: hostname in x, dbutils.get_dbnodes(vigilant_url))
    if db_hosts:
        db_hosts = db_hosts[0]
    else:
        db_hosts = dbutils.get_dbpartner_in_dbscanner(hostname, vigilant_url)

    db_len = len(db_hosts)
    if db_len == 1:
        status, outmsg = 0, 'OK - This environment contains single DB node.'
        return status, outmsg
    elif db_len == 0:
        status, outmsg = 2, 'CRITICAL - Unable to retrieve DB nodes due to connection issue with vigilant or DB nodes not reachable.'
        return status, outmsg
    elif db_len > 1:
        if not dbutils.get_mirroring_primary_check(hostname=hostname, username=username, password=password,
                                                   database=database):
            status, outmsg = 0, 'OK: This service check only applicable for primary node. {node} is not the primary node.'.format(
                node=hostname)
            return status, outmsg
    state_result = []
    failover_result = []
    con_err, conn_err_msg = [], ''
    for host in db_hosts:
        try:
            conn = pymssql.connect(server=host, user=username, password=password, database=database, login_timeout=10)
        except Exception as e:
            con_err.append(host)
            continue
        cursor = conn.cursor()
        cursor.execute(DB_STATE_QUERY)
        state_result.append(cursor.fetchone()[0])
        cursor.execute(FAILOVER_QUERY)
        failover_result.append(cursor.fetchone())
        conn.close()

    if con_err:
        status, outmsg = 2, 'CRITICAL - Connection error with {node}.'.format(node=str(con_err).strip('[]'))
        return status, outmsg

    return get_formatted_data(db_hosts, state_result, failover_result, service)


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_db_mirror_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()
