#!/usr/bin/env python

#################################################################################
#
# Plugin name : esx_extension.py
# Author      : shital.patil@philips.com
# Date        : 01 July 2019
# Usage       : ./esx_extension.py -X '$SERVICESTATE::Virtualization__VMware__ESX__Overall__Status$'
#                -C ./check_vmware_api.pl -H hostname -u username -p password -l runtime -s health
# Description : This plugin is aimed to have a better extension for check_vmware_api.pl.
#               The nagios plugin(check_vmware_api.pl) was too verbose and was not giving the
#               response in the correct format.
#
#               Applied for ESXi:
#                   1. Hardware Health
#                   2. Temperature
#
#               The plugin takes the unusual parameters like Service state and command.
#               Service State: Service state of esx overall status.
#               command : The command to be executed using Popen inside plugin.
#
################################################################################


from __future__ import print_function

import argparse
import sys
from subprocess import Popen, PIPE


def check_args(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-X', '--condition', help='the string to decide if to run', required=True)
    parser.add_argument('-C', '--command', help='the command to run', nargs=argparse.REMAINDER, required=True)
    results = parser.parse_args(args)
    return results.condition, results.command


def get_unknown_output(message):
    return 3, 'UNKNOWN - {0}'.format(message), None


def scrub_output(message):
    if message:
        return message.replace('$', '#')


def set_outmsg(service_msg, outmsg):
    return outmsg.replace('UNKNOWN', service_msg, 1)


def esx_extension(condition, command):
    if condition != 'OK':
        return get_unknown_output('Condition to run not met.')
    try:
        popen_obj = Popen(command, stdout=PIPE, stderr=PIPE)
        _stdout, _stderr = popen_obj.communicate()
    except Exception as e:
        return get_unknown_output(str(e))
    outmsg = scrub_output(_stdout)
    if popen_obj.returncode == 3:
        if ':' in outmsg:
            result_msg = outmsg[outmsg.index(':'):]
            unknown_count = result_msg.count('UNKNOWN')
            critical_count = result_msg.count('CRITICAL')
            warning_count = result_msg.count('WARNING')
            if critical_count and unknown_count:
                service_msg = 'CRITICAL - Reset of ESX sensor and CIM service restart might be required to get right status.\n Results'
                outmsg = set_outmsg(service_msg, outmsg)
                return 2, outmsg, _stderr
            elif warning_count and unknown_count or unknown_count:
                service_msg = 'WARNING - Reset of ESX sensor and CIM service restart might be required to get right status.\n Results'
                outmsg = set_outmsg(service_msg, outmsg)
                return 1, outmsg, _stderr
    return popen_obj.returncode, outmsg, _stderr


def main():
    state, output, error = esx_extension(*check_args())
    if output:
        print(output, end='')
    if error:
        print(error, file=sys.stderr, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()
