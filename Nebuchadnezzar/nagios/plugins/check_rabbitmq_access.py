#!/usr/bin/env python

import argparse
import requests
import sys
from requests.exceptions import ConnectTimeout, HTTPError, RequestException

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check access for RabbitMq')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the rabbitmq server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the rabbitmq server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-r', '--protocol', default='https',
                        help='The password of the iis application server')
    parser.add_argument('-t', '--timeout', default=15,
                        help='The rabbitmq server timeout.')
    results = parser.parse_args(args)

    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.protocol,
        results.timeout)


def get_rabbitmq_status(hostname, port, username, password, protocol, timeout):
    try:
        url = '{0}://{1}:{2}/api/aliveness-test/%2F'.format(protocol, hostname, port)
        auth = (username, password)
        response = requests.get(url, auth=auth, verify=False, timeout=int(timeout))
        response.raise_for_status()
        return OK, 'OK : RabbitMq Alive'
    except HTTPError as e:
        if 'Unauthorized' in e.message:
            return CRITICAL, 'CRITICAL : Authentication Error - RabbitMQ Username or Password is not valid'
        else:
            return CRITICAL, 'CRITICAL : HTTPError - {0}'.format(e)
    except ConnectTimeout as e:
        return CRITICAL, 'CRITICAL : Connection Error - RabbitMQ server is not accessible'
    except RequestException as e:
        return CRITICAL, 'CRITICAL : Request Exception  - {0}'.format(e)
    except Exception as e:
        return CRITICAL, 'CRITICAL : Error while connecting to {0} - {1}'.format(hostname, e)


def main():
    status, message = get_rabbitmq_status(*check_arg())
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()
