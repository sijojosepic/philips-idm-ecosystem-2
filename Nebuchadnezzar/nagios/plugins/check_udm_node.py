#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

import argparse
import json
import redis
import sys
from scanline.utilities.http import HTTPRequester
from scanline.utilities.config_reader import get_site_id

OK = 0
UNKNOWN = 3
CRITICAL = 2
MODULE_URL = '/pma/facts/{site_id}/modules/ISP/keys/module_type'


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of RabbitMQ Services')
    parser.add_argument('-V', '--vigilant_url', required=True,
                        help='The vigilant URl')
    results = parser.parse_args(args)
    return results.vigilant_url


def check_in_facts(siteid, vigilant_url):
    module_url = vigilant_url.rstrip('/') + MODULE_URL.format(site_id=siteid)
    http_obj = HTTPRequester()
    resp = http_obj.suppressed_get(module_url, headers={'X-Requested-With': "True"})
    if resp:
        host_info = json.loads(resp.content) if resp.content else {}
        for host in host_info.get('result', []):
            if host['ISP']['module_type'] == 1024:
                return True


def check_in_redis(host='localhost', port=6379, db=0):
    redis_con = redis.StrictRedis(host=host, port=port, db=db)
    for key in redis_con.keys(pattern="*ISPACS*"):
        if key.split('#')[-1] == '1024':
            return True


def main():
    status, outmsg = UNKNOWN, 'UNKNOWN - APP server(1024) is not present, UDM not available'
    if check_in_redis() or check_in_facts(get_site_id(), check_args()):
        status, outmsg = OK, 'OK - APP server(1024) is present, UDM available'
    print outmsg
    sys.exit(status)


if __name__ == '__main__':
    main()
