#!/usr/bin/env python
import winrm
import sys
import argparse
import requests
from scanline.utilities.installing_package_status import connect, check_package


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script to install packages on windows machine.')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host name of the windows server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the windows server')
    parser.add_argument('-p', '--password', required=True,
                        help='The passowrd of the windows server')
    parser.add_argument('-s', '--service_name', required=True,
                        help='Service name of the installable file')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.username,
        results.password,
        results.service_name)

def main():
    hostname, username, password, service_name = check_args()
    username = username.replace('\\\\', '\\')
    conn = connect(hostname, username, password)
    status_code, msg = check_package(conn, service_name, hostname)
    print msg
    sys.exit(status_code)


if __name__ == '__main__':
    main()
