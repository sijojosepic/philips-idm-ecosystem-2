#!/usr/bin/env python

"""
This plugin is used for recursively listing down the files in a given directory, based on the file extentions supplied.
1. distributed packages in the '/var/philips/repo/Tools/PCM/packages/'
example: ./get_distributed_package.py -p '/var/philips/repo/Tools/PCM/packages/' -e '["xml", "zip"]'
output:
CE411
https://192.168.59.66/repo/Tools/PCM/packages/CE411/CE_Bundle-4.11.xml
https://192.168.59.66/repo/Tools/PCM/packages/CE411/CE_Bundle-4.11.zip
https://192.168.59.66/repo/Tools/PCM/packages/CE411/PhilipsWin2k12r2_base_binaries-4.11.0.0.1712.xml
https://192.168.59.66/repo/Tools/PCM/packages/CE411/PhilipsWin2k12r2_base_binaries-4.11.0.0.1712.zip
ISP
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.552.28.201904081308.xml
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.552.28.201904081308.zip
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.553.30.201907081308.xml
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.553.30.201907081308.zip

2. site info packages in the '/var/philips/info/'
example: ./get_distributed_package.py -p '/var/philips/info/' -e '["xml"]'
output:
https://192.168.59.66/info/siteinfo1234.xml
https://192.168.59.66/info/siteinfo2345.xml
"""

from __future__ import print_function
import os
from scanline.component import localhost
import sys
import argparse
from scanline.utilities.utils import str_to_list


PKG_MSG_TEMPLATE = 'https://{ip_address}/{valid_path}/{parent_dir}/{file_name}'
SITE_INFO_MSG_TEMPLATE = 'https://{ip_address}/{valid_path}/{file_name}'
EOL = '\n'


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Path and extensions parameters')
    parser.add_argument('-p', '--path', required=True, help='The path of the directory')
    parser.add_argument('-e', '--extensions', required=True, help='The extensions of file')
    results = parser.parse_args(args)

    return (results.path, results.extensions)


def get_neb_ip():
    localhost_obj = localhost.LocalHost()
    return localhost_obj.ip_address


def check_valid_file(tmp_file, extensions):
    tmp = tmp_file.split('.')
    if tmp[-1] in extensions:
        return True
    else:
        return False


def get_directory_struct(path, valid_path, temp_str, ip_address, extensions, use_case, parent_dir=''):
    dir_contents = os.listdir(path)
    for content in sorted(dir_contents):
        if os.path.isdir(path + content):
            temp_str += content + EOL
            temp_str = get_directory_struct(path + content + '/', valid_path, temp_str, ip_address, extensions, use_case, content)
        else:
            if check_valid_file(content, extensions):
                if use_case == 'packages':
                    temp_str += PKG_MSG_TEMPLATE.format(ip_address=ip_address, valid_path=valid_path, parent_dir=parent_dir, file_name=content) + EOL
                else:
                    temp_str += SITE_INFO_MSG_TEMPLATE.format(ip_address=ip_address, valid_path=valid_path, file_name=content) + EOL
    return temp_str


def get_files_list(path, extensions):
    msg_str = ''
    temp_str = ''
    valid_path = ''
    extensions = str_to_list(extensions)
    ip_address = get_neb_ip()
    path_values = path.strip('/').split('/')
    use_case = path_values[-1]
    for value in path_values:                   #path - /var/philips/repo
        if value not in ['var', 'philips']:     #valid _path -becomes /repo as the download url should not include it.
            valid_path += value + '/'
    if os.path.exists(path):
        msg_str = get_directory_struct(path, valid_path.rstrip('/'), temp_str, ip_address, extensions, use_case)
    else:
        msg_str = 'Specified path does not exist in Neb'
    return msg_str


def main():
    cmd_args = check_arg()
    output = get_files_list(*cmd_args)
    print(output, end='')
    sys.exit(0)


if __name__ == '__main__':
    main()