#!/usr/bin/env python
# Version:0.1
##########################################################
# This is intended to find the DFS Replication error in the windows Domain Controller Node.
# The plugin status should be "CRITICAL" if there is any error with event id : 5002 in the windows event log.
# Behaviour
# ---------
# IF there is DFS Replication error and the event id is 5002
#         plugin exit with CRITICAL status with the count of occurances and error message
# else
#         plugin exit with OK status
#
# The event id which needs to monitor should be configurable.
# if any more event id  which needs to be checked/monitored can be supplied to the Plugin.
# The number of hours the error logs monitored should be configurable and the default should be 10 hours.
#
#
# example
# python check_windows_eventlog_status.py -H 10.10.xxx.xxx -U 'admin' -P 'password' -T 10 -E "['5002','5008']" -S DFSR
#
# CRITICAL : EventId-5002,Count-17, The DFS Replication service encountered an error communicating with partner
# ISEEDC1 for replication group Domain System Volume.
# OK : EventId-5008,No Error Log Found
#
##########################################################


from __future__ import print_function

import argparse
import sys
from scanline.utilities.win_rm import exec_ps_script
from scanline.utilities.utils import str_to_list

# This is for service graph in the thruk portal
GRAPHER = '| service status={0};1;2;0;2'

OK = 0
CRITICAL = 2
UNKNOWN = 3

EVENT_QUERY = "$result = Get-WinEvent -ProviderName  {0} | where-object {{$_.Id -In {1} -And $_.LevelDisplayName -eq 'Error' -And $_.TimeCreated -ge  $from_time }}| Sort-Object TimeCreated -Descending |ForEach-Object {{($_.Message -split '\n')[0].Trim() +'|'+ $_.Id}}; $result"

FROM_TIME_QUERY = "$from_time = (Get-Date).AddHours(-{0})"


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Monitor specific windows event log error')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of the windows node')
    parser.add_argument('-U', '--username', required=True,
                        help='Username of the windows node')
    parser.add_argument('-P', '--password', required=True,
                        help='Password of the windows node')
    parser.add_argument('-S', '--source', required=True, default='DFSR',
                        help='Source of windows eventlog ')
    parser.add_argument('-T', '--hours', required=True,
                        help='Number of hours of windows eventlog data to process')
    parser.add_argument('-E', '--eventid', required=True,
                        help='Windows event error log ids to monitor')

    results = parser.parse_args(args)
    return (results.hostname, results.username, results.password, results.source, results.hours, results.eventid)


class WinEventLogParser(object):

    def __init__(self, hostname, username, password, source, hours, event_ids):
        self.username = username
        self.password = password
        self.hostname = hostname
        self.source = source
        self.hours = hours
        self.event_ids = str_to_list(event_ids)

    def from_time_query(self):
        return FROM_TIME_QUERY.format(self.hours)

    def event_query(self):
        return EVENT_QUERY.format(self.source, ','.join(self.event_ids))

    def ps_query(self):
        return self.from_time_query() + ";" + self.event_query()

    def format_msg(self, eventid, count, err_msg):
        if err_msg and count:
            msg = 'EventId-{0},Count-{1}, {2}\n'.format(eventid, count, err_msg)
        else:
            msg = 'EventId-{0},No Error Log Found.\n'.format(eventid)
        return msg

    def compose_msg(self, ok_msg, crt_msg):
        pre_ok = 'OK : '
        pre_crt = 'CRITICAL : '
        if crt_msg:
            crt_msg = pre_crt + crt_msg
        if ok_msg:
            ok_msg = pre_ok + ok_msg
        final_msg = crt_msg + ok_msg
        return final_msg

    def compose_status(self, status_dict):
        ok_msg, crt_msg = '', ''
        status = OK
        for eventid in self.event_ids:
            if status_dict.get(eventid):
                count = status_dict[eventid].get('count')
                err_msg = status_dict[eventid].get('err_msg')
                crt_msg += self.format_msg(eventid, count, err_msg)
                status = CRITICAL
            else:
                ok_msg += self.format_msg(eventid, count=None, err_msg=None)
        final_msg = self.compose_msg(ok_msg, crt_msg)
        return status, final_msg

    def aggregate_log(self, event_log):
        status_dict = dict()
        for line in event_log.splitlines():
            err_msg, eventid = line.split('|')
            if not status_dict.get(eventid):
                status_dict[eventid] = {}
                status_dict[eventid]['err_msg'], status_dict[eventid]['count'] = err_msg, 0
            status_dict[eventid]['count'] += 1
        return status_dict

    def get_status(self, event_log):
        status_dict = self.aggregate_log(event_log)
        return self.compose_status(status_dict)

    def execute_powershell(self):
        status, event_log = exec_ps_script(self.hostname, self.username, self.password, self.ps_query())
        if status == OK:
            if event_log.status_code == 0:
                status, msg = self.get_status(event_log.std_out)
                if status == CRITICAL:
                    msg = msg + GRAPHER.format(CRITICAL)
                else:
                    msg = msg + GRAPHER.format(OK)
            else:
                msg = 'CRITICAL : {0}'.format(str(event_log.std_out.strip())) + GRAPHER.format(CRITICAL)
        else:
            msg = event_log + GRAPHER.format(status)
        return status, msg


def main(hostname, username, password, source, hours, event_ids):
    status, msg = CRITICAL, ''
    try:
        if event_ids:
            obj = WinEventLogParser(hostname, username, password, source, hours, event_ids)
            status, msg = obj.execute_powershell()
        else:
            status = OK
            msg = 'Try after setting this variable - $DOMAIN_EVENT_ID$'
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_args())
