#!/usr/bin/env python

###########################################################################################################
# This plugin is to determine the presence of internet access from a targed windows host.
# As per the feautre, In the Plugin there are two ways used to identify internet access.
# First - check for access of  www.google.com
# Second - check for access of www.usa.philips.com
# If any of them is reponding with status code as 200, then internet access is present,
# and respective alert would be raised
#
#
# The PowerShell script to identify internet access behaves as follows:
#
# 1. [System.Net.WebRequest]::Create($r).GetResponse() if the query returns 200 - internet access is there.
# 2. if the WebExpection with NameResolution is the ExceptionStatus - then no internet access is there
# 3. if the WebExpection with NameResolution is not the ExceptionStatus - then internet access is there.
# 4. if any other exception, which means the powershell script execution fails.
###########################################################################################################


from __future__ import print_function
import argparse
import sys
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException

QUERY_FOR_GOOGLE = '''
try
{
    $r =  "https://www.google.com"
    $res = [System.Net.WebRequest]::Create($r).GetResponse()
    if([int]$res.StatusCode -eq 200){
        Write-Host "2"
    }
}
catch [System.Net.WebException]
{
    if($_.Exception.Status -eq [System.Net.WebExceptionStatus]::NameResolutionFailure)
    {
        Write-Host "0"
        return $_
    }
    else
    {
        Write-Host "2"
        return $_
    }
}
catch
{
    Write-Host "3"
    return $_
}
'''
QUERY_FOR_PHILIPS = '''
try
{
    $r =  "https://www.usa.philips.com"
    $res = [System.Net.WebRequest]::Create($r).GetResponse()
    if([int]$res.StatusCode -eq 200){
        Write-Host "2"
    }
}
catch [System.Net.WebException]
{
    if($_.Exception.Status -eq [System.Net.WebExceptionStatus]::NameResolutionFailure)
    {
        Write-Host "0"
        return $_
    }
    else
    {
        Write-Host "2"
        return $_
    }
}
catch
{
    Write-Host "3"
    return $_
}
'''

def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server')
    results = parser.parse_args(args)
    return results.hostname, results.username, results.password

def get_output(out_put):
    return out_put.split('\n')[0]

def get_status(out_put_google, out_put_philips):
    """Pass the appropriate output as per threshold"""
    msg = 'OK: Internet access in not available in the Node'
    state = 0
    if out_put_google == '2' or out_put_philips == '2':
        msg = 'CRITICAL: Internet access is available in the Node'
        state = 2
    elif out_put_google == '3' or out_put_philips == '3':
        msg = 'UNKNOWN: Please check powershell script'
        state = 3
    return state, msg


def check_internet(hostname, username, password):
    """Get the internet access details"""
    try:
        win_rm = WinRM(hostname, username, password)
        out_put_google = win_rm.execute_ps_script(QUERY_FOR_GOOGLE)
        out_put_philips = win_rm.execute_ps_script(QUERY_FOR_PHILIPS)
        state, msg = get_status(get_output(out_put_google.std_out), get_output(out_put_philips.std_out))
    except RequestException as e:
        state, msg = 3, 'UNKNOWN : Authentication Error - {0}'.format(e)
    except AuthenticationError as e:
        state, msg = 3, 'UNKNOWN : Authentication Error - {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        state, msg = 2, 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            state, msg = 2, 'CRITICAL : Issue in connecting to node - {0}'.format(hostname)
        else:
            state, msg = 2, 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(hostname)
    except Exception as e:
        state, msg = 2,  'CRITICAL : Exception {0}'.format(e)
    return state, msg


def main():
    state, output = check_internet(*check_arg())
    print(output, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()
