#!/usr/bin/env python
######################################################################
# Shinken Plugin - Veeam Backup & Replication License monitoring.
#
# The aim of this plugin is to monitor the Veeam Back & Replication
# license.
# Plugin uses WinRM, and executes Veeam cmdlets to obtain the license
# expiration date and subtracts against actual system date, and provides
# license remaining days
# Plugin exit with OK or CRITICAL status.
# CRITICAL Status will be triggered
#   1) When the critical threshold value is met and lower.
#   2) When error in the WinRM response.
# Example
# python check_veeam_license_info.py -H 192.168.xx.xx -U 'username' -P 'pass' -C '30' #days critical threshold value
######################################################################

from __future__ import print_function
import sys
import argparse
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)
from requests.exceptions import RequestException
from scanline.utilities.win_rm import WinRM

# This is for service graph in the thruk portal
GRAPHER = '| service status={0};1;2;0;2'

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


QUERY_VEEAM_LIC_INFO = '''
Add-PSSnapin VeeamPSSnapin
$ExpirationDate = Get-VBRInstalledLicense | select -ExpandProperty ExpirationDate
if($ExpirationDate){
    $License_info = $ExpirationDate - (get-date)
}
else{
Throw "Couldn't get the License ExpirationDate"
}
Write-host $License_info.days
'''

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Monitor the Veeam Back & Replication license')
    parser.add_argument('-H', '--host', required=True, help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True, help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-C', '--critical_val', required=True, type=int, help='Threshold for the critical state (in days)')
    results = parser.parse_args(args)
    return results.host, results.username, results.password, results.critical_val

def process_output(out_put, critical_val):
    status, msg = CRITICAL, ''
    out_put = out_put.strip()
    if "couldn't get the license expirationdate" in out_put.lower():
        msg = 'CRITICAL : Veeam Backup and Replication license expiration date in not available' \
              + GRAPHER.format(CRITICAL)
    elif out_put.isdigit():
        days = int(out_put)
        if days <= critical_val:
            msg = 'CRITICAL : {0} days remaining for Veeam Backup and Replication license to expire'.format(out_put)\
                  + GRAPHER.format(CRITICAL)
        elif days > critical_val:
            status, msg = OK, 'OK: {0} days remaining for Veeam Backup and Replication license to expire'.format(out_put) \
                          + GRAPHER.format(OK)
    else:
        msg = 'CRITICAL : {0} days before Veeam Backup and Replication license expired or unknown response of ' \
              'expiration date'.format(out_put) + GRAPHER.format(CRITICAL)
    return status, msg

def main(host, username, password, critical_val):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        out_put = win_rm.execute_ps_script(QUERY_VEEAM_LIC_INFO)
        if out_put.std_out:
            status, msg = process_output(out_put.std_out, critical_val)
        else:
            msg = 'CRITICAL : {0}'.format(out_put.std_err) + GRAPHER.format(CRITICAL)
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : Authentication Error - {0}'.format(e) + GRAPHER.format(UNKNOWN)
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e) + GRAPHER.format(CRITICAL)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e) + GRAPHER.format(CRITICAL)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(host) + GRAPHER.format(CRITICAL)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(host) + GRAPHER.format(CRITICAL)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e) + GRAPHER.format(CRITICAL)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_arg())

