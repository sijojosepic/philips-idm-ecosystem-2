#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

from __future__ import print_function
import sys
import argparse
import pymssql
import datetime

'''log_file_format = (datetime.datetime(2018, 10, 20, 2, 49, 11, 90000), u'spid22s', 
                   u'The availability group database "StentorClinical" is changing roles from "RESOLVING" to "PRIMARY"'
                    because the mirroring session or availability group failed over due to role synchronization. This
                    is an informational message only. No user action is required)'''

QUERY_FAILOVER = """DECLARE @dbdate datetime;
                    SET @dbdate = CURRENT_TIMESTAMP-{timediff};
                    EXEC master.dbo.xp_readerrorlog {log_file},1, N'"StentorClinical" is changing roles from "RESOLVING" to ', 
                    NULL, @dbdate, NULL, N'desc'
                    SELECT 'FLAG'"""

FAILOVER_COUNT = []


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-T', '--timediff', required=True, help='time in hours', default=6)
    parser.add_argument('-F', '--failovercount', required=True, help='number of failover in timediff', default=2)
    parser.add_argument('-L', '--logdiff', required=True, help='time difference between two logs in seconds', default=5)
    results = parser.parse_args(args)

    return (
        results.hostname, results.username, results.password, results.database, results.timediff, results.failovercount,
        results.logdiff)


def validate_query(cursor, timediff):
    global FAILOVER_COUNT
    time_diff = float(timediff) / 24
    try:
        for log_file_count in range(4):
            cursor.execute(QUERY_FAILOVER.format(log_file=log_file_count, timediff=time_diff))
            freq_failover = cursor.fetchall()
            if freq_failover and freq_failover[0][0] != 'FLAG':
                FAILOVER_COUNT = FAILOVER_COUNT + freq_failover
    except Exception as e:
        pass


def get_dbnodes(hostname, username, password, database):
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database,
                               login_timeout=20)
        cursor = conn.cursor()
        cursor.execute('select replica_server_name from sys.availability_replicas')
        node_names = cursor.fetchall()
        domain = '.' + '.'.join(hostname.split('.')[1:])
        node_names = [i + domain for sub in node_names for i in sub]
        return node_names
    except Exception as e:
        return False


def get_failover_count(logdiff):
    """ This method will return count of failover.
        It will remove the duplicate failover and those failover where time
        difference is less than and equal to logdiff """
    failovercount = set()
    count = 0
    for fail in FAILOVER_COUNT:
        failovercount.add(fail[0])
    failovercount = sorted(list(failovercount), reverse=True)
    count = len(failovercount)
    for duplicate in range(len(failovercount)):
        search = failovercount[duplicate]
        for delta_check in range(duplicate + 1, len(failovercount)):
            if search - failovercount[delta_check] <= datetime.timedelta(seconds=logdiff):
                count = count - 1
    return count


def get_failover_status(hostname, username, password, database, timediff, failovercount, logdiff):
    node_list = get_dbnodes(hostname, username, password, database)
    if node_list:
        node_list.append(hostname)
    else:
        node_list = [hostname]
    status, outmsg = 0, ''
    error_connection = []
    for node in node_list:
        try:
            conn = pymssql.connect(server=node, user=username, password=password, database=database,
                                   login_timeout=10)
        except Exception as e:
            error_connection.append(node)
            continue
        cursor = conn.cursor()
        validate_query(cursor, timediff)

    count_fl = get_failover_count(int(logdiff))
    failovercount = int(failovercount)
    if count_fl <= (failovercount - 1) and count_fl != 0:
        status, outmsg = 1, 'WARNING - Database failover occurred {failover_count} times in last {timediff} hours.'.format(
            failover_count=count_fl, timediff=timediff)
    elif count_fl >= failovercount and count_fl != 0:
        status, outmsg = 2, 'CRITICAL - Database failover occurred {failover_count} times in last {timediff} hours. ' \
                            'This could cause database unavailability.' \
            .format(failover_count=count_fl, timediff=timediff)
    elif len(error_connection) == len(node_list):
        status, outmsg = 2, 'CRITICAL: Cannot connect to database due to an incorrect username/password, ' \
                            'or service check could not resolve db nodes.'
    else:
        status, outmsg = 0, 'OK - There is no database failover occurred in last {timediff} hours.'.format(
            timediff=timediff)

    return status, outmsg


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_failover_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)



if __name__ == '__main__':
    main()
