#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

from __future__ import print_function
import sys
import argparse
import pymssql
from scanline.utilities import dbutils

QUERY_NODE_STATE = """SELECT replica_server_name,role_desc,operational_state_desc, connected_state_desc 
                    FROM sys.dm_hadr_availability_replica_states AS drs
                    INNER JOIN sys.availability_replicas AS ar 
                    ON drs.replica_id = ar.replica_id;"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.vigilant_url)


def get_formatted_data(results, error_connection):
    """ This method returns formatted message with service status for DB availability."""
    status, outmsg = 2, ''
    disconnected_nodes = []
    if results:
        result_count = list(map(lambda x: len(x), results))
        node_results = results[result_count.index(max(result_count))]
        for node_result in node_results:
            if 'PRIMARY' in node_result and 'ONLINE' in node_result and 'CONNECTED' in node_result:
                status, outmsg = 0, "OK: All the databases are available. Primary up on {0}.".format(node_result[0])
            elif 'RESOLVING' in node_result and not outmsg:
                status, outmsg = 2, "CRITICAL: Database with resolving state detected, " \
                                    "this could cause potentially database unavailability."
            elif 'SECONDARY' in node_result and 'DISCONNECTED' in node_result:
                disconnected_nodes.append(node_result[0])

    if disconnected_nodes and status == 0:
        status, outmsg = 1, 'WARNING: Database disconnection identified on {0}.'.format(
            ', '.join(i for i in disconnected_nodes)) + outmsg[36:]
    if error_connection and not outmsg:
        status, outmsg = 2, 'CRITICAL: Cannot connect to database due to an incorrect username/password, ' \
                            'or service check could not resolve db nodes.'
        return status, outmsg
    if not outmsg:
        status, outmsg = 2, "CRITICAL: Database Replica unexpected dropdown or not available or disconnected."
    return status, outmsg


def get_availability_status(hostname, username, password, database, vigilant_url):
    """ This method returns the availability status for SQL Always On."""
    nodes = filter(lambda x: hostname.split('.')[1] in x[0], dbutils.get_dbnodes(vigilant_url))
    if nodes:
        nodes = nodes[0]
    else:
        nodes = [hostname]
    error_connection = []
    results = []
    for node in nodes:
        try:
            conn = pymssql.connect(server=node, user=username, password=password, database=database, login_timeout=10)
        except Exception as e:
            error_connection.append(node)
            continue
        cursor = conn.cursor()
        cursor.execute(QUERY_NODE_STATE)
        results.append(cursor.fetchall())
    return get_formatted_data(results, error_connection)


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_availability_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()
