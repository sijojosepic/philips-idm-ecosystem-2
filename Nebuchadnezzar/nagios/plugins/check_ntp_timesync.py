#!/usr/bin/env python
from __future__ import absolute_import,print_function
import requests
import subprocess
import sys
from datetime import datetime

OK = 0
CRITICAL = 2

requests.packages.urllib3.disable_warnings()

def get_lb_time():
    # Api url to fetch date string from server
    api_url = 'https://vigilant/health'
    try:
        res = requests.get(api_url, verify=False)
    except Exception as e:
        return CRITICAL, e.message
    headers = res.headers
    currentdate = headers['Date']
    dateformat = "%a, %d %b %Y %H:%M:%S"
    dt = datetime.strptime(currentdate[:-4], dateformat)
    return datetime.strftime(dt, dateformat)


def sync_time(res):
    subprocess.Popen(["sudo", "date", "--set", res])
    subprocess.Popen(["sudo", "hwclock", "--set", "--date", res, "--utc"])

def main():
    currenttime = datetime.now()
    frmtd_dt = currenttime.strftime("%a, %d %b %Y %H:%M:%S")
    res = get_lb_time()
    state, msg = OK, 'Successfully Updated Time from {from_time} to {to_time}'.format(from_time=frmtd_dt, to_time=res)
    if res and not isinstance(res, tuple):
        try:
            sync_time(res)
        except Exception as  e:
            state, msg = CRITICAL, 'Updating time failed due to {msg}'.format(msg=e.message)
    else:
        state, msg = OK, 'Successfully Updated Time'
    print(msg, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()