#!/usr/bin/env python

"""
This plugin is used to monitor All SQL Backup Jobs.
example
./check_sql_jobs_status.py  -H 1.1.1.1 -U 'db1.IDM04' -P 'password' -D mydb'
CRITICAL - There are 3/5 SQL jobs in Failed state.
Job:AdminDBDiffBackup Last execution on date/time:2019-07-29 00:00:00 state:Failed
Job:AdminDBDBCCCheck Last execution on date/time:2019-07-28 03:00:00 state:Failed
Job:AdminDBFullBackup Last execution on date/time:2019-07-28 00:00:00 state:Failed
Job:AdminDBTrnBackup Last execution on date/time:2019-07-29 06:15:00 state:Succeeded
Job:MSSQLServerLogsCollect Last execution on date/time:2019-07-29 03:00:00 state:Succeeded

"""

from __future__ import print_function
import sys
import argparse
import pymssql
from scanline.utilities import dbutils
from datetime import datetime

OK = 0
WARNING = 1
CRITICAL = 2
EOL = '\n'
ok_msg = 'OK - All the SQL jobs {success_count}/{job_count} are Success'
critical_msg = 'CRITICAL - There are {failed_count}/{job_count} SQL jobs in Failed state'
warning_msg = 'WARNING - There are {canceled_count}/{job_count} SQL jobs in Canceled state'
job_template = 'Job:{job_name} Last execution on date/time:{timestamp} state:{status}'
cancelled_msg = ' and {canceled_count}/{job_count} SQL jobs in Canceled state'
success_jobs = []
failed_jobs = []
canceled_jobs = []

QUERY = """SELECT [sJOB].[name] AS [JobName], CASE WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
        ELSE CAST(CAST([sJOBH].[run_date] AS CHAR(8))+ ' '+ STUFF(STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS
        VARCHAR(6)),  6), 3, 0, ':'), 6, 0, ':')AS DATETIME) END AS [LastRunDateTime], CASE [sJOBH].[run_status]
        WHEN 0 THEN 'Failed' WHEN 1 THEN 'Succeeded' WHEN 2 THEN 'Retry' WHEN 3 THEN 'Canceled'
        WHEN 4 THEN 'Running' WHEN NULL THEN 'NotStarted' END AS [LastRunStatus]
        FROM [msdb].[dbo].[sysjobs] AS [sJOB] LEFT JOIN ( SELECT [job_id], MIN([next_run_date]) AS [NextRunDate],
        MIN([next_run_time]) AS [NextRunTime] FROM [msdb].[dbo].[sysjobschedules] GROUP BY [job_id]) AS [sJOBSCH]
        ON [sJOB].[job_id] = [sJOBSCH].[job_id]
        LEFT JOIN (SELECT [job_id], [run_date], [run_time], [run_status], [run_duration], [message], ROW_NUMBER() OVER (
        PARTITION BY [job_id] ORDER BY [run_date] DESC, [run_time] DESC) AS RowNumber FROM [msdb].[dbo].[sysjobhistory]
        WHERE [step_id] = 0) AS [sJOBH] ON [sJOB].[job_id] = [sJOBH].[job_id] AND [sJOBH].[RowNumber] = 1
        ORDER BY [LastRunDateTime]	DESC"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username to DB login')
    parser.add_argument('-P', '--password', required=True, help='Password to DB login')
    parser.add_argument('-D', '--database', required=True, help='Database to be used')
    parser.add_argument('-E', '--env', required=False, default='mirroring', help='DB Environment Type')
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.env, results.vigilant_url)


def get_message(failed_msg, cancelled_msg, success_msg):
    msg_str = ''
    for job_msg in failed_msg:
        msg_str = msg_str + job_msg + EOL
    for job_msg in cancelled_msg:
        msg_str = msg_str + job_msg + EOL
    for job_msg in success_msg:
        msg_str = msg_str + job_msg + EOL
    return msg_str


def get_job_status(hostname, username, password, database, env, vigilant_url):
    if env != 'cluster':
        dbnodes = filter(lambda x: hostname in x, dbutils.get_dbnodes(vigilant_url))
        if dbnodes:
            dbnodes = dbnodes[0]
        else:
            dbnodes = dbutils.get_dbpartner_in_dbscanner(hostname, vigilant_url)

        if len(dbnodes) > 1:
            if not dbutils.get_mirroring_primary_check(hostname=hostname, username=username, password=password,
                                                       database=database):
                status, outmsg = OK, 'OK: This service check only applicable for primary node. {node} is not the primary node.'.format(
                    node=hostname)
                return status, outmsg
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
    except Exception as e:
        status, outmsg = CRITICAL, 'CRITICAL - Connection error with {node}. '.format(node=hostname)
        return status, outmsg

    cursor = conn.cursor()
    query = QUERY
    cursor.execute(query)
    result = cursor.fetchall()
    number_of_jobs = 0
    if result:
        for jobname, timestamp, status in result:
            if status != None:
                number_of_jobs += 1
            if status == 'Succeeded':
                success_jobs.append(job_template.format(job_name=jobname, timestamp= datetime.strftime(timestamp,
                                                                '%Y-%m-%d %H:%M:%S'), status=status))
            if status == 'Failed':
                failed_jobs.append(job_template.format(job_name=jobname, timestamp=datetime.strftime(timestamp,
                                                                '%Y-%m-%d %H:%M:%S'), status=status))
            if status == 'Canceled':
                canceled_jobs.append(job_template.format(job_name=jobname, timestamp=datetime.strftime(timestamp,
                                                                '%Y-%m-%d %H:%M:%S'), status=status))

    msg_str = get_message(failed_jobs, canceled_jobs, success_jobs)
    if len(failed_jobs) > 0:
        if (len(canceled_jobs) > 0):
            outmsg = critical_msg.format(failed_count=len(failed_jobs), job_count=number_of_jobs) + \
                     cancelled_msg.format(canceled_count= len(canceled_jobs), job_count=number_of_jobs) + EOL + msg_str
        else:
            outmsg = critical_msg.format(failed_count= len(failed_jobs), job_count= number_of_jobs) + EOL + msg_str
        return CRITICAL, outmsg
    elif len(canceled_jobs) > 0:
        outmsg = warning_msg.format(canceled_count= len(canceled_jobs), job_count = number_of_jobs) + EOL + msg_str
        return WARNING, outmsg
    else:
        outmsg = ok_msg.format(success_count= len(success_jobs), job_count= number_of_jobs) + EOL + msg_str
        return OK, outmsg


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_job_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()