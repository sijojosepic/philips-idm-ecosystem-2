#!/usr/bin/env python

"""
This plugin is used to monitor the rhapsody route status in the ibe app server.
workflow
  Executes the api against ibe app server
     sample api: https://localhost:8544/api/components
  The response from the server is all routes and its corresponding status example ERROR,RUNNING,STOPPED etc
    sample response:{
            'id': 1234,
            'type': 'ROUTE',
            'childComponents': {
                "id": 218,
                "name": "HL7IHERoute",
                "state": "RUNNING",
                "type": "ROUTE"
            },
            'child': {}
        }
  As per the configuration the plugin will classify the output. If there is any critical route the status should be crtitical.
  if there there is no critical route only warning route exists the output should be in warning state.If there is no crtical, warning routes
  and routes with OK status exists then only the output reports a OK status

example:
     python check_rhapsody_routes_status.py -H 'idm04hl7.idm04.isyntax.net' -P 8544 -u administrator -p '1nf0M@t1csPh!lt0r'
     -c '["STOPPED","ERROR","DELETED"]' -w '["STARTING","STOPPING"]' -o '["RUNNING"]'

    CRITICAL - 7 route status is unhealthy. STOPPED; Count 7; IDs [204, 192, 188, 208, 225, 231, 227].
    OK - 6 route status is healthy. RUNNING; Count 6; IDs [196, 212, 201, 218, 215, 221].


The service will alert different status  based on the routes status
     CRITICAL - Stopped, Deleted, Error
     WARNING - Starting, Stopping
     OK - Running
"""

import argparse
import ast
import requests
import sys

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get Rhapsody route status')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The name of the Rhapsody server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the Rhapsody server')
    parser.add_argument('-u', '--rhapsodyusername', required=True,
                        help='The username of the Rhapsody server')
    parser.add_argument('-p', '--rhapsodypassword', required=True,
                        help='The password of the Rahpsody server')
    parser.add_argument('-pr', '--protocol', required=False, default='https',
                        help='The protocol for rhapsody server')
    parser.add_argument('-c', '--critical', required=True,
                        help='Configuration for critical status')
    parser.add_argument('-w', '--warning', required=True,
                        help='Configuration for warning status')
    parser.add_argument('-o', '--ok', required=True,
                        help='Configuration for ok status')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.port,
        results.rhapsodyusername,
        results.rhapsodypassword,
        results.protocol,
        results.critical,
        results.warning,
        results.ok)


def json_extractor(data):
    """ This method is used to extract the route status from the json data which get as
    rhapsody route api response.
    """
    if 'id' and 'state' in data and data['type'].strip() == 'ROUTE':
        yield data
    for k_value in data:
        if isinstance(data[k_value], list):
            for row_value in data[k_value]:
                for l_value in json_extractor(row_value):
                    yield l_value
        if isinstance(data[k_value], dict):
            for d_value in json_extractor(data[k_value]):
                yield d_value


def check_route_status(hostname, port, username, password, protocol):
    """ This method is used to execute the rhapsody route status api against the ibe app server."""
    headers = {
        'Accept': 'application/json',
        'Accept': 'application/vnd.orchestral.rhapsody.6_1+json',
    }
    auth = (username, password)
    url = '{0}://{1}:{2}/api/components'.format(protocol, hostname, port)
    response = requests.get(url, headers=headers, verify=False,
                            auth=auth, timeout=30)
    return response


def aggregate_status(route_data):
    """ This method used to construct the route status count."""
    status_dict = {}
    for item in route_data:
        try:
            status_dict[item['state']]['count'] += 1
            status_dict[item['state']]['id'].append(item['id'])
        except KeyError:
            status_dict[item['state']] = {'count': 1, 'id': [item['id']]}
    return status_dict


def unknown_message(critical, warning, ok):
    critical_msg = 'CRITICAL - {0}'.format(critical)
    warning_msg = 'WARNING : {0}'.format(warning)
    ok_msg = 'OK: {0}'.format(ok)
    unknown_msg = 'No Route data found with given configuration {0}, {1},{2}'.format(critical_msg, warning_msg, ok_msg)
    return unknown_msg


def compose_status_message(agg_statuses, critical, warning, ok):
    status = UNKNOWN
    crit_msg, warn_msg, ok_msg = '', '', ''
    msg_template = '{0}; Count {1}; IDs {2}. '
    status_msg = '{0} - {1} route status is {2}. {3}\n'
    critical_cnt, war_cnt, ok_cnt = 0, 0, 0
    for item, st in agg_statuses.iteritems():
        if item in critical:
            crit_msg += msg_template.format(item, st['count'], st['id'])
            critical_cnt += st['count']
        elif item in warning:
            warn_msg += msg_template.format(item, st['count'], st['id'])
            war_cnt += st['count']
        elif item in ok:
            ok_msg += msg_template.format(item, st['count'], st['id'])
            ok_cnt += st['count']
    if ok_cnt:
        status = OK
        ok_msg = status_msg.format('OK', ok_cnt, 'healthy', ok_msg)

    if war_cnt:
        status = WARNING
        warn_msg = status_msg.format('WARNING', war_cnt, 'unhealthy', warn_msg)

    if critical_cnt:
        status = CRITICAL
        crit_msg = status_msg.format('CRITICAL', critical_cnt, 'unhealthy', crit_msg)
    msg = crit_msg + warn_msg + ok_msg or unknown_message(critical, warning, ok)
    return status, msg


def process_data(data, critical, warning, ok):
    """ The rhapsody route status api response is processing in this method. """
    route_data = json_extractor(data)
    agg_statuses = aggregate_status(route_data)
    return compose_status_message(agg_statuses, critical, warning, ok)


def str_to_list(value):  # '["STOPPED","ERROR","DELETED"]' => ["STOPPED","ERROR","DELETED"]
    return ast.literal_eval(value)


def main():
    status, msg = CRITICAL, ''
    hostname, port, username, password, protocol, critical, warning, ok = check_arg()
    try:
        response = check_route_status(
            hostname, port, username, password, protocol)
        if response.status_code == 200:
            status, msg = process_data(response.json(), str_to_list(critical), str_to_list(warning), str_to_list(ok))
        elif response.status_code == 401:
            status, msg = CRITICAL, 'CRITICAL: Rhapsody Username or Password is not valid'
        elif response.status_code < 200 or response.status_code > 400:
            status, msg = CRITICAL, 'CRITICAL: Rhapsody server might receive too many requests or is not accessible'
    except requests.exceptions.RequestException as e:
        status, msg = 2, 'CRITICAL: Rhapsody is not accessible/down'
    except Exception as e:
        status, msg = 2, "Exception:{0}".format(str(e))
    print msg
    sys.exit(status)


if __name__ == '__main__':
    main()
