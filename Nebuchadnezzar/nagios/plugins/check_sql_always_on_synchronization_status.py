#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

from __future__ import print_function
import sys
import argparse
import pymssql
from scanline.utilities import dbutils

QUERY_SYNC_HEALTH = """SELECT adc.database_name 
                    FROM sys.dm_hadr_database_replica_states AS drs
                    INNER JOIN sys.availability_databases_cluster AS adc 
                    ON drs.group_id = adc.group_id AND 
                    drs.group_database_id = adc.group_database_id
                    INNER JOIN sys.availability_groups AS ag
                    ON ag.group_id = drs.group_id
                    INNER JOIN sys.availability_replicas AS ar 
                    ON drs.group_id = ar.group_id AND 
                    drs.replica_id = ar.replica_id
                    where synchronization_health=0;"""

QUERY_SYNC_BACKLOG = """SELECT ar.replica_server_name, 
                     adc.database_name, ag.name AS ag_name, drs.is_local, 
                     --drs.is_primary_replica, drs.synchronization_state_desc, drs.is_commit_participant, 
                     drs.synchronization_health_desc,  drs.log_send_queue_size 
                     FROM sys.dm_hadr_database_replica_states AS drs
                     INNER JOIN sys.availability_databases_cluster AS adc 
                     ON drs.group_id = adc.group_id AND drs.group_database_id = adc.group_database_id
                     INNER JOIN sys.availability_groups AS ag
                     ON ag.group_id = drs.group_id
                     INNER JOIN sys.availability_replicas AS ar 
                     ON drs.group_id = ar.group_id AND  drs.replica_id = ar.replica_id
                     where log_send_queue_size >{counterval}
                     ORDER BY 
                     ag.name, 
                     ar.replica_server_name, 
                     adc.database_name;"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-C', '--counterval', required=False, help='', default=102400)
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.counterval, results.vigilant_url)


def get_connection(hostname, username, password, database):
    """ This method returns the connection object."""
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
        return conn
    except Exception as e:
        return False


def get_formatted_data(sync_health, sync_backlog, counterval):
    """ This method returns formatted message with service status for synchronization."""
    if sync_health:
        status, outmsg = 2, 'CRITICAL: Problem in database synchronization. Unhealthy databases are {0}.'.format(
            ', '.join(i[0].encode('utf-8') for i in set(sync_health)))
        return status, outmsg
    if sync_backlog:
        status, outmsg = 2, 'Detected slow data synchronization between db nodes. ' \
                            'Current backlog {current_val}MB has reached threshold of {counterval}MB. ' \
                            'This could cause to data loss.'.format(counterval=int(counterval) / 1024,
                                                                    current_val=(sync_backlog[-1]) / 1024)
        return status, outmsg

    status, outmsg = 0, 'OK: Data synchronization is healthy.'
    return status, outmsg


def get_synchronization_status(hostname, username, password, database, counterval, vigilant_url):
    """ This method returns the synchronization status for SQL Always On."""
    status, outmsg = 0, ''
    conn = get_connection(hostname, username, password, database)
    if not conn:
        node = dbutils.get_cluster_primary_db(hostname, username, password, database, vigilant_url)
        conn = get_connection(node, username, password, database)
    if not conn:
        status, outmsg = 2, 'CRITICAL: Cannot connect to database due to an incorrect username/password, ' \
                            'or service check could not resolve db nodes.'
        return status, outmsg
    cursor = conn.cursor()
    cursor.execute(QUERY_SYNC_HEALTH)
    sync_health = cursor.fetchall()
    cursor.execute(QUERY_SYNC_BACKLOG.format(counterval=counterval))
    sync_backlog = cursor.fetchone()
    return get_formatted_data(sync_health, sync_backlog, counterval)


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_synchronization_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()
