#!/usr/bin/env python
"""
This plugin is used to monitor the msssql database backup status in the various DB nodes.
workflow

  Execute a powershell script against db node to get the SqlErrorlog and collects the db status of Sql backups (FULL , DIFFERENTIAL, LOG) in ASCENDING ORDER.
  As per the powershell query response the plugin will classify the output. If any DBs latest status is FAILED the service status should be CRITICAL.
  If the latest status of the DB is SUCCESSFUL and any previous status is FAILED, then the service status should be WARNING.
  and the status of all DBs are SUCCESSFUL then the service status should be OK.

example:
    python  check_log_sqlbackup_status.py -H 10.xxx.xxx.146 -U 'dummyuser' -P  'pass'

      CRITICAL Backups :- Backup FULL Database Xcelera failed On 10/23/2019 5:27:12 PM.

      WARNING Backups :- Backup FULL Database Echo failed On 10/23/2019 1:09:45 PM. Backup FULL Database Echo successful On 10/23/2019 5:26:35 PM.

      OK Backups :- Backup LOG Database Xcelera successful On 10/24/2019 10:30:01 AM. Backup LOG Database model successful On 10/24/2019 10:30:00 AM. Backup LOG Database Echo successful On 10/24/2019 10:30:00 AM. Backup DIFFERENTIAL Database Xcelera successful On 10/24/2019 12:15:07 AM. Backup DIFFERENTIAL Database msdb successful On 10/24/2019 12:15:05 AM. Backup DIFFERENTIAL Database model successful On 10/24/2019 12:15:03 AM. Backup DIFFERENTIAL Database Echo successful On 10/24/2019 12:15:02 AM. Backup FULL Database master successful On 10/23/2019 12:58:35 PM. Backup FULL Database msdb successful On 10/19/2019 12:00:12 AM. Backup FULL Database model successful On 10/19/2019 12:00:10 AM

Sample log:

116/09/2019 15:19:24 AM BACKUP DATABASE successfully processed 24195 pages in 0.823 seconds (229.671 MB/sec).
16/09/2019 15:19:24 AM Database backed up. Database: Advntures2012<c/> creation date(time): 2019/10/09(13:02:54)<c/> pages dumped: 24203<c/> first LSN: 42:248:37<c/> last LSN: 42:288:1<c/> number of dump devices: 1<c/> device information: (FILE=1<c/> TYPE=DISK: {'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\Advntures2012.bak'}). This is an informational message only. No user action is required.

10/09/2019 15:35:46 AM BACKUP failed to complete the command BACKUP DATABASE Advntures2012. Check the backup application log for detailed messages.

10/09/2019 16:02:29 AM Log was backed up. Database: model<c/> creation date(time): 2003/04/08(09:13:36)<c/> first LSN: 31:432:37<c/> last LSN: 32:32:1<c/> number of dump devices: 1<c/> device information: (FILE=3<c/> TYPE=DISK: {'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\Backup\model.bak'}). This is an informational message only. No user action is required.

"""
import argparse
import re
import sys
from dateutil import parser
from functools import partial
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException
from scanline.utilities.win_rm import WinRM

PS_SCRIPT = """$sqlinstanceobj = (Get-itemproperty "HKLM:\SOFTWARE\Microsoft\Microsoft SQL Server").installedInstances; $hostname = hostname; $sqlinstance = "SQLSERVER:\SQL"+"\$hostname\"+"\$sqlinstanceobj"; Import-Module SQLPS; $filterdate = (Get-Date).AddDays(-15); $serverobj = ((Get-Item $sqlinstance).ReadErrorLog()|?{$_.text -match 'failed' -OR $_.text -match 'database' -AND $_.processinfo -eq 'backup' -AND $_.logdate -ge $filterdate} |sort logdate | select logdate, text | out-string -Width 500).Trim(); Write-output $serverobj """

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to monitor msssql database backup status')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of the db node')
    parser.add_argument('-U', '--username', required=True,
                        help='Username of the node')
    parser.add_argument('-P', '--password', required=True,
                        help='Password of the node')
    results = parser.parse_args(args)
    return (results.hostname, results.username, results.password)


def get_logdate_and_db(date_pattern, data_pattern, log_line):
    try:
        log_date = date_pattern.match(log_line).group()
        db_name = data_pattern.search(log_line).group().rstrip(
            "<c/>").rstrip(",").split()[-1].rstrip(".")
        return log_date, db_name
    except (AttributeError, IndexError):
        return None, None


class DBLogParser(object):
    MAIN_BACKUP_SUCCESS = "Database backed up"
    DIFFERENTIAL_BACKUP_SUCCESS = "Database differential changes were backed up"
    LOG_BACKUP_SUCCESS = "Log was backed up"
    BACKUP_FAILURE = "BACKUP failed to complete the command"
    LOG, DIFFERENTIAL, MAIN = "LOG", "DIFFERENTIAL", "FULL"
    LOG_DATE_PATTERN = re.compile(r'^\d{1,2}/\d{1,2}/\d{4}.+(PM|AM)')
    LOG_FAILURE_PATTERN = re.compile(r'BACKUP\s(DATABASE|LOG)\s(?!WITH)\S+')
    LOG_SUCCESS_PATTERN = re.compile(r'\sDatabase:\s\S+')

    def __init__(self, log):
        '''
        :param log: is the output of Power Shell query in ascending order.
        as its been in ascending order, when the update method updates the job_status.
        at the end job_status contains the most recent item.
        '''
        self.log = log
        self.job_status = {}
        self._retrieve = partial(get_logdate_and_db, self.LOG_DATE_PATTERN)
        self._dc = parser.parse

    def update(self, db, backup_type, status, date):
        if self.job_status.get(db):
            if self.job_status[db].get(backup_type):
                self.job_status[db][backup_type][status] = date
            else:
                self.job_status[db].update({backup_type: {status: date}})
        else:
            self.job_status.update({db: {backup_type: {status: date}}})

    def check_failures(self, line):
        bk_type, status = self.MAIN, 'FAILED'
        if self.BACKUP_FAILURE in line:
            date, db = self._retrieve(self.LOG_FAILURE_PATTERN, line)
            if self.LOG in line:
                bk_type = 'TRANSACTIONAL_LOG'
            if self.DIFFERENTIAL in line:
                bk_type = self.DIFFERENTIAL
            self.update(db, bk_type, status, date)
            return True

    def check_success(self, line):
        bk_type, status = '', 'SUCCESS'
        if self.LOG_BACKUP_SUCCESS in line:
            bk_type = 'TRANSACTIONAL_LOG'
        elif self.DIFFERENTIAL_BACKUP_SUCCESS in line:
            bk_type = self.DIFFERENTIAL
        elif self.MAIN_BACKUP_SUCCESS in line:
            bk_type = self.MAIN
        if bk_type:
            date, db = self._retrieve(self.LOG_SUCCESS_PATTERN, line)
            self.update(db, bk_type, status, date)

    def _job_status(self, fail_date, success_date):
        job_status = 'OK'
        if fail_date:
            if success_date and (self._dc(fail_date) < self._dc(success_date)):
                job_status = 'WARNING'
            else:
                job_status = 'CRITICAL'
        return job_status

    def process_log(self):
        for line in self.log.splitlines():
            # Either success or Failure can be there in the line
            if not self.check_failures(line):
                self.check_success(line)

    def compose_status(self):
        statuses = {'CRITICAL': [], 'WARNING': [], 'OK': []}
        for db in self.job_status:
            for job in self.job_status[db]:
                failure_date = self.job_status[db][job].get('FAILED', '')
                success_date = self.job_status[db][job].get('SUCCESS', '')
                job_status = self._job_status(failure_date, success_date)
                statuses[job_status].append((db, job, failure_date, success_date))
        return statuses

    def status(self):
        self.process_log()
        return self.compose_status()


def add_messge(template, prefix, items):
    msg = prefix
    for item in items:
        msg = msg + template.format(*item)
    return msg


def format_msg(status):
    final_status, final_msg = OK, ''
    crt_prfix, warn_prfx, ok_prfx = 'FAILED : ', 'WARNING : ', 'SUCCESSFUL : '
    crt_template = "Database {0} BackupType {1} failed On {2}.\n"
    wrn_template = "Database {0} BackupType {1} failed On {2} successful On {3}.\n"
    ok_template = "Database {0} BackupType {1} is successful. \n"
    crt_items, warn_items, ok_items = status['CRITICAL'], status['WARNING'], status['OK']
    if crt_items:
        final_status = CRITICAL
        final_msg = add_messge(crt_template, crt_prfix, crt_items)
    if warn_items:
        final_status = final_status if final_status == CRITICAL else WARNING
        # final_status = WARNING
        final_msg += add_messge(wrn_template, warn_prfx, warn_items)
    if ok_items:
        final_msg += add_messge(ok_template, ok_prfx, ok_items)
    return final_status, final_msg or 'No backup Data Found'


def main():
    status, msg = CRITICAL, ''
    host, username, password = check_arg()
    try:
        win_rm = WinRM(host, username, password)
        out_put = win_rm.execute_ps_script(PS_SCRIPT)
        if out_put.status_code == 0 and out_put.std_out:
            log_parser = DBLogParser(out_put.std_out)
            bk_status = log_parser.status()
            status, msg = format_msg(bk_status)
        else:
            msg = 'WARNING : Error while retrieving  the information - {0}'
            status, msg = WARNING, msg.format(str(out_put.std_err))
    except AuthenticationError as e:
        msg = 'CRITICAL : Authentication Error - {0}'.format(e)
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(host)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(host)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print msg
    sys.exit(status)


if __name__ == '__main__':
    main()