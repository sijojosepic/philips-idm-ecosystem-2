import unittest
from mock import MagicMock, patch, PropertyMock, call

class RestartWinServiceAndNotifyCheckArgTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_post_notification = MagicMock(name='post_notification')
        self.mock_argparse = MagicMock(name='argparse')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'post_notification': self.mock_post_notification,
            'argparse': self.mock_argparse,
            'phimutils': self.mock_phimutils,
            'phimutils.argument': self.mock_phimutils.argument,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import restart_win_service_and_notify
        self.module = restart_win_service_and_notify

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_check_arg(self):
        self.assertEqual(self.module.check_arg(), self.mock_argparse.ArgumentParser().parse_args())

    def test_main(self):
        self.module.main()

class RestartWinServiceAndNotifyTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_post_notification = MagicMock(name='post_notification')
            self.mock_argparse = MagicMock(name='argparse')
            self.mock_phimutils = MagicMock(name='phimutils')
            modules = {
                'post_notification': self.mock_post_notification,
                'argparse': self.mock_argparse,
                'phimutils': self.mock_phimutils,
                'phimutils.argument': self.mock_phimutils.argument,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import restart_win_service_and_notify
            self.module = restart_win_service_and_notify

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()

class RestartWinServiceAndNotifyHandlerEventTestCase(RestartWinServiceAndNotifyTest.TestCase):
    def setUp(self):
        RestartWinServiceAndNotifyTest.TestCase.setUp(self)
        self.module.FAILURE_MESSAGE = 'TOTAL FAILURE'
        self.module.RESTART_MESSAGE = 'RESTART ISSUED'
        self.module.RECOVERY_MESSAGE = 'RECOVERED'

    def test_soft_critical_in_restart_numbers(self):
        result = self.module.HandlerEvent('service1', 'something happened', 3, [3, 6], 'CRITICAL', 'SOFT', False)
        self.assertTrue(result.restartable)
        self.assertEqual(
            result.messages,
            [{'output': 'service1 RESTART ISSUED - something happened [1/2]', 'state': 'CRITICAL'}]
        )
        self.assertTrue(result.is_restart_event())
        self.assertFalse(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())

    def test_soft_critical_not_in_restart_numbers(self):
        result = self.module.HandlerEvent('service1', 'something happened', 2, [3, 6], 'CRITICAL', 'SOFT', False)
        self.assertFalse(result.restartable)
        self.assertEqual(result.messages, [])
        self.assertFalse(result.is_restart_event())
        self.assertFalse(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())

    def test_hard_critical_hard_not_set(self):
        result = self.module.HandlerEvent('service1', 'something happened', 9, [3, 6], 'CRITICAL', 'HARD', False)
        self.assertFalse(result.restartable)
        self.assertEqual(
            result.messages,
            [{'output': 'service1 TOTAL FAILURE - something happened [2/2]', 'state': 'WARNING'}]
        )
        self.assertFalse(result.is_restart_event())
        self.assertTrue(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())

    def test_hard_critical_hard_set(self):
        result = self.module.HandlerEvent('service1', 'something happened', 9, [3, 6], 'CRITICAL', 'HARD', True)
        self.assertTrue(result.restartable)
        self.assertEqual(
            result.messages,
            [
                {'output': 'service1 TOTAL FAILURE - something happened [2/2]', 'state': 'WARNING'},
                {'output': 'service1 RESTART ISSUED - something happened [2+/2]', 'state': 'CRITICAL'}
            ]
        )
        self.assertFalse(result.is_restart_event())
        self.assertTrue(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())

    def test_hard_ok_hard_set(self):
        result = self.module.HandlerEvent('service1', 'something happened', 9, [3, 6], 'OK', 'HARD', True)
        self.assertFalse(result.restartable)
        self.assertEqual(result.messages, [])
        self.assertFalse(result.is_restart_event())
        self.assertFalse(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())

    def test_soft_ok_no_previous(self):
        result = self.module.HandlerEvent('service1', 'something happened', 2, [3, 6], 'OK', 'SOFT', False)
        self.assertFalse(result.restartable)
        self.assertEqual(result.messages, [])
        self.assertFalse(result.is_restart_event())
        self.assertFalse(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())
        self.assertEqual(result.previous_attempt, 0)

    def test_soft_ok_with_previous(self):
        result = self.module.HandlerEvent('service1', 'something happened', 4, [3, 6], 'OK', 'SOFT', False)
        self.assertFalse(result.restartable)
        self.assertEqual(
            result.messages,
            [{'output': 'service1 RECOVERED - something happened [1/2]', 'state': 'OK'}]
        )
        self.assertFalse(result.is_restart_event())
        self.assertFalse(result.is_failure_event())
        self.assertTrue(result.is_recovery_event())
        self.assertEqual(result.previous_attempt, 1)

    def test_soft_warning(self):
        result = self.module.HandlerEvent('service1', 'something happened', 4, [3, 6], 'WARNING', 'SOFT', False)
        self.assertFalse(result.restartable)
        self.assertEqual(result.messages, [])
        self.assertFalse(result.is_restart_event())
        self.assertFalse(result.is_failure_event())
        self.assertFalse(result.is_recovery_event())


class RestartWinServiceAndNotifyHandlerMessengerTestCase(RestartWinServiceAndNotifyTest.TestCase):
    def setUp(self):
        RestartWinServiceAndNotifyTest.TestCase.setUp(self)
        self.messenger = self.module.HandlerMessenger(
            'http://vigilant/noti',
            'host1',
            '10.4.5.6',
            'service__1__x__y__z',
            'HANDLER',
            '/etc/file1.txt',
            False
        )

    def test_send_notification(self):
        self.module.post_notification.post_notification.return_value = 0, 'posted fine'
        self.assertEqual(self.messenger.send_message('CRITICAL', 'some message here'), 'posted fine')
        self.module.post_notification.post_notification.assert_called_once_with(
            description='service__1__x__y__z',
            hostaddress='10.4.5.6',
            hostname='host1',
            notificationtype='HANDLER',
            output='some message here',
            perfdata=None,
            siteid=None,
            siteid_file='/etc/file1.txt',
            state='CRITICAL',
            timestamp=None,
            url='http://vigilant/noti',
            insecure=False
        )


class RestartWinServiceAndNotifyWindowsServiceOperatorTestCase(RestartWinServiceAndNotifyTest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_functools = MagicMock(name='functools')
        modules = {
            'functools': self.mock_functools,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        RestartWinServiceAndNotifyTest.TestCase.setUp(self)
        self.operator = self.module.WindowsServiceOperator(
            '10.4.5.6',
            'service1',
            'userone@something.com',
            'super_secret',
            6,
            10
        )
        self.module.sleep = MagicMock(name='sleep')
        self.module.subprocess = MagicMock(name='subprocess')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_user_pass(self):
        self.assertEqual(self.operator.user_pass, 'userone%super_secret')

    @patch('restart_win_service_and_notify.WindowsServiceOperator.user_pass', new_callable=PropertyMock)
    def test_get_command(self, mock_user_pass):
        self.assertEqual(
            self.operator.get_command('ACTION'),
            [
                '/usr/bin/net',
                'rpc',
                'service',
                'ACTION',
                'service1',
                '-U',
                mock_user_pass.return_value,
                '-I',
                '10.4.5.6',
                '-W',
                'something.com'
            ]
        )

    @patch('restart_win_service_and_notify.WindowsServiceOperator.user_pass', new_callable=PropertyMock)
    def test_get_command_no_domain(self, mock_user_pass):
        self.operator = self.module.WindowsServiceOperator(
            '10.4.5.6',
            'service1',
            'userone',
            'super_secret',
            6,
            10
        )
        self.assertEqual(
            self.operator.get_command('ACTION'),
            [
                '/usr/bin/net',
                'rpc',
                'service',
                'ACTION',
                'service1',
                '-U',
                mock_user_pass.return_value,
                '-I',
                '10.4.5.6'
            ]
        )

    def test_caller(self):
        self.assertEqual(self.operator.caller(), self.mock_functools.partial())

    def test_restart(self):
        self.operator.restart()

#    def test_perform_operation(self):
#        self.operator.get_command = MagicMock(name='get_command')
#        self.operator.get_command.return_value = ['/usr/bin/net', 'rpc', 'service', 'stop']
#        self.operator.perform_operation('stop')
#        self.module.subprocess.call.assert_called_once_with(self.operator.get_command.return_value, timeout=10)


#    def test_perform_operation_blocked(self):
#        self.operator.get_command = MagicMock(name='get_command')
#        self.operator.get_command.return_value = ['/usr/bin/curl', 'http://badsite']
#        self.operator.perform_operation('stop')
#        self.assertFalse(self.module.subprocess.called)

#    def test_restart(self):
#        self.operator.perform_operation = MagicMock(name='perform_operation')
#        self.operator.restart()
#        self.assertEqual(self.operator.perform_operation.mock_calls, [call('stop'), call('start')])
#        self.module.sleep.assert_called_once_with(6)


class RestartWinServiceAndNotifyWindowsMainTestCase(RestartWinServiceAndNotifyTest.TestCase):
    def setUp(self):
        RestartWinServiceAndNotifyTest.TestCase.setUp(self)
        self.module.HandlerEvent = MagicMock(name='HandlerEvent')
        self.module.HandlerMessenger = MagicMock(name='HandlerMessenger')
        self.module.WindowsServiceOperator = MagicMock(name='WindowsServiceOperator')
        self.arguments = MagicMock()

    def test_no_messages_not_restartable(self):
        self.module.HandlerEvent.return_value.messages = []
        self.module.HandlerEvent.return_value.restartable = False
        self.module.restart_win_service_and_notify(self.arguments)
        self.module.HandlerEvent.assert_called_once_with(
            service=self.arguments.service,
            output=self.arguments.output,
            attempt=self.arguments.attempt,
            attempt_numbers=self.arguments.attempt_numbers,
            state=self.arguments.state,
            statetype=self.arguments.statetype,
            hard=self.arguments.hard
        )
        self.assertFalse(self.module.HandlerMessenger.return_value.send_message.called)
        self.assertFalse(self.module.WindowsServiceOperator.return_value.restart.called)

    def test_messages_and_restartable(self):
        self.module.HandlerEvent.return_value.messages = [
            {'state': 'FAILURE', 'output': 'something'},
            {'state': 'OK', 'output': 'something else'}
        ]
        self.module.HandlerMessenger.return_value.send_message.return_value = 0, 'OK'
        self.module.HandlerEvent.return_value.restartable = True
        self.module.restart_win_service_and_notify(self.arguments)
        self.module.HandlerEvent.assert_called_once_with(
            service=self.arguments.service,
            output=self.arguments.output,
            attempt=self.arguments.attempt,
            attempt_numbers=self.arguments.attempt_numbers,
            state=self.arguments.state,
            statetype=self.arguments.statetype,
            hard=self.arguments.hard
        )
        self.module.HandlerMessenger.assert_called_once_with(
            url=self.arguments.url,
            hostname=self.arguments.hostname,
            hostaddress=self.arguments.hostaddress,
            description=self.arguments.description,
            notificationtype=self.arguments.notificationtype,
            siteid_file=self.arguments.siteid_file,
            insecure=self.arguments.insecure
        )
        self.module.WindowsServiceOperator.assert_called_once_with(
            hostaddress=self.arguments.hostaddress,
            service=self.arguments.service,
            username=self.arguments.username,
            password=self.arguments.password,
            sleep_before_start=self.arguments.sleep,
            timeout=self.arguments.timeout
        )
        self.assertEqual(
            self.module.HandlerMessenger.return_value.send_message.mock_calls,
            [call(output='something', state='FAILURE'), call(output='something else', state='OK')]
        )
        self.assertTrue(self.module.WindowsServiceOperator.return_value.restart.called)


if __name__ == '__main__':
    unittest.main()
