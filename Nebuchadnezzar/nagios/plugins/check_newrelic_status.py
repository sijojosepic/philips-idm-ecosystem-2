#!/usr/bin/env python
# Version:0.1
##########################################################
# Shinken Plugin - The intent of this plugin is
# connect to the New Relic alert violations API end point
# and it returns a list of the associated event to your
# New Relic account.
# Upon 200 HTTPS response status from API, the events
# that are not closed will be retrieved and status will
# be displayed based on the status received either
# CRITICAL or WARNING
# If API response is not equal to 200 plugin will return
# CRITICAL status with response status code
##########################################################

from __future__ import print_function

import sys
import requests
import argparse
from requests.exceptions import RequestException

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Check the Health of an Application over HTTP')
    parser.add_argument('-K', '--api_key', required=True,
                        help='The New Relic API key to login')
    parser.add_argument('-u', '--url', required=True,
                        help='The New Relic API url to login')
    results = parser.parse_args(args)
    return results.api_key, results.url


def get_url(key, url):
    headers = {'content-type': 'application/json', 'X-Api-Key': key}
    response = requests.get(url, verify=False, headers=headers, timeout=5)
    return response


def error_stats(response):
    if response.status_code in [401, 403]:
        return CRITICAL, 'CRITICAL - Invalid API key or Your New Relic API access is not enabled, status code {0}.'.\
            format(response.status_code)
    elif response.status_code in [500, 422, 408]:
        return CRITICAL, 'CRITICAL - We hit a server error or error occurred while trying to list violations or resource' \
                         ' timed out, status code {0}.'.format(response.status_code)
    else:
        return CRITICAL, 'CRITICAL - Error connecting to New Relic API, status code {0}.'.format(response.status_code)


class StatusVerifier(object):
    MSG_TEMP = 'APM name - {0}, Alert Policy - {1}, Alert label - {2}.\n'
    OK_MSG = 'OK - No Alerts violations found.'
    CRIT_MSG = 'CRITICAL - {0}'
    WARN_MSG = 'WARNING - {0}'
    OK, WARNING, CRITICAL = 0, 1, 2

    def __init__(self, violations, msg=''):
        self.violations = violations
        self.st_dct = {'Critical': [], 'Warning': []}
        self.stats = [self.OK]
        self.msg = msg

    def format_msg(self, obj):
        name = obj.get('entity', {}).get('name', '')
        policy_name, label = obj.get('label', ''), obj.get('policy_name', '')
        return self.MSG_TEMP.format(name, policy_name, label)

    def compose_status(self):
        for item in self.violations:
            if not item.get('closed_at'):
                self.st_dct[item['priority']].append(self.format_msg(item))

    def compose_msg(self, msg_lst, msg_template):
        msg = ''.join(msg_lst)
        return msg_template.format(msg)

    def get_status(self):
        msg = ''
        self.compose_status()
        if self.st_dct['Critical']:
            self.stats.append(self.CRITICAL)
            msg = self.compose_msg(self.st_dct['Critical'], self.CRIT_MSG)
        if self.st_dct['Warning']:
            self.stats.append(self.WARNING)
            msg += self.compose_msg(self.st_dct['Warning'], self.WARN_MSG)
        return max(self.stats), msg or self.OK_MSG


def main(api_key, url):
    status, msg = CRITICAL, ''
    try:
        response = get_url(api_key, url)
        if response:
            data = response.json()
            obj = StatusVerifier(data.get('violations', []))
            status, msg = obj.get_status()
        else:
            status, msg = error_stats(response)
    except RequestException as e:
        msg = 'CRITICAL - Request Error - {0}.'.format(e)
    except Exception as e:
        msg = 'CRITICAL - Error - {0}.'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_args())
