#!/usr/bin/env python

# check_rabbitmq_data plugin checks the channel objects, network partition status, memory watermark status, queues
# length(number of messages) and cluster member status using the API call to RabbitMQ server.

# Redis Cache : The services make multiple RabbitMQ API calls during execution, which leads to making the same API calls
# multiple times within a very short span of time.
# In order to avoid this duplicate API calls, the caching layer got introduced with 9mns TTL (TimeToLive) as the default
# service frequency is 10mns.
# This cache can be disabled by passing the argument --enable_cache with value as 'no'.
# During retries or non OK previous service status cached data would not be used, rather the information would be
# fetched directly from the API.
# For storing the data in the cache the key will be composed using the following convention
#   '<hostname>_rabbitmq_<subpath>' (subpath - queues,channels,connections, nodes, overview).

# API calls
# 1. https://rabbitmq-hostname:port/api/nodes
# 2. https://rabbitmq-hostname:port/api/overview
# 3. https://rabbitmq-hostname:port/api/queues
# 4. https://rabbitmq-hostname:port/api/channels

# RabbitMQ Queue Monitoring Examples
# ./check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "queues" -w 950 -c 1000

# RabbitMQ Cluster Monitoring Examples
# ./check_rabbitmq_data.py -H 'hostaddress' -P 15671 -u 'username' -p 'password' -r 'https' -s 'nodes' -S 'cluster'


import argparse
import sys
import json
import requests
from collections import defaultdict
from requests.exceptions import RequestException, HTTPError, ConnectTimeout
from scanline.utilities.cache import SafeCache

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of RabbitMQ Services')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the rabbitmq server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the rabbitmq server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-r', '--protocol', default='https',
                        help='The protocol of the rabbitmq server ')
    parser.add_argument('-s', '--subpath', required=True,
                        help='The subpath for the rabbitmq server')
    parser.add_argument('-S', '--service',
                        help='Service name of the rabbitmq server')
    parser.add_argument('-w', '--warning_val', default=500,
                        help='The warning threshold.')
    parser.add_argument('-c', '--critical_val', default=1000,
                        help='The critcal threshold.')
    parser.add_argument('-t', '--req_timeout', default=15,
                        help='The request timeout.')
    parser.add_argument('-X', '--previous_state', required=True,
                        help='The service previous execution state OK, WARNING or CRITICAL')
    parser.add_argument('-C', '--enable_cache', default='no',
                        help='Enable caching for RabbitMQ API response[Yes/No]')
    parser.add_argument('-T', '--cache_timeout', type=int, default=15,
                        help='Cache timeout.')

    results = parser.parse_args(args)

    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.protocol,
        results.subpath,
        results.service,
        results.warning_val,
        results.critical_val,
        results.req_timeout,
        results.previous_state,
        results.enable_cache,
        results.cache_timeout,
    )


def get_url(hostname, port, protocol, subpath):
    return '{0}://{1}:{2}/api/{3}'.format(protocol, hostname, port, subpath)


def get_api_response(url, username, password, req_timeout):
    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
        'connection': 'keep-alive',
        'content-type': 'application/json',
        'upgrade-insecure-requests': '1',
    }
    auth = (username, password)
    response = requests.get(url, headers=headers, auth=auth,
                            timeout=int(req_timeout), verify=False)
    response.raise_for_status()
    return json.loads(response.content)


def read_cache(key):
    return SafeCache.do('get', key)


def write_cache(key, json_data, cache_timeout):
    SafeCache.do('setex', key, json.dumps(json_data), cache_timeout)


def do_request(hostname, subpath, port, protocol, username, password, req_timeout):
    url = get_url(hostname, port, protocol, subpath)
    return get_api_response(url, username, password, req_timeout)


def get_status_data(hostname, subpath, port, protocol, username, password, req_timeout, enable_cache, cache_timeout,
                    previous_state):
    """ This function computes the status data either from cache or by an API request.
    It reads and writes from and to cache, only when the enable_cache flag value is 'yes'.
    Same time if the previous service status is non OK, function will not use cached values. """

    if enable_cache.lower() == 'yes':
        key = "{0}_rabbitmq_{1}".format(hostname, subpath)
        if previous_state == 'OK':
            json_data = read_cache(key)
            if not json_data:
                json_response = do_request(hostname, subpath, port, protocol, username, password, req_timeout)
                write_cache(key, json_response, cache_timeout)
            else:
                json_response = json.loads(json_data)
        else:
            json_response = do_request(hostname, subpath, port, protocol, username, password, req_timeout)
            write_cache(key, json_response, cache_timeout)
    else:
        json_response = do_request(hostname, subpath, port, protocol, username, password, req_timeout)
    return json_response


def check_status(hostname, port, username, password, protocol, subpath, service, warning_val, critical_val, req_timeout,
                 previous_state, cached, cache_timeout):
    try:
        config_json = get_status_data(hostname, subpath, port, protocol, username, password, req_timeout, cached,
                                      cache_timeout, previous_state)
        if subpath == 'queues':
            return get_queues_status(config_json, warning_val, critical_val)
        elif subpath == 'channels':
            return get_channel_obj_status(config_json, warning_val, critical_val)
        elif subpath == 'connections' and service == 'state':
            return get_connection_state(config_json)
        elif subpath == 'connections' and service == 'count':
            return get_connection_count(config_json, critical_val)
        elif subpath == 'nodes' and service == 'watermark':
            return get_watermark_status(config_json)
        elif subpath == 'nodes' and service == 'disk':
            return get_disk_status(config_json)
        elif subpath == 'nodes' and service == 'partitions':
            return get_partitions_status(config_json)
        elif subpath == 'nodes' and service == 'cluster':
            overview_json = get_status_data(hostname, 'overview', port, protocol, username, password, req_timeout,
                                            cached, cache_timeout, previous_state)
            return get_cluster_status(config_json, overview_json)
    except HTTPError as e:
        return UNKNOWN, 'UNKNOWN : HTTPError - {0}'.format(e)
    except ConnectTimeout as e:
        return UNKNOWN, 'UNKNOWN : Connection Error - RabbitMQ server is not accessible'
    except RequestException as e:
        return UNKNOWN, 'UNKNOWN : Request Error {0}'.format(e)
    except Exception as e:
        return CRITICAL, 'CRITICAL : Error while retrieving response data {0}'.format(e)


def get_watermark_status(config_json):
    node_mem_details = ''
    for data in config_json:
        if data.get('mem_alarm'):
            node_mem_details = "{0} {1} - Memory limit: {2}, Memory used: {3} \n" \
                .format(node_mem_details, str(data['name']), str(data['mem_limit']), str(data['mem_used']))
    if node_mem_details:
        return CRITICAL, 'CRITICAL : Watermark is critical, memory alarm is True. \n {0}'.format(node_mem_details)
    else:
        return OK, 'OK : RabbitMQ Memory utilization is OK.'


def get_disk_status(config_json):
    node_disk_details = ''
    for data in config_json:
        if data.get('disk_free_alarm'):
            node_disk_details = "{0} {1} - Disk free limit: {2}, Disk free: {3} \n" \
                .format(node_disk_details, str(data['name']), str(data['disk_free_limit']), str(data['disk_free']))
    if node_disk_details:
        return CRITICAL, 'CRITICAL : Disk is critical, disk alarm is True. \n {0}.'.format(node_disk_details)
    else:
        return OK, 'OK : RabbitMQ Disk utilization is OK.'


def get_connection_state(config_json):
    msg = ''
    for data in config_json:
        if data.get('state') == 'flow':
            name = '\n Name: {0}, State: {1}'.format(data.get('name'), data.get('state'))
            msg = '{0}{1}'.format(msg, name)
    if msg:
        return CRITICAL, 'CRITICAL : Connection(s) in Flow state {0}'.format(msg)
    else:
        return OK, 'OK : RabbitMQ connections state are OK.'


def get_connection_count(config_json, connection_obj_critical):
    if len(config_json) > int(connection_obj_critical):
        return CRITICAL, 'CRITICAL : Total Connection Count is {0}, which has breached the CRITICAL threshold of {1}'\
            .format(len(config_json), connection_obj_critical)
    else:
        return OK, 'OK : Total Connection Count: {0}'.format(len(config_json))


def get_channel_obj_status(config_json, channel_obj_warn, channel_obj_critical):
    warn_condition1 = len(config_json) >= int(channel_obj_warn)
    warn_condition2 = len(config_json) < int(channel_obj_critical)
    if len(config_json) < int(channel_obj_warn):
        return OK, 'OK : The Channel Object Count is {0}'.format(len(config_json))
    elif warn_condition1 and warn_condition2:
        return WARNING, 'WARNING : The Channel Object Count is {0}, which has breached the WARNING threshold of {1}'\
            .format(len(config_json), channel_obj_warn)
    elif len(config_json) >= int(channel_obj_critical):
        return CRITICAL, 'CRITICAL : The Channel Object Count is {0}, which has breached the CRITICAL threshold of {1}'\
            .format(len(config_json), channel_obj_critical)


def get_partitions_status(config_json):
    node_partition_details = ''
    for data in config_json:
        if data.get('partitions'):
            node_partition_details = "{0} {1}: Partition- {2} \n ".format(node_partition_details, str(data['name']),
                                                                          [str(name) for name in data['partitions']])
    if node_partition_details:
        return CRITICAL, 'CRITICAL : Network partitions detected: \n {0}'.format(node_partition_details)
    else:
        return OK, 'OK : No network partitions.'


def get_queues_status(config_json, warning_val, critical_val):
    warning_queues, critical_queues = '', ''
    queue_count, crit_count, warn_count = 0, 0, 0
    for dict_data in config_json:
        data = defaultdict(str, dict_data)
        queue_count += 1
        if data.get('messages'):
            if int(data['messages']) > int(critical_val):
                critical_queues = '{0} {1} - {2} Messages \n' \
                    .format(critical_queues, data['name'], data['messages'])
                crit_count = crit_count + 1
            elif int(data['messages']) > int(warning_val):
                warning_queues = '{0} {1} - {2} Messages \n' \
                    .format(warning_queues, data['name'], data['messages'])
                warn_count = warn_count + 1
    state, msg = OK, 'OK : Found {0} Queues, None of them has breached the threshold defined for number of messages ' \
                     '(Warning-{1}, Critical-{2})'.format(queue_count, warning_val, critical_val)
    if crit_count:
        state = CRITICAL
        if warn_count:
            msg = "CRITICAL : Out of {0} queues, {1} queues has breached CRITICAL threshold of {2}, {3} queues has " \
                  "breached WARNING threshold of {4}. \n CRITICAL : \n {5} \n WARNING : \n {6}" \
                .format(queue_count, crit_count, critical_val, warn_count, warning_val, critical_queues, warning_queues)
        else:
            msg = "CRITICAL : Out of {0} queues, {1} queues has breached  CRITICAL threshold of {2}. \n " \
                  "CRITICAL : \n  {3}" \
                .format(queue_count, crit_count, critical_val, critical_queues)
    elif warn_count:
        state = WARNING
        msg = 'WARNING : Out of {0} queues, {1} queues has breached  WARNING threshold of {2}. \n {3}' \
            .format(queue_count, warn_count, warning_val, warning_queues)
    return state, msg


def get_cluster_status(node_json, overview_json):
    # Check if node is out of network
    node_list = [str(data['name']) for data in node_json if not data['running']]
    if node_list:
        return CRITICAL, "CRITICAL : Node is out of network/not reachable {0}".format(node_list)

    # Check if node is out of cluster
    cluster_data = overview_json['listeners']
    context_data = overview_json['contexts']
    cluster_node = []
    context_node = []
    for data in cluster_data:
        if data["protocol"] == "clustering":
            cluster_node.append(str(data["node"]))
    for data in context_data:
        context_node.append(str(data["node"]))

    # In the RabbitMQ overview API response there are 'contexts' and 'listeners' attributes available.
    # Both of these contains information about the nodes in the cluster.
    # When a node is out a cluster, it will present in listeners but not in context.
    # If network partition occurs, it has be observed that all node details will present in context but not in listeners
    # By comparing these contexts and listeners, the difference of which provides which all nodes are out of the Cluster

    check_context_node = set(cluster_node) - set(context_node)
    check_listener_node = set(context_node) - set(cluster_node)
    if check_context_node:
        return CRITICAL, "CRITICAL : Node is out of cluster {0}".format(list(check_context_node))
    elif check_listener_node:
        return WARNING, "WARNING : All nodes are not part of cluster, may be due to network partition"
    else:
        return OK, "OK : All nodes are up and running {0}".format(cluster_node)


def main():
    state, msg = check_status(*check_args())
    print(msg)
    sys.exit(state)


if __name__ == '__main__':
    main()
