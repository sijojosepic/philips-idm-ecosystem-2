#!/usr/bin/env python
# Version:0.1
##########################################################
# This is intended to find the windows firewall profile state,
# firewall states can be any of Domain, Private, Public, or all of them.
# The plugin status should be "CRITICAL" if there is no firewall profile configured.
# Behaviour
# ---------
# profile state is on
#	  plugin exit with OK status
# Profile state is off,
#	  plugin exit with CRITICAL status
#
# The firewall state which needs to be checked/monitored can be supplied to the Plugin
# Domain state will be checked all the times, even if its not suplied through cmd args
#
#
# example
# python check_firewall_status.py -H 192.168.xx.xxx -U 'username' -P 'pass' -F "['Domain','Private','public']"
#
# CRITICAL - The Domain Firewall state is OFF.
# The Private Firewall state is OFF.
#
##########################################################

from __future__ import print_function

import argparse
import re
import sys
from requests.exceptions import RequestException
from scanline.utilities.utils import str_to_list
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)

# This is for service graph in the thruk portal
GRAPHER = '| service status={0};1;2;0;2'

OK = 0
CRITICAL = 2
UNKNOWN = 3


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Check the Firewall state of Windows server')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of the windows node')
    parser.add_argument('-U', '--username', required=True,
                        help='Username of the windows node')
    parser.add_argument('-P', '--password', required=True,
                        help='Password of the windows node')
    parser.add_argument('-F', '--fwall_profiles', required=True,
                        help='Firewall profiles of the windows node')
    results = parser.parse_args(args)
    return (results.hostname, results.username, results.password, results.fwall_profiles)


class FwallStatusParser(object):
    PS_FWALL_STATES = r'''netsh advfirewall show allprofiles state'''
    REGX_FWALL_PROFILE_STATES = '(.*?):\s+State\s+(OFF|off|ON|on)'

    def __init__(self, hostname, username, password, fwall_profiles):
        self.username = username
        self.password = password
        self.hostname = hostname
        self.fwall_profiles = fwall_profiles

    def get_profiles(self):  # '["Domain","Private","public"]' => ['domain','private','public']
        profiles = ['domain']
        profiles.extend(str_to_list(self.fwall_profiles.lower()))
        return set(profiles)

    def clean_ps_response(self, data):
        self.cleaned_data = data.replace('\n', '').replace('\r', '').replace('-', '').replace(' Profile Settings', '')

    def format_msg(self, profile, state):
        msg = 'The {0} Firewall state is {1}.\n'.format(profile, state)
        return msg

    def compose_msg(self, ok_msg, crt_msg):
        pre_ok = 'OK : '
        pre_crt = 'CRITICAL : '
        if crt_msg:
            crt_msg = pre_crt + crt_msg
        if ok_msg:
            ok_msg = pre_ok + ok_msg
        final_msg = crt_msg + ok_msg
        return final_msg

    @property
    def matched_fwall_profiles(self):
        return re.findall(self.REGX_FWALL_PROFILE_STATES, self.cleaned_data)

    def compose_state(self):
        ok_msg, crt_msg = '', ''
        status = OK
        profiles = self.get_profiles()
        for match in self.matched_fwall_profiles:
            profile = match[0].lower()
            state = match[1].lower()
            if profile in profiles:
                if state == 'off':
                    status = CRITICAL
                    crt_msg += self.format_msg(profile, state)
                else:
                    ok_msg += self.format_msg(profile, state)
        final_msg = self.compose_msg(ok_msg, crt_msg)
        if not final_msg:
            status = CRITICAL
        return status, final_msg or 'No firewall state found'

    def get_status(self, fwall_states):
        self.clean_ps_response(fwall_states)
        return self.compose_state()

    def execute_powershell(self):
        status = CRITICAL
        try:
            win_rm = WinRM(self.hostname, self.username, self.password)
            fwall_states = win_rm.execute_ps_script(self.PS_FWALL_STATES)
            if fwall_states.status_code == 0:
                status, msg = self.get_status(fwall_states.std_out)
                if status == CRITICAL:
                    msg = msg + GRAPHER.format(CRITICAL)
                else:
                    msg = msg + GRAPHER.format(OK)
            else:
                msg = 'CRITICAL : {0}'.format(str(fwall_states.std_out.strip())) + GRAPHER.format(CRITICAL)
        except AuthenticationError as e:
            status, msg = UNKNOWN, 'UNKNOWN : WinRM Error {0}'.format(e) + GRAPHER.format(UNKNOWN)
        except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
            msg = 'CRITICAL : WinRM Error {0}'.format(e) + GRAPHER.format(CRITICAL)
        except RequestException as e:
            msg = 'CRITICAL : Request Error {0}'.format(e) + GRAPHER.format(CRITICAL)
        except TypeError as e:
            if 'takes exactly 2' in str(e):
                msg = 'CRITICAL : Issue in connecting to node - {0}'.format(self.hostname) + GRAPHER.format(CRITICAL)
            else:
                msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(
                    self.hostname) + GRAPHER.format(
                    CRITICAL)
        except Exception as e:
            msg = 'CRITICAL : Exception {0}'.format(e) + GRAPHER.format(CRITICAL)
        return status, msg


def main(hostname, username, password, fwall_profiles):
    status, msg = CRITICAL, ''
    obj = FwallStatusParser(hostname, username, password, fwall_profiles)
    status, msg = obj.execute_powershell()
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_args())
