#!/usr/bin/env python
from __future__ import print_function
import sys
import argparse
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)
from requests.exceptions import RequestException
from scanline.utilities.win_rm import WinRM

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Monitor the age of files inside a folder, and its subfolders.')
    parser.add_argument('-H', '--host', required=True,
                        help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True,
                        help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-P', '--password', required=True,
                        help='The password for the server')
    parser.add_argument('-C', '--command', required=True,
                        help='The command Name (eg Get-ClusterNodes)')
    parser.add_argument('-S', '--subcommand', required=True,
                        help='The sub command Name (eg Nodes)')
    results = parser.parse_args(args)
    return (results.host, results.username,
            results.password, results.command, results.subcommand)


def ps_script(command):
    execute = r"Get-{0} | select Name,State"
    return execute.format(command)


def cluster_status(out_put):
    status, msg = CRITICAL, ''
    out_put = out_put.std_out.replace('-', '').split('\r\n')
    for data in out_put:
        if 'Cluster Name' in data:
            cluster_output = data.split('Cluster Name')[1]
            cluster_state = cluster_output.strip()
            if cluster_state == 'Online':
                status, msg = OK, 'OK : Cluster is up and running'
            elif cluster_state == 'Offline':
                status, msg = CRITICAL, 'CRITICAL : Cluster is Offline'
    return status, msg


def file_share_status(out_put):
    status, msg = CRITICAL, ''
    out_put = out_put.std_out.replace('-', '').split('\r\n')
    for data in out_put:
        if 'File Share Witness' in data:
            file_share_output = data.split('File Share Witness')[1]
            file_share_state = file_share_output.strip()
            if file_share_state == 'Online':
                status, msg = OK, 'OK : File Share Witness is Online'
            elif file_share_state == 'Offline':
                status, msg = CRITICAL, 'CRITICAL : File Share Witness is Offline'
            elif file_share_state == 'Failed':
                status, msg = CRITICAL, 'CRITICAL : File Share Witness is Failed to start'
    return status, msg


def cluster_node_status(out_put):
    out_put = out_put.std_out.replace('-', '').split('\r\n')
    critical_list = []
    warning_list = []
    for data in out_put[3:]:
        if data:
            if 'Down' in data:
                critical_list.append(data.split()[0])
            elif 'Paused' in data:
                warning_list.append(data.split()[0])
    if critical_list:
        status, msg = CRITICAL, 'CRITICAL : Following node in the cluster is Down - {0}'\
            .format(','.join(i for i in critical_list))
        if warning_list:
            msg = msg + '\nFollowing node in the cluster is in Paused state{0}'.format(','.join(i for i in warning_list))
    elif warning_list:
        status, msg = WARNING, 'WARNING : Following node in the cluster is Paused - {0}'\
            .format(','.join(i for i in warning_list))
    else:
        status, msg = OK, 'OK : All nodes in the cluster are up and running'
    return status, msg


def main(host, username, password, command, subcommand):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        if command == 'ClusterResource' and subcommand == 'Cluster Name':
            out_put = win_rm.execute_ps_script(ps_script(command))
            status, msg = cluster_status(out_put)
        elif command == 'ClusterResource' and subcommand == 'File Share Witness':
            out_put = win_rm.execute_ps_script(ps_script(command))
            status, msg = file_share_status(out_put)
        elif command == 'ClusterNode' and subcommand == 'Node':
            out_put = win_rm.execute_ps_script(ps_script(command))
            status, msg = cluster_node_status(out_put)
        else:
            status, msg = UNKNOWN, 'UNKNOWN: Command or sub-command is invalid'
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to cluster IP'
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to cluster IP)'
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*parse_cmd_args())
