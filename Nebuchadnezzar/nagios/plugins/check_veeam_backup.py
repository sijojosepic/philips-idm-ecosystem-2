#!/usr/bin/env python
from __future__ import print_function
import sys
import argparse
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check veeam backup or veeam sure backup jobs status')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address to post to')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the server')
    parser.add_argument('-j', '--jobtype', required=True,
                        help='The job type of veeam backup or veeam sure backup')
    parser.add_argument('-b', '--backuptype', required=False,
                        help='The backup type, whether veeam backup or veeam sure backup', default='veeam_backup')
    results = parser.parse_args(args)
    return (results.hostname,
            results.username,
            results.password,
            results.jobtype,
            results.backuptype)


def get_ps_vbr_script():
    """ powershell script to get the veeam backup jobs"""
    vbr_script = """ Add-PSSnapin VeeamPSSnapin
        $VJobs = Get-VBRJob
        ForEach ($VJob in $VJobs) {
        $name = $VJob.Info.Name
        $jobtype = $VJob.Info.JobType
        $result = $VJob.Info.LatestStatus
        Write-Output $name`t$jobtype`t$result
        } """
    return vbr_script


def get_ps_vsb_script():
    """powershell script to get the veeam sure backup jobs"""
    vsb_script = """ Add-PSSnapin VeeamPSSnapin
        $VJobs = Get-VSBJob
        ForEach ($VJob in $VJobs) {
        $name = $VJob.Name
        $jobtype = $VJob.JobType
        $result = $VJob.GetLastResult()
        Write-Output $name`t$jobtype`t$result
        } """
    return vsb_script


def get_status(failed_job, success_job, warning_job):
    if len(failed_job) == 0 and len(success_job) == 0 and len(warning_job) == 0:
        status = OK
        msg = 'OK - No jobs are running'
    elif failed_job:
        status = CRITICAL
        msg = 'CRITICAL - Failed Jobs ', str(len(failed_job)), '\n', '\n'.join(failed_job)
        if warning_job:
            msg = 'CRITICAL - Failed Jobs ', str(len(failed_job)), '\n', '\n'.join(failed_job),\
                  '\n\nWarning Jobs ', str(len(warning_job)), '\n', '\n'.join(warning_job)
    elif warning_job:
        status = WARNING
        msg = 'WARNING - Warning Jobs ', str(len(warning_job)), '\n', '\n'.join(warning_job)
    else:
        status = OK
        msg = 'OK - All jobs run successfully'
    return status, ''.join(msg)


def get_backup_type(backuptype):
    if backuptype == 'sure_backup':
        return get_ps_vsb_script()
    else:
        return get_ps_vbr_script()


def get_job_status(hostname, username, password, jobtypee, backuptype):
    jobname = []
    jobtype = []
    result = []
    failed_job = []
    success_job = []
    warning_job = []
    try:
        win_rm = WinRM(hostname, username, password)
        ps_script = get_backup_type(backuptype)
        powershell_output = win_rm.execute_ps_script(ps_script)
        if powershell_output.status_code != 0:
            status, msg = CRITICAL, 'Error while running cmdlets -' + powershell_output.std_err
            return status, msg
        powershell_stdout = powershell_output.std_out.split('\r\n')
        for results in powershell_stdout:
            if results:
                jobname.append(results.split('\t')[0])
                jobtype.append(results.split('\t')[1])
                result.append(results.split('\t')[2])
        for job_name, job_result, job_type in zip(jobname, result, jobtype):
            if job_type == jobtypee:
                if job_result == 'Failed':
                    failed_job.append(job_name)
                if job_result == 'Success':
                    success_job.append(job_name)
                if job_result == 'Warning':
                    warning_job.append(job_name)
        status, msg = get_status(failed_job, success_job, warning_job)
    except (AuthenticationError, WinRMOperationTimeoutError,
            WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
        status = 2
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(hostname)
            status = 2
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(hostname)
            status = 2
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
        status = 2
    return status, msg


def main():
    status, message = get_job_status(*check_arg())
    print(message)
    sys.exit(status)


if __name__ == '__main__':
    main()
