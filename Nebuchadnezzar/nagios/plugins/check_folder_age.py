#!/usr/bin/env python

import argparse
import logging
import sys
from datetime import datetime, timedelta
from phimutils.timestamp import wmi_datetime
from scanline.utilities.wmi import WMIHostDriller
from scanline.utilities.dns import get_address


logger = logging.getLogger(__name__)
DELTAS = ('minutes', 'seconds', 'hours', 'days')
COUNT = 0


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a wmi command')
    parser.add_argument('-T', '--delta', type=int, help='the time delta to add', required=True)
    parser.add_argument('-N', '--unit', help='The unit for the delta', choices=DELTAS, default='minutes')
    parser.add_argument('-D', '--domain', required=True, help='The domain of the server')
    parser.add_argument('-H', '--hostaddress', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=True, help='The user name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-R', '--drive', required=True, help='The drive name of the server')
    parser.add_argument('-F', '--folder', required=True, help='The password for the server')
    results = parser.parse_args(args)
    return (results.delta, results.unit, results.domain, results.hostaddress,
            results.username, results.password, results.drive, results.folder)


def wmi_folder_age(delta, unit, domain, hostaddress, username, password, drive, folder):
    global COUNT
    status = 2
    timestamp = wmi_datetime(datetime.now() - timedelta(**{unit: delta}))
    output = 'CRITICAL - {count} Backup found compared to timestamp {timestamp}'.format(count=COUNT,
                                                                                        timestamp=timestamp)
    folder = folder.replace('/', '\\\\')
    logger.info('Trying to fetch host address for {host}'.format(host=hostaddress))
    hostaddress = get_address(hostaddress)
    driller = WMIHostDriller(host=hostaddress, user=username,
                             password=password, domain=domain)
    command = ''.join(["select LastAccessed From CIM_Directory Where Drive =",
                       "'{drive}' And Path =".format(drive=drive),
                       "'{folder}' AND LastAccessed > ".format(folder=folder),
                       "'{timestamp}'".format(timestamp=timestamp)])
    wmi_file_command = ''.join(["select LastAccessed From CIM_DataFile ",
                                "Where Drive ='{drive}' And Path =".format(drive=drive),
                                "'{folder}' AND LastAccessed > ".format(folder=folder),
                                "'{timestamp}'".format(timestamp=timestamp)])
    try:
        service_reader = driller.query_wmi(command)
        COUNT = len(list(service_reader))
        service_reader = driller.query_wmi(wmi_file_command)
        COUNT= COUNT + len(list(service_reader))
    except TypeError as e:
        logger.warning('WMI is not accessible for host {host}'.format(host=hostaddress))

    if COUNT:
        status = 0
        output = 'OK - {count} Backup found compared to timestamp {timestamp}'.format(count=COUNT,
                                                                                      timestamp=timestamp)

    return status, output


def main():
    state, output = wmi_folder_age(*check_arg())
    if output:
        print(output)
    sys.exit(state)


if __name__ == '__main__':
    main()
