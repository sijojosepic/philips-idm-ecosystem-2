#!/usr/bin/env python

"""
 Check the size of a given file or folder and results approprite statuses,
 Based on critical and warning threshold
 RabbitMQ log file monitoring, file name has to be composed from hostname
 Corner cases: Authurization, File path doesn't exist.

 Examples

  ./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'folder' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log11' -w 450 -c 500

WARNING : Path does not exist - S:/Philips/apps/iSite/data/rabbitmQ/log11


./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'folder' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log' -w 450 -c 500

OK: Current Folder Size is 5.73MB.

./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'rabbitmq' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/' -w 450 -c 500

OK: The Rabbitmq Log file size is 3.75MB.

./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'rabbitmq' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log12' -w 450 -c 500

WARNING : Could not locate RabbitMQ log file (rabbit@<hostname>.log), in the given path S:/Philips/apps/iSite/data/rabbitmQ/log12


./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'file' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/rabbitmq@idm04if1.log' -w 450 -c 500

WARNING : Path does not exist - S:/Philips/apps/iSite/data/rabbitmQ/log/rabbitmq@idm04if1.log


./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'file' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/rabbit@idm04if1.log' -w 450 -c 500

OK: Current File Size is 3.75MB.


./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'usero' -P pwd' -t 'file' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/rabbit@idm04if1.log' -w 450 -c 500

UNKNOWN : Authentication Error - the specified credentials were rejected by the server
"""

from __future__ import print_function
import argparse
import sys
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)


OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

FILE_SIZE_SCRIPT = """(Get-ChildItem -Path {0}).Length/1mb"""
FOLDER_SIZE_SCRIPT = """(Get-ChildItem -LiteralPath {0} -recurse | Measure-Object -property length -sum).sum/1mb"""
RBQ_LOG_SIZE = """$hostName = hostname;(Get-ChildItem -Path {0}rabbit@$hostName.log).Length/1mb"""


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(
        description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of the server')
    parser.add_argument('-U', '--username', required=True,
                        help='Username of the server')
    parser.add_argument('-P', '--password', required=True,
                        help='Password of the server')
    parser.add_argument('-t', '--type', required=False,
                        help='file or folder', default='file')
    parser.add_argument('-p', '--path', required=True, help='File/Folder Path')
    parser.add_argument('-w', '--warning', required=True,
                        help='File/folder size warning threshold flag')
    parser.add_argument('-c', '--critical', required=True,
                        help='File/folder size critical threshold flag')
    results = parser.parse_args(args)
    return (
        results.hostname, results.username, results.password, results.type, results.path, results.critical,
        results.warning)


def clean_path(path):
    return path if path.endswith('/') else path + '/'


def get_rabbitmq_log_file_size(win_rm, path):
    return win_rm.execute_ps_script(RBQ_LOG_SIZE.format(path))


def get_file_size(win_rm, path):
    return win_rm.execute_ps_script(FILE_SIZE_SCRIPT.format(path))


def get_folder_size(win_rm, path):
    return win_rm.execute_ps_script(FOLDER_SIZE_SCRIPT.format(path))


def format_output(std_out, std_err, path, type_):
    if std_err:
        if 'cannot find path' in std_err.lower():
            if type_ == "rabbitmq":
                std_err = "Could not locate RabbitMQ log file (rabbit@<hostname>.log), in the given path {0}"
            else:
                std_err = 'Path does not exist - {0}'
        raise RuntimeError(std_err.format(path))
    if std_out:
        return round(float(std_out.strip('\r\n')), 2)


def rabbitmq_status(size, critical, warning):
    status, msg = OK, 'The RabbitMQ log file size is {0}MB'.format(size)
    if size > critical:
        status, msg = CRITICAL, '{0}, which has breached the CRITICAL threshold of {1}MB.'.format(
            msg, critical)
    elif size > warning:
        status, msg = WARNING, '{0}, which has breached the WARNING threshold of {1}MB.'.format(
            msg, warning)
    return status, msg


def get_status(size, critical, warning, type_):
    msg_template = 'Current {0} Size is {1}MB.'
    if type_ == 'rabbitmq':
        return rabbitmq_status(size, critical, warning)
    if size > critical:
        state, msg = CRITICAL, 'CRITICAL: '
    elif size > warning:
        state, msg = WARNING, 'WARNING: '
    else:
        state, msg = OK, 'OK: '
    return state, msg + msg_template.format(type_.title(), size)


def check_size(hostname, username, password, type_, path, critical, warning):
    """ Check the size of a given file or folder and results approprite statuses based on critical and warning threshold"""
    state, msg = CRITICAL, ''
    try:
        win_rm = WinRM(hostname, username, password)
        # For RabbitMQ log file name has to be formed dynamically
        # so here the path shold be folder where RabbitMQ file resides
        if type_ == 'rabbitmq':
            out_put = get_rabbitmq_log_file_size(win_rm, clean_path(path))
        elif type_ == 'folder':
            out_put = get_folder_size(win_rm, clean_path(path))
        elif type_ == 'file':
            out_put = get_file_size(win_rm, path)
        else:
            return CRITICAL, "CRITICAL: Unknown type {0}, type should be either file or folder".format(type_)

        size = format_output(out_put.std_out, out_put.std_err, path, type_)
        state, msg = get_status(size, float(critical), float(warning), type_)

    except AuthenticationError as e:
        state, msg = UNKNOWN, 'UNKNOWN : Authentication Error - {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        state, msg = CRITICAL, 'CRITICAL : WinRM Error {0}'.format(e)
    except RuntimeError as e:
        state, msg = WARNING, 'WARNING : {0}'.format(e)
    except Exception as e:
        state, msg = WARNING, 'WARNING : Exception {0}'.format(e)
    return state, msg


def main():
    state, output = check_size(*check_arg())
    print(output, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()

