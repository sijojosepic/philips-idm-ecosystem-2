#!/usr/bin/env python

import sys
import argparse
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)
from requests.exceptions import RequestException
from scanline.utilities.win_rm import WinRM

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Monitor Tivoli backup jobs by connecting to Tivoli client system')
    parser.add_argument('-H',  '--host', required=True, help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True, help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-u', '--tivoliuser', required=True, help='The user name of the Tivoli client')
    parser.add_argument('-p', '--tivolipassword', required=True, help='The password for the Tivoli client')
    results = parser.parse_args(args)
    return results.host, results.username, results.password, results.tivoliuser, results.tivolipassword


def ps_script(tivoliuser, tivolipassword):
    execute = r"cd 'C:\Program Files\Tivoli\TSM\baclient\' | dsmadmc.exe -ID={0} -PASSWORD={1} -DISPLAY=LIST " \
              "'QUERY EVENT * * BEGINDATE=TODAY-32 ENDDATE=TODAY FORMAT=DETAILED'"
    return execute.format(tivoliuser, tivolipassword)


def query_sch(tivoliuser, tivolipassword):
    execute = r"cd 'C:\Program Files\Tivoli\TSM\baclient\' | dsmadmc.exe -ID={0} -PASSWORD={1} -DISPLAY=LIST 'query schedule format=detailed'"
    return execute.format(tivoliuser, tivolipassword)


def format_query_sch(schedule_list):
    result = []
    out_put = schedule_list.split('\r\n')
    for data in out_put:
        if 'Schedule Name' in data:
            result.append(data.split(': ')[1])
    return result


def format_output(std_out, schedule_name):
    latest_list = []
    complete_list = std_out.split('\r\n\r\n')[::-1]
    for schedule in schedule_name:
        for data in complete_list:
            if schedule in data:
                content = []
                for item in data.replace('\r', '').split('\n'):
                    content.append(item.strip())
                latest_list.append(tuple(content))
                break
    critical_list = ['Missed', 'Failed', 'Uncertain', 'Pending', 'Restarted', 'Severed']
    ok_list = ['Started', 'Completed', 'Future']
    result_item = ['4', '8', '12']
    critical_sch = set()
    warning_sch = set()
    for schedule in latest_list:
        for sch in schedule:
            val = sch.split(':')[-1].strip()
            if 'Status' in sch and val not in ok_list:
                if val in critical_list:
                    critical_sch.add(str(schedule))
                else:
                    warning_sch.add(str(schedule))
            elif 'Result' in sch and val in result_item:
                critical_sch.add(str(schedule))
    if critical_sch:
        status = CRITICAL
        if warning_sch:
            msg = 'CRITICAL: Failed Jobs {0}\n{1}, \nWarning Jobs {2}\n{3} '\
                .format(len(critical_sch), '\n'.join(i for i in critical_sch),
                        len(warning_sch), '\n'.join(i for i in warning_sch))
        else:
            msg = 'CRITICAL: Failed Jobs {0}\n{1}'.format(len(critical_sch), '\n'.join(i for i in critical_sch))
    elif warning_sch:
        status = WARNING
        msg = 'WARNING: Warning Jobs {0}\n{1}'.format(len(warning_sch), '\n'.join(i for i in warning_sch))
    else:
        status = OK
        msg = 'OK : All backup scheduled jobs completed successfully'
    return status, msg


def process_output(out_put, schedule_name):
    if 'unable to establish session with server.' in out_put.std_out.lower():
        return CRITICAL, 'CRITICAL: Unable to establish session with Tivoli Server'
    else:
        status, msg = format_output(out_put.std_out, schedule_name)
        return status, msg


def main(host, username, password, tivoliuser, tivolipassword):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        schedule_list = win_rm.execute_ps_script(query_sch(tivoliuser, tivolipassword))
        if "the term 'dsmadmc.exe' is not recognized" in schedule_list.std_err.lower():
            msg = 'CRITICAL : dsmadmc.exe is not available or the path(C:\Program Files\Tivoli\TSM\Baclient)is not set' \
                  ' in Environment variables of Utility server'
        elif not schedule_list.std_out:
            msg = 'CRITICAL : Empty response from Tivoli client'
        else:
            schedule_name = format_query_sch(schedule_list.std_out)
            out_put = win_rm.execute_ps_script(ps_script(tivoliuser, tivolipassword))
            status, msg = process_output(out_put, schedule_name)
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to Tivoli client'
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to Tivoli client)'
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)

if __name__ == '__main__':
    main(*check_arg())
