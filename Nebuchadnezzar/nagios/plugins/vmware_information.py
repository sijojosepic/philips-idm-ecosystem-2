#!/usr/bin/env python

import sys
import argparse
from pysphere import VIServer, MORTypes
from phimutils.perfdata import INFO_MARKER

HARDWARE_OTHERIDENTIFYINGINFO_PROPERTY = "summary.hardware.otherIdentifyingInfo"
SERVICE_TAG_LABEL = 'ServiceTag'
MODEL_NUMBER_PROPERTY = "summary.hardware.model"

STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')

ACTIONS = {
    'model': {
        'label': 'Model',
        'function': 'get_model'
    },
    'servicetag': {
        'label': 'ServiceTag',
        'function': 'get_servicetag'
    }
}


def check_arg():
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Plugin to check Hardware Serial Number and Model Number')
    parser.add_argument('-H', '--hostaddress', help='Address of ESXi host', required=True)
    parser.add_argument('-u', '--user', help='ESXi Username', required=True)
    parser.add_argument('-p', '--password', help='ESXi Password', required=True)
    parser.add_argument('subfunction', choices=ACTIONS.keys(), help="The type of property to retrieve")
    results = parser.parse_args()

    return results.hostaddress, results.subfunction, results.user, results.password


def retrieve_host_property_value(esx_server, property):
    host_property = esx_server._retrieve_properties_traversal(property_names=[property], obj_type=MORTypes.HostSystem)
    return host_property[0].PropSet[0].Val


def get_model(esx_server):
    return retrieve_host_property_value(esx_server, MODEL_NUMBER_PROPERTY)


def get_servicetag(esx_server):
    other_hwi_value = retrieve_host_property_value(esx_server, HARDWARE_OTHERIDENTIFYINGINFO_PROPERTY)
    for HostSystemIdentificationEntry in other_hwi_value.HostSystemIdentificationInfo:
        if HostSystemIdentificationEntry.IdentifierType.Key == SERVICE_TAG_LABEL:
            return HostSystemIdentificationEntry.IdentifierValue


def vmware_information(host_address, subfunction, user, password):
    func = globals()[ACTIONS[subfunction]['function']]
    try:
        esx_server = VIServer()
        esx_server.connect(host_address, user, password)
        value = func(esx_server)
        state = 0
    except Exception as e:
        #print 'Unexpected error %s' % e
        state = 3
        value = 'Not_Found'
    finally:
        esx_server.disconnect()
    return state, ACTIONS[subfunction]['label'], value


def main():
    state, label, value = vmware_information(*check_arg())
    output_template = '{state} - {marker} {label} = {value}'
    print(output_template.format(state=STATES[state], label=label, value=value, marker=INFO_MARKER))
    sys.exit(state)


if __name__ == '__main__':
    main()
