#!/usr/bin/env python
# Version:0.1
############################################
# Shinken Plugin - File Age Monitoring using WinRM.
#
# The aim of this plugin is to monitor the age of files and count of files in a
# directory recursively and non recursively.
# Plugin uses four PowerShell scripts for file_count, file_count_recursive, file_age, file_age_recursive
# To Monitoring file count recursively, the ''service'' argument will be 'file_count_recursive'
# To Monitoring file count non recursively, the ''service'' argument will be 'file_count'
# To Monitoring file age recursively, the ''service'' argument will be 'file_age_recursive'
# To Monitoring file age non recursively, the ''service'' argument will be 'file_age'
# Plugin uses WinRM, to execute PowerShell scripts
# Plugin exit with OK, Critical or Warning statuses.

############################################
from __future__ import print_function
import sys


import argparse
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
from requests.exceptions import RequestException

from scanline.utilities.win_rm import WinRM

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Monitor the age of files inside a folder, and its subfolders.')
    parser.add_argument('-T', '--threshold', type=int, default=30,
                        help='Threshold in minutes')
    parser.add_argument('-H', '--host', required=True,
                        help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True,
                        help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-D', '--drive', required=True,
                        help='The Drive Name (eg S)')
    parser.add_argument('-P', '--password', required=True,
                        help='The password for the server')
    parser.add_argument('-F', '--folderpath', required=True,
                        help='The folder path (eg `\\Stentor\\forward`)')
    parser.add_argument('-S', '--service', default='file_age_recursive',
                        help='Select the service check')
    parser.add_argument('-C', '--critical', default=1000, type=int,
                        help='Critical Threshold for file_count')
    parser.add_argument('-W', '--warning', default=500, type=int,
                        help='Warning Threshold for file_count')
    results = parser.parse_args(args)
    return (results.host, results.username, results.password, results.threshold, results.drive, results.folderpath,
            results.service, results.critical, results.warning)


def get_path(drive, path):
    if not path.endswith('\\'):
        path = path + '\\'
    return drive + ':\\' + path


def ps_script(path, threshold, service):
    """ Returns PowerShell script for file_count, file_count_recursive, file_age and file_age_recursive """
    script_ps = ''
    if service == "file_count":
        script_ps = """Get-ChildItem -Path '"""+path+"""' | Where-Object { !$_.PSIsContainer }| % { 
        $dir=$_.directory 
        Write-Output $dir`t`t`t }"""
    elif service == "file_count_recursive":
        script_ps = """Get-ChildItem -Path '"""+path+"""' -Recurse | Where-Object { !$_.PSIsContainer }| % { 
        $dir=$_.directory 
        Write-Output $dir`t`t`t }"""
    elif service == "file_age":
        script_ps = """Get-childitem -path '"""+path+"""' | Where {!$_.PsIsContainer} | Where-object {($_.LastWriteTime -lt (Get-Date).AddMinutes(-""" + str(threshold) + """))}| % {
        $dir=$_.directory 
        $fullname=$_.FullName 
        Write-Output $dir`t$fullname`t`t`t }"""
    elif service == "file_age_recursive":
        script_ps = """Get-childitem -path '"""+path+"""' -Recurse| Where {!$_.PsIsContainer} | Where-object {($_.LastWriteTime -lt (Get-Date).AddMinutes(-"""+str(threshold)+"""))}| % {
        $dir=$_.directory 
        $fullname=$_.FullName 
        Write-Output $dir`t$fullname`t`t`t }"""
    return script_ps


def format_err(std_err, path):
    if 'pathnotfound' in std_err.lower():
        return OK, 'OK: {0} PathNotFound'.format(path)
    return WARNING, std_err


def compose_status_msg(directory_name, file_count, threshold):
    status = CRITICAL
    msg = '{0}: Found {1} {2}, aged more than {3} minutes \n{4}'
    directory_name = '\n'.join(directory_name)
    if directory_name:
        count_msg = 'files' if file_count > 1 else 'file'
        msg = msg.format('CRITICAL', file_count,
                         count_msg, threshold, directory_name)
    else:
        status = OK
        msg = msg.format('OK', file_count, 'file', threshold, directory_name)
    return status, msg


def process_output(out_put, path, threshold):
    if out_put.status_code == OK:
        out_put = out_put.std_out.split('\t\t\t')
        directory = set()
        filename = set()
        for result in out_put:
            result = result.replace('\r\n', '')
            if result:
                directory.add(result.split('\t')[0])
                filename.add(result.split('\t')[1])
        return compose_status_msg(directory, len(filename), threshold)
    return format_err(out_put.std_err, path)


def filecount_process_output(out_put, path, critical, warning):
    if out_put.status_code == OK:
        directory = set()
        file_count = 0
        out_put = out_put.std_out.split('\t\t\t')
        for result in out_put:
            result = result.replace('\r\n', '')
            if result:
                file_count = file_count+1
                directory.add(result.split('\t')[0])
        path = '\n'.join(directory)
        if file_count > critical:
            msg = "CRITICAL: Total file count is {0}, which has breached the critical threshold of {1} \n{2}"\
                .format(file_count, critical, path)
            return CRITICAL, msg
        elif file_count > warning:
            msg = "WARNING: Total file count is {0}, which has breached the warning threshold of {1} \n{2}"\
                .format(file_count, warning, path)
            return WARNING, msg
        else:
            msg = "OK: Found {0} File(s) ".format(file_count)
            return OK, msg
    return format_err(out_put.std_err, path)


def main(host, user, password, threshold, drive, path, service, critical, warning):
    """ Support four service  - file_count, file_count_recursive, file_age, and file_age_recursive """
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, user, password)
        if 'file_count' in service:   # If service is file_count or file_count_recursive
            out_put = win_rm.execute_ps_script(ps_script(get_path(drive, path), threshold, service))
            status, msg = filecount_process_output(out_put, path, critical, warning)
        else:                         # If service is file_age or file_age_recursive
            out_put = win_rm.execute_ps_script(ps_script(get_path(drive, path), threshold, service))
            status, msg = process_output(out_put, path, threshold)
    except RequestException as e:
        msg = 'CRITICAL : Request Error {0}'.format(e)
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : Authentication Error {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*parse_cmd_args())
