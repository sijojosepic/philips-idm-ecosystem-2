PS_QUERY = '''
$timesyncscript=
{{

        [string[]] $TimeServers = @("{ntpservers}")
        [string] $URL = '{passive_url}'
        [string] $TriggerId = "{uid}"

Add-Type @"
        using System.Net;
        using System.Security.Cryptography.X509Certificates;
        public class TrustAllCertsPolicy : ICertificatePolicy {{
            public bool CheckValidationResult(
                ServicePoint srvPoint, X509Certificate certificate,
                WebRequest request, int certificateProblem) {{
                return true;
            }}
        }}
"@    
    

    [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

    function Invoke-WebRequestPost {{
    param ([string] $URL,
           [string] $json)

    $BodyBytes = [System.Text.Encoding]::UTF8.GetBytes($json)
        $uriobj = [System.Uri]$URL
        $WebReq = [System.Net.HttpWebRequest][System.Net.WebRequest]::Create($uriobj);
        $WebReq.Method = 'POST';
        $WebReq.ContentType = 'application/json';
        $WebReq.ContentLength = $BodyBytes.Length;
        $WebReq.GetRequestStream().Write($BodyBytes, 0, $BodyBytes.Length)
        $response = $WebReq.GetResponse()
        if($response.StatusCode -eq "OK")
        {{
            Write-Log ("Posting json to url " + $URL + " completed")
        }}
        else
        {{
            Write-Log ("Posting json to url " + $URL + " failed")
        }}
        $response.Close()

    }}


    function Get-VMESXiTimeSync {{
        [CmdLetBinding()]
        Param()

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Getting time sync Status on $Env:COMPUTERNAME with ESXi host"
            $status = & $VMToolsPath timesync status
            Write-Log "Time sync Status on $Env:COMPUTERNAME with ESXi host is $status"
            if ($LASTEXITCODE -eq 0) {{
                Write-Log "Getting Time sync Status on $Env:COMPUTERNAME with ESXi host is Passed"
                Write-Verbose "Time sync Status on $Env:COMPUTERNAME Passed"
                $executionStatus = $true
            }}
            else {{
                Write-Log "Error - Getting Time sync Status on $Env:COMPUTERNAME with ESXi host is Failed!!"
            }}
        }}
        catch {{
            Write-Log "Error - Unable to get the Time sync Status on $Env:COMPUTERNAME with ESXi host!!"
            throw $_
        }}
        #returning status object
        @{{ Status = $status; ExecutionStatus = $executionStatus }}
    }}

    function Set-VMESXiTimeSync {{
        [CmdLetBinding()]
        Param(
            [string] $Action = 'disable'
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Setting time sync Status $Action on $Env:COMPUTERNAME"
            Write-Log "Command - '$VMToolsPath timesync $Action'  on $Env:COMPUTERNAME"
            & $VMToolsPath timesync $Action | Out-Null
            if ($LASTEXITCODE -eq 0) {{
                Write-Log "Time sync action '$Action' execution Passed on $Env:COMPUTERNAME"
                $executionStatus = $true
            }}
            else {{
                Write-Log "Error - Time sync action '$Action' execution Failed on $Env:COMPUTERNAME"
            }}
        }}
        catch {{
            Write-Log "Error - Unable to set Time sync action '$Action' on $Env:COMPUTERNAME"
            throw $_
        }}
        #returning status
        return $executionStatus
    }}

    function Execute-VMESXiTimeSync {{
        Param(
            [string] $Action = 'disable',
            [Int32] $Retries = 3
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        #default return value is 1;--> failed
        $returnCode = 2
        $count = 0
        While (($status = Get-VMESXiTimeSync).Status -notmatch $Action -and $count -lt $Retries) {{
            try {{
                Write-Log "Time sync Status on $Env:COMPUTERNAME with ESXi host is Enabled"
                $executionStatus = Set-VMESXiTimeSync -Action $Action
                Write-Log "Time sync Status on $Env:COMPUTERNAME with ESXi host is set to Disabled"
            }}
            catch {{
                $script = $_.InvocationInfo.ScriptName
                $lineNo = $_.InvocationInfo.ScriptLineNumber
                $line = $_.InvocationInfo.Line

                Write-Log "$script`nFailed at $lineNo`nFailed line $line`n$($_.Exception)`n"
                Write-Log $_

                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "Unknown error occurred"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            finally {{ $count ++ }}
        }}

        if ($status.Status -match $Action) {{
            $returnCode = 0

            if ($count -eq 0) {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is in desired state and no changes made"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            else {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix applied and recheck passed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
        }}
        else {{
            if ($executionStatus) {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix applied but recheck failed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            else {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix failed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
        }}

        Write-Verbose $message

        #return Service, Error Code, Message and FQDN
        @{{ProductService = $productService; ReturnCode = $returnCode; Message = $message; FQDN = $fqdn}}
    }}

    ##############################Execute-VMESXiTimeSync#####################################

    function Get-NTPParameterType {{
        [CmdLetBinding()]
        Param()

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Getting NTP-Parameters type on $Env:COMPUTERNAME"
            $NTPParameterType = Get-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name Type | Select -ExpandProperty Type
            $executionStatus = $true
            Write-Log "NTP-Parameters type on $Env:COMPUTERNAME - $NTPParameterType"
        }}
        catch {{
            Write-Log "Error - Getting NTP-Parameters type on $Env:COMPUTERNAME is Failed!!"
            throw $_
        }}
        #returning status object
        return $NTPParameterType
    }}

    function Get-NTPServerDetails {{
        [CmdLetBinding()]
        Param()

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Getting NTP-Server Details on $Env:COMPUTERNAME"
            $NTPServerDetails = Get-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name NTPServer | Select-Object -ExpandProperty NtpServer
            $executionStatus = $true
            Write-Log "NTP-Server Details on $Env:COMPUTERNAME - $NTPServerDetails"
        }}
        catch {{
            Write-Log "Error - Getting NTP-Server Details on $Env:COMPUTERNAME is Failed!!"
            throw $_
        }}
        #returning status object
        return $NTPServerDetails
    }}

    function Set-NTPParameterType {{
        [CmdLetBinding()]
        Param()

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        Write-Log "Checking $Env:COMPUTERNAME is DOMAIN CONTROLLER or DOMAIN COMPUTER"
        $domainServices = Get-Service -Name 'NTDS', 'DNS' -ErrorAction SilentlyContinue
        if ($domainServices) {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN CONTROLLER"
            $Type = 'NTP'
            Write-Log "Set NTP Paramter to $Type on $Env:COMPUTERNAME"
        }}
        else {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN COMPUTER"
            $Type = 'NT5DS'
            Write-Log "Set NTP Paramter to $Type on $Env:COMPUTERNAME"
        }}
        $executionStatus = $false
        try {{
            Write-Log "Setting NTP-Parameter Type on $Env:COMPUTERNAME"
            Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name Type -Value $Type
            Write-Log "NTP-Parameter Type on set to $Type on $Env:COMPUTERNAME"
            $executionStatus = $true
        }}
        catch {{
            Write-Log "Error - Setting NTP-Parameters type on $Env:COMPUTERNAME is Failed!!"
            throw $_
        }}
        #returning status
        return $executionStatus
    }}

    function Set-NTPServerDetails {{
        [CmdLetBinding()]
        param([string[]] $TimeServers)

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        Write-Log "Checking $Env:COMPUTERNAME is DOMAIN CONTROLLER or DOMAIN COMPUTER"
        $domainServices = Get-Service -Name 'NTDS', 'DNS' -ErrorAction SilentlyContinue
        if ($domainServices) {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN CONTROLLER"
            $TimeServerSuffix = ',0x9 '
            Write-Log "Add $TimeServerSuffix as NTPDetails suffix on $Env:COMPUTERNAME"
        }}
        else {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN COMPUTER"
            $TimeServers = $env:USERDNSDOMAIN
            $TimeServerSuffix = ',0x1 '
            Write-Log "Add $TimeServerSuffix as NTPDetails suffix on $Env:COMPUTERNAME"
        }}
        $executionStatus = $false
        try {{
            [string] $ServerValue = $null

            Write-Log "Setting NTP-Parameter Type on $Env:COMPUTERNAME"

            foreach ($Server in $TimeServers) {{
                $ServerValue += $Server + $TimeServerSuffix
            }}

            Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name NTPServer -Value $ServerValue.Trim()
            Write-Log "NTP-Parameter Type on setting to $ServerValue on $Env:COMPUTERNAME"
            $executionStatus = $true
        }}
        catch {{
            Write-Log "Error - Setting NTPDetails on $Env:COMPUTERNAME is Failed!!"
            throw $_
        }}
        #returning status
        return $executionStatus
    }}

    function Test-NTPServerDetails {{
        [CmdLetBinding()]
        param(
            [string[]] $TimeServers
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $flag = $false

        Write-Log "Getting NTP Server Details"
        $ntpServers = Get-NTPServerDetails
        Write-Log "Checking $Env:COMPUTERNAME is DOMAIN CONTROLLER or DOMAIN COMPUTER"
        $domainServices = Get-Service -Name 'NTDS', 'DNS' -ErrorAction SilentlyContinue
        if (-not $domainServices) {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN COMPUTER"
            $TimeServers = $env:USERDNSDOMAIN
            Write-Log "Set NTP Server on $Env:COMPUTERNAME is $TimeServers "
        }}
        if ($ntpServers) {{

            Write-Log "NTP Server is available on $Env:COMPUTERNAME"
            foreach ($Server in $TimeServers) {{
                $flag = $true
                if (-not $ntpServers.Contains($Server)) {{
                    Write-Log "NTP Server is not available on $Env:COMPUTERNAME"
                    $flag = $false
                    break
                }}
            }}
        }}
        return $flag
    }}

    function Test-NTPParameterType {{

        [CmdLetBinding()]
        Param()

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        Write-Log "Getting NTP Parameter Type"
        $ntpParameter = Get-NTPParameterType

        Write-Log "Checking $Env:COMPUTERNAME is DOMAIN CONTROLLER or DOMAIN COMPUTER"
        $domainServices = Get-Service -Name 'NTDS', 'DNS' -ErrorAction SilentlyContinue
        $flag = $false
        if ($domainServices) {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN COMPUTER"

            if ($ntpParameter -eq 'NTP') {{
                $flag = $true
                Write-Log "$Env:COMPUTERNAME - is Domain Controller"
            }}
        }}
        elseif ($ntpParameter -eq 'NT5DS') {{
            Write-Log "$Env:COMPUTERNAME is DOMAIN COMPUTER"
            $flag = $true
            Write-Log "$Env:COMPUTERNAME - is not a Domain Controller"
        }}
        else {{
            $flag = $false
        }}
        return $flag
    }}

    function Execute-NTPServerDetails {{
        param(
            [Int32] $Retries = 3,
            [string[]] $TimeServers
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $count = 0
        $returnCode = 2

        While (-not ($status = (Test-NTPParameterType) -and (Test-NTPServerDetails)) -and $count -lt $Retries) {{
            try {{
                Write-Log "NTPServerDetails not set as $TimeServers on $Env:COMPUTERNAME"
                Set-NTPParameterType -Verbose
                if (Set-NTPParameterType) {{
                    if (Test-NTPParameterType) {{
                        Write-Log "NTPParameterType type is not correct on $Env:COMPUTERNAME"
                        $executionStatus = Set-NTPServerDetails -TimeServers $TimeServers
                    }}
                }}
            }}
            catch {{
                $script = $_.InvocationInfo.ScriptName
                $lineNo = $_.InvocationInfo.ScriptLineNumber
                $line = $_.InvocationInfo.Line

                Write-Log "$script`nFailed at $lineNo`nFailed line $line`n$($_.Exception)`n"
                Write-Log $_

                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "Unknown error occurred"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            finally {{ $count ++ }}
        }}

        if ($status.Status -match $Action) {{
            $returnCode = 0

            if ($count -eq 0) {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is in desired state and no changes made"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            else {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix applied and recheck passed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
        }}
        else {{
            if ($executionStatus) {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix applied but recheck failed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            else {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix failed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
        }}

        Write-Verbose $message

        #return Service, Error Code, Message and FQDN
        @{{ProductService = $productService; ReturnCode = $returnCode; Message = $message; FQDN = $fqdn }}
    }}

    ##############################Execute-NTPServerDetails#####################################

    function Get-ServiceStatus {{
        [CmdLetBinding()]
        Param($ServiceName = "w32time")

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Getting $ServiceName service status  on $Env:COMPUTERNAME"
            $serviceStatus = Get-Service -Name $ServiceName -ErrorAction SilentlyContinue | select  -ExpandProperty Status
            Write-Log "$ServiceName service is $serviceStatus on $Env:COMPUTERNAME"
            $executionStatus = $true
        }}
        catch {{
            Write-Log "Error - Unable to get the $ServiceName service on $Env:COMPUTERNAME"
            throw $_
        }}
        #returning status object
        return $serviceStatus
    }}

    function Get-ServiceStartUpType {{
        [CmdLetBinding()]
        Param($ServiceName = "w32time")

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Getting $ServiceName service Start Up Type on $Env:COMPUTERNAME"
            $startTypeStatus = Get-WmiObject win32_Service -Filter "Name = '$ServiceName'" -ErrorAction SilentlyContinue | select -ExpandProperty StartMode
            Write-Log "$ServiceName Start UP Type is $startTypeStatus on $Env:COMPUTERNAME"
            $executionStatus = $true
        }}
        catch {{
            Write-Log "Error - Unable to get the $ServiceName ServiceStartUpType on $Env:COMPUTERNAME"
            throw $_
        }}
        #returning status object
        return $startTypeStatus
    }}


    function Set-ServiceStartupType {{
        [CmdLetBinding()]
        Param(
            [string] $StartupType = 'Automatic',
            [string] $ServiceName = 'w32time'
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Setting TimeSyncService Type $StartupType on $Env:COMPUTERNAME"
            Set-Service $ServiceName -StartupType $StartupType
            Write-Log "$ServiceName Start Up Time is set to $StartupType on $Env:COMPUTERNAME"
            $executionStatus = $true
        }}
        catch {{
            Write-Log "Error - Unable to set the $ServiceName ServiceStartUpType on $Env:COMPUTERNAME"
            throw $_
        }}
        #returning status
        return $executionStatus
    }}

    function Set-ServiceStatus {{
        [CmdLetBinding()]
        Param(
            [string] $ServiceName = 'w32time',
            [string] $ServiceStatus = 'Running'
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{

            Write-Log "Setting $ServiceName status to $serviceStatus on $Env:COMPUTERNAME"
            Set-Service $ServiceName -Status $serviceStatus
            Write-Log "$ServiceName status set to $serviceStatus on $Env:COMPUTERNAME"
            $executionStatus = $true
        }}
        catch {{
            Write-Log "Error - Unable to set the $ServiceName service on $Env:COMPUTERNAME"
            throw $_
        }}
        #returning status
        return $executionStatus
    }}

    function Test-Port {{
        [CmdLetBinding()]
        Param(
            [string[]] $TimeServers,
            [Int32] $Port = 80
        )

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        try {{
            Write-Log "Checking the connectivity from $Env:COMPUTERNAME to $TimeServers"
            return (New-Object System.Net.Sockets.TcpClient -ArgumentList $Timeservers, $Port).Connected
            Write-Log "$TimeServers is connected from $Env:COMPUTERNAME"
        }}
        catch {{
            Write-Log "Error - Unable to reach the $TimeServers server via $Port from $Env:COMPUTERNAME"
            return $false
        }}
        #returning status
        return $executionStatus
    }}


    function Compare-TimeServerTime {{
        [CmdLetBinding()]
        Param([Int32] $TimeDifSecs = 60, [Int32] $Port)

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        Write-Log "Checking $Env:COMPUTERNAME is Domain controller"
        $isCurrentMachineDomain = Get-Service -Name 'NTDS', 'DNS' -ErrorAction SilentlyContinue
        if (-not $isCurrentMachineDomain) {{
            Write-Log "Getting Domain member node's DOMAIN-CONTROLLER details on $Env:COMPUTERNAME"
            $timeServer = $env:USERDNSDOMAIN
            Write-Log "DOMAIN-CONTROLLER of $Env:COMPUTERNAME is $env:USERDNSDOMAIN"
        }}
        else {{
            #Get From Registry and verify the first reachable
            Write-Log "Getting NTP Server details on $Env:COMPUTERNAME"
            $NTPServerDetails = Get-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name NTPServer | select -ExpandProperty NtpServer
            Write-Log "NTP Server details on $Env:COMPUTERNAME are $NTPServerDetails"

            Write-Log "Collecting all NTP Server details on $Env:COMPUTERNAME"
            $timeServerArr = $NTPServerDetails.Split(" ") | Where {{ $_ }}
            foreach ($ts in $timeServerArr) {{
                if (Test-Port -TimeServers ($ts.Split(',')[0]) -Port $Port) {{
                    $timeServer = $ts.Split(',')[0]
                }}
            }}
        }}
        Write-Log "Checking first reachable NTP Server from $timeServer list on $Env:COMPUTERNAME"
        if ($timeServer) {{
            Write-Log "Fetching current Time Difference of $timeServer from $Env:COMPUTERNAME"
            $timeDif = w32tm /stripchart /computer:$timeServer /dataonly /samples:2 | select -Last 1
            [int32] $timeServerTime = $timeDif.Split(' ')[1].Trim('-').Trim('+').Split('.')[0]
            Write-Log "Current Time & Date in $timeServer is $timeServerTime"
            if ($timeServerTime -le $TimeDifSecs) {{
                Write-Log "Comparing current Time & Date on $timeServer and $Env:COMPUTERNAME"
                return $true
            }}
            else{{
                return $false
            }}
        }}
        else {{
                $false
        }}
    }}

    function Set-TimeSync {{

        [CmdLetBinding()]
        Param()

        Write-Log "Executing $($MyInvocation.MyCommand.Name)"

        $executionStatus = $false
        try {{
            Write-Log "Restarting and Sync TimeSyncService on $Env:COMPUTERNAME"
            net stop w32time; net start w32time
            sleep 15
            Write-Log "Checking $Env:COMPUTERNAME is Domain controller"
            $isCurrentMachineDomain = Get-Service -Name 'NTDS', 'DNS' -ErrorAction SilentlyContinue
            if (-not $isCurrentMachineDomain) {{
                Write-Log "$Env:COMPUTERNAME is not a DOMAIN CONTROLLER"
                $timeServer = $env:USERDNSDOMAIN
                w32tm /resync /force
                Write-Log "Wait for 5 Secs on $Env:COMPUTERNAME"
                sleep 5
                net time \$timeServer /set /y
            }}
            else {{
                Write-Log "$Env:COMPUTERNAME is a DOMAIN CONTROLLER"
                Write-Log "Getting NTP Server details on $Env:COMPUTERNAME"
                $NTPServerDetails = Get-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name NTPServer | select -ExpandProperty NtpServer
                Write-Log "Collecting all NTP Server details on $Env:COMPUTERNAME"
                $timeServerArr = $NTPServerDetails.Split(" ") | Where {{ $_ }}
                foreach ($ts in $timeServerArr) {{
                    if (Test-Port -TimeServers ($ts.Split(',')[0]) -Port $Port) {{
                        $timeServer = $ts.Split(',')[0]
                    }}
                }}
                w32tm /resync /force
                Write-Log "Wait for 120 Secs on $Env:COMPUTERNAME"
                sleep 120
                net time \$timeServer /set /y
            }}
            if ($LASTEXITCODE -eq 0) {{
                Write-Log "Restarting and Sync TimeSyncService on $Env:COMPUTERNAME is completed successfully"
                $executionStatus = $true
            }}
        }}
        catch {{
            Write-Log "Error - Unable to get the Time Sync status on $Env:COMPUTERNAME"
            throw $_
        }}
        #returning status
        return $executionStatus
    }}


    function Execute-TimeSync {{
        Param(
            [string] $ServiceName = 'w32time',
            [string] $StartupType = 'Automatic',
            [string] $ServiceStatus = 'Running',
            [Int32] $TimeDifMins = 1,
            [Int32] $Port,
            [Int32] $Retries = 1
        )
        Write-Log "Executing $($MyInvocation.MyCommand.Name)"
        $count = 0
        $returnCode = 2

        Write-Log "Checking $Env:COMPUTERNAME time is already synced with it's NTP Server"
        While (-not($iSTimeSynced = ("$StartupType" -match (Get-ServiceStartupType) -and (Get-ServiceStatus) -eq "$ServiceStatus" -and (Compare-TimeServerTime -Port $Port))) -and $count -lt $Retries) {{
            try {{
                Write-Log "Time isn't Synched state, Checking NTP Server StartUp Type on $Env:COMPUTERNAME"
                $currentStartUpType = Get-ServiceStartUpType -Verbose
                Write-Log "ServerStartUpType on $Env:COMPUTERNAME is $startUpType"
                if ($currentStartUpType -notmatch 'Auto') {{
                    Write-Log "$ServiceName service is on $currentStartUpType state, setting $ServiceName Startup Type to $StartupType on $Env:COMPUTERNAME"
                    Set-ServiceStartupType -ServiceName $ServiceName -StartupType $StartupType -Verbose | out-Null
                    Write-Log "$ServiceName Startup Type set to $StartupType on $Env:COMPUTERNAME"
                }}
                Write-Log "Gettting $ServiceName status on $Env:COMPUTERNAME"
                $status = Get-ServiceStatus -ServiceName $ServiceName -Verbose
                Write-Log "$ServiceName status is $status on $Env:COMPUTERNAME"
                if ($status -ne $ServiceStatus) {{
                    Write-Log "$ServiceName is not $ServiceStatus on $Env:COMPUTERNAME, Settting $ServiceName status to $ServiceStatus"
                    Set-ServiceStatus -ServiceName $ServiceName -ServiceStatus $ServiceStatus -Verbose | out-Null
                    Write-Log "$ServiceName service status set to $ServiceStatus on $Env:COMPUTERNAME"
                    Write-Log "Restarting $ServiceName service on $Env:COMPUTERNAME"
                    Set-TimeSync -Verbose | out-Null
                    Write-Log "$ServiceName service is restarted on $Env:COMPUTERNAME"
                }}
                else {{
                    Write-Log "Restarting $ServiceName service on $Env:COMPUTERNAME"
                    $executionStatus = Set-TimeSync
                    Write-Log "$ServiceName service is restarted on $Env:COMPUTERNAME"
                }}
            }}
            catch {{
                $script = $_.InvocationInfo.ScriptName
                $lineNo = $_.InvocationInfo.ScriptLineNumber
                $line = $_.InvocationInfo.Line

                Write-Log "$script`nFailed at $lineNo`nFailed line $line`n$($_.Exception)`n"
                Write-Log $_

                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "Unknown error occurred"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            finally {{ $count ++ }}
        }}

        if ($status.Status -match $Action) {{
            $returnCode = 0

            if ($count -eq 0) {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is in desired state and no changes made"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            else {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix applied and recheck passed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
        }}
        else {{
            if ($executionStatus) {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix applied but recheck failed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
            else {{
                $productService = "Product__IntelliSpace__TimeSync__Service__Status"
                $message = "The system is found to be not in desired state. Fix failed"
                $fqdn = [Net.Dns]::GetHostByAddress("{addr}").Hostname.tolower()
            }}
        }}

        Write-Verbose $message

        #return Service, Error Code, Message and FQDN
        @{{ProductService = $productService; ReturnCode = $returnCode; Message = $message; FQDN = $fqdn }}
    }}

    ##############################Execute-TimeSync#####################################


    function Write-Log {{
        [CmdLetBinding()]
        param([string] $Message, [string] $Path = $path)

        $time = Get-Date -Format "dd-MM-yyyy HH:mm:ss"
        $logTime = Get-Date -Format "dd-MM-yyyy"
        Write-Verbose "$time - $Message"
        "$time - $Message" | Out-File "$path\TimeSync_$logTime.log" -Append
    }}


    if (-not [Environment]::GetEnvironmentVariable("SOLUTION_ROOT")) {{
        $drive = 'C:'
        if (Test-Path -Path 'S:') {{
            $drive = 'S:'
        }}
        [Environment]::SetEnvironmentVariable("SOLUTION_ROOT", "$drive\Philips\Apps\iSite", 'Machine')
        [Environment]::SetEnvironmentVariable("SOLUTION_ROOT", "$drive\Philips\Apps\iSite")
    }}

    $path = "$Env:SOLUTION_ROOT\logs\TimeSync"

    If (-not (Test-path $path)) {{
        New-Item -ItemType Directory -Force -Path $path
    }}

    Get-ChildItem -Path "$Path\TimeSync*" -Recurse | Where-Object {{ ($_.LastWriteTime -lt (Get-Date).AddDays(-15)) }} | Remove-Item -Force

    $VMToolsPathx64 = 'C:\Program Files\VMware\VMware Tools\VMwareToolboxCmd.exe'
    $VMToolsPathx86 = 'C:\Program Files (x86)\VMware\VMware Tools\VMwareToolboxCmd.exe'
    if (Test-Path $VMToolsPathx64) {{
        $VMToolsPath = $VMToolsPathx64
    }}
    elseif (Test-Path $VMToolsPathx86) {{
        $VMToolsPath = $VMToolsPathx86
    }}
    else {{
        throw "$VMToolsPath not Available"
        Write-Log "Error - $VMToolsPath not Available"
    }}

    ForEach ($item in @("Execute-VMESXiTimeSync", "Execute-NTPServerDetails", "Execute-TimeSync")) {{
        $StatusObject = Invoke-Expression "$item -Retries 1 -TimeServers $TimeServers -TimeDifSecs 60 -Port 80"
        if ($StatusObject.ReturnCode -eq 2) {{
            Write-Log "Error - Failed to apply the fix on $item"
            break
        }}
    }}

    [string] $CurrentTime = Get-Date
    [string] $CurrentTimeZone = Get-WmiObject -Class win32_timezone | select -ExpandProperty StandardName
    [string] $NTPSource = Get-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\services\W32Time\Parameters -Name NTPServer | select -ExpandProperty NtpServer


    $json = @"
        {{
        "service": "$($StatusObject.ProductService)",
        "return_code": $($StatusObject.ReturnCode),
        "output": "{{'message': '$($StatusObject.Message)','DateTime': '$CurrentTime','TimeZome': '$CurrentTimeZone','NTPSource': '$NTPSource','TriggerId': '$TriggerId'}}",
        "host": "$($StatusObject.FQDN)"
        }}
"@
    Write-Log ("Posting json " + $json + " to url " + $URL)
    if($URL){{Invoke-WebRequestPost -URL $URL -json $json}}
    else{{return $json}}
    Write-Log ("Script Execution completed")
}}
$job = Start-Job -ScriptBlock $timesyncscript
Start-Sleep -Seconds 300
return "JobName: " + $job.Name + " Job Status: " + $job.State
'''


PDC_ROLE_OWNER_QUERY = '''
$context = new-object System.DirectoryServices.ActiveDirectory.DirectoryContext("Domain", "{domain}")
$domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetDomain($context)
Write-Host $domain.pdcRoleOwner.Name.tolower()
'''
