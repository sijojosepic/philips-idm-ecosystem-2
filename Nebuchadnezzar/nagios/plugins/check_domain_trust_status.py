#!/usr/bin/env python
######################################################################
# Shinken Plugin - Monitor Domain trust relationship using WinRM.
# The aim of this plugin is to monitor the one way outgoing domain
# trust between hospital domain and ISPACS domain. Also it supports
# to monitor trust between parent and child domains(ISPACS and ISEE)
# Plugin uses WinRM, to execute PowerShell scripts
# Plugin exit with OK or CRITICAL status.
# CRITICAL Status will be triggered
#   1) When the domain trust verification is failed.
#   2) When the incorrect credentials or trusted, trusting domain name
#      is supplied.
######################################################################

from __future__ import print_function
import sys
import argparse
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Monitor Windows service')
    parser.add_argument('-H', '--host', required=True, help='The hostname of the server')
    parser.add_argument('-u', '--username', required=True, help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-p', '--password', required=True, help='The password for the server')
    parser.add_argument('-d', '--trusteddomainname', required=True, help='The trusted domain name')
    parser.add_argument('-ud', '--trusteddomainuser', required=True, help='The user(eg `TRUSTEDDOMAIN\\user`) account '
                                                                          'to use to make the connection with the '
                                                                          'trusted domain')
    parser.add_argument('-pd', '--trusteddomainpassword', required=True, help='The password of the trusted domain user')
    parser.add_argument('-td', '--trustingdomainname', required=False, help='The trusting domain name')
    parser.add_argument('-uo', '--trustingdomainuser', required=False, help='The user(eg `TRUSTINGDOMAIN\\user`)account'
                                                                            ' to use to make the connection with the'
                                                                            ' trusting domain')
    parser.add_argument('-po', '--trustingdomainpassword', required=False, help='The password of the trusting'
                                                                                ' domain user')
    parser.add_argument('-t', '--trusttype', required=False, help='The trust type, whether one way trust(eg `one_way`)'
                                                                  ' or two way trust(eg `two_way`)', default='two_way')
    results = parser.parse_args(args)
    return (results.host, results.username, results.password, results.trusteddomainname, results.trusteddomainuser,
            results.trusteddomainpassword, results.trustingdomainname, results.trustingdomainuser,
            results.trustingdomainpassword, results.trusttype)


def get_one_way_trust_script():
    """ powershell script to get the one way outgoing trust status between trusted domain and trusting domain"""
    one_way_trust_script = r"""$remotecontext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', '{0}', '{1}', '{2}')
     $remoteForest = [System.DirectoryServices.ActiveDirectory.Forest]::getForest($remotecontext)
     $localcontext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Forest', '{3}', '{4}', '{5}')
     $localForest = [system.DirectoryServices.ActiveDirectory.Forest]::GetForest($localcontext)
     $localForest.VerifyTrustRelationship($remoteForest, 'Outbound')"""
    return one_way_trust_script


def get_two_way_trust_script():
    """ powershell script to get the one way outgoing trust status between trusted domain and trusting domain"""
    two_way_trust_script = r"""$remotecontext = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext('Domain', '{0}', '{1}', '{2}')
     $remoteForest = [System.DirectoryServices.ActiveDirectory.Domain]::getDomain($remotecontext)
     $localForest = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
     $localForest.VerifyTrustRelationship($remoteForest,'Bidirectional')"""
    return two_way_trust_script


def parse_err_string(std_err):
    try:
        # test_str = 'Exception : this part of the message need to be captured.\n"\nAt line: text after this discarded'
        # test_str.split(': ')[1].replace('\n', '').split('At line')[0].replace('"', '') =>
        # this part of the message need to be captured.
        return std_err.split(': ')[1].replace('\n', '').split('At line')[0].replace('"', '')
    except IndexError:
        return 'CRITICAL: Error while verifying the domain trust relationship'


def format_err(std_err):
    if 'the user name or password \nis incorrect.' in std_err.lower():
        return 'CRITICAL: The credentials of trusted or trusting domain is incorrect'
    elif 'the specified forest does \nnot exist or cannot be contacted.' in std_err.lower() \
            or 'the rpc server is unavailable' in std_err.lower() or 'the server is not operational' in std_err.lower():
        return 'CRITICAL: The trust cannot be validate for the following reasons: ' \
               'The trusted or trusting domain could not be contacted or does not exist.' \
               'The trust verification failed between trusted or trusting domain.'
    return parse_err_string(std_err)


def process_output(out_put):
    if out_put.status_code == OK:
        if out_put.std_out == '':
            return compose_status_msg(out_put)
    return CRITICAL, format_err(out_put.std_err)


def compose_status_msg(out_put):
    status, msg = OK, ''
    if out_put.std_out == '':
        msg = 'The trust has been validated. It is in place and active.'
    return status, msg


def get_trust_type(*args):
    if args[-1] == 'one_way':
        return get_one_way_trust_script().format(args[0], args[1], args[2], args[3], args[4], args[5])
    else:
        return get_two_way_trust_script().format(args[0], args[1], args[2])


def main(host, username, password, trusteddomainname, trusteddomainuser,
         trusteddomainpassword, trustingdomainname, trustingdomainuser,
         trustingdomainpassword, trusttype):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        ps_script = get_trust_type(trusteddomainname, trusteddomainuser, trusteddomainpassword, trustingdomainname,
                                   trustingdomainuser, trustingdomainpassword, trusttype)
        out_put = win_rm.execute_ps_script(ps_script)
        status, msg = process_output(out_put)
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : WinRM Error {0}'.format(e)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to the specified domain node(s) from the node - {0}'.format(host)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(host)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_arg())
