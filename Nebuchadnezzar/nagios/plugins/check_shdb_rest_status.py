#!/usr/bin/env python
# This script gets the logstash status along with the version
# if the version is retrieved, displays it with status OK
# else the status displayed is CRITICAL

import requests
import argparse


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to check the status of logstash')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server')
    parser.add_argument('-P', '--proto', required=False, help='http or https', default='http')
    parser.add_argument('-p', '--port', required=True, help='Port to connect')
    parser.add_argument('-s', '--subpath', required=False, help='uri path', default='')
    parser.add_argument('-n', '--servicename', required=True, help='Name of Service to monitor')
    parser.add_argument('-t', '--timeout', required=False, help='timeout while fetching the version', default=10)
    results = parser.parse_args(args)

    return (results.hostname, results.proto, results.port, results.subpath, results.servicename, results.timeout)

def get_rest_status(hostname, proto, port, subpath, servicename, timeout):
    """get logstash details"""
    if not hostname:
        return 2, 'Could not get due to missing arguments'
    uri = '{proto}://{hostname}:{port}{subpath}'.format(proto=proto, hostname=hostname, port=port, subpath=subpath)
    try:
        headers = {
          'accept': 'application/v1+json',
        }
        res = requests.get(uri, headers=headers, verify=False, timeout=float(timeout))
        res.raise_for_status()
        version = res.json().get('version')
        status = 0
        outmsg = 'OK - {servicename} is running; version:{version}'.format(servicename=servicename, version=version)
    except Exception as e:
        outmsg = 'CRITICAL - {servicename} is not running'.format(servicename=servicename)
        status = 2
    return status, outmsg

def main():
    status, outmsg = get_rest_status(*check_arg())
    print(outmsg)
    exit(status)


if __name__ == '__main__':
    main()
