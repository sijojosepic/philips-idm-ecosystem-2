#!/usr/bin/env python
import winrm
import sys
import os
import argparse
import glob
import requests
from scanline.utilities.installing_package_status import connect, check_package


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script to install packages on windows machine.')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host name of the windows server')
    parser.add_argument('-a', '--hostaddress', default=None,
                        help='The hostaddress of the windows server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the windows server')
    parser.add_argument('-p', '--password', required=True,
                        help='The passowrd of the windows server')
    parser.add_argument('-d', '--download_script', default=None,
                        help='Download script path')
    parser.add_argument('-i', '--install_script', default=None,
                        help='Install script path')
    parser.add_argument('-r', '--repo_path', default="",
                        help='Remote path of the installable file')
    parser.add_argument('-R', '--download_path', default='C:\\Temp',
                        help='Windows server path to store files')
    parser.add_argument('-s', '--target_service_name', required=True,
                        help='Service name of the installable file')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.hostaddress,
        results.username,
        results.password,
        results.download_script,
        results.install_script,
        results.repo_path,
        results.download_path,
        results.target_service_name)


def read_files(download_script, install_script):
    download_content = None
    install_content = None
    if download_script:
        with open(download_script, 'r+') as data:
            download_content = data.read()
    if install_script:
        with open(install_script, 'r+') as data:
            install_content = data.read()
    return download_content, install_content


def upload_ps_scripts(conn, download_content, install_content, download_path,
                      download_script, install_script):
    download_file_name = download_script.split('/')[-1]
    install_file_name = install_script.split('/')[-1]
    download_cmd = "New-Item -path {0} -name {1} -type 'file' -value '{2}'"
    conn.run_ps("New-Item -path '{0}' -ItemType directory".format(download_path))
    if install_content:
        conn.run_ps(download_cmd.format(download_path, install_file_name,
                                    install_content))
    if download_content:
        conn.run_ps(download_cmd.format(download_path, download_file_name,
                                    download_content))


def download_package(conn, repo_path, download_path, download_script):
    download_package_cmd = '-HTTPFilePathOnNEBNode "{0}" -LocalFolderPath "{1}"'
    download_package_cmd = download_package_cmd.format(repo_path, download_path)
    try:
        download_file_name = download_script.split('/')[-1]
        cmd = ''.join([download_path, '\\', download_file_name, ' ',
                       download_package_cmd])
        conn.run_ps(cmd)
    except TypeError:
        pass
    except Exception:
        print 'CRITICAL: Could not download package.'
        sys.exit(2)


def install_package(conn, username, password, domain, hostaddress, repo_path,
                download_path, install_script, target_service_name):
    neb_folder_path = '/'.join(repo_path.split('/')[:-1]) + '/'
    package_name = repo_path.split('/')[-1]
    download_path = download_path.replace('/', '\\')
    cmd = ' -UserName "{0}" -UserPassword "{1}" -UserDomain "{2}" ' \
          '-HostName "{3}" -HTTPMSIFolderPathOnNEBNode "{4}" ' \
          '-MSIFullPathOnLocalMachine "{5}\\{6}"'.format(username,
                                                        password,
                                                        domain,
                                                        hostaddress,
                                                        neb_folder_path,
                                                        download_path,
                                                        package_name)
    install_file_name = install_script.split('/')[-1]
    cmd = ''.join([download_path, '\\', install_file_name, cmd])
    try:
        conn.run_ps(cmd)
    except Exception:
        pass


def delete_partial_files(conn, download_path):
    cmd = 'Remove-Item "{0}" -Force  -Recurse -ErrorAction SilentlyContinue'
    cmd = cmd.format(download_path)
    conn.run_ps(cmd)


def get_neb_ip_address():
    """
    This function returns the Neb IP address
    :return: Neb IP address type str
    """
    cmd = "/sbin/ifconfig eth0 | /bin/awk '/inet addr/ { print substr($2, 6)}'"
    output = os.popen(cmd)
    ip_address = output.read().strip()
    return ip_address

def get_pcm_package():
    """
    This function returns the latest msi package file from PCM directory.
    directory: /var/philips/repo/Tools/PCM/packages/PCM/
    :return: package file type str
    """
    try:
        pcm_package_files = glob.glob('/var/philips/repo/Tools/PCM/packages/PCM/*.msi')
        pcm_package = max(pcm_package_files, key=os.path.getctime)
        return os.path.basename(pcm_package)
    except Exception as ex:
        return None


def main():
    hostname, hostaddress, user_with_domain, password, download_script,\
    install_script, repo_path, download_path, target_service_name = check_args()
    user_with_domain=user_with_domain.replace('\\\\', '\\')
    out_msg = 'OK: Package {0} installation started.'.format(target_service_name)
    conn = connect(hostname, user_with_domain, password)
    status_code, msg = check_package(conn, target_service_name, hostname)
    pcm_package = get_pcm_package()
    if not pcm_package:
        out_msg = 'OK: Could not find the PCM Package on this Neb.'
    username = user_with_domain.split('\\')[-1]
    domain = user_with_domain.split('\\')[0]
    if not status_code:
        print out_msg
        sys.exit(0)
    if status_code != 0:
        ip_address = get_neb_ip_address()
        # Building the actual repo path with correct IP and package file
        repo_path = repo_path.format(ip_address=ip_address, pcm_package=pcm_package)
        delete_partial_files(conn, download_path)
        download_content, install_content = read_files(download_script,
                                                       install_script)
        upload_ps_scripts(conn, download_content, install_content,
                          download_path, download_script, install_script)
        download_package(conn, repo_path, download_path, download_script)
        install_package(conn, username, password, domain, hostaddress,
                        repo_path, download_path, install_script, target_service_name)
    print out_msg
    sys.exit(0)


if __name__ == '__main__':
    main()
