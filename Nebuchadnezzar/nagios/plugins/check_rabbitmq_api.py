#!/usr/bin/env python

import argparse
import sys
import math
import json
import requests
from requests.exceptions import RequestException, HTTPError, ConnectTimeout
from ast import literal_eval

from scanline.product.isp import ISPProductScanner, ISPDBConfiguration
from scanline.utilities.back_office import BackOffice
from scanline.utilities.cache import SafeCache

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

CONNECTION = {'warning_msg': "Warning threshold breached for total number of connection."
                             "(Total Connections-{0}, Threshold-{1})\n",
              'critical_msg': "Critical threshold breached for total number of connection."
                              "(Total Connections-{0}, Threshold-{1})\n"}
CHANNELS = {'warning_msg': "Warning threshold breached for total number of channels."
                           "(Total Channels-{0}, Threshold-{1})\n",
            'critical_msg': "Critical threshold breached for total number of channels."
                            "(Total Channels-{0}, Threshold-{1})\n"}
CONSUMERS = {'warning_msg': "Warning threshold breached for total number of consumer."
                            "(Total Consumers-{0}, Threshold-{1})\n",
             'critical_msg': "Critical threshold breached for total number of consumer."
                             "(Total Consumers-{0}, Threshold-{1})\n"}
UNACKNOWLEDGED_MSG = {'warning_msg': "Warning threshold breached for total unacknowledged messages."
                                     "(Total Unacknowledged Messages-{0}, Threshold-{1})\n",
                      'critical_msg': "Critical threshold breached for total unacknowledged messages."
                                      "(Total Unacknowledged Messages-{0}, Threshold-{1})\n"}

MEM_ALARM_MSG = "Critical: Memory alarm is true for node: {0}\n"
DISK_ALARM_MSG = "Critical: Disk Alarm is true for node: {0}\n"
PARTITION_MSG = "Critical: Partition detected for nodes: {0} and partitions: {1}\n"


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of RabbitMQ Services')
    parser.add_argument('-S', '--site_id', required=True,
                        help='The Site ID')
    parser.add_argument('-V', '--vig_url', required=True,
                        help='The Vigilant URL')
    parser.add_argument('-I', '--isite_address', required=True,
                        help='Entry point for iSiteWeb')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the rabbitmq server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the rabbitmq server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-r', '--protocol', default='https',
                        help='The protocol of the rabbitmq server ')
    parser.add_argument('-a', '--api', required=True,
                        help='The rabbitmq API')
    parser.add_argument('-t', '--req_timeout', type=int, default=15,
                        help='The request timeout.')
    parser.add_argument('-w', '--warning', default='',
                        help='The overview warning thresholds.')
    parser.add_argument('-c', '--critical', default='',
                        help='The overview critical thresholds.')
    parser.add_argument('-n', '--nodes', default='',
                        help='Total number of nodes in RBQ env.')
    parser.add_argument('-d', '--default_queue', default='',
                        help='Default queue details for monitoring RabbitMQ queues')
    parser.add_argument('-q', '--queues', default='',
                        help='Customized queue details for monitoring RabbitMQ queues')
    results = parser.parse_args(args)
    return (
        results.site_id,
        results.vig_url,
        results.isite_address,
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.protocol,
        results.api,
        results.req_timeout,
        results.warning,
        results.critical,
        results.nodes,
        results.default_queue,
        results.queues,
    )


class Cache(object):

    def read_cache(self, key):
        return SafeCache.do('get', key)

    def write_cache(self, key, value, timeout):
        SafeCache.do('setex', key, value, timeout)


class HostsInfo(object):
    HOSTS_LEN_KEY = 'hosts_len'
    CACHE_TTL = 60 * 60

    def __init__(self, site_id, vig_base_url, isite_server):
        self.site_id = site_id
        self.isite_server = isite_server
        self.vig_base_url = vig_base_url
        self._hosts_len = None
        self.cache = Cache()
        self._bo = None
        self._isp_module = None

    @property
    def bo(self):
        if not self._bo:
            self._bo = BackOffice(self.vig_base_url, self.site_id)
        return self._bo

    @property
    def isp_module(self):
        if not self._isp_module and self.isite_server:
            self._isp_module = ISPProductScanner.create_isp(self.isite_server)
        return self._isp_module

    def from_pacs(self):
        if self.isp_module:
            return len(self.isp_module.get_hosts() or [])

    def from_backoffice(self):
        hosts = []
        response = self.bo.hosts_with_mod_type()
        if response:
            hosts_info = response.json()
            for item in hosts_info['result']:
                ISP = item.get('ISP') or {}
                m_t = ISP.get('module_type')
                if m_t and m_t not in ISPDBConfiguration.EXTENDEDNODES:
                    hosts.append(item['hostname'])
        return len(hosts)

    def from_cache(self, key):
        val = self.cache.read_cache(key)
        return int(val) if val else 0

    def get_hosts_len(self):
        host_len = self.from_cache(self.HOSTS_LEN_KEY)
        if not host_len:
            host_len = self.from_pacs() or self.from_backoffice()
            self.cache.write_cache(self.HOSTS_LEN_KEY, host_len,
                                   self.CACHE_TTL)
        return host_len

    @property
    def num_core_nodes(self):
        # Number of UDM core nodes
        if not self._hosts_len:
            self._hosts_len = self.get_hosts_len()
        return self._hosts_len


def get_url(hostname, port, protocol, api):
    return '{0}://{1}:{2}/api/{3}'.format(protocol, hostname, port, api)


def get_api_response(url, username, password, req_timeout):
    headers = {
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
        'connection': 'keep-alive',
        'content-type': 'application/json',
        'upgrade-insecure-requests': '1',
    }
    auth = (username, password)
    response = requests.get(url, headers=headers, auth=auth,
                            timeout=req_timeout, verify=False)
    response.raise_for_status()
    return json.loads(response.content)


def do_request(hostname, subpath, port, protocol, username, password, req_timeout):
    url = get_url(hostname, port, protocol, subpath)
    return get_api_response(url, username, password, req_timeout)


def get_status_data(hostname, subpath, port, protocol, username, password, req_timeout):
    json_response = do_request(hostname, subpath, port, protocol, username, password, req_timeout)
    return json_response


def get_threshold_values(thresold_string):
    return thresold_string.split(',')


def check_critical_threshold(total_count, crit_threshold, parameter):
    critical_msg = ''
    if int(total_count) >= int(crit_threshold):
        critical_msg = parameter['critical_msg'].format(total_count, crit_threshold)
    return critical_msg


def check_warning_threshold(total_count, warn_threshold, crit_threshold, parameter):
    warning_msg = ''
    if int(crit_threshold) > int(total_count) >= int(warn_threshold):
        warning_msg = parameter['warning_msg'].format(total_count, warn_threshold)
    return warning_msg


def get_overview_status(response, overview_warn, overview_crit, nodes):
    connection_warn, channel_warn, consumer_warn, unack_msg_warn = get_threshold_values(
        overview_warn)
    connection_criti, channel_criti, consumer_criti, unack_msg_criti = get_threshold_values(
        overview_crit)
    nodes = int(nodes)
    unack_msg_criti, unack_msg_warn = int(unack_msg_criti), int(unack_msg_warn)
    connection_warn, channel_warn, consumer_warn = int(connection_warn) * nodes, int(
        channel_warn) * nodes, int(consumer_warn) * nodes
    connection_criti, channel_criti, consumer_criti = int(connection_criti) * nodes, int(channel_criti) * nodes, int(
        consumer_criti) * nodes
    connections_count = response.get('object_totals').get('connections', 0)
    channels_count = response.get('object_totals').get('channels', 0)
    consumers_count = response.get('object_totals').get('consumers', 0)
    unacknowledged_msg_count = response.get('queue_totals').get('messages_unacknowledged', 0)
    state, msg = OK, 'OK: Total number of connections-{0}, channels-{1}, consumers-{2}, unacknowledged messages-{3} ' \
                     'and the number of nodes used for threshold calculation - {4}'\
        .format(connections_count, channels_count, consumers_count, unacknowledged_msg_count, nodes)
    critical_msg = check_critical_threshold(connections_count, connection_criti, CONNECTION)
    warning_msg = check_warning_threshold(connections_count, connection_warn, connection_criti, CONNECTION)
    critical_msg += check_critical_threshold(channels_count, channel_criti, CHANNELS)
    warning_msg += check_warning_threshold(channels_count, channel_warn, channel_criti, CHANNELS)
    critical_msg += check_critical_threshold(consumers_count, consumer_criti, CONSUMERS)
    warning_msg += check_warning_threshold(consumers_count, consumer_warn, consumer_criti, CONSUMERS)
    critical_msg += check_critical_threshold(unacknowledged_msg_count, unack_msg_criti, UNACKNOWLEDGED_MSG)
    warning_msg += check_warning_threshold(unacknowledged_msg_count, unack_msg_warn, unack_msg_criti,
                                           UNACKNOWLEDGED_MSG)
    messages = ''
    if critical_msg:
        messages = 'CRITICAL : ' + critical_msg
        state = CRITICAL
        msg = messages
    if warning_msg:
        messages = messages + 'WARNING : ' + warning_msg
        if not critical_msg:
            state = WARNING
        msg = messages
    return state, msg


def get_nodes_threshold(threshold):
    return threshold.split(',')


def convert_size(size_bytes):
    """
    Converts the byte size into KB, MB, GB and so on
    It also checks and return if size_bytes fall under KB or MB or GB
    Exp:
    If byte sizes are: 1500, 1500000, 1500000000
    Respected value will be returned: 1.46 KB , 1.43 MB, 1.4 GB
    """
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    try:
        index = int(math.floor(math.log(size_bytes, 1024)))
        pow = math.pow(1024, index)
        size = round(size_bytes / pow, 2)
    except ValueError:
        return "0B"
    return "%s %s" % (size, size_name[index])


def get_memory_used_response(data, key, mem_used_crt, mem_used_warn):
    critical, warning, msg = False, False, ''
    data_value_per = (float(data.get(key)) / data.get('mem_limit')) * 100
    if data_value_per >= int(mem_used_crt):
        critical = True
        msg = "Critical threshold breached for memory used-node: {node}, (Total-{total}%, Threshold-{threshold}%)\n".format(
            node=str(data.get('name')), total=round(data_value_per, 2), threshold=mem_used_crt)
    elif data_value_per >= int(mem_used_warn):
        warning = True
        msg = "Warning threshold breached for memory used-node: {node}(Total-{total}%, Threshold-{threshold}%)\n".format(
            node=str(data.get('name')), total=round(data_value_per, 2), threshold=mem_used_warn)
    return critical, warning, msg


def get_disk_free_response(data, key, disk_free_crt, disk_free_warn):
    critical, warning, msg = False, False, ''
    data_value = data.get(key)
    crt_bytes = int(disk_free_crt) * 1024 * 1024 * 1024
    warn_bytes = int(disk_free_warn) * 1024 * 1024 * 1024
    if data_value <= crt_bytes:
        critical = True
        msg = "Critical threshold breached for disk free space-node: {node}(Total-{total}, Threshold-{threshold}GB)\n".format(
            node=str(data.get('name')), total=convert_size(data_value), threshold=disk_free_crt)
    elif data_value <= warn_bytes:
        warning = True
        msg = "Warning threshold breached for disk free space-node: {node}(Total-{total}, Threshold-{threshold}GB)\n".format(
            node=str(data.get('name')), total=convert_size(data_value), threshold=disk_free_warn)
    return critical, warning, msg


def get_fd_used_response(data, key, fd_used_crt, fd_used_warn):
    critical, warning, msg = False, False, ''
    data_value = data.get(key)
    if data_value >= int(fd_used_crt):
        critical = True
        msg = "Critical threshold breached for file descriptors used-node: {node}(Total-{total}, Threshold-{threshold})\n".format(
            node=str(data.get('name')), total=data_value, threshold=fd_used_crt)
    elif data_value >= int(fd_used_warn):
        warning = True
        msg = "Warning threshold breached for file descriptors used-node: {node}(Total-{total}, Threshold-{threshold})\n".format(
            node=str(data.get('name')), total=data_value, threshold=fd_used_warn)
    return critical, warning, msg


def get_alarm_status(data):
    alarm_msg = ''
    if data.get('mem_alarm'):
        alarm_msg += MEM_ALARM_MSG.format(str(data.get('name')))
    if data.get('disk_free_alarm'):
        alarm_msg += DISK_ALARM_MSG.format(str(data.get('name')))
    if data.get('partitions'):
        alarm_msg += PARTITION_MSG.format(str(data.get('name')), str(data.get('partitions')))
    return alarm_msg


def get_nodes_result(data, nwarining, ncritical):
    mem_used_warn, disk_free_warn, fd_used_warn = get_nodes_threshold(nwarining)
    mem_used_crt, disk_free_crt, fd_used_crt = get_nodes_threshold(ncritical)
    node_msg = ''
    mem_critical, mem_warning, mem_msg = get_memory_used_response(data, 'mem_used', mem_used_crt, mem_used_warn)
    node_msg += mem_msg
    disk_critical, disk_warning, disk_msg = get_disk_free_response(data, 'disk_free', disk_free_crt, disk_free_warn)
    node_msg += disk_msg
    fd_critical, fd_warning, fd_msg = get_fd_used_response(data, 'fd_used', fd_used_crt, fd_used_warn)
    node_msg += fd_msg
    alarm_msg = get_alarm_status(data)
    alarm_critical = True if alarm_msg else False
    node_msg += alarm_msg
    return (mem_critical or disk_critical or fd_critical or alarm_critical), (
                mem_warning or disk_warning or fd_warning), node_msg


def get_nodes_details(config_json, nwarining, ncritical):
    final_message = ''
    final_crt, final_war = False, False
    for data in config_json:
        if not (data.get('mem_used') or data.get('disk_free') or data.get('fd_used')):
            continue
        nodes_critical, nodes_warning, nodes_msg = get_nodes_result(data, nwarining, ncritical)
        if nodes_critical:
            final_crt = True
            final_message += nodes_msg
        elif nodes_warning:
            final_war = True
            final_message += nodes_msg
    if not (final_crt or final_war):
        final_message = "All Nodes are OK state"
    return final_crt, final_war, final_message


def get_nodes_status(config_json, nwarining, ncritical):
    final_critical, final_warning, final_message = get_nodes_details(config_json, nwarining, ncritical)
    if final_critical:
        return CRITICAL, final_message
    elif final_warning:
        return WARNING, final_message
    return OK, final_message


class QueueStatus(object):
    def __init__(self, api_response, warning_thld, critical_thld, default_queues, custom_queues):
        self.warning = int(warning_thld)
        self.critical = int(critical_thld)
        self.default_q_str = default_queues
        self.custom_q_str = custom_queues
        self.mapping = {}
        self.api_response = api_response
        self.status_dct = {'CRITICAL': [], 'WARNING': []}
        self.Msgs = QueueMsgs
        self.sts_set = set([OK])
        self.form_queue_mapping()
        self.q_len = len(api_response)

    def format_queue(self, str_queues):
        # "['Q1,34,50']" -> ['Q1,34,50']
        queues = literal_eval(str_queues)
        return queues if isinstance(queues, list) else []

    def form_queue_mapping(self):
        queues = self.format_queue(self.default_q_str)
        queues.extend(self.format_queue(self.custom_q_str))
        for item in queues:
            # 'Q1,34,50'
            name, warn, crit = item.split(',')
            self.mapping.update({name: {'WAR': int(warn), 'CRT': int(crit)}})

    def thresholds(self, q_name):
        if q_name in self.mapping:
            warning = self.mapping[q_name]['WAR']
            critical = self.mapping[q_name]['CRT']
        else:
            warning, critical = self.warning, self.critical
        return warning, critical

    def compose_status(self):
        for queue in self.api_response:
            msg_len = queue.get('messages') or 0
            warn_thrld, crt_thrld = self.thresholds(queue['name'])
            if msg_len >= crt_thrld:
                msg = self.Msgs.CRITICAL.format(
                    queue['name'], msg_len, warn_thrld, crt_thrld)
                self.status_dct['CRITICAL'].append(msg)
            elif msg_len >= warn_thrld:
                msg = self.Msgs.WARNING.format(
                    queue['name'], msg_len, warn_thrld, crt_thrld)
                self.status_dct['WARNING'].append(msg)

    def st_msg(self, crt_msg, warn_msg):
        final_msg = crt_msg + warn_msg
        if not final_msg:
            if self.q_len == 0:
                final_msg = self.Msgs.NO_Q
            elif self.q_len == 1:
                final_msg = self.Msgs.ONE_Q
            else:
                final_msg = self.Msgs.OK.format(self.q_len)
        return final_msg

    def get_status(self):
        crt_msg, warn_msg = '', ''
        self.compose_status()
        if self.status_dct['CRITICAL']:
            crt_msg = self.Msgs.C_PRFX + \
                      ''.join(self.status_dct['CRITICAL'])
            self.sts_set.add(CRITICAL)
        if self.status_dct['WARNING']:
            warn_msg = self.Msgs.W_PRFX + \
                       ''.join(self.status_dct['WARNING'])
            self.sts_set.add(WARNING)
        final_msg = self.st_msg(crt_msg, warn_msg)
        return max(self.sts_set), final_msg


class QueueMsgs(object):
    CRITICAL = 'Messages in {0} queue has breached the critical threshold (actual - {1}, Thresholds: warning-{2}, ' \
               'critical-{3}),\n'
    WARNING = 'Messages in {0} queue has breached the warning threshold (actual - {1}, Thresholds: warning-{2}, ' \
              'critical-{3}),\n'
    ONE_Q = 'OK :: Found one Queue, which has not breached the threshold defined for number of messages'
    NO_Q = 'No Queues Found'
    OK = 'OK :: Found {0} Queues, None of them has breached the threshold defined for number of messages'
    C_PRFX = 'CRITICAL:: '
    W_PRFX = 'WARNING:: '


def check_status(site_id, vig_url, isite_address, hostname, port, username, password, protocol, api, req_timeout,
                 warning, critical, nodes, default_queues, custom_queues):
    try:
        api_resonse = get_status_data(hostname, api, port, protocol, username, password, req_timeout)
        if api == 'overview':
            obj = HostsInfo(site_id, vig_url, isite_address)
            if not obj.num_core_nodes:
                return CRITICAL, 'Unable to retrieve UDM core nodes information'
            nodes = obj.num_core_nodes + int(nodes)
            return get_overview_status(api_resonse, warning, critical, nodes)
        elif api == 'queues':
            obj = QueueStatus(api_resonse, warning, critical,
                              default_queues, custom_queues)
            return obj.get_status()
        elif api == 'nodes':
            return get_nodes_status(api_resonse, warning, critical)
        return UNKNOWN, 'UNKNOWN API - {0}'.format(api)
    except HTTPError as e:
        return UNKNOWN, 'UNKNOWN : HTTPError - {0}'.format(e)
    except ConnectTimeout as e:
        return UNKNOWN, 'UNKNOWN : Connection Error - RabbitMQ server is not accessible'
    except RequestException as e:
        return UNKNOWN, 'UNKNOWN : Request Error {0}'.format(e)
    except Exception as e:
        return CRITICAL, 'CRITICAL : Error while retrieving response data {0}'.format(e)


def main():
    state, msg = check_status(*check_args())
    print(msg)
    sys.exit(state)


if __name__ == '__main__':
    main()
