#!/usr/bin/env python

import sys
import requests
import argparse

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check message count of individual Queue')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address to post to')
    parser.add_argument('-P', '--port', required=True,
                        help='The port to post to')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the rabbitmq server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the rabbitmq server')
    parser.add_argument('-q', '--queue_name', required=True,
                        help='The queue_name of the rabbitmq server to be created by IDM')
    parser.add_argument('-w', '--warning', required=True,
                        help='The threshold value for warning status')
    parser.add_argument('-c', '--critical', required=True,
                        help="The threshold value for critical status")
    parser.add_argument('-pf', '--perfdata',
                        help="The performance data input")
    results = parser.parse_args(args)
    return (results.hostname,
            results.port,
            results.username,
            results.password,
            results.queue_name,
            results.warning,
            results.critical)

def get_queue_message_count(hostname,port,username,password,queue_name,warning,critical):
    try:
        url = 'https://{0}:{1}/api/queues/%2F/{2}'.format(hostname, port,
                                                              queue_name)
        auth = (username,password)
        response = requests.get(url,auth=auth,verify=False)
        data = response.json()
        message_count = data['messages']
        status_message = 'Queue message count {0}|message count={0};{1};{2};1;100'
        if message_count >= int(warning) and message_count < int(critical):
            return 1, status_message.format(message_count,int(warning),int(critical))
        elif message_count >= int(critical):
            return 2, status_message.format(message_count,int(warning),int(critical))
        else:
            return 0, status_message.format(message_count,int(warning),int(critical))
    except:
        return 2, 'Could not connect to Queue {0}'.format(queue_name)

def main():
    status, message = get_queue_message_count(*check_arg())
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()