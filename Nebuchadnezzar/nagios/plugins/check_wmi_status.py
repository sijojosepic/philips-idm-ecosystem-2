#!/usr/bin/env python
from __future__ import print_function
import argparse
import sys
from scanline.utilities.win_rm import WinRM
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server')
    results = parser.parse_args(args)
    return results.hostname, results.username, results.password


def wmi_status(hostname, username, password):
    """Get OS details and authentication check"""
    try:
        state, msg = 2, "CRITICAL : The WMI query had problems. WMI repository might be corrupted."
        win_rm = WinRM(hostname, username, password)
        out_put = win_rm.execute_ps_script('(Get-WmiObject -class Win32_OperatingSystem).Caption')
        if out_put.std_out:
            state, msg = 0, "OK : " + out_put.std_out
    except AuthenticationError as e:
        state, msg = 2, "CRITICAL : Authentication Error. You might have your username/password wrong or the user's " \
                        "access level is too low"
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        state, msg = 2, 'CRITICAL : WinRM Error {0}'.format(e)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            state, msg = 2, 'CRITICAL : Issue in connecting to node - {0}'.format(hostname)
        else:
            state, msg = 2, 'CRITICAL : Typeerror {0}'.format(e)
    except Exception as e:
        state, msg = 2, 'CRITICAL : Exception {0}'.format(e)
    return state, msg


def main():
    state, output = wmi_status(*check_arg())
    print(output, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()
