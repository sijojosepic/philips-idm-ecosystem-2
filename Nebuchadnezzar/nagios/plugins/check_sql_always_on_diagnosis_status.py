#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

from __future__ import print_function
import sys
import argparse
import pymssql
import datetime
from scanline.utilities import dbutils

QUERY_GET_LOG = """
     EXEC master.dbo.xp_readerrorlog {log_file}, 1, {query_string}, NULL, {start_date}, {end_date}, N'desc'
     SELECT 'FLAG'"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-T', '--logsnap', required=True, help='Log Snap duration', default=6)
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.logsnap, results.vigilant_url)


def get_connection(hostname, username, password, database):
    """ This method returns the connection object."""
    try:
        conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
        return conn
    except Exception as e:
        return False


def log_result_head(log_file_count, cursor):
    """ This method returns the detected error code from logs."""
    start_date = datetime.datetime.now() - datetime.timedelta(days=1)
    end_date = datetime.datetime.now()
    start_date = "'" + str(start_date.replace(microsecond=0)) + "'"
    end_date = "'" + str(end_date.replace(microsecond=0)) + "'"
    cursor.execute(
        QUERY_GET_LOG.format(log_file=log_file_count, query_string="N'Error: 19407'", start_date=start_date,
                             end_date=end_date))
    return cursor.fetchone()


def log_result_detail(log_file_count, cursor, logsnap, result):
    """ This method returns details of detected error code from logs."""
    start_date = result[0] - datetime.timedelta(seconds=int(logsnap) / 2)
    end_date = result[0] + datetime.timedelta(seconds=int(logsnap) / 2)
    start_date = "'" + str(start_date.replace(microsecond=0)) + "'"
    end_date = "'" + str(end_date.replace(microsecond=0)) + "'"
    cursor.execute(
        QUERY_GET_LOG.format(log_file=log_file_count, query_string='NULL', start_date=start_date,
                             end_date=end_date))
    return cursor.fetchall()


def get_formatted_data(cursor, logsnap):
    """ This method returns formatted message with service status for diagnosis."""
    for log_file_count in range(3):
        result = log_result_head(log_file_count, cursor)
        if result and result[0] != 'FLAG':
            log_result = log_result_detail(log_file_count, cursor, logsnap, result)
            if log_result:
                outmsg = "WARNING: Cluster Database diagnosed unhealthy :\nError : 19407\n"
                for log in log_result:
                    outmsg = outmsg + '{log_datetime} : {description}\n'.format(log_datetime=log[0],
                                                                                description=log[2].strip())
                    return 1, outmsg + '\n For more information, please check SQL Server Logs.'

    status, outmsg = 0, "OK: Internal Database health is OK."
    return status, outmsg


def get_diagnosis_status(hostname, username, password, database, logsnap, vigilant_url):
    """ This method returns the diagnosis status for SQL Always On."""
    conn = get_connection(hostname, username, password, database)
    if not conn:
        node = dbutils.get_cluster_primary_db(hostname, username, password, database, vigilant_url)
        conn = get_connection(node, username, password, database)
    if not conn:
        status, outmsg = 2, 'CRITICAL: Cannot connect to database due to an incorrect username/password, ' \
                            'or service check could not resolve db nodes.'
        return status, outmsg
    cursor = conn.cursor()
    return get_formatted_data(cursor, logsnap)


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_diagnosis_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()
