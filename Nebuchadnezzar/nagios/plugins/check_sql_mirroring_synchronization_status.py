#!/usr/bin/env python
# This script should always exit 0
# it may display problem information as output
# problems with this script are meant to be determined by the system expecting the post

from __future__ import print_function
import sys
import argparse
import pymssql
from scanline.utilities import dbutils

SYNC_QUERY = """select instance_name,cntr_value from sys.dm_os_performance_counters PC
      where object_name like '%database mirroring%'
      and counter_name in ('log send queue kb','Redo Queue Kb')
      and instance_name in (select db_name(dbm.database_id) from master.sys.database_mirroring dbm 
      where dbm.mirroring_role_desc in ('Principal','Mirror'))
      and PC.cntr_value >={counterval}
      order by cntr_value desc"""

SYNC_HEALTH_QUERY = """select name,dm.mirroring_state_desc from  sys.databases as dbs
        inner join sys.database_mirroring as dm on dbs.database_id = dm.database_id
        where mirroring_state <=1 or mirroring_state=5"""


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to send a message via HTTP post')
    parser.add_argument('-H', '--hostname', required=True, help='The address of the server originating the message')
    parser.add_argument('-U', '--username', required=True, help='Username of the server')
    parser.add_argument('-P', '--password', required=True, help='Password of the server', default=2)
    parser.add_argument('-D', '--database', required=True, help='Database name for sql query', default='')
    parser.add_argument('-C', '--counterval', required=True, help='', default=102400)
    parser.add_argument('-V', '--vigilant_url', required=True, help='The vigilant URl')
    results = parser.parse_args(args)

    return (results.hostname, results.username, results.password, results.database, results.counterval, results.vigilant_url)


def get_formatted_data(db_hosts, sync_result, counterval, sync_health_result):
    """ This method returns formatted message with service status for synchronization."""
    critical_backlog_msg = 'CRITICAL: Detected slow data synchronization between {node}. Current backlog {current_val}MB has reached' \
                           ' threshold of {counterval}MB. This could cause data loss on mirror node.'
    critical_health_msg = 'CRITICAL: Problem in data synchronization. Synchronization issues found in {databases} databases.'
    status, outmsg = 0, 'OK : Data synchronization is healthy'
    db_nodes = db_hosts[0] + ' and ' + db_hosts[1]
    if sync_result:
        if sync_result[0]:
            status, outmsg = 2, critical_backlog_msg.format(
                node=db_nodes, counterval=int(counterval) / 1024, current_val=(sync_result[0][1]) / 1024)
        if sync_result[1]:
            status, outmsg = 2, critical_backlog_msg.format(
                node=db_nodes, counterval=int(counterval) / 1024, current_val=(sync_result[1][1]) / 1024)
        if sync_result[0] and sync_result[1]:
            status, outmsg = 2, critical_backlog_msg.format(
                node=db_nodes, counterval=int(counterval) / 1024,
                current_val=max([sync_result[0][1], sync_result[1][1]]) / 1024)
    if sync_health_result:
        status, outmsg = 2, critical_health_msg.format(databases=str(sync_health_result).strip('[]'))
    return status, outmsg


def get_db_mirroring_synchronization_status(hostname, username, password, database, counterval, vigilant_url):
    """ This method returns the DB Mirroring synchronization status."""
    db_hosts = filter(lambda x: hostname in x, dbutils.get_dbnodes(vigilant_url))
    if db_hosts:
        db_hosts = db_hosts[0]
    else:
        db_hosts = dbutils.get_dbpartner_in_dbscanner(hostname, vigilant_url)

    db_len = len(db_hosts)
    if db_len == 1:
        status, outmsg = 0, 'OK - This environment contains single DB node.'
        return status, outmsg
    elif db_len == 0:
        status, outmsg = 2, 'CRITICAL - Unable to retrieve DB nodes due to connection issue with vigilant or DB nodes not reachable.'
        return status, outmsg
    elif db_len > 1:
        if not dbutils.get_mirroring_primary_check(hostname=hostname, username=username, password=password,
                                                   database=database):
            status, outmsg = 0, 'OK: This service check only applicable for primary node. {node} is not the primary node.'.format(
                node=hostname)
            return status, outmsg

    sync_result = []
    sync_health_result = []
    con_err, conn_err_msg = [], ''
    for host in db_hosts:
        try:
            conn = pymssql.connect(server=host, user=username, password=password, database=database, login_timeout=10)
        except Exception as e:
            con_err.append(host)
            continue
        cursor = conn.cursor()
        cursor.execute(SYNC_QUERY.format(counterval=counterval))
        sync_result.append(cursor.fetchone())
        cursor.execute(SYNC_HEALTH_QUERY)
        sync_health_result.extend([i[0].encode('utf-8') + '(' + i[1].encode('utf-8') + ')' for i in cursor.fetchall()])
        conn.close()

    if con_err:
        status, outmsg = 2, 'CRITICAL - Connection error with {node}.'.format(node=str(con_err).strip('[]'))
        return status, outmsg
    return get_formatted_data(db_hosts, sync_result, counterval, list(set(sync_health_result)))


def main():
    cmd_args = check_arg()
    try:
        status, msg = get_db_mirroring_synchronization_status(*cmd_args)
    except Exception as err:
        if "permission was denied" in str(err):
            status, msg = 2, "CRITICAL - SQL access permissions denied, access level low for '{0}' user.".format(cmd_args[1])
        else:
            status, msg = 2, "CRITICAL - {0}.".format(str(err))
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main()
