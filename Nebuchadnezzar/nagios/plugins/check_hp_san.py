#! /usr/bin/env python

#################################################################################
#
# Plugin name : check_hp_san.py
# Author      : manoj.chandrankutty@philips.com
# Date        : 5 Dec 2017
# Usage       : ./check_hp_san.py -H hostname -u username -p password
#               -c 'show controllers' -x 'controllers'
# Description : This plugin is aimed to have a better monitoring of HP SAN.
#               The original plugin was too verbose and was not giving the
#               response in the correct format.
#
#               Paramaters monitored are
#                   1. show controllers
#                   2. show disks
#                   3. show volumes
#                   4. show power-supplies
#
#               The plugin takes the usual paramters like hostname, username,
#               password and along with this there are 2 additional parameters.
#               They are the command and xpath.
#               command : The command to be executed using SSH.
#               xpath   : Upon executing the command, the response is in an xml
#                         format. The xpath query should be for the nodes with a
#                         basetype="controllers" attribute. In this case it wou
#                         ld be "controllers"
# Updated by : sijo.jose@philips.com
# Provided proper messaging format for controllers, disks, volumes and
# power-supplies
#
################################################################################


import sys
import argparse
import xml.etree.ElementTree as ET
import shlex

from subprocess import Popen
from subprocess import PIPE as pp


XPATH_STR = ".//OBJECT[@basetype='{0}']/PROPERTY[@name='health']"

supplies_template = {'singular': 'Power Supply', 'plural': 'Power Supplies'}
supplies_template['req_attrs'] = ('health', 'health-reason', 'position')
supplies_template['part_msg'] = 'Power Supply at the position - {0},'
supplies_template['part_attr'] = 'position'


drives_template = {'singular': 'Disk', 'plural': 'Disks'}
drives_template['req_attrs'] = ('health', 'health-reason', 'location')
drives_template['part_msg'] = 'In Enclousure {0} - Slot {1}, disk '
drives_template['part_attr'] = 'location'


controller_template = {'singular': 'Controller', 'plural': 'Controllers'}
controller_template['req_attrs'] = ('health', 'health-reason', 'position')
controller_template['part_msg'] = 'Controller at the position - {0},'
controller_template['part_attr'] = 'position'


volume_template = {'singular': 'Volume', 'plural': 'Volumes'}
volume_template['req_attrs'] = ('health', 'health-reason', 'volume-name')
volume_template['part_msg'] = 'Volume with name - {0},'
volume_template['part_attr'] = 'volume-name'

SAN_TEMPLATE = {'critical_msg': 'CRITICAL - Out of {0}, {1} {2} Failed'}
SAN_TEMPLATE['status_msg'] = ('\n{cnt}. {part_msg}'
                              'failed with Status - {health},'
                              'Health Reason - {health_reason}')
SAN_TEMPLATE['power-supplies'] = supplies_template
SAN_TEMPLATE['drives'] = drives_template
SAN_TEMPLATE['controllers'] = controller_template
SAN_TEMPLATE['volumes'] = volume_template
SAN_TEMPLATE['succes_msg'] = 'OK: HP SAN - {0} health is bright and sunny, found {1} {2}'


def get_part_msg(part_msg, san_type, item, attr):
    if san_type == 'drives':
        enclosure, slot = item[attr].split('.')  # location 1.12
        return part_msg.format(enclosure, slot)
    return part_msg.format(item[attr])


def plural_or_singular(item_count):
    return 'plural' if item_count > 1 else 'singular'


def get_critical_msg(san_type, template, err_list, total_count):
    type_msg = template[plural_or_singular(len(err_list))]
    return SAN_TEMPLATE['critical_msg'].format(total_count,
                                               len(err_list),
                                               type_msg)


def compose_err_status(san_type, err_list, total_count, template):
    err_msg = get_critical_msg(san_type, template, err_list, total_count)
    cnt = 0
    for item in err_list:
        cnt += 1
        part_msg = get_part_msg(template['part_msg'], san_type, item,
                                template['part_attr'])
        err_msg += SAN_TEMPLATE['status_msg'].format(cnt=cnt, part_msg=part_msg,
                                                     health=item['health'],
                                                     health_reason=item['health-reason'])
    return err_msg


def get_xpath_str(replace_str):
    return XPATH_STR.format(replace_str)


def cleanup_xml(in_xml):
    in_list = in_xml.splitlines()
    for ctr in range(2):
        in_list.pop(0)
    in_list.pop()
    return ''.join(in_list)

# works like butter in 2.7 but :( in 2.6
# def ok_status(elem_list):
#     return all([1 if elem.text == 'OK' else 0 for elem in elem_list])


def ok_status(root, xpath):
    unhealthy_list = []
    item_count = 0
    o_nodes = root.findall('OBJECT')
    for o_node in o_nodes:
        if o_node.attrib.get('basetype') == xpath:
            item_count += 1
            p_nodes = o_node.findall('PROPERTY')
            for p_node in p_nodes:
                if p_node.attrib.get('name') == 'health':
                    if p_node.text != 'OK':
                        unhealthy_list.append(o_node)
    return (len(unhealthy_list) == 0, unhealthy_list, item_count)


# works like butter in 2.7 but :( on 2.6
# def get_formatted_status(command, parent_list):
#     def node_text(el, name):
#         return el.findall(".//PROPERTY[@name='"+ name  +"']")[0].text
#
#     def formatted_response(parent_list):
#         unhealthy =  [el for el in parent_list if node_text(el, 'health') != 'OK']
#         response = []
#         for el in unhealthy:
#             response.append('Health Status  : ' + node_text(el, 'health'))
#             response.append('Serial No.     : ' + node_text(el, 'serial-number'))
#             response.append('Reason         : ' + node_text(el, 'health-reason'))
#             response.append('Recommendation : ' + node_text(el, 'health-recommendation'))
#             return '\n'.join(response)
#
#     return 2, formatted_response(parent_list)


def get_formatted_status(san_type, err_items, total_count, template):
    err_list = []
    for item in err_items:
        status_dict = {}
        for node in item.findall('PROPERTY'):
            node_name = node.attrib.get('name')
            if node_name in template['req_attrs']:
                status_dict[node_name] = node.text or ''
        err_list.append(status_dict)
    return compose_err_status(san_type, err_list, total_count, template)


def success_msg(san_type, total_count):
    item_type = SAN_TEMPLATE[san_type][plural_or_singular(total_count)]
    return SAN_TEMPLATE['succes_msg'].format(san_type, total_count, item_type)


def get_san_status(hostname, username, password, command, xpath):
    try:
        pw_list = [password + '!', password]

        for pw in pw_list:
            cmd = 'sshpass -p ' + pw + ' ssh -o StrictHostKeyChecking=no ' + \
                username + '@' + hostname
            p_out = Popen(shlex.split(cmd) + [command], stdout=pp, stderr=pp)
            xml_out, _ = p_out.communicate()
            if xml_out:
                break
            xml_out = ''

        if xml_out == '':
            return 2, 'Unable to connect to SAN. Invalid credentials.'

        tree = ET.ElementTree(ET.fromstring(cleanup_xml(xml_out)))
        root = tree.getroot()

        status, unhealthy_list, total_count = ok_status(root, xpath)
        if not status:
            return 2, get_formatted_status(xpath, unhealthy_list, total_count, SAN_TEMPLATE[xpath])
        return 0, success_msg(xpath, total_count)
    except Exception as ex:
        return 2, 'HP SAN - Critical error. ' + str(ex)


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Advanced HP SAN monitoring')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of HP SAN.')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of HP SAN.')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of HP SAN.')
    parser.add_argument('-c', '--command', default='show controllers',
                        help='The command to be executed on HP SAN.')
    parser.add_argument('-x', '--xpath', default='controllers',
                        help='The id of xpath query for the XML returned on SSH.')
    results = parser.parse_args(args)

    return (
        results.hostname,
        results.username,
        results.password,
        results.command,
        results.xpath
    )


def main():
    state, msg = get_san_status(*check_args())
    print(msg)
    sys.exit(state)


if __name__ == "__main__":
    main()
