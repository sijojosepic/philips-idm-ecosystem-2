#!/usr/bin/env python

"""
This plugin is used to retrieve vcentre information based on property(disk/datastore) supplied.
1. disk information
example: ./check_vcenter_disk.py -H 172.19.38.200 -u 'administrator@vsphere.local' -p 'Nsah2damsj@Vc3' -w 80 -c 90 -s 'disk'
output:
OK: Path= S:\\, Capacity= 8.0GB, Free space= 1.4GB, Percentage used= 82.96
Path= D:\\, Capacity= 16.0GB, Free space= 8.0GB, Percentage used= 50.00

2. datastore information
example: ./check_vcenter_disk.py -H 172.19.38.200 -u 'administrator@vsphere.local' -p 'Nsah2damsj@Vc3' -w 80 -c 90 -s 'datastore'
output:
CRITICAL: There are 3/5 datastores which have crossed critical value of 93%
Name= Localstore70, Capacity= 5581.2GB, Free space= 208.9GB, Percentage used= 96.26
Name= Localstore71, Capacity= 5581.2GB, Free space= 368.5GB, Percentage used= 93.40
Name= IDM1_15k, Capacity= 16670.5GB, Free space= 837.4GB, Percentage used= 94.98
OK: There are 2/5 datastores which are under critical value of 93%
Name= Localstore15, Capacity= 30731.8GB, Free space= 22163.2GB, Percentage used= 27.88
Name= Localstore16, Capacity= 30731.8GB, Free space= 23458.6GB, Percentage used= 23.67
"""

from pyVim import connect
from pyVmomi import vim
import argparse
import atexit
import sys


DATASTORE_FACT = 'Name= {datastore_name}, Capacity= {datastore_capacity}GB, Free space= {datastore_freespace}GB, Percentage used= {percent_used}'
PERF_MSG = "'{datastore_name}'={used_space}GB;;{critical_value};{min_value};{max_value}"
EOL = '\n'
CRITICAL_MSG = 'CRITICAL: There are {critical_total}/{total} datastores which have crossed critical value of {critical_value}%'
OK_MSG = 'OK: There are {ok_total}/{total} datastores which are under critical value of {critical_value}%'


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get vCenter disk/datastore usage information')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The host address of the vCenter server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the vCenter server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the vCenter server')
    parser.add_argument('-w', '--warning_val', required=False,
                        help='The warning threshold for the disk usage')
    parser.add_argument('-c', '--critical_val',required=True,
                        help='The critical threshold for the disk usage/datastore usage')
    parser.add_argument('-s', '--property', required=True,
                        help='The storage parameter(disk/datastore) to retrieve usage')
    results = parser.parse_args(args)

    return (results.hostname,
            results.username,
            results.password,
            results.warning_val,
            results.critical_val,
            results.property)


def sizeof_fmt(num):
    for item in ['bytes', 'KB', 'MB', 'GB']:
        if num < 1024.0:
            return "%3.1f%s" % (num, item)
        num /= 1024.0
    return "%3.1f%s" % (num, 'TB')


def convert_to_gb(numofbytes):
    gb = float(numofbytes)/float(1024*1024*1024)
    return "%3.1f" % (gb)


def server_connect(hostname, username, password):
    try:
        connection = connect.SmartConnect(host=hostname, user=username, pwd=password)
        atexit.register(connect.Disconnect, connection)
        return connection
    except IOError:
        return 2,  'Could not connect to vCenter server.'
    except vim.fault.InvalidLogin:
        return 2, 'Cannot complete login due to an incorrect user name or password.'


def calculate_percentage(free_space, capacity):
    res = 100.00 - (float(free_space)/float(capacity))*100
    return format(res, '.2f')


def disk_info(disks, warning_val, critical_val):
    disk_fact = ''
    perf_msg = ''
    perct_lst = []
    for disk in disks:
        percent_used = calculate_percentage(disk.freeSpace, disk.capacity)
        disk_fact += 'Path= ' + disk.diskPath + ', ' + 'Capacity= ' + sizeof_fmt(disk.capacity) + ', ' + 'Free space= '\
                     + sizeof_fmt(disk.freeSpace) + ', ' + 'Percentage used= ' + percent_used + '\n'
        perf_msg += "'{0}'={1}%;{2};{3};\n".format(disk.diskPath, percent_used, warning_val, critical_val)
        perct_lst.append(percent_used)
    return perct_lst, disk_fact, perf_msg


def datastore_info(ds_obj_list, critical_value):
    perf_msg = ''
    ok_msgs = []
    critical_msgs = []
    critical_perct = float(critical_value)/float(100)
    for ds in ds_obj_list:
        summary = ds.summary
        percent_used = calculate_percentage(summary.freeSpace, summary.capacity)
        datastore_fact = DATASTORE_FACT.format(datastore_name=summary.name, datastore_capacity=convert_to_gb(summary.capacity),
                                                datastore_freespace=convert_to_gb(summary.freeSpace), percent_used=percent_used) + EOL
        perf_msg += PERF_MSG.format(datastore_name=summary.name, used_space=convert_to_gb(summary.capacity-summary.freeSpace),
                                    critical_value=convert_to_gb(critical_perct*summary.capacity), min_value=0,
                                    max_value=convert_to_gb(summary.capacity)) + EOL
        if float(percent_used) > float(critical_value):
            critical_msgs.append(datastore_fact)
        else:
            ok_msgs.append(datastore_fact)
    return  ok_msgs, critical_msgs, perf_msg


def check_threshold(percent_used, check_val, other_val=0, critical=True):
    res = False
    for i in percent_used:
            if float(i) >= float(check_val) and float(i) < float(other_val):
                res = True
    if critical:
        for i in percent_used:
            if float(i) >= float(check_val):
                res = True
    return res


def get_status_datastore_usage(ok_msgs, critical_msgs, perf_msg, critical_val):
    state = 0
    msg = ''
    if len(critical_msgs) > 0:
        state = 2
        msg = msg + CRITICAL_MSG.format(critical_total=len(critical_msgs), total=len(critical_msgs) + len(ok_msgs),
                                        critical_value=critical_val) + EOL
        for crt_msg in critical_msgs:
            msg += crt_msg
    if len(ok_msgs) > 0:
        msg = msg + OK_MSG.format(ok_total=len(ok_msgs), total=len(critical_msgs) + len(ok_msgs),
                                critical_value=critical_val) + EOL
        for ok_msg in ok_msgs:
            msg += ok_msg
    return state, '{0} | {1}'.format(msg, perf_msg)


def get_status(percent_used, disk_fact, perf_msg, warning_val, critical_val):
    state = 0
    msg = 'OK: {0} | {1}'.format(disk_fact, perf_msg)
    if check_threshold(percent_used, critical_val):
        msg = 'CRITICAL: {0} | {1}'.format(disk_fact, perf_msg)
        state, msg = 2, msg
    elif check_threshold(percent_used, warning_val, other_val=critical_val, critical=False):
        msg = 'WARNING: {0} | {1}'.format(disk_fact, perf_msg)
        state, msg = 1, msg
    return state, msg


def main(hostname, username, password, warning_val, critical_val, property):
    state, msg = 0, ''
    try:
        server_instance = server_connect(hostname, username, password)
        if property == 'disk':
            virtual_machine_instance = server_instance.content.searchIndex.FindByIp(None, hostname, True)
            disks = virtual_machine_instance.guest.disk
            percent_used, disk_fact, perf_msg = disk_info(disks, warning_val, critical_val)
            state, msg = get_status(percent_used, disk_fact, perf_msg, warning_val, critical_val)
        elif property == 'datastore':
            content = server_instance.RetrieveContent()
            container = content.viewManager.CreateContainerView(content.rootFolder, [vim.Datastore], True)
            ds_obj_list = container.view
            ok_msgs, critical_msgs, perf_msg = datastore_info(ds_obj_list, critical_val)
            state, msg = get_status_datastore_usage(ok_msgs, critical_msgs, perf_msg, critical_val)
        print msg
        sys.exit(state)
    except Exception:
        print 'Cannot complete login due to an incorrect user name or password or Could not connect to vCenter server.'
        sys.exit(2)


if __name__ == '__main__':
    main(*check_arg())
