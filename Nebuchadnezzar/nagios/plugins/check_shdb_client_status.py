#!/usr/bin/env python


import argparse
from winrm.exceptions import (
    AuthenticationError, WinRMOperationTimeoutError,
    WinRMTransportError, WinRMError)
import redis
from scanline.utilities.win_rm import WinRM


OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Nagios Plugin to wrap a wmi command and check process status with version')
    parser.add_argument('-H', '--host', required=True, help='The address of the server')
    parser.add_argument('-U', '--username', required=True, help='The user name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-E', '--process', required=True, help='The service name on the server')
    parser.add_argument('-S', '--versionpath', required=False, help='The path of version(ver) file')
    results = parser.parse_args(args)
    return (results.host,
            results.username, results.password, results.process, results.versionpath)


def get_process_status(host, username, password, process, versionpath):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        output = win_rm.execute_cmd('tasklist /FO CSV')
        process_name = process
        if process_name in output.std_out:
            status = OK
            msg = msg + 'OK - {process} is running'.format(process = process)
            if versionpath:
                version_output = win_rm.execute_cmd('type',[versionpath])
                msg = msg + ', version {version}'.format(version = version_output.std_out)
        else:
            msg = 'CRITICAL - {process} is not running'.format(process=process)
    except (AuthenticationError, WinRMOperationTimeoutError,
            WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e)
    return status, msg



def is_shdb_configured():
    try:
        redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)
        return redis_con.sismember('scanners', 'SHDB')
    except:
        return False

def main():
    status, msg = OK, 'OK - SHDB node is not configured'
    if is_shdb_configured():
        status, msg = get_process_status(*check_arg())
    print msg
    exit(status)


if __name__ == '__main__':
    main()
