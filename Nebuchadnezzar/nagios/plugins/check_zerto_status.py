#!/usr/bin/env python

# This plugin will fetch all the alerts provided by ZVM using the API call,
# And provide the alert on the basis of alertId.

# Hostaddress, Username, Password, Category and Previous State parameters are mandatory.
# Subcategory is mandatory, when category is 'alerts'.
# URL and Port parameters are optional, In case not provided it will use default port and url.

# Redis Cache : In order to avoid this duplicate API calls, the caching layer got introduced with 4mns TTL (TimeToLive)
# as the default service frequency is 10mns.
# This cache can be disabled by passing the argument --enable_cache with value as 'no'.
# During retries or non OK previous service status cached data would not be used, rather the information would be
# fetched directly from the API.
# For storing the data in the cache the key will be composed using the following convention:'<hostaddress>_zerto_alerts'

# API calls:
# 1. https://zvmip:port/v1/alerts
# 2. https://zvmip:port/v1/vpgs
# Zerto Alert Monitoring Examples
# ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'VRA' -X 'OK'


import argparse
import sys
import json
from scanline.utilities.cache import SafeCache
from scanline.product.disaster_recovery.zerto_wrapper import IDMZerto


OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

# Dictionary to get alert specific OK message and to get alert identifiers to fetch specific alerts.
VRA_DISK_ALERTS_LIST = ['VRA0006', 'VRA0008', 'VRA0009', 'VRA0010', 'VRA0011', 'VRA0012', 'VRA0013', 'VRA0014',
                        'VRA0015', 'VRA0016', 'VRA0017', 'VRA0018', 'VRA0019', 'VRA0021', 'VRA0022', 'VRA0026',
                        'VRA0027', 'VRA0052', 'VRA0053']
VRA_HOST_ALERTS_LIST = ['VRA0001', 'VRA0003', 'VRA0049', 'VRA0050']
VRA_HEALTH_ALERTS_LIST = ['VRA0020', 'VRA0023', 'VRA0024', 'VRA0028', 'VRA0029', 'VRA0032', 'VRA0033', 'VRA0055',
                          'VRA0056']
VRA_NETWORK_ALERTS_LIST = ['VRA0025', 'VRA0002', 'VRA0004', 'VRA0005', 'VRA0037', 'VRA0038']
VRA_JOURNAL_ALERTS_LIST = ['VRA0030', 'VRA0039', 'VRA0040', 'VRA0054', 'VRA0007']
LICENSE_ALERTS_LIST = ['LIC0001', 'LIC0002', 'LIC0003', 'LIC0004', 'LIC0006', 'LIC0007', 'LIC0009', 'LIC0010',
                       'LIC0011']
ALERT_DETAILS_DICT = {'RPO': {'ok_msg': 'OK: Average RPO value is {0}', 'alert_ids': ['VPG0009', 'VPG0010']},
                      'LICENSE': {'ok_msg': 'OK: License is valid and applicable for Max. {0} Vms. \n '
                                            'Presently total protected VMs are {1}', 'alert_ids': LICENSE_ALERTS_LIST},
                      'DISK': {'ok_msg': 'OK: No VRA Disk alerts', 'alert_ids': VRA_DISK_ALERTS_LIST},
                      'HOST': {'ok_msg': 'OK: No VRA Host alerts', 'alert_ids': VRA_HOST_ALERTS_LIST},
                      'JOURNAL': {'ok_msg': 'OK: No VRA Journal alerts', 'alert_ids': VRA_JOURNAL_ALERTS_LIST},
                      'NETWORK': {'ok_msg': 'OK: No VRA Network alerts', 'alert_ids': VRA_NETWORK_ALERTS_LIST},
                      'HEALTH': {'ok_msg': 'OK:  No VRA alerts', 'alert_ids': VRA_HEALTH_ALERTS_LIST},
                      'OTHER': {'ok_msg': 'OK: No Alerts', 'alert_ids': []}}


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of RabbitMQ Services')
    parser.add_argument('-H', '--hostaddress', required=True,
                        help='The host address of the ZVM node')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the ZVM server')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the ZVM server')
    parser.add_argument('-P', '--port', default=9669,
                        help='The port of the ZVM server')
    parser.add_argument('-l', '--url', default=None,
                        help='The URL of the ZVM server')
    parser.add_argument('-s', '--category', required=True,
                        help='The category for the ZVM API call')
    parser.add_argument('-a', '--subcategory', default=None,
                        help='subcategory of the ZVM API')
    parser.add_argument('-C', '--enable_cache', default='yes',
                        help='Enable caching for ZVM Alert API response[Yes/No]')
    parser.add_argument('-T', '--cache_timeout', type=int, default=240,
                        help='Cache timeout.')
    parser.add_argument('-X', '--previous_state', required=True,
                        help='The service previous execution state OK, WARNING or CRITICAL')

    results = parser.parse_args(args)

    return (
        results.hostaddress,
        results.username,
        results.password,
        results.port,
        results.url,
        results.category,
        results.subcategory,
        results.enable_cache,
        results.cache_timeout,
        results.previous_state
    )


def read_cache(key):
    return SafeCache.do('get', key)


def write_cache(key, json_data, cache_timeout):
    SafeCache.do('setex', key, json.dumps(json_data), cache_timeout)


def get_zerto_obj(hostaddress, username, password, url, port):
    """
    This function will create the zerto API session and return the object for API calls.
    """
    zerto_obj = IDMZerto(hostaddress, username, password, url, port)
    return zerto_obj


def do_request(hostaddress, username, password, url, port, path):
    zerto_obj = get_zerto_obj(hostaddress, username, password, url, port)
    response = zerto_obj.get_request(path)
    return response.json()


def get_zerto_alerts(hostaddress, username, password, url, port, enable_cache, cache_timeout, previous_state):
    """
    This function will fetch the response for ZVM 'alerts' either from cache or by ZVM alerts API request.
    It reads and writes from and to cache, only when the enable_cache flag value is 'yes'.
    Same time if the previous service status is non OK, function will not use cached values.
    """
    if enable_cache.lower() == 'yes':
        key = "{0}_zerto_{1}".format(hostaddress, 'alerts')
        cache_data = read_cache(key)
        json_data = cache_data if cache_data else '[]'
        response = json.loads(json_data)
        if previous_state != 'OK' or not response:
            response = do_request(hostaddress, username, password, url, port, 'v1/alerts')
            write_cache(key, response, cache_timeout)
    else:
        response = do_request(hostaddress, username, password, url, port, 'v1/alerts')
    return response


def get_excluded_alertids():
    excluded_ids_list = []
    for category in ALERT_DETAILS_DICT:
        excluded_ids_list.extend(ALERT_DETAILS_DICT[category]['alert_ids'])
    return excluded_ids_list


def check_alert_status(hostaddress, username, password, port, url, sub_category, enable_cache, cache_timeout,
                       previous_state):
    """
        Get the Alert based on sub_category
        :sub_category - RPO, VRA
    """
    state, msg = CRITICAL, ''
    critical_msg_template = '{0} {1}: {2} \n'
    response = get_zerto_alerts(hostaddress, username, password, url, port, enable_cache, cache_timeout, previous_state)
    for alerts in response:
        # Other - Capturing the alerts which are not part of any service-checks
        if sub_category == 'OTHER':
            if alerts.get('HelpIdentifier') not in get_excluded_alertids():
                msg = critical_msg_template.format(msg, alerts.get('HelpIdentifier'), alerts.get('Description'))
        elif alerts.get('HelpIdentifier') in ALERT_DETAILS_DICT[sub_category]['alert_ids']:
            msg = critical_msg_template.format(msg, alerts.get('HelpIdentifier'), alerts.get('Description'))
    if not msg:
        state = OK
        # When there are not any RPO alerts, capturing present RPO
        if sub_category == 'RPO':
            msg = get_present_rpo(hostaddress, username, password, port, url, sub_category)
        # When there are not any license alerts, capturing license information
        elif sub_category == 'LICENSE':
            msg = get_license_details(hostaddress, username, password, port, url, sub_category)
        else:
            msg = ALERT_DETAILS_DICT[sub_category]['ok_msg']
    else:
        msg = 'CRITICAL: {0}'.format(msg)
    return state, msg


def get_present_rpo(hostaddress, username, password, port, url, sub_category):
    response = do_request(hostaddress, username, password, url, port, 'v1/vpgs')
    vpg_count = 0
    average_rpo = 0
    for vpg in response:
        average_rpo = average_rpo + int(vpg["ActualRPO"])
        vpg_count += 1
    average_rpo = average_rpo/vpg_count
    rpo_msg = ALERT_DETAILS_DICT[sub_category]['ok_msg'].format(average_rpo)
    # perf_msg for Thruk Graphical representation of RPO trends
    perf_msg = "'Average RPO '={0};\n".format(average_rpo)
    msg = '{0} | {1}'.format(rpo_msg, perf_msg)
    return msg


def get_license_details(hostaddress, username, password, port, url, sub_category):
    response = do_request(hostaddress, username, password, url, port, 'v1/license')
    license_msg = ALERT_DETAILS_DICT[sub_category]['ok_msg'].format(response["Details"]["MaxVms"],
                                                                    response["Usage"]["TotalVmsCount"])
    return license_msg


def check_zvm_status(hostaddress, username, password, url, port):
    zerto_obj = get_zerto_obj(hostaddress, username, password, url, port)
    if zerto_obj.is_protected:
        state, msg = OK, 'OK: ProtectedSite, all dependent service monitoring is enabled'
    else:
        status = zerto_obj.protectedsite_pairing_status()
        if status == 0:
            # 0 -> Paired, 1 -> Pairing, 2-> Unpaired
            state, msg = UNKNOWN, 'UNKNOWN: ProtectedSite pairing is successful, dependent service monitoring is ' \
                                  'disabled for the RecoverySite'
        else:
            state, msg = OK, 'OK: ProtectedSite pairing is unsuccessful - dependent service monitoring is enabled'
    return state, msg


def check_zerto_auth(hostaddress, username, password, url, port):
    zerto_obj = get_zerto_obj(hostaddress, username, password, url, port)
    get_session = zerto_obj.get_connection()
    state, msg = OK, 'OK: Zerto APIs are accessible'
    return state, msg


def check_status(hostaddress, username, password, port, url, category, subcategory, enable_cache, cache_timeout,
                 previous_state):
    try:
        state, msg = CRITICAL, ''
        if category == 'auth_status':
            state, msg = check_zerto_auth(hostaddress, username, password, url, port)
        elif category == 'zvm_status':
            state, msg = check_zvm_status(hostaddress, username, password, url, port)
        elif category == 'alerts':
            state, msg = check_alert_status(hostaddress, username, password, port, url, subcategory, enable_cache,
                                            cache_timeout, previous_state)
        return state, msg
    except Exception as e:
        return CRITICAL, 'CRITICAL : {0}'.format(e)


def main():
    state, msg = check_status(*check_args())
    print(msg)
    sys.exit(state)


if __name__ == '__main__':
    main()
