#!/usr/bin/env python


from __future__ import print_function
from scanline.utilities.config_reader import discovery_yml_contents
from scanline.product.isp import ISPProductScanner
import sys

def get_status(isp_hostaddress, vcenter):
    if (isp_hostaddress and vcenter):
        isp_version = get_isp_version(isp_hostaddress)
        if(not isp_version):
            state = 2
            msg = 'CRITICAL: ISP and vCenter Scanners are present but ISP version cannot be fetched'
        elif(isp_version.startswith('3') or isp_version.startswith('4,1')):
            state = 2
            msg = 'CRITICAL: ISP and vCenter Scanners are present in discovery.yml but current ISP version({0}) is less than 4.4'.format(isp_version)
        else:
            state = 0
            msg = 'OK: ISP and vCenter Scanners are present in discovery.yml and current ISP version({0}) is greater than 4.4'.format(isp_version)
    elif(isp_hostaddress and not vcenter):
    	state = 2
    	msg = 'CRITICAL: ISP Scanner is present but vCenter Scanner is not available in discovery.yml'
    elif(not isp_hostaddress and vcenter):
    	state = 2
    	msg = 'CRITICAL: vCenter Scanner is present but ISP Scanner is not available in discovery.yml'
    else:
    	state = 2
    	msg = 'CRITICAL: ISP and vCenter Scanners are not present in discovery.yml'
    return state,msg


def get_isp_version(hostaddress):
    """get ISP version from API"""
    version = ''
    scanned = ISPProductScanner(scanner='ISP', address=hostaddress, username=None, password=None)
    if scanned.isp:
        version = scanned.isp.get_software_version()
    return version


def check_scanners():
    try:
        is_vcenter_present = False
        isp_hostaddress = None
        for endpoint in discovery_yml_contents():
            if(endpoint.get('scanner').lower() == 'isp'):
                isp_hostaddress = endpoint.get('address')
            elif(endpoint.get('scanner').lower() == 'vcenter'):
                is_vcenter_present = True
        state, msg = get_status(isp_hostaddress, is_vcenter_present)
    except Exception as e:
        state, msg = 2,  'CRITICAL : Exception {0}'.format(e.message)
    return state, msg


def main():
    state, output = check_scanners()
    print(output, end='')
    sys.exit(state)


if __name__ == '__main__':
    main()
