#!/usr/bin/env python

import argparse
import sys
from scanline.product.isp import ISPProductScanner
import logging
from StringIO import StringIO
from phimutils.perfdata import INFO_MARKER


# the following requires python-argparse to be installed
def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script to get ISP version from API')
    parser.add_argument('-H', '--hostaddress',
                        required=True, help='The address of the server originating the message')
    parser.add_argument('-v', '--isp_version',
                        required=False, help='ISP version')
    results = parser.parse_args(args)
    return results.hostaddress, results.isp_version


def get_isp_version(hostaddress):
    """get ISP version from API"""
    version = ''
    scanned = ISPProductScanner(scanner='ISP', address=hostaddress, username=None, password=None)
    if scanned.isp:
        version = scanned.isp.get_software_version()

    return version


def main(hostaddress, isp_version):
    logger = logging.getLogger(__name__)
    out_file = StringIO()
    handler_console = logging.StreamHandler(out_file)
    logger.addHandler(handler_console)
    software_version = isp_version if isp_version else get_isp_version(hostaddress)
    if not software_version:
        print('UNKNOWN - ISP Version not found')
        sys.exit(3)
    out_template = 'OK - ISP Version {version} {info_marker} Version={version}'
    print(out_template.format(version=software_version, info_marker=INFO_MARKER))


if __name__ == '__main__':
    main(*check_arg())