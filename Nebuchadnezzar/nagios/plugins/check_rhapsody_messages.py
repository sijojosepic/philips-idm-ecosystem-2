#!/usr/bin/env python
import sys
import json
import argparse
import requests
from scanline.utilities.http import HTTPRequester


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to get message counts in error and hold queues')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The name of the Rahpsody server')
    parser.add_argument('-P', '--port', required=True,
                        help='The port of the Rahpsody server')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the Rahpsody server')
    parser.add_argument('-p', '--password', required=True,
                        help='The passowrd of the Rahpsody server')
    parser.add_argument('-s', '--subpath',
                        help='The passowrd of the Rahpsody server')
    parser.add_argument('-w', '--warning_val',
                        help='The warning threshold for the queue')
    parser.add_argument('-c', '--critical_val',
                        help='The critical threshold for the queue')
    parser.add_argument('-v', '--version_req', default=0,
                        help='Fetches the Rhapsody version, if "1" passed')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.port,
        results.username,
        results.password,
        results.subpath,
        results.warning_val,
        results.critical_val,
        results.version_req)


def get_message_count(hostname, port, username, password, subpath, warning_val,
                      critical_val):
    url = 'https://{0}:{1}/api/{2}/count'.format(hostname, port, subpath)
    headers = {'Accept': 'application/json, text/html',}
    auth = (username, password)
    warning_val, critical_val = int(warning_val), int(critical_val)
    try:
        http_request = HTTPRequester()
        msg_str = http_request.suppressed_get_parsed(url, headers=headers,
                                                     auth=auth, section='div',
                                                     attrs={'class': 'section'})
        msg_count = int(str(msg_str).strip().split(':')[1])
        msg = 'Total message(s) found in the queue: {0}'
        if msg_count < warning_val and msg_count < critical_val:
            return 0, msg.format(msg_count)
        elif msg_count >= warning_val and msg_count < critical_val:
            return 1, msg.format(msg_count)
        elif msg_count >= critical_val:
            return 2, msg.format(msg_count)
    except IndexError:
        return 2, 'Could not fetch attribute.'
    except Exception:
        return 2, 'Could not connect to server.'


def get_version(hostname, username, password, port):
    url = 'https://{0}:{1}/api/info'.format(hostname, port)
    headers = {'Accept': 'application/vnd.orchestral.rhapsody.6_0+json',
               'Accept': 'application/json', }
    auth = (username, password)
    try:
        response = requests.get(url, headers=headers, verify=False, auth=auth,
                                timeout=30)
        if response.status_code == 200:
            version = json.loads(response.content)['data']['version']
            return 0, 'Rhapsody version is: {0}'.format(version)
        else:
            error_msg = 'Could not fetch version, status code: {0}'
            return 2, error_msg.format(response.status_code)
    except Exception:
        return 2, 'Could not connect to server.'


def main():
    hostname, port, username, password, subpath, warning_val, critical_val,\
                                                    version_req = check_arg()
    if int(version_req) == 0:
        state, msg = get_message_count(hostname, port, username, password,
                                       subpath, warning_val, critical_val)
        if 'message' in msg:
            updated_msg = '{0} | messsage_count={1};{2};{3}'
            msg = updated_msg.format(msg, int(msg.split(': ')[1]), warning_val,
                                     critical_val)
    elif int(version_req) == 1:
        state, msg = get_version(hostname, username, password, port)
    print msg
    sys.exit(state)


if __name__ == '__main__':
    main()
