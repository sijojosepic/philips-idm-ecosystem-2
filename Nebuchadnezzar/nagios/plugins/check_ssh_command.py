#!/usr/bin/env python

import argparse
import sys
from scanline.utilities.ssh import SSHConnection
from scanline.utilities.utils import format_status
from datetime import datetime


OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

SSH_COMMANDS = {'partition': 'df -h', 'license': 'tmsh show /sys license | grep License'}


def check_args(args=None):
    parser = argparse.ArgumentParser(
        description='Script get status of SSH Services')
    parser.add_argument('-H', '--hostname', required=True,
                        help='Host address')
    parser.add_argument('-u', '--username', required=True,
                        help='Host username')
    parser.add_argument('-p', '--password', required=True,
                        help='Host Password')
    parser.add_argument('-x', '--command',
                        help='SSH Command')
    parser.add_argument('-w', '--warning', default=80, type=int,
                        help='Warning threshold')
    parser.add_argument('-c', '--critical', default=85, type=int,
                        help='Critical threshold')

    results = parser.parse_args(args)

    return (
        results.hostname,
        results.username,
        results.password,
        results.command,
        results.warning,
        results.critical
    )


def check_partition_used_percent(partition_list, warning_threshold, critical_threshold):
    critical_msg = warning_msg = ok_msg = perf_msg = ''
    for partitions in partition_list:
        if len(partitions) > 1:
            p_name, used_percentage = partitions[-1], partitions[-2]
            used = float(used_percentage.split('%')[0])
            if (used >= warning_threshold and used < critical_threshold):
                warning_msg = warning_msg + 'Mounted on : {0}, Used%: {1} \n'.format(p_name, used)
            elif used >= critical_threshold:
                critical_msg = critical_msg + 'Mounted on: {0}, Used%: {1} \n'.format(p_name, used)
            else:
                ok_msg = ok_msg + 'Mounted on: {0}, Used%: {1} \n'.format(p_name, used)
            perf_msg = perf_msg + "{0}={1};{2};{3};\n".format(p_name, used_percentage, warning_threshold,
                                                              critical_threshold)
    if ok_msg:
        ok_msg = 'OK: Partitions: ' + ok_msg
    if warning_msg:
        warning_msg = 'WARNING: Threshold breached for partitions: ' + warning_msg + ok_msg
    if critical_msg:
        if warning_msg:
            critical_msg = 'CRITICAL: Threshold breached for partitions: ' + critical_msg + warning_msg
        else:
            critical_msg = 'CRITICAL: Threshold breached for partitions: ' + critical_msg + ok_msg
    return warning_msg, critical_msg, ok_msg, perf_msg


def check_partition(raw_ssh_output, warn_threshold, criti_threshold):
    """
    :param raw_ssh_output: Filesystem Size Used Avail Use% Mounted on
                           9.3G  3.7G  5.1G  86% /
                           tmpfs 1.9G   60K  1.9G   82% /dev/shm

    :param warn_threshold: 80
    :param criti_threshold: 85
    :return: 0 , "OK"
    """
    # Split data to get last two values from every line (partition name and used percentage)
    data_list = raw_ssh_output.split('\n')
    partition_list = [partition.split(' ') for partition in data_list[1:]]
    warning_msg, critical_msg, ok_msg, perf_msg = check_partition_used_percent(partition_list, warn_threshold,
                                                                               criti_threshold)
    state, msg = format_status(warning_msg, critical_msg, ok_msg, perf_msg)
    return state, msg


def check_license(ssh_output, warning, critical):
    critical_msg = ok_msg = warning_msg = perf_msg = ''
    license_data = ssh_output.split('\n')
    if "License End Date" in license_data[-2]:
        end_date = license_data[-2].split(" ")[-1]
        license_date = datetime.strptime(end_date, '%Y/%m/%d')
        days_remaining = (license_date - datetime.now()).days
        if days_remaining < 0:
            critical_msg = 'CRITICAL: F5 License is expired. License end date: {0} \n {1}'\
                .format(license_date, ssh_output)
        elif days_remaining <= critical:
            critical_msg = 'CRITICAL: Temporary License is in use. {0} days left for license expiry and' \
                           ' license end date is {1} \n {2}'.format(days_remaining, license_date, ssh_output)
        else:
            ok_msg = 'OK: Temporary License is in use. {0} days left for license expiry and license end date is {1} ' \
                     '\n {2}'.format(days_remaining, license_date, ssh_output)
    else:
        ok_msg = 'OK: Permanent License in use: \n'+str(ssh_output)
    state, msg = format_status(warning_msg, critical_msg, ok_msg, perf_msg)
    return state, msg


def check_status(hostname, username, password, command, warning, critical):
    try:
        function_mapping = {'partition': check_partition, 'license': check_license}
        if command in SSH_COMMANDS.keys():
            ssh_obj = SSHConnection(hostname, username, password)
            error, ssh_output = ssh_obj.execute_command(SSH_COMMANDS[command])
            if error:
                return CRITICAL, 'CRITICAL : Error - {0}'.format(error)
            state, msg = function_mapping[command](ssh_output, warning, critical)
        else:
            state, msg = CRITICAL, 'CRITICAL : Invalid command - {0}'.format(command)
        return state, msg
    except Exception as e:
        return CRITICAL, 'CRITICAL : Error while retrieving data {0}'.format(e)


def main():
    state, msg = check_status(*check_args())
    print(msg)
    sys.exit(state)


if __name__ == '__main__':
    main()
