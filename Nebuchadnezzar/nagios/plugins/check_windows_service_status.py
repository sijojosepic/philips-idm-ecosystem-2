#!/usr/bin/env python
##########################################################
# Shinken Plugin - Monitor windows service using WinRM.
# The aim of this plugin is to monitor the windows service
# Plugin uses WinRM, to execute PowerShell scripts
# Plugin exit with OK or CRITICAL status.
# CRITICAL Status will be triggered
#   1) When the service startup type set to Automatic
#      and state of the service in stopped.
#   2) When the --argument supplied is 'INSTALLED'
#       and the service is not available in the node.
###########################################################
from __future__ import print_function
import sys
import argparse
from winrm.exceptions import (AuthenticationError, WinRMOperationTimeoutError, WinRMTransportError, WinRMError)
from scanline.utilities.win_rm import WinRM

GRAPHER = '| service status={0};1;2;0;2'

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Monitor Windows service')
    parser.add_argument('-H',  '--host', required=True, help='The hostname of the server')
    parser.add_argument('-U', '--username', required=True, help='The user(eg `DOMAIN\\user`) name of the server')
    parser.add_argument('-P', '--password', required=True, help='The password for the server')
    parser.add_argument('-S', '--service', required=True, help='The name of the Windows service to monitor')
    parser.add_argument('--argument', default='NOTINSTALLED', help='The argument to check whether service is by default'
                                                                   'INSTALLED or NOTINSTALLED')
    parser.add_argument('-s', '--state', required=False, default=None, help='Provide service check state to return in '
                                                                            'case --argument "INSTALLED" is provided')
    results = parser.parse_args(args)
    return results.host, results.username, results.password, results.service, results.argument, results.state


def ps_script(service):
    execute = r'''Get-WmiObject win32_service -Filter "name='{0}'"'''
    return execute.format(service)


def check_status(state, msg):
    state_check = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2, 'UNKNOWN': 3}
    status, msg = state_check[state], '{0} : {1}'.format(state, msg)
    return status, msg


def check_mode(out_put, argument, state):
    if out_put.status_code == OK:
        if out_put.std_out:
            out_put = out_put.std_out.split('\r\n')
            return get_service_mode_state(out_put)
        elif argument == 'NOTINSTALLED':
            status, msg = OK, 'OK : Service is not installed' + GRAPHER.format(OK)
            return status, msg
        elif argument == 'INSTALLED':
            msg = 'Service not Installed' + GRAPHER.format(CRITICAL)
            status, msg = check_status(state, msg)
            return status, msg
        else:
            status, msg = WARNING, 'WARNING: Argument or state supplied to plugin is invalid'
            return status, msg


def get_service_mode_state(out_put):
    """ This method fetches the service start up mode and state"""
    service_mode_state = []
    for result in out_put:
        if 'StartMode' in result:
            mode = result.split(': ')[1]
            service_mode_state.append(mode)
        if 'State' in result:
            state = result.split(': ')[1]
            service_mode_state.append(state)
    return check_state(service_mode_state)


def check_state(state):
    status, msg = CRITICAL, 'Service startup type or status unknown'
    if state[0] == 'Disabled' and state[1] == 'Stopped':
        status, msg = OK, 'OK : Service startup type is set to Disabled and Status is Stopped' + GRAPHER.format(OK)
    elif state[0] == 'Disabled' and state[1] == 'Running':
        status, msg = OK, 'OK : Service startup type is set to Disabled and Status is Running' + GRAPHER.format(OK)
    elif state[0] == 'Manual' and state[1] == 'Stopped':
        status, msg = CRITICAL, 'CRITICAL : Service startup type is set to Manual and Status is Stopped' + GRAPHER.format(CRITICAL)
    elif state[0] == 'Manual' and state[1] == 'Paused':
        status, msg = WARNING, 'WARNING : Service startup type is set to Manual and Status is Paused'\
                      + GRAPHER.format(WARNING)
    elif state[0] == 'Manual' and state[1] == 'Running':
        status, msg = OK, 'OK : Service startup type is set to Manual and Status is Running' + GRAPHER.format(OK)
    elif state[0] == 'Auto' and state[1] == 'Running':
        status, msg = OK, 'OK : Service startup type is set to Automatic and Status is Running' + GRAPHER.format(OK)
    elif state[0] == 'Auto' and state[1] == 'Paused':
        status, msg = WARNING, 'WARNING : Service startup type is set to Automatic and Status is Paused' \
                      + GRAPHER.format(WARNING)
    elif state[0] == 'Auto' and state[1] == 'Start Pending':
        status, msg = WARNING, 'WARNING : Service startup type is set to Automatic and Status is Start Pending' \
                      + GRAPHER.format(WARNING)
    elif state[0] == 'Auto' and state[1] == 'Stopped':
        status, msg = CRITICAL, 'CRITICAL : Service startup type is set to Automatic and Status is not Running'\
                      + GRAPHER.format(CRITICAL)
    return status, msg


def process_output(out_put, argument, state):
    status, msg = check_mode(out_put, argument, state)
    return status, msg


def main(host, username, password, service, argument, state):
    status, msg = CRITICAL, ''
    try:
        win_rm = WinRM(host, username, password)
        out_put = win_rm.execute_ps_script(ps_script(service))
        status, msg = process_output(out_put, argument, state)
    except AuthenticationError as e:
        status, msg = UNKNOWN, 'UNKNOWN : WinRM Error {0}'.format(e) + GRAPHER.format(UNKNOWN)
    except (WinRMOperationTimeoutError, WinRMTransportError, WinRMError) as e:
        msg = 'CRITICAL : WinRM Error {0}'.format(e) + GRAPHER.format(CRITICAL)
    except TypeError as e:
        if 'takes exactly 2' in str(e):
            msg = 'CRITICAL : Issue in connecting to node - {0}'.format(host) + GRAPHER.format(CRITICAL)
        else:
            msg = 'CRITICAL : Typeerror(May be Issue in connecting to node - {0})'.format(host) + GRAPHER.format(CRITICAL)
    except Exception as e:
        msg = 'CRITICAL : Exception {0}'.format(e) + GRAPHER.format(CRITICAL)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*check_arg())
