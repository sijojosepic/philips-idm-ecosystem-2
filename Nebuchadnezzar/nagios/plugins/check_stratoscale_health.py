#!/usr/bin/env python

import json
import sys
import argparse
import requests
import redis
from collections import namedtuple

MEMORY_STATUS_PARAM = {
                     "memory_used": ["query_top", {"metric_name": "memory__used__of__cluster__in__MiB"}],
                     "memory_provisioned": ["query_top", {"metric_name": "memory__total__of__cluster__in__MiB"}],
                     "memory_used_by_vms": ["query_top", {"metric_name": "memory__used_by_vms__of__cluster__in__MiB"}],
                     "memory_used_by_system": ["query_top",
                                               {"metric_name": "memory__used_by_system__of__cluster__in__MiB"}],
                     "memory_reserved_for_vms": ["query_top",
                                                 {"metric_name": "memory__reserved_for_vms__of__cluster__in__MiB"}],
                     "memory_reserved_for_system": ["query_top", {
                         "metric_name": "memory__reserved_for_system__of__cluster__in__MiB"}],
                     "memory_available_for_vms": ["query_top",
                                                  {"metric_name": "memory__available_for_vms__of__cluster__in__MiB"}]
                     }

CPU_STATUS_PARAM =  {
                     "cpu_count": ["query_top", {"metric_name": "cpu__count__of__cluster__in__CPU"}],
                     "cpu_used": ["query_top", {"metric_name": "cpu__used__of__cluster__in__MHz"}],
                     "cpu_provisioned": ["query_top", {"metric_name": "cpu__total__of__cluster__in__MHz"}]
                    }

redis_con = redis.StrictRedis(host='localhost', port=6379, db=0)

def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to check StratoScale infra health')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of StratoScale instance'),
    parser.add_argument('-d', '--domainname', required=True,
                        help='Domain name on Stratoscale')
    parser.add_argument('-u', '--username', required=True,
                        help='The username of the Stratoscale')
    parser.add_argument('-p', '--password', required=True,
                        help='The password of the Stratoscale')
    parser.add_argument('-w', '--warningthreshold', required=False, type=int, choices=range(1, 101),
                        help='The threshold value in percentage for warning status')
    parser.add_argument('-c', '--criticalthreshold', required=False, type=int, choices=range(1, 101),
                        help="The threshold value in percentage for critical status")
    parser.add_argument('-r', '--resourceType', required=False, choices=['MEMORY', 'CPU', 'STORAGE'],
                        help="Resource Type for which status needs to be checked")
    parser.add_argument('-s','--servicetype',required=False,
                        help="service type for cluster either precheck or other services", default=None)


    results = parser.parse_args(args)
    return (results.hostname,
            results.domainname,
            results.username,
            results.password,
            results.warningthreshold,
            results.criticalthreshold,
            results.resourceType,
            results.servicetype)

def get_token_from_redis():
    token = redis_con.hgetall("hsopClusterToken")
    return token['token'] if token else False

def insert_token_to_redis(token):
    token = {'token': token}
    redis_con.hmset("hsopClusterToken", token)

def generate_token(hostname, domainname, username, password):
    token_payload = create_token_payload(domainname, username, password)
    token = get_token(hostname, token_payload)
    insert_token_to_redis(token)
    return token

def get_json_response(hostname, token, param_val):
    try:
        url = 'https://{0}/api/v2/metrics/queries'.format(hostname)
        param_val_json_str = json.dumps(param_val)
        param_pay_load = {'queries': param_val_json_str}
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, verify=False, params=param_pay_load, headers=headers)
        response.raise_for_status()
        return response.json()
    except Exception as e:
        return {}

def get_memory_status(hostname, domainname, username, password, token, warn_threshold, crit_threshold):
    result = get_json_response(hostname, token, MEMORY_STATUS_PARAM)
    if not result:
        token = generate_token(hostname, domainname, username, password)
        result = get_json_response(hostname, token, MEMORY_STATUS_PARAM)
    if not result:
        raise Exception('GET Query for Memory API failed')
    mem_used_percentage = compute_memory_usage(result)
    status, message =  get_health_status(mem_used_percentage, warn_threshold, crit_threshold, 'MEMORY')
    return (status, message)
    
def get_cpu_status(hostname, domainname, username, password, token, warn_threshold, crit_threshold):
    result = get_json_response(hostname, token, CPU_STATUS_PARAM)
    if not result:
        token = generate_token(hostname, domainname, username, password)
        result = get_json_response(hostname, token, CPU_STATUS_PARAM)
    if not result:
        raise Exception('GET Query for CPU API failed')
    cpu_used_percentage = compute_cpu_usage(result)
    status, message = get_health_status(cpu_used_percentage, warn_threshold, crit_threshold, 'CPU')
    return (status, message)

def get_json_response_storage(hostname, token):
    try:
        url = 'https://{0}/api/v2/storage/pools'.format(hostname)
        headers = {'content-type': 'application/json', 'x-auth-token': token}
        response = requests.get(url, headers=headers, verify=False)
        response.raise_for_status()
        return response.json()
    except Exception as e:
        return {}

def get_storage_status(hostname, domainname, username, password, token, warn_threshold, crit_threshold):
    result = get_json_response_storage(hostname, token)
    if not result:
        token = generate_token(hostname, domainname, username, password)
        result = get_json_response_storage(hostname, token)
    if not result:
        raise Exception('GET Query for storage failed')
    storage_percentage_used = compute_storage_usage(result)
    status, message = get_health_status(storage_percentage_used, warn_threshold, crit_threshold, 'STORAGE')
    return (status, message)

def create_token_payload(domainname, username, password):
    payload = {'auth': {'identity': {'methods': ['password'], 'password': {
        'user': {'name': username, 'password': password, 'domain': {'name': domainname}}}},
                        'scope': {'domain': {'name': domainname}}}}
    headers = {'content-type': 'application/json'}
    body = json.dumps(payload)
    TokenPayload = namedtuple('TokenPayload', 'headers body')
    token_payload = TokenPayload(headers, body)
    return token_payload

def get_token(hostname, token_payload):
    try:
        url = 'https://{0}/api/v2/identity/auth'.format(hostname)
        response = requests.post(url, verify=False, headers=token_payload.headers, data=token_payload.body)
        token = response.headers['x-subject-token']
        return token
    except Exception as e:
        raise Exception('Token generation failed : Please check credentials for {hostname}'.format(hostname))


def get_stratoscale_resources_status(hostname, domainname, username, password, warn_threshold, crit_threshold, resource_type):
    try:
        resource_mths = {
            'MEMORY': get_memory_status,
            'CPU': get_cpu_status,
            'STORAGE': get_storage_status
        }
        token = get_token_from_redis()
        if not token:
            token = generate_token(hostname, domainname, username, password)
        s, m = resource_mths[resource_type](hostname, domainname, username, password, token, warn_threshold, crit_threshold)
        return s, m
    except Exception as e:
        return (2, 'Critical - '+str(e))


def compute_cpu_usage(cpu_attributes):
    try:
        cpu_used = float(cpu_attributes['cpu_used'][0][1])
        cpu_provisioned = float(cpu_attributes['cpu_provisioned'][0][1])
        calculated_result = round((cpu_used / cpu_provisioned) * 100)
        return calculated_result
    except:
        raise Exception('Error while getting attributes for cpu from api, result: '+ str(cpu_attributes))


def compute_memory_usage(memory_attributes):
    try:
        memory_used_by_system = float(memory_attributes['memory_used_by_system'][0][1])
        memory_used_by_vms = float(memory_attributes['memory_used_by_vms'][0][1])
        memory_reserved_for_system = float(memory_attributes['memory_reserved_for_system'][0][1])
        memory_reserved_for_vms = float(memory_attributes['memory_reserved_for_vms'][0][1])
        memory_provisioned = float(memory_attributes['memory_provisioned'][0][1])
        calculated_result = (round(((
                                    memory_used_by_system + memory_used_by_vms + memory_reserved_for_system + memory_reserved_for_vms) / memory_provisioned) * 100))
        return calculated_result
    except:
        raise Exception('Error while getting attributes for memory from api, result: '+ str(memory_attributes))


def compute_storage_usage(storage_attributes):
    try:
        total_capacity_mb = 0.0
        free_capacity_mb = 0.0
        for storage_info in storage_attributes:
            total_capacity_mb = total_capacity_mb + storage_info['total_capacity_mb']
            free_capacity_mb = free_capacity_mb + storage_info['free_capacity_mb']
        calculated_result = round(((total_capacity_mb - free_capacity_mb) / total_capacity_mb) * 100)
        return calculated_result
    except:
        raise Exception('Error while getting attributes for storage from api, result: '+ str(storage_attributes))


def get_health_status(currentvalue, warn_threshold, crit_threshold, resource_type):
    currentvalue = int(currentvalue)
    warn_threshold = int(warn_threshold)
    crit_threshold = int(crit_threshold)
    grapher = '|message info={0};{1};{2};1;100'

    if warn_threshold <= currentvalue < crit_threshold:
        return 1, '{0} usage = {1}%, Status is Warning'.format(resource_type, currentvalue) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)
    elif currentvalue >= crit_threshold:
        return 2, '{0} usage = {1}%, Status is Critical'.format(resource_type, currentvalue) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)
    else:
        return 0, '{0} usage = {1}%, Status is Ok'.format(resource_type, currentvalue) + grapher.format(
            currentvalue, warn_threshold, crit_threshold)

def get_cluster_reachability(hostname, domainname, username, password):
    try:
        token_payload = create_token_payload(domainname, username, password)
        token = get_token(hostname, token_payload)
        redis_con.hset('hsop','hsopPrecheck', 1)
        return 0, 'Ok - Token generation passed for {hostname}'.format(hostname=hostname)
    except Exception as e:
        redis_con.hset('hsop','hsopPrecheck', 0)
        return 2, 'Critical - Token generation failed for {hostname}, Please check credentials'.format(
            hostname=hostname)

def main():
    hostname, domainname, username, password, warn_threshold,crit_threshold,\
     resource_type, service_type = check_arg()
    status, message = 0, ''
    if service_type == 'precheck':
        status, message = get_cluster_reachability(hostname, domainname, username, password)
    else:
        status, message = get_stratoscale_resources_status(hostname, domainname, username,
         password, warn_threshold,crit_threshold, resource_type)
    print message
    sys.exit(status)


if __name__ == '__main__':
    main()
