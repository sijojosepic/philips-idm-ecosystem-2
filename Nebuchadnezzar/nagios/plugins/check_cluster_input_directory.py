#!/usr/bin/env python

import argparse
import sys

STATES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')
DATA_DESCRIPTION = 'The status codes of the hosts or services in the cluster followed by a label to identify the node'
LEVEL_DESCRIPTION = 'The level to consider a problem 1-Warning 2-Critical'
CRITICAL_DESCRIPTION = 'The CRITICAL threshold. Number of hosts or services in cluster that must be in a non-OK state'
WARNING_DESCRIPTION = 'The WARNING threshold. Number of hosts or services in cluster that must be in a non-OK state'

truths = {
    (2,2): 2,
    (2,0): 1,
    (0,2): 1,
    (1,0): 1,
    (0,1): 1,
    (1,2): 1,
    (2,1): 1,
    (3,3): 3,
    (0,3): 3,
    (3,0): 3,
    (3,1): 3,
    (3,2): 3,
    (1,3): 3,
    (2,3): 3,
    (0,0): 0,
}


class LevelListAction(argparse.Action):
    LEVELS = map(str, range(4))

    def __call__(self, parser, namespace, values, option_string=None):
        if not (len(values) == 2):
            raise argparse.ArgumentError(self, '%s takes 2 values, %d given' % (option_string, len(values)))
        level, label = values
        if level in self.LEVELS:
            destination = getattr(namespace, self.dest) or []
            destination.append((int(level), label))
            setattr(namespace, self.dest, destination)


def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Service Cluster Plugin for Nagios')
    parser.add_argument('-l', '--label', help='Optional prepended text output (i.e. "Service cluster")')
    parser.add_argument('-r', '--level', type=int, choices=(1, 2), default=2, help=LEVEL_DESCRIPTION)
    parser.add_argument('-d', '--data', required=True, action=LevelListAction, help=DATA_DESCRIPTION, nargs=2)
    results = parser.parse_args(args)
    return (results.label,
            results.level,
            results.data)


def check_cluster_input_directory(label, level, data):
    state = 0
    input1 = int(data[0][1])
    input2 = int(data[1][1])
    state = truths[(input1, input2)]
    label_txt = '{0}: '.format(label) if label else ''
    msg = '{label}'.format(label=label_txt)
    return state, msg.strip()


def main():
    state, msg = check_cluster_input_directory(*check_arg())
    print('{state} - {msg}'.format(state=STATES[state], msg=msg))
    sys.exit(state)


if __name__ == '__main__':
    main()
