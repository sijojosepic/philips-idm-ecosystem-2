#!/usr/bin/env python

"""Aim of this plugin is to update the various status of hosts to facts collection.
This plugin will push the payload to backoffice using version endpoint
-------------
'VERSION': {
        'ENDPOINT': 'http://vigilant/version'
    },
-------------

The hosts are differentiated with the module_type argument

As of now it can support with only RabbiMQ version and hsop stratoscale version updation

NOTE :- All the future hosts status updates to the facts, should be integrated with this plugin.

Example

./node_status.py -H 'rabbitmq-server.isyntax.net' -U phiadmin -P '1nf0M@t1csPh!lt0r' -M 'RabbitMQ'

./node_status.py -H 'hsop_hostname' -M 'RabbitMQ' -vu 'required_url'

Example for SQL Version 
./node_status.py -H 'idm10db1.idm10.isyntax.net' -u 'phisqlmonitor' -p 'Nsah2damsh@60s!' -M sql_version -D msdb

"""

from __future__ import print_function
import argparse
import sys
import json
import pymssql
import requests
from requests.exceptions import RequestException

from scanline.utilities.config_reader import read_from_tbconfig, get_site_id
from phimutils.message import Message

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

RBQ_URL_TEMPLATE = '{0}://{1}:{2}/api/{3}'
QUERY = """SELECT
  SERVERPROPERTY('ProductLevel') AS ProductLevel,
  SERVERPROPERTY('Edition') AS Edition,
  SERVERPROPERTY('ProductVersion') AS ProductVersion,
  @@version AS ProductName
"""


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(
        description='Nagios Plugin to wrap a command deciding whether to run it')
    parser.add_argument('-H', '--hostname', required=True,
                        help='The address of the server')
    parser.add_argument('-u', '--username', required=False,
                        help='Username of the server')
    parser.add_argument('-p', '--password', required=False,
                        help='Password of the server')
    parser.add_argument('-M', '--module_type', required=True,
                        help='ISP ModuleType of the host')
    parser.add_argument('-r', '--protocol', required=False,
                        help='Protocol http/https etc', default='https')
    parser.add_argument('-P', '--port', required=False,
                        help='Port', default=15671)
    parser.add_argument('-t', '--timeout', default=15,
                        help='Request timeout.')
    parser.add_argument('-ru', '--req_url', required=False,
                        help='url to get version')
    parser.add_argument('-D', '--database', required=False,
                        help='Database to be used')
    results = parser.parse_args(args)
    return (
        results.hostname,
        results.username,
        results.password,
        results.module_type,
        results.protocol,
        results.port,
        results.timeout,
        results.req_url,
        results.database
    )


def get_url(template, *args):
    return template.format(*args)


def update_status(data, verify=False, **kwargs):
    version_url = read_from_tbconfig('VERSION')['ENDPOINT']
    siteid = get_site_id()
    data = {'facts': data}
    message = Message(siteid=siteid, payload=data, **kwargs)
    response = requests.post(version_url, json=message.to_dict(), verify=verify)
    response.raise_for_status()


def _request(url, username, password, timeout, headers, verify=False):
    auth = (username, password)
    response = requests.get(url, headers=headers, auth=auth,
                            timeout=int(timeout), verify=verify)
    response.raise_for_status()
    return json.loads(response.content)


def _format_status_info(hostname, data):
    version_info = {'name': 'Rabbitmq', 'version': data.get('rabbitmq_version')}
    components = {'Components': [version_info]}
    return {hostname: components}


def update_rabbitmq_status(hostname, username, password, protocol, port, req_timeout):
    url = get_url(RBQ_URL_TEMPLATE, protocol, hostname, port, 'overview')
    response = _request(url, username, password, req_timeout, {})
    status = _format_status_info(hostname, response)
    update_status(status)
    return OK, 'Status Updated'

def _format_status_info_stratoscale(hostname, data):
    version_info = []
    for service, version in data.items():
        version = [v for v in version if 'version-' in v.lower()]
        if version:
            version_info.append({'name': service, 'version': ''.join(version).split('-')[1]})
    components = {'Components': version_info}
    return {hostname: components}


def update_stratoscale_status(hostname, req_url, req_timeout):
    response = _request(req_url, '', '', req_timeout, {})
    status = _format_status_info_stratoscale(hostname, response)
    update_status(status)
    return OK, 'OK - Consul services version Updated'

def update_sql_version(hostname, username, password, database):
    conn = pymssql.connect(server=hostname, user=username, password=password, database=database, login_timeout=20)
    cursor = conn.cursor()
    cursor.execute(QUERY)
    result = cursor.fetchone()
    if result:
        status = _format_sql_version_info(hostname, result)
        update_status(status)
        return OK, 'SQL Version Updated'
    return WARNING, 'Failed to retrieve SQL Version'

def _format_sql_version_info(hostname, data):
    version = data[2]
    name = data[3].split('(')
    version_name= "{0}{1}".format(name[0], data[1])
    version_info = {"name": version_name,"version": version}
    components = {'Components': [version_info]}
    return {hostname: components}

def main(hostname, username, password, module_type, protocol, port, req_timeout, req_url, database):
    try:
        if module_type.lower() == 'rabbitmq':
            return update_rabbitmq_status(hostname, username, password, protocol, port, req_timeout)
        if module_type.lower() == 'stratoscale':
            return update_stratoscale_status(hostname, req_url, req_timeout)
        if module_type.lower() == 'sql_version':
            return update_sql_version(hostname, username, password, database)
        return WARNING, 'WARNING : UNKNOWN module_type - {0}'.format(module_type)
    except pymssql.Error as e:
        return WARNING, 'WARNING : Error {0}'.format(e)
    except RequestException as e:
        return WARNING, 'WARNING : Request Error {0}'.format(e)
    except Exception as e:
        return WARNING, 'WARNING : Error {0}'.format(e)


if __name__ == '__main__':
    status, msg = main(*check_arg())
    print(msg)
    sys.exit(status)
