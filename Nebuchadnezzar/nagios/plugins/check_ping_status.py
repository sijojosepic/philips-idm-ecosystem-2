#!/usr/bin/env python

from __future__ import print_function

import sys
import requests
import argparse
import ast

from requests.exceptions import RequestException
from scanline.utilities import token_mgr

URL_TEMPLATE = '{protocol}://{host}:{port}{service_url}'
GRAPHER = '| service status={0};1;2;0;2'

STATUSES = ('OK', 'WARNING', 'CRITICAL', 'UNKNOWN')

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3


def parse_cmd_args(args=None):
    parser = argparse.ArgumentParser(
        description='Check the Health of an Application over HTTP')
    parser.add_argument('-H', '--host', required=True,
                        help='The hostname of the server')
    parser.add_argument('-s', '--service_url',
                        help='The Service URL')
    parser.add_argument('-p', '--protocol', default='https',
                        help='The Request protocol HTTP vs HTTPS, defaults to HTTPS')
    parser.add_argument('-c', '--hmac', default=True,
                        help='The rest service if use HMAC authentication')
    parser.add_argument('-P', '--port', default=443,
                        help='The port of protocol')
    results = parser.parse_args(args)
    return (results.host, results.service_url, results.protocol, ast.literal_eval(results.hmac), results.port)


def get_url(host, service_url, protocol, port):
    if service_url:
        return URL_TEMPLATE.format(protocol=protocol, host=host, service_url=service_url, port = port)
    else:
        return URL_TEMPLATE.format(protocol=protocol, host=host, service_url='', port = port)

def ping_status(url, hmac, verify=False):
    if hmac:
        response = token_mgr.do_hmac_request(url, timeout=5)
    else:
        response = requests.get(url, verify=verify, timeout=5)
    response.raise_for_status()
    status = 0
    outmsg = '{res} - Service is up'.format(res=response.status_code)
    return status, outmsg


def main(host, service_url, protocol, hmac, port):
    status, msg = CRITICAL, ''
    try:
        status, msg = ping_status(get_url(host, service_url, protocol, port), hmac)
    except RequestException as e:
        msg = 'CRITICAL : Service is down - Request Error - {0}'.format(e) + GRAPHER.format(2)
    except Exception as e:
        msg = 'CRITICAL : Service is down - Error - {0}'.format(e) + GRAPHER.format(2)
    print(msg)
    sys.exit(status)


if __name__ == '__main__':
    main(*parse_cmd_args())