from string import punctuation
from collections import namedtuple


NS_DIVIDER = '__'
NS_OBJECT_POSITION = 3
NS_PARTS = ('category', 'vendor', 'object', 'attribute', 'dimension')
CLEANUP_CHARS = punctuation.translate(None, '_/\\')
CLEANUP_UNICODE_TRANSLATION = dict.fromkeys(map(ord, CLEANUP_CHARS), None)
NS_INFORMATION_KEY = 'Information'


Namespace = namedtuple('Namespace', NS_PARTS)
Namespace.__new__.__defaults__ = (None,) * len(Namespace._fields)


def cleanup_key(key):
    if isinstance(key, unicode):
        return key.replace('.', '_').translate(CLEANUP_UNICODE_TRANSLATION)
    return key.replace('.', '_').translate(None, CLEANUP_CHARS)


def information_id(info_key, service):
    """Generate an id from information key and service

    :param info_key: they key name
    :type info_key: str
    :param service: the service name, expected in namespace format at least up to NS_OBJECT_POSITION container
    :type service: str
    :return: the information key name in namespace format
    :rtype: str
    """
    ns_decomposed = service.split(NS_DIVIDER)
    info_ns = ns_decomposed[:NS_OBJECT_POSITION]
    info_ns.append(NS_INFORMATION_KEY)
    info_ns.append(info_key)
    return cleanup_key(NS_DIVIDER.join(info_ns))


def metric_value(metric, service):
    """Get metric name and value from a metric dictionary and service name

    :param metric: expected to contain key, value and optionally uom
    :type metric: dict
    :param service: the service name, expected in namespace format
    :type service: str
    :return: the metric identity and it's value with unit of measure
    :rtype: str
    """
    ident = NS_DIVIDER.join([service, metric['label']])
    value = '{value}{uom}'.format(value=metric['value'], uom=metric.get('uom', ''))
    return cleanup_key(ident), value


def ns_attach(base, item):
    return list2ns([base, item])


def list2ns(raw_list):
    """Generate a namespace formatted string from a list"""
    return cleanup_key(NS_DIVIDER.join(raw_list))


def parse_ns(key):
    return Namespace(*key.split(NS_DIVIDER))


def ns2dict(key):
    return parse_ns(key)._asdict()
