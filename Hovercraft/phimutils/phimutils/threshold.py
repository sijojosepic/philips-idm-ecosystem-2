import linecache
from phimutils.resource import NagiosResourcer
from tbconfig import PACKAGE_CONFIG, VMWARE


def get_threshold_value(key_name):
    secret = linecache.getline(VMWARE['SECRET_FILE'], 1).strip()
    nt = NagiosResourcer(PACKAGE_CONFIG['NAGIOS_THRESHOLD_FILE'], secret)
    return nt.get_resources(key_name)[0]
