from namespace import metric_value, information_id, ns_attach


STATE_NS = 'State'


class StateRedis(object):
    def __init__(self, rc, prefix, setname):
        self.rc = rc
        self.prefix = prefix
        self.setname = setname

    def prefix_hostname(self, hostname):
        return "{prefix}{hostname}".format(prefix=self.prefix, hostname=hostname)

    def remove_prefix(self, keyname):
        return keyname[len(self.prefix):]

    def _insert_metrics(self, data, key_name, service):
        """Insert service metrics to redis
        :param data: a list of dictionaries containing perfdata
        :param key_name: they key name where to insert the metrics
        :param service: the service name the metrics are related to
        """
        for metric in data:
            ident, value = metric_value(metric, service)
            self.rc.hset(key_name, ident, value)

    def _insert_information(self, data, key_name, service):
        """Add service Information to redis
        :param data: a dictionary containing information to be inserted
        :param key_name: the key name where to store the information
        :param service: the service name the information is related to"""
        for info_key, info_val in data.iteritems():
            ident = information_id(info_key, service)
            self.rc.hset(key_name, ident, info_val)

    @staticmethod
    def get_state_key_name(service):
        return ns_attach(service, STATE_NS)

    def insert(self, hostname, service, state, infos, perfdata):
        key_name = self.prefix_hostname(hostname)
        # Add key to the set of Host State keys
        self.rc.sadd(self.setname, key_name)
        # Store the service state
        self.rc.hset(key_name, self.get_state_key_name(service), state)
        # Store the performance data
        self._insert_metrics(perfdata, key_name, service)
        # store any found information
        self._insert_information(infos, key_name, service)
        
    def insert_facts(self,key,facts):
        self.rc.hmset(key,facts)

    def add_scanners(self, key, scanner):
        self.rc.sadd(key, scanner)

    def find_keys(self, hostname, keys):
        if keys:
            fetcher = lambda x: dict(zip(keys, self.rc.hmget(x, keys)))
        else:
            fetcher = self.rc.hgetall
        return fetcher(hostname)

    @staticmethod
    def map_keys(original, key_map):
        result = original.copy()
        for key, value in key_map.iteritems():
            try:
                result[value] = result.pop(key)
            except KeyError:
                pass
        return result

    @staticmethod
    def get_mapped_keys(keys, key_map):
        return [key_map.get(key, key) for key in keys]

    def query(self, keys=None, hosts=None, key_map=None):
        """returns dictionary of dictionaries where the keys are the hostnames, and values are dictionaries of the
        stored state data
        :param hosts: a list of the host names to return
        :param keys: a list of the keys to return
        :param key_map: a dictionary of mappings to use, the key is the original key name, value is the desired key to
        replace it with
        The value None will prevent filtering from happening
        """
        dicto = {}
        prefixed_hosts = self.rc.smembers(self.setname)

        if hosts:
            arg_prefixed_hosts = map(self.prefix_hostname, hosts)
            prefixed_hosts = prefixed_hosts.intersection(arg_prefixed_hosts)

        for hostname in prefixed_hosts:
            found_keys = self.find_keys(hostname, keys)
            if key_map:
                found_keys = self.map_keys(found_keys, key_map)
            dicto[self.remove_prefix(hostname)] = found_keys

        return dicto

    def flush(self):
        self.rc.delete(self.setname, *self.rc.smembers(self.setname))
