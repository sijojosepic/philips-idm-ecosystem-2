from package_service import PackageCleanUpService


class CleanUpContext(object):
    # This is a context class to execute the respective service at runtime.
    def __init__(self, service):
        self.service = service

    def execute(self):
        return self.service.execute()


class CleanUpServices(object):
    PACKAGE = 'package'

    # Add all clean up services here.
    SERVICES = {
        PACKAGE: PackageCleanUpService,
    }


class Service(object):
    # This class returns respective service class.
    def __init__(self):
        self.services = CleanUpServices.SERVICES

    def get_service(self, service_name):
        return self.services[service_name]


class CleanUp():
    # Execute cleanups on Neb.
    def __init__(self):
        self.CleanUpContext = CleanUpContext

    def run(self, service_obj):
        return self.CleanUpContext(service_obj).execute()
