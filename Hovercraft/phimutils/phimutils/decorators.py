from __future__ import absolute_import

from datetime import datetime
from celery.utils.log import get_logger

logger = get_logger(__name__)



def log_execution_time(func):
    def wrapper(*args, **kwargs):
        start_time = datetime.now()
        result = func(*args, **kwargs)
        end_time = datetime.now()
        logger.info("Total execution time for {func_name} is: {execution_time}".format(func_name=func.__name__,
                                                                                       execution_time=str(
                                                                                           end_time - start_time)))
        return result
    return wrapper


def log_execution_time_with_args(func):
    def wrapper(*args, **kwargs):
        start_time = datetime.now()
        result = func(*args, **kwargs)
        end_time = datetime.now()
        logger.info(
            "Total execution time for {func_name} with args:{args}, {kwargs} is: {execution_time}".format(
                func_name=func.__name__, args=str(args), kwargs=str(kwargs), execution_time=str(end_time - start_time)))
        return result
    return wrapper
