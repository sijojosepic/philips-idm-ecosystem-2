import json
import argparse


def positive_int(value):
    ivalue = int(value)
    if ivalue < 1:
         raise argparse.ArgumentTypeError('%s is not a valid positive int value' % value)
    return ivalue


def from_json(arg):
    if not arg:
        return
    try:
        result = json.loads(arg)
    except ValueError:
        raise argparse.ArgumentTypeError('No JSON object could be decoded from %s' % arg)
    return result
