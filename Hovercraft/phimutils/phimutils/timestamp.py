from datetime import datetime
import dateutil.parser

CUSTOM_FORMAT = '%m/%d/%Y %T.000%p'
ISO_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'
WMI_FORMAT = '%Y%m%d%H%M%S.000000+000'


def custom_timestamp(timestamp=None):
    """Returns a string representation of the timestamp in custom format
    
    if timestamp is not defined or not a valid input it returns the current timestamp"""
    d = datetime.now()
    if timestamp:
        try:
            d = datetime.fromtimestamp(float(timestamp))
        except ValueError:
            pass
    return custom_datetime(d)
    
    
def iso_timestamp(timestamp=None):
    """Returns a string representation of the timestamp in iso format
    
    if timestamp is not defined or not a valid input it returns the current timestamp"""
    d = datetime.utcnow()
    if timestamp:
        try:
            d = datetime.utcfromtimestamp(float(timestamp))
        except ValueError:
            pass
    return iso_datetime(d)


def dt_fromiso(datestring):
    """Returns a datetime object from parsing the datestring dropping timezone"""
    return dateutil.parser.parse(datestring, ignoretz=True)
    
    
def iso_datetime(dt):
    """Returns a string representation of the datetime object in iso format. timezone is ignored"""
    return dt.strftime(ISO_FORMAT)


def custom_datetime(dt):
    """Returns a string representation of the datetime object in custom format. timezone is ignored"""
    return dt.strftime(CUSTOM_FORMAT)


def wmi_datetime(dt):
    """Returns a string representation of the datetime object in wmi format. timezone is ignored"""
    return dt.strftime(WMI_FORMAT)
