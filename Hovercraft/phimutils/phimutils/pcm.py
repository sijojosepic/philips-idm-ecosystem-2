import re
import os
import math

from lxml import objectify, etree
from enums import PackageStatus, FileExtensions

class PCMPackage(object):
    def __init__(self, filepath):
        self.doc_tree = objectify.parse(filepath).getroot()
        self.__filepath = filepath

    @property
    def Name(self):
        return self.doc_tree["Name"].text

    @property
    def Version(self):
        return self.doc_tree["Version"].text

    @property
    def Package_Name(self):
        return "{name}-{version}".format(name=self.Name, version=self.Version)

    @property
    def Description(self):
        return self.doc_tree["Description"].text

    def get_required_databags(self):
        return list(set(re.findall(r'(?:\[databag@)(.*?)(?:\])', etree.tostring(self.doc_tree), re.IGNORECASE | re.DOTALL)))

    def get_dependencies(self):
        dependencies = {}

        for dep in self.doc_tree["Dependencies"].iterchildren() :
            try :
                dependencies[str(dep["Name"])] = str(dep["Version"]) #str needed else 1.2 comes through as float
            except AttributeError :
                pass
        return dependencies

    @property
    def part_file(self):
        return os.path.splitext(self.__filepath)[0] + ''.join(FileExtensions.ZIP_SUFFIX) + ''.join(
            FileExtensions.PART_SUFFIX)

    @property
    def zip_file(self):
        return os.path.splitext(self.__filepath)[0] + ''.join(FileExtensions.ZIP_SUFFIX)

    def file_exists(self, file_path):
        return os.path.isfile(file_path)

    def get_download_status_and_filename(self):
        """
        This method returns the download status along with file name.
        e.g:
            If file is partly downloaded than returns (True , filename)
            If file is fully downloaded than returns (False, filename).
        """
        part_file = self.part_file
        zip_file = self.zip_file
        # If there is .part file exists so it means file is being downloading.
        party_downloaded = True if self.file_exists(part_file) else False

        # If there is no part file and there a zip file so it means file is successfully downloaded.
        # TODO: Need a better way to check if file is downloaded. Exp: comparing file size
        file_name = part_file if party_downloaded else (zip_file if self.file_exists(zip_file) else None)
        return party_downloaded, file_name

    def get_file_size(self, filepath):
        """
        This method gets the file size in bytes and converts it into readable format:
        e.g: 10 KB, 20 MB
        """
        size_bytes = os.path.getsize(filepath)
        file_size = self.convert_size(size_bytes)
        return file_size

    def get_download_status_and_size(self):
        """
        Returns the file downloading status and file size.
        """
        partly_downloaded, file_name = self.get_download_status_and_filename()

        if partly_downloaded:
            status = PackageStatus.INPROGRESS
            fsize = self.get_file_size(file_name)
        elif file_name:
            status = PackageStatus.AVAILABLE
            fsize = self.get_file_size(file_name)
        else:
            status = PackageStatus.NONE
            fsize = '0B'
        return status, fsize

    def convert_size(self, size_bytes):
        """
        Converts the byte size into KB, MB, GB and so on
        It also checks and return if size_bytes fall under KB or MB or GB
        Exp:
        If byte sizes are: 1500, 1500000, 1500000000
        Respected value will be returned: 1.46 KB , 1.43 MB, 1.4 GB
        """
        if size_bytes == 0:
            return "0B"
        size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
        try:
            index = int(math.floor(math.log(size_bytes, 1024)))
            pow = math.pow(1024, index)
            size = round(size_bytes / pow, 2)
        except ValueError:
            return "0B"
        return "%s %s" % (size, size_name[index])
