import unittest
from mock import patch


class PhimutilsPloggingTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phimutils import plogging
        self.module = plogging

    def test_check_variableValues(self):
        self.assertEqual(self.module.LOG_DATE_FORMAT, str('%Y-%m-%dT%H:%M:%S'))
        self.assertEqual(self.module.LOG_FORMAT, str('%(asctime)s.%(msecs).06dZ - %(process)d - %(name)s - %(levelname)s - %(message)s'))

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

if __name__ == '__main__':
    unittest.main()
