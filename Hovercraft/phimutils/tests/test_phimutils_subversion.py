import unittest
from mock import MagicMock, patch
 
class PhimutilsSubversionTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_os = MagicMock(name='os')
        self.mock_pysvn = MagicMock(name='pysvn')
        self.mock_shutil = MagicMock(name='shutil')
        modules = {
            'os': self.mock_os,
            'pysvn': self.mock_pysvn,
            'shutil': self.mock_shutil,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils.subversion import PhiSubversion
        self.module = PhiSubversion( 'url', 'work_dir', 'username', 'password')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
 
    def test__item_full_path(self):
        self.mock_os.sep = 'val'
        self.mock_os.path.join = MagicMock(return_value='value')
        self.assertEqual(self.module._item_full_path('item'), 'value')

    def test_remove_with_if(self):
        self.module._item_full_path = MagicMock(return_value = 'item_fp')
        self.mock_os.path.exists.return_value = True
        self.module.client.remove = MagicMock(return_value='')
        self.module.remove('item')
        self.module.client.remove.assert_called_once_with('item_fp')

    def test_remove_no_if(self):
        self.module._item_full_path = MagicMock(return_value = 'item_fp')
        self.mock_os.path.exists.return_value = False
        self.module.client.remove = MagicMock(return_value='')
        self.module.remove('item')
        self.module.client.remove.assert_not_called()

    def test_add_with_if(self):
        self.module._item_full_path = MagicMock(return_value = 'item_fp')
        self.module.client.status('item_fp')[0].text_status = '1'
        self.mock_pysvn.wc_status_kind.unversioned = '1'
        self.module.add('item')
        self.module.client.add.assert_called_once_with('item_fp')
        
    def test_add_no_if(self):
        self.module._item_full_path = MagicMock(return_value = 'item_fp')
        self.module.client.status('item_fp')[0].text_status = '1'
        self.mock_pysvn.wc_status_kind.unversioned = '0'
        self.module.add('item')
        self.module.client.add.assert_not_called()

    def test_login(self):
        self.assertEqual(self.module.login('realm', 'username', 'may_save'), (True, 'username', 'password', False))

    def test_client_without__client(self):
        client = self.mock_pysvn.Client()
        self.assertEqual(self.module.client, client)

    def test_client_with__client(self):
        self.module._client = 'value'
        self.assertEqual(self.module.client, 'value')

    def test_setup_wc(self):
        self.module.setup_wc()
        self.module.client.checkout.assert_called_once_with('url', 'work_dir', ignore_externals=True)

    def test_status(self):
        status_groups = {
            self.mock_pysvn.wc_status_kind.added: ['added', []],
            self.mock_pysvn.wc_status_kind.deleted: ['removed', []],
            self.mock_pysvn.wc_status_kind.modified: ['changed', []],
            self.mock_pysvn.wc_status_kind.conflicted: ['conflicts', []],
            self.mock_pysvn.wc_status_kind.unversioned: ['unversioned', []]
        }
        self.mock_os.sep = 'val'
        self.module.client.status = MagicMock(return_value=MagicMock())
        self.assertEqual(self.module.status(), dict(status_groups.values()))

    def test_commit(self):
        self.mock_os.sep = 'val'
        self.module.client.checkin = MagicMock(return_value='value')
        self.assertEqual( self.module.commit(), 'value')



if __name__ == '__main__':
    unittest.main()