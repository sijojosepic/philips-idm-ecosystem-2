import unittest
from mock import patch
from datetime import datetime


## If freezegun was to be available
#>>> from freezegun import freeze_time
#>>> freezer = freeze_time('1980-8-8')
#>>> freezer.start()
## <do call here>
#>>> freezer.stop()


class PhimutilsTimestampTestCase(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            modules = {}
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()

            from phimutils import timestamp
            self.module = timestamp

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class PhimutilsTimestampTest(PhimutilsTimestampTestCase.TestCase):
    def setUp(self):
        PhimutilsTimestampTestCase.TestCase.setUp(self)

        self.patcher = patch('phimutils.timestamp.datetime')
        self.mock_tiem = self.patcher.start()
        self.mock_tiem.fromtimestamp.side_effect = datetime.utcfromtimestamp
        self.mock_tiem.utcfromtimestamp.side_effect = datetime.utcfromtimestamp

    def tearDown(self):
        PhimutilsTimestampTestCase.TestCase.tearDown(self)
        self.patcher.stop()

    def test_custom_timestamp_normal(self):
        self.assertEqual(self.module.custom_timestamp('1378850585'), '09/10/2013 22:03:05.000PM')

    def test_custom_timestamp_short(self):
        self.assertEqual(self.module.custom_timestamp(25), '01/01/1970 00:00:25.000AM')

    def test_iso_timestamp_normal(self):
        self.assertEqual(self.module.iso_timestamp('1378850585'), '2013-09-10T22:03:05.000000Z')

    def test_iso_timestamp_short(self):
        self.assertEqual(self.module.iso_timestamp(25), '1970-01-01T00:00:25.000000Z')

    def test_dt_fromiso(self):
        self.assertEqual(self.module.dt_fromiso('2013-09-10T22:03:00.456000Z'), datetime(2013, 9, 10, 22, 3, 0, 456000))

    def test_dt_fromiso_tzincluded(self):
        self.assertEqual(
            self.module.dt_fromiso('2013-09-10T22:03:00.456000+0800'),
            datetime(2013, 9, 10, 22, 3, 0, 456000)
        )

    def test_iso_datetime(self):
        self.assertEqual(
            self.module.iso_datetime(datetime(2013, 9, 10, 22, 3, 0, 456000)),
            '2013-09-10T22:03:00.456000Z'
        )

    def test_custom_datetime(self):
        self.assertEqual(
            self.module.custom_datetime(datetime(2013, 9, 10, 22, 3, 0, 456000)),
            '09/10/2013 22:03:00.000PM'
        )

    def test_wmi_datetime(self):
        self.assertEqual(self.module.wmi_datetime(datetime(2013, 9, 10, 22, 3, 0, 456000)), '20130910220300.000000+000')


class PhimutilsTimestampFrozenTest(PhimutilsTimestampTestCase.TestCase):
    def setUp(self):
        PhimutilsTimestampTestCase.TestCase.setUp(self)

        self.patcher = patch('phimutils.timestamp.datetime')
        self.mock_tiem = self.patcher.start()
        self.mock_tiem.now.return_value = datetime.utcfromtimestamp(1378850580.456)
        self.mock_tiem.fromtimestamp.side_effect = datetime.utcfromtimestamp

    def tearDown(self):
        PhimutilsTimestampTestCase.TestCase.tearDown(self)
        self.patcher.stop()

    def test_custom_timestamp_none(self):
        self.assertEqual(self.module.custom_timestamp(None), '09/10/2013 22:03:00.000PM')

    def test_custom_timestamp_blank(self):
        self.assertEqual(self.module.custom_timestamp(''), '09/10/2013 22:03:00.000PM')

    def test_custom_timestamp_nothing(self):
        self.assertEqual(self.module.custom_timestamp(), '09/10/2013 22:03:00.000PM')

    def test_custom_timestamp_too_long_date(self):
        self.assertEqual(
            self.module.custom_timestamp(4375897589347573489573489538997853459),
            '09/10/2013 22:03:00.000PM'
        )

    def test_custom_timestamp_not_a_date(self):
        self.assertEqual(self.module.custom_timestamp('this is not a date'), '09/10/2013 22:03:00.000PM')


class PhimutilsTimestampFrozenUTCTest(PhimutilsTimestampTestCase.TestCase):
    def setUp(self):
        PhimutilsTimestampTestCase.TestCase.setUp(self)

        self.patcher = patch('phimutils.timestamp.datetime')
        self.mock_tiem = self.patcher.start()
        self.mock_tiem.utcnow.return_value = datetime.utcfromtimestamp(1378850580.456)
        self.mock_tiem.utcfromtimestamp.side_effect = datetime.utcfromtimestamp

    def tearDown(self):
        PhimutilsTimestampTestCase.TestCase.tearDown(self)
        self.patcher.stop()

    def test_iso_timestamp_none(self):
        self.assertEqual(self.module.iso_timestamp(None), '2013-09-10T22:03:00.456000Z')

    def test_iso_timestamp_blank(self):
        self.assertEqual(self.module.iso_timestamp(''), '2013-09-10T22:03:00.456000Z')

    def test_iso_timestamp_nothing(self):
        self.assertEqual(self.module.iso_timestamp(), '2013-09-10T22:03:00.456000Z')

    def test_iso_timestamp_too_long_date(self):
        self.assertEqual(
            self.module.iso_timestamp(43758975893475734895734895389978534598),
            '2013-09-10T22:03:00.456000Z'
        )

    def test_iso_timestamp_not_a_date(self):
        self.assertEqual(self.module.iso_timestamp('this is not a date'), '2013-09-10T22:03:00.456000Z')


if __name__ == '__main__':
    unittest.main()
