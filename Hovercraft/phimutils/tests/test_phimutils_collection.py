import unittest
from mock import MagicMock, patch


class PhimutilsDeciderTermTreeNodeTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import collection
        self.module = collection

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_DictConditional_is_allowed(self):
        obj = self.module.DictConditional()
        def test_error(self):
                with self.assertRaises(NotImplementedError): obj.is_allowed({'val': 1})

    def test_update_error(self):
        def test_error(self):
                with self.assertRaises(TypeError): self.module.DictConditional('more','than','one','args','here')

    def test_update_error(self):
        obj = self.module.DictConditional()
        obj.is_allowed = MagicMock(return_value='val')
        self.assertEqual(obj.setdefault('key','val'), 'val')

    def test_dictnofalsey_is_allowed(self):
        obj2 = self.module.DictConditional()
        obj = self.module.DictNoFalsey(obj2)
        self.assertEqual((obj.is_allowed('val')), True)


    def test_dictnoempty_init(self):
        self.assertEqual(
            self.module.DictNoEmpty([('a', None), ('b', 'val1')], x=12, y=None),
            {'x': 12, 'b': 'val1'}
        )

    def test_dictnoempty_init_nothing_allowed(self):
        self.assertEqual(
            self.module.DictNoEmpty([('a', None), ('b', '')], x='', y=None),
            {}
        )

    def test_dictnoempty_is_allowed(self):
        tester = self.module.DictNoEmpty()
        self.assertTrue(tester.is_allowed('string'))

    def test_dictnoempty_is_allowed_false_value(self):
        tester = self.module.DictNoEmpty()
        self.assertTrue(tester.is_allowed(False))

    def test_dictnoempty_is_allowed_zero_value(self):
        tester = self.module.DictNoEmpty()
        self.assertTrue(tester.is_allowed(0))

    def test_dictnoempty_is_allowed_empty_list(self):
        tester = self.module.DictNoEmpty()
        self.assertTrue(tester.is_allowed([]))

    def test_dictnoempty_is_allowed_empty_dict(self):
        tester = self.module.DictNoEmpty()
        self.assertTrue(tester.is_allowed({}))

    def test_dictnoempty_is_allowed_none(self):
        tester = self.module.DictNoEmpty()
        self.assertFalse(tester.is_allowed(None))

    def test_dictnoempty_is_allowed_empty_string(self):
        tester = self.module.DictNoEmpty()
        self.assertFalse(tester.is_allowed(''))

    def test_dictnoempty_setitem(self):
        result = self.module.DictNoEmpty()
        result['a'] = 5
        self.assertEqual(result, {'a': 5})

    def test_dictnoempty_setitem_replace_to_none(self):
        result = self.module.DictNoEmpty(a=5, x=6)
        result['a'] = None
        self.assertEqual(result, {'x': 6})

    def test_dictnoempty_update(self):
        tester = self.module.DictNoEmpty(a=5, c=7, x=6)
        tester.update({'a': None, 'b': 'val2'}, c=None, j=[])
        self.assertEqual(tester, {'x': 6, 'b': 'val2', 'j': []})


if __name__ == '__main__':
    unittest.main()
