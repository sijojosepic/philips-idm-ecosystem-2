import unittest
from mock import MagicMock, patch

class PhimutilsThresholdTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_linecache = MagicMock(name='linecache')
        self.mock_tbconfig = MagicMock(name='tbconfig')
        modules = {
            'linecache':self.mock_linecache,
            'tbconfig': self.mock_tbconfig,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import threshold
        self.module = threshold

        self.module.PACKAGE_CONFIG = {'NAGIOS_THRESHOLD_FILE':'/test/threshold.cfg'}
        self.module.VMWARE = {'SECRET_FILE':'/test/secret'}

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()


    def test_get_threshold_value(self):
        self.mock_linecache.getline.return_value = "/test/sercret"
        mock_ng = MagicMock(name="get_resources")
        mock_ng.get_resources.return_value = [2]
        self.module.NagiosResourcer = MagicMock(name="NagiosResourcer", return_value=mock_ng)

        self.assertEqual(self.module.get_threshold_value("TEST_KEY"), 2)



if __name__ == '__main__':
    unittest.main()
