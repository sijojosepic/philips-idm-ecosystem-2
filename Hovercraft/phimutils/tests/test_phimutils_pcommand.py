import unittest
from mock import MagicMock, patch
from subprocess import CalledProcessError 


class PhimutilsPcommandTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_subprocess = MagicMock(name='subprocess')
        self.mock_shlex = MagicMock(name='shlex')
        modules = {
            'subprocess': self.mock_subprocess,
            'shlex': self.mock_shlex,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import pcommand
        self.module = pcommand 

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_execute_command_CalledProcessError(self):
        self.module.subprocess.CalledProcessError = CalledProcessError
        self.mock_subprocess.check_call = MagicMock(side_effect=CalledProcessError('1', '2'))
        self.assertRaises(CalledProcessError, self.module.execute_command,'command', None, None, None, False)

    def test_execute_command_OSError(self):  
        self.mock_subprocess.check_call = MagicMock(side_effect=OSError)
        self.assertRaises(OSError, self.module.execute_command,'command', None, None, None, False)
   
    def test_execute_command_Exception(self): 
        self.mock_subprocess.check_call = MagicMock(side_effect=Exception)
        self.assertRaises(Exception, self.module.execute_command, 'command', None, None, None, False)


if __name__ == '__main__':
    unittest.main()
