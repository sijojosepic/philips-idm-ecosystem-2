import unittest
from mock import MagicMock, patch


class PhimutilsQueuemanTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_kombu = MagicMock(name='kombu')
        self.mock_property = MagicMock(name='property', cached_property=property)
        modules = {
            'kombu': self.mock_kombu,
            'kombu.Exchange': self.mock_kombu.Exchange,
            'kombu.Queue': self.mock_kombu.Queue,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils.queueman import QueueMan
        self.module = QueueMan(MagicMock())

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test__create_exchanges_and_queues(self):
        obj = [ ('exchange', 'e_queues')]
        self.module.exchange_queue_map.iteritems = MagicMock(return_value=obj)
        self.module._create_exchanges_and_queues()
        self.mock_kombu.Exchange.assert_called_once_with('exchange', type="fanout")
        self.assertEqual(self.mock_kombu.Queue.call_count,len('e_queues'))
    
    def test_exchanges_without_if_execution(self):
        self.module._exchanges = 'val'
        self.assertEqual(self.module.exchanges, 'val')

    def test_exchanges_with_if_execution(self):
        self.module._exchanges = None
        self.module._create_exchanges_and_queues = MagicMock(return_value='return')
        self.module.exchanges
        self.module._create_exchanges_and_queues.assert_called_once_with()

    def test_queues_without_if_execution(self):
        self.module._queues = 'val'
        self.assertEqual(self.module.queues, 'val')
        
    def test_queues_with_if_execution(self):
        self.module._queues = None
        self.module._create_exchanges_and_queues = MagicMock(return_value='return')
        self.module.queues
        self.module._create_exchanges_and_queues.assert_called_once_with()

    def test_get_queue(self):
        self.module.queues['queue_name'] = 'value'
        self.assertEqual(self.module.get_queue('queue_name'), 'value')

    def test_get_exchange(self):
        self.module.exchanges['exchange_name'] = 'value'
        self.assertEqual(self.module.get_exchange('exchange_name'), 'value')

if __name__ == '__main__':
    unittest.main()