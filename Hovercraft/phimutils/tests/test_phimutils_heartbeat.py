import unittest
from mock import MagicMock, patch

class PhimutilsHeartbeatTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_emt = MagicMock(name='email.mime.text')
        self.mock_smtplib = MagicMock(name='smtplib')
        modules = {
            'email.mime.text': self.mock_emt,
            'smtplib': self.mock_smtplib,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import heartbeat
        self.module = heartbeat 

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_send_message_without_user(self): 
        self.module.send_message('message', 'smtpfrom', 'smtpto', 'smtpserver', subject=None, user=None, password='password', port=25)  
        self.mock_smtplib.SMTP().login.assert_not_called()
    
    def test_send_message_with_user(self): 
        self.module.send_message('message', 'smtpfrom', 'smtpto', 'smtpserver', subject=None, user='user', password='password', port=25)  
        self.mock_smtplib.SMTP().login.assert_called_once_with('user', 'password')
       
    def test_create_msg(self):
        messagestring = '''?<?xml version="1.0" encoding="utf-8"?>\n<MESSAGE_ROOT>\n  <VERSION>1.0</VERSION>\n  <CATEGORY>LM_NT_EVENT</CATEGORY>\n  <COMPONENT>RemoteEventLogMonitor</COMPONENT>\n  <SITE_NAME>siteid</SITE_NAME>\n  <LOCAL_HOST>hostname</LOCAL_HOST>\n  <CLUSTER>NO</CLUSTER>\n  <FILTER_CONFIG_VERSION>1.0.1.0</FILTER_CONFIG_VERSION>\n  <LOCATION>Main Location</LOCATION>\n  <BODY>\n    <REPORT>\n      <SITE_NAME>siteid</SITE_NAME>\n      <LOCAL_HOST>hostname</LOCAL_HOST>\n      <CLUSTER>NO</CLUSTER>\n      <LOGMESSAGE>\n        <ORIGINATING_HOST>hostname</ORIGINATING_HOST>\n        <ORIGINATING_HOST_IPS> hostaddress </ORIGINATING_HOST_IPS>\n        <TIMESTAMP>timestamp</TIMESTAMP>\n        <PRIORITY>LM_NT_EVENT</PRIORITY>\n        <PRIORITY_CODE>12345</PRIORITY_CODE>\n        <THREAD_ID>1</THREAD_ID>\n        <MESSAGE_BODY>Application log:\nerrtype [hostname] generated error Event errcode: output\n\n</MESSAGE_BODY>\n      </LOGMESSAGE>\n      <SYSTEM_INFO>Module Info: hostname hostaddress</SYSTEM_INFO>\n    </REPORT>\n  </BODY>\n</MESSAGE_ROOT>'''
        self.assertEqual(self.module.create_msg('siteid', 'hostname', 'hostaddress', 'timestamp', 'errtype', 'errcode', 'output'), messagestring )

if __name__ == '__main__':
    unittest.main()
