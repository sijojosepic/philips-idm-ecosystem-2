import unittest
from mock import patch


class PhimutilsPassiveTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import passive
        self.module = passive

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_service_string_simple(self):
        self.assertEqual(
            self.module.service_string('localhost', 'service 1', 0, 'output goes here', 1398199182),
            '[1398199182] PROCESS_SERVICE_CHECK_RESULT;localhost;service 1;0;output goes here'
        )

    def test_service_string_simple_odd_timestamp(self):
        self.assertEqual(
            self.module.service_string('localhost', 'service 1', '1', 'output goes here', '1398199182.445'),
            '[1398199182] PROCESS_SERVICE_CHECK_RESULT;localhost;service 1;1;output goes here'
        )

    def test_host_string_simple(self):
        self.assertEqual(
            self.module.host_string('localhost', '3', 'output goes here', '1398199182.445'),
            '[1398199182] PROCESS_HOST_CHECK_RESULT;localhost;3;output goes here'
        )

    def test_get_state_good_int_0(self):
        self.assertEqual(self.module.get_state(0), 0)

    def test_get_state_good_int(self):
        self.assertEqual(self.module.get_state(1), 1)

    def test_get_state_good_int_in_string(self):
        self.assertEqual(self.module.get_state('1'), 1)

    def test_get_state_bad_int(self):
        self.assertRaises(ValueError, self.module.get_state, 9)

    def test_get_state_good_string(self):
        self.assertEqual(self.module.get_state('CRITICAL'), 2)

    def test_get_state_good_string_lower(self):
        self.assertEqual(self.module.get_state('warning'), 1)

    def test_get_state_bad_string(self):
        self.assertRaises(ValueError, self.module.get_state, 'FAIL')


if __name__ == '__main__':
    unittest.main()