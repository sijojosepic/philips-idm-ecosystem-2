import unittest
from mock import MagicMock, patch, call


class PhimutilsPerfdataTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        modules = {}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import perfdata
        self.module = perfdata

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_perfdata_tokens_one_metric(self):
        self.assertEqual(self.module.get_perfdata_tokens('x=34556m;;;'), [('x', '34556', 'm', '', '', '', '')])

    def test_get_perfdata_tokens_three_metrics_one_invalid_number(self):
        self.assertEqual(
            self.module.get_perfdata_tokens('x=34m;2;3;4 y=44 z=5.5.5m;;;;4;'),
            [
                ('x', '34', 'm', '2', '3', '4', ''),
                ('y', '44', '', '', '', '', ''),
                ('z', '5.5.5', 'm', '', '', '', '4')
            ]
        )

    def test_get_perfdata_tokens_too_many_attributes(self):
        self.assertEqual(
            self.module.get_perfdata_tokens('sss=44;5;6;7;8;9;97;7;6'),
            [('sss', '44', '', '5', '6', '7', '8')]
        )

    def test_get_perfdata_tokens_missing_value(self):
        self.assertEqual(self.module.get_perfdata_tokens('x=m'), [])

    def test_get_perfdata_tokens_missing_label(self):
        self.assertEqual(self.module.get_perfdata_tokens('=m'), [])

    def test_get_perfdata_tokens_space_in_label(self):
        self.assertEqual(
            self.module.get_perfdata_tokens("'this is a variable'=541ZIG;45;"),
            [("'this is a variable'", '541', 'ZIG', '45', '', '', '')]
        )

    def test_get_perfdata_tokens_missing_end_quote_in_label(self):
        self.assertEqual(
            self.module.get_perfdata_tokens(
                "'this is a variable'=541ZIG;45; =m this is garbage 'broken=9;4;5;67"
            ),
            [("'this is a variable'", '541', 'ZIG', '45', '', '', ''), ("'broken", '9', '', '4', '5', '67', '')]
        )

    def test_perfdata_one_metric(self):
        self.assertEqual(
            self.module.PerfData('x', '34556', 'm', '', '', '', '').to_dict(),
            {'value': 34556.0, 'uom': 'm', 'label': 'x'}
        )

    def test_perfdata_invalid_number(self):
        self.assertRaises(ValueError, self.module.PerfData, 'z', '5.5.5', 'm', '', '', '', '4')

    def test_perfdata_empty_number(self):
        self.assertRaises(ValueError, self.module.PerfData, 'z', '', 'm', '', '', '', '4')

    def test_perfdata_empty_label(self):
        self.assertRaises(ValueError, self.module.PerfData, '', '5.5.5', 'm', '', '', '', '4')

    def test_perfdata_all_values(self):
        self.assertEqual(
            self.module.PerfData('sss', '44', 'GB', '5', '6', '7', '8').to_dict(),
            {'warn': 5.0, 'uom': 'GB', 'min': 7.0, 'crit': 6.0, 'max': 8.0, 'value': 44.0, 'label': 'sss'}
        )

    def test_perfdata_missing_end_quote_in_label(self):
        self.assertEqual(
            self.module.PerfData("'broken", '9', '', '4', '5', '67', '').to_dict(),
            {'warn': 4.0, 'crit': 5.0, 'label': 'broken', 'value': 9.0, 'min': 67.0}
        )

    def test_perfdata_space_in_label(self):
        self.assertEqual(
            self.module.PerfData("'this is a variable'", '541', 'ZIG', '45', '', '', '').to_dict(),
            {'warn': 45.0, 'value': 541.0, 'uom': 'ZIG', 'label': 'this is a variable'}
        )

    def test_perfdata2dict_empty(self):
        self.assertEqual(self.module.perfdata2dict(''), [])

    def test_perfdata2dict(self):
        perfdata_result = iter([{'value': 3, 'label': 'xx'}, {'value': 5, 'label': 'xy'}])
        self.module.get_perfdata = MagicMock(name='get_perfdata', return_value=perfdata_result)
        self.assertEqual(
            self.module.perfdata2dict('xx=3 xy=5'),
            [{'value': 3, 'label': 'xx'}, {'value': 5, 'label': 'xy'}]
        )
        self.module.get_perfdata.assert_called_once_with('xx=3 xy=5')

    def test_get_perfdata(self):
        self.module.get_perfdata_tokens = MagicMock(name='get_perfdata_tokens')
        self.module.get_perfdata_tokens.return_value = [
            ('xy', '5', '%', '', '', '', ''),
            ('', '3.6', '%', '', '', '', ''),
            ('xx', '3', '', '', '', '', ''),
        ]
        mock_perf = MagicMock()
        mock_perf.to_dict.side_effect = [{'label': 'xy', 'value': 5, 'uom': '%'}, {'label': 'xx', 'value': 3}]
        self.module.PerfData = MagicMock(name='PerfData')
        self.module.PerfData.side_effect = [mock_perf, ValueError, mock_perf]
        self.assertEqual(
            list(self.module.get_perfdata("xx=3 ''=3.6 xy=5")),
            [{'uom': '%', 'value': 5, 'label': 'xy'}, {'value': 3, 'label': 'xx'}]
        )
        self.assertEqual(
            self.module.PerfData.mock_calls,
            [
                call('xy', '5', '%', '', '', '', ''),
                call('', '3.6', '%', '', '', '', ''),
                call('xx', '3', '', '', '', '', '')
            ]
        )
        self.assertEqual(mock_perf.to_dict.mock_calls, [call(), call()])

    def test_info2dict_empty(self):
        self.assertEqual(self.module.info2dict(""), {})

    def test_info2dict_one(self):
        self.assertEqual(self.module.info2dict('++I# some thing = x'), {'some thing': 'x'})

    def test_info2dict_two_plus_invalid(self):
        self.assertEqual(
            self.module.info2dict('++I# some thing = x; x=y ; ddd dd = ddd ='),
            {'x': 'y', 'some thing': 'x'}
        )

    def test_info2dict_three_valid_with_space(self):
        self.assertEqual(
            self.module.info2dict('++I# some thing = x; x=y ; ddd dd = ddd '),
            {'x': 'y', 'ddd dd': 'ddd', 'some thing': 'x'}
        )

    def test_info2dict_valid_semicolon_end(self):
        self.assertEqual(self.module.info2dict('++I# some thing = x; '), {'some thing': 'x'})

    def test_info2dict_valid_quote_and_space_need_stripping(self):
        self.assertEqual(self.module.info2dict('++I# some thing = " x value "'), {'some thing': 'x value'})

    def test_info2dict_none(self):
        self.assertEqual(self.module.info2dict(None), {})

    def test_info2dict_only_identifier(self):
        self.assertEqual(self.module.info2dict('++I#'), {})

    def test_info2dict_meaningless_string(self):
        self.assertEqual(self.module.info2dict('this does not have the string'), {})

    def test_info2dict_wrong_identifier(self):
        self.assertEqual(self.module.info2dict('++i# x = wrong string'), {})

    def test_info2dict_missing_label(self):
        self.assertEqual(self.module.info2dict('++I# = wrong string'), {})

    def test_raw_metric_not_all_attributes(self):
        self.assertEqual(self.module.raw_metric(label='temp read', value=5, max=6, min='3'), "'temp read'=5;;;3;6")

    def test_raw_metric_all_attributes(self):
        self.assertEqual(
            self.module.raw_metric(label='magic', crit='1', value=5, max=6, min='3', uom='aws', warn=2),
            "'magic'=5aws;2;1;3;6"
        )

    def test_raw_metric_no_tail_attributes(self):
        self.assertEqual(self.module.raw_metric(label='magic', value=5), "'magic'=5")

    def test_get_numeric_value(self):
        self.assertEqual(self.module.get_numeric_value('5.4'), 5.4)

    def test_get_numeric_value_already_int(self):
        self.assertEqual(self.module.get_numeric_value(5), 5)

    def test_get_numeric_value_already_float(self):
        self.assertEqual(self.module.get_numeric_value(5.6), 5.6)

    def test_get_numeric_value_not_a_number(self):
        self.assertEqual(self.module.get_numeric_value('5.6.6'), None)


if __name__ == '__main__':
    unittest.main()
