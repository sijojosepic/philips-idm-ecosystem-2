import unittest
from mock import MagicMock, patch


class FakeContext(object):
    def __init__(self, fake_obj):
        self.fake_obj = fake_obj

    def execute(self):
        return "Not Package Found"


class PhimutilsCleanUpTest(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_package_service = MagicMock(name='package_service')
        modules = {
            'package_service': self.mock_package_service,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from phimutils import cleanup
        self.module = cleanup

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_cleanup_run(self):
        mock_service_obj = MagicMock(name="service_obj")
        self.cleanup = self.module.CleanUp()
        self.cleanup.CleanUpContext = MagicMock(name='CleanUpContext', return_value=FakeContext(mock_service_obj))
        self.assertEqual(self.cleanup.run(mock_service_obj), "Not Package Found")

    def test_cleanup_context_execute(self):
        mock_service_obj = MagicMock(name="service_obj")
        mock_service_obj.execute.return_value = "Service Executed"
        self.cleanup_context = self.module.CleanUpContext(mock_service_obj)
        self.assertEqual(self.cleanup_context.execute(), "Service Executed")

    def test_cleanup_service_string(self):
        self.assertEqual(self.module.CleanUpServices.SERVICES['package'],
                         self.mock_package_service.PackageCleanUpService)

    def test_get_package_service(self):
        self.service = self.module.Service()
        service_name = self.module.CleanUpServices.PACKAGE
        self.assertEqual(self.service.get_service(service_name), self.mock_package_service.PackageCleanUpService)


if __name__ == '__main__':
    unittest.main()
