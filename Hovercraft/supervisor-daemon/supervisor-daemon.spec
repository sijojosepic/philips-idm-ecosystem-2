# spec file for package 'supervisor-daemon' (version '3.0')
#
# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

# Basic Information
Name:      supervisor-daemon
Version:   3.0
Release:   1%{?dist}
Summary:   supervisor-daemon provides scripts to daemonize supervisor

Group:     none
License:   GPL
URL:       http://supervisord.org/
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  shadow-utils
Requires:  initscripts
Requires(post):   chkconfig
Requires(preun):  chkconfig
# This is for /sbin/service
Requires(preun):  initscripts
Requires(postun): initscripts
Provides:  supervisor-daemon

%description
supervisor-daemon provides scripts to daemonize supervisor

%prep
%setup -q

%build
echo OK


%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/sysconfig
%{__install} -d -m 0755 %{buildroot}%{_initrddir}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/redhat-sysconfig-jkoppe %{buildroot}%{_sysconfdir}/sysconfig/supervisord
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/redhat-init-jkoppe %{buildroot}%{_initrddir}/supervisord
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/supervisord.conf %{buildroot}%{_sysconfdir}/
%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/log/supervisor
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/supervisord.d


%clean
%{__rm} -rf %{buildroot}


%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    # %{_sbindir} != /sbin
    /sbin/chkconfig --add supervisord
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    # %{_sbindir} != /sbin
    /sbin/service supervisord stop >/dev/null 2>&1
    /sbin/chkconfig --del supervisord
fi

#%postun
if [ "$1" -ge "1" ] ; then
    # package upgrade
    # does this need to be restarted?
    # %{_sbindir} != /sbin
    /sbin/service supervisord restart >/dev/null 2>&1 || :
fi

%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/sysconfig/supervisord
%config %{_sysconfdir}/supervisord.conf
%attr(0755,root,root) %{_initrddir}/supervisord
%dir %{_localstatedir}/log/supervisor
%dir %{_sysconfdir}/supervisord.d


%changelog
* Thu Apr 24 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
