from celery import Task
from celery.utils import cached_property
import redis


class RedisTask(Task):
    abstract = True
    _url = None

    @cached_property
    def rc(self):
        return redis.StrictRedis.from_url(self._url)


def create_redis_task(url):
    class RDT(RedisTask):
        abstract = True
        _url = url

    return RDT
