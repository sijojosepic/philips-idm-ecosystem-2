from celery import Task
from celery.utils import cached_property
import json


class MapFileTask(Task):
    abstract = True

    def file_map(self):
        raise NotImplementedError('file_map not implemented')

    @cached_property
    def mapper(self):
        return self.file_map()


def create_json_map_file_task(map_file):
    class MFT(MapFileTask):
        abstract = True

        def file_map(self):
            try:
                with open(map_file) as se_config:
                    result = json.load(se_config)
            except (ValueError, IOError):
                result = {}
            return result
    return MFT
