# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}


# Basic Information
Name:      phim-uwsgi
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-uwsgi setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  PyXML
Requires:  libxml2-devel
Requires:  pcre-devel
Requires:  nginx
Requires:  shadow-utils
Requires:  initscripts
Requires(post):   chkconfig
Requires(preun):  chkconfig
# This is for /sbin/service
Requires(preun):  initscripts
Requires(postun): initscripts
Provides:  phim-uwsgi

%description
phim-uwsgi setup, includes daemonization script for uwsgi.


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_initrddir}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/uwsgi.init %{buildroot}%{_initrddir}/uwsgi
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/logrotate.d
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/uwsgi.logrotate %{buildroot}%{_sysconfdir}/logrotate.d/uwsgi
#%{__install} -d -m 0755 %{buildroot}%{_localstatedir}/log/uwsgi
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/default
%{__cp} %{_builddir}/%{name}-%{version}/uwsgi.default %{buildroot}%{_sysconfdir}/default/uwsgi


%clean
%{__rm} -rf %{buildroot}

%pre
groupadd -f uwsgi
if ! getent passwd uwsgi >/dev/null ; then
    useradd -M -r --shell /bin/sh --home-dir /usr/lib/philips/uwsgi -g uwsgi uwsgi
fi

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    # %{_sbindir} != /sbin
    /sbin/chkconfig --add uwsgi
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    # %{_sbindir} != /sbin
    /sbin/service uwsgi stop >/dev/null 2>&1
    /sbin/chkconfig --del uwsgi
fi

#%postun
#if [ "$1" -ge "1" ] ; then
    # package upgrade
    # does this need to be restarted?
    # %{_sbindir} != /sbin
    # /sbin/service uwsgi restart >/dev/null 2>&1 || :
#fi

%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{_initrddir}/uwsgi
%attr(0755,root,root) %{_sysconfdir}/logrotate.d/uwsgi
%dir %{_sysconfdir}/uwsgi
%config %{_sysconfdir}/default/uwsgi


%changelog
* Tue Apr 07 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Changed config to setup emperor mode

* Mon Jan 19 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
