#!/usr/bin/env python

import requests
import sys
import os
import linecache


PSWITCH_PORT_FILE = os.environ.get('PSWITCH_PORT_FILE', '/etc/philips/pswitch_port.conf')


def url_from_args():
    port = linecache.getline(PSWITCH_PORT_FILE, 1).strip() or 8080
    hostaddress = 'http://mattis:{port}/config'.format(port=port)

    plugin = os.path.basename(sys.argv[0])
    argstring = ' '.join(sys.argv[1:])

    return os.path.join(hostaddress, plugin, argstring)


def pswitch(url):
    """get return output from service"""
    try:
        r = requests.get(url)
        r.raise_for_status()
    except requests.exceptions.RequestException:
        print('UNKNOWN - Output could not be determined')
        sys.exit(3)
    else:
        rj = r.json()
        print(rj.get('output'))
        sys.exit(rj.get('rc'))


def main():
    pswitch(url_from_args())


if __name__ == "__main__":
    main()
