#!/usr/bin/env python

import argparse
from pysphere import VIServer


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='For use with interacting with vSphere to work with snapshots and vms')
    parser.add_argument('-s', '--vcenter',
                        help='vCenter Server',
                        required=True)
    parser.add_argument('-u', '--username',
                        help='Username with privileges to call actions against the vCenter',
                        required=True)
    parser.add_argument('-p', '--password',
                        help='Associated password for the used username',
                        required=True)
    parser.add_argument('-m', '--virtual_machine',
                        help='Name of the Virtual Machine that should have actions applied',
                        required=True)
    results = parser.parse_args(args)
    return results.vcenter, results.username, results.password, results.virtual_machine


def snapper(vcenter, username, password, vm):
    con = VIServer()
    con.connect(vcenter, username, password)
    vm = con.get_vm_by_name(vm)
    vm.revert_to_snapshot()
    vm.power_on()
    con.disconnect()


def main():
    snapper(*check_arg())


if __name__ == '__main__':
    main()
