#!/usr/bin/env python
import socket
import sys
import argparse
from bottle import Bottle, request, BaseRequest
from StringIO import StringIO

BaseRequest.MEMFILE_MAX = 1024 * 1024 * 2


def check_arg(args=None):
    """Check the arguments passed"""
    parser = argparse.ArgumentParser(description='Script to setup a service to expect a callback')
    parser.add_argument('-p', '--port', help='Port number to use', required=True, type=int)
    parser.add_argument('-i', '--interface', help='IP address of the interface to bind to', default='0.0.0.0')
    results = parser.parse_args(args)
    return results.interface, results.port


class TaskOutputHandler(StringIO):
    TASK_PREFIX = 'PTASK:'

    def __init__(self, buffer=None, output=None):
        self.replacer = output
        StringIO.__init__(self, buffer)

    def write(self, s):
        if s.startswith(self.TASK_PREFIX):
            self.replacer.write(s)
        else:
            StringIO.write(self, s)


app = Bottle(catchall=False)


@app.post('/partial')
def submit_result():
    print('{prefix} {data}\n'.format(prefix=TaskOutputHandler.TASK_PREFIX, data=request.json))
    return "Success!"


@app.post('/notification')
def submit_result():
    yield "Success!"
    global callback_data
    callback_data = request.json
    # seems like double exception makes app exit, which is what is wanted
    sys.stderr.close()
    sys.exit()


def callback_server(interface, port):
    old_stderr = sys.stderr
    old_stdout = sys.stdout
    sys.stderr = TaskOutputHandler(output=old_stderr)
    sys.stdout = TaskOutputHandler(output=old_stdout)
    state = 1
    output = ''
    try:
        app.run(host=interface, port=port, quiet=True)
    except socket.error:
        output = 'Could not bind to port'
    except Exception:
        output = str(callback_data)
        if callback_data.get('state') == 'OK':
            state = 0
    finally:
        app.close()
    sys.stderr = old_stderr
    sys.stdout = old_stdout
    return state, output


def sparks(interface, port):
    state, output = callback_server(interface, port)
    print(output)
    sys.exit(state)

callback_data = {}


if __name__ == '__main__':
    sparks(*check_arg())
    #log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    #app.run()
