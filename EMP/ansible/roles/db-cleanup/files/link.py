import argparse
from cached_property import cached_property
from pymongo import MongoClient, errors
from datetime import datetime, timedelta


MONGODB = {
    'URL': 'mongo.phim.isyntax.net:27017/',
    'DB_NAME': 'somedb',
    'USER_NAME': 'idmportal',
    'PASSWORD': 'idmportalst3nt0r',
    'PROTOCOL': 'mongodb'
}


def check_arg(args=None):
    parser = argparse.ArgumentParser(
        description='Script to clean different db data')
    parser.add_argument(
        '-d', '--days', help='Number of days old data', required=True, type=int)
    results = parser.parse_args(args)
    return {'days': results.days}


class AUsefulThing(object):

    def __init__(self, Stratergy):
        self.stratergy_obj = Stratergy

    def do_clean(self):
        self.stratergy_obj.clean()


class StrategyCleaner(object):
    _db = None
    _uri = None

# class PostGresCleanup( StrategyCleaner ):
    #   _db = []
    #   _uri =[]
#     def theAPIMethod( self ):
#         pass # an implementation


class MongoCleanUp(StrategyCleaner):

    _db = MONGODB['DB_NAME']

    def __init__(self, **kwargs):
        self.args = kwargs
        db_params = dict(uri=MONGODB['URL'], username=MONGODB['USER_NAME'],
                         password=MONGODB['PASSWORD'], protocol=MONGODB['PROTOCOL'],
                         auth_source=MONGODB['DB_NAME'])
        self.dns = self.get_mongo_dns(**db_params)

    @cached_property
    def mc(self):
        return MongoClient(self.dns)

    @cached_property
    def db(self):
        return self.mc[self._db]

    def get_mongo_dns(self, uri, username, password, protocol, auth_source):
        return "{protocol}://{username}:{password}@{uri}?authSource={auth_source}".format(
            protocol=protocol,
            username=username,
            password=password,
            uri=uri,
            auth_source=auth_source
        )

    def clean(self):
        dt_dif = datetime.today() - timedelta(days=self.args.get('days', 60))
        evt_collection = self.db['events']
        print datetime.today()
        print evt_collection.remove({'timestamp': {'$lte': dt_dif}}).count()
        print '------'
    
    def __del__(self):
        del(self.mc)


if __name__ == '__main__':
    args = check_arg()
    cleaner_objs = [MongoCleanUp]
    for cleaner_obj in cleaner_objs:
        executor = AUsefulThing(cleaner_obj(**args), )
        executor.do_clean()
