---

# set /etc/sysconfig/crond file behavior
# it forces cron to log when it executes and disables cron mail messages
- name: setup crond file
  copy: src=crond dest=/etc/sysconfig/crond

- name: disable ctrl-alt-delete
  copy: src=control-alt-delete.override dest=/etc/init/control-alt-delete.override

- include: users.yml

- name: delete old yum transaction files
  file: path={{item}} state=absent
  with_fileglob:
    - /var/lib/yum/transaction-*

- name: sync local yum database/cache
# yum module doesn't have clean option
  command: /usr/bin/yum clean all
  changed_when: False

- name: install libselinux-python for ansible lineinfile
  yum: name=libselinux-python state=latest

- name: install python-passlib for ansible htpasswd
  yum: name=python-passlib state=latest

- name: ensure unneeded services are disabled
  service: name={{item}} state=stopped enabled=no
  with_items:
    - iptables
    - ip6tables

- name: ensure iscsi-initiator-utils is not present
  yum: name=iscsi-initiator-utils state=absent

- name: install base packages
  yum: name={{item}} state=latest
  with_items:
    - openssh-server
    - openssh-clients
    - openssl-devel
    - make
    - wget
    - vixie-cron
    - sudo
    - postfix
    - svn
    - syslog-ng
    - libffi-devel

- name: install base tools
  yum: name={{item}} state=latest
  with_items:
    - tcpdump
    - telnet
    - traceroute
    - tree
    - lsof
    - autossh
    - screen
    - rsync
    - patch
    - tmux

- include: time_settings.yml

- name: ensure hostname is set
  hostname: name={{fqdn}}
  notify:
    - reboot server
    - wait for server to shut down
    - wait for server to come back up

- include: host_customizations.yml

- include: setuppip.yml

- name: ensure python useful libraries are installed
  pip: name={{item}} state=latest
  with_items:
    - cached-property
    - cryptography

- name: ensure python plugin  pycontrol is installed
  command: pip install --upgrade --no-deps --ignore-installed --force-reinstall pycontrol

- name: ensure python plugin packages are installed
  pip: name={{item}} state=latest
  with_items:
    - pexpect
    - pywbem
    - pywinrm
    - zerto

- name: ensure svn is installed
  yum: name=CollabNetSubversion-client state=latest

- name: ensure the latest kernel is installed
  yum: name=kernel state=latest

- name: check if kernel was updated to reboot
  shell: '[ $(/bin/rpm -q kernel --qf "%{INSTALLTIME}\n"|tail -1) -gt $(/bin/sed -n "/^btime /s///p" /proc/stat) ]'
  register: kupdate_result
  changed_when: "kupdate_result.rc == 0"
  ignore_errors: yes
  notify:
    - reboot server
    - wait for server to shut down
    - wait for server to come back up
    - update facts

- meta: flush_handlers
