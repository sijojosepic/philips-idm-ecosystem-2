import logging
import time
from datetime import date, datetime

from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT


def get_logger():
    log = logging.getLogger('phim.billingapp')
    log.setLevel(logging.INFO)
    logging.Formatter.converter = time.gmtime
    formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
    handler_console = logging.StreamHandler()
    handler_console.setFormatter(formatter)
    log.addHandler(handler_console)
    return log


def custom_serializer(obj):
    """JSON serializer for objects not serializable by default json code
        datetimel
    """
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))