from pymongo import MongoClient
from pymongo.errors import PyMongoError
from cryptography.fernet import Fernet

from app_config import FERNET_SK
from utils import get_logger


class FernetCrypto(object):
    def __init__(self):
        self._cryptor = None

    @property
    def cryptor(self):
        if not self._cryptor:
            self._cryptor = Fernet(bytes(FERNET_SK))
        return self._cryptor

    def decrypt(self, value):
        return self.cryptor.decrypt(value)


def _mongo_auth_url(uri, username, password, protocol, auth_source):
    cryptor = FernetCrypto()
    return "{protocol}://{username}:{password}@{uri}?authSource={auth_source}".format(
        protocol=protocol,
        username=cryptor.decrypt(username),
        password=cryptor.decrypt(password),
        uri=uri,
        auth_source=auth_source
    )


def mongo_connection(uri, username, password, protocol, auth_source, database):
    try:
        con_uri = _mongo_auth_url(uri, username, password, protocol, auth_source)
        return MongoClient(con_uri, connect=False)[database]
    except PyMongoError as e:
        log = get_logger()
        log.error("{0} - PyMongo Unexpected Error!\n".format(e))
        raise e
