import unittest
from mock import MagicMock, patch


class Utils(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_time = MagicMock(name='time')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_pymongo = MagicMock(name='pymongo')
        modules = {
            'logging': self.mock_logging,
            'time': self.mock_time,
            'datetime': self.mock_datetime,
            'pymongo': self.mock_pymongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import utils
        self.module = utils

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_custom_serializer(self):
        obj_mock = MagicMock(name='mock_obj')
        obj_mock.isoformat.return_value = 'isoformat'
        isinstance_mock = MagicMock(name='isinstance')
        isinstance_mock.return_value = True
        self.module.isinstance = isinstance_mock
        val = self.module.custom_serializer(obj_mock)
        self.assertEqual(val, 'isoformat')
        self.assertEqual(isinstance_mock.call_count, 1)
        self.assertEqual(obj_mock.isoformat.call_count, 1)

    def test_custom_serializer_exception(self):
        instance_mock = MagicMock(name='instance')
        instance_mock.return_value = False
        self.module.isinstance = instance_mock
        self.assertRaises(
            TypeError, self.module.custom_serializer, 'arg1')
        self.assertEqual(instance_mock.call_count, 1)
