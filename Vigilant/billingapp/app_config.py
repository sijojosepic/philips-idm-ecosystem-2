CELERY_PATH = '/usr/lib/celery'
CELERY_CONFIGURATION_OBJECT = 'celeryconfig'
PATH_TASK_MAP = {
    '/billingapp/upload': 'phim_backoffice.routers.isite_billing',
}

# MONGODB / Collections
MONGODB = {
    'URL': 'mongo.phim.isyntax.net:27017/',
    'RETRIES': 2,
    'RETRY_DELAY': 10,
    'DB_NAME': 'somedb',
    'USER_NAME': 'gAAAAABbHjSX0WI7pFNty46xnNrFPzqWY0ybrbI5K7332_JEdSh6B1RS8O17_I8G0uzNmB-yo8N5hxYC_sZLUsLUWYga1X0SNA==',
    'PASSWORD': 'gAAAAABbHjSXNR8n2z5Q6ks8lx1fHDHc50Z0n-AgTOg2VUJr0omyNqCi0ON4phYwiYt_wlG28prTn6Oa7TBSSDAMEs6-ro6wnuyjHVsNErKec-3z_peCFC4=',
    'PROTOCOL': 'mongodb'
}

COLLECTIONS = {
    'BILLING': 'billing'
}

FERNET_SK = '-BEhr4qfb-GMRMZU1YK-Q6ifPIrQkKstA5cHbxHaF9M='
