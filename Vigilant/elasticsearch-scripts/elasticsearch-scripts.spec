# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phielasticsearch %{_prefix}/lib/philips/elasticsearch/

# Basic Information
Name:      elasticsearch-scripts
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   elasticsearch-scripts

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  python-argparse

Provides:  elasticsearch-scripts

%description
elasticsearch-scripts, includes time based alias setup script.


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{phielasticsearch}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/elasticsearch_alias.py %{buildroot}%{phielasticsearch}


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%attr(0755,root,root) %{phielasticsearch}/elasticsearch_alias.py


%changelog
* Wed Feb 04 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Require argparse

* Sat Dec 07 2013 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
