import logging
import json
import re
import requests
from lxml.etree import tostring
from lxml.builder import E
from tbconfig import SITE_INFO_MODULE_TYPES


spit_bucket = logging.getLogger(__name__)


class ConfiggenPCM(object):
    def __init__(self, data, **kwargs):
        self.data = data
        #Set encrypted as True
        self.encrypt_keys = ['DomainAdminPassword']

    def generate(self):
        deployment_id = self.data.pop('deployment_id')
        return { "siteinfo/pcm_manifest_{deployment_id}.json".format(deployment_id=deployment_id) : json.dumps(self.data, sort_keys=True, indent=4),
                 "siteinfo/SiteInfo_{deployment_id}.xml".format(deployment_id=deployment_id): self.data['xml']}

