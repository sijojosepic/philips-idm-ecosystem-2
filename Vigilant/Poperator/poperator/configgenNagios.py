import os
import logging
from pkg_resources import resource_filename
from nagios_configuration import NagiosConfigurationRuleSet, NagiosServices
from prometheus_configuration import PrometheusConfig


spit_bucket = logging.getLogger(__name__)


class ConfiggenNagios(object):
    def __init__(self, data, **kwargs):
        self.data = data
        self.hosts_dir = 'hosts'
        self.configuration_rules = resource_filename(__name__, 'nagios_configuration_rules.yml')
        self.rule_set = NagiosConfigurationRuleSet()
        self.rule_set.load(self.configuration_rules)
        self.credentials = kwargs.get('credentials') if kwargs.get('credentials') else {}
        self.service = NagiosServices(self.data)

    def get_filename(self, hostname):
        return os.path.join(self.hosts_dir, hostname + '.cfg')

    def generate(self):
        configs, service_configs, prom_hosts = {}, {}, []
        for host in self.data:
            if host.get('prometheusAgents'):
                prom_hosts.append(host)
            else:
                try:
                    host.update({'credentials': self.credentials.get(host['hostname'])})
                    host_config = self.rule_set.eval(host)
                    if host_config:
                        configs[self.get_filename(host['hostname'])] = str(host_config)
                    if host.get('applications'):
                        service_config = self.generate_service(host, service_configs)
                        if service_config:
                            service_configs.update(service_config) 
                except Exception:
                    spit_bucket.exception('Discovery : - Error while processing host %s', host)
        configs.update(service_configs)
        if prom_hosts:
            configs.update({'prometheus/prometheus.yml' : 
                self.generate_prometheus_yml(prom_hosts)})
        return configs

    def generate_service(self, host, service_config):
        ''' generate service configuration for all the hosts and their applications and 
        return it for svn write'''
        return self.service.evaluate(host, service_config)

    def generate_prometheus_yml(self,hosts):
        return PrometheusConfig(hosts).evaluate()

# var1 = [
#     {
#         'hostname': 'host1',
#         'address': '10.0.0.1',
#         'manufacturer': 'VMware, Inc.',
#         'ISP': {
#             'module_type': '2',
#             'anywhere_version': '1.2',
#             'version': '4,4,3,1',
#             'identifier': '4x',
#             'flags': ['domain_needed', 'anywhere']
#         }
#     },
#     {
#         'hostname': 'host02',
#         'address': '10.0.0.254',
#         'LN': {
#             'alias': 'HOST02',
#             'use': 'esx_server'
#         }
#     }
# ]
# hh = ConfiggenNagios(var1)
#
# config = hh.generate()