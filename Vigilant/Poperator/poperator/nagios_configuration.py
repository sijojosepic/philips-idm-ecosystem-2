import re
import yaml
import json
from pynag import Model
from jinja2 import Template, Environment
from cached_property import cached_property
from itertools import chain
from copy import deepcopy
import os
import logging
from pkg_resources import resource_filename
from tbconfig import GENERIC_DISCOVERY

logger = logging.getLogger(__name__)


class NagiosConfiguration(object):
    def __init__(self):
        self.attributes = {}
        self.hostgroups = HostgroupsAttribute()

    def add_attribute(self, attribute, value):
        if not value:
            return
        if attribute == 'hostgroups':
            self.hostgroups.add(value)
        else:
            self.attributes[attribute] = value

    def get_attribute(self, attribute):
        if attribute == 'hostgroups':
            return self.hostgroups
        return self.attributes[attribute]

    def __nonzero__(self):
        return 'use' in self.attributes

    def get_model(self):
        model_attributes = self.attributes.copy()
        model_attributes['hostgroups'] = str(self.hostgroups)
        model = Model.Host()
        for attribute, value in model_attributes.iteritems():
            if value:
                model.set_attribute(attribute, value)
        return model

    def __str__(self):
        return str(self.get_model())


class HostgroupsAttribute(object):
    def __init__(self):
        self.base = ''
        self.groups = []

    def __nonzero__(self):
        return bool(self.base or self.groups)

    def set_base(self, base):
        self.base = base
        self.groups = []

    def add(self, groups):
        if groups.startswith('+'):
            self.groups.append(groups[1:])
        else:
            self.set_base(groups)

    def __str__(self):
        glue = ','
        combined_groups = filter(None, [self.base] + self.groups)
        joined_groups = glue.join(combined_groups)
        if not self.base and joined_groups:
           return '+{0}'.format(joined_groups)
        return joined_groups


class NagiosConfigurationRule(object):
    def __init__(self, rule):
        self.attribute = rule['attribute']
        self.set_condition(rule.get('when'))

    @cached_property
    def environment(self):
        return self.get_environment()

    def get_environment(self):
        environment = Environment()
        environment.tests['re_search'] = lambda value, pattern: bool(re.search(pattern, value))
        environment.filters['json'] = json.dumps
        return environment

    def set_condition(self, raw_condition):
        if not raw_condition:
            self.condition = None
            return
        # a Jinja2 evaluation that results in something Python can eval! #Ripped from Ansible
        presented = "{%% if %s %%} True {%% else %%} False {%% endif %%}" % raw_condition
        template = self.environment.from_string(presented)
        self.condition = lambda x: template.render(x).strip() == 'True'

    def eval(self, data):
        if (not self.condition) or self.condition(data):
            value = self.get_value(data)
            if value:
                return self.attribute, value

    def get_value(self, data):
        raise NotImplementedError

    def render_bare(self, source, data):
        return self.environment.from_string('{{%s}}' % source).render(data)


class NagiosMapConfigurationRule(NagiosConfigurationRule):
    def __init__(self, rule):
        super(NagiosMapConfigurationRule, self).__init__(rule)
        self.value_map = rule['map']
        self.source = rule['source']
        self.default = rule.get('default')

    def get_value(self, data):
        source_key = self.render_bare(self.source, data)
        return self.value_map.get(source_key, self.default)


class NagiosValueConfigurationRule(NagiosConfigurationRule):
    def __init__(self, rule):
        super(NagiosValueConfigurationRule, self).__init__(rule)
        self.value = rule['value']

    def get_value(self, data):
        return self.render_bare(self.value, data)


class NagiosConfigurationRuleException(Exception):
    pass


class NagiosConfigurationRuleFactory(object):
    rule_map = {
        'map': NagiosMapConfigurationRule,
        'value': NagiosValueConfigurationRule
    }

    def get_nagios_rule(self, data):
        for key, rule_class in self.rule_map.iteritems():
            if key in data:
                return rule_class(data)
        raise NagiosConfigurationRuleException('Rule missing valid type.')


class NagiosConfigurationRuleSet(object):
    def __init__(self):
        self.raw_data = None
        self.rule_set = []

    def load_dict(self, data):
        self.raw_data = deepcopy(data)
        rule_factory = NagiosConfigurationRuleFactory()
        for sub_rule_set in data:
            required_key, raw_rules = sub_rule_set.popitem()
            rules = [rule_factory.get_nagios_rule(raw_rule) for raw_rule in raw_rules]
            self.rule_set.append(dict(required_key=required_key, rules=rules))

    def load(self, filename):
        with open(filename) as f:
            data = yaml.load(f)
        self.load_dict(data)

    def get_results(self, data):
        applicable_subsets = (subset['rules'] for subset in self.rule_set if subset['required_key'] in data)
        return chain.from_iterable(self.get_outcomes(data, rules) for rules in applicable_subsets)

    def get_outcomes(self, data, rules):
        for rule in rules:
            rule_result = rule.eval(data)
            if rule_result:
                yield rule_result

    def eval(self, data):
        if not self.rule_set:
            raise NagiosConfigurationRuleException('Rule set empty')
        result = NagiosConfiguration()
        for attribute, value in self.get_results(data):
            result.add_attribute(attribute, value)
        if data.get('service_excludes'):
            result.add_attribute('service_excludes', ','.join(data.get('service_excludes')))
        return result


class NagiosServices(object):
    def __init__(self, data):
        self.data = data
        self.service_dir = 'services'
        self.yml_data = self.load(resource_filename(__name__, 'services.yml'))

    def get_model(self, config):
        ''' constructing shinken service model here'''
        model_config = ''
        if not config:
            return model_config
        model = Model.Service()
        for conf in config:
            for attribute, value in conf.items():
                model[attribute] = value
            model_config = model_config + str(model)
        return model_config

    def get_filename(self, host):
        product_id = host.get('product_id')
        role = self.get_role(host)
        if not product_id or product_id == 'NA' or not role:
            return None
        fname = product_id + '_' + role
        return os.path.join(self.service_dir, fname + '.cfg').lower()

    def get_role(self, host):
        ''' host['role'] =['role1'] '''
        return None if host['role'] == 'NA' else '_'.join(host['role'])
    
    def get_hostgroup(self ,host):
        hostgroup = host['product_id']+ '_' + self.get_role(host)
        return hostgroup.lower()

    @classmethod
    def load(cls,filename):
        with open(filename) as f:
            data = yaml.load(f)
        return data

    def get_service_dsc(self, application):
        return application.get('serviceDescription')

    def get_url_fields(self, urls_dict):
        path = '' if urls_dict.get('resource') == '/' else urls_dict.get('resource')
        if not path:
            path = "''"
        protocol = urls_dict.get('protocol')
        port = urls_dict.get('port')
        if not port:
            port = '443' if protocol == 'https' else '80'
        return {'port': str(port), 'path': path, 'protocol': protocol}

    def get_monitoring_url(self, application):
        ''' Getting port, path and protocol as part of this function. '''
        cmd_args = {}
        try:
            applicationMonitoring = application.get('applicationMonitoring')
            urls_dict = applicationMonitoring.get('monitoringDetails')
            if not urls_dict:
                return cmd_args
            cmd_args = self.get_url_fields(urls_dict)
        except Exception as e:
            logger.exception('There might be error with the fields of monitoring URL %s', str(application))
            logger.exception(e)
        return cmd_args

    def get_service_field(self, app_type, host, application):
        service_fields = {}
        try:
            fields = [field for field in self.yml_data for field1 in field if app_type in field1][0][app_type]
            attributes = []
            values = []
            for attr in fields:
                attributes.append(attr.get('attribute'))
                if attr.get('value'):
                    values.append(attr.get('value'))
                if attr.get('map'):
                    values.append(attr.get('map'))
            service_fields = dict(zip(attributes, values))
            service_fields['hostgroup_name'] = service_fields['hostgroup_name'][self.get_hostgroup(host)]
            service_fields['service_description'] = self.get_service_dsc(application)
        except Exception as e:
            service_fields = {}
            logger.exception('There might be error with the fields of services yml %s', str(e))
        return service_fields

    def get_windows_outcomes(self, application_type, host, application):
        ''' Generate shinken service for windows process, windows service, passive check and windows firewall.'''
        if host.get('win_service_flag') and application_type == 'windows-service':
            application_type = 'ISEE-windows-service'
        service_outcomes = self.get_service_field(application_type, host, application)
        applicationMonitoring = application.get('applicationMonitoring')
        win_arg = applicationMonitoring.get('monitoringDetails').get('resource')
        service_outcomes['check_command'] = service_outcomes['check_command'].format(
                    arg=win_arg)
        return service_outcomes

    def get_checkType(self, appMonitoring):
        checkType = appMonitoring.get('monitoringDetails').get('checkType', 'Ping').lower()
        return checkType

    def get_authType(self, appMonitoring):
        authType = appMonitoring.get('monitoringDetails').get("authType")
        if authType:
            return True if authType['auth'].lower() == 'hmac' else False
        return False

    def get_webapi_outcomes(self, host, application, monitoring_URL):
        ''' Generate shinken service for webapi for HTTP Ping and Health'''
        applicationMonitoring = application.get('applicationMonitoring')
        service_outcomes = {}
        authType = checkType = None
        if applicationMonitoring:
            checkType = self.get_checkType(applicationMonitoring)
            monitoring_URL['auth'] = self.get_authType(applicationMonitoring)
        if checkType == 'ping':
            service_outcomes = self.get_service_field('rest_api', host, application)
            service_outcomes['check_command'] = service_outcomes['check_command'].format(**monitoring_URL)
        elif checkType == 'health':
            if monitoring_URL['path']:
                service_outcomes = self.get_service_field('health_path', host, application)
                service_outcomes['check_command'] = service_outcomes['check_command'].format(**monitoring_URL)
        return service_outcomes

    def get_service_outcomes(self, host, application, application_type):
        ''' Assigning values to the shinken service fields '''
        service_outcomes = {}
        try:
            if application_type == 'webapi':
                monitoring_URL = self.get_monitoring_url(application)
                if not monitoring_URL:
                    return service_outcomes
                service_outcomes = self.get_webapi_outcomes(host, application, monitoring_URL)
            elif application_type in GENERIC_DISCOVERY['GD_SERVICE_TYPE']:
                service_outcomes = self.get_windows_outcomes(application_type, host, application)
            else:
                logger.info('%s application type is not supported by IDM currently')
        except Exception as e:
            logger.exception(e)
            service_outcomes = {}
        return service_outcomes

    def get_results(self, host):
        ''' Get shinken service configuration for all the application in host '''
        service_config = []
        applications = host.get('applications')
        if not applications or applications == 'None':
            return service_config
        for application in applications:
            application_type = str(application.get('applicationType')).lower()
            #enable_monitoring = application.get('enableMonitoring','true').title()
            if application_type not in GENERIC_DISCOVERY['GD_APP_SERVICE_TYPE']:
                continue
            service_fields = self.get_service_outcomes(host, application, application_type)
            if service_fields:
                service_config.append(service_fields)
        return service_config

    def evaluate(self, host, service_configs):
        ''' Shinken service configuration for all hosts and their applications '''
        configs = {}
        key_config = self.get_filename(host)
        if not key_config:
            return configs
        if not service_configs.get(key_config):
            service_config = self.get_model(self.get_results(host))
            configs[key_config] = service_config
        return configs
