from pkg_resources import resource_filename
import yaml
class Yml_File:
    def __init__(self, filename):
        self.config_file = resource_filename(__name__, filename)

    def config(self):
        with open(self.config_file) as f:
            return yaml.load(f)
