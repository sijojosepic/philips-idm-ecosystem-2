import yaml
from ymlfile import Yml_File

config = Yml_File('prometheus.yml').config()

class PrometheusJob(object):
    def __init__(self):
        self.agents = set()

    def modify_jobs(self, hostname, job):
        job.get('static_configs')[0]['targets'].append(hostname)
        config.get('scrape_configs').append(job)
        
    def search_agent(self, agent_name):
        jobs = config.get('scrape_configs')
        for job in jobs:
            if job.get('job_name') == agent_name:
                return jobs.pop(jobs.index(job))
        return {}

    def create_jobs(self, hostname, prometheus_agents):
        jobs = config.get('scrape_configs')
        for agent in prometheus_agents:
            agent_name = agent.get('name').lower()
            if agent_name!= 'node_exporter':
                self.agents.add(agent_name)
            job = self.search_agent(agent_name)
            if job:
                self.modify_jobs(hostname, job)
            else:
                jobs.append(self.construct_job(agent,hostname))

    def construct_job(self, agent, hostname):
        job = {}
        job['job_name'] = str(agent.get('name'))
        job['metrics_path'] = str(agent.get('uri'))
        job['scheme'] = 'https'
        job['tls_config'] = {'insecure_skip_verify': True}
        job['static_configs'] = [{'targets': [hostname]}]
        return job

class PrometheusRules(object):
    def __init__(self, jobs):
        self.jobs = jobs

    def add_rules(self, productid):
        rule_files = []
        for agent in self.jobs.agents:
            rule_file = config['rule_files'][0].strip()
            rule_file_name = '{0}_{1}.yml'.format(productid,agent)
            rule_file = rule_file.replace('node_exporter.yml', rule_file_name)
            rule_files.append(rule_file)
        config['rule_files'].extend(rule_files)

class PrometheusConfig(object):
    def __init__(self, hosts):
        self.hosts = hosts
        self.productid = ''
        self.jobs = PrometheusJob()
        self.rules = PrometheusRules(self.jobs)

    def evaluate(self):
        for host in self.hosts:
            self.jobs.create_jobs(str(host.get('hostname')),host.get('prometheusAgents'))
            if not self.productid:
                self.productid = host.get('product_id')
        if self.jobs.agents:
            self.rules.add_rules(self.productid)
        return yaml.dump(config)
