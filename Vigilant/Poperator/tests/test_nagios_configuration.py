import unittest
import mock
from mock import MagicMock, patch, call, PropertyMock, Mock


class NagiosConfigurationTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
            self.mock_jinja2 = MagicMock(name='jinja2')
            self.mock_json = MagicMock(name='json')
            self.mock_pynag = MagicMock(name='pynag')
            self.mock_pkg_resources = MagicMock(name='pkg_resources')
            self.mock_yml = MagicMock(name='yaml')
            self.mock_logging = MagicMock(name='logging')
            self.mock_tbconfig = MagicMock(name='tbconfig')
            modules = {
                'cached_property': self.mock_cached_property,
                'pynag': self.mock_pynag,
                'jinja2': self.mock_jinja2,
                'json': self.mock_json,
                'pkg_resources': self.mock_pkg_resources,
                'yaml': self.mock_yml,
                'logging': self.mock_logging,
                'tbconfig': self.mock_tbconfig
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import poperator.nagios_configuration
            self.module = poperator.nagios_configuration

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class NagiosConfigurationTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_configuration = self.module.NagiosConfiguration()
        self.nagios_configuration.hostgroups = MagicMock(name='hostgroups')

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_false_on_missing_use_attribute(self):
        self.assertFalse(self.nagios_configuration)

    def test_true_on_having_use_attribute(self):
        self.nagios_configuration.add_attribute('use', 'some-server')
        self.assertTrue(self.nagios_configuration)

    def test_add_use_attribute_none(self):
        self.nagios_configuration.add_attribute('use', None)
        self.assertFalse(self.nagios_configuration)

    def test_add_and_get_attribute(self):
        self.nagios_configuration.add_attribute('hostname', 'host1')
        self.assertEqual(self.nagios_configuration.get_attribute('hostname'), 'host1')

    def test_add_to_hostgroups_attribute(self):
        self.nagios_configuration.add_attribute('hostgroups', '+some-servers')
        self.nagios_configuration.hostgroups.add.assert_called_once_with('+some-servers')

    def test_get_model(self):
        self.nagios_configuration.add_attribute('hostname', 'host1')
        self.nagios_configuration.add_attribute('hostgroups', '+some-servers')
        result = self.nagios_configuration.get_model()
        self.assertEqual(
            result.mock_calls,
            [
                call.set_attribute('hostname', 'host1'),
                call.set_attribute('hostgroups', self.nagios_configuration.hostgroups.__str__.return_value)
            ]
        )


class HostgroupsAttributeTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.hostgroups_attribute = self.module.HostgroupsAttribute()

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_false_on_empty(self):
        self.assertFalse(self.hostgroups_attribute)

    def test_true_on_not_empty(self):
        self.hostgroups_attribute.add('group1')
        self.assertTrue(self.hostgroups_attribute)

    def test_str_empty(self):
        self.assertEqual(str(self.hostgroups_attribute), '')

    def test_add_one_noplus_and_add_one_plus(self):
        self.hostgroups_attribute.add('hostgroup1')
        self.hostgroups_attribute.add('+hostgroup2')
        self.assertEqual(str(self.hostgroups_attribute), 'hostgroup1,hostgroup2')

    def test_add_one_plus_and_add_one_noplus(self):
        self.hostgroups_attribute.add('+hostgroup1')
        self.hostgroups_attribute.add('hostgroup2')
        self.assertEqual(str(self.hostgroups_attribute), 'hostgroup2')

    def test_add_two_plus(self):
        self.hostgroups_attribute.add('+hostgroup1')
        self.hostgroups_attribute.add('+hostgroup2')
        self.assertEqual(str(self.hostgroups_attribute), '+hostgroup1,hostgroup2')


class NagiosConfigurationRuleTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        raw_rule = {
            'attribute': 'hostname',
        }
        self.nagios_configuration_rule = self.module.NagiosConfigurationRule(raw_rule)

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_no_attribute(self):
        self.assertRaises(KeyError, self.module.NagiosMapConfigurationRule, {})

    def test_set_condition_none(self):
        self.assertTrue(self.nagios_configuration_rule.condition is None)

    @patch('poperator.nagios_configuration.NagiosConfigurationRule.environment', new_callable=PropertyMock)
    def test_set_condition(self, mock_environment):
        raw_rule = {
            'attribute': 'hostname',
            'when': 'somevar == "val1"'
        }
        mock_environment.return_value.from_string.return_value.render.return_value = ' True '
        self.nagios_configuration_rule = self.module.NagiosConfigurationRule(raw_rule)
        mock_environment.return_value.from_string.assert_called_once_with(
            '{% if somevar == "val1" %} True {% else %} False {% endif %}'
        )
        self.assertTrue(self.nagios_configuration_rule.condition({'var': 'val'}))

    def test_eval(self):
        self.nagios_configuration_rule.get_value = MagicMock(name='get_value')
        self.assertEqual(
            self.nagios_configuration_rule.eval({}),
            ('hostname', self.nagios_configuration_rule.get_value.return_value)
        )

    def test_eval_none_value(self):
        self.nagios_configuration_rule.get_value = MagicMock(name='get_value', return_value=None)
        self.assertEqual(self.nagios_configuration_rule.eval({}), None)

    def test_get_value_unimplemented(self):
        self.assertRaises(NotImplementedError, self.nagios_configuration_rule.get_value, {})

    @patch('poperator.nagios_configuration.NagiosConfigurationRule.environment', new_callable=PropertyMock)
    def test_render_bare(self, mock_environment):
        template = mock_environment.return_value.from_string
        self.assertEqual(
            self.nagios_configuration_rule.render_bare('hostname', {'hostname': 'host1'}),
            template.return_value.render.return_value
        )
        template.assert_called_once_with('{{hostname}}')
        template.return_value.render.assert_called_once_with({'hostname': 'host1'})


class NagiosMapConfigurationRuleTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        raw_rule = {
            'attribute': 'hostname',
            'map': {
                'one': 'val1',
                'two': 'val2',
            },
            'source': 'node1.var1',
            'default': 'defval'
        }
        self.nagios_configuration_rule = self.module.NagiosMapConfigurationRule(raw_rule)

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_no_source(self):
        raw_rule = {
            'attribute': 'hostname',
            'map': {},
        }
        self.assertRaises(KeyError, self.module.NagiosMapConfigurationRule, raw_rule)

    def test_get_value_in_map(self):
        self.nagios_configuration_rule.render_bare = MagicMock(name='render_bare', return_value='one')
        self.assertEqual(self.nagios_configuration_rule.get_value({'data1': 'kdata1'}), 'val1')

    def test_get_value_not_in_map_default_set(self):
        self.nagios_configuration_rule.render_bare = MagicMock(name='render_bare', return_value='not_there')
        self.assertEqual(self.nagios_configuration_rule.get_value({'data1': 'kdata1'}), 'defval')
        self.nagios_configuration_rule.render_bare.assert_called_once_with('node1.var1', {'data1': 'kdata1'})


class NagiosValueConfigurationRuleTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        raw_rule = {
            'attribute': 'hostname',
            'value': 'name_of_host',
        }
        self.nagios_configuration_rule = self.module.NagiosValueConfigurationRule(raw_rule)

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_no_value(self):
        raw_rule = {
            'attribute': 'hostname',
        }
        self.assertRaises(KeyError, self.module.NagiosValueConfigurationRule, raw_rule)

    def test_get_value(self):
        self.nagios_configuration_rule.render_bare = MagicMock(name='render_bare')
        self.assertEqual(self.nagios_configuration_rule.get_value({'data1': 'kdata1'}),
                         self.nagios_configuration_rule.render_bare.return_value)
        self.nagios_configuration_rule.render_bare.assert_called_once_with('name_of_host', {'data1': 'kdata1'})


class NagiosConfigurationRuleFactoryTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_configuration_rule_factory = self.module.NagiosConfigurationRuleFactory()

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_map_rule(self):
        raw_rule = {
            'attribute': 'hostname',
            'map': {},
            'source': 'key2'
        }
        result = self.nagios_configuration_rule_factory.get_nagios_rule(raw_rule)
        self.assertEqual(type(result), self.module.NagiosMapConfigurationRule)

    def test_value_rule(self):
        raw_rule = {
            'attribute': 'hostname',
            'value': 'val2'
        }
        result = self.nagios_configuration_rule_factory.get_nagios_rule(raw_rule)
        self.assertEqual(type(result), self.module.NagiosValueConfigurationRule)


class NagiosConfigurationRuleSetTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_configuration_rule_set = self.module.NagiosConfigurationRuleSet()

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_load_dict(self):
        self.module.NagiosConfigurationRuleFactory = MagicMock(name='NagiosConfigurationRuleFactory')
        raw_ruleset = [
            {'hostname': [ 'SENTINEL1', 'SENTINEL2' ]},
            {'LN': ['ASENTINEL']}
        ]
        mock_get_nagios_rule = self.module.NagiosConfigurationRuleFactory.return_value.get_nagios_rule
        mock_get_nagios_rule.side_effect = lambda x: 'rule_{0}'.format(x)
        self.nagios_configuration_rule_set.load_dict(raw_ruleset)
        self.assertEqual(
            self.nagios_configuration_rule_set.rule_set,
            [
                {
                    'rules': ['rule_SENTINEL1', 'rule_SENTINEL2'],
                    'required_key': 'hostname'
                },
                {
                    'rules': ['rule_ASENTINEL'],
                    'required_key': 'LN'
                }
            ]
        )

    def test_get_results(self):
        data = {'hostname': 'host1', 'mod1': {'key1': 'val1'}}
        self.nagios_configuration_rule_set.rule_set = [
            {'rules': ['rule_1_1', 'rule_1_2'], 'required_key': 'hostname'},
            {'rules': ['rule_2_1', 'rule_2_2'], 'required_key': 'LN'},
            {'rules': ['rule_3_1'], 'required_key': 'hostname'}
        ]
        def outcomer(x, rules):
            for rule in rules:
                yield 'outcome form rule {rule}'.format(rule=rule)

        self.nagios_configuration_rule_set.get_outcomes = MagicMock(name='get_outcomes', side_effect=outcomer)
        self.assertEqual(
            list(self.nagios_configuration_rule_set.get_results(data)),
            ['outcome form rule rule_1_1', 'outcome form rule rule_1_2', 'outcome form rule rule_3_1']
        )
        self.assertEqual(
            self.nagios_configuration_rule_set.get_outcomes.call_args_list,
            [
                call({'mod1': {'key1': 'val1'}, 'hostname': 'host1'}, ['rule_1_1', 'rule_1_2']),
                call({'mod1': {'key1': 'val1'}, 'hostname': 'host1'}, ['rule_3_1'])
            ]
        )

    def test_get_outcomes(self):
        data = {'hostname': 'host1', 'mod1': {'key1': 'val1'}}
        mocked_rules = [MagicMock(name='rule1'), MagicMock(name='rule2')]
        result = self.nagios_configuration_rule_set.get_outcomes(data, mocked_rules)
        self.assertEqual(list(result), [mocked_rules[0].eval.return_value, mocked_rules[1].eval.return_value])

    def test_eval_no_rule_set(self):
        self.assertRaises(self.module.NagiosConfigurationRuleException, self.nagios_configuration_rule_set.eval, {})

    def test_eval(self):
        self.module.NagiosConfiguration = MagicMock(name='NagiosConfiguration')
        self.nagios_configuration_rule_set.rule_set = True
        self.nagios_configuration_rule_set.get_results = MagicMock(
            name='get_results',
            return_value=iter([('atr1', 'val1'), ('atr2', 'val2')])
        )
        result = self.nagios_configuration_rule_set.eval({'hostname': 'host1'})
        self.nagios_configuration_rule_set.get_results.assert_called_once_with({'hostname': 'host1'})
        self.assertEqual(result.mock_calls, [call.add_attribute('atr1', 'val1'), call.add_attribute('atr2', 'val2')])

class NagiosServicesTestCase(NagiosConfigurationTest.TestCase):
    def setUp(self):
        NagiosConfigurationTest.TestCase.setUp(self)
        self.nagios_services = self.module.NagiosServices
        yml_data = yml_data = [{'windows-service': [{'attribute': 'check_command', 'value': 'check_win_service!^"{arg}"$!_NumGood=1:'}, {'attribute': 'use', 'value': 'wmi-dependant-service'}, {'attribute': 'hostgroup_name',
         'map': {'hsop_fluentd': 'hsop-fluentd', 'hsop_es': 'hsop-es', 'hsop_kibana': 'hsop-kibana'}}]},
          {'ISEE-windows-service': [{'attribute': 'check_command', 'value': 'check_win_service_without_argument!{arg}'},
           {'attribute': 'use', 'value': 'standard-service'}, {'attribute': 'hostgroup_name', 'map': {'hsop_fluentd': 'hsop-fluentd',
            'hsop_es': 'hsop-es', 'hsop_kibana': 'hsop-kibana'}}]}, {'windows-process': [{'attribute': 'check_command',
             'value': 'check_win_process!{arg}'}, {'attribute': 'use', 'value': 'wmi-dependant-service'},
              {'attribute': 'hostgroup_name', 'map': {'hsop_fluentd': 'hsop-fluentd', 'hsop_es': 'hsop-es',
               'hsop_kibana': 'hsop-kibana'}}]}, {'rest_api': [{'attribute': 'use', 'value': 'standard-service'},
                {'attribute': 'check_command', 'value': 'check_ping_status!{port}!{path}!{protocol}!{auth}'},
                 {'attribute': 'hostgroup_name', 'map': {'hsop_fluentd': 'hsop-fluentd', 'hsop_es': 'hsop-es',
                  'hsop_kibana': 'hsop-kibana'}}]}, {'health_path': [{'attribute': 'use', 'value': 'standard-service'},
                   {'attribute': 'check_command', 'value': 'check_health_status!{port}!{path}!{protocol}!{auth}'},
                    {'attribute': 'hostgroup_name', 'map': {'hsop_fluentd': 'hsop-fluentd', 'hsop_es': 'hsop-es',
                     'hsop_kibana': 'hsop-kibana'}}]}]
        self.yml_data = yml_data
        self.nagios_services.loadbk = self.nagios_services.load
        self.nagios_services.load = Mock(return_value=yml_data)
        self.module.GENERIC_DISCOVERY = {
            'GD_SERVICE_TYPE': ['windows-service', 'windows-process', 'windows-firewall', 'passive-check'],
            'GD_APP_SERVICE_TYPE': ['webapi', 'windows-service', 'windows-process', 'windows-firewall',
                                    'passive-check']}

    def tearDown(self):
        NagiosConfigurationTest.TestCase.tearDown(self)

    def test_get_role(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        services = self.nagios_services(host)
        self.assertEqual(services.get_role(host), 'es')

    def test_get_filename(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        services = self.nagios_services(host)
        self.assertEqual(services.get_filename(host), 'services/hsop_es.cfg')

    def test_get_filename_none_productid(self):
        host = {'product_id': None, 'role': ['es']}
        services = self.nagios_services(host)
        self.assertEqual(services.get_filename(host), None)

    def test_get_filename_NA_role(self):
        host = {'product_id': None, 'role': 'NA'}
        services = self.nagios_services(host)
        self.assertEqual(services.get_filename(host), None)

    @mock.patch("__builtin__.open", create=True)
    def test_load(self, mock_open):
        self.nagios_services.load = self.nagios_services.loadbk
        mock_open.return_value = MagicMock(spec=file)
        self.mock_yml.load.return_value = 'data'
        self.assertEqual(self.nagios_services.load('example.yml'),'data')

    def test_get_hostgroup(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        services = self.nagios_services(host)
        self.assertEqual(services.get_hostgroup(host), 'hsop_es')

    def test_get_service_field(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'windows-service','serviceDescription':'Product__hsop__windows-service__service1__Status', 'applicationMonitoring':
        {'monitoringDetails':{'resource': 'service_name'}}}
        services = self.nagios_services(host)
        expected_output =  {'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service', 'check_command':
        'check_win_service!^\"{arg}\"$!_NumGood=1:', 'service_description': 'Product__hsop__windows-service__service1__Status'}
        self.assertEqual(services.get_service_field('windows-service',
            host, application), expected_output)

    def test_get_service_field_wrong_type(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'windows-serv'}
        services = self.nagios_services(host)
        self.assertEqual(services.get_service_field('windows-serv',
        host, application), {})

    def test_get_service_dsc(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'windows-service', 'serviceDescription':'Product__hsop__windows-service__service1__Status'}
        services = self.nagios_services(host)
        self.assertEqual(services.get_service_dsc(application), 'Product__hsop__windows-service__service1__Status')

    def test_get_windows_outcomes(self):
        host = {'product_id': 'hsop', 'role': ['es'], 'win_service_flag': 'yes'}
        application = {'name' : 'service1','serviceDescription':'Product__hsop__windows-service__service1__Status', 'applicationType': 'windows-service', 'applicationMonitoring':
        {'monitoringDetails':{'resource': 'service_name'}}}
        expected_output = {'hostgroup_name': 'hsop-es',
         'use': 'standard-service', 'check_command': 'check_win_service_without_argument!service_name',
         'service_description': 'Product__hsop__windows-service__service1__Status'}
        services = self.nagios_services(host)
        self.assertEqual(services.get_windows_outcomes('windows-service', host, application), expected_output)

    def test_get_windows_outcomes_process(self):
        host = {'product_id': 'hsop', 'role': ['es'], 'win_service_flag': 'yes'}
        application = {'name' : 'service1', 'applicationType': 'windows-process','serviceDescription': 'Product__hsop__windows-process__service1__Status','applicationMonitoring':
        {'monitoringDetails':{'resource': 'process_name'}}}
        expected_output = {'hostgroup_name': 'hsop-es',
         'use': 'wmi-dependant-service', 'check_command': 'check_win_process!process_name',
         'service_description': 'Product__hsop__windows-process__service1__Status'}
        services = self.nagios_services(host)
        self.assertEqual(services.get_windows_outcomes('windows-process', host, application), expected_output)

    def test_get_service_outcomes(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1','serviceDescription':'Product__hsop__windows-service__service1__Status', 'applicationType': 'windows-service', 'applicationMonitoring':
        {'monitoringDetails':{'resource': 'service_name'}}}
        services = self.nagios_services(host)
        expected_output = {'hostgroup_name': 'hsop-es',
         'use': 'wmi-dependant-service', 'check_command': 'check_win_service!^"service_name"$!_NumGood=1:',
         'service_description': 'Product__hsop__windows-service__service1__Status'}
        self.assertEqual(services.get_service_outcomes(host,application,'windows-service'), expected_output)

    def test_get_service_outcomes_rest_nourl(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'webapi'}
        services = self.nagios_services(host)
        expected_output = {}
        self.assertEqual(services.get_service_outcomes(host,application,'webapi'), expected_output)

    def test_get_service_outcomes_rest_url_with_path(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'webapi','serviceDescription':'Product__hsop__webapi__service1__Status', "applicationMonitoring":{'monitoringDetails':
        {'port': 9200,'resource': '/discovery/rest', 'protocol': 'https', 'host': '127.0.0.1'}}}
        services = self.nagios_services(host)
        expected_output =  {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': 'check_ping_status!9200!/discovery/rest!https!False',
        'service_description': 'Product__hsop__webapi__service1__Status'}
        self.assertEqual(services.get_service_outcomes(host,application,'webapi'), expected_output)

    def test_get_service_outcomes_rest_url_without_path(self):
        host = {'product_id': 'hsop', 'role': ['es'], 'applications':[{'name' : 'service1', 'applicationType': 'webapi', "applicationMonitoring":{'monitoringDetails':
        {'port': 9200, 'protocol': 'https', 'host': '127.0.0.1'}}}]}
        application = {'name' : 'service1', 'applicationType': 'webapi','serviceDescription':'Product__hsop__webapi__service1__Status', "applicationMonitoring":{'monitoringDetails':
        {'port': 9200, 'protocol': 'https', 'host': '127.0.0.1'}}}
        services = self.nagios_services(host)
        expected_output =   {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': "check_ping_status!9200!''!https!False",
        'service_description': 'Product__hsop__webapi__service1__Status'}
        self.assertEqual(services.get_service_outcomes(host,application,'webapi'), expected_output)

    def test_get_service_outcomes_rest_url_http(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'webapi','serviceDescription':'Product__hsop__webapi__service1__Status', "applicationMonitoring":{'monitoringDetails':
        {'port': 9200, 'protocol': 'http', 'host': '127.0.0.1'}}}
        services = self.nagios_services(host)
        expected_output =   {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': "check_ping_status!9200!''!http!False",
        'service_description': 'Product__hsop__webapi__service1__Status'}
        self.assertEqual(services.get_service_outcomes(host,application,'webapi'), expected_output)

    def test_get_service_outcomes_rest_http_without_port(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'webapi','serviceDescription':'Product__hsop__webapi__service1__Status', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'http', 'host': '127.0.0.1'}}}
        services = self.nagios_services(host)
        expected_output =   {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': "check_ping_status!80!''!http!False",
        'service_description': 'Product__hsop__webapi__service1__Status'}
        self.assertEqual(services.get_service_outcomes(host,application,'webapi'), expected_output)

    def test_get_service_outcomes_rest_https_without_port(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'webapi','serviceDescription':'Product__hsop__webapi__service1__Status', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'https', 'host': '127.0.0.1'}}}
        services = self.nagios_services(host)
        expected_output =   {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': "check_ping_status!443!''!https!False",
        'service_description': 'Product__hsop__webapi__service1__Status'}
        self.assertEqual(services.get_service_outcomes(host,application,'webapi'), expected_output)

    def test_get_service_outcomes_exception(self):
        host = {'product_id': 'hsop', 'role': ['es']}
        application = {'name' : 'service1', 'applicationType': 'windows-service'}
        services = self.nagios_services(host)
        services.get_service_field = Mock(return_value={})
        self.assertEqual(services.get_service_outcomes(host,application,'windows-service'), {})

    def test_get_results(self):
        data = {'product_id': 'hsop', 'role': ['es'], 'applications': [{'name' : 'service1',
        'applicationType': 'webapi','serviceDescription': 'Product__hsop__webapi__service1__Status', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'https', 'host': '127.0.0.1'}}}]}
        services = self.nagios_services(data)
        expected_output =    [{'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': "check_ping_status!443!''!https!False",
        'service_description': 'Product__hsop__webapi__service1__Status'}]
        self.assertEqual(services.get_results(data), expected_output)

    def test_get_results_wrongapptype(self):
        data = {'product_id': 'hsop', 'role': ['es'], 'applications': [{'name' : 'service1',
        'applicationType': 'webservice', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'https', 'host': '127.0.0.1'}}}]}
        services = self.nagios_services(data)
        expected_output = []
        self.assertEqual(services.get_results(data), expected_output)

    def test_get_results_multiple_apps(self):
        data = {'product_id': 'hsop', 'role': ['es'], 'applications': [{'name' : 'service1', 'serviceDescription': 'Product__hsop__webapi__service1__Status',
        'applicationType': 'webapi', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'https', 'host': '127.0.0.1'}}},{'name' : 'service2', 'serviceDescription': 'Product__hsop__windows-service__service2__Status',
        'applicationType': 'windows-service','applicationMonitoring': {'monitoringDetails': {'resource': 'DynamicService1'}}},
        {'name' : 'service3','serviceDescription': 'Product__hsop__webapi__service3__Status', 'applicationType': 'webapi', "applicationMonitoring":
        {'monitoringDetails':{'protocol': 'http', 'host': '127.0.0.1'}}},{'name' : 'service4', 'serviceDescription': 'Product__hsop__windows-service__service4__Status',
        'applicationType': 'windows-service', 'applicationMonitoring': {'monitoringDetails':
        {'resource': 'DynamicService2'}}}]}
        services = self.nagios_services(data)
        expected_output = [{'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': "check_ping_status!443!''!https!False",
        'service_description': 'Product__hsop__webapi__service1__Status'},
         {'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',
          'check_command': 'check_win_service!^\"DynamicService1\"$!_NumGood=1:',
          'service_description': 'Product__hsop__windows-service__service2__Status'},
          {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
           'check_command': "check_ping_status!80!''!http!False",
           'service_description': 'Product__hsop__webapi__service3__Status'},
           {'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',
           'check_command': 'check_win_service!^\"DynamicService2\"$!_NumGood=1:',
            'service_description': 'Product__hsop__windows-service__service4__Status'}]
        self.assertEqual(services.get_results(data), expected_output)

    def test_get_results_noapps(self):
        data = data = {'product_id': 'hsop', 'role': ['es'], 'applications' : None}
        services = self.nagios_services(data)
        expected_output =    []
        self.assertEqual(services.get_results(data), expected_output)

    def test_get_monitoring_url(self):
        application = {'name' : 'service1',
        'applicationType': 'webapi', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'https', 'host': '127.0.0.1'}}}
        services = self.nagios_services(application)
        expected_output = {'path': "''", 'protocol': 'https', 'port': '443'}
        self.assertEqual(services.get_monitoring_url(application), expected_output)

    def test_get_monitoring_url_empty(self):
        application = {'name' : 'service1',
        'applicationType': 'webapi', "applicationMonitoring":{'monitoringDetails':{}}}
        services = self.nagios_services(application)
        expected_output = {}
        self.assertEqual(services.get_monitoring_url(application), expected_output)

    def test_get_monitoring_no_url(self):
        application = {'name' : 'service1',
        'applicationType': 'rest', "applicationMonitoring":{'monitoringDetails':{}}}
        services = self.nagios_services(application)
        expected_output = {}
        self.assertEqual(services.get_monitoring_url(application), expected_output)

    def test_get_monitoring_no_url_https_port(self):
        application = {'name' : 'service1',
        'applicationType': 'webapi', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'https', 'host': '127.0.0.1', 'port' : ''}}}
        services = self.nagios_services(application)
        expected_output = {'path': "''", 'protocol': 'https', 'port': '443'}
        self.assertEqual(services.get_monitoring_url(application), expected_output)

    def test_get_monitoring_no_url_http_port(self):
        application = {'name' : 'service1',
        'applicationType': 'webapi', "applicationMonitoring":{'monitoringDetails':
        {'protocol': 'http', 'host': '127.0.0.1', 'port' : ''}}}
        services = self.nagios_services(application)
        expected_output = {'path': "''", 'protocol': 'http', 'port': '80'}
        self.assertEqual(services.get_monitoring_url(application), expected_output)

    def test_get_model(self):
        config = [{'hostgroup_name': 'hsop-es', 'use': 'standard-service',
        'check_command': 'check_http_port!443!-S',
        'service_description': 'Product__abc__rest__service1_status'},
         {'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',
          'check_command': 'check_win_process!service2',
          'service_description': 'Product__abc__windows-service__service2_status'},
          {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
           'check_command': 'check_http_port!80!',
           'service_description': 'Product__abc__rest__service3_status'},
           {'hostgroup_name': 'hsop-es', 'use': 'standard-service',
           'check_command': 'check_http_port!80!',
           'service_description': 'Product__abc__rest__service3_status'},
           {'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',
           'check_command': 'check_win_process!service4',
            'service_description': 'Product__abc__windows-service__service4_status'}]
        self.mock_pynag.Model.Service.return_value = {}
        services = self.nagios_services(config)
        expected_output = "{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_http_port!443!-S',\
 'service_description': 'Product__abc__rest__service1_status'}\
{'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',\
 'check_command': 'check_win_process!service2',\
 'service_description': 'Product__abc__windows-service__service2_status'}\
{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_http_port!80!',\
 'service_description': 'Product__abc__rest__service3_status'}\
{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_http_port!80!',\
 'service_description': 'Product__abc__rest__service3_status'}\
{'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',\
 'check_command': 'check_win_process!service4',\
 'service_description': 'Product__abc__windows-service__service4_status'}"
        self.assertEqual(services.get_model(config), expected_output)

    def test_get_model_empty(self):
        config = []
        services = self.nagios_services(config)
        expected_output = ''
        self.assertEqual(services.get_model(config), expected_output)

    def test_eval_with_none_productid(self):
        data = [{'product_id': None, 'role': ['es'], 'hostname': '192.168.59.12'}]
        host = {'role':'y'}
        service_configs = {'a':'b'}
        services = self.nagios_services(data)
        self.assertEqual(services.evaluate(host, service_configs), {})


    def test_eval_mixapps(self):
        data = [{
        'product_version': '1.1',
        'product_id': 'HSOP',
        'endpoints': [{
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        }],
        'tags': 'production',
        'hostname': '192.168.59.12',
        'modules': ['ISCVGD'],
        'applications': [{
            'enableMonitoring': 'true',
            'serviceDescription': 'Product__HSOP__WebApi__vmware__Status',
            'applicationMonitoring': {
                'monitoringType': 'shinken',
                'monitoringDetails': {
                        'resource': '<path>',
                        'host': '<service>',
                        'protocol': 'https',
                        'port': '<port>'
                    },
                    'Sample': {
                        'payload': '<httpRequestPayload>',
                        'response': '<httpResponseCode>'
                    },
                'runbookMessages': {
                    'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                    'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
                }
            },
            'applicationType': 'WebApi',
            'version': '1.0.0.0',
            'name': 'vmware'
        }, {
            'enableMonitoring': 'true',
            'serviceDescription': 'Product__HSOP__Windows-service__DynamicServiceFiles__Status',
            'applicationMonitoring': {
                'monitoringType': 'shinken',
                'monitoringDetails': {'resource': 'DynamicService'},
                'runbookMessages': {
                    'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                    'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
                }
            },
            'applicationType': 'Windows-service',
            'version': '1.0.0.0',
            'name': 'DynamicServiceFiles'
        }],
        'ISCVGD': {
            'endpoint': {
                'scanner': 'ISCVGD',
                'address': '192.168.59.63'
            },
            'tags': 'production'
        },
        'role': ['es'],
        'Components': [{
            'version': '1.1',
            'name': 'Health Suite On Premise'
        }],
        'address': '192.168.59.12',
        'siteid': 'MAN11',
        '_id': 'MAN11-192.168.59.12',
        'product_name': 'Health Suite On Premise'
    }
]
        services = self.nagios_services(data)
        host = {'product_id': 'hsop', 'role': ['es']}
        service_configs = {}
        self.mock_pynag.Model.Service.return_value = {}
        expected_output =  {'services/hsop_es.cfg': "{'hostgroup_name':\
 'hsop-es', 'use': 'standard-service', 'check_command':\
 'check_ping_status!<port>!<path>!https!False', 'service_description':\
 'Product__HSOP__WebApi__vmware__Status'}{'hostgroup_name': 'hsop-es',\
 'use': 'wmi-dependant-service',\
 'check_command': 'check_win_service!^\"DynamicService\"$!_NumGood=1:',\
 'service_description': 'Product__HSOP__Windows-service__DynamicServiceFiles__Status'}"}
        self.nagios_services.get_model = MagicMock(name='get_model')
        self.nagios_services.get_model.return_value = "{'hostgroup_name':\
 'hsop-es', 'use': 'standard-service', 'check_command':\
 'check_ping_status!<port>!<path>!https!False', 'service_description':\
 'Product__HSOP__WebApi__vmware__Status'}{'hostgroup_name': 'hsop-es',\
 'use': 'wmi-dependant-service',\
 'check_command': 'check_win_service!^\"DynamicService\"$!_NumGood=1:',\
 'service_description': 'Product__HSOP__Windows-service__DynamicServiceFiles__Status'}"
        self.assertEqual(services.evaluate(host, service_configs), expected_output)

    def test_eval_noapps(self):
        data = [{
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.12',
    'modules': ['ISCVGD'],
    'applications': 'None',
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['es'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.12',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.12',
    'product_name': 'Health Suite On Premise'
}, {
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.14',
    'modules': ['ISCVGD'],
    'applications': 'None',
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['es'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.14',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.14',
    'product_name': 'Health Suite On Premise'
}, {
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.13',
    'modules': ['ISCVGD'],
    'applications': 'None',
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['es'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.13',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.13',
    'product_name': 'Health Suite On Premise'
}]
        services = self.nagios_services(data)
        host = {'product_id': 'hsop', 'role': ['es']}
        service_configs = {}
        self.mock_pynag.Model.Service.return_value = {}
        expected_output = {'services/hsop_es.cfg': ''}
        self.assertEqual(services.evaluate(host, service_configs), expected_output)

    def test_eval_multiplerest(self):
        data = [{
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.12',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmware'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmwa'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmw'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['es'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.12',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.12',
    'product_name': 'Health Suite On Premise'
}]
        services = self.nagios_services(data)
        host = {'product_id': 'hsop', 'role': ['es']}
        service_configs = {}
        self.mock_pynag.Model.Service.return_value = {}
        expected_output = {'services/hsop_es.cfg':
 "{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_ping_status!<port>!<path>!https!False',\
 'service_description': 'Product__HSOP__WebApi__vmware__Status'}\
{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_ping_status!<port>!<path>!https!False',\
 'service_description': 'Product__HSOP__WebApi__vmwa__Status'}\
{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_ping_status!<port>!<path>!https!False',\
 'service_description': 'Product__HSOP__WebApi__vmw__Status'}"}
        self.nagios_services.get_model = MagicMock(name='get_model')
        self.nagios_services.get_model.return_value = "{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_ping_status!<port>!<path>!https!False',\
 'service_description': 'Product__HSOP__WebApi__vmware__Status'}\
{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_ping_status!<port>!<path>!https!False',\
 'service_description': 'Product__HSOP__WebApi__vmwa__Status'}\
{'hostgroup_name': 'hsop-es', 'use': 'standard-service',\
 'check_command': 'check_ping_status!<port>!<path>!https!False',\
 'service_description': 'Product__HSOP__WebApi__vmw__Status'}"
        self.assertEqual(services.evaluate(host, service_configs), expected_output)

    def test_eval_multipleappsw(self):
        data = [{
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.17',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails':{'resource': 'service_name1'},
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'Windows-service',
        'version': '1.0.0.0',
        'name': 'DynamicService1'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['fluentd'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.17',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.17',
    'product_name': 'Health Suite On Premise'
}, {
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.16',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails':{'resource': 'service_name2'},
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'Windows-service',
        'version': '1.0.0.0',
        'name': 'DynamicService2'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['es'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.16',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.16',
    'product_name': 'Health Suite On Premise'
}, {
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.18',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails':{'resource': 'service_name3'},
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'Windows-service',
        'version': '1.0.0.0',
        'name': 'DynamicService3'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['kibana'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.18',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.18',
    'product_name': 'Health Suite On Premise'
}]
        services = self.nagios_services(data)
        host = {'product_id': 'hsop', 'role': ['es']}
        service_configs = {}
        self.mock_pynag.Model.Service.return_value = {}
        expected_output = {'services/hsop_es.cfg':
 "{'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',\
 'check_command': 'check_win_service!^\"service_name2\"$!_NumGood=1:',\
 'service_description': 'Product__HSOP__Windows-service__DynamicService2__Status'}"}
        self.nagios_services.get_model = MagicMock(name='get_model')
        self.nagios_services.get_model.return_value =  "{'hostgroup_name': 'hsop-es', 'use': 'wmi-dependant-service',\
 'check_command': 'check_win_service!^\"service_name2\"$!_NumGood=1:',\
 'service_description': 'Product__HSOP__Windows-service__DynamicService2__Status'}"
        self.assertEqual(services.evaluate(host, service_configs), expected_output)

    def test_eval_multipleappsr(self):
        data = [{
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.12',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'false',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmware'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmwa'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmw'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['es'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.12',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.12',
    'product_name': 'Health Suite On Premise'
}, {
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.13',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmware'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmwa'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmw'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['kibana'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.13',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.13',
    'product_name': 'Health Suite On Premise'
}, {
    'product_version': '1.1',
    'product_id': 'HSOP',
    'endpoints': [{
        'scanner': 'ISCVGD',
        'address': '192.168.59.63'
    }],
    'tags': 'production',
    'hostname': '192.168.59.14',
    'modules': ['ISCVGD'],
    'applications': [{
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmware'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            },
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmwa'
    }, {
        'enableMonitoring': 'true',
        'applicationMonitoring': {
            'monitoringType': 'shinken',
            'monitoringDetails': {
                    'resource': '<path>',
                    'host': '<service>',
                    'protocol': 'https',
                    'port': '<port>'
                },
                'Sample': {
                    'payload': '<httpRequestPayload>',
                    'response': '<httpResponseCode>'
                },
            'runbookMessages': {
                'warning': '<mandatory_runbook_message_to_show_to_our_lx_team_on_warning>',
                'critical': '<mandatory_runbook_message_to_show_to_our_lx_team_on_critical>'
            }
        },
        'applicationType': 'WebApi',
        'version': '1.0.0.0',
        'name': 'vmw'
    }],
    'ISCVGD': {
        'endpoint': {
            'scanner': 'ISCVGD',
            'address': '192.168.59.63'
        },
        'tags': 'production'
    },
    'role': ['fluentd'],
    'Components': [{
        'version': '1.1',
        'name': 'Health Suite On Premise'
    }],
    'address': '192.168.59.14',
    'siteid': 'MAN11',
    '_id': 'MAN11-192.168.59.14',
    'product_name': 'Health Suite On Premise'
}]
        services = self.nagios_services(data)
        host = {'product_id': 'hsop', 'role': ['es']}
        service_configs = {}
        self.mock_pynag.Model.Service.return_value = {}
        expected_output = {'services/hsop_es.cfg':
 "{'hostgroup_name': 'hsop-es', 'use': 'standard-service', 'check_command':\
 'check_ping_status!<port>!<path>!https!False', 'service_description': 'Product__HSOP__WebApi__vmwa__Status'}"}
        self.nagios_services.get_model = MagicMock(name='get_model')
        self.nagios_services.get_model.return_value = "{'hostgroup_name': 'hsop-es', 'use': 'standard-service', 'check_command':\
 'check_ping_status!<port>!<path>!https!False', 'service_description': 'Product__HSOP__WebApi__vmwa__Status'}"
        self.assertEqual(services.evaluate(host, service_configs), expected_output)

if __name__ == '__main__':
    unittest.main()

