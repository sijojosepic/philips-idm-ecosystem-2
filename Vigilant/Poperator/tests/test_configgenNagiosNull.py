import unittest
from mock import MagicMock, patch


class ConfiggenNagiosTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_logging = MagicMock(name='logging')
        self.mock_poperator = MagicMock(name='poperator')
        modules = {
            'logging': self.mock_logging,
            'poperator.nagios_configuration': self.mock_poperator.nagios_configuration,
            'poperator.prometheus_configuration': self.mock_poperator.prometheus_configuration
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from poperator import configgenNagiosNull
        self.configgenNagiosNull = configgenNagiosNull

        var1 = [
            {
                'hostname': 'host1',
                'address': '10.0.0.1',
            },
            {
                'hostname': 'host02',
                'address': '10.0.0.2',
            }
        ]
        self.cnn = self.configgenNagiosNull.ConfiggenNagiosNull(var1)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_generate(self):
        self.assertEqual(
            self.cnn.generate(),
            {'hosts/host1.cfg': None, 'hosts/host02.cfg': None}
        )


if __name__ == '__main__':
    unittest.main()
