import unittest
import mock
from mock import MagicMock, patch


class PrometheusTests:
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_yml = MagicMock(name='yaml')
            self.mock_ymlfile = MagicMock(name='ymlfile')
            modules = {
                'yaml': self.mock_yml,
                'ymlfile': self.mock_ymlfile
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import poperator.prometheus_configuration
            self.prometheus_configuration = poperator.prometheus_configuration
            self.prometheus_configuration.config = {'scrape_configs': [], 'global': 
            {'evaluation_interval': '15s', 'scrape_interval': '15s'}, 'rule_files':
             ['node_exporter.yml']}
            self.module = self.prometheus_configuration.PrometheusConfig('hostname')

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()

class PrometheusConfigTest(PrometheusTests.TestCase):
        def setUp(self):
            PrometheusTests.TestCase.setUp(self)

        def tearDown(self):
            PrometheusTests.TestCase.tearDown(self)
            
        def test_evaluate(self):
            self.module.hosts = [{"product_id": "fcs","hostname": "192.168.59.64",
            "role": "CS","prometheusAgents":[{"name": "node_exporter",
            "uri": "/node/metrics/"}]}]
            self.module.jobs = MagicMock()
            self.module.evaluate()
            self.module.jobs.create_jobs.assert_called_once_with('192.168.59.64',[{"name": "node_exporter", "uri": "/node/metrics/"}])
            self.mock_yml.dump.assert_called_once_with(self.prometheus_configuration.config)

        def test_evaluate_with_rules(self):
            self.module.hosts = [{"product_id": "fcs","hostname": "192.168.59.64",
            "role": "CS","prometheusAgents":[{"name": "node_exporter",
            "uri": "/node/metrics/"}]}]
            self.module.jobs = MagicMock()
            self.module.rules = MagicMock()
            self.module.jobs.agents = set(['process_exporter'])
            self.module.evaluate()
            self.module.jobs.create_jobs.assert_called_once_with('192.168.59.64',[{"name": "node_exporter", "uri": "/node/metrics/"}])
            self.module.rules.add_rules.assert_called_once_with('fcs')
            self.mock_yml.dump.assert_called_once_with(self.prometheus_configuration.config)

class PrometheusRulesTest(PrometheusTests.TestCase):
        def setUp(self):
            PrometheusTests.TestCase.setUp(self)
            self.prometheus_rules = self.prometheus_configuration.PrometheusRules(self.module.jobs)

        def tearDown(self):
            PrometheusTests.TestCase.tearDown(self)

        def test_add_rules(self):
            self.module.jobs.agents = set(['process_exporter'])
            self.prometheus_rules.add_rules('fcs')
            modified_config = {'scrape_configs': [], 'global': 
            {'evaluation_interval': '15s', 'scrape_interval': '15s'},
             'rule_files': ['node_exporter.yml', 'fcs_process_exporter.yml']}
            self.assertEqual(self.prometheus_configuration.config, modified_config)

class PrometheusJobsTest(PrometheusTests.TestCase):
    def setUp(self):
       PrometheusTests.TestCase.setUp(self)
       self.prometheus_jobs = self.prometheus_configuration.PrometheusJob()

    def tearDown(self):
        PrometheusTests.TestCase.tearDown(self)

    def test_create_jobs(self):
        prometheus_agents = [{"name": "node_exporter",
        "uri": "/node/metrics/"}]
        self.prometheus_jobs.search_agent = MagicMock(return_value={})
        self.prometheus_jobs.construct_job = MagicMock(return_value={'job': 'hello'})
        self.prometheus_jobs.create_jobs('localhost',prometheus_agents)
        expected_output = {'scrape_configs': [{'job': 'hello'}], 'global':
         {'evaluation_interval': '15s', 'scrape_interval': '15s'},
          'rule_files': ['node_exporter.yml']}
        self.assertEqual(self.prometheus_configuration.config, expected_output)

    def test_create_jobs_with_search_agent(self):
        prometheus_agents = [{"name": "node_exporter",
        "uri": "/node/metrics/"}]
        self.prometheus_jobs.search_agent = MagicMock(return_value={'a':'b'})
        self.prometheus_jobs.modify_jobs = MagicMock()
        self.prometheus_jobs.create_jobs('localhost',prometheus_agents)
        self.prometheus_jobs.modify_jobs.assert_called_once_with('localhost', {'a': 'b'})

    def test_create_jobs_agent(self):
        prometheus_agents = [{"name": "process_exporter",
        "uri": "/process/metrics/"}]
        self.prometheus_jobs.search_agent = MagicMock(return_value={})
        self.prometheus_jobs.construct_job = MagicMock(return_value={'job': 'hello'})
        self.prometheus_jobs.create_jobs('localhost',prometheus_agents)
        self.assertEqual(self.prometheus_jobs.agents, set(['process_exporter']))

    def test_construct_job(self):
        agent = {'name': 'node_exporter', 'uri': '/node/metrics'}
        expected_output = {'scheme': 'https', 'metrics_path': '/node/metrics', 
        'static_configs': [{'targets': ['localhost']}], 
        'job_name': 'node_exporter', 'tls_config': {'insecure_skip_verify': True}}
        self.assertEqual(self.prometheus_jobs.construct_job(agent,'localhost'),expected_output)

    def test_search_agent(self):
        self.assertEqual(self.prometheus_jobs.search_agent('node_exporter'),{})

    def test_search_agent_with_job(self):
        self.prometheus_configuration.config = {'scrape_configs': [{'job_name':'node_exporter'}]}
        self.assertEqual(self.prometheus_jobs.search_agent('node_exporter'),{'job_name':'node_exporter'})

    def test_modify_jobs(self):
        job = {'static_configs': [{'targets': ['host1']}], 'job_name': 'prometheus'}
        self.prometheus_jobs.modify_jobs('localhost', job)

if __name__ == '__main__':
    unittest.main()

