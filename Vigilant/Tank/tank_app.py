#!/usr/bin/env python

from bottle import Bottle, BaseRequest
from tank.controllers import (
    audit,
    country,
    dashboard,
    device,
    description_details,
    directory,
    event,
    exception,
    fact,
    field,
    info,
    site,
    state,
    subscription,
    tasks,
    tags,
    iam_admin_user,
    iam_user,
    iam_group,
    iam_roles,
    geo_user,
    geo_user_audit,
    site_rules,
    site_notes,
    lhost_utilities,
    racks,
    site_updates,
    site,
    state,
    site_rules,
    site_notes,
    solution_landscape,
    subscription,
    tasks,
    tags,
    technical_id,
    groups,
    nodes_ips,
    site_configuration,
    scanner,
    site_info_xml,
    rsa_encoding
)
from tank.tnconfig import MONGODB
from tank.util import get_logger

log = get_logger()


def get_app():
    app = Bottle()
    db_params = dict(uri = MONGODB['URL'],
                     db_name = MONGODB['DB_NAME'],
                     username = MONGODB['USER_NAME'],
                     password = MONGODB['PASSWORD'],
                     protocol = MONGODB['PROTOCOL'])

    app.mount('/audit/', audit.get_app(**db_params))
    app.mount('/info/', info.get_app(**db_params))
    app.mount('/groups/', iam_group.get_app(**db_params))
    app.mount('/dashboard/', dashboard.get_app(**db_params))
    app.mount('/device/', device.get_app(**db_params))
    app.mount('/description_details/', description_details.get_app(**db_params))
    app.mount('/directory/', directory.get_app(**db_params))
    app.mount('/subscription/', subscription.get_app(**db_params))
    app.mount('/facts/', fact.get_app(**db_params))
    app.mount('/event/', event.get_app(**db_params))
    app.mount('/exceptions/', exception.get_app(**db_params))
    app.mount('/field/', field.get_app(**db_params))
    app.mount('/site/', site.get_app(**db_params))
    app.mount('/country/', country.get_app(**db_params))
    app.mount('/state/', state.get_app(**db_params))
    app.mount('/tags/', tags.get_app(**db_params))
    app.mount('/geo_user/', geo_user.get_app(**db_params))
    app.mount('/geo_user_audit/', geo_user_audit.get_app(**db_params))
    app.mount('/iam_admin_user/', iam_admin_user.get_app(**db_params))
    app.mount('/iam_user/', iam_user.get_app(**db_params))
    app.mount('/iam_roles/', iam_roles.get_app(**db_params))
    app.mount('/site_rules/', site_rules.get_app(**db_params))
    app.mount('/notes/', site_notes.get_app(**db_params))
    app.mount('/siteupdates/', site_updates.get_app(**db_params)),
    app.mount('/racks/', racks.get_app(**db_params)),
    app.mount('/solution_landscape/', solution_landscape.get_app(**db_params))
    app.mount('/lhost_utilities/', lhost_utilities.get_app(**db_params))
    app.mount('/groups_nodes/', groups.get_app(**db_params))
    app.mount('/nodesipinventory/', nodes_ips.get_app(**db_params))
    app.mount('/technical_id/', technical_id.get_app(**db_params))
    app.mount('/site_configuration/',site_configuration.get_app(**db_params))
    app.mount('/scanner/',scanner.get_app(**db_params))
    app.mount('/site_info/', site_info_xml.get_app(**db_params))
    app.mount('/rsa_encoding/', rsa_encoding.get_app(**db_params))
    app.merge(tasks.get_app())
    app.get('/health', callback=health)
    return app


def health():
    log.debug('Returning Health')
    return 'OK'


BaseRequest.MEMFILE_MAX = 1024 * 512 * 2
app = get_app()

if __name__ == '__main__':
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run(host='0.0.0.0', port=8080, debug=True)
