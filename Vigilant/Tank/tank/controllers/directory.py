from bottle import request
from tank.models.directory import DirectoryModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/<site_id>', callback=get_directories)
    app.post('/', callback=save_directory)
    app.put('/', callback=update_directory)
    app.delete('/', callback=delete_directory)
    return app


def get_directories(mongodb, site_id):
    return DirectoryModel(mongodb, site_id).get_directories()


def save_directory(mongodb):
    return DirectoryModel(mongodb, request.json['siteid']).\
        save_directory(request.json)


def update_directory(mongodb):
    return DirectoryModel(mongodb, request.json['siteid']).\
        update_directory(request.json)


def delete_directory(mongodb):
    return DirectoryModel(mongodb).delete_directory(request.query['ids'])
