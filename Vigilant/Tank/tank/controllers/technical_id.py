from tank.models.technical_id import TechnicalIdModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/all_ids', callback = get_all_technical_ids)
    app.get('/product/<product_id>', callback = get_technical_id)
    return app


def get_all_technical_ids(mongodb):
    return TechnicalIdModel(mongodb).get_all_technical_ids()


def get_technical_id(mongodb, product_id):
    return TechnicalIdModel(mongodb).get_technical_id(product_id)
