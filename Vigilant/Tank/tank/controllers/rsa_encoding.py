from tank.models.rsa_encoding import RSAEncoding
from tank.util import get_bottle_app_with_mongo
from bottle import request


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/encrypt', callback = rsa_encrypt)
    app.post('/decrypt', callback = rsa_decrypt)
    return app


def rsa_encrypt(mongodb):
    return RSAEncoding().rsa_encrypt(request.json['user_input'])

def rsa_decrypt(mongodb):
    return RSAEncoding().rsa_decrypt(request.json['user_input'])
