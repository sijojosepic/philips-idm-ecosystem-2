from bottle import request
from tank.models.site_rules import RulesModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/save_rules', callback=save_rules)
    app.get('/rules/<siteid>', callback=get_rules_with_siteid)
    app.get('/site_check/<siteid>', callback=is_site_under_maintenance)
    app.get('/node_check/<siteid>/<nodename>', callback=is_node_under_maintenance)
    app.get('/node/<siteid>/<nodename>', callback=get_node_maintenance_schedule)
    app.get('/rules/list', callback=get_rule_list)
    app.post('/rules/cache', callback=update_rules_in_cache)
    app.get('/rules', callback=get_rules)
    app.post('/rules/assignment', callback=save_assignment_rule)
    app.delete('/rules/site_unassign/<siteid>', callback=delete_site_assignment_rule)
    app.delete('/maintenance/remove_site/<siteid>', callback=delete_site_maintenance_rule)
    app.delete('/maintenance/remove_node/<siteid>/<nodename>', callback=delete_node_maintenance_rule)
    return app

def save_rules(mongodb):
    return RulesModel(mongodb).save_rules(request.json)

def get_rules_with_siteid(mongodb, siteid):
    return RulesModel(mongodb).get_rules_with_siteid(siteid)

def is_site_under_maintenance(mongodb, siteid):
    return RulesModel(mongodb).is_site_under_maintenance(siteid)

def is_node_under_maintenance(mongodb, siteid, nodename):
    return RulesModel(mongodb).is_node_under_maintenance(siteid, nodename)

def get_node_maintenance_schedule(mongodb, siteid, nodename):
    return RulesModel(mongodb).get_node_maintenance_schedule(siteid, nodename)

def get_rule_list(mongodb):
    return RulesModel(mongodb).get_rule_list()

def update_rules_in_cache(mongodb):
    return RulesModel(mongodb).update_rules_in_cache()

def get_rules(mongodb):
    return RulesModel(mongodb).get_rules(request.query)

def save_assignment_rule(mongodb):
    return RulesModel(mongodb).save_assignment_rule(request.json)

def delete_site_assignment_rule(mongodb, siteid):
    return RulesModel(mongodb).delete_site_assignment_rule(siteid)

def delete_site_maintenance_rule(mongodb, siteid):
    return RulesModel(mongodb).delete_site_maintenance_rule(siteid)

def delete_node_maintenance_rule(mongodb, siteid, nodename):
    return RulesModel(mongodb).delete_node_maintenance_rule(siteid, nodename)

