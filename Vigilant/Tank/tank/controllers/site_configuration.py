from bottle import request
from tank.models.site_configuration import SiteConfigurationModel
from tank.util import get_bottle_app_with_mongo

def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/<siteid>', callback=get_data)
    app.post('/approve', callback=approve_config)
    return app

def get_data(mongodb, siteid):
    return SiteConfigurationModel(mongodb).get_data(siteid)

def approve_config(mongodb):
    return SiteConfigurationModel(mongodb).approve_config(request.json['site'], request.json['nodes'])
