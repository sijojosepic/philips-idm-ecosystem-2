from bottle import request
from tank.models.iam_roles import IAMRoleModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/', callback=get_all_roles)
    return app


def get_all_roles(mongodb):
    return IAMRoleModel(mongodb).get_all_roles(request.query)
