from bottle import request
from tank.models.site_updates import SiteUpdatesModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/<site_id>', callback=get_site_updates)
    app.post('/', callback=save_site_updates)
    app.put('/', callback=update_site_updates)
    app.delete('/', callback=delete_site_updates)
    return app


def get_site_updates(mongodb, site_id):
    return SiteUpdatesModel(mongodb)\
        .get_site_updates(site_id)


def save_site_updates(mongodb):
    return SiteUpdatesModel(mongodb).save_site_updates(request.json)


def update_site_updates(mongodb):
    return SiteUpdatesModel(mongodb)\
        .update_site_updates(request.json)


def delete_site_updates(mongodb):
    return SiteUpdatesModel(mongodb)\
        .delete_site_updates(request.query['ids'])
