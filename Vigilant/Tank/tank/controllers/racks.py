from bottle import request
from tank.models.racks import RackModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/<site_id>/<location_id>',
            callback=get_rack_details_by_location_id)
    app.post('/', callback=create_rack_details_by_location_id)
    app.put('/', callback=update_rack_details_by_location_id)
    app.delete('/<site_id>/<location_id>', callback=delete_location_details)
    app.get('/<site_id>', callback=get_locations)
    return app


def get_rack_details_by_location_id(mongodb, site_id, location_id):
    return RackModel(mongodb)\
        .get_rack_details_by_location_id(site_id, location_id)


def create_rack_details_by_location_id(mongodb):
    return RackModel(mongodb).create_rack_details_by_location_id(request.json)


def update_rack_details_by_location_id(mongodb):
    return RackModel(mongodb).update_rack_details_by_location_id(request.json)


def delete_location_details(mongodb, site_id, location_id):
    return RackModel(mongodb).delete_location_details(site_id, location_id)

def get_locations(mongodb, site_id):
    return RackModel(mongodb).get_locations(site_id)
