from bottle import request

from tank.models.event import EventModel
from tank.tnconfig import HANDLER_SERVICES
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/history', callback=get_historical_data)
    app.get('/handlerservices', callback=get_handler_services)
    app.get('/handlerreport/services/<service>', callback=get_handler_report)
    return app


def get_historical_data(mongodb):
    return EventModel(mongodb).get_historical(request.query)


def get_handler_services():
    return {'services': HANDLER_SERVICES}


def get_handler_report(mongodb, service):
    return EventModel(mongodb).get_handler_report(service, request.query)
