from bottle import request
from tank.models.description_details import DescriptionModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/<site_id>', callback=get_details)
    app.post('/', callback=save_details)
    app.put('/', callback=update_details)
    return app


def get_details(mongodb, site_id):
    return DescriptionModel(mongodb, site_id).get_details()


def save_details(mongodb):
    return DescriptionModel(mongodb, request.json['siteid']).\
        save_details(request.json)


def update_details(mongodb):
    return DescriptionModel(mongodb, request.json['siteid']).\
        update_details(request.json)
