from tank.models.nodes_ips import NodesIPInventoryModel
from tank.util import get_bottle_app_with_mongo
from bottle import request


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/nodes/<site_id>', callback=get_nodes)
    app.get('/ips/available/<site_id>', callback=get_available_site_ips)
    app.get('/ips/reserved/<site_id>', callback=get_reserve_site_ips)
    app.get('/ips/consumed/<site_id>', callback=get_consumed_site_ips)
    app.get('/facts/consumed/<site_id>', callback=get_ips)
    app.put('/ips/setstatus', callback=set_ip_status)
    return app


def get_available_site_ips(mongodb, site_id):
    return NodesIPInventoryModel(mongodb, site_id)\
        .get_available_site_ips(request.query['refresh'])


def get_reserve_site_ips(mongodb, site_id):
    return NodesIPInventoryModel(mongodb, site_id).get_reserve_site_ips()


def get_ips(mongodb, site_id):
    return NodesIPInventoryModel(mongodb, siteid=site_id).\
        get_ips(request.query['refresh'])


def get_nodes(mongodb, site_id):
    return NodesIPInventoryModel(mongodb, siteid=site_id).\
        get_nodes(request.query['refresh'])


def set_ip_status(mongodb):
    return NodesIPInventoryModel(mongodb, siteid=request.json['siteid'])\
        .set_ip_status(request.json)


def get_consumed_site_ips(mongodb, site_id):
    return NodesIPInventoryModel(mongodb, site_id).\
        get_consumed_site_ips(request.query['refresh'])
