import requests
import time
from base64 import b64encode, b64decode, encodestring
import json
from urllib import urlencode, quote
from tank.tnconfig import IAM_API_ENDPOINTS, IAM_CREDENTIALS, COLLECTIONS, HTTP_PROXY, HTTPS_PROXY, IDM_USER_CREATE, \
    IDM_USER_EXISTS, ADMIN_USER_ID, ADMIN_USER_PASS, PROXY_FLAG, AUTH_HEADER, IAM_API_ENDPOINTS, IDM_DEFAULT_GROUPS, IDM_DEFAULT_ROLES
from tank.util import create_hmac, get_bottle_app_with_mongo, FernetCrypto, UserValidator, FORBIDDEN_STATUS_CODE
from bottle import request, abort
from tank.models.iam_user import IAMUserModel
from tank.models.iam_group import IAMGroupModel
from tank.models.iam_roles import IAMRoleModel
from tank.models.iam_views import IAMViewModel
from requests import HTTPError


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/login', callback=SignIn(request).post)
    app.post('/create', callback=SignUp(request).post)
    app.post('/reset', callback=PWChange(request).post)
    app.post('/logout', callback=LogoutUser(request).post)
    app.put('/create_default_groups', callback=Groups(request).create_default_groups)
    app.put('/create_default_roles', callback=Roles(request).create_default_roles)
    app.post('/remove_members', callback=Groups(request).remove_members)
    return app


class IAMAccessor(object):
    request = requests
    response = ''
    url = IAM_API_ENDPOINTS['IAM_URL']
    uri_path = ''
    headers = {}
    status_code = ''
    json = {},
    collection = None
    get_req_params = {}

    def __init__(self):
        pass

    def post(self, mongodb, payload='json'):
        if payload == 'json':
            r = self.request.post(self.get_url(), json=self.get_payload(), headers=self.get_headers(), proxies=self.get_proxy())
        else:
            r = self.request.post(self.get_url(), data=urlencode(self.get_payload()), headers=self.get_headers(),
                                  proxies=self.get_proxy())
        r.raise_for_status()
        try:
            self.response = r.json()
        except ValueError:
            self.response = ''
        self.headers = r.headers
        self.status_code = r.status_code
        return self.response

    def put(self):
        r = self.request.put(self.get_url(), json=self.get_payload(), headers=self.get_headers(), proxies=self.get_proxy())
        r.raise_for_status()

    def get(self):
        r = self.request.get(self.get_url(), headers=self.get_headers(), params = self.get_params(), proxies=self.get_proxy())
        r.raise_for_status()
        self.response = r.json

    def get_response(self):
        return self.response

    def sig_header(self):
        secret_key_prefix = 'DHPWS'
        sign_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())
        secret = ''.join([secret_key_prefix, FernetCrypto.decrypt(IAM_CREDENTIALS['SECRET_KEY'])])
        sig = create_hmac(b64encode(sign_time), secret)
        sig_header = ("HmacSHA256;Credential:{0};SignedHeaders:SignedDate;"
                      "Signature:{1}".format(FernetCrypto.decrypt(IAM_CREDENTIALS['SHARED_KEY']), b64encode(sig)))
        return sig_header

    def auth_string(self, result_type):
        base64string = encodestring('%s:%s' % (FernetCrypto.decrypt(IAM_CREDENTIALS['CLIENT_ID']),
                                               FernetCrypto.decrypt(IAM_CREDENTIALS['CLIENT_SECRET']))).replace('\n', '')
        return "%s %s" % (result_type , base64string)

    def get_headers(self):
        return self.headers

    def get_payload(self):
        return self.json

    def get_url(self):
        return self.url + self.uri_path

    def get_proxy(self):
        proxy_dict = {}
        if PROXY_FLAG:
            proxy_dict = {
                "http": HTTP_PROXY,
                "https": HTTPS_PROXY
            }
        return proxy_dict

    def get_params(self):
        pass


class SignIn(IAMAccessor):
    idm_url = IAM_API_ENDPOINTS['IDM_URL']
    uri_path = IAM_API_ENDPOINTS['OAUTH2_LOGIN_URI']
    info_path = IAM_API_ENDPOINTS['OAUTH2_LOGIN_INFO']
    uuid_path = IAM_API_ENDPOINTS['UUID_URI']
    user_info_path = IAM_API_ENDPOINTS['USER_INFO_URI']
    access_token = ''
    admin_token = ''
    login_id = ''
    request_obj = None
    role_ids = []
    view_ids = []
    routes_info = []

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return {'grant_type': 'password', 'username': self.request_obj.json.get('loginId'),
                'password': self.request_obj.json.get('password')}

    def post(self, mongodb, payload='text'):
        post_response = super(SignIn, self).post(mongodb, payload)
        if post_response:
            self.access_token = post_response.get('access_token')
            user_validator_instance = UserValidator()
            user_validator_instance.add_token(post_response)
            self.login_id = self.get_user_email()
            user_details = IAMUserModel(mongodb).get_user_details(self.login_id)
            ret_obj = {'message': 'Success',
                       'access_token': post_response.get('access_token'),
                       'refresh_token': post_response.get('refresh_token'),
                       'loginId': self.login_id
                       }
            if len(user_details) != 0:
                ret_obj['firstname'] = user_details[0]['givenName']
                ret_obj['lastname'] = user_details[0]['familyName']
                ret_obj['approved'] = user_details[0]['approved']
            else:
                user_details = self.get_user_information(mongodb)
                ret_obj['firstname'] = user_details['firstname']
                ret_obj['lastname'] = user_details['lastname']
            if 'groupID' in user_details[0]:
                ret_obj['routes_info'] = self.get_routes_info(mongodb, user_details[0]['groupID'], user_details[0]['approved'])
                ret_obj['roles'] = self.get_role_names(mongodb, user_details[0]['groupID'])
                return ret_obj
            else:
                abort(FORBIDDEN_STATUS_CODE, 'Bad Request')

    def get_role_names(self,mongodb, user_group_ids):
        roles = []
        for group_id in user_group_ids:
            group_info = IAMGroupModel(mongodb).get_group_by_id(group_id)
            roles.append(group_info['result'][0]['name'])
        return roles

    def get_routes_info(self, mongodb, user_group_ids, approval):
        self.role_ids = []
        for group_id in user_group_ids:
            roles = []
            group_info = IAMGroupModel(mongodb).get_group_by_id(group_id)
            if group_info['result'][0]['restricted']:
                if approval:
                    roles = group_info['result'][0]['roles']
            else:
                roles = group_info['result'][0]['roles']
            self.set_role_ids(mongodb, roles)
        self.view_ids = []
        for role_id in self.role_ids:
            roles_info = IAMRoleModel(mongodb).get_roles_by_id(role_id)
            views = roles_info['result'][0]['views']
            self.set_view_ids(mongodb, views)
        self.routes_info = IAMViewModel(mongodb).get_routes_info(self.view_ids)
        return self.routes_info

    def set_role_ids(self, mongodb, roles):
        for role in roles:
            if role not in self.role_ids:
                self.role_ids.append(role)

    def set_view_ids(self, mongodb, views):
        for view in views:
            if view not in self.view_ids:
                self.view_ids.append(view)

    def get_user_email(self):
        r = self.request.get(self.url + self.info_path, headers=self.get_user_info_headers(), proxies=self.get_proxy())
        r.raise_for_status()
        login_id = r.json().get('sub')
        return login_id

    def get_admin_token(self):
        admin_payload = {'grant_type': 'password', 'username': b64decode(ADMIN_USER_ID),
                         'password': b64decode(ADMIN_USER_PASS)}
        r = self.request.post(self.get_url(), data=urlencode(admin_payload), headers=self.get_headers(),
                              proxies=self.get_proxy())
        r.raise_for_status()
        admin_token = r.json().get('access_token')
        return admin_token

    def get_user_uuid(self, login_id, fetch_token=False):
        r = self.request.get("{0}{1}{2}".format(self.idm_url, self.uuid_path, login_id),
                             headers=self.get_user_uuid_headers(fetch_token), proxies=self.get_proxy())
        r.raise_for_status()
        if r.json().get('exchange'):
            uuid_respose = r.json().get('exchange').get('users')
            uuid = uuid_respose[0].get('userUUID')
            return uuid

    def get_user_information(self, mongodb):
        uuid = self.get_user_uuid(self.login_id, fetch_token=True)
        user_details = {}
        r = self.request.get("{0}{1}{2}".format(self.idm_url, self.user_info_path, uuid),
                             headers=self.get_user_info_headers(), proxies=self.get_proxy())
        r.raise_for_status()
        user_info_response = r.json().get('exchange')
        if user_info_response:
            user_details['email'] = user_info_response.get('loginId')
            user_details['firstname'] = user_info_response.get('profile').get('givenName')
            user_details['lastname'] = user_info_response.get('profile').get('familyName')
            IAMUserModel(mongodb).add_new_user(uuid, user_details)
        return user_details

    def get_headers(self):
        return {
            'Accept': 'application/json',
            'Authorization': self.auth_string('Basic'),
            'Content-Type': 'application/x-www-form-urlencoded',
            'api-version': '1'
        }

    def get_user_info_headers(self):
        return {
            'Accept': 'application/json',
            'Authorization': ('Bearer %s' % self.access_token).encode("utf-8")
        }

    def get_user_uuid_headers(self, fetch_token=False):
        if fetch_token:
            self.admin_token = self.get_admin_token()
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': ('Bearer %s' % self.admin_token).encode("utf-8")
        }


# Creates user under same organization
class SignUp(IAMAccessor):
    uri_path = IAM_API_ENDPOINTS['NEW_USER_URI']
    url = IAM_API_ENDPOINTS['IDM_URL']
    request_obj = None
    mongodb = None
    access_token = ''

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return {
            "resourceType": "Person",
            "managingOrganization": FernetCrypto.decrypt(IAM_CREDENTIALS['ORGANIZATION_ID']),
            "name": {
                "text": "",
                "family": self.request_obj.json.get('lastname'),
                "given": self.request_obj.json.get('firstname'),
                "prefix": ""
            },
            "telecom": [{
                "system": "email",
                "value": self.request_obj.json.get('email')
            }, {
                "system": "mobile",
                "value": self.request_obj.json.get('mobile')
            }]
        }

    def post(self, mongodb, payload='json'):
        post_response = super(SignUp, self).post(mongodb, payload)
        if post_response:
            if self.status_code == IDM_USER_EXISTS:
                return {'status_code': self.status_code, 'message': 'Email already registered'}
            elif self.status_code == IDM_USER_CREATE:
                uuid = self.headers.get('Location').split("/")[4]
                IAMUserModel(mongodb).add_new_user(uuid, self.request_obj.json)
                Groups(mongodb).assign_members_to_group(uuid, self.request_obj.json.get('groups', []))
                return {'status_code': self.status_code,
                        'message': 'Registeration Success!! Verification Email has been sent'}

    def get_headers(self):
        return {
            'hsdp-api-signature': self.sig_header(),
            'SignedDate': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'api-version': '1',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': self.get_auth_string()
        }

    def get_auth_string(self):
        url = IAM_API_ENDPOINTS['IAM_URL'] + IAM_API_ENDPOINTS['OAUTH2_LOGIN_URI']
        headers = {
            'Accept': 'application/json',
            'Authorization': self.auth_string('Basic'),
            'Content-Type': 'application/x-www-form-urlencoded',
            'api-version': '1'
        }
        payload = {'grant_type': 'password', 'username': b64decode(ADMIN_USER_ID),
                   'password': b64decode(ADMIN_USER_PASS)}
        r = self.request.post(url, data=urlencode(payload), headers=headers, proxies=self.get_proxy())
        self.access_token = r.json().get('access_token')
        return ('Bearer %s' % self.access_token).encode("utf-8")


class PWChange(IAMAccessor):
    uri_path = IAM_API_ENDPOINTS['RECOVER_PASSWORD']
    url = IAM_API_ENDPOINTS['IDM_URL']

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return {
            "resourceType": "Parameters",
            "parameter": [{
                'name': 'recoverPassword',
                'resource': {
                    'loginId': self.request_obj.json.get('email')
                }
            }]
        }

    def get_headers(self):
        return {
            'Content-Type': 'application/json',
            'hsdp-api-signature': self.sig_header(),
            'api-version': '1',
            'SignedDate': time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime()),
            'Accept': 'application/json',
        }


class LogoutUser(IAMAccessor):
    url = IAM_API_ENDPOINTS['IAM_URL']
    uri_path = IAM_API_ENDPOINTS['LOGOUT_USER_URI']
    refresh_token = ''

    def __init__(self, request):
        IAMAccessor.__init__(self)
        self.request_obj = request

    def get_payload(self):
        return {'token': self.request_obj.headers[AUTH_HEADER]}

    def post(self, mongodb, payload='text'):
        self.refresh_token = self.request_obj.headers[AUTH_HEADER]
        post_response = super(LogoutUser, self).post(mongodb, payload)
        user_validator_instance = UserValidator()
        user_validator_instance.delete_token(self.refresh_token)

    def get_headers(self):
        return {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': self.auth_string('Basic'),
            'Accept': 'application/json'
        }


class Groups(IAMAccessor):
    uri_path = IAM_API_ENDPOINTS['CREATE_GROUP']
    url = IAM_API_ENDPOINTS['IDM_URL']

    def __init__(self, request=None):
        IAMAccessor.__init__(self)
        self.request_obj = request
        self.group_name = ''
        self.group_description = 'IDM Portal Group'
        self.headers = {}
        self.payload = {}
        self.admin_token = ''
        self.get_req_params = {}
        self.set_admin_token()

    def set_admin_token(self):
        admin_sign = SignIn(self.request_obj)
        self.admin_token = admin_sign.get_admin_token()

    def get_headers(self):
        return self.headers

    def get_payload(self):
        return self.payload

    def get_common_headers(self):
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-version': '1',
            'Authorization': ('Bearer %s' % self.admin_token).encode("utf-8"),
        }

    def create_default_groups(self, mongodb):
        self.uri_path = 'authorize/identity/Group'
        grp_response = None
        for group in IDM_DEFAULT_GROUPS:
            self.headers = self.get_common_headers()
            try:
                self.payload = {
                    "name": group,
                    "managingOrganization": FernetCrypto.decrypt(IAM_CREDENTIALS['ORGANIZATION_ID']),
                    "description": self.group_description
                }
                grp_response = self.post(mongodb)
            except HTTPError as e:
                if e.response.status_code == 409:
                    grp_response = self.get_group_details(group)
                    continue
            finally:
                if self.status_code == 409:
                    grp_response = self.get_group_details(group)
                IAMGroupModel(mongodb).create_group(grp_response)

    def assign_members_to_group(self, user_uuid=None, groups=[], approved=True):
        for group_id in groups:
            self.headers = self.get_common_headers()
            self.uri_path = IAM_API_ENDPOINTS['ASSIGN_NUMBERS_TO_GROUP'].format(group_id=group_id)
            self.payload = {
                "resourceType": "Parameters",
                "parameter": [
                    {
                        "name": "UserIDCollection",
                        "references": [
                            {
                                "reference": user_uuid
                            }
                        ]
                    }
                ]
            }
            self.post(None)
    
    def get_group_details(self, group):
        self.headers = self.get_common_headers()
        self.get_req_params['name'] = quote(group)
        self.get()
        response = self.get_response()()
        all_groups = response.get('entry')
        if len(all_groups):
            temp = [ grp.get('resource') for grp in all_groups if grp.get('resource') and grp.get('resource').get('groupName') == group]
            resource = temp[0] if len(temp) else {}
        grp = {
            'id': resource.get('_id'),
            'name': resource.get('groupName'),
            'description': resource.get('groupDescription'),
            'managingOrganization': resource.get('orgId')
        }
        return grp

    def assign_role_to_group(self, mongodb, group_name, role_id):
        group_details = self.get_group_details(group_name)
        if group_details:
            group_id = group_details.get('id')
            self.uri_path = '/authorize/identity/Group/{group_id}/$assign-role'.format(group_id=group_id)
            self.headers = self.get_common_headers()
            try:
                self.payload = {
                    'roles': [role_id]
                }
                self.post(mongodb)
            except HTTPError as e:
                pass
            finally:
                IAMGroupModel(mongodb).assign_role_to_groups(role_id, group_id)

    def get_params(self):
        return self.get_req_params

        # def del_group(self, group_id):
        #     url = self.get_url()
        #     url = url+'/{group_id}'.format(group_id=group_id)
        #     print url
        #     t = self.request.delete(url,headers = self.get_common_headers())
        #     print t ,dir(t)

    def remove_members(self, mongodb):
        group_ids = self.request_obj.json.get('group_ids')
        self.headers = self.get_common_headers()
        for group_id in group_ids:
            self.uri_path = IAM_API_ENDPOINTS['REMOVE_MEMBERS_FROM_GROUP'].format(group_id=group_id)
            try:
                self.payload = {
                    "resourceType": "Parameters",
                    "parameter": [
                        {
                            "name": "UserIDCollection",
                            "references": [
                                {
                                    "reference": self.request_obj.json.get('uuid')
                                }
                            ]
                        }
                    ]
                }
                self.post(mongodb)
            except HTTPError as e:
                if e.response.status_code == 422:
                    grp_arr = []
                    grp_arr.append(group_id)
                    self.assign_members_to_group(self.request_obj.json.get('uuid'), grp_arr)
                    return {'status_code': 422}
        return {'status_code': self.status_code}
        
        
class Roles(IAMAccessor):
    url = IAM_API_ENDPOINTS['IDM_URL']
    uri_path = IAM_API_ENDPOINTS['ROLES']

    def __init__(self, request=None):
        IAMAccessor.__init__(self)
        self.request_obj = request
        self.payload = {}
        self.admin_token = ''
        self.get_req_params = {}
        self.set_admin_token()
        self.all_roles = {}
        self.get_all_roles()

    def set_admin_token(self):
        admin_sign = SignIn(self.request_obj)
        self.admin_token = admin_sign.get_admin_token()

    def get_headers(self):
        return self.headers

    def get_payload(self):
        return self.payload

    def get_common_headers(self):
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-version': '1',
            'Authorization': ('Bearer %s' % self.admin_token).encode("utf-8"),
        }

    def create_default_roles(self, mongodb):
        for group_name, role_name in IDM_DEFAULT_ROLES.items():
            self.create_iam_role(role_name, group_name, mongodb)

    def create_iam_role(self, role_name, group_name, mongodb):
        response = None
        self.headers = self.get_common_headers()
        try:
            self.payload = {
                'name': role_name,
                'description': 'IDM Roles',
                "managingOrganization": FernetCrypto.decrypt(IAM_CREDENTIALS['ORGANIZATION_ID']),
            }
            self.post(mongodb)
            response = self.get_response()
        except HTTPError as e:
            if e.response.status_code == 409:
                response = self.all_roles.get(role_name.lower(), None)
        finally:
            if self.status_code == 409:
                response = self.all_roles.get(role_name.lower(), None)
            Groups(self.request_obj).assign_role_to_group(mongodb, group_name, response.get('id'))
            IAMRoleModel(mongodb).save_roles(response)

    def get_all_roles(self):
        self.headers = self.get_common_headers()
        self.get()
        response = self.get_response()()
        resource = response.get('entry')
        for role in resource:
            r_name = role.get('name').lower()
            self.all_roles[r_name] = role
