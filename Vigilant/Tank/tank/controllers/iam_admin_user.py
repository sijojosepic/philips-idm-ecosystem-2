from bottle import request
from tank.models.iam_user import IAMUserModel
from tank.util import get_bottle_app_with_mongo
from tank.controllers.iam_user import Groups


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/', callback=get_all_users)
    app.post('/update_group', callback=update_user_group)
    return app


def get_all_users(mongodb):
    return IAMUserModel(mongodb).get_all_user_details()

def update_user_group(mongodb):
    user_uuid = request.json.get('uuid')
    group_ids = request.json.get('group_ids')
    approved = request.json.get('approved')
    Groups(request).assign_members_to_group(user_uuid, group_ids, approved)
    return IAMUserModel(mongodb).update_user_group(group_ids,user_uuid,approved)