from tank.util import get_bottle_app_with_mongo
from tank.models.groups import GroupsModel
from bottle import request


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/groups/<site_id>', callback=get_all_groups)
    app.delete('/delete/<site_id>/<group_name>',
               callback=delete_group)
    app.post('/addgroup', callback=add_to_group)
    return app


def get_all_groups(mongodb, site_id):
    return GroupsModel(mongodb, siteid=site_id).get_all_groups()

def delete_group(mongodb, site_id, group_name):
    return GroupsModel(mongodb, siteid=site_id)\
        .delete_group(group_name)


def add_to_group(mongodb):
    return GroupsModel(mongodb, siteid=request.json['siteid'])\
        .add_to_group(request.json['group_name'],
                      created_by=request.json['created_by'],
                      triplet=request.json['triplet'])
