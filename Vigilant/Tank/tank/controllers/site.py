from tank.models.site import SiteModel
from tank.util import get_bottle_app_with_mongo, get_list_from_param
from bottle import request

def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/details/siteids/<siteids>', callback=get_sites_details)
    app.get('/details/countries/<countries>', callback=get_sites_details)
    app.get('/details/facts', callback=get_sites_facts_details)
    app.get('/details', callback=get_sites_details)
    app.get('/siteids', callback=get_all_siteids)
    app.get('/details/candidates/subscription/<subscription_name>', callback=get_subscription_candidates)
    app.post('/encrypt', callback=custom_encrypt)
    app.post('/<siteid>', callback=update_site)
    app.delete('/<siteid>', callback=delete_site)
    app.get('/count', callback=get_site_count)
    app.get('/scanner/<siteid>', callback=scanner_info)
    app.get('/main/shinken_refs', callback=shinken_refs)
    app.get('/main/all_thresholds', callback=all_thresholds)
    app.put('/scanner/', callback=update_lhost)
    return app


def get_sites_details(mongodb, siteids=None, countries=None):
    siteids = get_list_from_param(siteids)
    countries = get_list_from_param(countries)
    return SiteModel(mongodb).get_sites(siteids, countries)


def get_all_siteids(mongodb):
    return SiteModel(mongodb).get_siteids()


def get_subscription_candidates(mongodb, subscription_name):
    return SiteModel(mongodb).get_subscription_candidates(subscription_name)


def get_sites_facts_details(mongodb):
    return SiteModel(mongodb).get_facts_hosts_details()


def update_site(mongodb, siteid):
    name = request.json['name']
    country = request.json['country']
    return SiteModel(mongodb).update_site(siteid=siteid, name=name, country=country)


def delete_site(mongodb, siteid):
    return SiteModel(mongodb).delete_site(siteid)


def get_site_count(mongodb):
    return SiteModel(mongodb).get_site_count()


def shinken_refs(mongodb):
    return SiteModel(mongodb).shinken_refs()


def all_thresholds(mongodb):
    return SiteModel(mongodb).all_thresholds()


def scanner_info(mongodb, siteid):
    return SiteModel(mongodb).scanner_info(siteid)


def update_lhost(mongodb):
    return SiteModel(mongodb).update_lhost(request.json)


def custom_encrypt(mongodb):
    return SiteModel(mongodb).custom_encrypt(request.json['given_string'])
