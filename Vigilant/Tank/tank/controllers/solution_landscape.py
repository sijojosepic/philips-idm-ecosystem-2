from bottle import request
from tank.models.solution_landscape import SolutionLandscapeModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/<site_id>', callback=get_solution_landscape_file)
    app.post('/', callback=save_solution_landscape_file)
    app.put('/', callback=update_solution_landscape_file)
    return app


def get_solution_landscape_file(mongodb, site_id):
    return SolutionLandscapeModel(mongodb, site_id)\
        .get_solution_landscape_file()


def save_solution_landscape_file(mongodb):
    return SolutionLandscapeModel(mongodb, request.forms.get('siteid'))\
        .save_solution_landscape_file(
        file=request.files.get('file', ''),
        modified_by=request.forms.get('modified_by', ''))

def update_solution_landscape_file(mongodb):
    return SolutionLandscapeModel(mongodb, request.forms.get('siteid'))\
        .update_solution_landscape_file(
        file=request.files.get('file', ''),
        modified_by=request.forms.get('modified_by', ''))
