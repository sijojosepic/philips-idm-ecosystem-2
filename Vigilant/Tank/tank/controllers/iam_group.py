from bottle import request
from tank.models.iam_group import IAMGroupModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/', callback=get_all_groups)
    return app


def get_all_groups(mongodb):
    return IAMGroupModel(mongodb).get_all_groups(request.query)
