from bottle import request
from tank.models.audit import AuditDistributionModel, AuditDeploymentModel, PackageDetails
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/distribution', callback=get_distribution_data)
    app.get('/deployment', callback=get_deployment_data)
    app.get('/package_details/<siteid>', callback=get_package_details)
    return app


def get_distribution_data(mongodb):
    return AuditDistributionModel(mongodb).get_data(request.query)


def get_deployment_data(mongodb):
    return AuditDeploymentModel(mongodb).get_data(request.query)


def get_package_details(mongodb, siteid):
    return PackageDetails(mongodb, siteid).get_data()
