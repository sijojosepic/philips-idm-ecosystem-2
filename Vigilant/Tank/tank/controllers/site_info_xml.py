from tank.models.site_info_xml import SiteInfoXmlModel, SiteInfoXmlKeys
from tank.util import get_bottle_app_with_mongo
from bottle import request


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/generate_xml', callback = get_site_info_xml)
    app.get('/keys', callback=get_keys)
    return app


def get_site_info_xml(mongodb):
    return SiteInfoXmlModel(mongodb, request.json['manifest']).create_xml()


def get_keys(mongodb):
    return SiteInfoXmlKeys(mongodb).get_keys()
