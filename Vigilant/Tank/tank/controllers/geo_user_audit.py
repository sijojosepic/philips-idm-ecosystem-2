from bottle import request
from tank.models.geo_user_audit import GeoUserAuditModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/rules_audit', callback=set_rules_audit)
    return app

def set_rules_audit(mongodb):
    return GeoUserAuditModel(mongodb, request.json, request.headers).set_rules_audit()