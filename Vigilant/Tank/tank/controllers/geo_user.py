from bottle import request
from tank.models.geo_user import GeoUserModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.post('/login', callback=validate_credentials)
    return app


def validate_credentials(mongodb):
    return GeoUserModel(mongodb, request.json).validate_credentials()
