from bottle import request
from tank.models.device import DeviceModel
from tank.util import get_bottle_app_with_mongo


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name,
                                    username=username, password=password,
                                    protocol=protocol)
    app.get('/', callback=get_device)
    app.post('/', callback=save_device)
    app.put('/', callback=update_device)
    app.delete('/', callback=delete_device)
    return app


def get_device(mongodb):
    return DeviceModel(mongodb).get_devices()


def save_device(mongodb):
    return DeviceModel(mongodb).save_device(request.json)


def update_device(mongodb):
    return DeviceModel(mongodb).update_device(request.json)


def delete_device(mongodb):
    return DeviceModel(mongodb).delete_device(request.query['ids'])
