from tank.models.scanner import ScannerModel
from tank.util import get_bottle_app_with_mongo
from bottle import request


def get_app(uri, db_name, username, password, protocol):
    app = get_bottle_app_with_mongo(uri=uri, db_name=db_name, username=username, password=password, protocol=protocol)
    app.get('/details', callback=get_scanner_details)
    return app


def get_scanner_details(mongodb):
    return ScannerModel(mongodb).get_scanner_details()
