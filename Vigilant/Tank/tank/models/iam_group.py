from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder, get_date_to_string_field_projection
from datetime import datetime


class IAMGroupModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['IAM_GROUP']]
        self.filter = {}

    def get_projection(self):
        return {
            '_id': 1,
            'description': 1,
            'managingOrganization': 1,
            'name': 1,
            'roles': 1,
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'restricted': 1
        }

    def create_group(self, group_details):
        """ {'id':'string, 'name':string, description:'string' ,managingOrgnization:string}"""
        identifier = group_details.pop('id')
        group_details['timestamp'] = datetime.now()
        group_details['roles'] = []
        self.collection.update_one({'_id': identifier}, {'$set': group_details}, upsert=True)
        return True

    def get_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()},
            {'$sort': {'timestamp': -1}}
        ])
        return {'result': list(matches)}

    def get_all_groups(self, query):
        self.filter = IAMGroupQuery(query).get_filter() or {}
        return self.get_matches()

    def get_group_by_id(self, id):
        self.filter['_id'] = id
        return self.get_matches()

    def assign_role_to_groups(self, role_id, group_id):
        group = self.get_group_by_id(group_id)
        if len(group.get('result')):
            group_info = group.get('result')[0]
            group_info.get('roles').append(role_id)
            self.collection.update_one({'_id': group_id}, {'$set': {'roles': list(set(group_info.get('roles')))}}
                                       , upsert=True)


class IAMGroupQuery(QueryBuilder):
    paramters = {
        'id': 'id'
    }

    def __init__(self, query):
        self.query = query

    def get_filter(self):
        pass