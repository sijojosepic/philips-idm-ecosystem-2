from tank.tnconfig import COLLECTIONS


class TechnicalIdModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['PRODUCT_TECHNICAL_IDS']]

    def get_all_technical_ids(self):
        matches = self.collection.find(
            {},
            {
                'product_id': '$product_id',
                'technical_id': '$technical_id',
                '_id': 0
            }
        )
        all_technical_ids = {}
        for match in list(matches):
            all_technical_ids[match['product_id']] = match['technical_id']
        return all_technical_ids

    def get_technical_id(self, product_id):
        matches = self.collection.find_one(
            {'product_id': product_id},
            {
                'technical_id': '$technical_id',
                '_id': 0
            }
        )
        return matches
