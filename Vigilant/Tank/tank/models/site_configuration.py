from tank.tnconfig import COLLECTIONS
from tank.util import  get_projection

class SiteConfigurationModel(object):

    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['SITE_CONFIGURATION']]

    def get_data(self, siteid):
        matches = self.collection.find({'_id': {
            '$regex':siteid
        }},
            get_projection(['hostname', 'ram_size','cpu_cores','gold_copy','module_type']))
        return {'result': list(matches)}
    
    def approve_config(self, site, nodes):
        for i in range(len(nodes)):
            self.collection.update_one({
                '_id':site + '-' + nodes[i]['hostname']
            },
            {'$set':{
                'gold_copy':{
                    'ram_size': nodes[i]['ram_size'],
                    'cpu_cores': nodes[i]['cpu_cores']
                }
            }})
        return {'result': True}
