from base64 import b64decode, b64encode
from Crypto.PublicKey import RSA
from tank.tnconfig import MODULUS, EXPONENT, D, P, Q

class RSAEncoding:
    def __init__(self):
        mod_raw = b64decode(MODULUS)
        exp_raw = b64decode(EXPONENT)
        d_raw = b64decode(D)
        p_raw = b64decode(P)
        q_raw = b64decode(Q)
        mod = long(mod_raw.encode('hex'), 16)
        exp = long(exp_raw.encode('hex'), 16)
        d = long(d_raw.encode('hex'), 16)
        p = long(p_raw.encode('hex'),16)
        q= long(q_raw.encode('hex'), 16)
        self.keyPriv = RSA.construct([mod, exp, d, p, q])
        self.keyPub = self.keyPriv.publickey()

    def rsa_encrypt(self, user_input):
        cyphertext = self.keyPub.encrypt(str(user_input), 0)
        encrypted_string = b64encode(cyphertext[0])
        return {'result': unicode(encrypted_string, errors = 'ignore')}

    def rsa_decrypt(self, user_cypher):
        val = b64decode(user_cypher)
        decrypt_given_string = self.keyPriv.decrypt(val)
        decrypted_string = unicode(decrypt_given_string, errors = 'ignore')
        return {'result': decrypted_string}
