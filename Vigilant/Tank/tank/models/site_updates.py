from tank.tnconfig import COLLECTIONS, ID_DOES_NOT_EXIST,\
    NOTES_MAXIMUM_LENGTH, NOTES_MAXIMUM_LENGTH_ERROR_CODE,\
    OK_STATUS_CODE
from datetime import datetime
from tank.util import get_date_to_string_field_projection
import time
import calendar


class SiteUpdatesModel(object):
    '''
    This class addresses recent activities for a particular site id.
    '''
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['SITEUPDATES']]

    def get_site_updates(self, siteid):
        result = self.collection.aggregate([
            {'$match': {'siteid': siteid}},
            {'$sort': {'created_on': -1}},
            {'$project': self.get_projection()}
        ])
        return {'result': list(result)}

    def save_site_updates(self, request):
        if len(request['message']) > NOTES_MAXIMUM_LENGTH:
            return {'code': NOTES_MAXIMUM_LENGTH_ERROR_CODE}
        else:
            self.collection.insert_one({
                '_id': request['siteid'] + '-' +
                str(calendar.timegm(time.gmtime())),
                'siteid': request['siteid'],
                'created_by': request['created_by'],
                'message': request['message'],
                'created_on': datetime.now()
            })
            return {'code': OK_STATUS_CODE}

    def update_site_updates(self, request):
        if (len(self.get_site_updates
                (request['siteid'])
                .get('result'))) == 0:
            return {'code': ID_DOES_NOT_EXIST}
        else:
            self.collection.update_one(
                {'_id': request['elementid']},
                {'$set': {'message': request['message'],
                          'created_by': request['created_by'],
                          'created_on': datetime.now()}})
            return {'code': OK_STATUS_CODE}

    def delete_site_updates(self, ids):
        id_list = ids.split(',')
        map(lambda id: self.collection.delete_one({'_id': id}), id_list)
        return {'result': True}

    def get_projection(self):
        return {
            'elementid': '$_id',
            '_id': 0,
            'siteid': '$siteid',
            'created_by': '$created_by',
            'message': '$message',
            'created_on': get_date_to_string_field_projection('$created_on')
        }
