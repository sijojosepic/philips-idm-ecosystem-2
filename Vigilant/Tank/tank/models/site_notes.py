from tank.tnconfig import COLLECTIONS, NOTES_MAXIMUM_LENGTH, NOTES_MAXIMUM_COUNT, NOTES_MAXIMUM_COUNT_ERROR_CODE, NOTES_MAXIMUM_LENGTH_ERROR_CODE, OK_STATUS_CODE
from datetime import datetime
from tank.util import get_date_to_string_field_projection, get_projection, QueryBuilder
import time, calendar


class NotesModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['SITE_NOTES']]
        self.filter = {}

    def get_notes(self, siteid, query):
        if query is not None:
            self.filter = SiteNotesQuery(query).get_filter()
        self.filter = {'siteid':siteid}
        result = self.collection.aggregate([
            {'$match': self.filter},
            {'$sort': {'created_on': -1}},
            {'$project': self.get_projection()}
        ])
        return {'result':list(result)}
    
    def save_notes(self, request):
        if len(self.get_notes(request['siteid'], self.filter).get('result'))>=NOTES_MAXIMUM_COUNT:
            return {'code':NOTES_MAXIMUM_COUNT_ERROR_CODE} 
        elif len(request['message'])>NOTES_MAXIMUM_LENGTH:
            return {'code': NOTES_MAXIMUM_LENGTH_ERROR_CODE}
        else:
            self.collection.insert_one({
                '_id': request['siteid']+'-'+str(calendar.timegm(time.gmtime())),
                'siteid': request['siteid'],
                'created_by': request['created_by'],
                'message':request['message'],
                'created_on': datetime.now()
            })
            return {'code': OK_STATUS_CODE}
            
    def delete_notes(self, ids):
        id_list = ids.split(',')
        map(lambda id: self.collection.delete_one({'_id': id}), id_list)
        return {'result': True}

    def get_projection(self):
        return {
                'elementid': '$_id',
                '_id':0,
                'siteid': '$siteid',
                'created_by':'$created_by',
                'message':'$message',
                'created_on': get_date_to_string_field_projection('$created_on')
        }


class SiteNotesQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'created_by': 'created_by',
    }
