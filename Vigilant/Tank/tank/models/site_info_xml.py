import logging
import json
import re
import requests
from lxml.etree import tostring
from lxml.builder import E
from tank.tnconfig import COLLECTIONS, SITE_INFO_MODULE_TYPES
from tank.controllers.fact import get_site_item_details

spit_bucket = logging.getLogger(__name__)


class SiteInfoXmlModel(object):
    def __init__(self,mongodb, data, **kwargs):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['FACTS']]
        self.data = data
        #Set encrypted as True
        self.encrypt_keys = ['DomainAdminPassword', 'ISiteServiceAccountPassword', 'DBPassword']

    def update_data(self):
        self.all_keys = SiteInfoXmlKeys(self.mongodb).get_keys()['result'][0]['keys'] if len(SiteInfoXmlKeys(self.mongodb).get_keys()['result']) != 0 else []
        for key in self.all_keys:
            self.data[key] = "NA"
        for host in self.data.get('hosts'):
            package_list = self.data['hosts'][host].get('packages')
            packages = []
            for package in package_list:
                package_with_version = package.split('-')
                package_name = re.split("[^a-zA-Z]*", package_with_version[0])[0]
                try:
                    package_name = package_name + '-' + package_with_version[1]
                except IndexError:
                    pass
                finally:
                    packages.append(package_name)
            self.data['hosts'][host]['packages'] = packages

    def get_facts(self):
        siteid = self.data['siteid']
        response = get_site_item_details(self.mongodb, siteid, 'ISP')
        return response.get('result', [])

    def check_for_tag(self, facts, hostname):
        try:
            for hosts in facts:
                host = hosts.get('hostname')
                if host == hostname:
                    tags = hosts.get('ISP', {}).get('tags')
                    if 'production' in tags:
                        return {'valid':True, 'value': 'production'}
                    elif 'test' in tags:
                        return {'valid':True, 'value': 'test'}
                    else:
                        return {'valid':False, 'value': None}
            return {'valid':False, 'value': None}
        except:
            return {'valid':False, 'value': None}

    def add_noncore_node_details(self):
        facts = self.get_facts()
        hostname = self.data['hosts'].keys()[0]
        tag = self.check_for_tag(facts, hostname)
        if tag['valid']:
            for host in facts:
                tags = host.get('ISP',{}).get('tags')
                if not tags or tag['value'] not in tags:
                    continue
                module_type = host.get('ISP',{}).get('module_type')
                if module_type in SITE_INFO_MODULE_TYPES.keys():
                    self.data[SITE_INFO_MODULE_TYPES[module_type]] = host['hostname']
        for req_module_type in SITE_INFO_MODULE_TYPES.values():
            if req_module_type not in self.data.keys():
                self.data[req_module_type] = 'NA'

    def add_site_keys(self, key_name, encrypted, text):
        xml_key = E.Key(KeyName=key_name, Encrypted=encrypted)
        xml_key.text = self.data.get(text, 'NA')
        return xml_key

    def create_xml(self):
        self.update_data()
        self.add_noncore_node_details()
        site_keys = E.Keys()
        if "site_vault" in self.data :
            for site_vault_key, site_vault_value in self.data["site_vault"].items() :
                xml_key = E.Key(KeyName=site_vault_key, Encrypted="true")
                xml_key.text = site_vault_value
                site_keys.append(xml_key)
        if "site_data" in self.data:
            for site_data_key, site_data_value in self.data["site_data"].items() :
                xml_key = E.Key(KeyName=site_data_key, Encrypted= "true" if site_data_key in self.encrypt_keys else "false")
                xml_key.text = site_data_value
                site_keys.append(xml_key)
        site_keys.append(self.add_site_keys('SiteID', 'false', 'siteid'))
        used_keys = ['ISiteServiceAccountPassword', 'DeploymentRootFolder', 'ISiteServiceAccountUser', 'DomainAdminPassword', 
            'LocalizationCode', 'DomainAdmin', 'Federated', 'SolutionRootFolder', 'DeploymentScripts', 'SiteID']
        remaining_keys = filter(lambda x: x not in used_keys, self.all_keys)
        for key in remaining_keys:
            site_keys.append(self.add_site_keys(key,'true' if(key in self.encrypt_keys) else 'false', key))

        roles = E.Roles()
        for host_key, host_value in self.data["hosts"].items() :

            ipv4 = E.IPv4()
            ipv4.text = host_value["ip"]
            fqdn = E.FQDN()
            fqdn.text = host_key

            packages = E.Packages()
            for package in host_value["packages"] :
                pcm_package_components = package.rsplit('-', 1)
                if len(pcm_package_components) > 1 :
                    packages.append(E.PackageInfo(Name=pcm_package_components[0], Version=pcm_package_components[1]))
                else :
                    packages.append(E.PackageInfo(Name=pcm_package_components[0], Version=""))

            host_keys = E.RoleBag()
            if "host_vault" in host_value :
                for host_vault_key, host_vault_value in host_value["host_vault"].items() :
                    xml_key = E.Key(KeyName=host_vault_key, Encrypted="true")
                    xml_key.text = host_vault_value
                    host_keys.append(xml_key)

            if "host_data" in host_value :
                for data_key, data_value in host_value["host_data"].items() :
                    keys = E.Keys()
                    xml_key = E.Key(KeyName=data_key, Encrypted="false")
                    xml_key.text = data_value
                    keys.append(xml_key)
                    host_keys.append(keys)
            roles.append(E.Role(ipv4, fqdn, packages, host_keys,Sequence=host_value['Sequence']))

        return {'result': tostring(E.SiteMap((E.SiteBag(site_keys)), roles), pretty_print=True, xml_declaration=True, encoding='UTF-8')}


class SiteInfoXmlKeys(object):
    def __init__(self,mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['DEPLOYMENT_KEYS']]

    def get_keys(self):
        return {'result':list(self.collection.find({}, { "_id": 0, "keys": 1 }))}
