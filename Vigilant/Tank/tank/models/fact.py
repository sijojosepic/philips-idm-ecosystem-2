import re

from tank.tnconfig import COLLECTIONS, PUBLIC_MODULES, PUBLIC_HOST_KEYS
from tank.util import get_logger, get_projection, get_keys_in_site
from tank.models.dashboard import DashboardModel
from tank.models.site_rules import RulesModel
from datetime import datetime

log = get_logger()


class SiteFactsModel(object):
    def __init__(self, mongodb, siteid):
        self.mongodb = mongodb
        self.siteid = siteid
        self.collection = mongodb[COLLECTIONS['FACTS']]

    def get_hosts(self):
        hosts = self.collection.distinct('hostname', filter={'siteid': self.siteid})
        return {'hosts': hosts}

    def get_host_detail(self, hostname):
        public_modules_keys = PUBLIC_MODULES.keys()
        match = self.collection.find_one(
            {'hostname': hostname, 'siteid': self.siteid},
            get_projection(['hostname'] + PUBLIC_HOST_KEYS + public_modules_keys)
        )
        return self.get_filtered_host_facts(match, PUBLIC_HOST_KEYS, public_modules_keys)

    def get_site_module_type(self):
        match = self.collection.aggregate(
        [
            {'$match': {'siteid': self.siteid}},
            {'$project': {'_id': 0, 'hostname': '$hostname', 'address': '$address',
                          'module_type': {'$ifNull': ['$ISP.module_type', {'$cond': { 'if': {'$or': [
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "MGNode"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "Database"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "HL7"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowArchiveServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowHDServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowArchiveHDServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4EV"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4Prep"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4Viewer"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4"]}
            ]},
            'then': {'$arrayElemAt': [
                "$endpoints.scanner", 0]},
            'else': {'$cond': {
                'if': {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4GD"]},
                    'then': {'$arrayElemAt': ["$role", 0]},
                'else': '$NULL'
            }}}}]},
            'modules': '$modules'}},
        ])
        return {'results': list(match)}

    def get_modules(self):
        site_facts = self.collection.find({'siteid': self.siteid}, get_projection(PUBLIC_MODULES.keys()))
        return {'modules': get_keys_in_site(site_facts)}

    def get_item_details(self, item):
        match = self.collection.find(
            {'siteid': self.siteid, item: {'$exists': True}},
            get_projection(['hostname', 'address', item])
        )
        return {'result': list(match)}

    def get_components(self):
        match = self.collection.find(
            {'siteid': self.siteid, 'Components': {'$exists': True}},
            get_projection(['hostname', 'Components'])
        )
        return {'results': list(match)}

    def get_host_components(self, hostname):
        match = self.collection.aggregate([
            {'$unwind': '$Components'},
            {'$match': {'_id': self.siteid + '-' + hostname, 'Components': {'$exists': True}}},
            {'$group': {'_id': {'name': '$Components.name', 'version': '$Components.version',
                                'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                                'date': '$Components.timestamp'}}}}},
            {'$group': {'_id': '$_id.name', 'version': {'$first': '$_id.version'},
                        'timestamp': {'$first': '$_id.timestamp'}}},
            {"$sort": {"timestamp": -1}},
            {'$project': {'_id': 0, 'name': '$_id', 'version': '$version', 'timestamp': '$timestamp'}}
        ])
        return {'results': list(match)}

    def get_site_key_details(self, item, value):
        try:
            # When val is integer
            value = int(value) if isinstance(int(value), int) else value
        except ValueError, e:
            pass
        match = self.collection.find(
            {'siteid': self.siteid, item: {'$exists': True}, item: value},
            get_projection(['hostname', 'Components.name', 'Components.version', item])
        )
        return {'result': list(match)}

    def get_one_component(self, component):
        match = self.collection.aggregate(
            [
                {'$match': {'siteid': self.siteid, 'Components.name': component}},
                {'$project': {'_id': 0, 'hostname': 1, 'Components': 1}},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': component}}
            ]
        )
        return {'results': list(match)}

    def get_filtered_host_facts(self, host_facts, keys, modules):
        result = {}
        if host_facts:
            result['hostname'] = host_facts['hostname']
            result.update([(key, host_facts[key]) for key in keys if key in host_facts])
            result['modules'] = [key for key in host_facts.keys() if key in modules]
        return result

    def get_node_details(self):
        match = self.collection.aggregate(
            [{'$match': {'$and': [{'siteid': self.siteid}, {'hostname': {'$not': re.compile('localhost')}}]}},
             {
                 '$project': {
                     '_id': 0,
                     'hostname': '$hostname',
                     'address': '$address',
                     'product_id': '$product_id',
                     'product_version': '$product_version',
                     'role': '$role',
                     'vCenter': '$vCenter',
                     'module_type': '$ISP.module_type',
                     'modules': '$modules',
                     'servicetag': '$servicetag',
                     'Windows': '$Windows',
                     'HPSwitch': '$HPSwitch',
                     'Components': '$Components',
                     'host_vendor': '$manufacturer',
                     'host_model': '$model'
                 }
             }
             ])
        nodes = list(match)
        hosts_down = DashboardModel(self.mongodb).get_nodes_down(self.siteid)
        vms = []
        vm_status = {'poweredOn': 'Up', 'poweredOff': 'Down', 'suspended': 'Down'}
        hostnames = []
        localhost_regex = re.compile('localhost')
        vce_vms = []
        vce_hosts = []
        nodes_with_maintenance_rules = RulesModel(self.mongodb).get_rules_with_siteid(self.siteid)
        for node in nodes:
            node.update({'discovery_type': 'Discovery'})
            if node['hostname'] in hosts_down:
                node['status'] = 'Down'
            else:
                node['status'] = 'Up'
                hostnames.append(node['hostname'])
                for maintenance_node in nodes_with_maintenance_rules.get('result', []):
                    if node['hostname'] == maintenance_node.get('nodename',False):
                        node['status'] = 'Maintenance'
                        node['start_time'] = maintenance_node['start_time']
                        node['end_time'] = maintenance_node['end_time']
            components = node.get('Components', [])
            for component in components:
                component['timestamp'] = (component.get('timestamp', None).strftime('%Y/%m/%d  %H:%M:%S')
                                          if component.get('timestamp', None) else None)
            if node.get('vCenter', {}).get('datacenter', None):
                datacenters = node['vCenter']['datacenter']
                node.pop('vCenter')
                for datacenter in datacenters.values():
                    for host_cluster_key, host_cluster_value in datacenter.iteritems():
                        if host_cluster_key == 'cluster' and len(host_cluster_value) > 0:
                            for domain in host_cluster_value.values():
                                hosts = domain.get('host')
                                if hosts is not None and len(hosts) > 0:
                                    for host in hosts.values():
                                        vcenter_vms = host.pop('vms', None)
                                        if vcenter_vms is not None:
                                            vce_vms.append(vcenter_vms)
                                        vce_hosts.append(host)
                        else:
                            if host_cluster_key == 'host':
                                for domain in host_cluster_value.values():
                                    vce_hosts.append(domain)
        if len(vce_hosts) > 0:
            for host in vce_hosts:
                for node in nodes:
                    if 'name' in host:
                        if host['name'] in node['hostname']:
                            node.update({'host_vendor': host.get('hardware', {}).get('vendor', None)})
                            node.update({'host_model': host.get('hardware', {}).get('model', None)})
                            node.update({'host_version': host.get('product', {}).get('version', None)})
        if len(vce_vms) > 0:
            for vce_vm in vce_vms:
                for vm in vce_vm:
                    vm_data = {}
                    if ('hostName' in vm['guest']) and (not localhost_regex.match(vm['guest']['hostName'])) and (
                            (vm['guest']['hostName']).lower() not in hostnames):
                        if ('ipAddress' in vm['guest']) and (vm['guest']['ipAddress'] in hostnames):
                            continue
                        vm_data.update({'status': vm_status[vm['powerState']]})
                        vm_data.update({'hostname': vm['guest'].get('hostName', None)})
                        vm_data.update({'address': vm['guest'].get('ipAddress', None)})
                        vm_data.update({'discovery_type': 'vCenter'})
                        vms.append(vm_data)
            nodes.extend(vms)

        return {'result': nodes}

    def get_facts_hosts(self):
        match = self.collection.aggregate([
            {'$group': {
                '_id': '$siteid',
                'result': {'$push': {
                    'hostname': '$hostname',
                    "vCenter": "$vCenter"
                }}
            }},
            {
                '$project': {
                    '_id': 1,
                    'result': 1
                }
            }
        ])
        return list(match)

    def ecosystem_verisons(self):
        match = self.collection.aggregate([
            {'$match': {'hostname': 'localhost', 'Components': {'$exists': True}}},
            {'$unwind': '$Components'},
            {"$sort": {"Components.timestamp": -1}},
            {'$project': {'siteid': '$siteid', 'version': '$Components.version', '_id': 0}}
        ])
        return {'result': list(match)}

    def get_host_count(self):
        return {'count': len(self.collection.distinct('_id', {'hostname': {'$nin': ['localhost']}}))}

    def get_version_by_product_id(self, product_id, module_key=None, module_val=None):
        match = self.collection.aggregate([
            {'$match': {'product_id': product_id, module_key: module_val, 'tags': 'production'}},
            {'$project': {'product_version': '$product_version', 'siteid': '$siteid'}}
        ])
        return {'result': list(match)}
        
    def get_facts(self, limit= 0):
        return { 'facts': list(self.collection.find({},{'Components.timestamp':0}).limit(limit))}
