from tank.util import get_date_to_string_field_projection as get_date_to_string
from tank.tnconfig import COLLECTIONS, ID_DOES_NOT_EXIST, \
    ID_ALREADY_EXIST, OK_STATUS_CODE, \
    UNAUTHORIZED_USER
from datetime import datetime

class GroupsModel(object):
    '''
    This class addresses groups/location of all nodes for a particular site id.
    '''

    def __init__(self, mongodb, siteid=None):
        self.mongodb = mongodb
        self.siteid = siteid

        self.groups_collection = mongodb[COLLECTIONS['GROUPS']]
        self.ip_status_collection = mongodb[COLLECTIONS['IP_STATUS']]

        self.default_group = 'All'

    def get_all_groups(self):
        groups = self.groups_collection.aggregate([
            {'$match': {'siteid': self.siteid}},
            {'$project': {
                '_id': 0,
                'siteid': '$siteid',
                'name': '$name',
                'triplet': '$triplet',
                'created_by': '$created_by',
                'created_on': get_date_to_string('$created_on')
            }}
        ])
        return {'result': list(groups)}

    def get_group_by_triplet(self, siteid, triplet):
        triplet_group_list = list(self.groups_collection.find({"triplet": {"$in": triplet},
                                                               'siteid': siteid}))
        triplet_group_dict = {}
        for group_data in triplet_group_list:
            group_triplets = group_data.get('triplet', [])
            for tpl in group_triplets:
                if tpl in triplet:
                    triplet_group_dict.update({tpl: group_data})
        return triplet_group_dict

    def get_triplets_by_group_name(self, group_name):
        triplets = self.groups_collection.find_one({'siteid': self.siteid,
                                                    'name': group_name},
                                                   {'_id': 0, 'triplet': 1})
        return triplets.get('triplet', []) if triplets else []

    def get_group_by_siteid(self, group_name):
        result = self.groups_collection.find_one({'name': group_name,
                                                  'siteid': self.siteid},
                                                 {'_id': 0})
        return result if result else {}

    def check_triplet_group_exists(self, group_info, group_name, created_by, triplet):
        if group_name == self.default_group:
            return False
        else:
            if group_info:
                group_triplets = group_info.get('triplet')
                group_created_by = group_info.get('created_by')
                return (not any(tpl for tpl in triplet if tpl not in group_triplets)) \
                       and (group_created_by == created_by)
            else:
                return False

    def create_group(self, group_name, created_by, triplet):
        self.groups_collection.update_one({'siteid': self.siteid, 'name': group_name},
                                          {'$set': {'name': group_name, 'triplet': triplet,
                                                    'created_by': created_by, 'created_on': datetime.now()}},
                                          upsert=True)
        self.update_triplet_group_name_dict(group_name=group_name,
                                            triplet=triplet)
        return {'result': OK_STATUS_CODE}

    def delete_group(self, group_name):
        group_data = self.groups_collection.find_one({'siteid': self.siteid, 'name': group_name, 'triplet': {'$exists': True, '$eq': []}})
        if group_data:
            self.groups_collection.delete_one({'name': group_name,
                                               'siteid': self.siteid})
            self.ip_status_collection.update_many(
                {'siteid': self.siteid, 'group_name': group_name},
                {'$set': {'group_name': self.default_group}})
            return {'result': OK_STATUS_CODE}
        else:
            return {'result': ID_DOES_NOT_EXIST}

    def add_to_group(self, group_name, created_by, triplet):
        '''
        This function add/update group of nodes for a site id.
        It performs following validations and actions :
        1.If triplet(s) belongs to any existing group then traverse through
            all those triplet-group information and if any of the triplet created by is
            diffrent than existing group created_by then response UNAUTHORIZED_USER will be sent.
        2.Check if provided group exists then get all triplet(s) from group and compare group triplet(s)
            with triplet(s) provided. if there are no unique triplets send response ID_ALREADY_EXIST which
            means those triplet already belong to that group otherwise add those triplet to group.
        3.If group not exists create a group with the data provided.
        4.If no existing triplet-group data found then create/update group with the data provided.
        :param group_name: group name to which triplet added.
        :param created_by: user name who's creating the group.
        :param triplet: list of ip's triplet(s) for respective nodes.
        :return: response code
        '''
        group_info = self.get_group_by_siteid(group_name)
        existing_group_triplets = group_info.get('triplet', [])
        existing_group_created_by = group_info.get('created_by')

        if self.check_triplet_group_exists(group_info, group_name, created_by, triplet):
            return {'result': ID_ALREADY_EXIST}

        if existing_group_created_by and existing_group_created_by != created_by and group_name != self.default_group:
            return {'result': UNAUTHORIZED_USER}

        triplet_group_data = self.get_group_by_triplet(siteid=self.siteid, triplet=triplet)
        if triplet_group_data:
            for _triplet, group_data in triplet_group_data.iteritems():
                triplet_created_by = group_data.get('created_by')
                triplet_group_name = group_data.get('name')
                triplet_list = [_triplet]
                if triplet_created_by != created_by and triplet_group_name != self.default_group:
                    return {'result': UNAUTHORIZED_USER}
                if group_info:
                    self.add_triplet_to_group(
                        group_name, created_by, triplet_list)
                    if group_name != triplet_group_name:
                        self.remove_triplet_from_group(
                            triplet_group_name, created_by, triplet_list)
                else:
                    self.create_group(group_name, created_by, triplet_list)
                    self.remove_triplet_from_group(
                        triplet_group_name, created_by, triplet_list)
            additional_triplets = [tpl for tpl in triplet if
                                   tpl not in triplet_group_data.keys()]
            if additional_triplets:
                self.add_triplet_to_group(
                    group_name, created_by, additional_triplets)
            return {'result': OK_STATUS_CODE}
        else:
            if group_info:
                existing_group_triplets.extend(triplet)
                self.add_triplet_to_group(group_name,
                                          created_by, existing_group_triplets)
                return {'result': OK_STATUS_CODE}
            else:
                self.create_group(group_name, created_by, triplet)
                self.update_triplet_group_name_dict(
                    group_name=group_name, triplet=triplet)
                return {'result': OK_STATUS_CODE}

    def remove_triplet_from_group(self, triplet_group_name,
                                  created_by, triplet):
        group_triplets = self.get_triplets_by_group_name(triplet_group_name)
        for tpl in triplet:
            if tpl in group_triplets:
                group_triplets.remove(tpl)
        self.update_group_and_triplet(created_by,
                                      group_triplets, triplet_group_name)

    def add_triplet_to_group(self, group_name, created_by, triplet):
        group_triplets = self.get_triplets_by_group_name(group_name)
        for tpl in triplet:
            if tpl not in group_triplets:
                group_triplets.append(tpl)
        self.update_group_and_triplet(created_by,
                                      group_triplets, group_name)

    def update_group_and_triplet(self, created_by,
                                 group_triplets, triplet_group_name):
        self.groups_collection.update_one(
            {'siteid': self.siteid,
             'name': triplet_group_name, 'created_by': created_by},
            {"$set": {'triplet': group_triplets,
                      'created_on': datetime.now()}})
        self.update_triplet_group_name_dict(group_name=triplet_group_name,
                                            triplet=group_triplets)

    def update_triplet_group_name_dict(self, group_name=None, triplet=[]):
        self.ip_status_collection.update_many({'siteid': self.siteid, 'triplet': {'$in': triplet}},
                                              {"$set": {'group_name': group_name}})
