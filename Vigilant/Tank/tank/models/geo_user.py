import hashlib, jwt, json
from tank.tnconfig import COLLECTIONS, TOKEN_TIMEOUT, TOKEN_SECRET
from datetime import datetime
import time


class GeoUserModel(object):
    def __init__(self, mongodb, request):
        self.mongodb = mongodb
        self.request = request
        self.collection = mongodb[COLLECTIONS['USER']]
        self.status = {'valid': False}

    def get_password(self):
        password = list(self.collection.find({'_id': self.request['loginId']},
                                        {"_id":0,"password":1}))
        return password

    def validate_credentials(self):
        password = self.get_password()
        if password:
            m = hashlib.md5()
            m.update(self.request['password'].encode('utf-8'))
            if m.hexdigest() == password[0]['password']:
                self.status['valid'] = True
                self.status['tokenid'] = self.get_token()
                return self.status
        return self.status

    def get_token(self):
        key = self.request['loginId']
        json_str = {
            'key': key,
            'timestamp': time.time(),
            'exp': time.time() + TOKEN_TIMEOUT
        }
        token = jwt.encode(json_str, TOKEN_SECRET,
                           algorithm='HS256').decode('utf-8')
        return token



