from tank.tnconfig import COLLECTIONS, OK_STATUS_CODE, \
    ID_ALREADY_EXIST, ID_DOES_NOT_EXIST


class DescriptionModel(object):
    '''
    This class addresses About site summary details for a particular site id.
    '''
    def __init__(self, mongodb, siteid=None):
        self.mongodb = mongodb
        self.siteid = siteid
        self.collection = mongodb[COLLECTIONS['DESCRIPTION_DETAILS']]

    def get_details(self):
        desc_details = self.collection.aggregate([
            {'$match': {'siteid': self.siteid}},
            {'$project': {
                '_id': 0,
                'siteid': '$siteid',
                'description': '$description',
                'role': '$role',
                'market_leader': '$market_leader',
                'csm': '$csm',
                'address': '$address',
                'poc': '$poc',
                'number': '$number'
            }}
        ])
        return {'result': list(desc_details)}

    def save_details(self, request_json):
        desc_details_len = len(self.get_details().get('result'))
        if desc_details_len != 0:
            return {'code': ID_ALREADY_EXIST}
        else:
            self.collection.update_one(
                {'siteid': self.siteid},
                {'$set': {
                    'description': request_json['description'],
                    'role': request_json['role'],
                    'market_leader': request_json['market_leader'],
                    'csm': request_json['csm'],
                    'address': request_json['address'],
                    'poc': request_json['poc'],
                    'number': request_json['number']
                }}, upsert=True)
            return {'code': OK_STATUS_CODE}

    def update_details(self, request_json):
        desc_details_len = len(self.get_details().get('result'))
        if desc_details_len == 0:
            return {'code': ID_DOES_NOT_EXIST}
        else:
            self.collection.update_one(
                {'siteid': self.siteid},
                {'$set': {
                    'description': request_json['description'],
                    'role': request_json['role'],
                    'market_leader': request_json['market_leader'],
                    'csm': request_json['csm'],
                    'address': request_json['address'],
                    'poc': request_json['poc'],
                    'number': request_json['number']
                }})
            return {'code': OK_STATUS_CODE}
