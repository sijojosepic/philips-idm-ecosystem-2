from tank.tnconfig import COLLECTIONS
from datetime import datetime
from tank.util import RedisInstance, get_logger, get_date_to_string_field_projection, get_projection, QueryBuilder
import json

log = get_logger()


class RulesModel(object):
    def __init__(self, mongodb):
        self.collection = mongodb[COLLECTIONS['SITE_MAINTENANCE_RULES']]
        self.hash_set = 'SITE_MAINTENANCE_RULES'
        self.rds = RedisInstance.get_redis_instance()
        self.maintenance_type = 'maintenance'

    def save_rules(self, request):
        start_time = datetime.strptime(request['start_time'], "%Y-%m-%d %H:%M:%S")
        end_time = datetime.strptime(request['end_time'], "%Y-%m-%d %H:%M:%S")

        def update_site():
            id = request['siteid']
            db_update(id)

        def update_node(node_name):
            id = '{siteid}_{node_name}'.format(siteid=request['siteid'], node_name=node_name)
            db_update(id, node_name)

        def db_update(id, nodename = ''):     
            data  = {
                "_id": id,
                "type": self.maintenance_type,
                "siteid": request['siteid'],
                "nodename": nodename,
                "user": request['user'],
                "start_time": start_time,
                "end_time": end_time,
                "remark": request['maintenance_remark']
            }
            self.collection.update({"type": self.maintenance_type, "_id": id}, data, upsert=True)

        map(update_node, request['nodes']) if len(request['nodes']) > 0 else update_site()
        self.update_rules_in_cache()
        return {'valid': True}

    def get_rules_with_siteid(self, siteid):
        site_ids = self.collection.aggregate([
            {'$match': {'siteid': siteid, 'type': self.maintenance_type}},
            {'$project': {
                '_id': 0,
                'siteid': '$siteid',
                'nodename': '$nodename',
                'user': '$user',
                'start_time': get_date_to_string_field_projection('$start_time'),
                'end_time': get_date_to_string_field_projection('$end_time'),
                'remark': '$remark'
            }}
        ])
        return {'result': list(site_ids)}
    
    def is_site_under_maintenance(self, siteid):
        return {'result': self.collection.count({'_id': siteid, 'type': self.maintenance_type}) > 0}
    
    def is_node_under_maintenance(self, siteid, nodename):
        return {'result': self.collection.count({'_id': '{siteid}_{nodename}'.format(siteid=siteid, nodename=nodename), 'type': self.maintenance_type})> 0}
        
    def get_node_maintenance_schedule(self, siteid, nodename):
        nodes = self.collection.aggregate([
            {'$match': {'_id': '{siteid}_{nodename}'.format(siteid=siteid, nodename=nodename), 'type': self.maintenance_type}},
            {'$project': 
                {
                '_id': 0,
                'siteid': '$siteid',
                'nodename': '$nodename',
                'start_time': get_date_to_string_field_projection('$start_time'),
                'end_time': get_date_to_string_field_projection('$end_time'),
                }
            }
            ])
        return {'result': list(nodes)}

    def get_rule_list(self):
        return {'sites': list(self.collection.distinct('siteid', {'type': self.maintenance_type}))}

    def get_all_sites_under_maintenance(self):
        return {'rules': list(self.collection.find({'type': self.maintenance_type}))}

    def update_rules_in_cache(self):
        rules = self.get_all_sites_under_maintenance()
        try:
            self.rds.delete(self.hash_set)
            for rule in rules['rules']:
                self.set_cache(rule)

        except Exception as e:
            log.error("Redis update failed because of ", e.message)
            return True

    def get_hash_val(self, rule):
        return json.dumps({'start_time': datetime.strftime(rule.get('start_time'),'%Y-%m-%d %H:%M:%S.%f'),
                'end_time': datetime.strftime(rule.get('end_time'),'%Y-%m-%d %H:%M:%S.%f')})

    def set_cache(self, rule):
        self.rds.hset(self.hash_set, rule['_id'], self.get_hash_val(rule))

    def get_rules(self, query):
        self.filter = SiteRulesQuery(query).get_filter()
        result = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()}
        ])
        return {'result':list(result)}

    def save_assignment_rule(self, request):
        if self.block_rule_update(request['siteid'], request['type']):
            self.collection.insert_one({
                'siteid': request['siteid'],
                'user': request['user'],
                'type':request['type'],
                'start_time': datetime.now()
            })
            return {'valid': True}
        else:
            return {'valid': False}

    def block_rule_update(self, siteid, type):
        return True if len(list(self.collection.find({'siteid': siteid, 'type': type}))) == 0 else False

    def delete_site_assignment_rule(self, siteid):
        self.collection.delete_one({'siteid':siteid, 'type':'assignment'})
        self.update_rules_in_cache()

    def delete_site_maintenance_rule(self, siteid):
        self.collection.delete_one({'_id':siteid, 'type': self.maintenance_type})
        self.update_rules_in_cache()

    def delete_node_maintenance_rule(self, siteid, nodename):
        self.collection.delete_one({'_id':'{siteid}_{nodename}'.format(siteid=siteid, nodename=nodename), 'type':self.maintenance_type})
        self.update_rules_in_cache()

    def get_projection(self):
        return {
                '_id': 0,
                'siteid': '$siteid',
                'user':'$user',
                'type':'$type',
                'maintenance_remark':'$remark',
                'nodename':'$nodename',
                'start_time': get_date_to_string_field_projection('$start_time'),
                'end_time': get_date_to_string_field_projection('$end_time')
        }


class SiteRulesQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'type': 'type',
    }
