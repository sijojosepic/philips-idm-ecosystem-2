from tank.tnconfig import COLLECTIONS, OK_STATUS_CODE, \
    ID_ALREADY_EXIST
import calendar
import time

class DeviceModel(object):
    '''
    This class addresses devices in racks for a particular site id.
    '''
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['DEVICES']]

    def get_devices(self):
        device_list = list(self.collection.aggregate([{"$sort": {"_id": -1}}]))
        return {'results': device_list}

    def get_device_by_details(self, device_name, device_type):
        filtered_device_list = self.collection.find({'name': device_name,
                                                     'type': device_type})
        return list(filtered_device_list)

    def save_device(self, request):
        if len(self.get_device_by_details(request['name'],
                                            request['type'])) > 0:
            return {'code': ID_ALREADY_EXIST}
        else:
            self.collection.insert_one({
                'name': request['name'],
                'type': request['type'],
                '_id':  str(calendar.timegm(time.gmtime()))
            })
        return {'code': OK_STATUS_CODE}

    def update_device(self, request):
        self.collection.update_one(
            {'_id': request['_id']},
            {'$set': {
                'name': request['name'],
                'type': request['type']
            }})
        return {'code': OK_STATUS_CODE}

    def delete_device(self, ids):
        id_list = ids.split(',')
        map(lambda id: self.collection.delete_one({'_id': id}), id_list)
        return {'result': OK_STATUS_CODE}
