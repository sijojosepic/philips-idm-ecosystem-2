from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder
from tank.util import get_date_to_string_field_projection
from tank.models.subscription import SubscriptionModel
from tank.models.product import ProductModel


class AuditQuery(QueryBuilder):
    audit_type = None

    def process_query(self):
        site_id = self.query.pop('site_id', None)
        if site_id:
            self.query['site_id'] = {'$in': site_id.split(',')}
        start_date = self.query.pop('start_date', None)
        end_date = self.query.pop('end_date', None)
        self.query['timestamp'] = self.get_between_dates_filter(start_date, end_date)
        self.query['type'] = self.audit_type


class AuditDeploymentQuery(AuditQuery):
    audit_type = 'deployment'

    parameters = {
        'site_id': 'siteid',
        'package': 'package',
        'timestamp': 'timestamp',
        'type': 'type',
    }


class AuditDistributionQuery(AuditQuery):
    audit_type = 'distribution'

    parameters = {
        'site_id': 'siteid',
        'subscription': 'subscription',
        'timestamp': 'timestamp',
        'type': 'type',
    }


class AuditModel(object):
    query_builder = None

    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['AUDIT']]
        self.filter = {}

    def get_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()},
            {'$sort': {'timestamp': -1}}
        ])
        return {'result': list(matches)}

    def get_projection(self):
        pass

    def get_data(self, query):
        self.filter = self.query_builder(query).get_filter()
        return self.get_matches()


class AuditDistributionModel(AuditModel):
    query_builder = AuditDistributionQuery

    def get_projection(self):
        return {
            '_id': 0,
            'subscription': '$subscription',
            'status': '$status',
            'user': '$user',
            'siteid': '$siteid',
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'version': '$version',
            'size': '$size'
        }


class AuditDeploymentModel(AuditModel):
    query_builder = AuditDeploymentQuery

    def get_projection(self):
        return {
            '_id': 0,
            'package': '$package',
            'status': '$status',
            'hosts': '$hosts',
            'user': '$user',
            'siteid': '$siteid',
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'version': '$version',
            'deployment_id': '$deployment_id'
        }

class PackageAuditData(AuditModel):

    def get_package_data(self, siteid):
        self.filter = {'siteid': siteid, 'status': 'Available', 'subscription_id': {'$exists': True}}
        return self.get_matches()

    def get_deleted_data(self, siteid, subscription_id):
        self.filter = {'siteid': siteid, 'subscription_id': subscription_id, 'status': 'Deleted'}
        return self.get_matches()

    def get_projection(self):
        return {
        '_id': 0,
        'status': '$status',
        'version': '$version',
        'siteid': '$siteid',
        'user': '$user',
        'timestamp': get_date_to_string_field_projection('$timestamp'),
        'subscription_id':'$subscription_id',
        'name': '$subscription',
        'type': '$type',
        'size': '$size'
        }


class PackageDetails(object):
    '''
    This API returns the data in the following format:
    {
        'package_data': [{
            'product_name': '<product_1_name>',
            'package_list': [{Package_1_Data},{Package_2_Data}, {Package_3_Data}]
        }, {
            'product_name': '<product_2_name>',
            'package_list': [{Package_Data}]
        }]
    }

    format of Package_Data:
        {
        'status': '<status>',
        'name': '<Package Name>',
        'timestamp': '<Time stamp>',
        'version': '<version info>',
        'siteid': '<siteid>',
        'user': '<user>',
        'subscription_id': '<subscription id>',
        'size': '<size>',
        'type': '<type>',
        'package_file': '<package file name>'
        }
    '''
    def __init__(self, mongodb, siteid):
        self.mongodb = mongodb
        self.siteid = siteid
        self.package_data_dict = {}
        self.__audit_data = None

    @property
    def audit_data(self):
        if self.__audit_data is None:
            self.__audit_data = PackageAuditData(self.mongodb).get_package_data(self.siteid).get('result')
        return self.__audit_data

    def _subscription_data(self, subscription_id):
        data = SubscriptionModel(self.mongodb).get_subscription_details(subscription_id).get('result')
        return data[0] if data else []

    def _validate_data(self, audit, subscription_data):
        if not (subscription_data and subscription_data.get('package_file') and subscription_data.get(
                'product_name') and self._is_not_deleted_data(audit)):
            return False
        return True

    def _is_not_deleted_data(self, audit):
        return False if PackageAuditData(self.mongodb).get_deleted_data(self.siteid, audit.get('subscription_id')).get('result') else True

    def _product_value(self, product_dict):
        return product_dict.get('product_value')

    def _get_product_value(self, product_name):
        data = ProductModel(self.mongodb).get_product_value(product_name).get('result')
        return self._product_value(data[0]) if data else None

    def __update_package_data(self, audit, product_name, package_list):
        if package_list:
            package_list.append(audit)
            self.package_data_dict[product_name] = package_list
        else:
            self.package_data_dict[product_name] = [audit]

    def __get_final_data(self):
        return {
            'package_data': [
                {
                    'product_name': key, 'package_list': value, 'product_value': self._get_product_value(key)
                }
                for key, value in self.package_data_dict.iteritems()
            ]
        }

    def get_data(self):
        for audit in self.audit_data:   
            subscription_data = self._subscription_data(audit.get('subscription_id'))
            if self._validate_data(audit, subscription_data):
                # Adding a package file to audit data.
                audit['package_file'] = subscription_data.get('package_file')
                # Building package data for a product name with its package file details. 
                # Ex: {'ISP': [{audit1},{audit2}]}
                subscription_product_name = subscription_data.get('product_name')
                package_list = self.package_data_dict.get(subscription_product_name)
                self.__update_package_data(audit, subscription_product_name, package_list)
        return self.__get_final_data()
