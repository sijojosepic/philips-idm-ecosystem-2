from tank.models.site import SiteModel
from tank.tnconfig import COLLECTIONS
from tank.util import get_logger, is_version_match, QueryBuilder
from tank.util import get_date_to_string_field_projection

log = get_logger()


class FieldModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['FACTS']]
        self.filter = {}
        self.mod_types_ignored = [3]

    def get_ual_component_version(self, component, version):
        site_hosts_with_component = self.collection.aggregate(
            [
                {'$match': {'Components.name': component}},
                {'$project': {'_id': 0, 'siteid': 1, 'hostname': 1, 'Components': 1}},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': component}},
                {'$group': {
                    '_id': '$siteid',
                    'hosts': {'$push': {'hostname': '$hostname', 'version': '$Components.version'}}
                }},
                {'$project': {'_id': 0, 'siteid': '$_id', 'hosts': 1}}
            ]
        )
        return self.get_component_ual(site_hosts_with_component, version)

    def get_component_details(self, query=None):
        self.filter = FieldQuery(query).get_filter()
        if self.filter.get('siteid'):
            self.filter['siteid'] = {'$in': self.filter.get('siteid').split(",")}
        if self.filter.get('region') and not self.filter.get('siteid'):
            sites = SiteModel(self.mongodb).get_sites(countries=[self.filter.get('region')])
            self.filter['siteid'] = {'$in': [site.get('siteid') for site in sites.get('sites')]}
            self.filter.pop('region')

        if self.filter.get('productid', None):
             self.filter['$or'] = [{'Components.name': {'$in': self.filter.get('productid').split(",")}},
                                  {'product_name': {'$in': self.filter.pop('productid').split(",")}}]
        if self.filter.get('tags'):
            self.filter['tags'] = {'$in': self.filter.get('tags').split(",")}
        module_type_mapping = {'infra':128, 'p0n':2, 'eid':3, 'ssi':270}
        component_name_mapping = {'ispacs':'IntelliSpace PACS'}
        match = self.collection.aggregate(
            [
                {'$project': {'_id': 0, 'siteid': 1, 'Components': 1, 'module_type':'$ISP.module_type', 'tags':1, 'Windows.tags': 1, 'role': 1}},
                {'$unwind': '$Components'},
                {'$redact':
                    {'$cond':
                            [{'$and':
                                    [{'$and':[
                                                {'$ne':['$module_type', module_type_mapping.get('infra')]},
                                                {'$ne':['$module_type', module_type_mapping.get('p0n')]}, 
                                                {'$ne':['$module_type', module_type_mapping.get('eid')]}, 
                                                {'$ne':['$module_type', module_type_mapping.get('ssi')]}
                                                ] 
                                        }, 
                                        {'$eq':['$Components.name', component_name_mapping.get('ispacs')]}
                                    ]
                            },
                            '$$PRUNE',
                            '$$KEEP'
                            ]
                    }
                },
                {'$unwind': {'path': '$tags', 'preserveNullAndEmptyArrays': True}},
                {'$sort': {'Components.timestamp': -1}},
                {'$match': self.filter},
                {'$group': {'_id': {'siteid':'$siteid', 'tag':'$tags'}, 'Components': {
                    '$push': {'name': '$Components.name', 'version': '$Components.version',
                              'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                              'date': '$Components.timestamp'}},
                              'tag': '$Windows.tags', 'role': '$role'
                              }}}},
                {'$project': {'_id': 0, 'siteid': '$_id.siteid', 'tag': '$_id.tag', 'Components': 1}}
            ]
        )
        results = list(match)
        return {'result': results}

    def get_component_ual(self, site_hosts_with_component, version):
        result = []
        for site in site_hosts_with_component:
            match_hosts = filter(lambda x: is_version_match(version, x['version']), site['hosts'])
            if match_hosts:
                result.append(dict(siteid=site['siteid'], hosts=match_hosts))
        return result

    def get_unique_components(self):
        matches = self.collection.distinct('Components.name')
        return {"components": matches}

    def get_unique_models(self):
        matches = self.collection.distinct('product_id')
        return {"result": sorted(matches)}

    def get_unique_product_names(self):
        return {'result': sorted(self.collection.distinct('product_name'))}

    def get_unique_tags(self):
        return {'result': sorted(self.collection.distinct('tags'))}

class FieldQuery(QueryBuilder):
    parameters = {
        'siteid': 'siteid',
        'productid': 'productid',
        'region': 'region',
        'tags': 'tags'
    }