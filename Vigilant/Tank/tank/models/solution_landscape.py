from tank.tnconfig import COLLECTIONS, ID_ALREADY_EXIST, \
    OK_STATUS_CODE, ID_DOES_NOT_EXIST, FILE_NOT_FOUND
from gridfs import GridFS
from gridfs.errors import FileExists
from bottle import response


class SolutionLandscapeModel(object):
    '''
    This class addresses Solution Landscape file for a particular site id.
    We are using GridFS to store file in MongoDB.
    '''
    file_data_dict = {}

    def __init__(self, mongodb, siteid=None):
        self.mongodb = mongodb
        self.siteid = siteid

        self.collection = mongodb[COLLECTIONS['SOLUTION_LANDSCAPE']]
        self.default_namespace = "sl"
        self.fs = GridFS(self.mongodb, self.default_namespace)

    def get_solution_landscape_file(self):
        result = self.collection.find_one({'siteid': self.siteid}, {'_id': 0})
        if result:
            file_name = result.get('file')
            db_file = self.fs.get(file_name)
            file_content = db_file.read()
            return file_content.encode("base64")
        else:
            response.status = FILE_NOT_FOUND
            return response

    def save_file(self, file, file_name, modified_by):
        '''
        This function saves file in MongoDB using GridFS and reference of the file in solution_landscape collection.
        It gets file data and put in db using GridFS and update file referecence in solution landscape collection.
        :param file: File Object
        :param modified_by: User String updating/inserting file in the db.
        :return:
        '''
        file_data = self.get_file_data(file)
        sl_file = self.save_file_in_db(file_data, file_name)
        self.collection.update_one({'siteid': self.siteid},
                                   {'$set': {'file': sl_file,
                                             'modified_by': modified_by}},
                                   upsert=True)

    def update_file(self, file, old_file_name, modified_by):
        # delete old file from GridFS
        self.fs.delete(old_file_name)

        new_file_name = file.filename

        file_data = self.get_file_data(file)

        #put file in DB using GridFS and if there is existing reference of the file in DB then enforcing
        #deletion of the file then saving in DB.
        try:
            sl_file = self.save_file_in_db(file_data, new_file_name)
        except FileExists:
            self.fs.delete(new_file_name)
            sl_file = self.save_file_in_db(file_data, new_file_name)

        self.collection.update_one({'siteid': self.siteid, 'file': old_file_name},
                                   {'$set': {'file': sl_file,
                                             'modified_by': modified_by}})

    def save_file_in_db(self, file_data, file_name):
        sl_file = self.fs.put(file_data, content_type='application/pdf',
                              filename=file_name,
                              _id=self.siteid + '-' + file_name)
        return sl_file

    def get_file_data(self, file):
        filename = file.filename
        cached_file_data = self.file_data_dict.\
            get(self.siteid + '-' + filename)
        if cached_file_data:
            return cached_file_data
        else:
            raw = ''
            while True:
                datachunk = file.file.read()
                if not datachunk:
                    break
                raw = raw + datachunk
            SolutionLandscapeModel.file_data_dict.\
                update({self.siteid + '-' + filename: raw})
            return raw

    def save_solution_landscape_file(self, file, modified_by):
        try:
            self.save_file(file, file.filename, modified_by)
            return {'result': OK_STATUS_CODE}
        except FileExists:
            return {'result': ID_ALREADY_EXIST}

    def update_solution_landscape_file(self, file, modified_by):
        result = self.collection.find_one({'siteid': self.siteid}, {'_id': 0})
        if result:
            self.update_file(file, result.get('file'), modified_by)
            return {'result': OK_STATUS_CODE}
        else:
            return {'result': ID_DOES_NOT_EXIST}
