from tank.tnconfig import COLLECTIONS
from datetime import datetime


class IAMUserModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['IAM_USER']]
        self.status = {'valid': False}

    def get_all_user_details(self):
        details = self.collection.aggregate([
            {'$match': {}},
            {'$project': {"_id":0,"givenName":1,"familyName":1,"groupID":1,"userUUID":1,"loginId":1,"approved":1}}
        ])
        return {'result': list(details)}

    def get_user_details(self, loginId):
        return list(self.collection.find({'loginId': loginId},{"_id":0,"givenName":1,"familyName":1,"groupID": 1,"approved": 1}))

    def add_new_user(self, uuid, request_obj):
        self.collection.update_one({'userUUID': uuid}, {'$set': {'loginId': request_obj.get('email'),
                                    'givenName': request_obj.get('firstname'), 'familyName': request_obj.get('lastname'),
                                    'timestamp': datetime.now(), 'groupID': request_obj.get('groups'), 'approved': request_obj.get('approved')}}, upsert=True)

    def update_user_group(self, group_ids, uuid, approved):
        update_status = self.collection.update_one({'userUUID': uuid}, {'$set': {'groupID': group_ids, 'approved': approved}}, upsert=False)

        if update_status.matched_count == 1:
            self.status['valid'] = True

        return self.status
        