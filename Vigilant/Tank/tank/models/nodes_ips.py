from tank.tnconfig import COLLECTIONS, OK_STATUS_CODE, ID_ALREADY_EXIST, \
    MEMORY_VALUES_DICT
from collections import defaultdict
import re
from operator import itemgetter
from tank.util import normalized_value
from groups import GroupsModel


class NodesIPInventoryModel(object):
    '''
    This class addresses node(s) and ip(s) for a particular site id.
    We are using facts collection to fetch nodes information.
    This class methods provides data for Site Config and IP Inventory module.
    '''
    cached_nodes = {}

    def __init__(self, mongodb, siteid=None):
        self.mongodb = mongodb
        self.siteid = siteid

        self.facts_collection = mongodb[COLLECTIONS['FACTS']]
        self.ip_status_collection = mongodb[COLLECTIONS['IP_STATUS']]
        self.groups_collection = mongodb[COLLECTIONS['GROUPS']]

        self.physical_nodes, self.vms_data, self.untagged_nodes = [], [], []
        self.virtual_node_key_list = ['Windows']
        self.virtual_node_dict = {}
        self.available_status = 'available'
        self.reserved_status = 'reserved'
        self.default_value = 'NA'
        self.untagged_nodes_str = 'Untagged Nodes'
        self.physical_node_keys = ['nodename', 'productid', 'ipaddress',
                                   'version', 'location', 'vendor', 'type',
                                   'model', 'virtualnodes']
        self.virtual_node_keys = ['vnodename', 'vproductid', 'vipaddress',
                                  'vversion', 'type', 'operatingsystem',
                                  'cores', 'ram', 'diskdata']
        self.consumed_ip_data_keys = ['hostname', 'ip', 'group_name']
        self.disk_data_keys = ['Name', 'Total', 'Used']
        self.ip_tree_keys = ['ip', 'children', 'count']
        self.ip_status_keys = ['status', 'message', 'siteid', 'ip', 'modified_by', 'group_name']
        self.groups = GroupsModel(siteid=siteid, mongodb=mongodb)

    def update_data_dict_list(self, res_list, keys, data):
        data_dict = dict(zip(keys, data))
        if data_dict not in res_list:
            res_list.append(data_dict)

    def get_available_site_ips(self, refresh="False"):
        '''
        This function generates available ips with following steps:
        1.get all ip(s) from all node(s) for a site id.
        2.get all reserve ip(s)
        3.Traverse through all ip(s) and validate on basis of ip octate
        4.get minimum and maximum value of last octate of ip(s).
        if minimum and maximum values are same then return only maximum
        value otherwise return range between max and min values.
        :param refresh: this value must be either "True" or "False".
        :return: available ip list
        '''
        consumed_site_ips = self.get_consumed_site_ips(refresh).get('result', [])
        consumed_ip_addresses = self.get_valid_v4_ips(consumed_site_ips)
        response_data = []
        available_ips = self.generate_available_ips(consumed_ip_addresses)
        available_status_ip_dict = self.get_available_status_ip_dict()
        triplet_group_dict = self.get_triplet_group_from_ips(available_ips)
        for available_ip in available_ips:
            existing_available_ip_info = available_status_ip_dict.get(available_ip)
            if existing_available_ip_info:
                existing_available_ip_info['group_name'] = self.get_group_by_ip(triplet_group_dict, existing_available_ip_info.get('ip'))
                response_data.append(existing_available_ip_info)
            else:
                ip_status_data = [self.available_status, '', self.siteid, available_ip, '', self.get_group_by_ip(triplet_group_dict, available_ip)]
                self.update_data_dict_list(response_data, self.ip_status_keys, ip_status_data)
        return {'result': response_data}

    def get_group_by_ip(self, triplet_group_dict, ip):
        group_info = triplet_group_dict.get(self.get_ip_triplet(ip), {})
        return group_info.get('name', self.groups.default_group)

    def get_triplet_group_from_ips(self, available_ips):
        ip_triplets = list(set([self.get_ip_triplet(ip) for ip in available_ips]))
        triplet_group_name_dict = self.groups.get_group_by_triplet(siteid=self.siteid, triplet=ip_triplets)
        return triplet_group_name_dict if triplet_group_name_dict else {}

    def get_valid_v4_ips(self, consumed_site_ips):
        return [ip['ip'] for ip in consumed_site_ips if self.validate_ipv4_addr(ip['ip'])]

    def generate_available_ips(self, ip_address_data):
        reserved_ips = self.get_reserve_site_ips().get('result', [])
        reserved_site_ips = [ip['ip'] for ip in reserved_ips]
        triplet_ips_dict = defaultdict(list)
        for ip in ip_address_data:
            triplet_ips_dict[self.get_ip_triplet(ip)].append(ip)

        available_ips = []
        for triplet, ips in triplet_ips_dict.iteritems():
            ips_list = [map(int, ip.split(".")) for ip in ips]
            ip_d_octates = [ip[3] for ip in ips_list]
            min_val, max_val = min(ip_d_octates), max(ip_d_octates)
            available_vals = [i for i in range(min_val, max_val + 1)
                              if i not in ip_d_octates]
            for av in available_vals:
                available_ip = "{0}.{1}".format(triplet, av)
                available_ips.append(available_ip)

        available_site_ips = [ip for ip in available_ips
                              if ip not in reserved_site_ips]
        return available_site_ips

    def get_ips(self, refresh):
        consumed_site_ips = self.get_consumed_site_ips(refresh).get('result', [])
        consumed_ips_data = self.get_valid_v4_ips(consumed_site_ips)
        additional_ips = [ip['ip'] for ip in consumed_site_ips if ip['ip'] not in consumed_ips_data]
        return_data = []
        return_data.append({'additional_ips' : additional_ips})
        consumed_ips = [map(int, ip.split("."))
                        for ip in consumed_ips_data]
        ip_collection = defaultdict(list)
        for ip in consumed_ips:
            ip_collection[ip[0]].append(ip)

        ip_tree_collection = {}
        ip_childs = []

        for key, val in ip_collection.iteritems():
            ip_child_collection = {}
            ip_tree_collection[key] = val
            ip_child_collection['A'] = key
            coll_b, coll_c = defaultdict(list), defaultdict(list)
            for i in val:
                coll_b[i[1]].append(i)
                coll_c[i[2]].append(i)
            ip_tree_collection[key].append(coll_b)
            ip_child_collection['B'] = coll_b.keys()
            ip_tree_collection[key].append(coll_c)
            ip_child_collection['C'] = coll_c.keys()
            ip_childs.append(ip_child_collection)

        for child in ip_childs:
            octate_a, octate_b, octate_c = \
                child.get('A'), child.get('B'), child.get('C')
            octate_a_ips = ip_tree_collection.get(octate_a)

            all_ips = [ip for ip in octate_a_ips if type(ip) == list]

            tree_data = ["{0}.X.X.X".format(octate_a), self.get_childs(octate_a, octate_b, octate_c, all_ips), len(all_ips)]
            self.update_data_dict_list(return_data, self.ip_tree_keys, tree_data)
        return {'result': return_data}

    def get_childs(self, octate_a, octate_b, octate_c, all_ips):
        octate_b_childs = []
        for b in octate_b:
            b_child_count = 0
            octate_b_dict = {}
            octate_b_dict['ip'] = "{0}.{1}.X.X".format(octate_a, b)
            octate_b_dict['children'] = []
            for c in octate_c:
                c_childs = self.get_triplet_childs('{0}.{1}.{2}'
                                                   .format(octate_a, b, c),
                                                   all_ips)
                c_child_count = len(c_childs)
                octate_c_data = ["{0}.{1}.{2}.X".format(octate_a, b, c), c_childs, c_child_count]
                octate_c_dict = dict(zip(self.ip_tree_keys, octate_c_data))
                b_child_count = b_child_count + c_child_count
                octate_b_dict['children'].append(octate_c_dict)
            octate_b_dict['count'] = b_child_count
            octate_b_childs.append(octate_b_dict)
        return octate_b_childs

    def get_triplet_childs(self, triplet, ips):
        triplet_dict = defaultdict(list)
        for ip in ips:
            tpl = '{0}.{1}.{2}'.format(ip[0], ip[1], ip[2])
            triplet_dict[tpl].append(ip)
        triplet_ips = sorted(triplet_dict.get(triplet, []), key=itemgetter(3))
        return ['.'.join(map(str, ip)) for ip in triplet_ips]

    def generate_virtual_node_dict(self, nodes):
        '''
        generates virtual nodes dict bases on which ever node has key from
        virtual node key list.
        :param nodes:
        :return:
        '''
        for node in nodes:
            for key in self.virtual_node_key_list:
                if key in node:
                    self.virtual_node_dict.update({node['address']: node})

    def get_nodes(self, refresh="False"):
        '''
        This function generate nodes data including physical nodes,
        vcenter and virtual nodes.
        if refresh is true then get nodes information from db and
        update in class dict otherwise try to get data from dict
        and if there is no data in dict generate nodes data and update dict.
        :param refresh: this value must be either "True" or "False".
        :return: nodes
        '''
        if refresh == "True":
            return self.generate_nodes_and_update_cache()
        else:
            cached_nodes_data = self.\
                cached_nodes.get(self.siteid, [])
            if cached_nodes_data:
                updated_cached_nodes = self.updated_nodes_group(cached_nodes_data)
                return {'result': updated_cached_nodes}
            else:
                return self.generate_nodes_and_update_cache()

    def generate_nodes_and_update_cache(self):
        site_nodes = list(self.facts_collection.find({'siteid': self.siteid}))
        self.generate_virtual_node_dict(site_nodes)
        self.create_physical_nodes_list(site_nodes)
        nodes_data = self.generate_nodes_data()
        untagged_nodes_data = dict(zip(self.physical_node_keys, [self.untagged_nodes_str, '', '', '', '', '', '', '',
                                                       self.generate_untagged_nodes(nodes_data)]))
        return_data = nodes_data + [untagged_nodes_data] if \
            untagged_nodes_data.get('virtualnodes') else nodes_data
        NodesIPInventoryModel.cached_nodes.update({self.siteid: return_data})
        return {'result': return_data}

    def generate_untagged_nodes(self, nodes):
        virtual_nodes = [node.get('virtualnodes') for node in nodes]
        vm_nodes_ips = []
        for vms in virtual_nodes:
            for vm in vms:
                vm_nodes_ips.append(vm.get('vipaddress'))
        untagged_nodes = []
        for vn_key, vn_val in self.virtual_node_dict.items():
            if vn_key not in vm_nodes_ips and \
                    vn_val.get('hostname') != 'localhost':
                vn_val.pop('Components', None)
                self.update_data_dict_list(untagged_nodes, self.virtual_node_keys, data=[normalized_value(vn_val.get('hostname')),
                                                               normalized_value(vn_val.get('product_id')),
                                                               normalized_value(vn_val.get('address')),
                                                               normalized_value(vn_val.get('product_version')),
                                                               self.get_untagged_node_type(vn_val),
                                                               normalized_value(vn_val.get('modules')[0]),
                                                               self.default_value, self.default_value, []])
                self.virtual_node_dict.pop(vn_key, None)
        return untagged_nodes

    def get_untagged_node_type(self, node):
        isp = node.get('ISP')
        return normalized_value(isp.get('module_type')) if isp else self.default_value

    def generate_nodes_data(self):
        nodes_data = []
        for node in self.physical_nodes:
            vcenter = node.get('vCenter')
            self.update_data_dict_list(nodes_data, self.physical_node_keys, data=[normalized_value(node.get('hostname')),
                                                       normalized_value(node.get('product_id')),
                                                       normalized_value(node.get('address')),
                                                       normalized_value(node.get('product_version')),
                                                       normalized_value(self.groups.default_group),
                                                       normalized_value(node.get('manufacturer')),
                                                       self.get_physical_node_type(node.get('modules', [])),
                                                       normalized_value(node.get('model')),
                                                       self.get_vcenter_data(vcenter) if vcenter else []])
        nodes = self.updated_nodes_group(nodes_data)
        return nodes

    def get_physical_node_type(self, modules):
        if not modules:
            return self.default_value
        else:
            return normalized_value(",".join(map(str, modules)))

    def get_ip_triplet(self, ip):
        return ".".join(ip.split(".")[:3]) if ip else None

    def updated_nodes_group(self, nodes):
        nodes_ips = [node.get('ipaddress') for node in nodes]
        triplet_group_dict = self.get_triplet_group_from_ips(nodes_ips)
        for node in nodes:
            node['location'] = self.get_group_by_ip(triplet_group_dict, node.get('ipaddress'))
        return nodes

    def get_triplet_group_by_ip(self, ip):
        triplet = self.get_ip_triplet(ip)
        triplet_group_name_dict = self.groups.get_group_by_triplet(siteid=self.siteid, triplet=[triplet])
        group_info = triplet_group_name_dict.get(triplet, {})
        return triplet, group_info.get('name', self.groups.default_group), ip

    def create_physical_nodes_list(self, nodes_data):
        for node in nodes_data:
            for key in self.virtual_node_key_list:
                if key not in node and node.get('hostname') != "localhost":
                    self.physical_nodes.append(node)

    def get_vcenter_data(self, vcenter):
        self.generate_vms_data(vcenter)
        vcenter_data = []
        for vm in self.vms_data:
            guest = vm.get('guest', {})
            virtual_node = self.virtual_node_dict.get(guest.get('ipAddress'), {})
            vproduct_id = virtual_node.get('product_id', self.default_value)
            vproduct_version = virtual_node.get('product_version', self.default_value)
            self.update_data_dict_list(vcenter_data, self.virtual_node_keys, data=[normalized_value(vm.get('name')),
                                                         vproduct_id, normalized_value(guest.get('ipAddress')),
                                                         vproduct_version, self.get_vm_type(guest.get('ipAddress')),
                                                         normalized_value(vm.get('guestFullName')),
                                                         normalized_value(vm.get('numCpu')),
                                                         normalized_value(vm.get('memorySizeMB')),
                                                         self.generate_disk_data(vm.get('disks'), guest.get('disk', []))])
        return vcenter_data

    def generate_vms_data(self, vcenter):
        for vc_key, vc_val in vcenter.iteritems():
            if vc_key == "vms":
                for vm in vc_val:
                    self.vms_data.append(vm)
            if isinstance(vc_val, dict):
                self.generate_vms_data(vc_val)

    def generate_disk_data(self, disks, disk):
        disk_data = []
        self.generate_disk_data_from_disks(disk_data, disks)
        self.generate_disk_data_from_disk(disk_data, disk)
        return disk_data

    def generate_disk_data_from_disk(self, disk_data, disk):
        for disk_dict in disk:
            disk_info = [normalized_value(disk_dict.get('diskPath')), self.to_gb(disk_dict.get('capacity')), self.to_gb((disk_dict.get('capacity') - disk_dict.get('freeSpace')))]
            self.update_data_dict_list(disk_data, self.disk_data_keys, disk_info)

    def generate_disk_data_from_disks(self, disk_data, disks):
        if disks and isinstance(disks, dict):
            for disk_key, disk_val in disks.iteritems():
                disk_info = [normalized_value(disk_val.get('diskPath')), normalized_value(disk_val.get('capacity')), self.get_disk_usage(disk_val.get('capacity'),disk_val.get('freeSpace'))]
                self.update_data_dict_list(disk_data, self.disk_data_keys, disk_info)
        self.get_disk_capacity(disk_data, disks)

    def get_disk_capacity(self, disk_data, disks):
        if disks and isinstance(disks, list):
            for disk_dict in disks:
                disk_capacity = disk_dict.get('capacityInKB')
                disk_info = [normalized_value(disk_dict.get('label')), self.to_gb(disk_capacity) if disk_capacity else self.default_value, self.default_value]
                self.update_data_dict_list(disk_data, self.disk_data_keys, disk_info)

    def to_gb(self, value):
        return str(value / 1048576) + 'GB'

    def get_bytes(self, value):
        _, value, suffix = value.partition(re.match("\d+\.\d+", value).group())
        mem_suffix = suffix.lower()
        return (float(value), suffix) if mem_suffix == "bytes" else \
            (float(value) * (10 ** MEMORY_VALUES_DICT[mem_suffix]), suffix)

    def calculate_disk_usage(self, base, power,
                             total_mem_bytes,
                             free_mem_bytes, total_mem_suffix):
        unit_value = base ** power
        total_bytes = total_mem_bytes - free_mem_bytes
        final_value = total_bytes / float(unit_value)
        return str(final_value) + total_mem_suffix.upper()

    def get_disk_usage(self, total_mem, free_mem):
        if total_mem and free_mem:
            total_mem_bytes, total_mem_suffix = self.get_bytes(total_mem)
            free_mem_bytes, free_mem_suffix = self.get_bytes(free_mem)

            total_mem_suffix = total_mem_suffix.lower()
            return self.calculate_disk_usage(
                10, MEMORY_VALUES_DICT[total_mem_suffix], total_mem_bytes,
                free_mem_bytes, total_mem_suffix)

        if total_mem and not free_mem:
            return total_mem
        return self.default_value

    def get_vm_type(self, vm_ip_address):
        if vm_ip_address:
            virtual_node = self.virtual_node_dict.get(vm_ip_address)
            if virtual_node:
                isp = virtual_node.get('ISP')
                if isp:
                    endpoint = isp.get('endpoint')
                    isp_address = endpoint.get('address')
                    if isp_address == vm_ip_address:
                        return isp.get('module_type', self.default_value)
        else:
            return self.default_value

    def check_ip_status_exist(self, ip, status):
        result = self.ip_status_collection \
            .find_one({'ip': ip,
                       'siteid': self.siteid,
                       'status': status}, {'_id': 0})
        return result

    def set_ip_status(self, request):
        ip = request['ip']
        triplet, group_name, ip = self.get_triplet_group_by_ip(ip)
        status = request['status']
        modified_by = request['modified_by']
        message = request['message']
        ip_status_exist = self.check_ip_status_exist(ip, status)
        if ip_status_exist:
            return {'result': ID_ALREADY_EXIST}
        self.ip_status_collection.update_one(
            {'siteid': self.siteid, 'ip': ip},
            {'$set': {
                'status': status,
                'ip': ip,
                'message': message,
                'modified_by': modified_by,
                'group_name': group_name,
                'triplet': triplet
            }}, upsert=True
        )
        return {'result': OK_STATUS_CODE}

    def get_reserve_site_ips(self):
        reserved_site_ips = self.get_ip_status_collection_ips(
            self.reserved_status)
        return {'result': reserved_site_ips}

    def get_available_status_ip_dict(self):
        available_status_ips = self.get_ip_status_collection_ips(
            self.available_status)
        available_ip_dict = {}
        for ip in available_status_ips:
            available_ip_dict.update({ip['ip']: ip})
        return available_ip_dict

    def get_ip_status_collection_ips(self, status):
        self.filter = {'status': status,
                       'siteid': self.siteid}
        result = self.ip_status_collection.aggregate([
            {'$match': self.filter},
            {'$sort': {'ip': 1}},
            {'$project': self.get_projection()}
        ])
        return list(result)

    def get_projection(self):
        return {
            'status': '$status',
            '_id': 0,
            'siteid': '$siteid',
            'ip': '$ip',
            'message': '$message',
            'modified_by': '$modified_by',
            'group_name': '$group_name'
        }

    def validate_ipv4_addr(self, ip):
        matched_ip = re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", ip)
        return bool(matched_ip)

    def get_consumed_site_ips(self, refresh):
        nodes = self.get_nodes(refresh)
        addresses = nodes.get('result', [])
        ip_addresses = []
        for addr in addresses:
            nodename = addr.get('nodename')
            group_name = addr.get('location')
            if nodename == self.untagged_nodes_str:
                self.virtual_node_consumed_ips(addr, group_name, ip_addresses)
            ip_data = [normalized_value(nodename), addr.get('ipaddress'), group_name]
            self.virtual_node_consumed_ips(addr, group_name, ip_addresses)
            self.update_data_dict_list(ip_addresses, self.consumed_ip_data_keys, ip_data)
        consumed_ips = [ip for ip in ip_addresses if ip['ip']
                        not in [None, '', self.default_value]]
        return {'result': consumed_ips}

    def virtual_node_consumed_ips(self, addr, group_name, ip_addresses):
        virtual_nodes = addr.get('virtualnodes')
        for vn in virtual_nodes:
            vip_data = [normalized_value(vn.get('vnodename')), normalized_value(vn.get('vipaddress')),
                        group_name + " (virtual)" if group_name not in [None, '']
                        else self.groups.default_group]
            self.update_data_dict_list(ip_addresses, self.consumed_ip_data_keys, vip_data)
