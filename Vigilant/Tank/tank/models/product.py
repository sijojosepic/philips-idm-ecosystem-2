from tank.tnconfig import COLLECTIONS


class ProductModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['PRODUCT']]

    def get_projection(self):
        return {
        '_id': 0,
        'product_value': '$value',
        }

    def get_product_value(self, product_name):
        matches = self.collection.aggregate([
            {'$match': {'name': product_name}},
            {'$project': self.get_projection()}
        ])
        return {'result': list(matches)}
