from tank.tnconfig import COLLECTIONS, OK_STATUS_CODE, \
    DIRECTORY_MAX_ELEMENTS, DIRECTORY_MAX_ELEMENTS_LENGTH_EXCEED, \
    ID_DOES_NOT_EXIST
import calendar
import time


class DirectoryModel(object):
    '''
    This class addresses Directory(s) details for a particular site id.
    '''
    def __init__(self, mongodb, siteid=None):
        self.mongodb = mongodb
        self.siteid = siteid
        self.collection = self.mongodb[COLLECTIONS['DIRECTORIES']]

    def get_directories(self):
        directories = self.collection.aggregate([
            {'$match': {'siteid': self.siteid}},
            {'$project': {
                'siteid': '$siteid',
                'poc_number': '$poc_number',
                'poc_name': '$poc_name',
                'role': '$role',
                'address': '$address',
            }}
        ])
        return {'result': list(directories)}

    def save_directory(self, request_json):
        directories_len = len(self.get_directories().get('result'))
        if directories_len > DIRECTORY_MAX_ELEMENTS:
            return {'code': DIRECTORY_MAX_ELEMENTS_LENGTH_EXCEED}
        else:
            self.collection.insert_one(
                {'siteid': self.siteid,
                '_id': self.siteid + '-' + str(calendar.timegm(time.gmtime())),
                'poc_number': request_json['poc_number'],
                'poc_name': request_json['poc_name'],
                'role': request_json['role'],
                'address': request_json['address'],
                })
            return {'code': OK_STATUS_CODE}

    def update_directory(self, request_json):
        self.collection.update_one(
            {'_id': request_json['_id']},
            {'$set': {
                'poc_number': request_json['poc_number'],
                'poc_name': request_json['poc_name'],
                'role': request_json['role'],
                'address': request_json['address'],
            }})
        return {'code': OK_STATUS_CODE}

    def delete_directory(self, ids):
        id_list = ids.split(',')
        map(lambda id: self.collection.delete_one({'_id': id}),
            id_list)
        return {'result': OK_STATUS_CODE}
