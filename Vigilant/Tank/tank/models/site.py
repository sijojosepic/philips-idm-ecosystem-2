import re

from tank.models.fact import SiteFactsModel
from tank.models.dashboard import DashboardModel
from tank.models.site_rules import RulesModel
from tank.models.fact import SiteFactsModel
from subscription import SubscriptionModel
from tank.tnconfig import COLLECTIONS, IBC, LHOST_IGNORE_KEYS
from tank.util import rename_id, get_logger, shift_key, FernetCrypto
from cached_property import cached_property
from datetime import datetime, timedelta
import py
import yaml
import temporary
import time
import os

log = get_logger()

class SiteModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['SITES']]
        self.facts_collection = SiteFactsModel(self.mongodb, None)
        self.filter = {}
        self.lhost_site_uri_path = ''

    def get_siteids(self):
        result = self.collection.distinct('_id', {'_id': {'$nin': [None]}})
        return {'siteids': list(result)}

    def set_siteid_filter(self, siteids=None):
        if siteids:
            self.filter['$and'] = [{'_id': {'$nin': [None]}}, {'_id': {'$in': siteids}}]
        else:
            self.filter['_id'] = {'$nin': [None]}

    def set_country_filter(self, countries=None):
        if countries:
            self.filter['country.country'] = {'$in': countries}

    def get_site_names(self, siteids=None):
        """Returns a map of siteid to site name"""
        self.set_siteid_filter(siteids)
        sites_info = self.collection.find(self.filter, {'name': 1})
        return dict((site['_id'], site.get('name')) for site in sites_info)

    def get_sites(self, siteids=None, countries=None):
        def getKey(site):
            return site.get('_id')
        """ Get Site details """
        self.set_siteid_filter(siteids)
        self.set_country_filter(countries)
        sites_info = self.collection.find(self.filter, {'Components': 0})
        site_infra_versions = self.facts_collection.get_version_by_product_id('ISPACS', 'ISP.module_type', 128).get('result')
        infra_versions = {}
        for site in site_infra_versions:
            infra_versions[site.get('siteid')] =  site.get('product_version')
        return {'sites': [ shift_key(rename_id('siteid', site), infra_versions, 'pacs_version', site.get('siteid')) for site in sorted(sites_info, key=getKey)]}

    def get_subscription_candidates(self, subscription_name):
        subscription = SubscriptionModel(self.mongodb).get_subscription(subscription_name)
        countries = subscription.get('countries')
        if not countries:
            return {'sites': []}
        return self.get_sites(countries=countries)

    def get_facts_hosts_details(self):
        sites = self.get_sites()['sites']
        db = DashboardModel(self.mongodb)
        db.filter = {'service': 'Product__IDM__Nebuchadnezzar__Alive__Status', 'unreachable': {'$not': {'$eq': True}}}
        neb_status = db.get_matches()
        neb_siteids = []
        if len(neb_status) > 0:
            neb_siteids = [site['siteid'] for site in neb_status['result']]
        localhost_regex = re.compile('localhost')
        hostnames = []
        db = RulesModel(self.mongodb)
        sites_with_maintenance_rules = db.get_rule_list()
        for site in sites:
            if site['siteid'] in neb_siteids:
                site['status'] = 'Down'
            else:
                site['status'] = 'Up'
            if site['siteid'] in sites_with_maintenance_rules['sites']:
                site['status'] = 'Maintenance'
                result = db.get_rules_with_siteid(site['siteid'])
                site['maintenance_rule'] = result['result']
        return {'result': sites}

    def get_host_count(self, vcenters, localhost_regex, hostnames):
        count = 0
        for vcenter in vcenters:
            if 'vms' in vcenter:
                for vm in vcenter['vms']:
                    if ('hostName' in vm['guest']) and \
                            (not localhost_regex.match(vm['guest']['hostName'])) and \
                            ((vm['guest']['hostName']).lower() not in hostnames):
                        count += 1
        return count

    def update_site(self, siteid, name, country):
        self.collection.update_one({'_id': siteid}, {'$set': {'name': name, 'country': country}}, upsert=False)

    def delete_site(self, siteid):
        self.collection.delete_one({'_id': siteid})

    def get_site_count(self):
        return { 'count': len(self.collection.distinct('_id', {'_id': {'$nin': [None]}, 'production': {'$ne': 'False'}}))}

    def set_site_svn_lhost_path(self, siteid):
        self.lhost_site_uri_path = IBC['SVN_URL'] + IBC['SITES_URL'] + "{siteid}/lhost.yml".format(siteid=siteid)

    def get_site_svn_lhost_path(self):
        return self.lhost_site_uri_path

    @cached_property
    def wc(self):
        return self.setup_wc()

    @cached_property
    def wc2(self):
        return self.setup_wc2()

    def setup_wc(self):
        auth = py.path.SvnAuth(IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], cache_auth=False, interactive=False)
        wc = py.path.svnurl(self.get_site_svn_lhost_path())
        wc.auth = auth
        return wc

    def setup_wc2(self):
        auth = py.path.SvnAuth(IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], cache_auth=False, interactive=False)
        wc = py.path.svnurl(IBC['SVN_URL'] + IBC['MAIN_YML_URL'])
        wc.auth = auth
        return wc

    def scanner_info(self, siteid):
        self.set_site_svn_lhost_path(siteid)
        try:
            with temporary.temp_dir(parent_dir='/tmp/') as tmp_work_dir:
                teml_yml_fl = str(tmp_work_dir) + '/lhost.yml'
                self.wc.export(teml_yml_fl)
                info = self.wc.info()
                with open(teml_yml_fl) as scanner_conts:
                    return {'code': 600, 'contents': yaml.load(scanner_conts) ,
                            'last_modified_time': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(info.mtime))}
        except py._process.cmdexec.ExecutionFailed as e:
            return {'code': 601, 'message': "Could not retrieve targets information because some targets don't exist"}
        except Exception as e:
            return {'code': 602, "message": "You did nasty thing and we are not responsible,that's all we know now"}

    def filter_scanner_contents(self, scanner_contents):
        if scanner_contents and LHOST_IGNORE_KEYS:
            [scanner.pop(ignore_key, None) for scanner in scanner_contents.get('endpoints', []) for ignore_key in
             LHOST_IGNORE_KEYS]
        return scanner_contents

    def main_yml(self):
        try:
            with temporary.temp_dir(parent_dir='/tmp') as tmp_work_dir:
                teml_yml_fl = str(tmp_work_dir) + '/main.yml'
                self.wc2.export(teml_yml_fl)
                with open(teml_yml_fl) as scanner_conts:
                    return yaml.load(scanner_conts)
        except Exception as e:
            log.error("Error loading main.yml with error:\n {}".format(e) )
            return {'shinken_initial_resources': {}, 'default_threshold_values': {}}

    def shinken_refs(self):
        main_yaml = self.main_yml()
        return {'result' : main_yaml['shinken_initial_resources'].keys()}

    def all_thresholds(self):
        main_yaml = self.main_yml()
        return {'result' : main_yaml['default_threshold_values']}

    def set_subpath_siteid(self, siteid):
        self.subpath = siteid

    def get_subpath_siteid(self):
        return self.subpath

    def set_work_dir(self,work_dir):
        self.work_dir = str(work_dir)

    def get_work_dir(self):
        return self.work_dir

    def get_subpath_wc(self):
        return os.path.join(self.get_work_dir(),self.get_subpath_siteid() )

    def get_svn_auth(self):
        self.sites_root_url = IBC['SVN_URL'] + IBC['SITES_URL']
        return py.path.SvnAuth(IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], cache_auth=False, interactive=False)

    @cached_property
    def wc_co(self):
        return self.setup_wc_for_co()

    @property
    def subpath_url(self):
        return os.path.join(self.sites_root_url, self.get_subpath_siteid())

    def setup_wc_for_co(self):
        auth = self.get_svn_auth()
        subpath_svn_url = self.subpath_url
        wc = py.path.svnwc(self.get_subpath_wc())
        wc.auth = auth
        wc.checkout(subpath_svn_url)
        return wc

    def process_config_file(self,filename,content):
        if content is None:
            file_to_remove = self.wc_co.join(filename)
            if file_to_remove.check():
                file_to_remove.remove()
        else:
            file_to_write = self.wc_co.ensure(filename)
            file_to_write.write(content)

    def log_status(self):
        status = self.wc_co.status(rec=1)
        attr_to_list = ['added', 'deleted', 'modified', 'conflict', 'unknown']
        for attr in attr_to_list:
            svnwc_files_with_attr = getattr(status, attr)
            with_attr_filenames = [item.strpath for item in svnwc_files_with_attr]
            log.info('%s files: %s', attr, str(with_attr_filenames))

    def update_lhost(self, update_contents):
        siteid = update_contents.get('siteid')
        user = update_contents.pop('user')
        comment = update_contents.pop('comment')
        self.set_subpath_siteid(siteid)
        with temporary.temp_dir(prefix='configgen_', parent_dir='/tmp') as work_dir:
            self.set_work_dir(work_dir)
            try:
                self.process_config_file('/lhost.yml' ,yaml.safe_dump(update_contents, default_flow_style=False))
                self.log_status()
                revision = self.wc_co.commit(msg='Updated lhost details of siteid:%s by %s on %s. Comment:%s' % (siteid, user, datetime.now(), comment))
                if revision:
                    log.info('revision: %s', str(revision))
                else:
                    log.debug('No changes, nothing to commit')
                return {'code': 200, 'message': 'Success'}
            except Exception as e:
                return {'code': 201, 'message': 'Failed'}
    
    def custom_encrypt(self, given_str):
        return {'result':'{+'+FernetCrypto.encrypt(bytes(given_str))+'+}'}
