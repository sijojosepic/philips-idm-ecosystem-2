from tank.tnconfig import COLLECTIONS
from tank.util import get_logger, rename_id


log = get_logger()


class SubscriptionModel(object):
    def __init__(self, mongodb, path=''):
        self.mongodb = mongodb
        self.path = path
        self.collection = mongodb[COLLECTIONS['SUBSCRIPTIONS']]
        self._slashed_path = None
        self.filter = {}

    @property
    def slashed_path(self):
        if self._slashed_path is None:
            self._slashed_path = '/{0}/'.format(self.path.strip('/')) if self.path else '/'
        return self._slashed_path

    def get_groups(self):
        all_subpaths = self.collection.distinct(
            'path',
            {'path': {'$regex': self.get_starts_with_regex(self.slashed_path)}}
        )
        result = self.get_direct_descendant_groups(self.slashed_path, all_subpaths)
        return {'groups': list(result)}

    def get_direct_descendant_groups(self, parent_path, sub_paths):
        result = set()
        parent_path_len = len(parent_path)
        for full_path in sub_paths:
            if not full_path.startswith(parent_path):
                continue
            sub_path = full_path[parent_path_len:]
            first_group = sub_path.split('/')[0]
            if first_group:
                result.add(str(first_group))
        return result

    def get_names(self):
        query = {}
        if self.path:
            query['path'] = {'$regex': self.get_path_equal_or_starts_with_regex(self.path)}
        result = self.collection.distinct('_id', query)
        return {'subscriptions': list(result)}

    def get_starts_with_regex(self, text):
        return r'^{0}'.format(text)

    def get_path_equal_or_starts_with_regex(self, text):
        return r'^/{0}(/|$)'.format(text.strip('/'))

    def get_subscriptions(self):
        subscriptions = self.collection.find(self.filter).sort('_id',1)
        return {'subscriptions': [rename_id('subscription', sub) for sub in subscriptions]}

    def get_subscription(self, name):
        return self.collection.find_one(name)

    def get_path(self, id):
        path = self.collection.find_one(id, {'_id':0, 'path':1, 'file_name':1})
        return path

    def get_projection(self):
        return {
        '_id': 0,
        'package_file': '$file_name',
        'product_name': '$product_name',
        }

    def get_subscription_details(self, subscription_id):
        matches = self.collection.aggregate([
            {'$match': {'_id': subscription_id}}, 
            {'$project': self.get_projection()}
        ])
        return {'result': list(matches)}

    def get_package_pcm_info(self):
        package_pcm_info = {}
        results = self.collection.find(self.filter, {'_id':1, 'pcm_enabled':1, 'display_name':1})
        for package in results:
            package_pcm_info[package['_id']] = {}
            package_pcm_info[package['_id']]['pcm_enabled'] = package.get('pcm_enabled', False)
            package_pcm_info[package['_id']]['display_name'] = package.get('display_name', package['_id'])
        return package_pcm_info
