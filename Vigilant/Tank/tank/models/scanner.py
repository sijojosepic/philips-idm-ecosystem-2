from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder


class ScannerModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['SCANNERS']]


    def get_scanner_details(self):
        details = self.collection.find({}, {"_id": 0, "scanner": 1, "credentials": 1})
        return {'result': list(details)}

class ScannerQuery(QueryBuilder):
    parameters = {
        'scanner': 'scanner',
        'credentials': 'credentials',
    }
    
    def get_filter(self):
        return {}