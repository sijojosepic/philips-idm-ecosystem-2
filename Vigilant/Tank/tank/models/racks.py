from tank.tnconfig import COLLECTIONS, OK_STATUS_CODE, ID_ALREADY_EXIST, \
    ID_DOES_NOT_EXIST
import calendar
import time

class RackModel(object):
    '''
    This class addresses racks information such as slots, devices, location, pdu
    for a particular site id. This class methods provides data about devices, and
    racks locations.
    '''
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = self.mongodb[COLLECTIONS['RACKS']]

    def get_locations(self, site_id):
        location_list = list(self.collection.aggregate([{'$match': {'siteid': site_id}}, {'$sort': {'_id': -1}}]))
        return {'results': location_list}

    def get_rack_details_by_location_id(self, siteid, location_id):
        rack_list = list(self.collection
                         .find({'siteid': siteid,
                                'location_id': location_id},
                               {'_id': 0}))
        return {'result': rack_list}

    def create_rack_details_by_location_id(self, request):
        racks = request.get('racks')
        rack_list = list(self.collection
                         .find({'siteid': request['siteid'],
                                'location_id': request['location_id']},
                               {'_id': 0}))
        if len(rack_list) == 0:
            self.collection.insert_one({
                '_id': request['siteid'] + '-' + str(
                    calendar.timegm(time.gmtime())),
                'siteid': request['siteid'],
                'location_id': request['location_id'],
                'location_name': request['location_name'],
                'racks': racks})
            return {'result': OK_STATUS_CODE}
        else:
            return {'result': ID_ALREADY_EXIST}

    def update_rack_details_by_location_id(self, request):
        racks = request.get('racks')
        rack_list = list(self.collection
                         .find({'siteid': request['siteid'],
                                'location_id': request['location_id']},
                               {'_id': 0}))
        if len(rack_list) > 0:
            self.collection\
                .update_one({'siteid': request['siteid'],
                             'location_id': request['location_id']},
                            {'$set':
                                {'siteid': request['siteid'],
                                 'location_id': request['location_id'],
                                 'location_name': request['location_name'],
                                 'racks': racks}})
            return {'result': OK_STATUS_CODE}
        else:
            return {'result': ID_DOES_NOT_EXIST}

    def delete_location_details(self, siteid, location_id):
        self.collection\
            .delete_one({'siteid': siteid, 'location_id': location_id})
        return {'result': OK_STATUS_CODE}
