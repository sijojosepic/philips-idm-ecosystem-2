from tank.tnconfig import COLLECTIONS, IGNORE_VIEW_IDS
from operator import itemgetter


class IAMViewModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['IAM_VIEWS']]
        self.filter = {}
        self.routes_info = []

    def get_projection(self):
        return {
            '_id': 1,
            'view_id': 1,
            'route_link': 1,
            'parent_view_id': 1,
            'route_label': 1,
            'priority': 1
        }

    def get_filter(self, view_ids):
        self.filter['view_id'] = {'$in': view_ids}
        return self.filter

    def get_matches(self, view_ids):
        matches = self.collection.aggregate([
            {'$match': self.get_filter(view_ids)},
            {'$project': self.get_projection()},
        ])
        return list(matches)

    def get_routes_info(self, view_ids):
        matches_list = self.get_matches(view_ids)
        child_routes_info, parent_ids = self.get_child_routes_info(matches_list)
        matches_list = self.get_matches(parent_ids.keys())
        for match in matches_list:
            tab_link = {}
            tab_link['label'] = match['route_label']
            if match['view_id'] == 'SITE_VIEW':
                tab_link['link'] = match['route_link']
            else:
                tab_link['link'] = match['route_link'] + parent_ids[match['view_id']]
            tab_link['parent_id'] = match['view_id']
            tab_link['childlinks'] = child_routes_info[match['view_id']]
            tab_link['priority'] = match['priority']
            if tab_link['parent_id'] not in IGNORE_VIEW_IDS:
                self.routes_info.append(tab_link)
        sorted_routes_info = sorted(self.routes_info, key=itemgetter('priority'))
        return sorted_routes_info

    def get_child_routes_info(self, matches):
        parent_ids = {}
        child_tab_links = {}
        for match in matches:
            if match['parent_view_id'] not in child_tab_links:
                child_tab_links[match['parent_view_id']] = []
            tab_link = {}
            tab_link['label'] = match['route_label']
            tab_link['link'] = match['route_link']
            tab_link['priority'] = match['priority']
            child_tab_links[match['parent_view_id']].append(tab_link)
        for key in child_tab_links:
            child_tab_links[key] = sorted(child_tab_links[key], key=itemgetter('priority'))
            parent_ids[key] = child_tab_links[key][0]['link']
        return child_tab_links, parent_ids

