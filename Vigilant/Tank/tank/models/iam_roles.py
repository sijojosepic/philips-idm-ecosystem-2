from tank.tnconfig import COLLECTIONS
from tank.util import QueryBuilder, get_date_to_string_field_projection
from datetime import datetime


class IAMRoleModel(object):
    def __init__(self, mongodb):
        self.mongodb = mongodb
        self.collection = mongodb[COLLECTIONS['IAM_ROLES']]
        self.filter = {}

    def get_projection(self):
        return {
            '_id': 1,
            'group_id': 1,
            'managingOrganization': 1,
            'name': 1,
            'description': 1,
            'timestamp': get_date_to_string_field_projection('$timestamp'),
            'views': 1
        }

    def save_roles(self, role_details):
        """ 'id':'string, 'name':string, description:'string' ,managingOrgnization:string}"""
        identifier = role_details.pop('id')
        role_details['timestamp'] = datetime.now()
        self.collection.update_one({'_id': identifier}, {'$set':role_details}, upsert= True)
        return True

    def get_matches(self):
        matches = self.collection.aggregate([
            {'$match': self.filter},
            {'$project': self.get_projection()},
            {'$sort': {'timestamp': -1}}
        ])
        return {'result': list(matches)}

    def get_roles_by_id(self, id):
        self.filter['_id'] = id
        return self.get_matches()

    def get_all_roles(self, query):
        self.filter = IAMRolesQuery(query).get_filter() or {}
        return self.get_matches()


class IAMRolesQuery(QueryBuilder):
    paramters = {}

    def __init__(self, query):
        self.query = query

    def get_filter(self):
        pass