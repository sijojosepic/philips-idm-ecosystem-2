CELERY_PATH = '/usr/lib/celery'
CELERY_CONFIGURATION_OBJECT = 'celeryconfig'
IN_TASK_NAME = 'phim_backoffice.routers.inbound'

## REDIS
REDIS = {
    'HOST': 'redis.phim.isyntax.net',
    'PORT': '6379'
}

## MONGODB / Collections
MONGODB = {
    'URL': 'mongo.phim.isyntax.net:27017/',
    'RETRIES': 2,
    'RETRY_DELAY': 10,
    'DB_NAME': 'somedb',
    'USER_NAME':'gAAAAABbGlSzLK42jTsneAHsJQLMnVT367HYbgpTnH9Eq_6vYpF3d5xSZ9swzg0ysZmrfzchU61yTxLFmMj_F30x2jYJWSWguA==',
    'PASSWORD':'gAAAAABbGlSzXd1AHpJkwPfs9utSGdu-l3mUHHoVELw0CQhXuFB2w9Aoh3ZSJ4zdKV1nP53_KfG4P-Plbyon-pT-4pNpqViCpfmHoS0qNGA8qFASdfsBplc=',
    'PROTOCOL': 'mongodb'
}

COLLECTIONS = {
    'ALIVES': 'alives',
    'AUDIT': 'audit',
    'COUNTRY': 'country',
    'DASHBOARD': 'dashboard',
    'DESCRIPTION_DETAILS': 'description_details',
    'DEVICES': 'devices',
    'DIRECTORIES': 'directories',
    'DEPLOYMENT_KEYS':'deployment_keys',
    'EVENTS': 'events',
    'EXCEPTIONS': 'exceptions',
    'FACTS': 'facts',
    'IAM_USER': 'iam_user',
    'IAM_GROUP': 'iam_groups',
    'IAM_ROLES': 'iam_roles',
    'IAM_VIEWS': 'iam_views',
    'LHOST_UTILITIES': 'lhost_utilities',
    'RACKS': 'racks',
    'NAMESPACES': 'namespaces',
    'SITE_MAINTENANCE_RULES': 'site_maintenance_rules',
    'SITE_NOTES': 'site_notes',
    'SITEUPDATES': 'siteupdates',
    'SITE_CONFIGURATION': 'site_configuration',
    'SITES': 'sites',
    'SOLUTION_LANDSCAPE': 'solution_landscape',
    'STATES': 'states',
    'SUBSCRIPTIONS': 'subscriptions',
    'TAGS': 'tags',
    'PRODUCT_TECHNICAL_IDS':'product_technical_ids',
    'USERKEYS': 'userkeys',
    'USER': 'user',
    'USER_AUDIT': 'user_audit',
    'GROUPS': 'groups',
    'IP_STATUS': 'ip_status',
    'SCANNERS': 'scanners',
    'PRODUCT': 'product',
}

PUBLIC_MODULES = {
    'ISP': 'Intellispace PACS',
    'LN': 'Legacy Nagios',
    'SWD': 'Software Distribution',
    'Windows': 'Window Related Facts',
    'Components': 'List of Software Components',
    'PCM': 'Package Configuration Management'
}

PUBLIC_HOST_KEYS = ['address', 'domain']
NEB_IP_NAMESPACE = 'Administrative__Philips__Host__Information__IPAddress'
NEB_HOSTNAME = 'localhost'
NEB_SHINKEN_HOSTNAME = 'nb.shinken'
MONITOR_URL_TEMPLATE = 'https://{address}/thruk'

HANDLER_SERVICES = ['Product__IntelliSpace__PACS__iSyntaxServer__Aggregate']

IAM_CREDENTIALS = {
    # Development credentials
    # 'ORGANIZATION_ID': 'gAAAAABan3tnUSriaR419jExuGpKwOh-wTStFfF5FqW1N1VbxnwIoDAYx9K6LNWV6OpnNn_exfdoV0RKAXdzBiLG2FfAf-JmbVBknsvgfvyKl2w-uNrh-rnWZQa1QUhFH7iJNkqaT9Z5',
    # 'ORGANIZATION_NAME': 'gAAAAABan3u0p124RWiThSVhDm39LDODyi52Wuv_vfcBqPZOE26h7gBrb6qxt0v-coGOv4BMrlo0rSSEoTLZyhJgBLatpg-cvDYq_omdNbgbp2_66Jj85Us=',
    # 'SHARED_KEY': 'gAAAAABan3vwqz1LzSutujw7N2e1MG5h8eEJJ0TbsMmoSZ0quH3i0Ud1eUoyQdOzV-fDfsxvD7Wl6SlT3Oh6utoNslg2sr6mDgTkayFsTLiamAyXGy_wGlDeHG__Q6zKsfY60P5axupu',
    # 'SECRET_KEY': 'gAAAAABan3wV0T_aao75lUI3uLdPFNpkLubXJ3N7Dk8o4jO3oGkYPh4Mfiv5GpZdofzByIyOnzyWpOiaqU_jODJGfEyr0MVNIBizhiHKiJh6UXZa0V1KdVntTLjhVi50dyLTM8ELAkHE',
    # 'CLIENT_ID': 'gAAAAABan3xE0dseFbR25jp5K7UH-7ksO5ICh1UlV2V14QdjfmLuSDgD0rqEUT85u8GbESwb-U7YeSj-Svhl4-L9elwqw9QZzKjWkvfN68CtTPip63d3G7s=',
    # 'CLIENT_SECRET': 'gAAAAABan3xoPNJMMDa4uQt846ZqwbDz38hWXDzxFigcImPT_yzLPYKDSayB2-4Ug5-HKGaDnmAzEt5VA1Z2BQv_fTZJkOQm_w=='

    # Production credentials
    'ORGANIZATION_ID': 'gAAAAABa8-VD21m36Bumb9oovVa73z2f_rKCGAK05U5A1ou3WNmvjc0sAgOkUiMXsOJFQTTGW3hoaWm7aMR-Laf49t3eWE77EipO4l_aVaEU6ai4M4eSyyDgJzlWzH4Rv-lArcEbY_Ly',
    'ORGANIZATION_NAME': 'gAAAAABa8-Toy1Fb5PzbI_aAqouK8Slu1Kqqn3BG4Zcxd5trq5vBWQ322ISqsPrIAc0BJHgd64ZN2YoNwEqlk-9_eJ0ow9-Sp1Ta8-YgYIRDYcr5o969ssM=',
    'SHARED_KEY': 'gAAAAABa8-V9CGEIFw4JYAahN76_Fu0S7vaf-v-Q_KGIGnKHF9sp7UCm1l8oRPZNJyw2WI-_OUSRXLnhueul4LYF4_FQ5qQeuuhuEXO-3K4_aFb7faGlV3TDT8tvdJhvumvlMgTmQHvV',
    'SECRET_KEY': 'gAAAAABa8-XpIaqzFMo4bm2kxTpTl2pFKz4BciR_NDE03-8YwLfy9MXN1YIKY79ZQ1_TbaP5c6vUtsBsD50wRKEiNfbwzkcZGonYQEspK8yvHa_hPDU4IgShu9iM3vrNH8hZ054TINp1',
    'CLIENT_ID': 'gAAAAABa8-aqfK10tUh-gs9zqh1KsQCmNu2sXUaqF_MMcb-gfDA27DqBpRkfWs9dPf24XnZFAtlv4o4kX5OaMeGsX5XU1EHCK2skTB3gGIyTQ0cwsIQN0zg=',
    'CLIENT_SECRET': 'gAAAAABa8-cS-mwifXLrDWrBwxdwKbEd0k0Qh9CsERMb2wKVvwN4N70yMrnEOiGJWuz1CxUuTdzuYH5A4AkcMtWgXirc4iJ5_m1boVUYO1xuzroIFjCIYEE='
}

IAM_API_ENDPOINTS = {
    # Development Endpoints
    # 'IAM_URL': 'https://iam-integration.us-east.philips-healthsuite.com/',
    # 'IDM_URL': 'https://idm-integration.us-east.philips-healthsuite.com',
    # Updated Development Endpoints
    # 'IAM_URL': 'https://iam-client-test.us-east.philips-healthsuite.com/',
    # 'IDM_URL': 'https://idm-client-test.us-east.philips-healthsuite.com',
    # Production Endpoints
    'IAM_URL': 'https://iam-service.us-east.philips-healthsuite.com/',
    'IDM_URL': 'https://idm-service.us-east.philips-healthsuite.com/',
    'LOGIN_URI': '/security/authentication/token/login',
    'OAUTH2_LOGIN_URI': '/authorize/oauth2/token',
    'OAUTH2_LOGIN_INFO': 'authorize/oauth2/userinfo',
    'NEW_USER_URI': '/authorize/identity/User',
    'RECOVER_PASSWORD': '/authorize/identity/User/$recover-password',
    # 'RECOVER_PASSWORD': '/authorize/identity/User/$reset-password',
    'LOGOUT_USER_URI': '/authorize/oauth2/revoke',
    'UUID_URI': '/security/users?loginId=',
    'USER_INFO_URI': '/security/users/',
    'CREATE_GROUP': '/authorize/identity/Group',
    'ASSIGN_NUMBERS_TO_GROUP': '/authorize/identity/Group/{group_id}/$add-members',
    'ROLES': '/authorize/identity/Role',
    'REMOVE_MEMBERS_FROM_GROUP': '/authorize/identity/Group/{group_id}/$remove-members',
}

UNREACHABLE_SERVICES = ['Product__IDM__Nebuchadnezzar__Alive__Status',
                        'Administrative__Philips__Host__Reachability__Status']
TOKEN_TIMEOUT = 300 #Number in seconds
TOKEN_SECRET = 'secret'
PROXY_FLAG = False
HTTP_PROXY  = "http://apac.zscaler.philips.com:9480"
HTTPS_PROXY = "https://apac.zscaler.philips.com:9480"
IDM_USER_CREATE = 201
IDM_USER_EXISTS = 200
# Development admin details
# ADMIN_USER_ID = 'bWFsYXRlc2gubXlsYXJhcHBhY2hhckBwaGlsaXBzLmNvbQ=='
# ADMIN_USER_PASS = 'UGhpbGlwc0AxMDAx'
# Production admin details
ADMIN_USER_ID = 'bmF2ZWVuLnBlbnVtYWxhQHBoaWxpcHMuY29t'
ADMIN_USER_PASS = 'UGhpbGlwc0A0NTY='
FERNET_SECRET = '-BEhr4qfb-GMRMZU1YK-Q6ifPIrQkKstA5cHbxHaF9M='
TOKEN_REFRESH_PEROID = 300
FORBIDDEN_STATUS_CODE = 401  # Status code for expired request
IGNORE_URI_PATHS = ['/iam_user/login', '/iam_user/create', '/iam_user/reset', '/groups/']
AUTH_HEADER = 'Authorization'
VALID_AUTH_HEADERS = ['X-Auth-Valid', 'X-Requested-With']
NOTES_MAXIMUM_LENGTH = 256
NOTES_MAXIMUM_COUNT = 10
NOTES_MAXIMUM_COUNT_ERROR_CODE = 601
NOTES_MAXIMUM_LENGTH_ERROR_CODE = 602
OK_STATUS_CODE = 200
IGNORE_VIEW_IDS = ['OVERVIEW']


IDM_DEFAULT_GROUPS = ['IDM_Portal_Admin', 'Geo_Rules_Admin', 'Monitoring', 'Software_Management', 'Lhost_Management', 'Site_Admin']
IDM_DEFAULT_ROLES = { 'IDM_Portal_Admin': 'Admin', 'Geo_Rules_Admin': 'Geo_Rules', 'Monitoring': 'Monitoring', 
'Software_Management' : 'SWD', 'Lhost_Management': 'Lhost','Site_Admin':'Site_Admin'}
ID_ALREADY_EXIST = 501
ID_DOES_NOT_EXIST = 502
UNAUTHORIZED_USER = 503
FILE_NOT_FOUND = 404

IBC = {
    'SVN_URL': 'http://sc.phim.isyntax.net/',
    'SITES_URL': 'svn/ibc/sites/',
    'MAIN_YML_URL': 'svn/ibc/common/ansible/roles/shinken/defaults/main.yml',
    'SVN_USERNAME': 'operator',
    'SVN_PASSWORD': 'st3nt0r',
}
LHOST_IGNORE_KEYS = ['password']
ONE_EMS_SERVICE = {
    # 'HOST': 'http://161.85.111.192/',
    'HOST': 'http://10.2.8.110/',
    'URI': 'IDMPortalServices/api/OneEMS/'
}

MEMORY_VALUES_DICT = {'bytes': 0, 'kb': 3, 'mb': 6, 'gb': 9, 'tb': 12, 'pb': 15}
#Directory Constraints
DIRECTORY_MAX_ELEMENTS = 20
DIRECTORY_MAX_ELEMENTS_LENGTH_EXCEED = 205
SITE_INFO_MODULE_TYPES= {
    'Database': 'DBNode',
    'Hl7': 'HL7Node'
}
ACTION_TYPE_MAPPING = {
    'Case': {'case_number': {'$exists': True, '$ne': 'ACK'}},
    'ACK': {'case_number': 'ACK'},
    'Noaction': {'case_number': {'$exists': False}}
}

MODULUS = "qyIzgT55wLAtrn1SvVjikp0ZNz80BfQ8XL/CIC4xWJesYr2AxjlKfFkBtuFQrk6W/PujNWzsQy88FfQ2RWdmBwhAhYOsgt+lyIK+DxWKwmkpkxF2OpDuiOhCf3P/B2h0dD8MLGXCHDLopDs+1RckK8D/mr6zq3qTJFh9pSNTAqkIu4EZBy5wyj8Es2H+pKbgD36KOnvo5bdNyAY+KvVafVMokSIQc4sK9lLLxLhbI2mQ1OqF1pgIxd8Ghu2x48/saqGmbSwgudb/7wKwoePyhqBzgvqYrqZ79iVJVTvSow0tfs4SimNx27bgR2Hv7EII/TwHj95Rp/oSwkkPH1gBjw=="
EXPONENT = "AQAB"
P = "6pBW+Iw9tQtS+72/QOgVZ7qNyJZ3+pNWWwd+Hp6hPNR4gkmo9lyYHTWDDO3FL2IR3djWPO5Cjs2uOhvPjW9IGMeHqm7DCiKgWeCfIMNvkPugkY8u3grGl7ICk9c28bUker7gvJxfU2sACAYCe5k7PDS0mzY1lFrppEcrUmsnYrE="
Q = "usXmi8D249yJ4KPizy63OAyeC4qTe34nY28JLUmkz4SE9k+3vrJQGVqXexaiNHPtNAA1/8m90je1YEL+152h22TWSVKuWA/yguudGgCP/NqTcM+FZI+HDDWP3mA7yn7OgYQ2XLV6HzqD0z3Tl453m08Fye1KRw64s8UerXgQOD8="
DP = "uqATAngaVKRo5vCmbY76EOjGDDVjylNV3uFMQDJ4GPz30LmonDM5t+uulCfNvpBppLf0ZNAw1ovhNkyQS3ihxU1jrH+p4LOCH2BvW1MDvjfmHknswS+TqHgyQsx148P6/UpHZumHkshMYxXc/dgB4DoTBHcSFDJtOI6DcnTdBlE="
DQ = "fVCzMymxfYlHaVYMo/4wKIcus4ezFLS8MNcbTSuQTTwZCjrKikSRwbEq2geBpbfye2V41brChf2i0Iei/YBiTRYHQQMcvzFUAcuciQm/kmQFcTeLxMEYV3TIpufiticG35eZ8fJBkhpswJtHEPUiAy6uL6HqqGqUOuoTf0iDeXk="
INVERSE_Q = "h9bvHO9muV6IF/MVwDJtAvzke4LU34jeIc4fbJ5ZSHfYPkL0DDhiSVhc0j6kvMgvRhOMaz+9d1pS8PGbstPJT9v3ieHZ2Xirkpl3Ib6mvcdxCgsCvFuW4jHOZPcgcAzur3803eHuZJUqzKpMl2viF0NjUCRCQ+uDMR9EFa2poOk="
D = "dsaAFN31xwUAT7Dvsjd02elrTOrzAobnNkBpTciBu0lZ/hYkwG+XAQMNq45qXvBw3xkPx87XlDG+dEZRPI5tQ8g4b/JZ02PCPo5vtyxuaExw7IAcHL3msOLy9F7fzJZ07RGtM0tVFBlJWmM8Us2C71M+lHpzXTrv0TQR0IHZ5WekFu521TVQ3vDdpw+dVJr8LrYpaTKgoCdrD1cacaoWhCVswnpTIl/ZJzE55MrNOdAGXm2DNlZvuapwANDoleZ7C0wVQBeWkW97K3Gu8NjKAaDiThj7OZgILOVBpIyio238acOu6cztcwKqIjH6gfoYjoqo3qoZS1+8WQCz5DlgwQ=="
