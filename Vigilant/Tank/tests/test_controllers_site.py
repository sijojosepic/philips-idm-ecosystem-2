import unittest
from mock import patch, MagicMock, call

class TankSiteControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.site': self.mock_tank.models.site,
            'tank.models.site.SiteModel': self.mock_tank.models.site.SiteModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import site
        self.site = site
        self.obj = self.mock_tank.models.site
        self.mock_tank.models.site.SiteModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.site.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_sites_details(self):
        self.obj.get_sites.return_value = "value"
        self.assertEqual(self.site.get_sites_details('abc','site','country'), "value")

    def test_get_all_siteids(self): 
        self.obj.get_siteids.return_value = "value"
        self.assertEqual(self.site.get_all_siteids('abc'), "value")

    def test_get_subscription_candidates(self):
        self.obj.get_subscription_candidates.return_value = "value"
        self.assertEqual(self.site.get_subscription_candidates('abc','subs'), "value")

    def test_get_sites_facts_details(self):
        self.obj.get_facts_hosts_details.return_value = "value"
        self.assertEqual(self.site.get_sites_facts_details('abc'), "value")

    def test_update_site(self):
        self.obj.update_site.return_value = "value"
        self.assertEqual(self.site.update_site('abc','site'), "value")

    def test_delete_site(self): 
        self.obj.delete_site.return_value = "value"
        self.assertEqual(self.site.delete_site('abc','site'), "value")

    def test_get_site_count(self): 
        self.obj.get_site_count.return_value = "value"
        self.assertEqual(self.site.get_site_count('abc'), "value")

    def test_scanner_info(self):
        self.obj.scanner_info.return_value = "value"
        self.assertEqual(self.site.scanner_info('abc','site'), "value")

    def test_update_lhost(self): 
        self.obj.update_lhost.return_value = "value"
        self.assertEqual(self.site.update_lhost('abc'), "value")
    def test_custom_encrypt(self):
        self.obj.custom_encrypt.return_value = "value"
        self.assertEqual(self.site.custom_encrypt('abc'),"value")

if __name__ == '__main__':
    unittest.main()
