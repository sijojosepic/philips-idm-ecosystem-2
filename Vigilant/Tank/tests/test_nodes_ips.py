from unittest import TestCase
from mock import MagicMock, patch
from tank.tnconfig import OK_STATUS_CODE, ID_ALREADY_EXIST

class TankNodesIPInventoryModelTestCase(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.mock_tank = MagicMock(name="tank")
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.mock_tank.util.normalized_value =  lambda value: value \
            if value not in ['null', '', None] else 'NA'
        self.module_patcher.start()

        from tank.models import nodes_ips
        self.nodes_ips = nodes_ips
        self.nodes_ips.COLLECTIONS = {'GROUPS': 'groups', 'FACTS': 'facts', 'IP_STATUS': 'ip_status'}

        self.mongodb = MagicMock(name="mongo_db")
        self.model = nodes_ips.NodesIPInventoryModel(self.mongodb)

        self.model.siteid = 'ABA01'

        self.triplet_group_name_dict = {'triplet': ['172.16.12'], 'name': 'group_test'}

        self.physical_node_data = {"_id": "ABA01-172.16.12.20", "hostname": "172.16.12.20", "product_id": "null",
                                   "ESXi": {"endpoint": {"scanner": "vCenter", "address": "172.16.12.224"},
                                            "tags": ["vCenter"]},
                                   "product_version": "5.0.0", "siteid": "ABA01", "address": "172.16.12.20",
                                   "model": "IBM System x3650 M4: -[7915B2G]-", "product_name": "NA",
                                   "manufacturer": "IBM",
                                   "endpoints": [{"scanner": "vCenter", "address": "172.16.12.224"}],
                                   "modules": ["ESXi"], "Components": [], "servicetag": "06CWXFM", "role": "NA"}

        self.virtual_node_data = {'Components': [{'name': 'Windows', 'timestamp': "ISODate('2018-01-24T17:48:46.906Z')",
                                                  'version': 'Microsoft Windows Server 2008 R2 Standard , Service Pack 1'},
                                                 {'name': 'IntelliSpace PACS',
                                                  'timestamp': "ISODate('2017-10-05T18:38:16.190Z')",
                                                  'version': '4,4,543,1'}],
                                  'ISP': {'endpoint': {'address': '172.16.12.192', 'scanner': 'ISP'},
                                          'identifier': '4x', 'input_folder': 'S:\\Stentor\\input\\',
                                          'module_type': 128, 'tags': ['production'], 'version': '4,4,543,1'},
                                  'Windows': {'endpoint': {'address': '172.16.12.192', 'scanner': 'ISP'},
                                              'tags': ['production']}, '_id': 'ABA01-aba00if1.aba00.isyntax.net',
                                  'address': '172.16.12.192',
                                  'endpoints': [{'address': '172.16.12.192', 'scanner': 'ISP'}],
                                  'hostname': 'aba00if1.aba00.isyntax.net', 'modules': ['Windows', 'ISP'],
                                  'product_id': 'ISPACS', 'product_name': 'IntelliSpace PACS',
                                  'product_version': '4,4,543,1', 'role': 'NA', 'siteid': 'ABA01'}

        self.untagged_node_data = {'Components': [{'name': 'Windows', 'timestamp': "ISODate('2018-01-24T17:48:46.906Z')",
                                                  'version': 'Microsoft Windows Server 2008 R2 Standard , Service Pack 1'},
                                                 {'name': 'IntelliSpace PACS',
                                                  'timestamp': "ISODate('2017-10-05T18:38:16.190Z')",
                                                  'version': '4,4,543,1'}],
                                  'ISP': {'endpoint': {'address': '172.16.12.235', 'scanner': 'ISP'},
                                          'identifier': '4x', 'input_folder': 'S:\\Stentor\\input\\',
                                          'module_type': 128, 'tags': ['production'], 'version': '4,4,543,1'},
                                  'Windows': {'endpoint': {'address': '172.16.12.235', 'scanner': 'ISP'},
                                              'tags': ['production']}, '_id': 'ABA01-aba00if1.aba00.isyntax.net',
                                  'address': '172.16.12.235',
                                  'endpoints': [{'address': '172.16.12.235', 'scanner': 'ISP'}],
                                  'hostname': 'aba00if1.aba00.isyntax.net', 'modules': ['Windows', 'ISP'],
                                  'product_id': 'ISPACS', 'product_name': 'IntelliSpace PACS',
                                  'product_version': '4,4,543,1', 'role': 'NA', 'siteid': 'ABA01'}

        self.vcenter_node_data = {"vCenter" : {
        "datacenter" : {
            "datacenter-2" : {
                "cluster" : {
                    "domain-c9" : {
                        "host" : {
                            "host-23" : {
                                "product" : {
                                    "osType" : "vmnix-x86",
                                    "fullName" : "VMware ESXi 6.0.0 build-2809209",
                                    "version" : "6.0.0",
                                    "vendor" : "VMware, Inc."
                                },
                                "name" : "172.16.12.246",
                                "hardware" : {
                                    "numCpuThreads" : 24,
                                    "numCpuPkgs" : 2,
                                    "vendor" : "HP",
                                    "cpuModel" : "Intel(R) Xeon(R) CPU E5-2620 v3 @ 2.40GHz",
                                    "numHBAs" : 1,
                                    "memorySize" : "63.9GB",
                                    "numCpuCores" : 12,
                                    "model" : "ProLiant DL380 Gen9",
                                    "cpuMhz" : 2397,
                                    "numNics" : 4,
                                    "uuid" : "30393137-3136-5a43-3336-343631525253"
                                },
                                "datastore" : {
                                    "ABA00l01vol01m" : {
                                        "freeSpace" : "11.6TB",
                                        "capacity" : "30.0TB",
                                        "type" : "VMFS"
                                    }
                                },
                                "runtime" : {
                                    "connectionState" : "connected",
                                    "inMaintenanceMode" : False
                                },
                                "vms" : [
                                    {
                                        "powerState" : "poweredOn",
                                        "guest" : {
                                            "guestFamily" : "windowsGuest",
                                            "toolsStatus" : "toolsOk",
                                            "hostName" : "ABA00M03.ABA00.isyntax.net",
                                            "guestId" : "windows7Server64Guest",
                                            "toolsVersion" : "9537",
                                            "ipAddress" : "172.16.12.192",
                                            "guestState" : "running",
                                            "guestFullName" : "Microsoft Windows Server 2008 R2 (64-bit)"
                                        },
                                        "numEthernetCards" : 1,
                                        "numCpu" : 2,
                                        "nics" : {
                                            "Network adapter 1" : {
                                                "macAddress" : "00:50:56:b5:1e:c7",
                                                "type" : "VirtualVmxnet3",
                                                "summary" : "VLAN10"
                                            }
                                        },
                                        "disks" : {
                                            "virtual_disk4" : {
                                                "freeSpace" : "62.3GB",
                                                "diskPath" : "S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\",
                                                "capacity" : "1.9TB"
                                            },
                                            "virtual_disk5" : {
                                                "freeSpace" : "62.3GB",
                                                "diskPath" : "S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\",
                                                "capacity" : "1.9PB"
                                            },
                                            "virtual_disk6" : {
                                                "freeSpace" : "",
                                                "diskPath" : "S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\",
                                                "capacity" : "1.9MB"
                                            },
                                        },
                                        "connectionState" : "connected",
                                        "memorySizeMB" : 4096,
                                        "guestId" : "windows7Server64Guest",
                                        "guestFullName" : "Microsoft Windows Server 2008 R2 (64-bit)",
                                        "overallStatus" : "green",
                                        "numVirtualDisks" : 6,
                                        "name" : "ABA00M03"
                                    },
                                    {
                                        "powerState" : "poweredOff",
                                        "name" : "ISPORTAL3TST1-down1",
                                        "numEthernetCards" : 1,
                                        "numCpu" : 12,
                                        "nics" : [
                                            {
                                                "macAddress" : "00:50:56:9D:10:FB",
                                                "summary" : "VLAN10",
                                                "type" : "VirtualE1000",
                                                "label" : "Network adapter 1"
                                            }
                                        ],
                                        "disks" : [
                                            {
                                                "capacityInKB" : 157286400,
                                                "label" : "Hard disk 1"
                                            },
                                            {
                                                "capacityInKB" : 1258291200,
                                                "label" : "Hard disk 2"
                                            }
                                        ],
                                        "uuid" : "421db191-7620-9bba-d6b4-88a3fbee5481",
                                        "connectionState" : "connected",
                                        "memorySizeMB" : 32768,
                                        "template" : False,
                                        "guestId" : "windows7Server64Guest",
                                        "guestFullName" : "Microsoft Windows Server 2008 R2 (64-bit)",
                                        "overallStatus" : "green",
                                        "numVirtualDisks" : 2,
                                        "guest" : {
                                            "guestFamily" : "windowsGuest",
                                            "toolsStatus" : "toolsNotRunning",
                                            "hostName" : "ISPORTALTST3.ISEE02.BOS02.iSyntax.net",
                                            "disk" : [
                                                {
                                                    "freeSpace" : long(1218216542208),
                                                    "diskPath" : "D:\\",
                                                    "capacity" : long(1288488087552)
                                                },
                                                {
                                                    "freeSpace" : long(117114798080),
                                                    "diskPath" : "C:\\",
                                                    "capacity" : long(160954314752)
                                                }
                                            ],
                                            "guestId" : "windows8Server64Guest",
                                            "net" : [],
                                            "toolsVersion" : "10246",
                                            "guestState" : "notRunning",
                                            "guestFullName" : "Microsoft Windows Server 2012 (64-bit)"
                                        }
                                    }
                                ]
                            }
                        },
                        "name" : "ABA00cluster02"
                    }
                },
                "name" : "ABA00"
            }
        },
        "endpoint" : {
            "scanner" : "vCenter",
            "address" : "172.16.12.224"
        },
        "tags" : [
            "vCenter"
        ]
    }}

    def tearDown(self):
        TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_nodes_with_refresh(self):
        self.model.facts_collection.find.side_effect = [[self.physical_node_data, self.vcenter_node_data, self.virtual_node_data, self.untagged_node_data], [self.triplet_group_name_dict]]
        self.assertEquals(self.model.get_nodes(refresh="True"), {'result': [{'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'version': '5.0.0', 'virtualnodes': [], 'location': 'group_test', 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'productid': 'NA'}, {'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'version': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '4,4,543,1', 'vnodename': 'ABA00M03', 'ram': 4096, 'vproductid': 'ISPACS', 'diskdata': [{'Used': '1.8377TB', 'Total': '1.9TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.8999377PB', 'Total': '1.9PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.9MB', 'Total': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}], 'cores': 2, 'type': 128, 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}, {'vipaddress': 'NA', 'vversion': 'NA', 'vnodename': 'ISPORTAL3TST1-down1', 'ram': 32768, 'vproductid': 'NA', 'diskdata': [{'Used': 'NA', 'Total': '150GB', 'Name': 'Hard disk 1'}, {'Used': 'NA', 'Total': '1200GB', 'Name': 'Hard disk 2'}, {'Used': '67016GB', 'Total': '1228797GB', 'Name': 'D:\\'}, {'Used': '41808GB', 'Total': '153497GB', 'Name': 'C:\\'}], 'cores': 12, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'location': 'All', 'model': 'NA', 'ipaddress': 'NA', 'productid': 'NA'}, {'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'version': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'ram': 'NA', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'location': '', 'model': '', 'ipaddress': '', 'productid': ''}]})

    def test_get_nodes_without_refresh(self):
        self.model.facts_collection.find.side_effect = [[self.physical_node_data, self.vcenter_node_data, self.virtual_node_data, self.untagged_node_data], [self.triplet_group_name_dict]]
        self.assertEquals(self.model.get_nodes(refresh="False"), {'result': [{'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'version': '5.0.0', 'virtualnodes': [], 'location': 'group_test', 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'productid': 'NA'}, {'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'version': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '4,4,543,1', 'vnodename': 'ABA00M03', 'ram': 4096, 'vproductid': 'ISPACS', 'diskdata': [{'Used': '1.8377TB', 'Total': '1.9TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.8999377PB', 'Total': '1.9PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.9MB', 'Total': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}], 'cores': 2, 'type': 128, 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}, {'vipaddress': 'NA', 'vversion': 'NA', 'vnodename': 'ISPORTAL3TST1-down1', 'ram': 32768, 'vproductid': 'NA', 'diskdata': [{'Used': 'NA', 'Total': '150GB', 'Name': 'Hard disk 1'}, {'Used': 'NA', 'Total': '1200GB', 'Name': 'Hard disk 2'}, {'Used': '67016GB', 'Total': '1228797GB', 'Name': 'D:\\'}, {'Used': '41808GB', 'Total': '153497GB', 'Name': 'C:\\'}], 'cores': 12, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'location': 'All', 'model': 'NA', 'ipaddress': 'NA', 'productid': 'NA'}, {'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'version': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'ram': 'NA', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'location': '', 'model': '', 'ipaddress': '', 'productid': ''}]})

    def test_get_nodes_from_cache_without_refresh(self):
        self.model.cached_nodes = {self.model.siteid : [{'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'version': '5.0.0', 'virtualnodes': [], 'location': 'group_test', 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'productid': 'NA'}, {'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'version': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vnodename': 'ABA00M03', 'ram': 4096, 'vproductid': 'windows7Server64Guest', 'diskdata': [{'Used': '1.8377TB', 'Total': '1.9TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.8999377PB', 'Total': '1.9PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.9MB', 'Total': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}], 'cores': 2, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'location': 'NA', 'model': 'NA', 'ipaddress': 'NA', 'productid': 'NA'}, {'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'version': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'ram': 'NA', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'location': '', 'model': '', 'ipaddress': '', 'productid': ''}]}
        self.assertEquals(self.model.get_nodes(refresh="False"), {'result': [{'ipaddress': '172.16.12.20', 'version': '5.0.0', 'vendor': 'IBM', 'nodename': '172.16.12.20', 'virtualnodes': [], 'model': 'IBM System x3650 M4: -[7915B2G]-', 'productid': 'NA', 'type': 'ESXi', 'location': 'All'}, {'ipaddress': 'NA', 'version': 'NA', 'vendor': 'NA', 'nodename': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vproductid': 'windows7Server64Guest', 'vnodename': 'ABA00M03', 'diskdata': [{'Used': '1.8377TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9TB'}, {'Used': '1.8999377PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9PB'}, {'Used': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9MB'}], 'cores': 2, 'ram': 4096, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'model': 'NA', 'productid': 'NA', 'type': 'NA', 'location': 'All'}, {'ipaddress': '', 'version': '', 'vendor': '', 'nodename': 'Untagged Nodes', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vproductid': 'ISPACS', 'vnodename': 'aba00if1.aba00.isyntax.net', 'diskdata': [], 'cores': 'NA', 'ram': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'model': '', 'productid': '', 'type': '', 'location': 'All'}]})

    def test_set_ip_status(self):
        self.model.ip_status_collection.find_one.return_value = {'ip' : '172.16.12.15', 'status': 'reserve'}
        notes_payload = {"status": "reserved", "ip": "172.16.12.15", "message": "test", "siteid": "ABA01",
                         "modified_by": "abc@xyz.com", "group_name": "default"}
        self.assertEquals(self.model.set_ip_status(notes_payload), {'result': ID_ALREADY_EXIST})

        self.model.ip_status_collection.find_one.return_value = {}
        notes_payload = {"status": self.model.reserved_status, "ip": "172.16.12.15", "message": "test", "siteid": "ABA01",
                         "modified_by": "abc@xyz.com", "group_name": "default"}
        self.assertEquals(self.model.set_ip_status(notes_payload), {'result': OK_STATUS_CODE})

        self.model.ip_status_collection.find_one.return_value = {}
        notes_payload = {"status": self.model.available_status, "ip": "172.16.12.15", "message": "test", "siteid": "ABA01",
                         "modified_by": "abc@xyz.com", "group_name": "default"}
        self.assertEquals(self.model.set_ip_status(notes_payload), {'result': OK_STATUS_CODE})

    def test_get_consumed_ips(self):
        self.model.cached_nodes = {self.model.siteid : [{'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'version': '5.0.0', 'virtualnodes': [], 'location': 'group_test', 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'productid': 'NA'}, {'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'version': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vnodename': 'ABA00M03', 'ram': 4096, 'vproductid': 'windows7Server64Guest', 'diskdata': [{'Used': '1.8377TB', 'Total': '1.9TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.8999377PB', 'Total': '1.9PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.9MB', 'Total': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}], 'cores': 2, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'location': 'NA', 'model': 'NA', 'ipaddress': 'NA', 'productid': 'NA'}, {'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'version': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'ram': 'NA', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'location': '', 'model': '', 'ipaddress': '', 'productid': ''}]}
        self.assertEquals(self.model.get_consumed_site_ips(refresh="False"), {'result': [{'ip': '172.16.12.20', 'hostname': '172.16.12.20', 'group_name': 'All'}, {'ip': '172.16.12.192', 'hostname': 'ABA00M03', 'group_name': 'All (virtual)'}, {'ip': '172.16.12.235', 'hostname': 'aba00if1.aba00.isyntax.net', 'group_name': 'All (virtual)'}]})

    def test_get_ips(self):
        self.model.cached_nodes = {self.model.siteid : [{'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'version': '5.0.0', 'virtualnodes': [], 'location': 'group_test', 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'productid': 'NA'}, {'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'version': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vnodename': 'ABA00M03', 'ram': 4096, 'vproductid': 'windows7Server64Guest', 'diskdata': [{'Used': '1.8377TB', 'Total': '1.9TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.8999377PB', 'Total': '1.9PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.9MB', 'Total': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}], 'cores': 2, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'location': 'NA', 'model': 'NA', 'ipaddress': 'NA', 'productid': 'NA'}, {'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'version': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'ram': 'NA', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'location': '', 'model': '', 'ipaddress': '', 'productid': ''}]}
        self.assertEquals(self.model.get_ips(refresh="False"), {'result': [{'additional_ips': []}, {'count': 3, 'ip': '172.X.X.X', 'children': [{'count': 3, 'ip': '172.16.X.X', 'children': [{'count': 3, 'ip': '172.16.12.X', 'children': ['172.16.12.20', '172.16.12.192', '172.16.12.235']}]}]}]})

    def test_get_available_site_ips_from_status_collection(self):
        self.model.cached_nodes = {self.model.siteid : [{'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'version': '5.0.0', 'virtualnodes': [], 'location': 'group_test', 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'productid': 'NA'}, {'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'version': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vnodename': 'ABA00M03', 'ram': 4096, 'vproductid': 'windows7Server64Guest', 'diskdata': [{'Used': '1.8377TB', 'Total': '1.9TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.8999377PB', 'Total': '1.9PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}, {'Used': '1.9MB', 'Total': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\'}], 'cores': 2, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'location': 'NA', 'model': 'NA', 'ipaddress': 'NA', 'productid': 'NA'}, {'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'version': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'ram': 'NA', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'location': '', 'model': '', 'ipaddress': '', 'productid': ''}]}
        self.model.ip_status_collection.aggregate.side_effect = [[{'ip': '172.16.12.225', 'status': 'reserved', 'group_name': 'test', 'siteid': self.model.siteid, 'message': 'test', 'modified_by': 'raman'}],
                                                                 [{'ip': '172.16.12.233', 'status': 'available', 'group_name': 'test', 'siteid': self.model.siteid, 'message': 'test', 'modified_by': 'raman'}]]
        self.assertEquals(self.model.get_available_site_ips(refresh="False"), {'result': [{'status': 'available', 'modified_by': '', 'ip': '172.16.12.21', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.22', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.23', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.24', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.25', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.26', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.27', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.28', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.29', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.30', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.31', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.32', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.33', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.34', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.35', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.36', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.37', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.38', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.39', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.40', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.41', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.42', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.43', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.44', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.45', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.46', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.47', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.48', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.49', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.50', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.51', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.52', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.53', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.54', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.55', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.56', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.57', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.58', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.59', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.60', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.61', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.62', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.63', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.64', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.65', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.66', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.67', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.68', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.69', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.70', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.71', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.72', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.73', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.74', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.75', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.76', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.77', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.78', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.79', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.80', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.81', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.82', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.83', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.84', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.85', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.86', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.87', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.88', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.89', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.90', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.91', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.92', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.93', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.94', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.95', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.96', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.97', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.98', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.99', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.100', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.101', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.102', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.103', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.104', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.105', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.106', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.107', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.108', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.109', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.110', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.111', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.112', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.113', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.114', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.115', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.116', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.117', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.118', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.119', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.120', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.121', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.122', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.123', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.124', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.125', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.126', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.127', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.128', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.129', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.130', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.131', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.132', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.133', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.134', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.135', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.136', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.137', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.138', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.139', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.140', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.141', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.142', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.143', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.144', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.145', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.146', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.147', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.148', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.149', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.150', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.151', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.152', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.153', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.154', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.155', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.156', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.157', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.158', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.159', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.160', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.161', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.162', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.163', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.164', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.165', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.166', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.167', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.168', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.169', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.170', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.171', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.172', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.173', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.174', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.175', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.176', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.177', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.178', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.179', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.180', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.181', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.182', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.183', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.184', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.185', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.186', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.187', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.188', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.189', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.190', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.191', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.193', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.194', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.195', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.196', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.197', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.198', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.199', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.200', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.201', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.202', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.203', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.204', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.205', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.206', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.207', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.208', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.209', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.210', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.211', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.212', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.213', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.214', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.215', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.216', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.217', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.218', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.219', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.220', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.221', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.222', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.223', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.224', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.226', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.227', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.228', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.229', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.230', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.231', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.232', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}, {'status': 'available', 'modified_by': 'raman', 'ip': '172.16.12.233', 'group_name': 'All', 'siteid': 'ABA01', 'message': 'test'}, {'status': 'available', 'modified_by': '', 'ip': '172.16.12.234', 'group_name': 'All', 'siteid': 'ABA01', 'message': ''}]})

    def test_get_group_by_ip(self):
        triplet_group_dict = {'192.168.180': {'triplet': ['192.168.180'], 'name': 'test', 'created_by': 'emailuser3', 'siteid': 'ABA01'}}
        self.assertEquals(self.model.get_group_by_ip(triplet_group_dict, '192.168.180.29'), 'test')

    def test_get_triplet_group_from_ips(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test", "triplet": ["192.168.180"], "created_by": "raman"}]
        self.assertEquals(self.model.get_triplet_group_from_ips(['192.168.180.29']), {'192.168.180': {'triplet': ['192.168.180'], 'name': 'test', 'created_by': 'raman', 'siteid': 'ABA01'}})

    def test_updated_nodes_group(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test2", "triplet": ["172.16.12"], "created_by": "raman"}]
        node_data=[{'ipaddress': '172.16.12.20', 'version': '5.0.0', 'vendor': 'IBM', 'nodename': '172.16.12.20', 'virtualnodes': [], 'model': 'IBM System x3650 M4: -[7915B2G]-', 'productid': 'NA', 'type': 'ESXi', 'location': 'group_test'}, {'ipaddress': 'NA', 'version': 'NA', 'vendor': 'NA', 'nodename': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vproductid': 'windows7Server64Guest', 'vnodename': 'ABA00M03', 'diskdata': [{'Used': '1.8377TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9TB'}, {'Used': '1.8999377PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9PB'}, {'Used': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9MB'}], 'cores': 2, 'ram': 4096, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'model': 'NA', 'productid': 'NA', 'type': 'NA', 'location': 'NA'}, {'ipaddress': '', 'version': '', 'vendor': '', 'nodename': 'Untagged Nodes', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vproductid': 'ISPACS', 'vnodename': 'aba00if1.aba00.isyntax.net', 'diskdata': [], 'cores': 'NA', 'ram': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'model': '', 'productid': '', 'type': '', 'location': ''}]
        self.assertEquals(self.model.updated_nodes_group(node_data), [{'version': '5.0.0', 'vendor': 'IBM', 'nodename': '172.16.12.20', 'type': 'ESXi', 'virtualnodes': [], 'model': 'IBM System x3650 M4: -[7915B2G]-', 'ipaddress': '172.16.12.20', 'location': 'test2', 'productid': 'NA'}, {'version': 'NA', 'vendor': 'NA', 'nodename': 'NA', 'type': 'NA', 'virtualnodes': [{'vipaddress': '172.16.12.192', 'vversion': '9537', 'vnodename': 'ABA00M03', 'vproductid': 'windows7Server64Guest', 'diskdata': [{'Used': '1.8377TB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9TB'}, {'Used': '1.8999377PB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9PB'}, {'Used': '1.9MB', 'Name': 'S:\\Philips\\Apps\\iSite\\Stack\\Stack02\\', 'Total': '1.9MB'}], 'cores': 2, 'ram': 4096, 'type': 'NA', 'operatingsystem': 'Microsoft Windows Server 2008 R2 (64-bit)'}], 'model': 'NA', 'ipaddress': 'NA', 'location': 'All', 'productid': 'NA'}, {'version': '', 'vendor': '', 'nodename': 'Untagged Nodes', 'type': '', 'virtualnodes': [{'vipaddress': '172.16.12.235', 'vversion': '4,4,543,1', 'vnodename': 'aba00if1.aba00.isyntax.net', 'vproductid': 'ISPACS', 'diskdata': [], 'cores': 'NA', 'ram': 'NA', 'type': 128, 'operatingsystem': 'Windows'}], 'model': '', 'ipaddress': '', 'location': 'All', 'productid': ''}])

    def test_get_triplet_group_by_ip(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test", "triplet": ["192.168.180"], "created_by": "raman"}]
        self.assertEquals(self.model.get_triplet_group_by_ip('192.168.180.29'), ('192.168.180', 'test', '192.168.180.29'))
