import unittest
from mock import patch, MagicMock


class IAMTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_tank = MagicMock(name='tank')
            self.mock_requests = MagicMock(name='requests')
            self.mock_time = MagicMock(name='time')
            self.mock_base64 = MagicMock(name='base64')
            self.mock_bottle_req = MagicMock(name='request')
            self.mongodb = MagicMock(name='mongo_db')
            self.mock_iam_group_model = MagicMock(name='tank.models.iam_group')
            self.mock_iam_role_model = MagicMock(name='tank.models.iam_roles')
            modules = {
                'tank.util': self.mock_tank.util,
                'tank.tnconfig': self.mock_tank.tnconfig,
                'requests': self.mock_requests,
                'time': self.mock_time,
                'base64': self.mock_base64,
                'request': self.mock_bottle_req,
                'tank.models.iam_group': self.mock_iam_group_model,
                'tank.models.iam_roles': self.mock_iam_role_model
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            from tank.controllers import iam_user
            self.iam_user = iam_user

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class IAMAccessorTestCase(IAMTest.TestCase):
    def setUp(self):
        IAMTest.TestCase.setUp(self)
        self.iam_accessor = self.iam_user.IAMAccessor()
        self.set_user_mock_objects()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.mock_bottle_req.reset_mock()
        self.iam_accessor.request.reset_mock()
        self.module_patcher.stop()

    def set_user_mock_objects(self):
        req_query = {"name": "test"}
        self.iam_accessor.url = 'url'
        self.iam_accessor.uri_path = ''
        self.iam_accessor.json = {'key': 'value'}
        self.iam_accessor.headers = {'auth': 'BASIC_AUTH', 'accept': 'json'}
        self.iam_accessor.sig_hdr = 'HmacSHA256;Credential:decrypted;SignedHeaders:SignedDate;Signature:encrypted'
        self.iam_accessor.get_headers = MagicMock(name='get_headers', return_value=self.iam_accessor.headers)
        self.iam_accessor.get_proxy = MagicMock(return_value={})
        self.iam_accessor.get_url = MagicMock(return_value=self.iam_accessor.url)
        self.iam_accessor.get_params = MagicMock(return_value=req_query)
        self.iam_accessor.request = self.mock_bottle_req

    def test_get(self):
        self.mock_bottle_req.get.json.return_value = self.iam_accessor.json
        self.iam_accessor.get()
        self.mock_bottle_req.get.assert_called_once_with(self.iam_accessor.url,
                                                         headers=self.iam_accessor.get_headers.return_value,
                                                         params=self.iam_accessor.get_params.return_value, proxies={})
        self.assertEqual(self.mock_bottle_req.get.json.return_value, self.iam_accessor.json)
        self.assertEqual(self.iam_accessor.url, self.iam_accessor.get_url())

    def test_post(self):
        self.mock_bottle_req.post.json.return_value = self.iam_accessor.json
        jsn_req = {'payload': 'json'}
        self.iam_accessor.post(self.mongodb, **jsn_req)
        self.mock_bottle_req.post.assert_called_once_with(self.iam_accessor.url, json=self.iam_accessor.json,
                                                          headers=self.iam_accessor.get_headers.return_value,
                                                          proxies={})
        self.assertEqual(self.mock_bottle_req.post.json.return_value, self.iam_accessor.json)
        self.assertEqual(self.iam_accessor.url, self.iam_accessor.get_url())

    def test_post_as_return_type_test(self):
        self.mock_bottle_req.post.json.return_value = self.iam_accessor.json
        jsn_req = {'payload': 'text'}
        self.iam_accessor.post(self.mongodb, **jsn_req)
        self.mock_bottle_req.post.assert_called_once_with(self.iam_accessor.url, data='key=value',
                                                          headers=self.iam_accessor.get_headers.return_value,
                                                          proxies={})
        self.assertEqual(self.mock_bottle_req.post.json.return_value, self.iam_accessor.json)
        self.assertEqual(self.iam_accessor.url, self.iam_accessor.get_url())

    def test_put(self):
        self.mock_bottle_req.put.json.return_value = self.iam_accessor.json
        self.iam_accessor.put()
        self.mock_bottle_req.put.assert_called_once_with(self.iam_accessor.url, json=self.iam_accessor.json,
                                                         headers=self.iam_accessor.headers,
                                                         proxies={})
        self.assertEqual(self.mock_bottle_req.put.json.return_value, self.iam_accessor.json)

    def test_sig_header(self):
        self.mock_time.strftime.return_value = 'DATE_FORMATTED_VAL'
        self.mock_base64.b64encode.return_value = 'encrypted'
        self.mock_tank.util.FernetCrypto.decrypt.return_value = 'decrypted'
        self.assertEqual(self.iam_accessor.sig_header(), self.iam_accessor.sig_hdr)


class SignInTestCase(IAMTest.TestCase):
    def setUp(self):
        IAMTest.TestCase.setUp(self)
        self.iam_signin = self.iam_user.SignIn({})

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_routes_info_unapproved_user(self):
        user_group_ids = ['G1', 'G2']
        approval = False
        self.mock_iam_group_model.IAMGroupModel().get_group_by_id.side_effect = [{'result': [{'_id': 'G1', 'roles': ['R1'],
            'restricted': False}]}, {'result': [{'_id': 'G2', 'roles': ['R2'], 'restricted': True}]}]
        self.mock_iam_role_model.IAMRoleModel().get_roles_by_id.return_value = {'result': [{'_id': 'R1', 'views':
            ['V1', 'V2']}]}
        self.iam_signin.get_routes_info(self.mongodb, user_group_ids, approval)
        self.assertEqual(self.iam_signin.role_ids, ['R1'])
        self.assertEqual(self.iam_signin.view_ids, ['V1', 'V2'])

    def test_get_routes_info_approved_user(self):
        user_group_ids = ['G1', 'G2']
        approval = True
        self.mock_iam_group_model.IAMGroupModel().get_group_by_id.side_effect = [{'result': [{'_id': 'G1', 'roles': ['R1'],
            'restricted': False}]}, {'result': [{'_id': 'G2', 'roles': ['R2'], 'restricted': True}]}]
        self.mock_iam_role_model.IAMRoleModel().get_roles_by_id.side_effect = [{'result': [{'_id': 'R1', 'views':
            ['V1', 'V2']}]}, {'result': [{'_id': 'R1', 'views': ['V3', 'V4']}]}]
        self.iam_signin.get_routes_info(self.mongodb, user_group_ids, approval)
        self.assertEqual(self.iam_signin.role_ids, ['R1', 'R2'])
        self.assertEqual(self.iam_signin.view_ids, ['V1', 'V2', 'V3', 'V4'])

    def test_get_role_names(self):
        user_group_ids = ['G1', 'G2']
        self.mock_iam_group_model.IAMGroupModel().get_group_by_id.side_effect = [{'result': [{'_id': 'G1','name':'N1', 'roles': ['R1'],
            'restricted': False}]}, {'result': [{'_id': 'G2','name':'N2', 'roles': ['R2'], 'restricted': True}]}]
        self.assertEqual(self.iam_signin.get_role_names(self.mongodb, user_group_ids), ['N1', 'N2'])

class GroupsTestCase(IAMTest.TestCase):
    def setUp(self):
        IAMTest.TestCase.setUp(self)
        self.groupObj = self.iam_user.Groups(self.mock_bottle_req)
        self.groupObj.get_common_header = MagicMock(return_value={'header': 'header'})
        self.groupObj.get_payload = MagicMock(return_value={
            "resourceType": "Parameters", "parameter": [
                {
                    "name": "UserIDCollection",
                    "references": [
                        {
                            "reference": 'userUid'
                        }
                    ]
                }
            ]
        })
        self.mongodb = MagicMock(name='mongo_db')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_create_default_groups(self):
        self.iam_user.IDM_DEFAULT_GROUPS = ['G1', 'G2']
        self.groupObj.create_default_groups(self.mongodb)
        self.groupObj.request.post.return_value = [{"name": "N"}]
        self.assertEqual(self.groupObj.request.post.called, True)

    def test_assign_members_to_group(self):
        self.groupObj.uri_path = MagicMock(side_effect=['GRP1/$add-members'])
        self.groupObj.assign_members_to_group('userUid', ['GRP1'])
        self.assertEqual(self.groupObj.get_payload(), self.groupObj.payload)

    def test_remove_members(self):
        self.mock_bottle_req.json = MagicMock(name='requests.json')
        self.mock_bottle_req.json.get.return_value = 'userUid'
        self.groupObj.uri_path = MagicMock(side_effect=['GRP1/$remove-members'])
        self.groupObj.remove_members(self.mongodb)
        self.assertEqual(self.groupObj.get_payload(), self.groupObj.payload)


if __name__ == '__main__':
    unittest.main()