import re
import unittest
from mock import patch, MagicMock, call, patch, PropertyMock,mock_open


class TankSiteModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        self.mock_cached_property = MagicMock(name='cached_property', cached_property=property)
        self.mock_py = MagicMock(name='py')
        self.mock_temporary = MagicMock(name='temporary')
        self.mock_yaml = MagicMock(name='yaml')
        self.mock_time = MagicMock(name='time')
        modules = {
            'cached_property': self.mock_cached_property,
            'tank.util': self.mock_tank.util,
            'temporary': self.mock_temporary,
            'time': self.mock_time,
            'py': self.mock_py,
            'yaml': self.mock_yaml,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import site
        self.site = site
        self.site.COLLECTIONS = {'SITES': 'sites'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.site.SiteModel(self.mongodb)

        self.patch_wc = patch('tank.models.site.SiteModel.wc', new_callable=PropertyMock)
        self.wc_mock = self.patch_wc.start()
        self.patch_wc_co = patch('tank.models.site.SiteModel.wc_co', new_callable=PropertyMock)
        self.wc_co_mock = self.patch_wc_co.start()
        self.patch_wc2 = patch('tank.models.site.SiteModel.wc2', new_callable=PropertyMock) 
        self.wc2_mock = self.patch_wc2.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.patch_wc.stop()
        self.module_patcher.stop()

    def test_get_siteids(self):
        self.model.collection.distinct.return_value = iter(['ABC00', 'YXR44'])
        self.assertEqual(self.model.get_siteids(), {'siteids': ['ABC00', 'YXR44']})
        self.model.collection.distinct.assert_called_once_with('_id',{'_id':{'$nin': [None]}})

    def test_set_siteid_filter(self):
        self.model.set_siteid_filter(['site1', 'site2'])
        self.assertEqual(self.model.filter, {'$and': [{'_id': {'$nin': [None]}}, {'_id': {'$in': ['site1', 'site2']}}]})

    def test_set_siteid_filter_none(self):
        self.model.set_siteid_filter(None)
        self.assertEqual(self.model.filter, {'_id': {'$nin': [None]}})

    def test_set_country_filter(self):
        self.model.set_country_filter(['usa', 'mx'])
        self.assertEqual(self.model.filter, {'country.country': {'$in': ['usa', 'mx']}})

    def test_set_country_filter_none(self):
        self.model.set_country_filter(None)
        self.assertEqual(self.model.filter, {})

    def test_get_site_names(self):
        self.model.set_siteid_filter = MagicMock(name='get_siteid_filter')
        self.model.collection.find.return_value = [
            {'_id': 'site1', 'name': 'the first site'},
            {'_id': 'site2', 'name': 'the second site'},
        ]
        self.assertEqual(
            self.model.get_site_names(['ABC01', 'XYZ00']),
            {'site2': 'the second site', 'site1': 'the first site'}
        )
        self.model.set_siteid_filter.assert_called_once_with(['ABC01', 'XYZ00'])
        self.model.collection.find.assert_called_once_with(self.model.filter, {'name': 1})

    def test_get_sites(self):
        self.model.set_siteid_filter = MagicMock(name='get_siteid_filter')
        self.model.set_country_filter = MagicMock(name='set_country_filter')
        self.model.collection.find.return_value = [{'_id': 'one', 'siteid': 'one'}, {'_id': 'two', 'siteid': 'two'}]
        self.model.rename_id = MagicMock(name='rename_id')
        self.assertEqual(
            self.model.get_sites(['ABC01', 'XYZ00'], ['usa']),
            {'sites': [self.site.shift_key.return_value, self.site.shift_key.return_value]}
        )
        self.model.set_siteid_filter.assert_called_once_with(['ABC01', 'XYZ00'])
        self.model.set_country_filter.assert_called_once_with(['usa'])
        self.model.collection.find.assert_called_once_with(self.model.filter, {'Components': 0})
        self.assertEqual(self.site.rename_id.mock_calls, [call('siteid', {'_id': 'one', 'siteid': 'one'}),
                                                          call('siteid', {'_id': 'two', 'siteid': 'two'})])

    def test_get_subscription_candidates(self):
        self.site.SubscriptionModel = MagicMock(name='SubscriptionModel')
        self.site.SubscriptionModel.return_value.get_subscription.return_value = {
            'countries': ['usa', 'mx'], 'name': 'av-1.4'
        }
        self.model.get_sites = MagicMock(name='get_sites')
        self.assertEqual(self.model.get_subscription_candidates('av-1.4'), self.model.get_sites.return_value)
        self.model.get_sites.assert_called_once_with(countries=['usa', 'mx'])

    def test_update_site(self):
        self.model.update_site(siteid='SS', name='XX', country={'country': 'CC', 'market': 'MM'})
        self.model.collection.update_one.assert_called_once_with(
            {'_id': 'SS'},
            {'$set': {'country': {'country': 'CC', 'market': 'MM'}, 'name': 'XX'}},
            upsert=False
        )

    def test_delete_site(self):
        self.model.delete_site('SS')
        self.model.collection.delete_one.assert_called_once_with({'_id': 'SS'})

    def test_get_facts_hosts_details(self):
        self.model.get_sites = MagicMock(name='get_sites')
        self.model.get_sites.return_value = {'sites': [
            {'name': 'site1', 'pacs_version': '1',
             'country': {'country': 'country1', 'market': 'market1'}, 'siteid': 'S1'},
            {'name': 'site2', 'pacs_version': '1',
             'country': {'country': 'country2', 'market': 'market2'}, 'siteid': 'S2'},
            {'name': 'site3', 'pacs_version': '1',
             'country': {'country': 'country3', 'market': 'market3'}, 'siteid': 'S3'}
        ]}
        self.site.DashboardModel = MagicMock(name='DashboardModel')
        self.site.DashboardModel.return_value.get_matches.return_value = {'result': [{'siteid': 'S3',
                                                                                      'name': 'site3',
                                                                                      'pacs_version': '1'},
                                                                                     ]}
        self.site.RulesModel = MagicMock(name='RulesModel')
        self.site.RulesModel.return_value.get_rule_list.return_value = {'sites': ['S2', 'S4']}
        self.site.RulesModel.return_value.get_rules_with_siteid.return_value = {'result': [{ 'remark': 'upgrade',
                                                                                             'siteid': 'S2',
                                                                                             'start_time': '2018/08/08',
                                                                                             'end_time': '2018/08/15'}]}
        self.assertEqual(self.model.get_facts_hosts_details(), {'result': [{'country': {'country': 'country1',
                                                                                        'market': 'market1'},
                                                                            'siteid': 'S1',
                                                                            'name': 'site1',
                                                                            'pacs_version': '1',
                                                                            'status': 'Up'},
                                                                           {'country': {'country': 'country2',
                                                                                        'market': 'market2'},
                                                                            'siteid': 'S2',
                                                                            'name': 'site2',
                                                                            'pacs_version': '1',
                                                                            'status': 'Maintenance',
                                                                            'maintenance_rule': [{
                                                                                'remark': 'upgrade',
                                                                                'siteid': 'S2',
                                                                                'start_time': '2018/08/08',
                                                                                'end_time': '2018/08/15'}]},
                                                                           {'country': {'country': 'country3',
                                                                                        'market': 'market3'},
                                                                            'siteid': 'S3',
                                                                            'name': 'site3',
                                                                            'pacs_version': '1',
                                                                            'status': 'Down'}
                                                                           ]})

    def test_get_host_count(self):
        vCenters = [{"vms": [
            {
                "guest": {
                    "hostName": 'host1',
                }
            },
            {
                "guest": {
                    "hostName": 'Host2',
                }
            },
            {
                "guest": {
                    "hostName": "localhost",
                }
            }
        ]}]
        localhost_regex = re.compile('localhost')
        hostnames = ['host2']
        self.assertEqual(self.model.get_host_count(vCenters, localhost_regex, hostnames), 1)

    def test_get_site_count(self):
        self.model.get_site_count()
        self.model.collection.distinct.assert_called_once_with('_id',{'_id':{'$nin': [None]},'production':{'$ne': 'False'}})

    @patch('__builtin__.open')
    def test_scanner_info_get_right_contents(self, mopen):
        self.mock_temporary.temp_dir.return_value.__enter__ = MagicMock(return_value='/tmp')
        self.mock_temporary.temp_dir.return_value.__exit__ = MagicMock(return_value=False)
        self.mock_yaml.load.return_value = {'a': 1, 'b': {'c': 3}}
        document = """
            a: 1
            b:
              c: 3
        """
        mopen.return_value.__enter__ = MagicMock(return_value=document)
        mopen.__exit__ = MagicMock(return_value=False)
        lhost = self.model.scanner_info('ABC12')
        self.wc_mock.return_value.export.assert_called_once_with('/tmp/lhost.yml')
        self.mock_yaml.load.assert_called_once_with(document)
        self.assertEqual(lhost, {'code': 600, 'contents': self.mock_yaml.load.return_value,
                                 'last_modified_time':self.mock_time.strftime.return_value})

    @patch('__builtin__.open')
    def test_scanner_info_exceptin(self, mopen):
        self.wc_mock.side_effect = Exception
        lhost = self.model.scanner_info('ABC12')
        self.assertEqual(lhost.get('code'), 602)

    @patch('__builtin__.open')
    def test_main_yml(self, mopen):
        self.mock_temporary.temp_dir.return_value.__enter__ = MagicMock(return_value='/tmp')
        self.mock_temporary.temp_dir.return_value.__exit__ = MagicMock(return_value=False)
        self.mock_yaml.load.return_value = {'a': 1, 'b': {'c': 3}}
        main_yaml = self.model.main_yml()
        self.wc2_mock.return_value.export.assert_called_once_with('/tmp/main.yml')
        self.assertEqual(main_yaml, self.mock_yaml.load.return_value)
    
    def test_shinken_refs(self):
        self.model.main_yml = MagicMock(return_value={
            'shinken_initial_resources':{'a':1,'b':2,'c':3},
            'default_threshold_values': {'x':1,'y':2,'z':3}
        })
        self.assertEqual(self.model.shinken_refs(), {'result' :['a', 'c', 'b']})
        self.model.main_yml.assert_called_once_with()
    
    def test_all_threseholds(self):
        self.model.main_yml= MagicMock(return_value={
            'shinken_initial_resources':{'a':1,'b':2,'c':3},
            'default_threshold_values': {'x':1,'y':2,'z':3}
        })
        self.assertEqual(self.model.all_thresholds(), {'result' :{'x':1,'y':2,'z':3}})
        self.model.main_yml.assert_called_once_with()

    def test_update_contents(self):
        content = {"siteid": "SITEID",
                   "fqdn": "fqdn1",
                   "dns_nameservers": ["dns1", "dns2"],
                   "endpoints": [],
                   "user": "user1",
                   "comment": "lhost page changes"
                   }
        self.mock_temporary.temp_dir.return_value.__enter__ = MagicMock(return_value='/tmp')
        self.mock_temporary.temp_dir.return_value.__exit__ = MagicMock(return_value=False)
        self.model.update_lhost(content)
        self.mock_yaml.safe_dump.assert_called_once_with(content, default_flow_style=False)
        self.assertEqual(self.wc_co_mock.return_value.commit.call_count, 1)

    def test_custom_encrypt(self):
        self.mock_tank.util.FernetCrypto.encrypt.return_value = MagicMock(return_value='h78992842384328429423-4924924+')
        self.assertEqual(self.model.custom_encrypt('P@ssw0rd')['result'],'{+'+self.mock_tank.util.FernetCrypto.encrypt.return_value +'+}')

if __name__ == '__main__':
    unittest.main()
