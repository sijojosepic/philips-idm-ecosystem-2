import re,sys
import unittest
from mock import patch, MagicMock


class TankGeoUserAuditModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_jwt = MagicMock(name='jwt')
        self.mock_json = MagicMock(name='json')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_time = MagicMock(name='time')
        modules = {
            'hashlib': self.mock_hashlib,
            'jwt': self.mock_jwt,
            'json': self.mock_json,
            'datetime': self.mock_datetime,
            'time': self.mock_time
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import geo_user_audit
        self.geo_user = geo_user_audit
        self.geo_user.COLLECTIONS = {'USER_AUDIT': 'user_audit'}
        self.geo_user.TOKEN_TIMEOUT = 300
        self.geo_user.TOKEN_SECRET = 'secret'

        self.mongodb = MagicMock(name='mongo_db')
        self.request = {"action": "SWD", "payload": {"TEST": "site1"}}
        self.headers = {"x-token": "12xyz"}
        # self.request.headers['x-token'] = ""
        self.status = {'valid': False}
        self.model = geo_user_audit.GeoUserAuditModel(self.mongodb, self.request, self.headers)
        from datetime import datetime
        import jwt, json
        self.model.datetime = datetime
        self.model.json = json
        self.model.jwt = jwt


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_set_rules_audit(self):
        self.model.decrypt_token = MagicMock(name='decrypt_token')
        self.model.update_audit_request = MagicMock(name='update_audit_request')
        self.model.decrypt_token.return_value = {'timestamp': 1516777888.612, 'key': 'hitcoe@philips.com',
                                              'exp': 1516778188.612}
        self.model.update_audit_request.return_value = None
        self.assertEqual(self.model.set_rules_audit(), {'valid': True})

    def test_decrypt_token(self):
        token = '123xyz'
        self.model.jwt.decode.return_value = {'timestamp': 1516777888.612, 'key': 'hitcoe@philips.com',
                                              'exp': 1516778188.612}
        self.model.json.loads.return_value = {'timestamp': 1516777888.612, 'key': 'hitcoe@philips.com',
                                              'exp': 1516778188.612}
        self.assertEqual(self.model.decrypt_token(token), {'timestamp': 1516777888.612, 'key': 'hitcoe@philips.com',
                                                'exp': 1516778188.612})

    def test_update_audit_request(self):
        self.model.datetime.now.return_value.strftime.return_value = '18-01-23-19-09-06'
        json_str = {}
        json_str['key'] = 'hit@philips.com'
        self.model.update_audit_request(json_str)
        self.model.collection.insert_one.assert_called_once_with( {'action': 'SWD', 'uname': 'hit@philips.com',
                    'payload': {'TEST': 'site1'}, 'request object': {'action': 'SWD', 'payload': {'TEST': 'site1'}},
                                                                   'timestamp': '18-01-23-19-09-06'} )



if __name__ == '__main__':
    unittest.main()