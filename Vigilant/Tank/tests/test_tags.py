import unittest
from mock import patch, MagicMock

class TankTagsModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import tags
        self.tags = tags
        self.tags.COLLECTIONS = {'TAGS': 'tags'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.tags.TagsModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_tags(self):
        self.model.collection.aggregate.return_value = iter(
            [
                {'tag': 'T1', 'description': 'D1'},
                {'tag': 'T2', 'description': 'D2'}
            ]
        )
        self.assertEqual(
            self.model.get_tags(),
            {'result': [
                {'tag': 'T1', 'description': 'D1'},
                {'tag': 'T2', 'description': 'D2'}
            ]}
        )

    def test_save_tag(self):
        self.assertEqual(self.model.save_tag('TAG', 'DESC'), {'result': True})
        self.model.collection.update_one.assert_called_once_with({'_id': 'TAG'}, {'$set': {'description': 'DESC'}}, upsert=True)


if __name__ == '__main__':
    unittest.main()
