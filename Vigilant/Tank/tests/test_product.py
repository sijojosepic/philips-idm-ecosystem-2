import unittest

from mock import patch, MagicMock


class TankProductModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.tnconfig': self.mock_tank.tnconfig}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import product
        self.product = product
        self.ProductModel = product.ProductModel
        self.product.COLLECTIONS = {'PRODUCT': 'product'}


        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.ProductModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_product_value(self):
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.collection.aggregate.return_value = {'value': 100}
        self.assertEqual(
            self.model.get_product_value('ISP'),
            {'result': ['value']}
        )

    def test_get_projection(self):
        self.assertEqual(
            self.model.get_projection(),
            {
                '_id': 0,
                'product_value': '$value',
            }
        )

if __name__ == '__main__':
    unittest.main()
