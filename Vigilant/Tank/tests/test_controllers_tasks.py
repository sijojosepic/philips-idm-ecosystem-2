import unittest
from mock import patch, MagicMock

 

class TankTasksControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_sys = MagicMock(name='sys')
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'sys': self.mock_sys,
            'bottle': self.mock_bottle,
            'bottle.Bottle': self.mock_bottle.Bottle,
            'bottle.request': self.mock_bottle.request,
            'celery': self.mock_celery,
            'celery.Celery': self.mock_celery.Celery,
            'tank.util': self.mock_tank.util,
            'tank.tnconfig': self.mock_tank.tnconfig,
            'tank.tnconfig.CELERY_CONFIGURATION_OBJECT': self.mock_tank.tnconfig.CELERY_CONFIGURATION_OBJECT,
            'tank.tnconfig.CELERY_PATH': self.mock_tank.tnconfig.CELERY_PATH,
            'tank.tnconfig.IN_TASK_NAME': self.mock_tank.tnconfig.IN_TASK_NAME,
            'tank.util.get_logger': self.mock_tank.util.get_logger,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import tasks
        self.tasks = tasks
        
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        obj = MagicMock()
        self.mock_bottle.Bottle.return_value = obj
        self.mock_bottle.Bottle.post.return_value = 'value'
        self.assertEqual(self.tasks.get_app(), obj)

    def test_sp_task_with_if(self):
        self.mock_bottle.request.json = None
        self.tasks.sp_task('resource', 'action')
        self.tasks.log.error.assert_called_with('no json')
        self.assertEqual(self.mock_celery.Celery().send_task.call_count, 0)


    def test_sp_task_with_no_if(self):
        self.mock_bottle.request.json = 'value'
        self.tasks.sp_task('resource', 'action')
        self.tasks.log.error.assert_not_called()
        self.assertEqual(self.mock_celery.Celery().send_task.call_count, 1)

if __name__ == '__main__':
    unittest.main()
