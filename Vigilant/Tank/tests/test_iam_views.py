import unittest
from mock import patch, MagicMock


class TankIAMViewModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_operator = MagicMock(name='operator')
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.util': self.mock_tank.util,
            'operator': self.mock_operator
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import iam_views
        self.views = iam_views
        self.views.COLLECTIONS = {'IAM_VIEWS': 'iam_views'}
        self.mongodb = MagicMock(name='mongodb')
        self.model = self.views.IAMViewModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_projection(self):
        self.assertEqual(
            self.model.get_projection(),
            {
                '_id': 1,
                'view_id': 1,
                'route_link': 1,
                'parent_view_id': 1,
                'route_label': 1,
                'priority': 1
            }
        )

    def test_get_filter(self):
        self.model.filter = {'$in': ['VIEW1', 'VIEW2']}
        self.assertEqual(
            self.model.get_filter(['VIEW1', 'VIEW2']),
            self.model.filter
        )

    def test_get_matches(self):
        self.model.filter = {'$in': ['VIEW1', 'VIEW2']}
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.get_matches(['VIEW1', 'VIEW2'])
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value}
        ])

    def test_get_routes_info(self):
        self.model.get_matches = MagicMock(name='get_matches')
        self.model.get_child_routes_info  = MagicMock(name='get_child_routes_info')
        self.model.get_matches.return_value = [
            {
                'view_id': 'VIEW_PARENT1',
                'route_link': 'par1/',
                'parent_view_id': 'DEFAULT',
                'route_label': 'PAR1',
                'priority': 1
            },
            {
                'view_id': 'VIEW_PARENT2',
                'route_link': 'par2/',
                'parent_view_id': 'DEFAULT',
                'route_label': 'PAR2',
                'priority': 2
            }
        ]
        self.model.get_child_routes_info.return_value = ({'VIEW_PARENT1': [{'label': 'ABC_DEF', 'link': 'abc/def',
            'priority': 1}, {'label': 'ABC_XYZ', 'link': 'abc/xyz', 'priority': 2}],
            'VIEW_PARENT2': [{'label': 'ABC_PQR', 'link': 'abc/pqr', 'priority': 1}]},
            {'VIEW_PARENT1': 'abc/def', 'VIEW_PARENT2': 'abc/pqr'})
        self.assertEqual(
            self.model.get_routes_info(['VIEW1', 'VIEW2']),
            [{'label': 'PAR1', 'link': 'par1/abc/def', 'parent_id': 'VIEW_PARENT1', 'priority': 1, 'childlinks':
                [{'label': 'ABC_DEF', 'link': 'abc/def', 'priority': 1}, {'label': 'ABC_XYZ', 'link': 'abc/xyz',
                                                                          'priority': 2}]},
            {'label': 'PAR2', 'link': 'par2/abc/pqr', 'parent_id': 'VIEW_PARENT2', 'priority': 2, 'childlinks':
                [{'label': 'ABC_PQR', 'link': 'abc/pqr', 'priority': 1}]}])

    def test_get_child_routes_info(self):
        matches = [{
                'view_id': 'VIEW1',
                'route_link': 'abc/def',
                'parent_view_id': 'VIEW_PARENT1',
                'route_label': 'ABC_DEF',
                'priority': 1
            },
            {
                'view_id': 'VIEW2',
                'route_link': 'abc/pqr',
                'parent_view_id': 'VIEW_PARENT2',
                'route_label': 'ABC_PQR',
                'priority': 1
            },
            {
                'view_id': 'VIEW3',
                'route_link': 'abc/xyz',
                'parent_view_id': 'VIEW_PARENT1',
                'route_label': 'ABC_XYZ',
                'priority': 2
            }]
        self.assertEqual(
            self.model.get_child_routes_info(matches),
            ({'VIEW_PARENT1': [{'label': 'ABC_DEF', 'link': 'abc/def', 'priority': 1}, {'label': 'ABC_XYZ',
                                'link': 'abc/xyz', 'priority': 2}],
              'VIEW_PARENT2': [{'label': 'ABC_PQR', 'link': 'abc/pqr', 'priority': 1}]},
             {'VIEW_PARENT1': 'abc/def', 'VIEW_PARENT2': 'abc/pqr'})
        )


if __name__ == '__main__':
    unittest.main()