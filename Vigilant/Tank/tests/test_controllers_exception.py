import unittest
from mock import patch, MagicMock, call

class TankExceptionControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.models': self.mock_tank.models,
            'tank.models.exception': self.mock_tank.models.exception,
            'tank.models.exception.ExceptionsModel': self.mock_tank.models.exception.ExceptionsModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import exception
        self.exception = exception

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.exception.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_exceptions(self):
        obj = self.mock_tank.models.exception
        self.mock_tank.models.exception.ExceptionsModel.return_value = obj
        obj.get_exceptions.return_value = "value"
        self.assertEqual(self.exception.get_exceptions('abc','exception_type'), "value")

if __name__ == '__main__':
    unittest.main()