import unittest
from mock import patch, MagicMock, call

class TankSiteConfigurationControllersTestCase(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.site_configuration': self.mock_tank.models.site_configuration,
            'tank.models.site_configuration.SiteConfigurationModel': self.mock_tank.models.site_configuration.SiteConfigurationModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import site_configuration
        self.site_configuration = site_configuration
        self.obj = self.mock_tank.models.site_configuration
        self.mock_tank.models.site_configuration.SiteConfigurationModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.site_configuration.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_data(self):
        self.obj.get_data.return_value = "value"
        self.assertEqual(self.site_configuration.get_data('abc','site'), "value")

    def test_approve_config(self): 
        self.obj.approve_config.return_value = "value"
        self.assertEqual(self.site_configuration.approve_config('abc'), "value")


if __name__ == '__main__':
    unittest.main()
