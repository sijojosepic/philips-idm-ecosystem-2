import unittest
from mock import patch, MagicMock


class TankDirectoryModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import directory
        self.directory = directory
        self.directory.COLLECTIONS = {'DIRECTORIES': 'directories'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.directory.DirectoryModel(mongodb=self.mongodb, siteid='ALR00')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def aggregate_qry_assertion(self):
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'siteid': self.model.siteid}},
            {'$project': {'siteid': '$siteid', 'poc_number': '$poc_number', 'poc_name': '$poc_name',
                          'role': '$role', 'address': '$address'}}
        ])

    def test_get_directory(self):
        self.model.collection.aggregate.return_value = [
            {"poc_number": "+31 40 279 3333", "role": "Manager", "poc_name": "POC1",
             "address": "address of the site", "id": "ALR00-1542783075"}]
        self.assertEqual(self.model.get_directories(), {'result': [
            {"poc_number": "+31 40 279 3333", "role": "Manager", "poc_name": "POC1",
             "address": "address of the site", "id": "ALR00-1542783075"}]})
        self.aggregate_qry_assertion()

    def test_save_directory(self):
        req_payload = {"poc_number": "+91-9876543210", "siteid": "test12", "role": "Manager", "poc_name": "POC1",
                              "address": "address of the site", "elementid": "test12-1542800060"}
        self.assertEqual(self.model.save_directory(req_payload), {'code': 200})
        self.aggregate_qry_assertion()

    def test_update_directory_on_existing_site_id(self):
        req_payload = {"_id": 1, "poc_number": "+91-9876543210", "role": "Manager", "poc_name": "POC1",
                              "address": "address of the site", "elementid": "ALR00-1542783075"}
        self.model.collection.aggregate.return_value = req_payload
        self.assertEqual(self.model.update_directory(req_payload), {'code': 200})

    def test_update_directory_on_max_dir_length_exceed(self):
        req_payload = {"_id": 1, "poc_number": "+91-9876543210", "role": "Manager", "poc_name": "POC1",
                              "address": "address of the site", "elementid": "ALR00-1542783075"}
        self.model.collection.aggregate.return_value = [req_payload] * 21
        self.assertEqual(self.model.save_directory(req_payload), {'code': 205})
        self.aggregate_qry_assertion()

    def test_delete_directory(self):
        self.assertEqual(self.model.delete_directory('1'), {'result': 200})
        self.model.collection.delete_one.assert_called_once_with({'_id': '1'})
        self.assertEqual(self.model.collection.delete_one.call_count, 1)
