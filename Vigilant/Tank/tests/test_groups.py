from unittest import TestCase
from mock import MagicMock, patch
from tank.tnconfig import OK_STATUS_CODE, ID_DOES_NOT_EXIST, ID_ALREADY_EXIST, UNAUTHORIZED_USER

class TankGroupsModelTestCase(TestCase):
    def setUp(self):
        TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_datetime = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util, 'datetime': self.mock_datetime}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import groups
        self.groups = groups
        self.groups.COLLECTIONS = {'GROUPS': 'groups', 'IP_STATUS': 'ip_status'}

        self.mongodb = MagicMock(name="mongo_db")
        self.model = self.groups.GroupsModel(mongodb=self.mongodb, siteid='ABA01')

        self.triplet = ["192.168.29", "168.19.32"]

    def tearDown(self):
        TestCase.tearDown(self)
        self.module_patcher.stop()

    def aggregate_qry_assertion(self):
        self.model.groups_collection.aggregate.assert_called_once_with([
            {'$match': {'siteid': self.model.siteid}},
            {'$project': {'_id': 0, 'siteid': '$siteid', 'name': '$name', 'triplet': '$triplet',
                          'created_by': '$created_by', 'created_on': self.groups.get_date_to_string.return_value}}
        ])

    def test_no_groups_found_for_site_id(self):
        self.model.groups_collection.aggregate.return_value = []
        self.assertEquals(self.model.get_all_groups(), {'result': []})
        self.aggregate_qry_assertion()

    def test_get_all_groups(self):
        self.model.groups_collection.aggregate.return_value = [{"siteid": "ABA01", "name": "test"}]
        self.assertEquals(self.model.get_all_groups(), {"result": [{"siteid": "ABA01", "name": "test"}]})
        self.aggregate_qry_assertion()

    def test_create_group(self):
        self.mock_datetime.datetime.now.return_value = '2018-07-02 22:00:00'
        self.assertEquals(self.model.create_group("api_test", "raman", ["192.168.29"]), {"result": OK_STATUS_CODE})
        self.model.groups_collection.update_one.assert_called_once_with(
            {'siteid': self.model.siteid, 'name': 'api_test'},
            {'$set': {'name': 'api_test', 'triplet': ["192.168.29"], 'created_by': 'raman',
                      'created_on': '2018-07-02 22:00:00'}}, upsert=True)

    def test_delete_group_id_not_exist(self):
        self.model.groups_collection.find_one.return_value = None
        self.assertEquals(self.model.delete_group("test"), { "result": ID_DOES_NOT_EXIST})
        self.model.groups_collection.find_one.assert_called_once_with({'siteid': self.model.siteid, 'name': "test",
                                                                       'triplet': {'$exists': True, '$eq': []}})

    def test_delete_group(self):
        self.model.groups_collection.find_one.return_value = {'name': 'test', 'triplet': [], }
        self.assertEquals(self.model.delete_group("test"), {"result": OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_once_with({'siteid': self.model.siteid, 'name': "test",
                                                                       'triplet': {'$exists': True, '$eq': []}})
        self.model.groups_collection.delete_one.assert_called_once_with({'name': 'test', 'siteid': self.model.siteid})
        self.assertEqual(self.model.groups_collection.delete_one.call_count, 1)
        self.model.ip_status_collection.update_many.assert_called_once_with({'siteid': self.model.siteid, 'group_name': 'test'},
                                                                            {'$set': {'group_name': self.model.default_group}})

    def test_add_to_group_data_already_exist(self):
        self.model.groups_collection.find_one.return_value = {"siteid": "ABA01", "name": "test", "triplet": self.triplet, "created_by": "raman"}
        self.assertEquals(self.model.add_to_group("test", "raman", self.triplet), {'result': ID_ALREADY_EXIST})
        self.model.groups_collection.find_one.assert_called_once_with({'name': "test", 'siteid': self.model.siteid}, {'_id': 0})

    def test_add_to_group_default_group_for_non_already_existing_group(self):
        self.model.groups_collection.find_one.return_value = {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}
        self.assertEquals(self.model.add_to_group("test", "abhiram", self.triplet), {'result': UNAUTHORIZED_USER})
        self.model.groups_collection.find_one.assert_called_once_with({'name': "test", 'siteid': self.model.siteid}, {'_id': 0})

    def test_add_to_group_default_group(self):
        self.model.groups_collection.find.return_value = [
            {"siteid": "ABA01", "name": "test1", "triplet": ["192.168.29"], "created_by": "raman"},
            {"siteid": "ABA01", "name": "test2", "triplet": ["168.19.32"], "created_by": "raman"}]
        self.model.groups_collection.find_one.side_effect = [{}, {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"},
                                                             {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}]
        self.assertEquals(self.model.add_to_group(self.model.default_group, "raman", self.triplet), {'result': OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_with({'name': "test2", 'siteid': self.model.siteid}, {'_id': 0, 'triplet': 1})

    def test_add_to_group_existing_group(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test1", "triplet": ["192.168.29"], "created_by": "raman"}]
        self.model.groups_collection.find_one.return_value = {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}
        self.assertEquals(self.model.add_to_group(self.model.default_group, "raman", self.triplet), {'result': OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_with({'name': self.model.default_group, 'siteid': self.model.siteid}, {'_id': 0, 'triplet': 1})

    def test_add_to_group(self):
        self.model.groups_collection.find.return_value = [
            {"siteid": "ABA01", "name": "test1", "triplet": ["192.168.29"], "created_by": "raman"},
            {"siteid": "ABA01", "name": "test2", "triplet": ["168.19.32"], "created_by": "raman"}]
        self.model.groups_collection.find_one.side_effect = [{}, {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"},
                                                             {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}]
        self.assertEquals(self.model.add_to_group("test", "raman", self.triplet), {'result': OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_with({'name': "test2", 'siteid': self.model.siteid}, {'_id': 0, 'triplet': 1})

    def test_add_to_group_triplet_group_data_not_found(self):
        self.model.groups_collection.find_one.side_effect = [{}, {"siteid": "ABA01", "name": "test", "triplet": ["192.168.100"], "created_by": "raman"}]
        self.assertEquals(self.model.add_to_group("test", "raman", self.triplet), {'result': OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_with({'name': "test", 'siteid': self.model.siteid}, {'_id': 0})

    def test_add_to_group_additional_triplets(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}]
        self.model.groups_collection.find_one.side_effect = [{}, {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"},
                                                             {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}]
        self.assertEquals(self.model.add_to_group("test1", "raman", self.triplet), {'result': OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_with({'name': "test1", 'siteid': self.model.siteid}, {'_id': 0, 'triplet': 1})


    def test_add_to_group_triplet_group_data_found_diffrent_created_by(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test", "triplet": ["192.168.100"], "created_by": "raman"}]
        self.model.groups_collection.find_one.return_value = {}
        self.assertEquals(self.model.add_to_group("test", "abhiram", ["192.168.100"]), {'result': UNAUTHORIZED_USER})
        self.model.groups_collection.find_one.assert_called_with({'name': "test", 'siteid': self.model.siteid}, {'_id': 0})

    def test_add_to_group_create_group(self):
        self.model.groups_collection.find_one.return_value = {"siteid": "ABA01", "name": "test", "triplet": ["192.157.17"], "created_by": "raman"}
        self.assertEquals(self.model.add_to_group("test", "raman", self.triplet), {'result': OK_STATUS_CODE})
        self.model.groups_collection.find_one.assert_called_with({'name': "test", 'siteid': self.model.siteid}, {'_id': 0, 'triplet': 1})

    def test_add_to_group_not_unique_triplets(self):
        self.model.groups_collection.find.return_value = [{"siteid": "ABA01", "name": "test1", "triplet": ["192.168.29"], "created_by": "raman"}]
        self.model.groups_collection.find_one.return_value = {"siteid": "ABA01", "name": "test", "triplet": ["192.168.29"], "created_by": "raman"}
        self.assertEquals(self.model.add_to_group("test", "raman", ["192.168.29"]), {'result': ID_ALREADY_EXIST})
        self.model.groups_collection.find_one.assert_called_with({'name': "test", 'siteid': self.model.siteid}, {'_id': 0})
