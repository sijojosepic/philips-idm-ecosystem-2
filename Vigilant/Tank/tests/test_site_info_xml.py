import json
import unittest
from mock import MagicMock, patch, Mock


class SiteInfoXmlModelTest(unittest.TestCase):
  
  def setUp(self):
    unittest.TestCase.setUp(self)
    self.mock_tank = MagicMock(name='tank')
    self.mock_logging = MagicMock(name='logging')
    self.mock_request = MagicMock(name='requests')
    self.mock_tnconfig = MagicMock(name='tnconfig')
    modules = {
      'logging': self.mock_logging,
      'requests': self.mock_request,
      'tnconfig': self.mock_tnconfig,
      'tank.util': self.mock_tank.util
    }
    self.module_patcher = patch.dict('sys.modules', modules)
    self.module_patcher.start()
    from tank.models import site_info_xml
    self.site_info_xml = site_info_xml
    self.mongodb = MagicMock(name='mongo_db')
    self.model = self.site_info_xml.SiteInfoXmlModel(self.mongodb, self.manifest_input)
    self.deployment_keys = self.site_info_xml.SiteInfoXmlKeys(self.mongodb)
    self.site_info_xml.SITE_INFO_MODULE_TYPES = {'Database': 'DBNode', 'Hl7': 'HL7Node'}
    self.site_info_xml.COLLECTIONS = {'DEPLOYMENT_KEYS':'deployment_keys','FACTS': 'facts'}
    self.maxDiff = None
  
  def tearDown(self):
    unittest.TestCase.tearDown(self)
    self.module_patcher.stop()

  def test_update_data(self):
    self.deployment_keys.collection.find.return_value = self.keys
    self.model.update_data()
    self.assertEqual(json.dumps(self.model.data, sort_keys=True), json.dumps(self.manifest_updated, sort_keys=True))
  
  def test_get_facts(self):
    self.model.data = {'siteid' : 'site'}
    json_val = {'result': [{'module_type': 'Database'}]}
    expected = [{'module_type': 'Database'}]
    self.site_info_xml.get_site_item_details = MagicMock(name='get_facts', return_value = json_val)
    self.assertEqual(self.model.get_facts(), expected)

  def test_check_for_tags(self):
    response2 = {
      "result": [{"ISP": {"tags": "production"},"hostname": "host1"},
        {"ISP": {"tags": "test"},"hostname": "host2"},
        {"ISP": {},"hostname": "host3"},
        {"hostname": "host4"}]
    }
    self.site_info_xml.get_site_item_details = MagicMock(name='get_facts', return_value = response2)
    facts_response = self.model.get_facts()
    self.assertEqual(self.model.check_for_tag(facts_response, "host1"),{'valid':True, 'value': 'production'})
    self.assertEqual(self.model.check_for_tag(facts_response, "host2"),{'valid':True, 'value': 'test'})
    self.assertEqual(self.model.check_for_tag(facts_response, "host3"),{'valid':False, 'value': None})
    self.assertEqual(self.model.check_for_tag(facts_response, "host4"),{'valid':False, 'value': None})

  def test_add_site_keys(self):
    self.model.data = {'knownkey':'this is shown'}
    from lxml.etree import tostring
    self.assertEqual(tostring(self.model.add_site_keys('unknownkey','false', 'not shown')), '<Key Encrypted="false" KeyName="unknownkey">NA</Key>')
    self.assertEqual(tostring(self.model.add_site_keys('abc','true', 'knownkey')), '<Key Encrypted="true" KeyName="abc">this is shown</Key>')
  
  def test_add_noncore_node_details(self):
    json_val = [{'module_type': 'Database', 'hostname': 'mock_host'}]
    self.model.get_facts = Mock(return_value=json_val)
    self.model.data = {'hosts': {'idm02mi1.idm02.isyntax.net': {'RoleType': '8', 'host_vault': {}}},'a':'b'}
    self.model.add_noncore_node_details()
    expected_output = {'a': 'b', 'HL7Node': 'NA', 'hosts': {'idm02mi1.idm02.isyntax.net': {'RoleType': '8', 'host_vault': {}}}, 'DBNode': 'NA'}
    self.assertEqual(self.model.data, expected_output)
    
  def test_create_xml(self):
    self.deployment_keys.collection.find.return_value = self.keys
    self.site_info_xml.get_site_item_details = MagicMock(name='get_facts', return_value = self.response)
    self.assertEqual(self.model.create_xml()['result'], self.site_xml_one)

  manifest_updated = {
    "ISiteServiceAccountUser": "NA",
    "DomainAdmin": "NA",
    "DBSharedPath": "NA",
    "Federated": "NA",
    "DBPassword": "NA",
    "DBNode": "NA",
    "DBUsername": "NA",
    "DeploymentRootFolder": "NA",
    "HL7Node": "NA",
    "DomainAdminPassword": "NA",
    "LocalizationCode": "NA",
    "DeploymentScripts": "NA",
    "SolutionRootFolder": "NA",
    "ISiteServiceAccountPassword": "NA",
    "siteid": "DEV03",
    "SiteID": "NA",
    "deployment_id": "a24thy",
    "site_vault": {
        "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
    },
    "SharedDataBaseHost": "NA",
    "site_data": {
      "LocalizationCode": "ENG",
      "DeploymentScripts": "C:\\provision\\isite\\deploymentscripts",
      "ISiteServiceAccountUser": "DEVO3\\iSiteService",
      "Federated": "False",
      "SolutionRootFolder": "S:\\Philips\\Apps\\iSite"
    },
    "hosts": {
      "DEV03AM1.DEV03.iSyntax.net": {
        "RoleType": "ActiveMirror",
        "host_vault": {},
        "Sequence": "5",
        "ip": "167.81.184.117",
        "packages": ["AWV", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "ActiveMirror"
        }  
      },
      "DEV03IF1.DEV03.iSyntax.net": {
        "RoleType": "Infrastructure",
        "host_vault": {},
        "Sequence": "1",
        "ip": "167.81.184.117",
        "packages": ["AWV-1.3", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "Infrastructure"
        }        
      },
      "DEV03DB1.DEV03.iSyntax.net": {
        "RoleType": "ActiveMirror",
        "host_vault": {},
        "Sequence": "5",
        "ip": "167.81.184.117",
        "packages": ["AWV", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "ActiveMirror"
        }  
      },
      "DEV03HL7.DEV03.iSyntax.net": {
        "RoleType": "Infrastructure",
        "host_vault": {},
        "Sequence": "1",
        "ip": "167.81.184.117",
        "packages": ["AWV-1.3", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "Infrastructure"
        }       
      }
    }
    }

  manifest_input =  {
    "HL7Node": "NA",
    "DBNode": "NA",
    "DBUsername": "NA",
    "DBPassword": "NA",
    "DBSharedPath": "NA",
    "SharedDataBaseHost": "NA",
    "siteid": "DEV03",
    "site_vault": {
        "ISiteServiceAccountPassword": "fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg=="
    },
    "site_data": {
        "Federated": "False",
        "LocalizationCode": "ENG",
        "ISiteServiceAccountUser": "DEVO3\\iSiteService",
        "SolutionRootFolder": "S:\\Philips\\Apps\\iSite",
        "DeploymentScripts": "C:\\provision\\isite\\deploymentscripts"
    },
    "deployment_id": "a24thy",
    "hosts": {
      "DEV03AM1.DEV03.iSyntax.net": {
        "RoleType": "ActiveMirror",
        "host_vault": {},
        "Sequence": "5",
        "ip": "167.81.184.117",
        "packages": ["AWV", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "ActiveMirror"
        }  
      },
      "DEV03IF1.DEV03.iSyntax.net": {
        "RoleType": "Infrastructure",
        "host_vault": {},
        "Sequence": "1",
        "ip": "167.81.184.117",
        "packages": ["AWV-1.3", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "Infrastructure"
        }        
      },
      "DEV03DB1.DEV03.iSyntax.net": {
        "RoleType": "ActiveMirror",
        "host_vault": {},
        "Sequence": "5",
        "ip": "167.81.184.117",
        "packages": ["AWV", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "ActiveMirror"
        }  
      },
      "DEV03HL7.DEV03.iSyntax.net": {
        "RoleType": "Infrastructure",
        "host_vault": {},
        "Sequence": "1",
        "ip": "167.81.184.117",
        "packages": ["AWV-1.3", "PCM-2.3.1.1.234.0"],
        "host_data": {
          "RoleType": "Infrastructure"
        }       
      }
    }
  }
  keys = [{"keys" : [ 
          "ISiteServiceAccountPassword", 
          "DeploymentRootFolder", 
          "ISiteServiceAccountUser", 
          "DomainAdminPassword", 
          "LocalizationCode", 
          "DomainAdmin", 
          "Federated", 
          "SolutionRootFolder", 
          "DeploymentScripts", 
          "SiteID", 
          "HL7Node", 
          "DBNode", 
          "DBUsername", 
          "DBPassword", 
          "DBSharedPath", 
          "SharedDataBaseHost"
        ]}]
  
  response = {"result": 
    [      
      {   
        "ISP": {
          "endpoint": {
              "scanner": "ISP",
              "address": "172.17.8.67"
          },
          "tags": "production",
          "version": "4,4,532,1",
          "module_type": 4
        },
        "hostname": "DEV03IF1.DEV03.iSyntax.net",
      },
      {
        "ISP": {
            "endpoint": {
                "scanner": "ISP",
                "address": "172.17.8.67"
            },
            "tags": "production",
            "module_type": 22
        },
        "hostname": "DEV03AM1.DEV03.iSyntax.net",
      },
      {   
        "ISP": {
          "endpoint": {
              "scanner": "ISP",
              "address": "172.17.8.67"
          },
          "version": "4,4,532,1",
          "tags": "production",
          "module_type": "Hl7"
        },
        "hostname": "DEV03HL7.DEV03.iSyntax.net",
      },
      {
        "ISP": {
            "endpoint": {
                "scanner": "ISP",
                "address": "172.17.8.67"
            },
            "tags": "production",
            "module_type": 'Database'
        },
        "hostname": "DEV03DB1.DEV03.iSyntax.net",
      }
    ] 
  }

  site_xml_one = """<?xml version='1.0' encoding='UTF-8'?>
<SiteMap>
  <SiteBag>
    <Keys>
      <Key Encrypted="true" KeyName="ISiteServiceAccountPassword">fQRAfhjsfa1fOiVR0JugjZqJOF3uuHeqLOzTeUj1Qqhip34uxpMkqTmGKclokOyJqlgJsBptRgX+LuZ8FjlPECoU+OoNwvutQAiAxSFM5ZrtrmUjn7H1oElB7SY3zEzuXJu53GXAhAGkdguR7ryWRiFfLubswE9sq7CE+WDdBD6nLxxPYDMKyaW0SVNjtEv4Qt7w+DFQrAbHwoTxYuSQc7+bb8NzjJ2543Pg0D+t86+p0hloW/6ziks5VBn5QzSlfQMqGyXoVditwuE8N866I6sy4xlIq2trOuzhn7V8Tp7fy3bW02eKxYB0d2LOQXz1AnFQ1fIvoh8ZV03dyViIJg==</Key>
      <Key Encrypted="false" KeyName="LocalizationCode">ENG</Key>
      <Key Encrypted="false" KeyName="DeploymentScripts">C:\provision\isite\deploymentscripts</Key>
      <Key Encrypted="false" KeyName="ISiteServiceAccountUser">DEVO3\iSiteService</Key>
      <Key Encrypted="false" KeyName="Federated">False</Key>
      <Key Encrypted="false" KeyName="SolutionRootFolder">S:\Philips\Apps\iSite</Key>
      <Key Encrypted="false" KeyName="SiteID">DEV03</Key>
      <Key Encrypted="false" KeyName="HL7Node">DEV03HL7.DEV03.iSyntax.net</Key>
      <Key Encrypted="false" KeyName="DBNode">DEV03DB1.DEV03.iSyntax.net</Key>
      <Key Encrypted="false" KeyName="DBUsername">NA</Key>
      <Key Encrypted="true" KeyName="DBPassword">NA</Key>
      <Key Encrypted="false" KeyName="DBSharedPath">NA</Key>
      <Key Encrypted="false" KeyName="SharedDataBaseHost">NA</Key>
    </Keys>
  </SiteBag>
  <Roles>
    <Role Sequence="5">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03AM1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">ActiveMirror</Key>
        </Keys>
      </RoleBag>
    </Role>
    <Role Sequence="1">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03HL7.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="1.3" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">Infrastructure</Key>
        </Keys>
      </RoleBag>
    </Role>
    <Role Sequence="5">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03DB1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">ActiveMirror</Key>
        </Keys>
      </RoleBag>
    </Role>
    <Role Sequence="1">
      <IPv4>167.81.184.117</IPv4>
      <FQDN>DEV03IF1.DEV03.iSyntax.net</FQDN>
      <Packages>
        <PackageInfo Version="1.3" Name="AWV"/>
        <PackageInfo Version="2.3.1.1.234.0" Name="PCM"/>
      </Packages>
      <RoleBag>
        <Keys>
          <Key Encrypted="false" KeyName="RoleType">Infrastructure</Key>
        </Keys>
      </RoleBag>
    </Role>
  </Roles>
</SiteMap>\n"""


if __name__ == '__main__':
  unittest.main()
