import re
import unittest
from mock import patch, MagicMock
import datetime


class TankFactsModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import fact
        self.fact = fact
        self.fact.COLLECTIONS = {'FACTS': 'facts'}
        self.fact.PUBLIC_MODULES = {'ISP': 'Intellispace PACS', 'LN': 'Legacy Nagios'}
        self.fact.PUBLIC_HOST_KEYS = ['address', 'domain']

        self.mongodb = MagicMock(name='mongo_db')
        self.model = fact.SiteFactsModel(self.mongodb, 'SITE1')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_hosts(self):
        self.assertEqual(
            self.model.get_hosts(),
            {'hosts': self.model.collection.distinct.return_value}
        )
        self.model.collection.distinct.assert_called_once_with('hostname', filter={'siteid': 'SITE1'})

    def test_get_host_detail(self):
        self.model.get_filtered_host_facts = MagicMock(name='get_filtered_host_facts')
        self.assertEqual(
            self.model.get_host_detail('host1.somesite.net'),
            self.model.get_filtered_host_facts.return_value
        )
        self.fact.get_projection.assert_called_once_with(['hostname', 'address', 'domain', 'LN', 'ISP'])
        self.model.collection.find_one.assert_called_once_with(
            {'hostname': 'host1.somesite.net', 'siteid': 'SITE1'},
            self.fact.get_projection.return_value
        )
        self.model.get_filtered_host_facts.assert_called_once_with(
            self.model.collection.find_one.return_value,
            ['address', 'domain'],
            ['LN', 'ISP']
        )

    def test_get_modules(self):
        self.assertEqual(
            self.model.get_modules(),
            {'modules': self.fact.get_keys_in_site.return_value}
        )
        self.fact.get_projection.assert_called_once_with(['LN', 'ISP'])
        self.model.collection.find.assert_called_once_with({'siteid': 'SITE1'}, self.fact.get_projection.return_value)
        self.fact.get_keys_in_site.assert_called_once_with(self.model.collection.find.return_value)

    def test_get_item_details(self):
        self.model.collection.find.return_value = ['item1', 'item2']
        self.assertEqual(
            self.model.get_item_details('mod1.key1'),
            {'result': ['item1', 'item2']}
        )
        self.fact.get_projection.assert_called_once_with(['hostname', 'address', 'mod1.key1'])
        self.model.collection.find.assert_called_once_with(
            {'mod1.key1': {'$exists': True}, 'siteid': 'SITE1'}, self.fact.get_projection.return_value
        )

    def test_get_site_module_type(self):
        self.model.get_site_module_type()
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'siteid': 'SITE1'}},
            {'$project': {'_id': 0, 'hostname': '$hostname', 'address': '$address',
                          'module_type': {'$ifNull': ['$ISP.module_type', {'$cond': { 'if': {'$or': [
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "MGNode"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "Database"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "HL7"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowArchiveServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowHDServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "AdvancedWorkflowArchiveHDServices"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4EV"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4Prep"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4Viewer"]},
            {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4"]}
            ]},
            'then': {'$arrayElemAt': [
                "$endpoints.scanner", 0]},
            'else': {'$cond': {
                'if': {'$eq': [{'$arrayElemAt': ["$endpoints.scanner", 0]}, "I4GD"]},
                    'then': {'$arrayElemAt': ["$role", 0]},
                'else': '$NULL'
            }}}}]},
            'modules': '$modules'}},
        ])

    def get_site_key_details(self):
        self.model.collection.find.return_value = ['item1', 'item2']
        self.assertEqual(
            self.model.get_site_key_details('mod1.key1', 12),
            {'result': ['item1', 'item2']}
        )
        self.fact.get_projection.assert_called_once_with(
            ['hostname', 'Components.name', 'Components.version'])
        self.model.collection.find.assert_called_once_with(
            {'mod1.key1': {'$exists': True}, 'siteid': 'SITE1', 'mod1.key1': 12}, self.fact.get_projection.return_value
        )

    def test_get_components(self):
        self.model.collection.find.return_value = [{'version': '1', 'name': 'one'}, {'version': '2', 'name': 'two'}]
        self.assertEqual(
            self.model.get_components(),
            {'results': [{'version': '1', 'name': 'one'}, {'version': '2', 'name': 'two'}]}
        )
        self.fact.get_projection.assert_called_once_with(['hostname', 'Components'])
        self.model.collection.find.assert_called_once_with(
            {'siteid': 'SITE1', 'Components': {'$exists': True}}, self.fact.get_projection.return_value
        )

    def test_get_host_components(self):
        self.model.get_host_components('hostname')
        self.model.collection.aggregate.assert_called_once_with([
            {'$unwind': '$Components'},
            {'$match': {'_id': 'SITE1-hostname', 'Components': {'$exists': True}}},
            {'$group': {'_id': {'name': '$Components.name', 'version': '$Components.version',
                                'timestamp': {'$dateToString': {'format': "%Y/%m/%d %H:%M:%S",
                                                                'date': '$Components.timestamp'}}}}},
            {'$group': {'_id': '$_id.name', 'version': {'$first': '$_id.version'},
                        'timestamp': {'$first': '$_id.timestamp'}}},
            {"$sort": {"timestamp": -1}},
            {'$project': {'_id': 0, 'name': '$_id', 'version': '$version', 'timestamp': '$timestamp'}}
        ])

    def test_get_one_component(self):
        self.model.collection.aggregate.return_value = ['comp1', 'comp2']
        self.assertEqual(
            self.model.get_one_component('compname1'),
            {'results': ['comp1', 'comp2']}
        )
        self.model.collection.aggregate.assert_called_once_with(
            [
                {'$match': {'Components.name': 'compname1', 'siteid': 'SITE1'}},
                {'$project': {'_id': 0, 'hostname': 1, 'Components': 1}},
                {'$unwind': '$Components'},
                {'$match': {'Components.name': 'compname1'}}
            ]
        )

    def test_filtered_host_facts_basic(self):
        host_facts = {
            '_id': 'site9_host3_site9_isyntax_net',
            'siteid': 'site9',
            'hostname': 'host3.site9.isyntax.net',
            'manufacturer': 'VMware, Inc.',
            'address': '10.0.0.5',
            'domain': 'site9.isyntax.net',
            'ISP': {
                'flags': ['anywhere', 'domain_needed'],
                'identifier': '4x'
            },
            'Windows': {
                'flags': ['domain_needed']
            }
        }
        result = self.model.get_filtered_host_facts(host_facts, ['hostname', 'address'], ['ISP', 'Windows'])
        self.assertEqual(
            result,
            {
                'hostname': 'host3.site9.isyntax.net',
                'modules': ['Windows', 'ISP'],
                'address': '10.0.0.5'
            }
        )

    def test_filtered_host_facts_no_public_modules(self):
        host_facts = {
            '_id': 'site9_host3_site9_isyntax_net',
            'siteid': 'site9',
            'hostname': 'host3.site9.isyntax.net',
            'address': '10.0.0.5',
            'domain': 'site9.isyntax.net',
        }
        result = self.model.get_filtered_host_facts(host_facts, ['hostname', 'address'], ['ISP', 'Windows'])
        self.assertEqual(result, {'hostname': 'host3.site9.isyntax.net', 'modules': [], 'address': '10.0.0.5'})

    def test_filtered_host_facts_no_host_facts(self):
        result = self.model.get_filtered_host_facts(None, ['hostname', 'address'], ['ISP', 'Windows'])
        self.assertEqual(result, {})

    def test_get_node_details(self):
        self.model.collection.aggregate.return_value = [{
            'hostname': 'host1',
            'address': 'N/A',
            'product_id': 'P1',
            'product_version': 'V1',
            'role': 'R1',
            'module_type': 'N/A',
            'modules': ['module1'],
            'Windows': {'name': 'Windows Server 1'},
            'vCenter': {
                "datacenter": {
                    "datacenter-2": {
                        "cluster": {
                            "domain-1": {
                                "host": {
                                    "host-1": {
                                        "product": {
                                            "version": "1.0.0"
                                        },
                                        "name": "host2",
                                        "hardware": {
                                            "vendor": "Vendor1",
                                            "model": "Model1"
                                        },
                                        "vms": [
                                            {
                                                "powerState": "poweredOn",
                                                "guest": {
                                                    "hostName": "host4",
                                                    "ipAddress": "1.1.1.1"
                                                },

                                            },
                                            {
                                                "powerState": "poweredOn",
                                                "guest": {
                                                    "hostName": "host5",
                                                    "ipAddress": "2.2.2.2"
                                                },
                                            }]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            'Components': [{'timestamp': datetime.datetime(2017, 3, 3, 18, 00, 00),
                            'version': 'Windows Server',
                            'name': 'Windows'}]
        },
            {
                'hostname': 'host2',
                'address': 'N/A',
                'product_id': 'P2',
                'product_version': 'V2',
                'role': 'R2',
                'module_type': 'N/A',
                'servicetag': 'tag 1',
                'modules': 'N/A'
            },
            {
                'hostname': 'host3',
                'address': 'N/A',
                'product_id': 'N/A',
                'product_version': 'N/A',
                'role': 'R3',
                'module_type': 'N/A',
                'HPSwitch': {'switch_model': 1},
                'modules': ['HPSwitch'],
                'vendor': 'Vendor2',
                'version': '1.0',
                'model': 'Model2'
            },
            {
                'hostname': '2.2.2.2',
                'address': 'N/A',
                'product_id': 'P5',
                'product_version': 'V5',
                'role': 'R5',
                'module_type': 'N/A',
                'servicetag': 'tag 5',
                'modules': 'N/A'
            }]
        self.fact.DashboardModel = MagicMock(name='DashboardModel')
        self.fact.DashboardModel.return_value.get_nodes_down.return_value = ['host2']
        self.fact.RulesModel = MagicMock(name='RulesModel')
        self.fact.RulesModel.return_value.get_rules_with_siteid.return_value = {'result': [{"nodename": "host3", 'start_time': '', 'end_time': ''}]}
        self.assertEqual(self.model.get_node_details(), {
            'result': [{'status': 'Up', 'hostname': 'host1', 'product_id': 'P1',
                        'product_version': 'V1', 'role': 'R1', 'module_type': 'N/A', 'address': 'N/A',
                        'modules': ['module1'], 'discovery_type': 'Discovery', 'Windows': {'name': 'Windows Server 1'},
                        'Components': [{'timestamp': '2017/03/03  18:00:00',
                                        'version': 'Windows Server',
                                        'name': 'Windows'}]},
                       {'status': 'Down', 'hostname': 'host2', 'product_id': 'P2',
                        'product_version': 'V2', 'role': 'R2', 'module_type': 'N/A', 'discovery_type': 'Discovery',
                        'address': 'N/A', 'servicetag': 'tag 1', 'modules': 'N/A', 'host_vendor': 'Vendor1',
                        'host_version': '1.0.0', 'host_model': 'Model1'},
                       {'status': 'Maintenance', 'hostname': 'host3', 'product_id': 'N/A',
                        'product_version': 'N/A', 'role': 'R3', 'module_type': 'N/A', 'discovery_type': 'Discovery',
                        'address': 'N/A', 'HPSwitch': {'switch_model': 1}, 'modules': ['HPSwitch'], 'vendor': 'Vendor2',
                        'version': '1.0', 'model': 'Model2', 'start_time': '', 'end_time': ''},
                       {'status': 'Up', 'servicetag': 'tag 5', 'product_version': 'V5', 'role': 'R5','product_id': 'P5',
                        'address': 'N/A', 'module_type': 'N/A', 'hostname': '2.2.2.2','modules': 'N/A',
                        'discovery_type': 'Discovery'},
                       {'status': 'Up', 'hostname': 'host4', 'discovery_type': 'vCenter', 'address': '1.1.1.1'}
                       ]})
        self.model.collection.aggregate.assert_called_once_with(
            [{'$match': {'$and': [{'siteid': 'SITE1'}, {'hostname': {'$not': re.compile('localhost')}}]}},
             {
                 '$project': {
                     '_id': 0,
                     'hostname': '$hostname',
                     'address': '$address',
                     'product_id': '$product_id',
                     'product_version': '$product_version',
                     'role': '$role',
                     'vCenter': '$vCenter',
                     'module_type': '$ISP.module_type',
                     'modules': '$modules',
                     'servicetag': '$servicetag',
                     'Windows': '$Windows',
                     'HPSwitch': '$HPSwitch',
                     'Components': '$Components',
                     'host_vendor': '$manufacturer',
                     'host_model': '$model'
                 }
             }
             ])

    def test_get_node_details_with_host_key(self):
        self.model.collection.aggregate.return_value = [{
            'hostname': 'host1',
            'address': 'N/A',
            'product_id': 'P1',
            'product_version': 'V1',
            'role': 'R1',
            'module_type': 'N/A',
            'modules': ['module1'],
            'Windows': {'name': 'Windows Server 1'},
            'vCenter': {
                "datacenter": {
                    "datacenter-2": {
                        "host": {
                            "domain-1": {
                                "host": {
                                    "host-1": {
                                        "product": {
                                            "version": "1.0.0"
                                        },
                                        "name": "host2",
                                        "hardware": {
                                            "vendor": "Vendor1",
                                            "model": "Model1"
                                        },
                                        "vms": [
                                            {
                                                "powerState": "poweredOn",
                                                "guest": {
                                                    "hostName": "host4",
                                                    "ipAddress": "1.1.1.1"
                                                },

                                            }]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            'Components': [{'timestamp': datetime.datetime(2017, 3, 3, 18, 00, 00),
                            'version': 'Windows Server',
                            'name': 'Windows'}]
        },
            {
                'hostname': 'host2',
                'address': 'N/A',
                'product_id': 'P2',
                'product_version': 'V2',
                'role': 'R2',
                'module_type': 'N/A',
                'servicetag': 'tag 1',
                'modules': 'N/A'
            },
            {
                'hostname': 'host3',
                'address': 'N/A',
                'product_id': 'N/A',
                'product_version': 'N/A',
                'role': 'R3',
                'module_type': 'N/A',
                'HPSwitch': {'switch_model': 1},
                'modules': ['HPSwitch'],
                'vendor': 'Vendor2',
                'version': '1.0',
                'model': 'Model2'
            }]
        self.fact.DashboardModel = MagicMock(name='DashboardModel')
        self.fact.DashboardModel.return_value.get_nodes_down.return_value = ['host2']
        self.assertEqual(self.model.get_node_details(), {
            'result':
                [{'Components': [{'name': 'Windows',
                                  'timestamp': '2017/03/03  18:00:00',
                                  'version': 'Windows Server'}],
                  'Windows': {'name': 'Windows Server 1'},
                  'address': 'N/A',
                  'discovery_type': 'Discovery',
                  'hostname': 'host1',
                  'module_type': 'N/A',
                  'modules': ['module1'],
                  'product_id': 'P1',
                  'product_version': 'V1',
                  'role': 'R1',
                  'status': 'Up'},
                 {'address': 'N/A',
                  'discovery_type': 'Discovery',
                  'hostname': 'host2',
                  'module_type': 'N/A',
                  'modules': 'N/A',
                  'product_id': 'P2',
                  'product_version': 'V2',
                  'role': 'R2',
                  'servicetag': 'tag 1',
                  'status': 'Down'},
                 {'HPSwitch': {'switch_model': 1},
                  'address': 'N/A',
                  'discovery_type': 'Discovery',
                  'hostname': 'host3',
                  'model': 'Model2',
                  'module_type': 'N/A',
                  'modules': ['HPSwitch'],
                  'product_id': 'N/A',
                  'product_version': 'N/A',
                  'role': 'R3',
                  'status': 'Up',
                  'vendor': 'Vendor2',
                  'version': '1.0'}
                 ]}
                         )
        self.assertEqual(self.model.collection.aggregate.call_count, 2)

    def test_get_facts_hosts(self):
        self.model.collection.aggregate.return_value = [{
            "_id": "S1",
            "result": [
                {
                    "hostname": "host1"
                },
                {
                    "hostname": "host2"
                }
            ]
        }]
        self.assertEqual(
            self.model.get_facts_hosts(),
            [{
                "_id": "S1",
                "result": [
                    {
                        "hostname": "host1"
                    },
                    {
                        "hostname": "host2"
                    }
                ]
            }]
        )
        self.model.collection.aggregate.assert_called_once_with([
            {'$group': {
                '_id': '$siteid',
                'result': {'$push': {
                    'hostname': '$hostname',
                    "vCenter": "$vCenter"
                }}
            }},
            {
                '$project': {
                    '_id': 1,
                    'result': 1
                }
            }
        ])

    def test_get_host_count(self):
        self.model.get_host_count()
        self.model.collection.distinct.assert_called_once_with('_id', {'hostname': {'$nin': ['localhost']}})

    def test_ecosystem_verisons(self):
        self.model.ecosystem_verisons()
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'hostname': 'localhost', 'Components': {'$exists': True}}},
            {'$unwind': '$Components'},
            {"$sort": {"Components.timestamp": -1}},
            {'$project': {'siteid': '$siteid', 'version': '$Components.version', '_id': 0}}
        ])

    def test_get_version_by_product_id(self):
        self.model.get_version_by_product_id('PID', 'I3', 'I4')
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'product_id': 'PID', 'I3': 'I4', 'tags': 'production'}},
            {'$project': {'product_version': '$product_version', 'siteid': '$siteid'}}
        ])
    
    def test_get_facts(self):
        self.model.get_facts()
        self.model.collection.find.assert_called_once_with(
            {},
            {'Components.timestamp': 0}
        )
        self.model.collection.find.return_value.limit.assert_called_once_with(
            0
        )

    def test_get_facts_with_limit(self):
        self.model.get_facts(100)
        self.model.collection.find.assert_called_once_with(
            {},
            {'Components.timestamp': 0}
        )
        self.model.collection.find.return_value.limit.assert_called_once_with(
            100
        )

if __name__ == '__main__':
    unittest.main()
