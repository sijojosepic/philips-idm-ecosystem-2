import re
import unittest
from mock import patch, MagicMock, call

class TankRulesModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_tank = MagicMock(name='tank')
        self.redis = MagicMock(name='redis')
        self.mock_bottle_mongo = MagicMock(name='bottle_mongo')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {
            'bottle_mongo': self.mock_bottle_mongo,
            'bottle': self.mock_bottle,
            'phimutils': self.mock_phimutils,
            'phimutils.plogging': self.mock_phimutils.plogging,
            'datetime': self.mock_datetime,
            'tank.util.RedisInstance': self.mock_tank.util.RedisInstance,
            'tank.util': self.mock_tank.util,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import site_rules
        self.site_rules = site_rules
        self.site_rules.COLLECTIONS = {'SITE_MAINTENANCE_RULES': 'site_maintenance_rules'}

        self.mongodb = MagicMock(name='mongo_db')
        self.request = {
            "siteid": "ABC01",
            "nodes": ["ABC01-192.168", "IST01-192.169", "IST01-192.170"],
            "services": ["Host__Information__output", "Nodes__Inforamtion__State", "Host__Information__Deployment"],
            "start_time": "2018-07-02 22:00:00",
            "end_time": "2018-07-02 22:00:00",
            "user": "ABCD",
            "maintenance_remark": "Os upgrade"
        }
        self.model = self.site_rules.RulesModel(self.mongodb)
        self.model.hash_set = 'TEST'
        self.model.get_hash_val = MagicMock(name='get_hash_val', return_value={'start_time': 'ST', 'end_time': 'ET'})
        self.model.rds = self.redis

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_rules(self):
        self.mock_datetime.datetime.strptime.return_value = '2018-07-02 22:00:00'
        self.model.save_rules(self.request)
        self.model.collection.update.assert_has_calls([
            call({'_id': 'ABC01_ABC01-192.168', 'type': 'maintenance'}, {'remark': 'Os upgrade',
            'end_time': '2018-07-02 22:00:00', 'siteid': 'ABC01', 'nodename': 'ABC01-192.168',
            'start_time': '2018-07-02 22:00:00', '_id': 'ABC01_ABC01-192.168', 'type': 'maintenance', 'user': 'ABCD'}, upsert=True),
            call({'_id': 'ABC01_IST01-192.169', 'type': 'maintenance'}, {'remark': 'Os upgrade',
            'end_time': '2018-07-02 22:00:00', 'siteid': 'ABC01', 'nodename': 'IST01-192.169',
            'start_time': '2018-07-02 22:00:00', '_id': 'ABC01_IST01-192.169', 'type': 'maintenance', 'user': 'ABCD'}, upsert=True),
            call({'_id': 'ABC01_IST01-192.170', 'type': 'maintenance'}, {'remark': 'Os upgrade',
            'end_time': '2018-07-02 22:00:00', 'siteid': 'ABC01', 'nodename': 'IST01-192.170',
            'start_time': '2018-07-02 22:00:00', '_id': 'ABC01_IST01-192.170', 'type': 'maintenance', 'user': 'ABCD'}, upsert=True)])
        self.assertEqual(self.model.collection.update.call_count,3)

    def test_get_rules_with_siteid(self):
        self.model.collection.aggregate.return_value = ["ABC01", "ABC02"]
        self.assertEqual(
            self.model.get_rules_with_siteid('TEST'),
            {'result': ["ABC01", "ABC02"]}
        )
        self.mock_datetime.datetime.strptime.return_value = 'YYYY-MM-DD HH:II:SS'
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'type': 'maintenance', 'siteid': 'TEST'}},
            {'$project': {'remark': '$remark',
                          'nodename': '$nodename',
                          'start_time': self.site_rules.get_date_to_string_field_projection.return_value,
                          '_id': 0,
                          'user': '$user',
                          'siteid': '$siteid',
                          'end_time': self.site_rules.get_date_to_string_field_projection.return_value}
             }
        ])
        self.assertEqual(self.site_rules.get_date_to_string_field_projection.call_count, 2)

    def test_get_all_sites_under_maintanance(self):
        self.model.collection.find.return_value = ["SITE1", "SITE2"]
        self.assertEqual(
            self.model.get_all_sites_under_maintenance(),
            {'rules': self.model.collection.find.return_value}
        )
        self.model.collection.find.assert_called_once_with({'type': self.model.maintenance_type})

    def test_is_site_under_maintenance(self):
        self.model.collection.count.return_value = ["SITE1"]
        self.model.is_site_under_maintenance('SITE1')
        self.model.collection.count.assert_called_once_with({'_id': 'SITE1','type': self.model.maintenance_type})
        self.assertEqual(self.model.is_site_under_maintenance('SITE1'), {'result': True})

    def test_is_node_under_maintenance(self):
        self.model.collection.count.return_value = ["NODE1"]
        self.model.is_node_under_maintenance('SITE1','NODE1')
        self.model.collection.count.assert_called_once_with({'_id': 'SITE1_NODE1','type': self.model.maintenance_type})
        self.assertEqual(self.model.is_node_under_maintenance('SITE1','NODE1'), {'result': True})

    def test_get_node_maintenance_schedule(self):
        self.model.collection.aggregate.return_value = ["Node1", "Node2"]
        self.assertEqual(
            self.model.get_node_maintenance_schedule('TESTSITE', 'TESTNODE'),
            {'result': ["Node1", "Node2"]}
        )
        self.mock_datetime.datetime.strptime.return_value = 'YYYY-MM-DD HH:II:SS'
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'type': 'maintenance', '_id': 'TESTSITE_TESTNODE'}},
            {'$project': {'nodename': '$nodename',
                          'start_time': self.site_rules.get_date_to_string_field_projection.return_value,
                          '_id': 0,
                          'siteid': '$siteid',
                          'end_time': self.site_rules.get_date_to_string_field_projection.return_value}
             }
        ])
        self.assertEqual(self.site_rules.get_date_to_string_field_projection.call_count, 2)

    def test_get_rule_list(self):
        self.model.collection.distinct.return_value = [{"INFO1": "INFO2"}]
        self.assertEqual(
            self.model.get_rule_list(),
            {'sites': [{"INFO1": "INFO2"}]}
        )

    def test_update_node_level_rules_in_cache(self):
        rule = {
                    'remark': 'random remark',
                    'nodename': 'idm01.isyn.net',
                    'start_time': 'ST',
                    'siteid': 'SUN02',
                    'end_time': 'ET',
                    'type': 'maintenance',
                    'user': 'abcd@philips.com'
                }
        self.model.get_all_sites_under_maintenance = MagicMock(name="get_all_sites_under_maintenance",return_value={'rules': [rule]})
        self.model.set_cache = MagicMock(name='set_cache')
        self.model.collection.find = MagicMock(name='find', return_value = [{'nodename':'idm01.isyn.net'}])

        self.model.update_rules_in_cache()
        self.model.rds.delete.assert_called_once_with(self.model.hash_set)
        self.model.set_cache.assert_called_once_with(rule)

    def test_update_site_level_rules_in_cache(self):
        rule = {
                    "_id" : "SUN03",
                    "remark" : "random remark",
                    "start_time" : "ST",
                    "siteid" : "SUN03",
                    "user" : "abcd@philips.com",
                    "type" : "maintenance",
                    "end_time" : "ET"
                }
        self.model.get_all_sites_under_maintenance = MagicMock(name="get_all_sites_under_maintenance",return_value={'rules': [rule]})
        self.model.set_cache = MagicMock(name='set_site_cache')

        self.model.update_rules_in_cache()
        self.model.rds.delete.assert_called_once_with(self.model.hash_set)
        self.model.set_cache.assert_called_once_with(rule)

    def test_update_multiple_rules_in_cache(self):
        rule = {
                    '_id': 'SUN02_idm01.isyn.net',
                    'remark': 'random remark',
                    'nodename': 'idm01.isyn.net',
                    'start_time': 'ST',
                    'siteid': 'SUN02',
                    'end_time': 'ET',
                    'type': 'maintenance',
                    'user': 'abcd@philips.com'
                }
        rule2 = {
                    "_id" : "SUN03",
                    "remark" : "random remark",
                    "start_time" : "ST",
                    "siteid" : "SUN03",
                    "user" : "abcd@philips.com",
                    "type" : "maintenance",
                    "end_time" : "ET"
                }
        self.model.get_all_sites_under_maintenance = MagicMock(name="get_all_sites_under_maintenance",
                                                               return_value={'rules': [rule, rule2]})
        self.model.set_cache = MagicMock(name='set_cache')
        self.model.collection.find = MagicMock(name='find', return_value = [{'nodename':'idm01.isyn.net'}])
        self.model.update_rules_in_cache()
        self.model.set_cache.assert_has_calls([
            call({'user': 'abcd@philips.com', 'remark': 'random remark',
            'siteid': 'SUN02', 'nodename': 'idm01.isyn.net', 'start_time': 'ST', '_id': 'SUN02_idm01.isyn.net',
            'type': 'maintenance', 'end_time': 'ET'}),
            call({'remark': 'random remark', 'start_time': 'ST',
            'siteid': 'SUN03', 'user': 'abcd@philips.com', '_id': 'SUN03',
            'type': 'maintenance', 'end_time': 'ET'})])

    def test_set_cache(self):
        rule = {'_id': 'SIT', 'nodename': 'N1'}
        self.model.set_cache(rule)
        self.redis.hset.assert_called_once_with(self.model.hash_set, 'SIT', self.model.get_hash_val.return_value)

    def test_get_rules(self):
        self.site_rules.SiteRulesQuery = MagicMock(name='SiteRulesQuery')
        self.site_rules.SiteRulesQuery.get_filter.return_value = {'siteid': 'ABC', 'type': 'assignment'}
        self.model.collection.aggregate.return_value = [
            {'siteid': 'ABC', 'type': 'assignment', 'user': 'user1', 'start_time': '2018-07-02 22:00:00'}]
        self.assertEqual(self.model.get_rules({'siteid': 'ABC', 'type': 'assignment'}), {
            'result': [{'siteid': 'ABC', 'type': 'assignment', 'user': 'user1', 'start_time': '2018-07-02 22:00:00'}]})

    def test_save_assignment_rule(self):
        self.assignment_request = {'siteid': 'ABC', 'type': 'assignment', 'user': 'user1'}
        self.mock_datetime.datetime.now.return_value = '2018-07-02 22:00:00'
        self.model.block_rule_update = MagicMock(name='block_rule_update')
        self.model.block_rule_update.return_value = True
        self.model.save_assignment_rule(self.assignment_request)
        self.model.collection.insert_one.assert_called_once_with({
            'siteid': 'ABC',
            'user': 'user1',
            'type': 'assignment',
            'start_time': '2018-07-02 22:00:00'
        })

    def test_block_save_assignment_rule(self):
        self.assignment_request = {'siteid': 'ABC', 'type': 'assignment', 'user': 'user1'}
        self.model.block_rule_update = MagicMock(name='block_rule_update')
        self.model.block_rule_update.return_value = False
        self.assertEqual(self.model.save_assignment_rule(self.assignment_request), {'valid': False})

    def test_block_rule_update(self):
        self.model.collection.find.return_value = {'siteid': 'ABC00', 'type': 'assignment', 'user': 'user1'}
        self.model.block_rule_update('ABC00', 'assignment')
        self.model.collection.find.assert_called_once_with({'siteid': 'ABC00', 'type': 'assignment'})
        self.assertEqual(self.model.block_rule_update('ABC00', 'assignment'), False)

    def test_delete_site_assignment_rule(self):
        self.model.delete_site_assignment_rule('ABC')
        self.model.collection.delete_one.assert_called_once_with({'type': 'assignment', 'siteid': 'ABC'})

    def test_delete_site_maintenance_rule(self):
        self.model.delete_site_maintenance_rule('ABC')
        self.model.collection.delete_one.assert_called_once_with({'type': 'maintenance', '_id': 'ABC'})

    def test_delete_node_maintenance_rule(self):
        self.model.delete_node_maintenance_rule('ABC', '192.178')
        self.model.collection.delete_one.assert_called_once_with({'type': 'maintenance', '_id': 'ABC_192.178'})

if __name__== '__main__':
    unittest.main()


