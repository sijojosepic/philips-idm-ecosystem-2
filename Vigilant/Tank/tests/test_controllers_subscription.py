import unittest
from mock import patch, MagicMock, call

class TankSubscriptionControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.models': self.mock_tank.models,
            'tank.models.subscription': self.mock_tank.models.subscription,
            'tank.models.subscription.SubscriptionModel': self.mock_tank.models.subscription.SubscriptionModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_logger': self.mock_tank.util.get_logger,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import subscription
        self.subscription = subscription
        self.obj = self.mock_tank.models.subscription
        self.mock_tank.models.subscription.SubscriptionModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.subscription.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_groups(self):
        self.obj.get_groups.return_value = "value"
        self.assertEqual(self.subscription.get_groups('abc','path'), "value")

    def test_get_names(self): 
        self.obj.get_names.return_value = "value"
        self.assertEqual(self.subscription.get_names('abc','path'), "value")

    def test_get_subscriptions(self):
        self.obj.get_subscriptions.return_value = "value"
        self.assertEqual(self.subscription.get_subscriptions('abc'), "value")

    def test_get_path(self):
        self.obj.get_path.return_value = "value"
        self.assertEqual(self.subscription.get_path('abc', 'path'), "value")

    def test_get_package_pcm_info(self):
        self.obj.get_package_pcm_info.return_value = 'value'
        self.assertEqual(self.subscription.get_package_pcm_info('abc'), 'value')


if __name__ == '__main__':
    unittest.main()