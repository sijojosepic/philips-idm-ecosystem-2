import unittest
from mock import patch, MagicMock, call

class TankDirectoryControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.directory': self.mock_tank.models.directory,
            'tank.models.directory.DirectoryModel': self.mock_tank.models.directory.DirectoryModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import directory
        self.directory = directory
        self.obj = self.mock_tank.models.directory
        self.mock_tank.models.directory.DirectoryModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.directory.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_directories(self):
        self.obj.get_directories.return_value = "value"
        self.assertEqual(self.directory.get_directories('mongo', 'site_id'), "value")

    def test_save_directory(self): 
        self.obj.save_directory.return_value = "value"
        self.assertEqual(self.directory.save_directory('mongo'), "value")
    
    def test_update_directory(self):
        self.obj.update_directory.return_value = "value"
        self.assertEqual(self.directory.update_directory('mongo'), "value")

    def test_delete_directory(self): 
        self.obj.delete_directory.return_value = "value"
        self.assertEqual(self.directory.delete_directory('mongo'), "value")

   
if __name__ == '__main__':
    unittest.main()