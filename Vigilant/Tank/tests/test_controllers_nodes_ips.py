import unittest
from mock import patch, MagicMock, call

class TankNodes_ipsControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.nodes_ips': self.mock_tank.models.nodes_ips,
            'tank.models.nodes_ips.NodesIPInventoryModel': self.mock_tank.models.nodes_ips.NodesIPInventoryModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import nodes_ips
        self.nodes_ips = nodes_ips
        self.obj = self.mock_tank.models.nodes_ips
        self.mock_tank.models.nodes_ips.NodesIPInventoryModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.nodes_ips.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_available_site_ips(self):
        self.obj.get_available_site_ips.return_value = "value"
        self.assertEqual(self.nodes_ips.get_available_site_ips('mongo', 'site_id'), "value")

    def test_get_reserve_site_ips(self):
        self.obj.get_reserve_site_ips.return_value = "value"
        self.assertEqual(self.nodes_ips.get_reserve_site_ips('mongo', 'site_id'), "value")

    def test_get_ips(self):
        self.obj.get_ips.return_value = "value"
        self.assertEqual(self.nodes_ips.get_ips('mongo', 'site_id'), "value")

    def test_get_nodes(self):
        self.obj.get_nodes.return_value = "value"
        self.assertEqual(self.nodes_ips.get_nodes('mongo', 'site_id'), "value")
    
    def test_set_ip_status(self):
        self.obj.set_ip_status.return_value = "value"
        self.assertEqual(self.nodes_ips.set_ip_status('mongo'), "value")

    def test_get_consumed_site_ips(self):
        self.obj.get_consumed_site_ips.return_value = "value"
        self.assertEqual(self.nodes_ips.get_consumed_site_ips('mongo', 'site_id'), "value")

   
if __name__ == '__main__':
    unittest.main()