import unittest
from mock import patch, MagicMock, call

class TankRacksControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.racks': self.mock_tank.models.racks,
            'tank.models.racks.RackModel': self.mock_tank.models.racks.RackModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import racks
        self.racks = racks
        self.obj = self.mock_tank.models.racks
        self.mock_tank.models.racks.RackModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.racks.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_rack_details_by_location_id(self):
        self.obj.get_rack_details_by_location_id.return_value = "value"
        self.assertEqual(self.racks.get_rack_details_by_location_id('mongo', 'site_id', 'location_id'), "value")

    def test_create_rack_details_by_location_id(self):
        self.obj.create_rack_details_by_location_id.return_value = "value"
        self.assertEqual(self.racks.create_rack_details_by_location_id('mongo'), "value")

    def test_update_rack_details_by_location_id(self):
        self.obj.update_rack_details_by_location_id.return_value = "value"
        self.assertEqual(self.racks.update_rack_details_by_location_id('mongo'), "value")

    def test_get_locations(self): 
        self.obj.get_locations.return_value = "value"
        self.assertEqual(self.racks.get_locations('mongo', 'site_id'), "value")

    def test_delete_location_details(self):
        self.obj.delete_location_details.return_value = "value"
        self.assertEqual(self.racks.delete_location_details('mongo', 'site_id', 'location_id'), "value")

   
if __name__ == '__main__':
    unittest.main()