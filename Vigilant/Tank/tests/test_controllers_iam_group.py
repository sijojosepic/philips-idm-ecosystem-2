import unittest
from mock import patch, MagicMock, call

class TankIam_groupControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.iam_group': self.mock_tank.models.iam_group,
            'tank.models.iam_group.IAMGroupModel': self.mock_tank.models.iam_group.IAMGroupModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import iam_group
        self.iam_group = iam_group

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.iam_group.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_get_all_groups(self):
        obj = self.mock_tank.models.iam_group
        self.mock_tank.models.iam_group.IAMGroupModel.return_value = obj
        obj.get_all_groups.return_value = "value"
        self.assertEqual(self.iam_group.get_all_groups('abc'), "value")

if __name__ == '__main__':
    unittest.main()