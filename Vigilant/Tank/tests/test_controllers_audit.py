import unittest
from mock import patch, MagicMock, call

class TankAuditControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.audit': self.mock_tank.models.audit,
            'tank.models.audit.AuditDistributionModel': self.mock_tank.models.audit.AuditDistributionModel,
            'tank.models.audit.AuditDeploymentModel': self.mock_tank.models.audit.AuditDeploymentModel,
            'tank.models.audit.PackageDetails': self.mock_tank.models.audit.PackageDetails,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import audit
        self.audit = audit

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.audit.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)


    def test_get_distribution_data(self):
        obj = self.mock_tank.models.audit
        self.mock_tank.models.audit.AuditDistributionModel.return_value = obj
        obj.get_data.return_value = "value"
        self.assertEqual(self.audit.get_distribution_data('abc'), "value")


    def test_get_deployment_data(self):
        obj = self.mock_tank.models.audit
        self.mock_tank.models.audit.AuditDeploymentModel.return_value = obj
        obj.get_data.return_value = "value"
        self.assertEqual(self.audit.get_deployment_data('abc'), "value")


    def test_get_package_details(self):
        obj = self.mock_tank.models.audit
        self.mock_tank.models.audit.PackageDetails.return_value = obj
        obj.get_data.return_value = "value"
        self.assertEqual(self.audit.get_package_details('mongo','abc'), "value")

if __name__ == '__main__':
    unittest.main()
