import unittest
from mock import patch, MagicMock


class FakeQueryBuilder(object):
    def __init__(self, query):
        self.query = query


class TankIaMGroupModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.QueryBuilder = FakeQueryBuilder
        modules = {
            'tank.util': self.mock_tank.util,
            'datetime': self.mock_datetime
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import iam_roles
        self.roles = iam_roles
        self.roles.COLLECTIONs = {'IAM_ROLES': 'iam_roles'}
        self.mongodb = MagicMock(name='mongodb')
        self.model = self.roles.IAMRoleModel(self.mongodb)
        self.reprt_qry = self.roles.IAMRolesQuery({'id': 'id'})
        self.roles.IAMRolesQuery = MagicMock(name='IAMRolesQuery')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_projection(self):
        self.assertEqual(
            self.model.get_projection(),
            {
                '_id': 1,
                'group_id': 1,
                'managingOrganization': 1,
                'name': 1,
                'description': 1,
                'timestamp': self.roles.get_date_to_string_field_projection.return_value,
                'views': 1
            }
        )
        self.roles.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_save_roles(self):
        obj = {'id': 'id'}
        self.model.save_roles(obj)
        self.model.collection.update_one.assert_called_once_with(
            {'_id': 'id'}, {'$set': {'timestamp': self.roles.datetime.now.return_value}}, upsert=True
        )

    def test_get_matches(self):
        self.model.filter = {'filter': 'please'}
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.get_matches()
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])

    def test_roles_by_id(self):
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.filter = {'_id': 'ABC102-DEF304'}
        self.model.get_roles_by_id('ABC102-DEF304')
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])


    def test_get_all_roles(self):
        self.model.get_projection = MagicMock(name='get_projection')
        self.roles.IAMRolesQuery.return_value.get_filter.return_value = {'id': 'id'}
        self.model.get_all_roles({'id': 'id'})
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.roles.IAMRolesQuery.return_value.get_filter.return_value},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])


if __name__ == '__main__':
    unittest.main()
