import unittest
from mock import patch, MagicMock, call

class TankSite_updatesControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.site_updates': self.mock_tank.models.site_updates,
            'tank.models.site_updates.SiteUpdatesModel': self.mock_tank.models.site_updates.SiteUpdatesModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import site_updates
        self.site_updates = site_updates
        self.obj = self.mock_tank.models.site_updates
        self.mock_tank.models.site_updates.SiteUpdatesModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.site_updates.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_site_updates(self):
        self.obj.get_site_updates.return_value = "value"
        self.assertEqual(self.site_updates.get_site_updates('mongo', 'site_id'), "value")

    def test_save_site_updates(self): 
        self.obj.save_site_updates.return_value = "value"
        self.assertEqual(self.site_updates.save_site_updates('mongo'), "value")
    
    def test_update_site_updates(self):
        self.obj.update_site_updates.return_value = "value"
        self.assertEqual(self.site_updates.update_site_updates('mongo'), "value")

    def test_delete_site_updates(self): 
        self.obj.delete_site_updates.return_value = "value"
        self.assertEqual(self.site_updates.delete_site_updates('mongo'), "value")

   
if __name__ == '__main__':
    unittest.main()