import unittest

from mock import patch, MagicMock


class TankTechnicalIdModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import technical_id
        self.technical_id = technical_id
        self.technical_id.COLLECTIONS = {'PRODUCT_TECHNICAL_IDS': 'product_technical_ids'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.technical_id.TechnicalIdModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_all_technical_ids(self):
        self.model.collection.find.return_value =   [
            {
            "product_id": "RWS",
            "technical_id": 197196
            },
            {
            "product_id": "AWS",
            "technical_id": 197196
            },
            {
            "product_id": "vCenter",
            "technical_id": 203568
            },
            {
            "product_id": "CONCERTO",
            "technical_id": 187520
            },
            {
            "product_id": "ISPACS",
            "technical_id": 197196
            }]
        self.assertEqual(self.model.get_all_technical_ids(), {"RWS": 197196, "AWS": 197196,"vCenter": 203568,"CONCERTO": 187520,"ISPACS": 197196})
        self.model.collection.find.assert_called_once_with({}, {'product_id': '$product_id', 'technical_id': '$technical_id', '_id': 0})

    def test_get_technical_id(self):
        self.model.collection.find_one.return_value =   {"technical_id": 197196 }
        self.assertEqual(
        self.model.get_technical_id('ABC_ISP'), {"technical_id": 197196 })
        self.model.collection.find_one.assert_called_once_with(
            {'product_id':'ABC_ISP'}, {'technical_id': '$technical_id', '_id': 0}
        )

if __name__ == '__main__':
    unittest.main()
