import unittest
from mock import patch, MagicMock
import calendar
import time


class TankDeviceModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import device
        self.device = device
        self.device.COLLECTIONS = {'DEVICES': 'devices'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.device.DeviceModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_device(self):
        self.model.collection.aggregate.return_value = [{"name": "dell", "type": "SAN", "_id": 1}]
        self.assertEqual(
            self.model.get_devices(),
            {'results': [{"name": "dell", "type": "SAN", "_id":1}]}
        )
        self.model.collection.aggregate.assert_called_once_with([{"$sort": {"_id": -1}}])

    def test_save_device(self):
        device_payload = {"name": "HP", "type": "SAN"}
        self.model.collection.find.return_value = []
        self.assertEqual(self.model.save_device(device_payload), {'code': 200})
        self.model.collection.find.assert_called_once_with(device_payload)
        self.model.collection.insert_one.assert_called_once_with({'_id': str(calendar.timegm(time.gmtime())),
                                                                  'type': 'SAN', 'name': 'HP'})

    def test_save_device_on_existing_device(self):
        device_payload = {"name": "dell", "type": "EsXi"}
        self.model.collection.find.return_value = device_payload
        self.assertEqual(self.model.save_device(device_payload), {'code': 501})
        self.model.collection.find.assert_called_once_with(device_payload)

    def test_update_device_on_existing_device_id(self):
        device_payload = {"name": "dell", "type": "SAN", "_id": '1234'}
        self.assertEqual(self.model.update_device(device_payload), {'code': 200})
        self.model.collection.update_one.assert_called_once_with({'_id': '1234'}, {'$set': {'name': 'dell', 'type': 'SAN'}})

    def test_delete_device(self):
        self.assertEqual(self.model.delete_device('1'), {'result': 200})
        self.model.collection.delete_one.assert_called_once_with({'_id': '1'})
        self.assertEqual(self.model.collection.delete_one.call_count, 1)
