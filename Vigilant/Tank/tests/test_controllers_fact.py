import unittest
from mock import patch, MagicMock, call


class TankFactControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.fact': self.mock_tank.models.fact,
            'tank.models.fact.SiteFactsModel': self.mock_tank.models.fact.SiteFactsModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_logger': self.mock_tank.util.get_logger,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import fact
        self.fact = fact
        self.obj = self.mock_tank.models.fact
        self.mock_tank.models.fact.SiteFactsModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1',
                                                            password='password1', protocol='protocol1')
        self.assertEqual(self.fact.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_get_site_hosts(self):
        self.obj.get_hosts.return_value = "value"
        self.assertEqual(self.fact.get_site_hosts('abc', 'site'), "value")

    def test_get_site_module_type(self):
        self.obj.get_site_module_type.return_value = "value"
        self.assertEqual(self.fact.get_site_module_type('abc', 'site'), "value")

    def test_get_host_detail(self):
        self.obj.get_host_detail.return_value = "value"
        self.assertEqual(self.fact.get_host_detail('abc', 'site', 'host'), "value")

    def test_get_site_modules(self):
        self.obj.get_modules.return_value = "value"
        self.assertEqual(self.fact.get_site_modules('abc', 'site'), "value")

    def test_get_site_item_details_no_if(self):
        self.obj.get_item_details.return_value = "value"
        self.assertEqual(self.fact.get_site_item_details('abc', 'site', 'module'), "value")

    def test_get_site_item_details_with_if(self):
        self.obj.get_item_details.return_value = "value"
        self.assertEqual(self.fact.get_site_item_details('abc', 'site', 'module', 'key'), "value")

    def test_get_site_key_details_no_if(self):
        self.obj.get_site_key_details.return_value = "value"
        self.assertEqual(self.fact.get_site_key_details('abc', 'site', 'module', 'val'), "value")

    def test_get_site_key_details_with_if(self):
        self.obj.get_site_key_details.return_value = "value"
        self.assertEqual(self.fact.get_site_key_details('abc', 'site', 'module', 'val', 'key'), "value")

    def test_get_site_components(self):
        self.obj.get_components.return_value = "value"
        self.assertEqual(self.fact.get_site_components('abc', 'site'), "value")

    def test_get_site_host_components(self):
        self.obj.get_host_components.return_value = "value"
        self.assertEqual(self.fact.get_site_host_components('abc', 'site', 'host'), "value")

    def test_get_site_one_component(self):
        self.obj.get_one_component.return_value = "value"
        self.assertEqual(self.fact.get_site_one_component('abc', 'site', 'component'), "value")

    def test_get_site_nodes(self):
        self.obj.get_node_details.return_value = "value"
        self.assertEqual(self.fact.get_site_nodes('abc', 'site'), "value")

    def test_get_host_count(self):
        self.obj.get_host_count.return_value = "value"
        self.assertEqual(self.fact.get_host_count('abc'), "value")

    def test_ecosystem_verisons(self):
        self.obj.ecosystem_verisons.return_value = "value"
        self.assertEqual(self.fact.ecosystem_verisons('abc'), "value")


if __name__ == '__main__':
    unittest.main()
