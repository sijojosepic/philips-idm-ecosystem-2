import unittest
from mock import patch, MagicMock, call

class Tankiam_rolesControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.iam_roles': self.mock_tank.models.iam_roles,
            'tank.models.iam_roles.IAMRoleModel': self.mock_tank.models.iam_roles.IAMRoleModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import iam_roles
        self.iam_roles = iam_roles

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.iam_roles.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_get_all_roles(self):
        obj = self.mock_tank.models.iam_roles
        self.mock_tank.models.iam_roles.IAMRoleModel.return_value = obj
        obj.get_all_roles.return_value = "value"
        self.assertEqual(self.iam_roles.get_all_roles('abc'), "value")

if __name__ == '__main__':
    unittest.main()