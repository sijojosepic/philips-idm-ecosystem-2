import unittest
from mock import patch, MagicMock, call

class TankDashboardControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.dashboard': self.mock_tank.models.dashboard,
            'tank.models.dashboard.DashboardModel': self.mock_tank.models.dashboard.DashboardModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import dashboard
        self.dashboard = dashboard
        self.obj = self.mock_tank.models.dashboard
        self.mock_tank.models.dashboard.DashboardModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.dashboard.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_get_dashboard_data(self):
        self.obj.get_dashboard.return_value = "value"
        self.assertEqual(self.dashboard.get_dashboard_data('abc'), "value")

    def test_get_unreachable_services(self):
        self.obj.get_unreachable_services.return_value = "value"
        self.assertEqual(self.dashboard.get_unreachable_services('abc'), "value")

    def test_get_unreachable_services_count(self):
        self.obj.get_unreachable_services_count.return_value = "value"
        self.assertEqual(self.dashboard.get_unreachable_services_count('abc'), "value")

    def test_update_case_number(self):
        self.mock_bottle.request.json =  {'siteid': '10','hostname':'20','service': '30','case_number':'12'}
        self.obj.update_case_number.return_value = "value"
        self.assertEqual(self.dashboard.update_case_number('abc'), "value")

    def test_get_namespace_container(self):
        self.obj.get_namespace_container.return_value = "value"
        self.assertEqual(self.dashboard.get_namespace_container('abc','container'), "value")

    def test_get_modules(self):
        self.obj.get_modules.return_value = "value"                                                                                                                                            
        self.assertEqual(self.dashboard.get_modules('abc'), "value")

    def test_get_stats(self):
        self.obj.get_stats.return_value = "value"
        self.assertEqual(self.dashboard.get_stats('abc'), "value")

    def test_get_products_stats(self):
        self.obj.get_prod_stats.return_value = "value"
        self.assertEqual(self.dashboard.get_products_stats('abc'), "value")

    def test_get_device_stats(self):
        self.obj.get_stats.return_value = "value"
        self.assertEqual(self.dashboard.get_device_stats('abc'), "value")

    def test_acknowledge_events(self):
        self.obj.acknowledge_events.return_value = "value"
        self.assertEqual(self.dashboard.acknowledge_events('abc'), "value")

    def test_unacknowledge_events(self):
        self.obj.unacknowledge_events.return_value = "value"
        self.assertEqual(self.dashboard.unacknowledge_events('abc','id'), "value")

    def test_get_app_services(self):
        self.obj.get_app_services.return_value = "value"
        self.assertEqual(self.dashboard.get_app_services('abc',['val']), "value")

    def test_create_case(self): 
        self.obj.create_case.return_value = "value"
        self.assertEqual(self.dashboard.create_case('abc'), "value")

if __name__ == '__main__':
    unittest.main()
