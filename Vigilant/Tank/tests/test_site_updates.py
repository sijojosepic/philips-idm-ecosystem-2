import unittest
from mock import patch, MagicMock

class TankSiteUpdatesModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import site_updates
        self.site_updates = site_updates
        self.site_updates.COLLECTIONS = {'SITEUPDATES': 'siteupdates'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.site_updates.SiteUpdatesModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_site_updates(self):
        self.model.collection.aggregate.return_value=[{'elementid':'ABC-1532690598', 'siteid':'ABC', 'message':'Added a node', 'created_by':'user1', 'created_on':'2018-07-02 22:00:00'}]
        self.assertEqual(self.model.get_site_updates('ABC'), {'result':[{'elementid':'ABC-1532690598', 'siteid':'ABC', 'message':'Added a node', 'created_by':'user1', 'created_on':'2018-07-02 22:00:00'}]})

    def test_save_site_updates(self):
        request_payload = {"message" : "Sample Notes", "siteid" : "DLP00","created_by" : "idmuser"}
        self.assertEqual(self.model.save_site_updates(request_payload), {'code':200})

    def test_save_site_updates_on_exceeding_maximum_notes_length(self):
        request_payload = {"message" : "Sample Notes"*1000, "siteid" : "DLP00","created_by" : "idmuser"}
        self.assertEqual(self.model.save_site_updates(request_payload), {'code':602})

    def test_delete_one_recent_activity(self):
        self.model.delete_site_updates('1')
        self.model.collection.delete_one.assert_called_once_with({'_id': '1'})

    def test_delete_multiple_activities(self):
        self.model.delete_site_updates('1,2,3')
        self.assertEqual(self.model.collection.delete_one.call_count, 3)

    def test_update_site_updates(self):
        request_payload = {"message" : "Sample Notes", "siteid" : "DLP00","created_by" : "idmuser", 'elementid':'ABC-1532690598'}
        self.model.collection.aggregate.return_value=[{'elementid':'ABC-1532690598', 'siteid':'ABC', 'message':'Added a node', 'created_by':'user1', 'created_on':'2018-07-02 22:00:00'}]
        self.assertEquals(self.model.update_site_updates(request_payload), {'code': 200})

    def test_not_update_site_updates(self):
        request_payload = {"message" : "Sample Notes", "siteid" : "DLP00","created_by" : "idmuser", 'elementid':'ABC-1532690598'}
        self.model.collection.aggregate.return_value=[]
        self.assertEquals(self.model.update_site_updates(request_payload), {'code': 502})
