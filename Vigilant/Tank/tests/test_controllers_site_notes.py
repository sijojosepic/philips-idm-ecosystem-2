import unittest
from mock import patch, MagicMock, call

class TankSiteNotesControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.model,
            'tank.models.site_notes': self.mock_tank.model.site_notes,
            'tank.models.site_notes.NotesModel': self.mock_tank.models.site_notes.NotesModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import site_notes
        self.site_notes = site_notes
        self.obj = self.mock_tank.model.site_notes
        self.mock_tank.model.site_notes.NotesModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.site_notes.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_get_notes(self):
        self.obj.get_notes.return_value = "value"
        self.assertEqual(self.site_notes.get_notes('abc','siteid'), "value")

    def test_save_notes(self):
        self.obj.save_notes.return_value = "value"
        self.assertEqual(self.site_notes.save_notes('abc'), "value")

    def test_delete_notes(self):
        self.obj.delete_notes.return_value = "value"
        self.assertEqual(self.site_notes.delete_notes('abc'), "value")

if __name__ == '__main__':
    unittest.main()
