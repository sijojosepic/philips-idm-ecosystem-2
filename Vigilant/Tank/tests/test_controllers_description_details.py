import unittest
from mock import patch, MagicMock, call

class Tankdescription_detailsControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.description_details': self.mock_tank.models.description_details,
            'tank.models.description_details.DescriptionModel': self.mock_tank.models.description_details.DescriptionModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import description_details
        self.description_details = description_details
        self.obj = self.mock_tank.models.description_details
        self.mock_tank.models.description_details.DescriptionModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.description_details.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_details(self):
        self.obj.get_details.return_value = "value"
        self.assertEqual(self.description_details.get_details('mongo','site_id'), "value")

    def test_save_details(self): 
        self.obj.save_details.return_value = "value"
        self.assertEqual(self.description_details.save_details('mongo'), "value")

    def test_update_details(self):
        self.obj.update_details.return_value = "value"
        self.assertEqual(self.description_details.update_details('mongo'), "value")


if __name__ == '__main__':
    unittest.main()