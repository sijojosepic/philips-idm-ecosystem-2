import unittest
from mock import patch, MagicMock, call

class TankSiteFieldControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.model,
            'tank.models.field': self.mock_tank.model.field,
            'tank.models.field.FieldModel': self.mock_tank.models.field.FieldModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_logger': self.mock_tank.util.get_logger,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import field
        self.field = field
        self.obj = self.mock_tank.model.field
        self.mock_tank.model.field.FieldModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.field.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_get_ual_component_version(self):
        self.obj.get_ual_component_version.return_value = "value"
        self.assertEqual(self.field.get_ual_component_version('abc','comp','ver'), "value")

    def test_get_field_component_details(self):
        self.obj.get_component_details.return_value = "value"
        self.assertEqual(self.field.get_field_component_details('abc'), "value")

    def test_get_unique_components(self):
        self.obj.get_unique_components.return_value = "value"
        self.assertEqual(self.field.get_unique_components('abc'), "value")

    def test_get_unique_models(self):
        self.obj.get_unique_models.return_value = "value"
        self.assertEqual(self.field.get_unique_models('abc'), "value")

    def test_get_unique_product_names(self):
        self.obj.get_unique_product_names.return_value = "value"
        self.assertEqual(self.field.get_unique_product_names('abc'), "value")

    def test_get_unique_tags(self):
        self.obj.get_unique_tags.return_value = "value"
        self.assertEqual(self.field.get_unique_tags('abc'), "value")


if __name__ == '__main__':
    unittest.main()