import unittest
from mock import patch, MagicMock


class TankDescriptionDetailsModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import description_details
        self.description_details = description_details
        self.description_details.COLLECTIONS = \
            {'DESCRIPTION_DETAILS': 'description_details'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.description_details.DescriptionModel(mongodb=self.mongodb, siteid='ALR00')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def aggregate_qry_assertion(self):
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'siteid': self.model.siteid}},
            {'$project': {'_id': 0, 'siteid': '$siteid', 'description': '$description', 'role': '$role',
                          'market_leader': '$market_leader', 'csm': '$csm', 'address': '$address', 'poc': '$poc',
                          'number': '$number'}}
        ])

    def test_get_details(self):
        self.model.collection.aggregate.return_value = [
            {'elementid': 'test-1532690598', 'siteid': self.model.siteid,
             'Description': 'Added a node', 'Role': 'Manager',
             'Level': 'user1', 'created_on': '2018-07-02 22:00:00',
             'Number': '123', 'POC': 'POC1',
             'Market_Leader': 'XYZ', 'Address': 'bng'}]
        self.assertEqual(self.model.get_details(), {'result': [
            {'elementid': 'test-1532690598', 'siteid': self.model.siteid,
             'Description': 'Added a node', 'Role': 'Manager',
             'Level': 'user1', 'created_on': '2018-07-02 22:00:00',
             'Number': '123', 'POC': 'POC1',
             'Market_Leader': 'XYZ', 'Address': 'bng'}]})
        self.aggregate_qry_assertion()

    def test_save_details_id_already_exist(self):
        req_payload = {"csm": "John", "market_leader": "John Hoe", "number": "+91-9876543210", "description": "Hospital Description",
                       "siteid": self.model.siteid, "poc": "Sample POC", "address": "Main Site Address", "role": "Manager"}
        self.model.collection.aggregate.return_value = req_payload
        self.assertEqual(self.model.save_details(req_payload), {'code': 501})
        self.aggregate_qry_assertion()

    def test_update_details_id_not_exist(self):
        req_payload = {"csm": "John", "market_leader": "John Hoe", "number": "+91-9876543210", "description": "Hospital Description",
                       "siteid": self.model.siteid, "poc": "Sample POC", "address": "Main Site Address", "role": "Manager"}
        self.model.collection.aggregate.return_value = {}
        self.assertEqual(self.model.update_details(req_payload), {'code': 502})
        self.aggregate_qry_assertion()

    def test_save_details(self):
        req_payload = {"csm": "John", "market_leader": "John Hoe", "number": "+91-9876543210", "description": "Hospital Description",
                       "poc": "Sample POC", "address": "Main Site Address", "role": "Manager", "siteid": self.model.siteid}
        self.model.collection.aggregate.return_value = []
        self.assertEqual(self.model.save_details(req_payload), {'code': 200})
        self.aggregate_qry_assertion()
        self.model.collection.update_one.assert_called_once_with(
            {'siteid': self.model.siteid}, {'$set': {"csm": "John", "market_leader": "John Hoe", "number": "+91-9876543210", "description": "Hospital Description",
                                                     "poc": "Sample POC", "address": "Main Site Address", "role": "Manager"}}, upsert=True
        )

    def test_update_details(self):
        req_payload = {"csm": "John", "market_leader": "John Hoe", "number": "+91-9876543210", "description": "Hospital Description",
                       "poc": "Sample POC", "address": "Main Site Address", "role": "Manager", "siteid": self.model.siteid}
        self.model.collection.aggregate.return_value = [req_payload]
        self.assertEqual(self.model.update_details(req_payload), {'code': 200})
        self.aggregate_qry_assertion()
        self.model.collection.update_one.assert_called_once_with(
            {'siteid': self.model.siteid}, {'$set': {"csm": "John", "market_leader": "John Hoe", "number": "+91-9876543210", "description": "Hospital Description",
                                                     "poc": "Sample POC", "address": "Main Site Address", "role": "Manager"}})
