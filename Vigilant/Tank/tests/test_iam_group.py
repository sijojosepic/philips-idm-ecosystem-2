import unittest
from mock import patch, MagicMock


class FakeQueryBuilder(object):
    def __init__(self, query):
        self.query = query


class TankIaMGroupModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.QueryBuilder = FakeQueryBuilder
        modules = {
            'tank.util': self.mock_tank.util,
            'datetime': self.mock_datetime
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import iam_group
        self.group = iam_group
        self.group.COLLECTIONs = {'IAM_GROUP': 'iam_groups'}
        self.mongodb = MagicMock(name='mongodb')
        self.model = self.group.IAMGroupModel(self.mongodb)
        self.grp_qry = self.group.IAMGroupQuery({'id': 'id'})
        self.group.IAMGroupQuery = MagicMock(name='IAMGroupQuery')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_projection(self):
        self.assertEqual(
            self.model.get_projection(),
            {
                '_id': 1,
                'managingOrganization': 1,
                'name': 1,
                'description': 1,
                'roles': 1,
                'timestamp': self.group.get_date_to_string_field_projection.return_value,
                'restricted': 1
            }
        )
        self.group.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_get_matches(self):
        self.model.filter = {'filter': 'please'}
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.get_matches()
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])

    def test_create_group(self):
        obj = {'id': 'id'}
        self.model.create_group(obj)
        self.model.collection.update_one.assert_called_once_with(
            {'_id': 'id'}, {'$set': {'timestamp': self.group.datetime.now.return_value
                , 'roles': []}}, upsert=True
        )

    def test_get_all_groups(self):
        self.model.get_projection = MagicMock(name='get_projection')
        self.group.IAMGroupQuery.return_value.get_filter.return_value = {'id': 'id'}
        self.model.get_all_groups({'id': 'id'})
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.group.IAMGroupQuery.return_value.get_filter.return_value},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])

    def test_get_group_by_id(self):
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.get_group_by_id('id')
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': {'_id': 'id'}},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])

    def test_assign_role_to_groups(self):
        self.model.get_group_by_id = MagicMock(name='group_by_id', return_value={'result': [{'id': 'id', 'roles': []}]})
        self.model.assign_role_to_groups('ROLE_ID', 'GRP_ID')
        self.model.collection.update_one.assert_called_once_with({'_id': 'GRP_ID'}, {'$set': {'roles': ['ROLE_ID']}}
                                                                 , upsert=True)


if __name__ == '__main__':
    unittest.main()