import unittest
from mock import patch, MagicMock, call

class TankdeviceControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.device': self.mock_tank.models.device,
            'tank.models.device.DeviceModel': self.mock_tank.models.device.DeviceModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import device
        self.device = device
        self.obj = self.mock_tank.models.device
        self.mock_tank.models.device.DeviceModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.device.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_device(self):
        self.obj.get_devices.return_value = "value"
        self.assertEqual(self.device.get_device('mongo'), "value")

    def test_save_device(self): 
        self.obj.save_device.return_value = "value"
        self.assertEqual(self.device.save_device('mongo'), "value")

    def test_update_device(self):
        self.obj.update_device.return_value = "value"
        self.assertEqual(self.device.update_device('mongo'), "value")

    def test_delete_device(self): 
        self.obj.delete_device.return_value = "value"
        self.assertEqual(self.device.delete_device('mongo'), "value")

if __name__ == '__main__':
    unittest.main()