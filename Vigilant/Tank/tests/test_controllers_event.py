import unittest
from mock import patch, MagicMock, call

class TankEventControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.event': self.mock_tank.models.event,
            'tank.models.country.CountryModel': self.mock_tank.models.country.CountryModel,
            'tank.tnconfig': self.mock_tank.tnconfig,
            'tank.tnconfig.HANDLER_SERVICES': self.mock_tank.tnconfig.HANDLER_SERVICES,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import event
        self.event = event

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.event.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)


    def test_get_historical_data(self):
        obj = self.mock_tank.models.event
        self.mock_tank.models.event.EventModel.return_value = obj
        obj.get_historical.return_value = "value"
        self.assertEqual(self.event.get_historical_data('abc'), "value")

    def test_get_handler_services(self):
        self.assertEqual(self.event.get_handler_services(), {'services': self.mock_tank.tnconfig.HANDLER_SERVICES})

    def test_get_handler_report(self):
        obj = self.mock_tank.models.event
        self.mock_tank.models.event.EventModel.return_value = obj
        obj.get_handler_report.return_value = "value"
        self.assertEqual(self.event.get_handler_report('service1','abc'), "value")

if __name__ == '__main__':
    unittest.main()