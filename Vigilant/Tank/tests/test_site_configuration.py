import re
import unittest
from mock import patch, MagicMock
import datetime


class TankSiteConfiguratuionModelTestCase(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import site_configuration
        self.site_configuration = site_configuration
        self.site_configuration.COLLECTIONS = {'SITE_CONFIGURATION': 'site_configuration'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = site_configuration.SiteConfigurationModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_data(self):
        mock_data  =   [{
                'hostname':'axy223.isyntax.net',
                'ram_size':4096,
                'cpu_cores':8,
                'module_type': 4096
            },
            {
                'hostname':'byu23.isyntax.net',
                'ram_size':8192,
                'cpu_cores':4,
                'module_type': 64
            }]
        self.model.collection.find.return_value = mock_data
        self.assertEqual(
            self.model.get_data('SHT13'),
            {'result': mock_data}
        )
        self.model.collection.find.assert_called_once_with({'_id': {
            '$regex':'SHT13'}
            },self.mock_tank.util.get_projection())
    
    def test_approve_config(self):
        self.assertEqual(
            self.model.approve_config('SHT13',[{
                'hostname':'axy223.isyntax.net',
                'ram_size':4096,
                'cpu_cores':8
            },
            {
                'hostname':'byu23.isyntax.net',
                'ram_size':8192,
                'cpu_cores':4
            }
            ]),
            {'result':True}
        )
        self.assertEqual(self.model.collection.update_one.call_count, 2)
        self.assertEqual(self.model.collection.update_one.call_args_list, 
            [   (({
                    '_id':'SHT13-axy223.isyntax.net'
                },
                {'$set':{
                    'gold_copy':{
                        'ram_size':4096,
                        'cpu_cores':8
                    }
                }}),),
                (({
                    '_id':'SHT13-byu23.isyntax.net'
                },
                {'$set':{
                    'gold_copy':{
                        'ram_size':8192,
                        'cpu_cores':4
                    }
                }}),)
           ])

if __name__ == '__main__':
    unittest.main()
