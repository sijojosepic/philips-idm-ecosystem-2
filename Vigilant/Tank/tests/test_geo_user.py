import re
import unittest
from mock import patch, MagicMock


class TankGeoUserModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_hashlib = MagicMock(name='hashlib')
        self.mock_jwt = MagicMock(name='jwt')
        self.mock_json = MagicMock(name='json')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_time = MagicMock(name='time')
        modules = {
            'hashlib': self.mock_hashlib,
            'jwt': self.mock_jwt,
            'json': self.mock_json,
            'datetime': self.mock_datetime,
            'time': self.mock_time
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import geo_user
        self.geo_user = geo_user
        self.geo_user.COLLECTIONS = {'USER': 'user', 'USER_AUDIT': 'user_audit'}
        self.geo_user.TOKEN_TIMEOUT = 300
        self.geo_user.TOKEN_SECRET = 'secret'

        self.mongodb = MagicMock(name='mongo_db')
        self.requestJson = { "loginId":"hit@philips.com", "password":"abc123" }
        self.status = {'valid': False}
        self.model = geo_user.GeoUserModel(self.mongodb, self.requestJson)


    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_password(self):
        self.model.collection.find.return_value = [{'password': 'abc123'}]
        self.assertEqual(self.model.get_password(), [{'password': 'abc123'}])
        self.model.collection.find.assert_called_once_with(
            {'_id': self.requestJson['loginId']},
            {"_id": 0, "password": 1}
        )

    @patch('hashlib.md5')
    def test_validate_credentials(self, hlib):
        self.model.get_password = MagicMock(name='get_password')
        self.model.get_token = MagicMock(name='get_token')
        self.model.get_password.return_value = [{'password':'abc123'}]
        self.model.get_token.return_value = 'asdf123'
        hlib.return_value.hexdigest.return_value = 'abc123'
        self.assertEqual(self.model.validate_credentials(), {'valid': True, 'tokenid': 'asdf123'})

    def test_get_token(self):
        self.mock_time.time.retrun_value = 1516709987.35
        self.mock_jwt.encode.return_value = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aW1l" \
        "c3RhbXAiOjE1MTY3MDk5ODcuMzQ4LCJleHAiOjE1MTY3MTAyODcuMzQ4LCJrZXkiOiJpZG0uaGl0Y29lQH" \
        "BoaWxpcHMuY29tIn0.FvPJ-Ai267jvjDspU3rflAqVgAx8DqJjoBTkcptWPqg"
        self.assertEqual(self.model.get_token(), "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aW1l" \
        "c3RhbXAiOjE1MTY3MDk5ODcuMzQ4LCJleHAiOjE1MTY3MTAyODcuMzQ4LCJrZXkiOiJpZG0uaGl0Y29lQH" \
        "BoaWxpcHMuY29tIn0.FvPJ-Ai267jvjDspU3rflAqVgAx8DqJjoBTkcptWPqg")



if __name__ == '__main__':
    unittest.main()

