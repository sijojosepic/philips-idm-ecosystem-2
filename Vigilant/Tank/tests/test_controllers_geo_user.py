import unittest
from mock import patch, MagicMock, call

class TankGeo_userControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.geo_user': self.mock_tank.models.geo_user,
            'tank.models.geo_user.GeoUserModel': self.mock_tank.models.geo_user.GeoUserModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import geo_user
        self.geo_user = geo_user

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.geo_user.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)


    def test_validate_credentials(self):
        obj = self.mock_tank.models.geo_user
        self.mock_tank.models.geo_user.GeoUserModel.return_value = obj
        obj.validate_credentials.return_value = "value"
        self.assertEqual(self.geo_user.validate_credentials('abc'), "value")

if __name__ == '__main__':
    unittest.main()