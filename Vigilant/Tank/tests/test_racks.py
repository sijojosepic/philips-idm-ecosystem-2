import unittest
from mock import patch, MagicMock


class TankRackDiagramModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import racks
        self.racks = racks
        self.racks.COLLECTIONS = {'RACKS': 'racks'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.racks.RackModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_locations(self):
        return_value = [{"siteid": "ABA01","location_id":"loc1", "_id":1,
                                                        "racks": [{"count": 18,"rackname": "rack 1",
                                                                   "Positions": [{"rackname": "rack 1",
                                                                                  "deviceName": "","details": "",
                                                                                  "position": 0}]}],
                                                        "location_name": "qwertyu"}]
        self.model.collection.aggregate.return_value = return_value
        self.assertEquals(self.model.get_locations('ABA01'), {'results': return_value})
        self.model.collection.aggregate.assert_called_once_with([{'$match': {'siteid': 'ABA01'}}, {'$sort': {'_id': -1}}])

    def test_get_rack_diagram(self):
        self.model.collection.find.return_value =[{"siteid": "qwertyui","location_id":"loc1",
                                                        "racks": [{"count": 18,"rackname": "rack 1",
                                                                   "Positions": [{"rackname": "rack 1",
                                                                                  "deviceName": "","details": "",
                                                                                  "position": 0}]}],
                                                        "location_name": "qwertyu"}]
        self.assertEqual(self.model.get_rack_details_by_location_id('qwertyui','loc1'),
                     {'result': [{"siteid": "qwertyui","location_id":"loc1",
                                  "racks": [{"count": 18,"rackname": "rack 1",
                                             "Positions": [{"rackname": "rack 1",
                                                            "deviceName": "","details": "",
                                                            "position": 0}]}],
                                  "location_name": "qwertyu"}]})
        self.model.collection.find.assert_called_once_with({'siteid': 'qwertyui', 'location_id': 'loc1'}, {'_id': 0})

    def test_update_rack_diagram_on_not_existing_location_id(self):
        rack_diagram_payload = {"siteid": "ALR00","location_id":"loc1",
                                     "racks": [{"count": 18,"rackname": "rack 1",
                                                "Positions": [{"rackname": "rack 1",
                                                               "deviceName": "","details": "",
                                                               "position": 0}]}],
                                     "location_name": "qwertyu"}
        self.model.collection.find.return_value = []
        self.assertEqual(self.model.update_rack_details_by_location_id(rack_diagram_payload), {'result': 502})
        self.model.collection.find.assert_called_once_with({'siteid': 'ALR00', 'location_id': 'loc1'}, {'_id': 0})

    def test_update_rack_diagram_on_existing_location_id(self):
        rack_diagram_payload = {"siteid": "ALR00","location_id":"loc1",
                                     "racks": [{"count": 18,"rackname": "rack 1",
                                                "Positions": [{"rackname": "rack 1",
                                                               "deviceName": "","details": "",
                                                               "position": 0}]}],
                                     "location_name": "qwertyu"}
        self.model.collection.find.return_value =[rack_diagram_payload]
        self.assertEqual(self.model.update_rack_details_by_location_id(rack_diagram_payload), {'result': 200})
        self.model.collection.find.assert_called_once_with({'siteid': 'ALR00', 'location_id': 'loc1'}, {'_id': 0})

    def test_create_rack_diagram_on_existing_location_id(self):
        rack_diagram_payload = {"siteid": "ALR00","location_id":"loc1",
                                     "racks": [{"count": 18,"rackname": "rack 1",
                                                "Positions": [{"rackname": "rack 1",
                                                               "deviceName": "","details": "",
                                                               "position": 0}]}],
                                     "location_name": "qwertyu"}
        self.model.collection.find.return_value = [rack_diagram_payload]
        self.assertEqual(self.model.create_rack_details_by_location_id(rack_diagram_payload), {'result': 501})
        self.model.collection.find.assert_called_once_with({'siteid': 'ALR00', 'location_id': 'loc1'}, {'_id': 0})

    def test_create_rack_diagram_on_new_location_id(self):
        rack_diagram_payload = {"siteid": "ALR00","location_id":"loc1",
                                     "racks": [{"count": 18,"rackname": "rack 1",
                                                "Positions": [{"rackname": "rack 1",
                                                               "deviceName": "","details": "",
                                                               "position": 0}]}],
                                     "location_name": "qwertyu"}
        self.model.collection.find.return_value = []
        self.assertEqual(self.model.create_rack_details_by_location_id(rack_diagram_payload), {'result': 200})
        self.model.collection.find.assert_called_once_with({'siteid': 'ALR00', 'location_id': 'loc1'}, {'_id': 0})

    def test_delete_rack_diagram_details_on_existing_location(self):
        self.model.collection.find.return_value =[{"siteid": "qwertyui","location_id":"loc1",
                                                        "racks": [{"count": 18,"rackname": "rack 1",
                                                                   "Positions": [{"rackname": "rack 1",
                                                                                  "deviceName": "",
                                                                                  "details": "","position": 0}]}],
                                                        "location_name": "qwertyu"}]
        self.model.delete_location_details('1','loc1')
        self.model.collection.delete_one.assert_called_once_with({'siteid': '1','location_id':'loc1'})
