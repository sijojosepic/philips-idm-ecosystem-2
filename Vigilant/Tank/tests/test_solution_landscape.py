from unittest import TestCase
from mock import patch, MagicMock, Mock
from bottle import FileUpload
from bottle import LocalResponse
from io import BytesIO

def mock_grid_fs(func):
    def _mock_grid_fs(*args, **kwargs):
        mocked_fs = Mock()
        with patch('gridfs.GridFS', Mock(return_value=mocked_fs)):
            db_file = Mock()
            mocked_fs.get = Mock(return_value=db_file)
            db_file.read = Mock(return_value="abc")
            return func(*args, **kwargs)
    return _mock_grid_fs

class TankSolutionLandscapeModelTestCase(TestCase):
    @mock_grid_fs
    def setUp(self):
        TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.date = MagicMock(name='datetime')
        modules = {'tank.util': self.mock_tank.util}
        self.module_patcher = patch.dict('sys.modules', modules)

        self.module_patcher.start()
        from tank.models import solution_landscape
        self.solution_landscape = solution_landscape
        self.solution_landscape.COLLECTIONS = {'SOLUTION_LANDSCAPE': 'solution_landscape'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.solution_landscape.SolutionLandscapeModel(self.mongodb)
        self.model.siteid = 'ALR00'

    def tearDown(self):
        TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_solution_landscape_file_not_found(self):
        self.model.collection.find_one.return_value = None
        expected_response = LocalResponse(status=404)
        actual_response = self.model.get_solution_landscape_file()
        self.assertEquals(actual_response.status, expected_response.status)

    def test_get_solution_landscape_file(self):
        self.model.collection.find_one.return_value = {'file': 'dummy.pdf', 'siteid': 'ALR00'}
        self.assertEquals(self.model.get_solution_landscape_file(), "YWJj\n")

    def test_save_solution_landscape_file(self):
        file = FileUpload(fileobj=BytesIO("abc"), name="dummy.pdf", filename="dummy.pdf")
        self.assertEqual(self.model.save_solution_landscape_file(file=file, modified_by="raman"), {'result': 200})

    def test_update_solution_landscape_file_not_found(self):
        self.model.collection.find_one.return_value = None
        file = FileUpload(fileobj=BytesIO("abc"), name="dummy.pdf", filename="dummy.pdf")
        self.assertEqual(self.model.update_solution_landscape_file(file=file, modified_by="raman"), {'result': 502})

    def test_update_solution_landscape_file(self):
        self.model.collection.find_one.return_value = {'file': 'dummy.pdf', 'siteid': 'ALR00'}
        file = FileUpload(fileobj=BytesIO("abc"), name="dummy.pdf", filename="dummy.pdf")
        self.assertEqual(self.model.update_solution_landscape_file(file=file, modified_by="raman"), {'result': 200})

    def test_save_solution_landscape_file_get_file_data_from_cache(self):
        self.solution_landscape.SolutionLandscapeModel.file_data_dict = {self.model.siteid + '-' + 'dummy.pdf': 1234}
        file = FileUpload(fileobj=BytesIO("abc"), name="dummy.pdf", filename="dummy.pdf")
        self.assertEqual(self.model.save_solution_landscape_file(file=file, modified_by="raman"), {'result': 200})
