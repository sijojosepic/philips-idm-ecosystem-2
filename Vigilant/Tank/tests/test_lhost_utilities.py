import re
import unittest
from mock import patch, MagicMock, call


class TestLhostUtilitiesModel(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle_mongo = MagicMock(name='bottle_mongo')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_phimutils = MagicMock(name='phimutils')
        modules = {
            'bottle_mongo': self.mock_bottle_mongo,
            'bottle': self.mock_bottle,
            'phimutils': self.mock_phimutils,
            'phimutils.plogging': self.mock_phimutils.plogging,
            'tank.util': self.mock_tank.util,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

        from tank.models import lhost_utilities
        self.lhost_utilities = lhost_utilities
        self.lhost_utilities.COLLECTIONS = {'LHOST_UTILITIES': 'lhost_utilities'}

        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.lhost_utilities.LhostUtilitiesModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_tags_list(self):
        self.model.collection.distinct.return_value = ["test", "prod"]
        self.assertEqual(
            self.model.get_tags_list(),
            {'tags': ["test", "prod"]}
        )

    def test_get_scanners_list(self):
        self.model.collection.distinct.return_value = ["XYZ", "ABC", "DEF", "PQR"]
        self.assertEqual(
            self.model.get_scanners_list(),
            {'scanners': ["ABC", "DEF", "PQR", "XYZ"]}
        )


if __name__== '__main__':
    unittest.main()