import unittest
from mock import patch, MagicMock, call

class TankSiteRulesControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.model,
            'tank.models.site_rules': self.mock_tank.model.site_rules,
            'tank.models.site_rules.RulesModel': self.mock_tank.models.site_rules.RulesModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import site_rules
        self.site_rules = site_rules
        self.obj = self.mock_tank.model.site_rules
        self.mock_tank.model.site_rules.RulesModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.site_rules.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_save_rules(self):
        self.obj.save_rules.return_value = "value"
        self.assertEqual(self.site_rules.save_rules('abc'), "value")

    def test_get_rules_with_siteid(self):
        self.obj.get_rules_with_siteid.return_value = "value"
        self.assertEqual(self.site_rules.get_rules_with_siteid('abc','site'), "value")

    def test_is_site_under_maintenance(self):
        self.obj.is_site_under_maintenance.return_value = "value"
        self.assertEqual(self.site_rules.is_site_under_maintenance('abc','site'), 'value')

    def test_is_node_under_maintenance(self):
        self.obj.is_node_under_maintenance.return_value = "value"
        self.assertEqual(self.site_rules.is_node_under_maintenance('abc','site','node'), 'value')

    def test_get_node_maintenance_schedule(self):
        self.obj.get_node_maintenance_schedule.return_value = "value"
        self.assertEqual(self.site_rules.get_node_maintenance_schedule('abc','site','node'), 'value')

    def test_get_rule_list(self):
        self.obj.get_rule_list.return_value = "value"
        self.assertEqual(self.site_rules.get_rule_list('node'), "value")

    def test_update_rules_in_cache(self):
        self.obj.update_rules_in_cache.return_value = "value"
        self.assertEqual(self.site_rules.update_rules_in_cache('abc'), "value")

    def test_get_rules(self):
        self.obj.get_rules.return_value = "value"
        self.assertEqual(self.site_rules.get_rules('abc'), "value")

    def test_save_assignment_rule(self):
        self.obj.save_assignment_rule.return_value = "value"
        self.assertEqual(self.site_rules.save_assignment_rule('abc'), "value")

    def test_delete_site_assignment_rule(self):
        self.obj.delete_site_assignment_rule.return_value = "value"
        self.assertEqual(self.site_rules.delete_site_assignment_rule('abc','site'), "value")

    def test_delete_site_maintenance_rule(self):
        self.obj.delete_site_maintenance_rule.return_value = "value"
        self.assertEqual(self.site_rules.delete_site_maintenance_rule('abc','site'), "value")

    def test_delete_node_maintenance_rule(self):
        self.obj.delete_node_maintenance_rule.return_value = "value"
        self.assertEqual(self.site_rules.delete_node_maintenance_rule('abc','site','node'), "value")

if __name__ == '__main__':
    unittest.main()
