import unittest
from mock import patch, MagicMock


class TankUserkeyModelsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
	        'tank.tnconfig': self.mock_tank.tnconfig,
	        'tank.tnconfig.COLLECTIONS': self.mock_tank.tnconfig.COLLECTIONS,
	    }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models.userkey import UserKeyModel
        self.UserKeyModel = UserKeyModel
        self.mongodb = MagicMock(name='mongo_db')
        self.classobj = self.UserKeyModel(self.mongodb)
        

    
    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    
    def test_get_userkey(self):
    	self.classobj.collection.aggregate = MagicMock(return_value = 'result')
        self.assertEqual( self.classobj.get_userkey('site'), {'result': list('result')})
    
    def test_save_userkey(self):
        self.classobj.collection.insert_one = MagicMock(return_value = True)
        self.assertEqual(self.classobj.save_userkey('host','site','key'),{'result': True} )
        self.assertEqual(self.classobj.collection.insert_one.call_count,1)


if __name__ == '__main__':
    unittest.main()