import unittest
from mock import patch, MagicMock


class IAMUserModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_datetime = MagicMock(name='datetime')
        modules = {
            'tank.util': self.mock_tank.util,
            'datetime': self.mock_datetime
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import iam_user
        self.iam_user = iam_user
        self.iam_user.COLLECTIONS = {'IAM_USER': 'iam_user'}
        self.mongodb = MagicMock(name='mongodb')
        self.model = self.iam_user.IAMUserModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    
    def test_get_all_user_details(self):
        self.model.get_user_details('xyz@philips.com')
        self.model.collection.find.assert_called_once_with({'loginId': 'xyz@philips.com'},
            {"_id":0,"givenName":1,"familyName":1,"groupID": 1,"approved": 1})

    def test_get_user_details(self):
        self.model.get_user_details('xyz@philips.com')
        self.model.collection.find.assert_called_once_with({'loginId': 'xyz@philips.com'},
            {"_id":0,"givenName":1,"familyName":1,"groupID": 1,"approved":1})

    def test_add_new_user(self):
        request_object = {'email': 'abc.xyz@philips.com',
                          'firstname': 'abc',
                          'lastname': 'xyz',
                          'groups': ['123-def-789', '321-pqr-456'],
                          'approved': True
                          }
        uuid = 'E123-F345-G789-H000'
        self.mock_datetime.datetime.now.return_value = "2018-09-20T06:32:26.812Z"
        self.model.add_new_user(uuid, request_object)
        self.model.collection.update_one.assert_called_once_with({'userUUID': 'E123-F345-G789-H000'},
                                    {'$set': {'loginId': 'abc.xyz@philips.com', 'givenName': 'abc', 'familyName': 'xyz',
                                    'timestamp': '2018-09-20T06:32:26.812Z', 'groupID': ['123-def-789', '321-pqr-456'], 'approved': True}}, upsert=True)

    def test_update_user_group(self):
        gids = ['G1']
        uuid = 'UID'
        approved = True
        self.model.update_user_group(gids, uuid, approved)
        self.model.collection.update_one.assert_called_once_with({'userUUID': 'UID'},{'$set': {'groupID': ['G1'], 'approved': True}}, upsert=False)

if __name__ == '__main__':
    unittest.main()