import unittest
from mock import patch, MagicMock

class ScannerModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.util': self.mock_tank.util
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import scanner
        self.scanner = scanner
        self.scanner.COLLECTIONS = {'SCANNERS': 'scanner'}
        self.mongodb = MagicMock(name='mongodb')
        self.model = self.scanner.ScannerModel(self.mongodb)

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_scanner_details(self):
        value = [{'scanner': 'S1', 'credentials': 'C1'}]
        self.model.collection.find = MagicMock(name="find", return_value=value)
        self.assertEqual(
            self.model.get_scanner_details(),
            {'result': [
                {'scanner': 'S1', 'credentials': 'C1'}
            ]}
        )

if __name__ == '__main__':
    unittest.main()