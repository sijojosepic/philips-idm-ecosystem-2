import unittest
from mock import patch, MagicMock, call

class TankSolution_landscapeControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.solution_landscape': self.mock_tank.models.solution_landscape,
            'tank.models.solution_landscape.SolutionLandscapeModel': self.mock_tank.models.solution_landscape.SolutionLandscapeModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import solution_landscape
        self.solution_landscape = solution_landscape
        self.obj = self.mock_tank.models.solution_landscape
        self.mock_tank.models.solution_landscape.SolutionLandscapeModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.solution_landscape.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_solution_landscape_file(self):
        self.obj.get_solution_landscape_file.return_value = "value"
        self.assertEqual(self.solution_landscape.get_solution_landscape_file('mongo', 'site_id'), "value")

    def test_save_solution_landscape_file(self): 
        self.obj.save_solution_landscape_file.return_value = "value"
        self.assertEqual(self.solution_landscape.save_solution_landscape_file('mongo'), "value")
    
    def test_update_solution_landscape_file(self):
        self.obj.update_solution_landscape_file.return_value = "value"
        self.assertEqual(self.solution_landscape.update_solution_landscape_file('mongo'), "value")

if __name__ == '__main__':
    unittest.main()