import unittest
from mock import patch, MagicMock, call

class TankSiteInfoXMLControllers(unittest.TestCase):

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.site_info_xml': self.mock_tank.models.site_info_xml,
            'tank.models.site_info_xml.': self.mock_tank.models.site_info_xml.SiteInfoXmlModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import site_info_xml
        self.site_info_xml = site_info_xml
        self.obj = self.mock_tank.models.site_info_xml
        self.mock_tank.models.site_info_xml.SiteInfoXmlModel.return_value = self.obj
        self.mock_tank.models.site_info_xml.SiteInfoXmlKeys.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_site_info_xml(self):
        self.obj.create_xml.return_value = "value"
        self.assertEqual(self.site_info_xml.get_site_info_xml('abc'), "value")

    def test_approve_config(self): 
        self.obj.get_keys.return_value = "value"
        self.assertEqual(self.site_info_xml.get_keys('abc'), "value")


if __name__ == '__main__':
    unittest.main()
