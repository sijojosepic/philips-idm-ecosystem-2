import unittest
from mock import patch, MagicMock, call

class TankCountryControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.models': self.mock_tank.models,
            'tank.models.country': self.mock_tank.models.country,
            'tank.models.country.CountryModel': self.mock_tank.models.country.CountryModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import country
        self.country = country

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    
    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.country.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_countries(self):
        obj = self.mock_tank.models.country
        self.mock_tank.models.country.CountryModel.return_value = obj
        obj.get_countries.return_value = "value"
        self.assertEqual(self.country.get_countries('abc'), "value")

if __name__ == '__main__':
    unittest.main()