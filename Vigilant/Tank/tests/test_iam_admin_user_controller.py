import unittest
from mock import patch, MagicMock


class IAMAdminUserControllerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_util = MagicMock(name='tank.util')
        self.mock_iam_user_model = MagicMock(name='tank.models.iam_user')
        self.mock_iam_user_controller = MagicMock(name='tank.controllers.iam_user')
        self.mongodb = MagicMock(name='mongo_db')
        self.mock_bottle = MagicMock(name='bottle')
        self.mock_bottle_req = MagicMock(name='bottle.request')
        modules = {
            'tank.models.iam_user': self.mock_iam_user_model,
            'tank.util': self.mock_tank,
            'bottle': self.mock_bottle,
            'tank.controllers.iam_user': self.mock_iam_user_controller
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import iam_admin_user
        self.iam_admin_user = iam_admin_user

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        uri, db_name, username, password, protocol = 'uri', 'db_name', 'username', 'password', 'protocol'
        self.iam_admin_user.get_app(uri, db_name, username, password, protocol)
        self.mock_tank.get_bottle_app_with_mongo.assert_called_once_with(uri='uri', db_name='db_name', username='username',
                                                password='password', protocol='protocol')

    def test_get_all_users(self):
        user_details = [{
            'givenName': 'xyz',
            'familyName': 'abc',
            'groupID': '123',
            'userUUID': 'QWER-1234',
            'loginId': 'xyz@philips.com'
        }]
        self.mock_iam_user_model.IAMUserModel().get_all_user_details.return_value = user_details
        self.assertEqual(self.iam_admin_user.get_all_users(self.mongodb), user_details)

    def test_update_user_group(self):
        self.mock_bottle_req.json.return_value = {
            'uuid' : 'ABC',
            'group_ids': ['1', '2'],
            'approved': True
        }
        # self.mock_iam_user_controller.Groups().assign_members_to_group.return_value = ''
        self.mock_iam_user_model.IAMUserModel().update_user_group.return_value = {'valid': True}
        self.assertEqual(self.iam_admin_user.update_user_group(self.mongodb), {'valid': True})


if __name__ == '__main__':
    unittest.main()

