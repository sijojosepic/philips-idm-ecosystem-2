import unittest
from mock import patch, MagicMock, PropertyMock


class FakeQueryBuilder(object):
    def __init__(self, query):
        self.query = query


class TankAuditModelTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_tank.util.QueryBuilder = FakeQueryBuilder
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'tank.util': self.mock_tank.util,
            'bottle': self.mock_bottle
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.models import audit
        self.audit = audit
        self.audit.COLLECTIONS = {'AUDIT': 'audit'}
        self.mongodb = MagicMock(name='mongo_db')
        self.model = self.audit.AuditModel(self.mongodb)
        self.package_details_model = self.audit.PackageDetails(self.mongodb, 'TES11')

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_matches(self):
        self.model.filter = {'filter': 'please'}
        self.model.get_projection = MagicMock(name='get_projection')
        self.model.collection.aggregate.return_value = iter(['test1', 'test2'])
        self.assertEqual(self.model.get_matches(), {'result': ['test1', 'test2']})
        self.model.collection.aggregate.assert_called_once_with([
            {'$match': self.model.filter},
            {'$project': self.model.get_projection.return_value},
            {'$sort': {'timestamp': -1}}
        ])

    def test_get_data(self):
        self.model.get_matches = MagicMock(name='get_matches')
        self.model.query_builder = MagicMock(name='QueryBuilder')
        self.assertEqual(self.model.get_data({'key1': 'val1'}), self.model.get_matches.return_value)
        self.assertEqual(self.model.filter, self.model.query_builder.return_value.get_filter.return_value)
        self.model.query_builder.assert_called_once_with({'key1': 'val1'})

    def test_audit_distribution_model_get_projection(self):
        model = self.model = self.audit.AuditDistributionModel(self.mongodb)
        self.assertEqual(
            model.get_projection(),
            {
                'status': '$status',
                'subscription': '$subscription',
                'siteid': '$siteid',
                'user': '$user',
                'timestamp': self.audit.get_date_to_string_field_projection.return_value,
                '_id': 0,
                'version': '$version',
                'size': '$size'
            }
        )
        self.audit.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_audit_deployment_model_get_projection(self):
        model = self.model = self.audit.AuditDeploymentModel(self.mongodb)
        self.assertEqual(
            model.get_projection(),
            {
                'status': '$status',
                'hosts': '$hosts',
                'user': '$user',
                'timestamp': self.audit.get_date_to_string_field_projection.return_value,
                'package': '$package',
                '_id': 0,
                'siteid': '$siteid',
                'version': '$version',
                'deployment_id': '$deployment_id'
            }
        )
        self.audit.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_audit_package_data_get_projection(self):
        self.model = self.audit.PackageAuditData(self.mongodb)
        self.assertEqual(
            self.model.get_projection(),
            {
                '_id': 0,
                'status': '$status',
                'version': '$version',
                'siteid': '$siteid',
                'user': '$user',
                'timestamp': self.audit.get_date_to_string_field_projection.return_value,
                'subscription_id':'$subscription_id',
                'name': '$subscription',
                'type': '$type',
                'size': '$size'
            }
        )
        self.audit.get_date_to_string_field_projection.assert_called_once_with('$timestamp')

    def test_audit_package_data_get_package_data(self):
        self.model = self.audit.PackageAuditData(self.mongodb)
        self.model.get_matches = MagicMock(name='get_matches')
        self.model.get_matches.return_value = {'result': ['val1', 'val2']}
        self.assertEqual(
            self.model.get_package_data('siteid'),
            {'result': ['val1', 'val2']}
        )

    def test_audit_package_data_get_deleted_data(self):
        self.model = self.audit.PackageAuditData(self.mongodb)
        self.model.get_matches = MagicMock(name='get_matches')
        self.model.get_matches.return_value = {'result': ['val1', 'val2']}
        self.assertEqual(
            self.model.get_deleted_data('TES11','subscription_id'),
            {'result': ['val1', 'val2']}
        )


    def test_audit_query_process_query(self):
        query_builder = self.audit.AuditQuery({'key1': 'val2'})
        query_builder.audit_type = 'type1'
        query_builder.get_between_dates_filter = MagicMock(name='get_between_dates_filter')
        query_builder.query = {'start_date': '2012-12-12', 'end_date': '2013-04-05', 'siteid': 'x,y'}
        query_builder.process_query()
        self.assertEqual(
            query_builder.query,
            {'type': 'type1', 'timestamp': query_builder.get_between_dates_filter.return_value, 'siteid':'x,y'}
        )
        query_builder.get_between_dates_filter.assert_called_once_with('2012-12-12', '2013-04-05')

    def test_packagedetails_get_data(self):
        self.model = self.audit.PackageDetails(self.mongodb,'TES11')
        type(self.model).audit_data = PropertyMock(return_value=[{"a":"b","x":"y"}])
        self.model._subscription_data = MagicMock(name='_subscription_data')
        self.model._subscription_data.return_value = {'package_file':'xyz.zip','product_name':'XYZ'}
        self.model._validate_data = MagicMock(name='_validate_data')
        self.model._validate_data.return_value = True
        self.model.__get_final_data = MagicMock(name='__get_final_data')
        self.model.__get_final_data.return_value = {'package_file':'xyz.zip','product_name':'XYZ'}
        self.model._get_product_value = MagicMock(name="_get_product_value")
        self.model._get_product_value.return_value = 100
        self.model.__update_package_data = MagicMock(name='__update_package_data')
        self.model.__update_package_data.return_value = True
        self.assertEqual(self.model.get_data(), {'package_data': [{'product_name': 'XYZ', 'product_value': 100,
                                                                   'package_list': [{'a': 'b', 'x': 'y',
                                                                                     'package_file': 'xyz.zip'}]}]})

    def test_is_not_deleted_data(self):
        mock_obj = MagicMock(name='get_deleted_data')
        mock_obj.get_deleted_data.return_value = {"result": True}
        self.audit.PackageAuditData = MagicMock(name="PackageAuditData", return_value=mock_obj)
        audit = {'subscription_id':'xyz'}
        self.assertEqual(self.package_details_model._is_not_deleted_data(audit), False)

    def test_is_not_deleted_data1(self):
        mock_obj = MagicMock(name='get_deleted_data')
        mock_obj.get_deleted_data.return_value = {"result": None}
        self.audit.PackageAuditData = MagicMock(name="PackageAuditData", return_value=mock_obj)
        audit = {'subscription_id': 'xyz'}
        self.assertEqual(self.package_details_model._is_not_deleted_data(audit), True)

    def test_packagedetails_audit_data(self):
        mock_obj = MagicMock(name='get_package_data')
        mock_obj.get_package_data.return_value = {"result": {'test':"test"}}
        self.audit.PackageAuditData = MagicMock(name="PackageAuditData", return_value=mock_obj)
        self.assertEqual(self.package_details_model.audit_data, {'test':"test"})

    def test___subscription_data(self):
        mock_obj = MagicMock(name='get_subscription_details')
        mock_obj.get_subscription_details.return_value = {"result": [{'test': "test"}]}
        self.mock_tank.models.subscription.SubscriptionModel.return_value=mock_obj
        subscription_id = 'xyz'
        self.assertEqual(self.package_details_model._subscription_data(subscription_id), [])

    def test__get_product_value(self):
        mock_obj = MagicMock(name="get_product_value")
        mock_obj.get_product_value = {'result': [{'product_value': None}]}
        self.mock_tank.models.product.ProductModel = MagicMock(name="ProductModel", return_value=mock_obj)
        self.assertEqual(self.package_details_model._get_product_value('ISP'), None)

    def test_product_value(self):
        product_dict = {'product_value': 100}
        self.assertEqual(self.package_details_model._product_value(product_dict), 100)



if __name__ == '__main__':
    unittest.main()
