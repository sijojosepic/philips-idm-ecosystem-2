import unittest
from mock import patch, MagicMock, call

class TankStateControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.models': self.mock_tank.models,
            'tank.models.state': self.mock_tank.models.state,
            'tank.models.state.StateModel': self.mock_tank.models.state.StateModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
            'tank.util.get_list_from_param': self.mock_tank.util.get_list_from_param,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import state
        self.state = state
        self.obj = self.mock_tank.models.state
        self.mock_tank.models.state.StateModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.state.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_key_state(self):
        self.obj.get_state.return_value = "value"
        self.assertEqual(self.state.get_key_state('abc','state','host','key'), "value")

    def test_get_all_siteids(self): 
        self.obj.get_siteids.return_value = "value"
        self.assertEqual(self.state.get_all_siteids('abc'), "value")

    def test_get_sites_thruk_mapping(self):
        self.obj.get_mappings.return_value = "value"
        self.assertEqual(self.state.get_sites_thruk_mapping('abc'), "value")

    def test_get_namespace_services(self):
        self.obj.get_namespace_services.return_value = "value"
        self.assertEqual(self.state.get_namespace_services('abc','site'), "value")


if __name__ == '__main__':
    unittest.main()