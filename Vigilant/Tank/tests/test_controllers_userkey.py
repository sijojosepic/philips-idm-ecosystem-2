import unittest
from mock import patch, MagicMock, call

class TankUserkeyControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.userkey': self.mock_tank.models.userkey,
            'tank.models.userkey.UserKeyModel': self.mock_tank.models.userkey.UserKeyModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import userkey
        self.userkey = userkey
        self.obj = self.mock_tank.models.userkey
        self.mock_tank.models.userkey.UserKeyModel.return_value = self.obj

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.userkey.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)

    def test_acknoledge_event(self):
        self.mock_bottle.request.json =  {'siteid': '10','hostname':'20','service': '30','status':'12'}
        self.obj.acknoledge_event.return_value = "value"
        self.assertEqual(self.userkey.acknoledge_event('abc'), "value")

    def test_save_userkey(self):
        self.obj.save_userkey.return_value = "value"
        self.assertEqual(self.userkey.save_userkey('abc'), "value")

if __name__ == '__main__':
    unittest.main()