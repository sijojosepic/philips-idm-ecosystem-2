import unittest
from mock import patch, MagicMock, call

class TechnicalIdControllerTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        modules = {
            'tank.models': self.mock_tank.models,
            'tank.models.technical_id': self.mock_tank.models.technical_id,
            'tank.models.technical_id.TechnicalIdModel': self.mock_tank.models.technical_id.TechnicalIdModel,
            'tank.util': self.mock_tank.util,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import technical_id
        self.technical_id = technical_id

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    
    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual( self.technical_id.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app )

    def test_get_all_technical_ids(self):
        obj = self.mock_tank.models.technical_id
        self.mock_tank.models.technical_id.TechnicalIdModel.return_value = obj
        obj.get_all_technical_ids.return_value = "value"
        self.assertEqual(self.technical_id.get_all_technical_ids('abc'), "value")
    
    def test_get_technical_id(self):
        obj = self.mock_tank.models.technical_id
        self.mock_tank.models.technical_id.TechnicalIdModel.return_value = obj
        obj.get_technical_id.return_value = "value"
        self.assertEqual(self.technical_id.get_technical_id('abc','productid'), "value")

if __name__ == '__main__':
    unittest.main()
