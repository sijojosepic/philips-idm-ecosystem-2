import unittest
from mock import patch, MagicMock, call

class TankGeo_user_auditControllersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tank = MagicMock(name='tank')
        self.mock_bottle = MagicMock(name='bottle')
        modules = {
            'bottle': self.mock_bottle,
            'bottle.request': self.mock_bottle.request,
            'tank.util': self.mock_tank.util,
            'tank.models': self.mock_tank.models,
            'tank.models.geo_user_audit': self.mock_tank.models.geo_user_audit,
            'tank.models.geo_user_audit.GeoUserAuditModel': self.mock_tank.models.geo_user_audit.GeoUserAuditModel,
            'tank.util.get_bottle_app_with_mongo': self.mock_tank.util.get_bottle_app_with_mongo,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from tank.controllers import geo_user_audit
        self.geo_user_audit = geo_user_audit

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_app(self):
        app = self.mock_tank.util.get_bottle_app_with_mongo(uri='uri1', db_name='db_name1', username='username1', password='password1', protocol='protocol1')
        self.assertEqual(self.geo_user_audit.get_app('uri1', 'db_name1', 'username1', 'password1', 'protocol1'), app)


    def test_set_rules_audit(self):
        obj = self.mock_tank.models.geo_user_audit
        self.mock_tank.models.geo_user_audit.GeoUserAuditModel.return_value = obj
        obj.set_rules_audit.return_value = "value"
        self.assertEqual(self.geo_user_audit.set_rules_audit('abc'), "value")

if __name__ == '__main__':
    unittest.main()