#### Manual setup for CollabNet Server
#### http://[IPAddress]:3343/csvn/ for administration page
#### Username: admin
#### Password: admin


export http_proxy=
rpm -Uhv http://167.81.182.65/phirepo/thirdparty/jre-7u40-linux-x64.rpm
export http_proxy="http://149.59.142.106:8080"
useradd -d /home/sc -m --shell /bin/bash sc -g users && passwd sc
cat >> /etc/sudoers.d/sc << "EOF"
sc ALL=(ALL) ALL
EOF

su sc
export http_proxy=
cd ~
wget http://sfo-ce-webhead/thirdparty/Collabnet/CollabNetSubversionEdge-4.0.2_linux-x86_64.tar.gz
sudo mkdir -v /scdata
sudo chown -v sc:users /scdata
export http_proxy="http://149.59.142.106:8080"

cd /scdata
tar -zxf ~/CollabNetSubversionEdge-4.0.2_linux-x86_64.tar.gz

sed -i '$ a\export JAVA_HOME=/usr/java/latest' ~/.bash_profile
source ~/.bash_profile
cd csvn
sudo -E bin/csvn install
bin/csvn start
sudo -E bin/csvn-httpd install

sudo reboot