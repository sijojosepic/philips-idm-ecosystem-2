#### Deploy REPO system
#### This should mirror an internal repo that is maintained and locked

#### Add a disk that has the capacity to hold the repo data plus product binaries
#### Recommend 500G

fdisk /dev/sdc
n
p
1
default
default
w

mkfs.ext4 /dev/sdc1
mkdir -pv /repo

blkid /dev/sdc1
echo "UUID=51b55a37-49d1-4201-88e5-000a28152c2f /repo ext4 defaults 1 2" >> /etc/fstab
mount -av

yum -y install nginx rsync
rsync -avzh -e ssh root@167.81.182.65:/var/www/html/phirepo /repo/
rsync -avzh -e ssh root@167.81.182.65:/repo/ /repo