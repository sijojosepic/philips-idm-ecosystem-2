#!/usr/bin/env python
import sys
import logging
import time
from phimutils.plogging import LOG_DATE_FORMAT, LOG_FORMAT
from phimutils.message import Message
from bottle import Bottle, request, BaseRequest
from celery import Celery

from gwconfig import (
    CELERY_CONFIGURATION_OBJECT,
    CELERY_PATH,
    PATH_TASK_MAP
)

BaseRequest.MEMFILE_MAX = 1024 * 1024 * 100
app = Bottle()

celery = Celery()
sys.path.append(CELERY_PATH)
celery.config_from_object(CELERY_CONFIGURATION_OBJECT)

log = logging.getLogger('phim.gateway')
log.setLevel(logging.INFO)
logging.Formatter.converter = time.gmtime
formatter = logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
handler_console = logging.StreamHandler()
handler_console.setFormatter(formatter)
log.addHandler(handler_console)


def post_to_task():
    if not request.json:
        log.error('no json')
        log.debug('%s', request.body.read())
        return
    log.debug('%s', request.json)
    if request.path in  ['/metricsdata','/billing','/siteconfiguration']:
        data = request.json
    else:
        data = Message.from_dict(request.json)
    log.debug('Received Request - %s, SiteID - %s', request.path, getattr(data, 'siteid', 'NA'))
    celery.send_task(PATH_TASK_MAP[request.path], [data], kwargs={})
    log.debug('Sent to task: %s', PATH_TASK_MAP[request.path])


for path in PATH_TASK_MAP.iterkeys():
    app.post(path, callback=post_to_task)


@app.get('/health')
def health():
    log.info('Health! %s', request.path)
    return "<p>OK</p>"


if __name__ == '__main__':
    #app.run(host='0.0.0.0', port=8080, debug=True, server='tornado')
    log.info('MemFileMax set to %s', BaseRequest.MEMFILE_MAX)
    app.run()
