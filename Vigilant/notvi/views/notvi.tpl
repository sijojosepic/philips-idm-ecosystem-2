<html>
<head>
    <title>no title</title>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0-rc-3/pure-min.css">
    <style scoped>
        .pure-button-red {
            background: rgb(255, 0, 0);
        }
    </style>
</head>
<body style="text-align:center">
    <h1>Events {{siteid}} - {{hostname}}</h1>
    <a class='pure-button pure-button-red' href='/notdel/{{siteid}}/{{hostname}}'>Empty</a>
    <table class="pure-table pure-table-striped" style="margin:auto">
        <thead>
            <tr>
                <th>Timestamp</th>
                <th>Service</th>
                <th>Type</th>
                <th>Status</th>
                <th>Hostaddress</th>
                <th>Output</th>
            </tr>
        </thead>
    %for item in notifications:
        <tr>
            <td>{{item['timestamp']}}</td>
            <td>{{item['service']}}</td>
            <td>{{item['type']}}</td>
            <td>{{item['status']}}</td>
            <td>{{item['hostaddress']}}</td>
            <td>{{item['output']}}</td>
        </tr>
    %end
    </table>
</body>
</html>
