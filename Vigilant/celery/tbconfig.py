from tchelp import load_config


## RabbitMQ EXCHANGES
ANALYTICS_CLINICAL_EXCHANGE = 'phim.analytics.clinicaldata.inbound'
ANALYTICS_DICOM_EXCHANGE = 'phim.analytics.dicomdata.inbound'

## RabbitMQ QUEUES
ANALYTICS_CLINICAL_QUEUE = 'phim.analytics.clinicaldata.inbound'
ANALYTICS_DICOM_QUEUE = 'phim.analytics.dicomdata.inbound'

## Generic Discovery Service Type
GENERIC_DISCOVERY_TYPE = ['windows-service','windows-process','windows-firewall', 'passive-check']

EXCHANGE_QUEUE_MAP = {
    ANALYTICS_CLINICAL_EXCHANGE: [ANALYTICS_CLINICAL_QUEUE],
    ANALYTICS_DICOM_EXCHANGE: [ANALYTICS_DICOM_QUEUE],
}

## QUEUE TRANSPORT
QUEUE_TRANSPORT_MAP = {
    'phim.analytics.clinicaldata.outbound': ANALYTICS_CLINICAL_EXCHANGE,
    'phim.analytics.dicomdata.outbound': ANALYTICS_DICOM_EXCHANGE,
}

## Poperator Type Class Map
POPERATOR_TYPE_CLASS_MAP = {
    'Nagios': {
        'module': 'poperator.configgenNagios',
        'class': 'ConfiggenNagios'
    },
    'NagiosNull': {
        'module': 'poperator.configgenNagiosNull',
        'class': 'ConfiggenNagiosNull'
    },
    'SWD': {
        'module': 'poperator.configgenSWD',
        'class': 'ConfiggenSWD'
    },
    'PCM_Manifest': {
        'module': 'poperator.configgenPCM',
        'class': 'ConfiggenPCM'
    },
    'SiteInfo': {
        'module': 'poperator.configgenSiteInfo',
        'class': 'ConfiggenSiteInfo'
    }
}

IN_TASK_MAP = {
    'softwaredistribution': {
        'subscriptions': 'phim_backoffice.persist.swd_subscribe'
    },
    'softwaredeployment': {
        'manifest': 'phim_backoffice.fact.pcm_manifest_collect'
    },
    'descriptor': {
        'site': 'phim_backoffice.descriptor.update_site_descriptors'
    },
    'dashboard': {
        'case_number': 'phim_backoffice.dashboard.update_case_number',
    },
    'subscription': {
        'create': 'phim_backoffice.subscription.create_subscription',
        'update': 'phim_backoffice.subscription.update_subscription'
    },
    'fact': {
        'delete_endpoint': 'phim_backoffice.fact.cleanup_endpoint'
    },
    'audit': {
        'distribution': 'phim_backoffice.audit.insert_distribution',
        'deployment': 'phim_backoffice.audit.insert_deployment',
        'login': 'phim_backoffice.audit.login',
        'case': 'phim_backoffice.audit.case_log',
        'ack': 'phim_backoffice.audit.ack_log',
	    'site_maintenance': 'phim_backoffice.audit.site_maintenance_log',
        'assignment': 'phim_backoffice.audit.assignment_log',
        'lhost': 'phim_backoffice.audit.lhost_log',
        'geo_rules': 'phim_backoffice.audit.geo_rules',
        'site_configuration': 'phim_backoffice.audit.site_configuration'
    },
    'siteconfiguration': {
        'update': 'phim_backoffice.site_configuration.update_approved_deviation'
    }
}

ALIVE_SERVICE = 'Product__IDM__Nebuchadnezzar__Alive__Status'

DEFAULT = {
    'RABBITMQ': {'URL': 'amqp://sb.phim.isyntax.net'},
    'REDIS': {
        #'URL': 'redis://username:psswrd@localhost:6379/0'
        'URL': 'redis://redis.phim.isyntax.net:6379'
    },
    ## ALIVE message configuration
    'ALIVE': {
        'SERVICE': ALIVE_SERVICE,
        'DEFAULT_HOSTNAME': 'localhost',
        'ADDRESS': '127.0.0.1',
        'CHECK_GAP': 6
    },
    ## MONGODB / Collections
    'MONGODB': {
        'URL': 'mongo.phim.isyntax.net:27017/',
        'RETRIES': 2,
        'RETRY_DELAY': 10,
        'DB_NAME': 'somedb',
        'USER_NAME': 'gAAAAABbHjSX0WI7pFNty46xnNrFPzqWY0ybrbI5K7332_JEdSh6B1RS8O17_I8G0uzNmB-yo8N5hxYC_sZLUsLUWYga1X0SNA==',
        'PASSWORD': 'gAAAAABbHjSXNR8n2z5Q6ks8lx1fHDHc50Z0n-AgTOg2VUJr0omyNqCi0ON4phYwiYt_wlG28prTn6Oa7TBSSDAMEs6-ro6wnuyjHVsNErKec-3z_peCFC4=',
        'PROTOCOL': 'mongodb'
    },
    'COLLECTIONS': {
        'ALIVES': 'alives',
        'AUDIT': 'audit',
        'DASHBOARD': 'dashboard',
        'EVENTS': 'events',
        'EXCEPTIONS': 'exceptions',
        'FACTS': 'facts',
        'NAMESPACES': 'namespaces',
        'SITES': 'sites',
        'STATES': 'states',
        'SUBSCRIPTIONS': 'subscriptions',
        'BILLING': 'billing',
        'SITE_CONFIGURATION': 'site_configuration',
        'SITE_MAINTENANCE_RULES': 'site_maintenance_rules',
        'NOTIFICATION_CONFIG': 'notification_config',
        'NOTIFICATION_STATUS': 'notification_status',
        'NOTIFICATION_AUDIT': 'notification_audit',
        'PRODUCT': 'product'
    },
    'FACT_MODULES': ['ISP', 'LN', 'SWD', 'Windows', 'Components', 'PCM', 'vCenter'],
    'DASHBOARD': {
        'DEFAULT_SITENAME': 'new_site',
        'PROBLEM_SITES': 'problem_sites',
        'UNREACHABLE_SITES': 'unreachable_sites',
        'UNREACHABLE_SITE_KEY': ALIVE_SERVICE,
        'UNREACHABLE_HOSTS': 'unreachable_hosts',
        'UNREACHABLE_HOST_KEY': 'Administrative__Philips__Host__Reachability__Status',
        'PROBLEMATIC_SITE_CONFIGURATION_KEY': 'Administrative__Philips__Site__Configuration__Status',
    },
    'STATE': {
        'HOSTADDRESS_KEY': 'Administrative__Philips__Host__Information__IPAddress',
        'IGNORE_NS': ['Administrative__Philips__Host__Reachability__Status',
                      'Administrative__Philips__Site__Configuration__Status']
    },
    'SUBSCRIPTION': {
        'DEFAULT_RULES': ['singlesitedeployment']
    },
    ## IBC (InstallBase Config)
    'IBC': {
        'SVN_URL': 'http://sc.phim.isyntax.net/svn/ibc/sites/',
        'SVN_USERNAME': 'operator',
        'SVN_PASSWORD': 'st3nt0r',
        'WORKING_DIRECTORY': '/var/philips/operator'
    },
    'POPERATOR_NAGIOS_DELAY_AFTER_COLLECTION': 2 * 60,
    ## ELASTICSEARCH
    # perf is an alias pointing to latest rolling index
    'PERFDATA': {
        'ELASTIC_INDEX': 'perf',
        'ELASTIC_TYPE': 'perfdata',
        'ELASTICS_URLS': ['el.phim.isyntax.net:9200']
    },
    'METRICSDATA': {
                'ELASTIC_INDEX': 'metrics',
                'ELASTIC_TYPE': 'metricsdata',
                'ELASTICS_URLS': ['el.phim.isyntax.net:9200']
    },
    'EVENTDATA': {
        'ELASTIC_INDEX': 'event',
        'ELASTIC_TYPE': 'eventdata',
        'ELASTICS_URLS': ['el.phim.isyntax.net:9200'],
        'PUSH_ENABLE': True
    },
    ## HeartBeat
    # SITE_EMAIL_MAP_FILE contains a single JSON object mapping siteid to email address
    'HB': {
        'TO_EMAIL': 'hbeat@stentor.com',
        'FROM_EMAIL': 'isyntaxserver@stentor.com',
        'SMTP_SERVER': 'smtp.phim.isyntax.net',
        'CODE_MAP_FILE': '/etc/philips/hb_code_map.conf',
        'SITE_EMAIL_MAP_FILE': '/etc/philips/emailmap.conf'
    },
    'BILLING': {
        'ACCESS_KEY': 'AKIAJZTXY6O6MCXBOO5SQ',
        'SECRET_KEY': 't435RkTU56S+nL4QbHjyL8Mr+xKSPIblx7q02Sc8w',
        'BUCKET': 'itgs-hiss-reporting-prod',
        'S3_FOLDER': 'hiss/ei-idm/dropin/'  # In S3 bucket, files will go inside this folder.

    },
    'SITE_INFO_MODULE_TYPES': {
        'Database': 'DBNode',
        'Hl7': 'HL7Node'
    },
    'FERNET_SECRET': '-BEhr4qfb-GMRMZU1YK-Q6ifPIrQkKstA5cHbxHaF9M=',
    'SITE_MAINTENANCE_RULES': 'SITE_MAINTENANCE_RULES',
    'SHINKEN_STATES':{
        'OK': 'OK',
        'WARNING': 'WARNING',
        'CRITICAL': 'CRITICAL'
    },
    'PROMETHEUS':{
        'NODE_EXPORTER_PORT': '9100'
    },
    'GENERIC_DISCOVERY': {
        'GD_SERVICE_TYPE': GENERIC_DISCOVERY_TYPE,
        'GD_APP_SERVICE_TYPE': GENERIC_DISCOVERY_TYPE + ['webapi']
    }
}

OUTER_NOTIFICATION = {
    'REQ_TIMEOUT': 10,
    'COUNTDOWN': 60,
    'MAX_RETRIES': 1,
    'NOTIFICATION_URL': 'https://notify'
}

# For Case Creation
CASE = {
    'REQ_TIMEOUT': 20,
    'COUNTDOWN': 60,
    'MAX_RETRIES': 1,
    'URL': 'http://161.85.111.192/IDMPortalServices/api/OneEMS/'
}

TBCONFIG_FILE = '/etc/philips/task/tbconfig.yml'


globals().update(load_config(DEFAULT, TBCONFIG_FILE))
