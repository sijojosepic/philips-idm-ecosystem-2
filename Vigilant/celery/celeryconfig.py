# Broker settings.

# BROKER_URL = 'amqp://'
BROKER_URL = 'amqp://sb.phim.isyntax.net'

CELERY_RESULT_BACKEND = 'redis://redis.phim.isyntax.net:6379'

# List of modules to import when celery starts.
CELERY_IMPORTS = ('phim_backoffice.fact', 'phim_backoffice.routers', 'phim_backoffice.perfdata',
                  'phim_backoffice.datastore', 'phim_backoffice.dashboard', 'phim_backoffice.descriptor',
                  'phim_backoffice.transport', 'phim_backoffice.event', 'phim_backoffice.state', 'phim_backoffice.persist',
                  'phim_backoffice.subscription', 'phim_backoffice.audit', 'phim_backoffice.billing.statistics',
                  'phim_backoffice.site_configuration', 'phim_backoffice.prometheus_event')

# disable if not needed (best practice)
CELERY_DISABLE_RATE_LIMITS = True

# name accepted content (best practice)
CELERY_ACCEPT_CONTENT = ['pickle', 'json', 'yaml']

# Routing for timeconsuming tasks
CELERY_ROUTES = {'phim_backoffice.fact.discovery_collect': {'queue': 'time-consuming'},
                 'phim_backoffice.fact.discovery_purge_missing': {'queue': 'time-consuming'},
                 'phim_backoffice.state.purge_extra_hosts_state': {'queue': 'time-consuming'},
                 'phim_backoffice.persist.poperator_gen': {'queue': 'time-consuming'},
                 'phim_backoffice.persist.generate_from_facts': {'queue': 'time-consuming'}
                 }

