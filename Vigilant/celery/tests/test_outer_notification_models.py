import unittest
from mock import MagicMock, patch, call


class ModelsTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
        modules = {
            # workaround to avoid the import issue, when the module is properly mocked
            # 'phim_backoffice.datastore': self.mock_phim_backoffice.datastore,
            'phim_backoffice.outer_notification.phim_backoffice': self.mock_phim_backoffice
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phim_backoffice.outer_notification import models
        self.module = models

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_query_filter(self):
        fields = {'siteid': 'S1', 'hostname': 'hostname',
                  'service': 'S2', 'a': 'b'}
        exp_res = {'siteid': 'S1', 'hostname': 'hostname',
                   'service': 'S2'}
        self.assertEquals(exp_res, self.module.Status.query_filter(**fields))

    def test_config_find_one(self):
        self.assertEquals(self.mock_phim_backoffice.datastore.find_one.return_value,
                          self.module.Config.find_one(siteid='S1', service='service'))
        self.mock_phim_backoffice.datastore.find_one.assert_called_once_with('NOTIFICATION_CONFIG',
                                                                             'S1-service'
                                                                             )

    def test_status_find_one(self):
        self.module.Status.query_filter = MagicMock(name='query_filter')
        self.assertEquals(self.mock_phim_backoffice.datastore.find_one.return_value,
                          self.module.Status.find_one(siteid='siteid',
                                                      hostname='hostname',
                                                      service='service'))

        self.mock_phim_backoffice.datastore.find_one.assert_called_once_with('NOTIFICATION_STATUS',
                                                                             self.module.Status.query_filter.return_value)
        self.module.Status.query_filter.assert_called_once_with(siteid='siteid',
                                                                hostname='hostname',
                                                                service='service')

    def test_status_delete_one(self):
        self.module.Status.query_filter = MagicMock(name='query_filter')
        self.assertEquals(self.mock_phim_backoffice.datastore.delete_one.return_value,
                          self.module.Status.delete_one(siteid='siteid',
                                                        hostname='hostname',
                                                        service='service'))
        self.mock_phim_backoffice.datastore.delete_one.assert_called_once_with('NOTIFICATION_STATUS',
                                                                               query_filter=self.module.Status.query_filter.return_value)
        self.module.Status.query_filter.assert_called_once_with(siteid='siteid',
                                                                hostname='hostname',
                                                                service='service')

    def test_status_upsert_one(self):
        data = {'d1': 'd2'}
        self.module.Status.query_filter = MagicMock(name='query_filter')
        self.assertEquals(self.mock_phim_backoffice.datastore.update_doc.return_value,
                          self.module.Status.upsert('st', 'config', data,
                                                    'notify_type'
                                                    ))
        update = {"$set": {'notify_type': 'st'}}
        self.mock_phim_backoffice.datastore.update_doc.assert_called_once_with('NOTIFICATION_STATUS',
                                                                               self.module.Status.query_filter.return_value, update, upsert=True)
        self.module.Status.query_filter.assert_called_once_with(d1='d2')

    def test_audit_insert_one(self):
        self.module.Audit.insert_one('data')
        self.mock_phim_backoffice.datastore.insert_one.assert_called_once_with('NOTIFICATION_AUDIT',
                                                                               'data')
