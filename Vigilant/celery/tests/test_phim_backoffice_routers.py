import unittest
from mock import patch, MagicMock, call

# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator

class TankRoutersPhim_backofficeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_functiontools = MagicMock(name='functiontools')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_json = MagicMock(name='json')
        self.mock_celery = MagicMock(name='celery')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_datastore = MagicMock(name='datastore')
        self.mock_celery.app.task = fakeorator_arg
        self.mock_functiontools.wraps = fakeorator_arg

        modules = {
            'datetime': self.mock_datetime,
            'functools': self.mock_functiontools,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.util.log,
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.event': self.mock_pb.event,
            'phim_backoffice.fact': self.mock_pb.fact,
            'phim_backoffice.audit': self.mock_pb.audit,
            'phim_backoffice.transport': self.mock_pb.transport,
            'phim_backoffice.celery': self.mock_celery,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.perfdata': self.mock_pb.perfdata,
            'phim_backoffice.state': self.mock_pb.state,
            'phim_backoffice.billing.statistics': self.mock_pb.statistics,
            'phim_backoffice.site_configuration': self.mock_pb.site_configuration,
            'phim_backoffice.prometheus_event': self.mock_pb.prometheus_event,
            'phim_backoffice.outer_notification': self.mock_pb.outer_notification,
            'phim_backoffice.outer_notification.task': self.mock_pb.outer_notification.task,
            'json': self.mock_json, 
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phim_backoffice import routers
        self.routers = routers

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_do_notification_data_type_is_alive(self):
        data = MagicMock()
        data.type = 'ALIVE'
        self.mock_pb.event.handle_event = MagicMock()
        self.mock_pb.event.handle_event.delay = MagicMock(return_value='eventdelay')
        self.routers.do_alive = MagicMock(return_value='delaycalled')
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.do_notification(data)
        self.assertEqual(self.routers.do_alive.delay.call_count, 1)
        self.assertEqual(self.mock_pb.event.handle_event.delay.call_count, 0)

    def test_do_notification_data_type_is_not_alive(self):
        data = MagicMock()
        data.type = 'NOTALIVE'
        self.mock_pb.event.handle_event = MagicMock()
        self.mock_pb.event.handle_event.delay = MagicMock(return_value='eventdelay')
        self.routers.do_alive = MagicMock(return_value='delaycalled')
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.do_notification(data)
        self.assertEqual(self.routers.do_alive.delay.call_count, 0)
        self.assertEqual(self.mock_pb.event.handle_event.delay.call_count, 1)
        self.mock_pb.outer_notification.task.notify.delay.assert_called_once_with(data)

    def test_do_alive(self):
        data = MagicMock()
        data.siteid = 'siteid'
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.do_alive(data)
        self.assertEqual(self.routers.upsert_one_by_id.call_count, 1)
        self.assertEqual(self.mock_pb.event.stale_alive.apply_async.call_count, 1)

    def test_do_perfdata(self):
        data = MagicMock()
        data.siteid = 'siteid'
        data.payload = 'payload'
        self.routers.handle_no_data = fakeorator_arg
        self.routers.do_perfdata(data)
        self.assertEqual(self.routers.bulk_elasticsearch_insert.delay.call_count, 1)

    def test_do_metricsdata(self):
        self.mock_pb.perfdata.push_metrics_data.delay = MagicMock(return_value=True)
        self.routers.handle_no_data = fakeorator_arg
        self.routers.do_metricsdata('data')
        self.assertEqual(self.routers.push_metrics_data.delay.call_count, 1 )


    def test_do_state(self):
        data = MagicMock()
        data.siteid = 'siteid'
        data.payload = 'payload'
        self.routers.handle_no_data = fakeorator_arg
        self.routers.do_state(data)
        self.assertEqual(self.routers.update_state.delay.call_count, 1)

    def test_do_discovery(self):
        data = MagicMock()
        data.siteid = 'siteid'
        data.payload = {'payload':'payload'}
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.do_discovery(data)
        self.assertEqual(self.mock_pb.fact.discovery_collect.delay.call_count, 1)

    def test_facts_update(self):
        data = MagicMock()
        data.siteid = 'siteid'
        data.payload = {'payload':'payload'}
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.facts_update(data)
        self.assertEqual(self.mock_pb.fact.update_facts.delay.call_count, 1)
        self.assertEqual(self.mock_pb.audit.update_package_status.delay.call_count, 1)

    def test_hbmessage2hbdashboard(self):
        data = MagicMock()
        data.siteid = 'siteid'
        data.payload['message'] = 'payloadmessage'
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.hbmessage2hbdashboard(data)
        self.assertEqual(self.mock_pb.transport.send_site_email_msg.delay.call_count, 1)

    def test_inbound(self):
        self.mock_pb.celery.app.send_task = MagicMock(return_value='val')
        self.routers.handle_no_data = fakeorator_arg
        self.routers.inbound('resource', 'action', 'payload')
        self.assertEqual(self.routers.app.send_task.call_count, 1)

    def test_deployment_status(self):
        data = MagicMock()
        data.payload = {'payload':'payload'}
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.deployment_status(data)
        self.assertEqual(self.mock_pb.audit.deployment_status.delay.call_count, 1)

    def test_isite_billing(self):
        data = MagicMock()
        self.routers.phim_backoffice = self.mock_pb
        self.mock_pb.billing.statistics.billing_task.delay = MagicMock(return_value='true')
        self.mock_json.loads = MagicMock(return_value='val')
        self.routers.handle_no_data = fakeorator_arg
        self.routers.isite_billing(data)
        self.assertEqual(self.routers.billing_task.delay.call_count, 1)
        self.routers.billing_task.delay.assert_called_once_with('val')

    def test_site_configuration(self):
        data = MagicMock()
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.site_configuration(data)
        self.assertEqual(self.mock_pb.site_configuration.handle_config_event.delay.call_count, 1)

    def test_prometheus_event(self):
        data = MagicMock()
        self.routers.handle_no_data = fakeorator_arg
        self.routers.phim_backoffice = self.mock_pb
        self.routers.prometheusdata(data)
        self.assertEqual(self.mock_pb.prometheus_event.prometheus_alerts.delay.call_count, 1)
        
if __name__ == '__main__':
    unittest.main()