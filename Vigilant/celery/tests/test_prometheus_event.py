import unittest
from mock import MagicMock, patch, call, PropertyMock

def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class PrometheusAlertsTestCase(unittest.TestCase):
        def setUp(self):
                unittest.TestCase.setUp(self)
                self.mock_pb = MagicMock(name='phim_backoffice')
                self.mock_pb.celery.app.task = fakeorator_arg
                self.mock_phimutils = MagicMock(name='phimutils')
                self.mock_tbconfig = MagicMock(name='tbconfig')
                modules = {
                        'tbconfig': self.mock_tbconfig,
                        'phim_backoffice.event': self.mock_pb.event,
                        'phim_backoffice.celery': self.mock_pb.celery,
                        'phimutils': self.mock_phimutils,
                        'phimutils.message': self.mock_phimutils.message
                }
                self.module_patcher = patch.dict('sys.modules', modules)
                self.module_patcher.start()
                import phim_backoffice.prometheus_event
                self.prometheus_event = phim_backoffice.prometheus_event
                self.prometheus_event.DASHBOARD['UNREACHABLE_SITE_KEY'] = "Administrative__Philips__Host__Reachability__Status"
                self.prometheus_event.SHINKEN_STATES['CRITICAL'] = "CRITICAL"
                self.prometheus_event.SHINKEN_STATES['OK'] = "OK"
                self.prometheus_event.SHINKEN_STATES['WARNING'] = "WARNING"
                self.prometheus_event.CRITICAL = 'CRITICAL'
                self.prometheus_event.WARNING = 'WARNING'
                self.prometheus_event.OK = 'OK'
                self.prometheus_event.UNREACHABLE_HOST = "Administrative__Philips__Host__Reachability__Status"
                self.prometheus_event_instance = self.prometheus_event.ParseData({'status': 'resolved','labels': {'instance': '1.1.1.2:8080','productid':'HSOP',
                        'job':'node_exporter','alertname':'CPU', 'event':'host', 'annotations':{'summary': 'Server under high load'}}})
                self.prometheus_event_instance_1 = self.prometheus_event.ParseData({'status': 'firing','labels': {'instance': '1.1.1.2:8080','productid':'HSOP',
                        'job':'node_exporter','alertname':'CPU','severity': 'critical','event':'service'},'annotations':{'summary': 'Server under high load'}})
                self.prometheus_event_instance_2 = self.prometheus_event.ParseData({'status': 'firing','labels': {'instance': '1.1.1.2:8080','productid':'HSOP',
                        'job':'node_exporter','alertname':'CPU','severity': 'warning','event': 'host'},'annotations':{'summary': 'Server under high load'}})


        def tearDown(self):
                unittest.TestCase.tearDown(self)
                self.module_patcher.stop()


        def test_get_message_data(self):
                print self.prometheus_event_instance.get_message_data()
                self.assertEqual(self.prometheus_event_instance.get_message_data(),{'service': 'Administrative__Philips__Host__Reachability__Status',
                 'hostname': '1.1.1.2', 'state': 'OK',
                        'hostaddress': '1.1.1.2', 'type': 'Recovery', 'payload': {'output': 'OK', 'back_office': True}})

        def test_hostname(self):
                self.assertEqual(
                        self.prometheus_event_instance.hostname, str("1.1.1.2"))

        def test_service(self):
                self.assertEqual(
                        self.prometheus_event_instance.service, "Administrative__Philips__Host__Reachability__Status")

        def test_service1(self):
                self.assertEqual(
                        self.prometheus_event_instance_1.service, "Product__HSOP__node_exporter__CPU__Status")

        def test_state1(self):
                self.assertEqual(
                        self.prometheus_event_instance.state, "OK")

        def test_state2(self):
                self.assertEqual(
                        self.prometheus_event_instance_1.state,"CRITICAL")

        def test_state3(self):
                self.assertEqual(
                        self.prometheus_event_instance_2.state,"WARNING")

        def test_payload(self):
                self.assertEqual(
                        self.prometheus_event_instance.payload,{'output': 'OK', 'back_office': True})

        def test_payload1(self):
                self.assertEqual(
                        self.prometheus_event_instance_1.payload,{'output': 'Server under high load', 'back_office': True})

        def test_payload2(self):
                self.assertEqual(
                        self.prometheus_event_instance_2.payload,{'output': 'Server under high load', 'back_office': True})

        def test_notification_type(self):
                self.assertEqual(
                        self.prometheus_event_instance.notification_type,'Recovery')

        def test_notification_type1(self):
                self.assertEqual(
                        self.prometheus_event_instance_1.notification_type,'Problem')

        def test_notification_type2(self):
                self.assertEqual(
                        self.prometheus_event_instance_2.notification_type,'Problem')

if __name__ == '__main__':
    unittest.main()
