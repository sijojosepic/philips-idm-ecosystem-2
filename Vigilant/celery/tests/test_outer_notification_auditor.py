import unittest
from mock import MagicMock, patch, call


class AuditorTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_functools = MagicMock(name='functools')
        self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
        modules = {
            'datetime': self.mock_datetime,
            'phim_backoffice.outer_notification.models': self.mock_phim_backoffice.models
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phim_backoffice.outer_notification import auditor
        self.module = auditor

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_auto_case_status_when_response(self):
        mock_resp = MagicMock(name='response')
        mock_resp.json.return_value = {'case_id': 2324}
        obj = self.module.AutoCaseAuditor('nt_type', 'data', mock_resp)
        self.assertEqual(0, obj.status)

    def test_auto_case_status_when_no_case_id(self):
        mock_resp = MagicMock(name='response')
        mock_resp.json.return_value = {}
        obj = self.module.AutoCaseAuditor('nt_type', 'data', mock_resp)
        self.assertEqual(1, obj.status)

    def test_auto_case_status_when_no_response(self):
        obj = self.module.AutoCaseAuditor('nt_type', 'data', None)
        self.assertEqual(1, obj.status)

    def test_auto_case_details_when_ok_response(self):
        mock_resp = MagicMock(name='repsone')
        mock_resp.json.return_value = {'case_id': 9898}
        obj = self.module.AutoCaseAuditor('nt_type', 'data', mock_resp)
        exp_status = [{'case_id': 9898}]
        self.assertEqual(exp_status, obj.details())

    def test_auto_case_details_when_ok_response_without_case_id(self):
        mock_resp = MagicMock(name='repsone')
        mock_resp.json.return_value = {}
        obj = self.module.AutoCaseAuditor('nt_type', 'data', mock_resp)
        exp_status = [{'case_id': 'NA'}]
        self.assertEqual(exp_status, obj.details())

    def test_auto_case_details_when_non_ok_response(self):
        obj = self.module.AutoCaseAuditor('nt_type', 'data', None)
        exp_status = None
        self.assertEqual(exp_status, obj.details())

    def test_auto_case_audit_info_when_details(self):
        data = {'siteid': 'S1', 'hostname': 'hostname', 'service': 'service'}
        obj = self.module.AutoCaseAuditor('nt_type', data, None)
        obj.details = MagicMock(name='details', return_value=[{'a': 'b'}])
        exp_status = {'status': 1, 'service': 'service', 'hostname': 'hostname',
                      'creation_time': self.mock_datetime.datetime.utcnow.return_value,
                      'siteid': 'S1', 'details': [{'a': 'b'}], 'notification_type': 'nt_type'}
        self.assertEqual(exp_status, obj.audit_info())

    def test_auto_case_audit_info_when_no_details(self):
        data = {'siteid': 'S1', 'hostname': 'hostname', 'service': 'service'}
        obj = self.module.AutoCaseAuditor('nt_type', data, None)
        obj.details = MagicMock(name='details', return_value=[{'a': 'b'}])
        exp_status = {'status': 1, 'service': 'service', 'hostname': 'hostname',
                      'creation_time': self.mock_datetime.datetime.utcnow.return_value,
                      'siteid': 'S1', 'details': [{'a': 'b'}], 'notification_type': 'nt_type'}
        self.assertEqual(exp_status, obj.audit_info())

    def test_audit_info(self):
        mock_AutoCaseAuditor = MagicMock(name='AutoCaseAuditor')
        self.module.AUDIT_MAPPING = {'auto_case': mock_AutoCaseAuditor}
        self.assertEqual(
            mock_AutoCaseAuditor.return_value.audit_info.return_value,
            self.module.audit_info('auto_case', 'data', 'repsone')
        )
        mock_AutoCaseAuditor.assert_called_once_with(
            'auto_case', 'data', 'repsone')

    def test_audit_decorator(self):
        def tester(*args, **kwargs):
            return 'response'
        self.module.audit_info = MagicMock(name='audit_info')
        decorated_tester = self.module.audit(tester)
        resp = decorated_tester('a', **{'nt_type': 'nt_type', 'json': 'cd'})
        self.module.audit_info.assert_called_once_with(
            'nt_type', 'cd', 'response')
        self.mock_phim_backoffice.models.Audit.insert_one.assert_called_once_with(
            self.module.audit_info.return_value)

    def test_audit_decorator_exception(self):
        from requests.exceptions import RequestException

        def tester(*args, **kwargs):
            raise RequestException('error...')
        self.module.audit_info = MagicMock(name='audit_info')
        decorated_tester = self.module.audit(tester)
        self.assertRaises(RequestException, decorated_tester,
                          'a', **{'nt_type': 'nt_type', 'json': 'cd'})
        self.module.audit_info.assert_called_once_with('nt_type', 'cd', None)
        self.mock_phim_backoffice.models.Audit.insert_one.assert_called_once_with(
            self.module.audit_info.return_value)


if __name__ == '__main__':
    unittest.main()
