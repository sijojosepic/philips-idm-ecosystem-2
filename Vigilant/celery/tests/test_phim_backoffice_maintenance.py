# -*- coding: utf-8 -*-
import unittest
from mock import MagicMock, patch, DEFAULT, call


class MaintenanceTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_celery = MagicMock(name='celery')
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_json = MagicMock(name='json')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.kvstore': self.mock_pb.kvstore,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'datetime': self.mock_datetime,
            'json': self.mock_json
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.maintenance
        self.maintenance = phim_backoffice.maintenance

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_is_site_under_maintanance(self):
        self.data = {
            'siteid': 'site01',
            'hostname': 'host01',
            'service': 'x__y__z',
            'type': 'PROBLEM',
            'timestamp': '2013-09-10T22:03:00.456000Z',
            'namespace' : {
                'category' : 'n',
                'attribute' : None,
                'object' : 'z',
                'vendor' : 'y',
                'dimension' : None
            },
        }
        self.maintenance.is_siteid_under_maintenance = MagicMock(name='is_siteid_under_maintenance', return_value = True)
        self.maintenance.is_node_under_maintenance = MagicMock(name='is_node_under_maintenance', return_value=False)
        self.assertEqual(self.maintenance.is_site_under_maintanance(self.data), [True, False])
        self.maintenance.is_siteid_under_maintenance.assert_called_once_with('site01', '2013-09-10T22:03:00.456000Z')
        self.maintenance.is_node_under_maintenance.assert_called_once_with('site01_host01', '2013-09-10T22:03:00.456000Z')

    def test_is_siteid_under_maintenance(self):
        self.maintenance.check_maintenance = MagicMock(name='check_maintenance', return_value=True)
        self.assertEqual(self.maintenance.check_maintenance('site01', '2013-09-10T22:03:00.456000Z'), True)
        self.maintenance.check_maintenance.assert_called_once_with('site01', '2013-09-10T22:03:00.456000Z')

    def test_is_node_under_maintenance(self):
        self.maintenance.check_maintenance = MagicMock(name='check_maintenance', return_value=False)
        self.assertEqual(self.maintenance.check_maintenance('site01_host01', '2013-09-10T22:03:00.456000Z'), False)
        self.maintenance.check_maintenance.assert_called_once_with('site01_host01', '2013-09-10T22:03:00.456000Z')

    def test_check_maintanance(self):
        self.maintenance.redis_check_and_update = MagicMock(name='redis_check_and_update')
        self.maintenance.redis_check_and_update.return_value = 'time_slots'
        self.mock_json.loads.return_value = {'start_time': 1, 'end_time': 3}
        self.mock_datetime.datetime.strptime.side_effect = [1, 3]
        self.assertEqual(self.maintenance.check_maintenance('site01_host01', 2), True)
        self.assertEqual(
            self.maintenance.redis_check_and_update.mock_calls,
            [call('site01_host01')]
        )

    def test_redis_check_and_update(self):
        self.maintenance.SITE_MAINTENANCE_RULES = 'SITE_MAINTENANCE_RULES'
        self.maintenance.get_hashset_val = MagicMock(name='get_hashset_val', return_value= None)
        self.maintenance.find_one.return_value = {'_id': 'site01_host01', 'start_time': '2019-07-16 10:00:00.000000',
                                                'end_time': '2019-07-19 18:30:00.000000'}
        time_slot = {"start_time": "2019-07-16 10:00:00.000000", "end_time": "2019-07-19 18:30:00.000000"}
        self.maintenance.get_hash_val = MagicMock(name='get_hash_val')
        self.maintenance.get_hash_val.return_value = time_slot
        self.maintenance.set_hashset_val = MagicMock(name='set_hashset_val')
        self.assertEqual(self.maintenance.redis_check_and_update('site01_host01'), time_slot)
        self.maintenance.get_hashset_val.assert_called_once_with('SITE_MAINTENANCE_RULES', 'site01_host01')
        self.maintenance.set_hashset_val.assert_called_once_with('SITE_MAINTENANCE_RULES', 'site01_host01', time_slot)

    def test_get_hash_val(self):
        self.mock_json.dumps.return_value = {"start_time": "2019-07-16 10:00:00.000000",
                                             "end_time": "2019-07-19 18:30:00.000000"}
        self.assertEqual(self.maintenance.get_hash_val({}), {"start_time": "2019-07-16 10:00:00.000000",
                                             "end_time": "2019-07-19 18:30:00.000000"})


if __name__ == '__main__':
    unittest.main()
