import unittest
from mock import MagicMock, patch


def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class TaskTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_datetime = MagicMock(name='datetime')
        self.mock_functools = MagicMock(name='functools')
        self.mock_celery = MagicMock(name='celery')
        self.mock_logger = MagicMock(name='logger')
        self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_phim_backoffice.celery.app.task = fakeorator_arg
        modules = {
            'phim_backoffice.maintenance': self.mock_phim_backoffice.maintenance,
            'phim_backoffice.celery': self.mock_phim_backoffice.celery,
            'phim_backoffice.outer_notification.handler': self.mock_phim_backoffice.handler
        }

        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phim_backoffice.outer_notification import task
        self.module = task
        self.module.OUTER_NOTIFICATION = {}
        self.module.OUTER_NOTIFICATION['COUNTDOWN'] = 10
        self.module.OUTER_NOTIFICATION['MAX_RETRIES'] = 1

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_notify_when_site_under_maintenance(self):
        mock_maintence = self.mock_phim_backoffice.maintenance
        mock_data = MagicMock()
        mock_data.to_dict.return_value = {'b': 'a'}
        mock_data.datetime = 'datetime'
        mock_maintence.is_site_under_maintanance.return_value = [True]
        self.module.notify(mock_data)
        mock_maintence.is_site_under_maintanance.assert_called_once_with({'b': 'a', 'timestamp': 'datetime'})
        self.assertEqual(0, self.mock_phim_backoffice.handler.NotificationHandler.call_count)

    def test_notify_when_site_not_under_maintenance(self):
        mock_maintence = self.mock_phim_backoffice.maintenance
        mock_data = MagicMock()
        mock_data.to_dict.return_value = {'b': 'a'}
        mock_data.datetime = 'datetime'
        mock_maintence.is_site_under_maintanance.return_value = [False]
        self.module.notify(mock_data)
        mock_maintence.is_site_under_maintanance.assert_called_once_with({'b': 'a', 'timestamp': 'datetime'})
        NotificationHandler = self.mock_phim_backoffice.handler.NotificationHandler
        NotificationHandler.assert_called_once_with({'b': 'a', 'timestamp': 'datetime'})
        NotificationHandler.return_value.run.assert_called_once_with()
