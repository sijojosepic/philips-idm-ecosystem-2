# import unittest
# from mock import MagicMock, patch


# class Fake(object):
#     """Create Mock()ed methods that match another class's methods."""

#     @classmethod
#     def imitate(cls, *others):
#         for other in others:
#             for name in other.__dict__:
#                 try:
#                     if name == '__init__':
#                         setattr(cls, name, lambda *args, **kwargs: None)
#                     else:
#                         setattr(cls, name, MagicMock())
#                 except (TypeError, AttributeError):
#                     pass
#         return cls


# def fakeorator_arg(*args, **kwargs):
#     def inner1(func):
#         def inner(*args, **kwargs):
#             return func(*args, **kwargs)
#         return inner
#     return inner1


# class TestClass(object):
#     def __init__(self, config, data):
#         self.config = config
#         self.data = data

#     def send(self):
#         return MagicMock(name='TestClass.send')


# class BaseTestCase(unittest.TestCase):
#     def setUp(self):
#         unittest.TestCase.setUp(self)
#         self.mock_requests = MagicMock(name='requests')
#         self.mock_celery = MagicMock(name='celery')
#         self.mock_datetime = MagicMock(name='datetime')
#         self.mock_tbconfig = MagicMock(name='tbconfig')
#         self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
#         self.mock_phim_backoffice.celery.app.task = fakeorator_arg
#         modules = {
#             'celery': self.mock_celery,
#             'phim_backoffice.celery': self.mock_phim_backoffice.celery,
#             'celery.utils': self.mock_celery.utils,
#             'celery.utils.log': self.mock_celery.utils.log,
#             'datetime': self.mock_datetime,
#             'tbconfig': self.mock_tbconfig,
#             'phim_backoffice.outer_notification.auditor': self.mock_phim_backoffice.auditor,
#             'phim_backoffice.outer_notification.models': self.mock_phim_backoffice.models
#             # 'phim_backoffice.outer_notification.nt_base': self.mock_phim_backoffice.nt_base
#         }
#         self.module_patcher = patch.dict('sys.modules', modules)
#         self.module_patcher.start()
#         from phim_backoffice.outer_notification.nt_base import Notifier
#         from phim_backoffice.outer_notification import case
#         self.module = case
#         self.module.Case.__bases__ = (Fake.imitate(Notifier),)

#     def tearDown(self):
#         unittest.TestCase.tearDown(self)
#         self.module_patcher.stop()


# class TaskTestCase(BaseTestCase):
#     def setUp(self):
#         BaseTestCase.setUp(self)
#         self.module.REQ_TIMEOUT = 10
#         self.module.COUNTDOWN = 10
#         self.module.MAX_RETRIES = 1

#     def test_case_base(self):
#         self.module.Case = MagicMock(name='Case')
#         self.module.create_case = MagicMock(name='create_case')
#         self.module.error_handler = MagicMock(name='error_handler')
#         self.module.status_update = MagicMock(name='status_update')
#         self.module.case_base('nt_type', 'config', 'data')
#         self.module.create_case.s.assert_called_once_with('config', 'data')
#         self.module.create_case.s.assert_called_once_with('config', 'data')
#         mock_set = self.module.create_case.s.return_value.set
#         mock_set.assert_called_once_with(
#             link_error=[self.module.error_handler.s.return_value])
#         self.module.status_update.s.assert_called_once_with(
#             'config', 'data', 'nt_type')
#         self.mock_celery.chain.assert_called_once_with(self.module.create_case.s.return_value.set.return_value,
#                                                        self.module.status_update.s.return_value)
#         self.mock_celery.chain.return_value.assert_called_once_with()

#     def test_create_case(self):
#         self.module.Case = MagicMock(name='Case')
#         mock_self = MagicMock(name='self')
#         resp = self.module.create_case(mock_self, 'config', 'data')
#         exp_res = self.module.Case.return_value.send.return_value
#         self.assertEqual(exp_res, resp)

#     def test_create_case_retry(self):
#         from requests.exceptions import ConnectTimeout
#         self.module.ConnectTimeout = ConnectTimeout
#         self.module.Case = MagicMock(name='Case')
#         exc = ConnectTimeout()
#         self.module.Case.side_effect = exc
#         mock_self = MagicMock(name='self')
#         self.module.create_case(mock_self, 'config', 'data')
#         mock_self.retry.assert_called_once_with(
#             countdown=10, exc=exc, max_retries=1)


# class CaseTestCase(BaseTestCase):
#     def setUp(self):
#         BaseTestCase.setUp(self)
#         data = {'siteid': 'siteid', 'hostname': 'hostname',
#                 'service': 'service', 'state': 'state'}
#         data['payload'] = {'output': 'output'}
#         data['hostaddress'] = 'hostaddress'
#         self.obj = self.module.Case(data)
#         self.obj.URL = 'URL'
#         self.obj.REQ_TIMEOUT = 10
#         self.obj.data = {'siteid': 'siteid', 'hostname': 'hostname',
#                          'hostaddress': 'hostaddress', 'service': 'service',
#                          'state': 'state', 'payload': {'output': 'output'}}
#         self.obj.config = {'autocase_priority': 'autocase_priority'}

#     def test_events_id(self):
#         self.assertEqual('siteid-hostname-service', self.obj.events_id)

#     def test_case_title(self):
#         self.assertEqual('hostname-service', self.obj.case_title)

#     def test_output(self):
#         self.assertEqual('output', self.obj.output)

#     def test_get_payload(self):
#         exp_result = {'createdby': 'IDM Portal', 'priority': 'autocase_priority',
#                       'siteid': 'siteid', 'hostname': 'hostname', 'service': 'service',
#                       'status': 'state', 'hostaddress': 'hostaddress',
#                       'events_id': 'siteid-hostname-service', 'case_title': 'hostname-service',
#                       'output': 'output'}
#         self.assertEqual(exp_result, self.obj.get_payload())

#     def test_format_response_when_response(self):
#         mock_response = MagicMock(name='resp')
#         mock_response.json.return_value = {'case_id': 123}
#         exp_resp = {'status': 0, 'case_id': 123,
#                     'submission_timestamp': self.mock_datetime.datetime.utcnow()
#                     }
#         self.assertEqual(exp_resp,
#                          self.obj.format_response(mock_response))

#     def test_format_response_empty_response(self):
#         exp_resp = {'status': 1, 'case_id': 'NA',
#                     'submission_timestamp': self.mock_datetime.datetime.utcnow()
#                     }
#         self.assertEqual(exp_resp,
#                          self.obj.format_response(None))

#     def test_format_response_case_id_unavailable(self):
#         mock_response = MagicMock(name='resp')
#         mock_response.json.return_value = {}
#         exp_resp = {'status': 1, 'case_id': 'NA',
#                     'submission_timestamp': self.mock_datetime.datetime.utcnow()
#                     }
#         self.assertEqual(exp_resp,
#                          self.obj.format_response(mock_response))
