import unittest
from mock import patch, MagicMock

# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator
 
class TankPerfdataPhim_backofficeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_taskbase = MagicMock(name='taskbase')
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_celery = MagicMock(name='celery') 
        self.mock_celery.app.task = fakeorator_arg
        self.mock_tb = MagicMock(name='tbconfig')
        modules = {
            'taskbase': self.mock_taskbase,
            'taskbase.elastictask': self.mock_taskbase.elastictask,
            'phim_backoffice.celery': self.mock_celery,
            'phimutils': self.mock_phimutils,
            'phimutils.namespace': self.mock_phimutils.namespace,
            'phimutils.timestamp': self.mock_phimutils.timestamp,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'tbconfig': self.mock_tb,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phim_backoffice import perfdata
        self.perfdata = perfdata

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_push2elasticsearch(self):
        self.perfdata.push2elasticsearch.es = MagicMock()
        self.perfdata.push2elasticsearch.es.index = MagicMock(return_value='val')
        self.perfdata.push2elasticsearch('data', index='index', doc_type='doc_type')
        self.perfdata.push2elasticsearch.es.index.assert_called_once_with(index='index', doc_type='doc_type', body='data')


    def test_push_metrics_data(self):
        self.perfdata.push_metrics_data.es = MagicMock()
        self.perfdata.push_metrics_data.es.index = MagicMock(return_value='val')
        self.perfdata.push_metrics_data('data', index='index', doc_type='doc_type')
        self.perfdata.push_metrics_data.es.index.assert_called_once_with(index='index', doc_type='doc_type', body='data')

    def test_bulk_elasticsearch_insert(self):
        self.perfdata.get_perfdata_records = MagicMock(return_value='val')
        self.perfdata.bulk_elasticsearch_insert.bulk = MagicMock(return_value = 'val')
        self.perfdata.bulk_elasticsearch_insert('siteid', 'data', index='index', doc_type='doc_type')
        self.assertEqual(self.perfdata.bulk_elasticsearch_insert.bulk.call_count, 1)


    def test_get_record_throw_exception(self):
        self.perfdata.cleanup_key = MagicMock(side_effect =KeyError)
        try:
            self.perfdata.get_record('siteid', 'timestamp', 'hostname', 'service', MagicMock())
        except:
            self.fail("KeyError not handled peroperly")

    
if __name__ == '__main__':
    unittest.main()