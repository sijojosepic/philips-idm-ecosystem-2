import unittest
from mock import patch, MagicMock, call

class TankBackoffice_enumsPhim_backofficeTestCase(unittest.TestCase):
    def test_defined_packages(self):
    	from phim_backoffice.backoffice_enums import PackageStatus
    	self.PackageStatus = PackageStatus
        
        self.assertEqual( self.PackageStatus.SUBMITTED, 'Submitted')
        self.assertEqual( self.PackageStatus.STARTED, 'Started')
        self.assertEqual( self.PackageStatus.INPROGRESS, 'In Progress')
        self.assertEqual( self.PackageStatus.FAILED, 'Failed')
        self.assertEqual( self.PackageStatus.AVAILABLE, 'Available')
        self.assertEqual( self.PackageStatus.NONE, 'None')

        
if __name__ == '__main__':
    unittest.main()