import unittest
from mock import MagicMock, patch, call, mock_open


class Fake(object):
    """Create Mock()ed methods that match another class's methods."""

    @classmethod
    def imitate(cls, *others):
        for other in others:
            for name in other.__dict__:
                try:
                    setattr(cls, name, MagicMock())
                except (TypeError, AttributeError):
                    pass
        return cls


class BillingTaskTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
        self.mock_celery_task = MagicMock(name='celery_task')
        modules = {
            'phim_backoffice.event': self.mock_phim_backoffice.event,
            'phim_backoffice.billing.event': self.mock_phim_backoffice.event_message,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.billing.task as task
        from celery import Task
        task.BillingTask.__bases__ = (Fake.imitate(Task),)
        self.task_obj = task.BillingTask()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    # def test_submit_event(self):
    #     self.task_obj.submit_event('data')
    #     self.mock_phim_backoffice.event.assert_called_once_with('data')

    def test_on_success(self):
        event_mock = MagicMock(name='event_mock')
        self.mock_phim_backoffice.event_message.event_message.return_value = 'event_message'
        self.task_obj.submit_event = event_mock
        self.task_obj.uploaded_mb = 34
        args = [{'siteid': 'S1'}]
        self.task_obj.on_success('retval', 'task_id', args, 'kwargs')
        event_mock.assert_called_once_with('event_message')
        payload = {
            'output': 'Upload to S3 Succeeded, size 34.0000MB', 'back_office': True}
        event_msg_call = call(
            'S1', notification_type='RECOVERY', payload=payload, state='OK')
        self.assertEqual(
            self.mock_phim_backoffice.event_message.event_message.call_args, event_msg_call)

    def test_on_failure(self):
        event_mock = MagicMock(name='event_mock')
        self.task_obj.submit_event = event_mock
        args = [{'siteid': 'S1'}]
        self.mock_phim_backoffice.event_message.event_message.return_value = 'event_message'
        self.task_obj.on_failure('exc', 'task_id', args, 'kwargs', 'einfo')
        event_mock.assert_called_once_with('event_message')
        payload = {'output': 'Unable to push data to S3, exc', 'back_office': True}
        event_msg_call = call('S1', notification_type='PROBLEM', payload=payload, state='CRITICAL')
        self.assertEqual(
            self.mock_phim_backoffice.event_message.event_message.call_args, event_msg_call)


if __name__ == '__main__':
    unittest.main()
