import unittest
from mock import MagicMock, patch, call


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class StateTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_pb = MagicMock(name='phim_backoffice')
        self.mock_pb.celery.app.task = fakeorator_arg
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'phim_backoffice.datastore': self.mock_pb.datastore,
            'phim_backoffice.event': self.mock_pb.event,
            'phim_backoffice.helpers': self.mock_pb.helpers,
            'phim_backoffice.celery': self.mock_pb.celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.site_configuration
        self.site_configuration = phim_backoffice.site_configuration
        self.site_config_data = {
            'siteid': 'ABC12',
            'payload': [
                {
                    'hostaddress' : '1.2.3.4',
                    'hostname': 'hostname1',
                    'module_type': 2,
                    'ram_size': 4096,
                    'cpu_cores': 2
                },
                {
                    'hostaddress': '1.2.3.5',
                    'hostname': 'hostname2',
                    'module_type': 4,
                    'ram_size': 4096,
                    'cpu_cores': 8
                }
            ]
        }

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_handle_config_event(self):
        self.site_configuration.handle_deviation = MagicMock(name='handle_deviation')
        self.site_configuration.get_host_data = MagicMock(name='get_host_data')
        self.site_configuration.get_identifier = MagicMock(name='get_identifier')
        self.site_configuration.get_identifier.side_effect = ['ABC12-hostname1', 'ABC12-hostname2']
        self.site_configuration.get_host_data.return_value = [
            {'_id': 'ABC12-hostname1', 'hostaddress': '1.2.3.4', 'hostname': 'hostname1', 'module_type': 2,
            'ram_size': 4096, 'cpu_cores': 2},
            {'_id': 'ABC12-hostname2', 'hostaddress': '1.2.3.5', 'hostname': 'hostname2', 'module_type': 4,
            'ram_size': 4096, 'cpu_cores': 8}]
        self.site_configuration.handle_config_event(self.site_config_data)
        self.assertEqual(
            self.site_configuration.upsert_one_by_id.mock_calls,
            [
            call('SITE_CONFIGURATION', 'ABC12-hostname1', {'_id': 'ABC12-hostname1', 'hostaddress': '1.2.3.4',
                'hostname': 'hostname1', 'module_type': 2, 'ram_size': 4096, 'cpu_cores': 2, 'siteid': 'ABC12'}),
            call('SITE_CONFIGURATION', 'ABC12-hostname2', {'_id': 'ABC12-hostname2', 'hostaddress': '1.2.3.5',
                'hostname': 'hostname2', 'module_type': 4, 'ram_size': 4096, 'cpu_cores': 8, 'siteid': 'ABC12'})
            ]
        )

    def test_purge_missing_configurations(self):
        self.site_configuration.find.return_value = [{'_id': 'ABC12-hostname1', 'hostaddress': '1.2.3.4',
                'hostname': 'hostname1', 'module_type': 2, 'ram_size': 4096, 'cpu_cores': 2, 'siteid': 'ABC12'},
                {'_id': 'ABC12-hostname4', 'hostaddress': '1.2.4.4', 'hostname': 'hostname4', 'module_type': 2,
                 'ram_size': 4096, 'cpu_cores': 2, 'siteid': 'ABC12'}]
        self.site_configuration.purge_missing_configurations('ABC12', ['hostname2', 'hostname3'])
        self.site_configuration.find.assert_called_once_with(
            'SITE_CONFIGURATION',
            {'siteid': 'ABC12', 'hostname': {'$nin': ['hostname2', 'hostname3']}}
        )
        self.site_configuration.delete_many.assert_called_once_with(
            'SITE_CONFIGURATION',
            {'siteid': 'ABC12', 'hostname': {'$nin': ['hostname2', 'hostname3']}}
        )

    def test_get_host_data(self):
        payload = self.site_config_data.get('payload')
        siteid = self.site_config_data.get('siteid')
        self.assertEqual(
            list(self.site_configuration.get_host_data(payload, siteid)),
            [
                {'hostaddress': '1.2.3.4', 'hostname': 'hostname1', 'module_type': 2, 'ram_size': 4096,'cpu_cores': 2},
                {'hostaddress': '1.2.3.5', 'hostname': 'hostname2', 'module_type': 4, 'ram_size': 4096, 'cpu_cores': 8}
            ]
        )

    def test_update_approved_deviation(self):
        self.site_configuration.get_validated_fields.return_value = {'siteid': 'ABC12', 'ids': ['ABC12-hostname1',
        'ABC12-hostname2']}
        self.site_configuration.handle_deviation.delay = MagicMock(return_value='true')
        self.site_configuration.handle_no_data = fakeorator_arg
        self.site_configuration.update_approved_deviation()
        self.site_configuration.handle_deviation.delay.assert_called_once_with(['ABC12-hostname1','ABC12-hostname2'],
                                                                               'ABC12')

    def test_get_identifier(self):
        self.assertEqual(self.site_configuration.get_identifier('ABC12', {'hostname':'hostname1'}), 'ABC12-hostname1')


    def test_handle_deviation_no_deviations(self):
        self.site_configuration.DASHBOARD = {'PROBLEMATIC_SITE_CONFIGURATION_KEY':
                                                 'Administrative__Philips__Site__Configuration__Status'}
        self.site_configuration.find_one.side_effect = [
            {'hostname': 'hostname1', 'ram_size': 4096, 'cpu_cores': 8, 'gold_copy': {'ram_size': 4096, 'cpu_cores': 8}},
            {'hostname': 'hostname2', 'ram_size': 4096, 'cpu_cores': 8, 'gold_copy': {'ram_size': 4096, 'cpu_cores': 8}}
        ]
        self.site_configuration.handle_deviation(['ABC12-hostname1','ABC12-hostname2'], 'ABC12')
        mock_deviations = {}
        self.assertEqual(
            self.site_configuration.send_event.mock_calls,
            [
                call(
                    siteid='ABC12',
                    hostname='localhost',
                    type='RECOVERY',
                    hostaddress='127.0.0.1',
                    service=self.site_configuration.DASHBOARD.get('PROBLEMATIC_SITE_CONFIGURATION_KEY'),
                    state='OK',
                    payload= {'output': 'OK - {}'}
                )
            ]
        )

    def test_handle_deviation_with_deviations(self):
        self.site_configuration.DASHBOARD = {'PROBLEMATIC_SITE_CONFIGURATION_KEY':
                                                 'Administrative__Philips__Site__Configuration__Status'}
        self.site_configuration.find_one.side_effect = [
            {'hostname': 'hostname1', 'ram_size': 4096, 'cpu_cores': 4, 'gold_copy': {'ram_size': 5192, 'cpu_cores': 8}},
            {'hostname': 'hostname2', 'ram_size': 4096, 'cpu_cores': 8, 'gold_copy': {'ram_size': 4096, 'cpu_cores': 4}}
        ]
        self.site_configuration.handle_deviation(['ABC12-hostname1','ABC12-hostname2'], 'ABC12')
        mock_deviations = {}
        self.assertEqual(
            self.site_configuration.send_event.mock_calls,
            [
                call(
                    siteid='ABC12',
                    hostname='localhost',
                    type='PROBLEM',
                    hostaddress='127.0.0.1',
                    service=self.site_configuration.DASHBOARD.get('PROBLEMATIC_SITE_CONFIGURATION_KEY'),
                    state='CRITICAL',
                    payload= {'output':  "CRITICAL - {'ABC12-hostname1': {'required_cpu_cores': 8, "
                                         "'required_ram_size': 5192, 'current_cpu_cores': 4, 'current_ram_size': 4096},"
                                         " 'ABC12-hostname2': {'required_cpu_cores': 4, 'current_cpu_cores': 8}}" }
                )
            ]
        )


if __name__ == '__main__':
    unittest.main()
