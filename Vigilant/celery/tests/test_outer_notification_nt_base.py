# import unittest
# from mock import MagicMock, patch


# def fakeorator_arg(*args, **kwargs):
#     def inner1(func):
#         def inner(*args, **kwargs):
#             return func(*args, **kwargs)
#         return inner
#     return inner1


# def fake_audit(func):
#     def inner(*args, **kwargs):
#         return func(*args, **kwargs)
#     return inner


# class BaseTestCase(unittest.TestCase):
#     def setUp(self):
#         unittest.TestCase.setUp(self)
#         self.mock_requests = MagicMock(name='requests')
#         self.mock_auditor = MagicMock(name='auditor')
#         self.mock_celery = MagicMock(name='celery')
#         self.mock_logger = MagicMock(name='logger')
#         self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
#         self.mock_phim_backoffice.celery.app.task = fakeorator_arg
#         self.mock_phim_backoffice.auditor.audit = fake_audit
#         modules = {
#             'requests': self.mock_requests,
#             'phim_backoffice.celery': self.mock_phim_backoffice.celery,
#             'phim_backoffice.outer_notification.auditor': self.mock_phim_backoffice.auditor,
#             'phim_backoffice.outer_notification.models': self.mock_phim_backoffice.models,
#             'phim_backoffice.outer_notification.kvstore': self.mock_phim_backoffice.kvstore
#         }
#         self.module_patcher = patch.dict('sys.modules', modules)
#         self.module_patcher.start()
#         from phim_backoffice.outer_notification import nt_base
#         self.module = nt_base

#     def tearDown(self):
#         unittest.TestCase.tearDown(self)
#         self.module_patcher.stop()


# class NotifierTestCase(BaseTestCase):
#     def setUp(self):
#         BaseTestCase.setUp(self)
#         self.obj = self.module.Notifier('config', 'data')
#         self.obj.URL = 'url'
#         self.obj.TYPE = 'T'
#         self.obj.REQ_TIMEOUT = 10

#     def test_get_payload(self):
#         self.assertRaises(NotImplementedError, self.obj.get_payload)

#     def test_format_response(self):
#         self.assertRaises(NotImplementedError,
#                           self.obj.format_response, 'resp')

#     def test_do_request(self):
#         self.assertEqual(self.mock_requests.post.return_value,
#                          self.obj.do_request(a='b'))

#     def test_send(self):
#         self.obj.get_payload = MagicMock(name='get_payload')
#         self.obj.do_request = MagicMock(name='do_request')
#         self.obj.format_response = MagicMock(name='format_response')
#         self.assertEqual(self.obj.format_response.return_value,
#                          self.obj.send())
#         self.obj.get_payload.assert_called_once_with()
#         exp_args = {'url': 'url', 'json': self.obj.get_payload.return_value,
#                     'verify': False, 'timeout': 10, 'nt_type': 'T'}
#         self.obj.do_request.assert_called_once_with(**exp_args)
#         self.obj.format_response.assert_called_once_with(
#             self.obj.do_request.return_value)


# class TaskTestCase(BaseTestCase):
#     def setUp(self):
#         BaseTestCase.setUp(self)

#     def test_status_update_default_model(self):
#         self.module.status_update('result', 'config', 'data', 'nt_type')
#         self.mock_phim_backoffice.models.Status.upsert.assert_called_once_with('result',
#                                                                                'config',
#                                                                                'data',
#                                                                                'nt_type')

#     def test_status_update_non_default_model(self):
#         mock_model = MagicMock(name='model')
#         self.module.status_update('result', 'config', 'data', 'nt_type', mock_model)
#         mock_model.upsert.assert_called_once_with('result', 'config', 'data', 'nt_type')

#     def test_error_handler_default_model(self):
#         mock_formatter = MagicMock(name='formatter')
#         mock_formatter.format_response.return_value = 'result'
#         self.module.error_handler('uuid', 'config', 'data', 'nt_type', mock_formatter)
#         self.mock_phim_backoffice.models.Status.upsert.assert_called_once_with('result',
#                                                                                'config',
#                                                                                'data',
#                                                                                'nt_type')
#         mock_formatter.format_response.assert_called_once_with(None)

#     def test_error_handler_non_default_model(self):
#         mock_formatter = MagicMock(name='formatter')
#         mock_formatter.format_response.return_value = 'result'
#         mock_model = MagicMock(name='model')
#         self.module.error_handler('uuid', 'config', 'data', 'nt_type', mock_formatter, mock_model)
#         mock_model.upsert.assert_called_once_with('result', 'config', 'data', 'nt_type')
#         mock_formatter.format_response.assert_called_once_with(None)