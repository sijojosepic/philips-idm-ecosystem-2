import unittest
from mock import patch, MagicMock

class TankCeleryPhim_backofficeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_celery = MagicMock(name='celery')
        modules = {
            'celery': self.mock_celery,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()
    
    def test_get_app(self):
    	from phim_backoffice import celery
        self.module = celery
        self.mock_celery.Celery = MagicMock(return_value = MagicMock())
        self.module.Celery.assert_called_once_with()
        self.module.app.config_from_object.assert_called_once_with('celeryconfig')

if __name__ == '__main__':
    unittest.main()