# import unittest
# from mock import MagicMock, patch, call


# class HandlerTestCase(unittest.TestCase):
#     def setUp(self):
#         unittest.TestCase.setUp(self)
#         self.mock_datetime = MagicMock(name='datetime')
#         self.mock_functools = MagicMock(name='functools')
#         self.mock_cached_property = MagicMock(name='cached_property',
#                                               cached_property=property)
#         self.mock_phim_backoffice = MagicMock(name='phim_backoffice')
#         self.mock_tbconfig = MagicMock(name='tbconfig')
#         modules = {
#             'cached_property': self.mock_cached_property,
#             'datetime': self.mock_datetime,
#             'tbconfig': self.mock_tbconfig,
#             'phim_backoffice.outer_notification.models': self.mock_phim_backoffice,
#             'phim_backoffice.outer_notification.case': self.mock_phim_backoffice.case,
#             'phim_backoffice.outer_notification.twilio': self.mock_phim_backoffice.twilio
#         }
#         self.module_patcher = patch.dict('sys.modules', modules)
#         self.module_patcher.start()
#         from phim_backoffice.outer_notification import handler
#         self.module = handler
#         # self.mock_notifier = MagicMock(name='notifier')
#         self.data = {'siteid': 'siteid',
#                      'service': 'service', 'hostname': 'hostname'}
#         self.obj = self.module.NotificationHandler(self.data)

#     def tearDown(self):
#         unittest.TestCase.tearDown(self)
#         self.module_patcher.stop()

#     def test_config_doc(self):
#         self.assertEquals(self.mock_phim_backoffice.Config.find_one.return_value,
#                           self.obj.config_doc)
#         self.mock_phim_backoffice.Config.find_one.assert_called_once_with(
#             **self.data)

#     def test_status_doc(self):
#         self.assertEquals(self.mock_phim_backoffice.Status.find_one.return_value,
#                           self.obj.status_doc)
#         self.mock_phim_backoffice.Status.find_one.assert_called_once_with(hostname='hostname',
#                                                                           service='service',
#                                                                           siteid='siteid')

#     def test_state_critical(self):
#         self.obj.data['state'] = 'CRITICAL'
#         self.assertEquals(True, self.obj.state_critical)

#     def test_state_ok(self):
#         self.obj.data['state'] = 'OK'
#         self.assertEquals(True, self.obj.state_ok)

#     def test_config_enabled(self):
#         self.obj.get_config_enabled = MagicMock(name='get_config_enabled')
#         self.assertEquals(self.obj.get_config_enabled.return_value, self.obj.config_enabled)

#     def test_status_enabled(self):
#         self.obj.get_status_enabled = MagicMock(name='get_status_enabled')
#         self.assertEquals(self.obj.get_status_enabled.return_value, self.obj.status_enabled)

#     def test_get_config_enabled(self):
#         mock_a = MagicMock(name='a')
#         mock_b = MagicMock(name='b')
#         self.obj.NT_TYPES = {'a': mock_a, 'b': mock_b}
#         self.mock_phim_backoffice.Config.find_one.return_value = {'a': 'Y'}
#         self.assertEquals(['a'], self.obj.get_config_enabled())

#     def test_get_status_enabled_when_no_status_doc(self):
#         self.obj.get_config_enabled = MagicMock(name='get_status_enabled')
#         r_val = ['a', 'b']
#         self.obj.get_config_enabled.return_value = r_val
#         self.mock_phim_backoffice.Status.find_one.return_value = None
#         self.assertEquals(r_val, self.obj.get_status_enabled())
#         self.assertNotEqual(id(r_val), id(self.obj.get_status_enabled()))

#     def test_get_status_enabled_when_status_doc(self):
#         self.obj.get_config_enabled = MagicMock(name='get_status_enabled')
#         r_val = ['a', 'b']
#         self.obj.get_config_enabled.return_value = r_val
#         self.mock_phim_backoffice.Status.find_one.return_value = {'a': True, 'b': False}
#         self.assertEquals(['b'], self.obj.get_status_enabled())

#     def test_submit_tasks(self):
#         self.obj.get_status_enabled = MagicMock(name='get_status_enabled')
#         self.obj.get_status_enabled.return_value = ['a', 'b']
#         mock_a = MagicMock(name='a')
#         mock_b = MagicMock(name='b')
#         self.obj.NT_TYPES = {'a': mock_a, 'b': mock_b}
#         self.obj.submit_tasks()
#         mock_a.delay.assert_called_once_with('a', self.obj.config_doc, self.obj.data)
#         mock_b.delay.assert_called_once_with('b', self.obj.config_doc, self.obj.data)

#     def test_run_state_critical(self):
#         self.obj.data['state'] = 'CRITICAL'
#         self.obj.submit_tasks = MagicMock(name='submit_tasks')
#         self.obj.run()
#         self.obj.submit_tasks.assert_called_once_with()

#     def test_run_state_ok(self):
#         self.mock_phim_backoffice.Config.find_one.return_value = True
#         self.obj.data['state'] = 'OK'
#         self.obj.submit_tasks = MagicMock(name='submit_tasks')
#         self.obj.run()
#         self.mock_phim_backoffice.Status.delete_one.assert_called_once_with(**self.obj.data)
#         self.assertEquals(0, self.obj.submit_tasks.call_count)


#     # def test_run_state_ok(self):
#     #     self.obj.data['state'] = 'OK'
#     #     self.obj.run()
#     #     self.mock_phim_backoffice.Status.delete_one({"siteid": self.data['siteid'],
#     #                                                  "hostname": self.data['hostname'],
#     #                                                  "service": self.data['service']})


# if __name__ == '__main__':
#     unittest.main()
