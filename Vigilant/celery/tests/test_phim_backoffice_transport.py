import unittest
from mock import patch, MagicMock

# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator
 
 
class TankTransportPhim_backofficeTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_phimutils = MagicMock(name='phimutils')
        self.mock_celery = MagicMock(name='celery') 
        self.mock_taskbase = MagicMock(name='taskbase')
        self.mock_celery.app.task = fakeorator_arg
        self.mock_tb = MagicMock(name='tbconfig')
        modules = {
            'phimutils': self.mock_phimutils,
            'phimutils.heartbeat': self.mock_phimutils.heartbeat,
            'phimutils.timestamp': self.mock_phimutils.timestamp,
            'phimutils.queueman': self.mock_phimutils.queueman,
            'taskbase': self.mock_taskbase,
            'taskbase.mapfiletask': self.mock_taskbase.mapfiletask,
            'phim_backoffice.celery': self.mock_celery,
            'celery': self.mock_celery,
            'celery.utils': self.mock_celery.utils,
            'celery.utils.log': self.mock_celery.utils.log,
            'tbconfig': self.mock_tb,
        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        from phim_backoffice import transport
        self.transport = transport

        unittest.TestCase.tearDown(self)
    def tearDown(self):
        self.module_patcher.stop()

    def test_hb_event_not_mapped(self):
        self.transport.hb_event.mapper = MagicMock()
        self.transport.hb_event.mapper.get = MagicMock(return_value= None)
        self.transport.hb_event(MagicMock())
        self.mock_phimutils.heartbeat.create_msg.assert_not_called()

    def test_hb_event_mapped_but_create_msg_not_called(self):
        self.transport.hb_event.mapper = MagicMock()
        obj = MagicMock()
        obj.get = MagicMock(return_value='NotNone')
        self.transport.hb_event.mapper.get = MagicMock(return_value= obj)
        data = MagicMock()
        data.type = 'val'
        self.transport.hb_event(data)
        self.mock_phimutils.heartbeat.create_msg.assert_not_called()

    def test_hb_event_mapped_and_create_msg_is_called(self):
        self.transport.hb_event.mapper = MagicMock()
        obj = MagicMock()
        obj.get = MagicMock(return_value=None)
        self.transport.hb_event.mapper.get = MagicMock(return_value= obj)
        self.transport.create_msg = MagicMock(return_value='val')
        self.transport.send_site_email_msg.delay = MagicMock(return_value='val')
        data = MagicMock()
        data.type = 'val'
        self.transport.hb_event(data)
        self.assertEqual(self.transport.create_msg.call_count, 1)
        
    def test_send_site_email_msg_no_address(self):
        self.transport.send_site_email_msg.mapper = MagicMock()
        self.transport.send_site_email_msg.mapper.get = MagicMock(return_value=None)
        self.transport.send_site_email_msg('siteid', 'message', 'subject')
        self.transport.send_message.assert_not_called()

    def test_send_site_email_msg_with_address(self):
        self.transport.send_site_email_msg.mapper = MagicMock()
        self.transport.send_site_email_msg.mapper.get = MagicMock(return_value='val')
        self.transport.send_message = MagicMock(return_value='val')
        self.transport.send_site_email_msg('siteid', 'message', 'subject')
        self.assertEqual(self.transport.send_message.call_count, 1)

    def test_queuemapper(self):
        self.transport.qm.get_exchange = MagicMock(return_value='val')
        self.transport.app.producer_or_acquire = MagicMock(return_value=MagicMock())
        self.transport.queuemapper(MagicMock())
        self.assertEqual( self.transport.qm.get_exchange.call_count, 1)

       

if __name__ == '__main__':
    unittest.main() 