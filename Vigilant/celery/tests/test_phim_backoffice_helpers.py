import unittest
from mock import patch,MagicMock


class HelpersTestCase(unittest.TestCase):
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.mock_tbconfig = MagicMock(name='tbconfig')
        self.mock_cryptography = MagicMock(name='cryptography')
        modules = {
            'tbconfig': self.mock_tbconfig,
            'cryptography': self.mock_cryptography,
            'cryptography.fernet': self.mock_cryptography.fernet,
            'cryptography.fernet.Fernet': self.mock_cryptography.fernet.Fernet,

        }
        self.module_patcher = patch.dict('sys.modules', modules)
        self.module_patcher.start()
        import phim_backoffice.helpers
        self.helpers = phim_backoffice.helpers
        self.fer = self.helpers.FernetCrypto()

    def tearDown(self):
        unittest.TestCase.tearDown(self)
        self.module_patcher.stop()

    def test_get_identifier(self):
        self.assertEqual(self.helpers.get_identifier('abc00', 'somehost01'), 'abc00-somehost01')

    def test_get_identified_host_dictionaries(self):
        facts = {
            'host1': {
                'data1': 'value1'
            },
            'host2': '',
            'host3': {
                'data1': 'value2'
            },
        }
        self.assertEqual(
            sorted(self.helpers.get_identified_host_dictionaries('xyz00', facts)),
            [
             ('xyz00-host1', {'siteid': 'xyz00', 'hostname': 'host1', 'data1': 'value1'}),
             ('xyz00-host3', {'siteid': 'xyz00', 'hostname': 'host3', 'data1': 'value2'})
            ]
        )

    @patch('__builtin__.__import__')
    def test_get_class(self, mock_patch):
        result = self.helpers.get_class('module1', 'attribute1')
        mock_patch.assert_called_once_with('module1', fromlist=['attribute1'])
        self.assertEqual(result, mock_patch.return_value.attribute1)

    def test_get_validated_fields(self):
        fields = frozenset(['name', 'rules', 'two'])
        self.assertEqual(self.helpers.get_validated_fields(fields, name='one', rules=None), {'name': 'one'})

    def test_get_validated_fields_unexpected_arg(self):
        self.assertRaises(TypeError, self.helpers.get_validated_fields, frozenset(['name']), siteid='XYZ00')

    def test_get_mongo_dns(self):
        str = ("protocol://{f1}:{f2}@uri?authSource=auth").format(f1=self.mock_cryptography.fernet.Fernet().decrypt(),f2=self.mock_cryptography.fernet.Fernet().decrypt())
        self.assertEqual(self.helpers.get_mongo_dns('uri','name','pass','protocol','auth'), str)

    def test_fernet(self):
        self.assertEqual(self.fer.fernet(), self.mock_cryptography.fernet.Fernet())

    def test_encrypt(self):
        self.assertEqual(self.fer.encrypt(MagicMock()),self.mock_cryptography.fernet.Fernet().encrypt())

    def test_decrypt(self):
        self.assertEqual(self.fer.decrypt(MagicMock()),self.mock_cryptography.fernet.Fernet().decrypt())


if __name__ == '__main__':
    unittest.main()
