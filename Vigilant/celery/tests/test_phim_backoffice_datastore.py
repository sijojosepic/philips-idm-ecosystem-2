import unittest
from mock import MagicMock, patch


# Parameterized decorator to mock celery.task
# It returns the function undecorated
def fakeorator_arg(*args, **kwargs):
    def fakeorator(func):
        return func
    return fakeorator


class FakeAutoReconnect(Exception):
    pass


class FakeRetry(BaseException):
    def __init__(self, exc):
        pass


class PBDatastoreTest(object):
    class TestCase(unittest.TestCase):
        def setUp(self):
            unittest.TestCase.setUp(self)
            self.mock_tbconfig = MagicMock(name='tbconfig')
            self.mock_taskbase = MagicMock(name='taskbase')
            self.mock_pb = MagicMock(name='phim_backoffice')
            self.mock_pb.celery.app.task = fakeorator_arg
            self.mock_pb.helpers.get_mongo_dns  = MagicMock(name='get_mongo_dns')
            self.mock_celery = MagicMock(name='celery')
            self.mock_datetime = MagicMock(name='datetime')
            modules = {
                'tbconfig': self.mock_tbconfig,
                'taskbase': self.mock_taskbase,
                'taskbase.mongotask': self.mock_taskbase.mongotask,
                'phim_backoffice.celery': self.mock_pb.celery,
                'celery': self.mock_celery,
                'celery.utils': self.mock_celery.utils,
                'celery.utils.log': self.mock_celery.utils.log,
                'celery.exceptions': self.mock_celery.exceptions,
                'phim_backoffice.helpers': self.mock_pb.helpers,
                'datetime': self.mock_datetime,
            }
            self.module_patcher = patch.dict('sys.modules', modules)
            self.module_patcher.start()
            import phim_backoffice.datastore
            self.datastore = phim_backoffice.datastore

        def tearDown(self):
            unittest.TestCase.tearDown(self)
            self.module_patcher.stop()


class DatastoreMongoDBTestCase(PBDatastoreTest.TestCase):
    def setUp(self):
        PBDatastoreTest.TestCase.setUp(self)
        self.mongodb_patcher = patch('phim_backoffice.datastore.mongodb')
        self.mock_mongodb = self.mongodb_patcher.start()

    def tearDown(self):
        PBDatastoreTest.TestCase.tearDown(self)
        self.mongodb_patcher.stop()

    @patch('phim_backoffice.datastore.COLLECTIONS', {'TEST': 'test'})
    def test_get_collection(self):
        result = self.datastore.get_collection('TEST')
        self.assertEqual(result, self.mock_mongodb.db.__getitem__('test'))

    def test_delete_one_by_id(self):
        self.datastore.delete_one_by_id('TEST', 'id01')
        self.mock_mongodb.delay.assert_called_once_with('TEST', 'delete_one', filter={'_id': 'id01'})

    def test_delete_many(self):
        self.datastore.delete_many('TEST', {'key1': 'val01'})
        self.mock_mongodb.delay.assert_called_once_with('TEST', 'delete_many', filter={'key1': 'val01'})

    def test_update_many(self):
        self.datastore.update_many('TEST', {'key1': 'val01'}, {'$set': {'key2': 'val2'}})
        self.mock_mongodb.delay.assert_called_once_with(
            'TEST', 'update_many', filter={'key1': 'val01'}, update={'$set': {'key2': 'val2'}}
        )

    def test_update_one_by_id_data(self):
        self.datastore.update_one_by_id('TEST', 'id01', {'x': 'y'})
        self.mock_mongodb.delay.assert_called_once_with(
            'TEST', 'update_one', filter={'_id': 'id01'}, update={'$set': {'x': 'y'}}, upsert=False, timestamp=False
        )

    def test_update_one_by_id_update(self):
        self.datastore.update_one_by_id('TEST', 'id01', update={'$doMagic': {'x': 'y'}})
        self.mock_mongodb.delay.assert_called_once_with(
            'TEST', 'update_one', filter={'_id': 'id01'}, update={'$doMagic': {'x': 'y'}}, upsert=False, timestamp=False
        )

    def test_upsert_one_by_id(self):
        self.datastore.update_one_by_id = MagicMock(name='update_one_by_id')
        self.datastore.upsert_one_by_id('TEST', 'id01', {'x': 'y'})
        self.datastore.update_one_by_id.assert_called_once_with(
            'TEST', 'id01', data={'x': 'y'}, update=None, upsert=True, timestamp=False
        )

    def test_upsert_one_by_id_with_timestamp(self):
        self.datastore.update_one_by_id = MagicMock(name='update_one_by_id')
        self.datastore.upsert_one_by_id('TEST', 'id01', {'x': 'y'}, timestamp=True)
        self.datastore.update_one_by_id.assert_called_once_with(
            'TEST', 'id01', data={'x': 'y'}, update=None, upsert=True, timestamp=True
        )

    def test_insert_one(self):
        self.datastore.insert_one('TEST', {'x': 'y'})
        self.mock_mongodb.delay.assert_called_once_with('TEST', 'insert_one', document={'x': 'y'}, timestamp=False)

    def test_insert_one_with_timestamp(self):
        self.datastore.insert_one('TEST', {'x': 'y'}, timestamp=True)
        self.mock_mongodb.delay.assert_called_once_with('TEST', 'insert_one', document={'x': 'y'}, timestamp=True)

    def test_update_one(self):
        self.datastore.update_one('TEST', {'key1': 'val01'}, {'key2': 'val2'})
        self.mock_mongodb.delay.assert_called_once_with(
            'TEST', 'update_one', filter={'key1': 'val01'}, update={'$set': {'key2': 'val2'}}
        )


class DatastoreCollectionOperationsTestCase(PBDatastoreTest.TestCase):
    def setUp(self):
        PBDatastoreTest.TestCase.setUp(self)
        self.collection_patcher = patch('phim_backoffice.datastore.get_collection')
        self.mock_collection = self.collection_patcher.start()

    def tearDown(self):
        PBDatastoreTest.TestCase.tearDown(self)
        self.collection_patcher.stop()

    def test_mongodb_execution(self):
        self.datastore.mongodb('some_self', 'TEST', 'find_one', filter='hey man nice shot', y='d')
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.find_one.assert_called_once_with(filter='hey man nice shot', y='d')

    def test_mongodb_execution_with_timestamp_insert_one(self):
        self.mock_datetime.datetime.utcnow.return_value = '2020-05-07T22:03:00.456000Z'
        self.datastore.mongodb('some_self', 'TEST', 'insert_one', timestamp=True,
                               document= {'siteid': 'ABC01', 'state': 'OK'})
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.insert_one.assert_called_once_with(document={'state': 'OK',
                                                'siteid': 'ABC01', 'creation_timestamp': '2020-05-07T22:03:00.456000Z'})

    def test_mongodb_execution_with_timestamp_update_one(self):
        self.mock_datetime.datetime.utcnow.return_value = '2020-05-07T22:03:00.456000Z'
        self.datastore.mongodb('some_self', 'TEST', 'update_one', timestamp=True,
                               update= {'$set': {'siteid': 'ABC01', 'state': 'OK'}})
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.update_one.assert_called_once_with(update= {'$set': {'siteid': 'ABC01',
                                                'state': 'OK', 'updation_timestamp': '2020-05-07T22:03:00.456000Z'}})

    def test_mongodb_exception(self):
        self.mock_collection.return_value.find_one.side_effect = FakeAutoReconnect
        some_self = MagicMock(name='some_self', retry=FakeRetry)
        some_self.errors.AutoReconnect = FakeAutoReconnect
        self.assertRaises(
            FakeRetry, self.datastore.mongodb, some_self, 'TEST', 'find_one', filter='hey man nice shot', y='d'
        )
        self.mock_collection.assert_called_once_with('TEST')

    def test_id_exists_true(self):
        self.mock_collection.return_value.count.return_value = 1
        self.assertTrue(self.datastore.id_exists('TEST', 'id01'))
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.count.assert_called_once_with({'_id': 'id01'}, limit=1)

    def test_id_exists_false(self):
        self.mock_collection.return_value.count.return_value = 0
        self.assertFalse(self.datastore.id_exists('TEST', 'id01'))
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.count.assert_called_once_with({'_id': 'id01'}, limit=1)

    def test_find_one(self):
        self.assertEqual(
            self.datastore.find_one('TEST', 'id01', {'value1': 1}),
            self.mock_collection.return_value.find_one.return_value
        )
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.find_one.assert_called_once_with('id01', projection={'value1': 1})

    def test_find(self):
        self.assertEqual(
            self.datastore.find('TEST', 'id01', {'value1': 1}),
            self.mock_collection.return_value.find.return_value
        )
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.find.assert_called_once_with('id01', projection={'value1': 1})

    def test_distinct(self):
        self.assertEqual(
            self.datastore.distinct('TEST', 'id01', {'value1': 1}),
            self.mock_collection.return_value.distinct.return_value
        )
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.distinct.assert_called_once_with('id01', query={'value1': 1})

    def test_aggregate(self):
        self.assertEqual(
            self.datastore.aggregate('TEST', [{'$match': {'_id': 'XYX00'}}]),
            self.mock_collection.return_value.aggregate.return_value
        )
        self.mock_collection.assert_called_once_with('TEST')
        self.mock_collection.return_value.aggregate.assert_called_once_with( [{'$match': {'_id': 'XYX00'}}])


if __name__ == '__main__':
    unittest.main()
