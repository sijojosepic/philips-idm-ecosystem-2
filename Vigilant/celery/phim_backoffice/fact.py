from __future__ import absolute_import
from itertools import ifilterfalse
from datetime import datetime
import copy
import re
from celery.utils.log import get_logger

from phim_backoffice.celery import app
from phim_backoffice.datastore import upsert_one_by_id, find, delete_many, find_one, aggregate, delete_one_by_id
from phim_backoffice.helpers import get_identified_host_dictionaries, get_identifier
from phim_backoffice.persist import poperator_gen, generate_from_facts
from phim_backoffice.state import purge_extra_hosts_state
from phim_backoffice.audit import del_endpoint_log
from tbconfig import POPERATOR_NAGIOS_DELAY_AFTER_COLLECTION


logger = get_logger(__name__)


@app.task(ignore_result=True)
def pcm_manifest_collect(manifest):
    poperator_gen.delay(siteid=manifest['siteid'], data_type='PCM_Manifest', data=manifest)

    site_data_items = {}
    if 'site_data' in manifest:
        site_data_items['site_data'] = manifest['site_data']
    if 'site_vault' in manifest:
        site_data_items['site_vault'] = manifest['site_vault']

    transformed_manifest = {}
    hosts = manifest.get('hosts', {})
    for host, host_data in hosts.iteritems():
        filtered_host_data = dict((k, v) for k, v in host_data.iteritems() if v)

        # place the site data under all hosts. A bit repetitive but allows subset queries to still return site info
        # data only comes from here so no chance of erroneous items.
        filtered_host_data.update(site_data_items)

        transformed_manifest[host] = {'PCM_Manifest': filtered_host_data}

    update_facts.delay(siteid=manifest['siteid'], facts=transformed_manifest)


def get_host_sets_update(endpoint, modules):
    sets = {}
    if endpoint:
        sets['endpoints'] = endpoint
    if modules:
        sets['modules'] = {'$each': modules}
    return {'$addToSet': sets} if sets else {}


# For generic discovery-track version via product discovery
def update_components_generic_discovery(host_facts):
    keys = ['product_name']
    valid = all(map(lambda x: host_facts.get(x, False) not in ['NA', 'null', False], keys))
    # valid = all(map(lambda x: host_facts.get(x, 'NA'), keys))
    if valid:
        host_facts['Components'] = host_facts.get('Components', [])
        if host_facts.get('ISP',{}).get('pcm_component'):
                host_facts['Components'].append(
                    {
                        'name': host_facts.get('ISP',{}).get('pcm_component', {}).get('name'),
                        'version': host_facts.get('ISP',{}).get('pcm_component', {}).get('version')
                    }
                )
        if host_facts.get('ISP',{}).get('module_type') == 'Hl7':
            host_facts['Components'].append(
                {
                    'name': 'Rhapsody',
                    'version': host_facts.get('rhapsody_components', {}).get('version','NA')
                }
        )
        else:           
            host_facts['Components'].append(
                {
                    'name': host_facts.get('product_name'),
                    'version': host_facts.get('product_version', 'NA')
                }
        )

    return host_facts


def filter_data(packages_list, deleted_data):
    package_data =[]
    data = []

    for package in packages_list:
        pcm = (package.get('name'), package.get('version'))
        if (pcm not in deleted_data) and (pcm not in data) and (package.get('status') == 'Available'):
            data.append(pcm)
            package_data.append(package)
    return package_data


def get_package_info(packages, identifier):
    result = find_one('FACTS', identifier)
    deleted_data = [(pcm.get('name'), pcm.get('version')) for pcm in packages if pcm.get('status') == 'Deleted']
    if result and result.get('PCM'):
        package_list = packages + result.get('PCM')
    else:
        package_list = packages
    return filter_data(package_list, deleted_data)


def delete_old_facts(endpoint, identifier):
    old_facts = find_one('FACTS', identifier)
    if old_facts:
        for key in old_facts:
            try:
                scanner = old_facts[key].get('endpoint').get('scanner')
                if scanner and scanner != endpoint.get('scanner'):
                    delete_one_by_id('FACTS', identifier)
            except (AttributeError, KeyError, TypeError):
                pass


def get_host_update(host_facts, endpoint, identifier=None):
    modules = host_facts.pop('modules', None)
    host_facts = update_components_generic_discovery(host_facts)
    facts_copy = copy.deepcopy(host_facts)
    delete_old_facts(endpoint, identifier)
    update = get_host_sets_update(endpoint, modules)
    if 'Components' in facts_copy and identifier:
        found_fact = find_one('FACTS', identifier) or {}
        found_fact_copy = copy.deepcopy(found_fact)
        # Ignore timestamp for comparision
        filter(lambda compo: compo.pop('timestamp', None), found_fact.get('Components', []))
        # Compare difference
        compo_diff_list = list(
            ifilterfalse(lambda x: x in found_fact.get('Components', []), facts_copy.get('Components', [])))
        compo_diff_list_copy = copy.deepcopy(compo_diff_list)
        for component in compo_diff_list_copy:
            component.setdefault('timestamp', datetime.utcnow())
        # Add new ones and update
        found_fact_copy.setdefault('Components', [])
        found_fact_copy.get('Components').extend(compo_diff_list_copy)
        host_facts['Components'] = found_fact_copy.get('Components', [])
        host_facts['Components'] = dict(((v['version'],v['name']) ,v) for v in host_facts.get('Components', [])).values()
        update_site_component_version(found_fact.get('siteid'),
                                      {'Components': host_facts['Components'],
                                       'pacs_version': guess_component_version(host_facts['siteid'], 'IntelliSpace PACS')})
        update['$set'] = host_facts
    else:
        for component in host_facts.get('Components', []):
            component.setdefault('timestamp', datetime.utcnow())
        pcm_packages = host_facts.get('PCM')
        if pcm_packages:
            host_facts.update({'PCM': get_package_info(pcm_packages, identifier)})
        update['$set'] = host_facts
    return update


def temp_host(ipaddr, hostname, host_data):
    if not ipaddr:
        ipaddr = hostname
    vmtype = 'Windows'
    if host_data.get('guest').get('guestFamily') == 'linuxGuest':
        vmtype = 'Linux'
    powerstate = host_data.get('powerState', 'poweredOff')
    template_dict={hostname: {"product_id": "NA",
                              "vmHost": {"endpoint": {"scanner": "vCenter",
                                                      "address": ipaddr
                                                     },
                                         "type": vmtype,
                                         "powerstate": powerstate,
                                        },
                              "product_version": "NA",
                              "modules": [vmtype],
                              "address": ipaddr,
                              "model": "NA",
                              "product_name": "NA",
                              "manufacturer": "NA"
                             }
                  }
    template_dict[hostname].update(host_data)
    return template_dict


def fact_gen(missed_host, vm_lst):
    fact = {}
    for host, ip in missed_host:
        host_data = [vm for vm in vm_lst if vm['guest'].get('hostName') and vm['guest']['hostName'] == host]
        if host_data:
            fact.update(temp_host(ip, host, host_data[0]))
    return fact


def vcenter_fact(siteid, facts):
    vm_lst = [facts.get(fact).get('vCenter').get('vms') for fact in facts \
              if facts.get(fact) and 'vCenter' in facts.get(fact) \
              and 'delta' in facts.get(fact).get('vCenter').get('tags', '')]
    if vm_lst:
        host_lst = [(vm['guest'].get('hostName'), vm['guest'].get('ipAddress')) for vm in vm_lst[0] if vm['guest'].get('hostName')]
        result = find('FACTS', {"siteid":siteid}, {'address':1, 'hostname':1})
        exist_hosts = [(doc.get('hostname'), doc.get('address')) for doc in result if doc.get('address')]
        if exist_hosts:
            facts.update(fact_gen(exist_hosts, vm_lst[0]))
        missed_host = [i for i in host_lst if i not in exist_hosts]
        if missed_host:
            facts.update(fact_gen(missed_host, vm_lst[0]))
    return facts


@app.task(ignore_result=True)
def update_facts(siteid, facts):
    facts = vcenter_fact(siteid, facts)
    for identifier, host_facts in get_identified_host_dictionaries(siteid, facts):
        logger.debug('Collecting facts for %s', identifier)
        #delete_one_by_id('FACTS', identifier)
        upsert_one_by_id('FACTS', identifier, update=(
            get_host_update(host_facts, None, identifier)))


@app.task(ignore_result=True)
def discovery_collect(siteid, facts, endpoint=None, credentials=None, exclude_modules=None):
    facts = vcenter_fact(siteid, facts)
    for identifier, host_facts in get_identified_host_dictionaries(siteid, facts):
        logger.debug('Collecting facts for %s', identifier)
        #delete_one_by_id('FACTS', identifier)
        upsert_one_by_id('FACTS', identifier, update=(
            get_host_update(host_facts, endpoint, identifier)))
    hostnames = facts.keys()
    logger.debug('Facts collected for hosts %s', hostnames)
    if endpoint:
        discovery_purge_missing.delay(siteid, hostnames, endpoint, exclude_modules)
        if endpoint.get('monitor', True):
            # Suppress sites .cfg file creation, when monitoring is
            # disabled
            generate_from_facts.apply_async(
                (siteid, hostnames, credentials), countdown=POPERATOR_NAGIOS_DELAY_AFTER_COLLECTION)
    else:
        generate_from_facts.apply_async(
            (siteid, hostnames, credentials), countdown=POPERATOR_NAGIOS_DELAY_AFTER_COLLECTION)


@app.task(ignore_result=True)
def discovery_purge_missing(siteid, hostnames, endpoint, exclude_modules):
    excedent_hosts_filter = {'siteid': siteid, 'hostname': {'$nin': hostnames}, 'endpoints': endpoint}
    if exclude_modules:
        excedent_hosts_filter['modules'] = {'$nin': exclude_modules}
    hosts = list(find('FACTS', excedent_hosts_filter))
    if hosts:
        logger.info('From siteid %s found hosts to purge %s', siteid, hosts)
        deleted_hosts = []
        for host in hosts:
            deleted_hosts.append(host['address'])
        excedent_dashboard_hosts_filter = {'siteid': siteid, 'hostaddress': {'$in': deleted_hosts}}
        poperator_gen.delay(siteid=siteid, data_type='NagiosNull', data=hosts)
        delete_many('FACTS', excedent_hosts_filter)
        #Once the hosts are deleted from facts collection.
        #Then the respective host's alerts also has to be cleared off from IDM dashboard. 
        delete_many('DASHBOARD', excedent_dashboard_hosts_filter)
        purge_extra_hosts_state.delay(siteid=siteid, hostnames=hostnames)
        return
    logger.debug('No hosts to purge for siteid %s endpoint %s', siteid, endpoint)


@app.task(ignore_result=True)
def cleanup_endpoint(siteid, endpoint, user_email=None):
    endpoint = [{'scanner': endpoint['scanner'], 'address': endpoint['address']}]
    facts_query = {'siteid': siteid, 'endpoints': endpoint}
    result = list(find('FACTS', facts_query, { '_id': 0, "hostname": 1}))
    hostnames = [x['hostname'] for x in result]
    # For collections site_configuration and maintenace_rules hostnames are having different case than from fact
    # Hence we are creating a query which performs a case insensitive search
    hostnames_re = [re.compile(x['hostname'], re.IGNORECASE) for x in result]
    dashboard_query = {'siteid': siteid, 'hostname': {'$in': hostnames}}
    site_config_query = {'siteid': siteid, 'hostname': {'$in': hostnames_re}}
    maintenance_query =  {'siteid': siteid, 'nodename': {'$in': hostnames_re}}
    poperator_gen.delay(siteid=siteid, data_type='NagiosNull', data=result)
    delete_many('FACTS', facts_query)
    delete_many('DASHBOARD', dashboard_query)
    purge_extra_hosts_state.delay(siteid=siteid, hostnames=hostnames)
    delete_many('SITE_CONFIGURATION', site_config_query)
    delete_many('SITE_MAINTENANCE_RULES', maintenance_query)
    del_endpoint_log(siteid=siteid, endpoint=endpoint, hostnames=hostnames, user=user_email)


def get_host_facts(siteid, hostname, facts=None):
    projection = dict(((fact, True) for fact in facts), _id=False) if facts else None
    return find_one('FACTS', get_identifier(siteid, hostname), projection) or {}


def update_site_component_version(siteid, components_versions):
    upsert_one_by_id('SITES', siteid, data=components_versions)


def guess_component_version(siteid, component):
    records = aggregate(
        'FACTS',
        [
            {'$match': {'Components.name': component, 'siteid': siteid}},
            {'$unwind': '$Components'},
            {'$match': {'Components.name': component}},
            {'$sort': {'Components.timestamp': -1 }},
            {'$limit': 1},
            {'$project': {'_id': 0, 'version': '$Components.version'}}
        ]
    )
    record = next(records, {})
    return record.get('version')
