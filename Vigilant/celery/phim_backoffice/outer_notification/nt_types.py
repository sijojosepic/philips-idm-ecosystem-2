
class WhatsApp(object):
    def __init__(self, config, data, contact):
        self.data = data
        self.config = config
        self.contact = contact

    def get_payload(self):
        return self.data

    def format_response(self, response):
        return response


class SMS(object):
    def __init__(self, config, data, contact):
        self.data = data
        self.config = config
        self.contact = contact

    def get_payload(self):
        return self.data

    def format_response(self, response):
        return response


class Email(object):
    def __init__(self, config, data, contact):
        self.data = data
        self.config = config
        self.contact = contact

    def get_payload(self):
        return self.data

    def format_response(self, response):
        return response


def get_type(_type):
    type_mapping = {'whatsapp': WhatsApp, 'sms': SMS, 'email': Email}
    try:
        return type_mapping[_type.lower()]
    except KeyError:
        pass
