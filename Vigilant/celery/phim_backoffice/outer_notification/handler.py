from cached_property import cached_property

from phim_backoffice.outer_notification.models import Config, Status
from phim_backoffice.outer_notification.case import case_base
from phim_backoffice.outer_notification.twilio import twilio_base
from phim_backoffice.outer_notification.nt_base import Cache


class NotificationHandler(object):
    NOTIFIED = 'notified'
    RESET = 'reset'
    STATE_CRITICAL = 'CRITICAL'
    STATE_OK = 'OK'
    NT_TYPES = {'autocase': case_base, 'notification': twilio_base}
    STATUS_ENABLED = 'Y'

    def __init__(self, data):
        self.data = data
        self.siteid = data['siteid']
        self.service = data['service']
        self.hostname = data['hostname']

    @cached_property
    def config_doc(self):
        return Config.find_one(**self.data)

    @cached_property
    def status_doc(self):
        return Status.find_one(**self.data)

    @property
    def state_critical(self):
        return self.data['state'].upper() == self.STATE_CRITICAL

    @property
    def state_ok(self):
        return self.data['state'].upper() == self.STATE_OK

    @cached_property
    def config_enabled(self):
        return self.get_config_enabled()

    @cached_property
    def status_enabled(self):
        return self.get_status_enabled()

    def get_status_enabled(self):
        items = []
        if not self.status_doc:
            # service is yet to be notified
            items = list(self.config_enabled)
        else:
            for item in self.config_enabled:
                if not self.status_doc.get(item):
                    items.append(item)
        return items

    def get_config_enabled(self):
        items = []
        for item in self.NT_TYPES:
            if self.config_doc.get(item) == self.STATUS_ENABLED:
                items.append(item)
        return items

    def submit_tasks(self):
        for item in self.status_enabled:
            if Cache.validate(nt_type=item, **self.data):
                self.NT_TYPES[item].delay(item, self.config_doc, self.data)

    def run(self):
        if self.config_doc:  # => service configured to be notified
            if self.state_critical:
                self.submit_tasks()
            elif self.state_ok:
                Status.delete_one(**self.data)
