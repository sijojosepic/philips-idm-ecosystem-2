from phim_backoffice import datastore as db


class Config(object):
    COLLECTION = 'NOTIFICATION_CONFIG'

    @staticmethod
    def find_one(**kwargs):
        _id = '{0}-{1}'.format(kwargs['siteid'], kwargs['service'])
        return db.find_one(Config.COLLECTION, _id)


class Status(object):
    COLLECTION = 'NOTIFICATION_STATUS'

    @staticmethod
    def query_filter(**fields):
        return {'siteid': fields['siteid'], 'hostname': fields['hostname'],
                'service': fields['service']}

    @staticmethod
    def find_one(**kwargs):
        return db.find_one(Status.COLLECTION, Status.query_filter(**kwargs))

    @staticmethod
    def delete_one(**kwargs):
        return db.delete_one(Status.COLLECTION,
                             query_filter=Status.query_filter(**kwargs))

    @staticmethod
    def upsert(status, config, data, nt_type):
        query = Status.query_filter(**data)
        update = {"$set": {nt_type: status}}
        return db.update_doc(Status.COLLECTION, query, update, upsert=True)


class Audit(object):
    COLLECTION = 'NOTIFICATION_AUDIT'

    @staticmethod
    def insert_one(data):
        db.insert_one(Audit.COLLECTION, data)
