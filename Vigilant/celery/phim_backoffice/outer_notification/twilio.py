from requests.exceptions import ConnectTimeout
from phim_backoffice.celery import app

from phim_backoffice.outer_notification.nt_base import (Notifier,
                                                        status_update,
                                                        error_handler
                                                        )
from celery.utils.log import get_logger
from tbconfig import OUTER_NOTIFICATION

logger = get_logger(__name__)

COUNTDOWN = OUTER_NOTIFICATION['COUNTDOWN']
MAX_RETRIES = OUTER_NOTIFICATION['MAX_RETRIES']


@app.task(bind=True, acks_late=True, ignore_result=True)
def twilio(self, config, data, type_obj):
    try:
        return Twilio(config, data).send()
    except ConnectTimeout as exc:
        # retry on ConnectTimeout, other Exceptions won't be resolved
        # with retry's
        logger.exception('Case creation Error retrying...')
        self.retry(countdown=COUNTDOWN, exc=exc, max_retries=MAX_RETRIES)


@app.task(acks_late=True, ignore_result=True)
def twilio_base(nt_type, config, data):
    header = []
    for contacts in config['notification_contacts']:
        for k in contacts:
            item_type = get_type(k)
            if item_type:
                _task = twilio.s(config, data, item_type(
                    config, data, contacts[k]))
                header.append(_task)
    # callback = status_update.subtask(kwargs={'config': config,
    #                                          'data': data,
    #                                          'nt_type': nt_type})
    # chord(header)(callback)


class Twilio(Notifier):
    URL = OUTER_NOTIFICATION['NOTIFICATION_URL']
    TYPE = 'twilio'

    def __init__(self, config, data, item_obj):
        super(Twilio, self).__init__(self, config, data)
        self.item_obj = item_obj

    def get_payload(self):
        return self.item_obj.get_payload()

    def format_response(self, response):
        return self.item_obj.format_response(response)