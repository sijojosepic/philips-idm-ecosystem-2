from phim_backoffice.celery import app
from phim_backoffice.maintenance import is_site_under_maintanance
from phim_backoffice.outer_notification.handler import NotificationHandler


@app.task(ignore_result=True)
def notify(data):
    event_info = data.to_dict()
    event_info['timestamp'] = data.datetime
    if not any(is_site_under_maintanance(event_info)):
        NotificationHandler(event_info).run()
