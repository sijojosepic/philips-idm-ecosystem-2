from datetime import datetime
from requests.exceptions import ConnectTimeout
from celery import chain
from phim_backoffice.celery import app
from celery.utils.log import get_logger

from phim_backoffice.outer_notification.nt_base import (Notifier,
                                                        status_update,
                                                        error_handler
                                                        )
from tbconfig import CASE

logger = get_logger(__name__)

COUNTDOWN = CASE['COUNTDOWN']
MAX_RETRIES = CASE['MAX_RETRIES']


@app.task(acks_late=True, ignore_result=True)
def case_base(nt_type, config, data):
    item = create_case.s(config, data).set(link_error=[error_handler.s(
        config, data, nt_type, Case)])
    st = status_update.s(config, data, nt_type)
    chain(item, st)()


@app.task(bind=True, acks_late=True)
def create_case(self, config, data):
    try:
        return Case(config, data).send()
    except ConnectTimeout as exc:
        logger.exception('Case creation Error retrying...for data - %s', data)
        self.retry(countdown=COUNTDOWN, exc=exc, max_retries=MAX_RETRIES)


class Case(Notifier):
    URL = CASE['URL']
    REQ_TIMEOUT = CASE['REQ_TIMEOUT']
    TYPE = 'autocase'
    FIELDS = ('events_id', 'case_title', 'output')

    @property
    def events_id(self):
        return '{siteid}-{hostname}-{service}'.format(**self.data)

    @property
    def case_title(self):
        return '{hostname}-{service}'.format(**self.data)

    @property
    def output(self):
        return self.data['payload']['output']

    def get_payload(self):
        payload = {'createdby': 'IDM Portal',
                   'priority': self.config['autocase_priority']}
        payload['status'] = self.data['state']
        data_fields = ('siteid', 'hostaddress', 'hostname', 'service')
        for item in data_fields:
            payload[item] = self.data[item]
        for item in self.FIELDS:
            payload[item] = getattr(self, item)
        return payload

    @staticmethod
    def format_response(response):
        status = 1
        case_id = 'NA'
        if response:
            if response.json().get('case_id'):
                status = 0
                case_id = response.json().get('case_id', 'NA')
        return {'status': status, 'case_id': case_id,
                'submission_timestamp': datetime.utcnow()}
