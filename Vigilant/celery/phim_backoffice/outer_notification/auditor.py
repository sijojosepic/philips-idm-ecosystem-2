from datetime import datetime
from functools import wraps
from requests.exceptions import RequestException

from phim_backoffice.outer_notification.models import Audit


class AutoCaseAuditor(object):

    def __init__(self, nt_type, data, response):
        self.data = data
        self.response = response
        self._info = {}
        self.nt_type = nt_type

    @property
    def status(self):
        _status = 1
        if self.response and self.response.json().get('case_id'):
            _status = 0
        return _status

    def details(self):
        if self.response:
            return [{'case_id': self.response.json().get('case_id', 'NA')}]

    def audit_info(self):
        info = {'status': self.status, 'siteid': self.data['siteid'],
                'hostname': self.data['hostname'], 'notification_type': self.nt_type,
                'service': self.data['service'], 'creation_time': datetime.utcnow()
                }
        details = self.details()
        if details:
            info['details'] = details
        return info


AUDIT_MAPPING = {'autocase': AutoCaseAuditor}


def audit_info(nt_type, data, response):
    return AUDIT_MAPPING[nt_type](nt_type, data, response).audit_info()


def audit(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            nt_type, response = kwargs.pop('nt_type'), None
            response = func(*args, **kwargs)
        except RequestException as e:
            raise e
        finally:
            Audit.insert_one(audit_info(nt_type, kwargs['json'], response))
        return response
    return wrapper
