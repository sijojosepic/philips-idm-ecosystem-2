import requests

from phim_backoffice.celery import app
from celery.utils.log import get_logger
from phim_backoffice.outer_notification.auditor import audit
from phim_backoffice.outer_notification.models import Status
from phim_backoffice.kvstore import setex, get, expire


logger = get_logger(__name__)

# Cache layer to avoid duplicate notification
# that is once notification begins for a  CRITICAL event,
# before it update the status collection as notified,
# another CRITICAL event of same type comes, that also will
# be attempted, with cache protection layer thats avoided


class Cache(object):
    ID = '{nt_type}{siteid}{hostname}{service}'

    @staticmethod
    def validate(**kwargs):
        _id = Cache.ID.format(**kwargs)
        if get(_id):
            return False
        setex(_id, 1, 10 * 60)  # 10 mns
        return True

    @staticmethod
    def expire(**kwargs):
        _id = Cache.ID.format(**kwargs)
        expire(_id, 2)


class Notifier(object):
    def __init__(self, config, data):
        self.config = config
        self.data = data

    @audit
    def do_request(self, **kwargs):
        return requests.post(**kwargs)

    def get_payload(self):
        raise NotImplementedError

    def format_response(self, response):
        raise NotImplementedError

    def send(self):
        params = dict(url=self.URL,
                      json=self.get_payload(),
                      verify=False,
                      timeout=self.REQ_TIMEOUT,
                      nt_type=self.TYPE)
        response = self.do_request(**params)
        return self.format_response(response)


@app.task(acks_late=True, ignore_result=True)
def status_update(result, config, data, nt_type, model=Status):
    model.upsert(result, config, data, nt_type)
    Cache.expire(nt_type=nt_type, **data)


@app.task(acks_late=True, ignore_result=True)
def error_handler(uuid, config, data, nt_type, formatter, model=Status):
    result = getattr(formatter, 'format_response')(None)
    model.upsert(result, config, data, nt_type)
    logger.error('%s Failed %s', nt_type.title(), data)
    Cache.expire(nt_type=nt_type, **data)
