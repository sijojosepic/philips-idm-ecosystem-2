from __future__ import absolute_import
from datetime import datetime
import json

from phim_backoffice.kvstore import (
    get_hashset_val,
    set_hashset_val
)

from phim_backoffice.datastore import find_one
from celery.utils.log import get_logger
from tbconfig import SITE_MAINTENANCE_RULES


logger = get_logger(__name__)


def is_site_under_maintanance(data):
    host_key = '{siteid}_{hostname}'.format(
        siteid=data['siteid'], hostname=data['hostname'])
    states = [is_siteid_under_maintenance(data['siteid'], data['timestamp']),
              is_node_under_maintenance(host_key, data['timestamp'])]
    return states


def is_siteid_under_maintenance(siteid, timestamp):
    return check_maintenance(siteid, timestamp)


def is_node_under_maintenance(hostname, timestamp):
    return check_maintenance(hostname, timestamp)


def check_maintenance(field, timestamp):
    time_slots = redis_check_and_update(field)
    state = False
    if time_slots:
        time_slots = json.loads(time_slots)
        time_slots['start_time'] = datetime.strptime(
            time_slots['start_time'], '%Y-%m-%d %H:%M:%S.%f')
        time_slots['end_time'] = datetime.strptime(
            time_slots['end_time'], '%Y-%m-%d %H:%M:%S.%f')
        state = (timestamp > time_slots['start_time'] and
                 timestamp < time_slots['end_time']
                 )
        logger.info('Checking if site -- %s -- is under maintenance, maintenance state: %s', field,
                    str(state))
    return state


def get_hash_val(data):
    return json.dumps({'start_time': datetime.strftime(data.get('start_time'), '%Y-%m-%d %H:%M:%S.%f'),
                       'end_time': datetime.strftime(data.get('end_time'), '%Y-%m-%d %H:%M:%S.%f')})


def redis_check_and_update(field):
    value = get_hashset_val(SITE_MAINTENANCE_RULES, field)
    if not value:
        data = find_one('SITE_MAINTENANCE_RULES', field)
        if data:
            hash_val = get_hash_val(data)
            set_hashset_val(SITE_MAINTENANCE_RULES, field, hash_val)
            value = hash_val
    return value
