from __future__ import absolute_import


from phim_backoffice.datastore import insert_one, find_one, update_one_by_id, update_one
from phim_backoffice.helpers import get_validated_fields
from celery.utils.log import get_logger
from phim_backoffice.celery import app
from phim_backoffice.persist import poperator_del, subscription_del
from datetime import datetime
from phim_backoffice.backoffice_enums import PackageStatus
from phimutils.decorators import log_execution_time_with_args


logger = get_logger(__name__)

DISTRIBUTION_FIELDS = frozenset(['subscription', 'siteid', 'status', 'user', 'version', 'size', 'subscription_id'])
DEPLOYMENT_FIELDS = frozenset(['package', 'siteid', 'hosts', 'status', 'user', 'version', 'deployment_id', 'output'])
DEPLOYMENT_UPDATE_FIELDS = frozenset(['status', 'deployment_id', 'output'])
USER_FIELDS = frozenset(['email', 'headers'])
CASE_FIELDS = frozenset(['case_number','hostname','service', 'siteid', 'uname'])
ACK_FIELDS = frozenset(['hostname','service',  'siteid', 'tag','uname'])
MAINTENANCE_RULES_FIELDS = frozenset(['siteid', 'start_time', 'end_time', 'nodes', 'services', 'user', 'maintenance_remark', 'type'])
ASSIGNMENT_FIELDS = frozenset(['user', 'type', 'siteid'])
LHOST_FIELDS = frozenset(['siteid', 'user', 'initial_data', 'updated_data'])
DELETE_ENDPOINT_FIELDS = frozenset(['siteid', 'hostnames', 'endpoint', 'user'])
GEO_RULES_FIELDS = frozenset(['payload', 'uname'])
SITE_CONFIGURATION_FIELDS = frozenset(['site', 'version', 'user', 'nodes'])

def insert_dated_audit(type, data):
    data['type'] = type
    data['timestamp'] = datetime.utcnow()
    insert_one('AUDIT', data)

def update_dated_audit(filter, data):
    data['timestamp'] = datetime.utcnow()
    update_one('AUDIT', filter, data)

@app.task(ignore_result=True)
def insert_distribution(**kwargs):
    data = get_validated_fields(DISTRIBUTION_FIELDS, **kwargs)
    logger.debug('Inserting distribution audit log data: %s', data)
    insert_dated_audit('distribution', data)

@app.task(ignore_result=True)
def update_distribution(filter, **kwargs):
    data = get_validated_fields(DISTRIBUTION_FIELDS, **kwargs)
    logger.debug('Updating distribution audit log data: %s', data)
    update_dated_audit(filter, data)

@app.task(ignore_result=True)
def insert_deployment(**kwargs):
    data = get_validated_fields(DEPLOYMENT_FIELDS, **kwargs)
    logger.debug('Inserting deployment audit log data: %s', data)
    insert_dated_audit('deployment', data)


@app.task(ignore_result=True)
@log_execution_time_with_args
def update_package_status(siteid, facts, endpoint=None, **kwargs):
    host_components = facts.get('localhost') if facts.get('localhost') else {}
    pcm_components = host_components.get('PCM', [])
    for component in pcm_components:
        results = find_one('AUDIT', {'type': 'distribution',
                                     'subscription': component.get('name'),
                                     'version': component.get('version'),
                                     'siteid': siteid,
                                     'status': component.get('status')
                                     })

        if not results:
            data = {
                'subscription': component.get('name'),
                'siteid': siteid,
                'status': component.get('status'),
                'user': 'IDM Portal',
                'version': component.get('version'),
                'size': component.get('fsize'),
                'subscription_id': component.get('subscription_id')
            }
            insert_distribution(**data)

        if results and component.get('status') == PackageStatus.INPROGRESS:
            filter = {
                'type': 'distribution',
                'subscription': component.get('name'),
                'version': component.get('version'),
                'siteid': siteid,
                'status': component.get('status')
            }
            data = {
                'size': component.get('fsize')
            }
            update_distribution(filter, **data)

        if component.get('status') in [PackageStatus.AVAILABLE, PackageStatus.DELETED]:
            # Delete the subscription from svn if package is available.
            subscription_del(siteid, component.get('subscription_id'))


@app.task(ignore_result=True)
def deployment_status(**kwargs):
    data = get_validated_fields(DEPLOYMENT_UPDATE_FIELDS, **kwargs)
    logger.debug('Updating deployment status in audit log data: %s', data)
    results = find_one('AUDIT', {'type': 'deployment', 'deployment_id': data.get('deployment_id')})
    if results:
        for field in ['timestamp', 'type', '_id']:
            results.pop(field)
        status_result = find_one('AUDIT', {'type': 'deployment', 'status': data['status'],
                                             'deployment_id': data.get('deployment_id')})
        results.update(data)
        if not status_result:
            insert_deployment(**results)
        if results.get('status') in ['Success', 'Deleted']:
            poperator_del(results.get('siteid'), 'PCM_Manifest', data)

@app.task(ignore_result=True)
def login(**kwargs):
    data = get_validated_fields(USER_FIELDS, **kwargs)
    logger.debug('Inserting login status to user audit : %s', data)
    insert_dated_audit('login', data)

@app.task(ignore_result=True)
def case_log(**kwargs):
    data = get_validated_fields(CASE_FIELDS, **kwargs)
    logger.debug('Inserting case log info to audit : %s', data)
    insert_dated_audit('case', data)

@app.task(ignore_result=True)
def ack_log(**kwargs):
    data = get_validated_fields(ACK_FIELDS, **kwargs)
    logger.debug('Inserting ack log info to audit : %s', data)
    insert_dated_audit('ack', data)

@app.task(ignore_result=True)
def site_maintenance_log(**kwargs):
    data = get_validated_fields(MAINTENANCE_RULES_FIELDS, **kwargs)
    logger.debug('Inserting %s rules log info to audit : %s', data['type'], data)
    insert_dated_audit(data['type'], data)

@app.task(ignore_result=True)
def assignment_log(**kwargs):
    data = get_validated_fields(ASSIGNMENT_FIELDS, **kwargs)
    logger.debug('Inserting %s log info to audit : %s', data['type'], data)
    insert_dated_audit(data['type'], data)

@app.task(ignore_result=True)
def lhost_log(**kwargs):
    data = get_validated_fields(LHOST_FIELDS, **kwargs)
    logger.debug('Inserting lhost edit log info to audit : %s', data)
    insert_dated_audit('lhost', data)

@app.task(ignore_result=True)
def del_endpoint_log(**kwargs):
    data = get_validated_fields(DELETE_ENDPOINT_FIELDS, **kwargs)
    logger.debug('Inserting Endpoint deletion log info to audit : %s', data)
    insert_dated_audit('del_endpoint', data)

@app.task(ignore_result=True)
def geo_rules(**kwargs):
    data = get_validated_fields(GEO_RULES_FIELDS, **kwargs)
    logger.debug('Inserting geo rules info to audit : %s', data)
    insert_dated_audit('geo_rules', data)

@app.task(ignore_result=True)
def site_configuration(**kwargs):
    data = get_validated_fields(SITE_CONFIGURATION_FIELDS, **kwargs)
    logger.debug('Inserting approved site configuration info to audit: %s', data)
    insert_dated_audit('site_configuration', data)
