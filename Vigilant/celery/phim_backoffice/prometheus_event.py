import linecache
from phim_backoffice.event import handle_event
from phim_backoffice.celery import app
from phimutils.message import Message

from tbconfig import DASHBOARD, SHINKEN_STATES
UNREACHABLE_HOST = DASHBOARD['UNREACHABLE_HOST_KEY']
OK = SHINKEN_STATES['OK']
WARNING = SHINKEN_STATES['WARNING']
CRITICAL = SHINKEN_STATES['CRITICAL']


class ParseData(object):
	def __init__(self, data):
		self._data = data
    #forms a json response to post to the back office
	def get_message_data(self):
		message_data = {}
		message_data['hostname'] = self.hostname
		message_data['hostaddress'] = self.hostname
		message_data['service'] =  self.service
		message_data['state'] = self.state
		message_data['type'] = self.notification_type
		message_data['payload'] = self.payload
		return message_data

    #getting hostname from alerts
	@property
	def hostname(self):
		return self._data['labels']['instance'].split(':')[0]

    #getting service namespace from alerts
	@property
	def service(self):
		productid = self._data['labels']['productid']
		exporter = self._data['labels']['job']
		alert = self._data['labels']['alertname']
		if self._data['labels'].get('event') == 'host':
			return UNREACHABLE_HOST
		else:
			return 'Product__{productid}__{exporter}__{alert}__Status'.format(productid=productid.upper(),
		 exporter=exporter, alert=alert)

    #defining the state for critical and warning
	@property
	def state(self):
		if self._data['status'] == 'firing':
			if (self._data['labels']['severity'] == 'critical'):
				state = CRITICAL
			elif(self._data['labels']['severity'] == 'warning'):
				state = WARNING
		elif(self._data['status'] == 'resolved'):
			state = OK
		return state

    #defining payload as blank if state is ok else the summary of alert
	@property
	def payload(self):
		output = 'OK'
		if self.state in [CRITICAL,WARNING]:
			output = self._data['annotations']['summary']
		return {'output': output, 'back_office': True}

    #getting notification type from alerts
	@property
	def notification_type(self):
		if self.state in [CRITICAL,WARNING]:
			return 'Problem'
		elif self.state == OK:
			return 'Recovery'

@app.task(ignore_result=True)
def prometheus_alerts(data): 
    try:
        data = data.to_dict()
        siteid = data['siteid']
        for alerts in data['payload']['alerts']:
            message_data = ParseData(alerts)
            message = message_data.get_message_data()
            msg = Message(siteid=siteid, **message)
            handle_event.delay(msg)
    except Exception as e:
        return 'Validate the json format'

