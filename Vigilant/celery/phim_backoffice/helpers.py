from cryptography.fernet import Fernet
from tbconfig import FERNET_SECRET

def get_class(module, name):
    mod = __import__(module, fromlist=[name])
    return getattr(mod, name)


def get_identified_host_dictionaries(siteid, host_dictionaries):
    for host, host_dictionary in host_dictionaries.iteritems():
        try:
            host_dictionary['hostname'] = host
            host_dictionary['siteid'] = siteid
        except TypeError:
            # host_dictionary is not a dictionary
            continue
        yield get_identifier(siteid, host), host_dictionary


def get_identifier(siteid, host):
    return '{siteid}-{host}'.format(siteid=siteid, host=host)


def get_validated_fields(fields, **kwargs):
    if not fields.issuperset(kwargs.keys()):
        unexpected_args = list(set(kwargs.keys()).difference(fields))
        raise TypeError('got unexpected arguments {0}'.format(unexpected_args))
    return dict((k, v) for k, v in kwargs.iteritems() if v)


def get_mongo_dns(uri, username, password, protocol, auth_source):
    return "{protocol}://{username}:{password}@{uri}?authSource={auth_source}".format(
        protocol=protocol,
        username=FernetCrypto.decrypt(username),
        password=FernetCrypto.decrypt(password),
        uri=uri,
        auth_source=auth_source
    )


class FernetCrypto(object):

    @staticmethod
    def fernet():
        secret = bytes(FERNET_SECRET)
        return Fernet(secret)

    @classmethod
    def encrypt(cls, value):
        cipher_suite = cls.fernet()
        return cipher_suite.encrypt(value)

    @classmethod
    def decrypt(cls, value):
        cipher_suite = cls.fernet()
        return cipher_suite.decrypt(value)


