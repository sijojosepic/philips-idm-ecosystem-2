from __future__ import absolute_import

from celery.utils.log import get_logger
from temporary import temp_dir

from phim_backoffice.celery import app
from phim_backoffice.datastore import find, find_one
from phim_backoffice.helpers import get_class
from poperator.configwrite import SVNConfigWriter
from tbconfig import POPERATOR_TYPE_CLASS_MAP, IBC


logger = get_logger(__name__)


@app.task(ignore_result=True)
def poperator_gen(siteid, data_type, data, credentials=None):
    ''' This method will be used to write to SVN'''
    class_map = POPERATOR_TYPE_CLASS_MAP.get(data_type)
    if not class_map:
        logger.error('Persist - No classmap for data type %s', data_type)
        return

    config_generator = get_class(class_map['module'], class_map['class'])
    with temp_dir(prefix='configgen_', parent_dir=IBC['WORKING_DIRECTORY']) as tmp_work_dir:
        logger.debug(
            'Persist Working on data from siteid %s using %s %s at %s',
            siteid,
            class_map['module'],
            class_map['class'],
            tmp_work_dir
        )
        cw = SVNConfigWriter(tmp_work_dir, IBC['SVN_URL'], IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], siteid)
        gen = config_generator(data, credentials=credentials)
        config = gen.generate()
        cw.write(config)
        #config_services = gen.generate_service()
        #cw.write(config_services)


@app.task(ignore_result=True)
def generate_from_facts(siteid, hostnames, credentials=None):
    hosts = list(find('FACTS', {'siteid': siteid, 'hostname': {'$in': hostnames}},{'Components.timestamp':0}))
    poperator_gen.delay(siteid=siteid, data_type='Nagios', data=hosts, credentials=credentials)


@app.task(ignore_result=True)
def swd_subscribe(sites, subscriptions):
    for siteid in sites:
        subscriptions_json = find_one('SUBSCRIPTIONS', subscriptions[0])
        pkg_id = subscriptions_json.get('_id')
        dependencies = subscriptions_json.get('dependencies', {})
        if dependencies:
            logger.info("Adding dependent subscriptions %s for siteid %s for main package %s", dependencies.keys(),
                        siteid, pkg_id)
            subscriptions.extend(dependencies.keys())
        logger.info('Adding subscriptions %s to siteid %s', subscriptions, siteid)
        poperator_gen.delay(siteid=siteid, data_type='SWD', data=subscriptions)


@app.task(ignore_result=True)
def poperator_del(siteid, data_type, data):
    class_map = POPERATOR_TYPE_CLASS_MAP.get(data_type)
    if not class_map:
        logger.error('Persist - No classmap for data type %s', data_type)
        return

    files = ['siteinfo/SiteInfo_{id}.xml'.format(id=data.get('deployment_id')),
             'siteinfo/pcm_manifest_{id}.json'.format(id=data.get('deployment_id'))]
    with temp_dir(prefix='configgen_', parent_dir=IBC['WORKING_DIRECTORY']) as tmp_work_dir:
        cw = SVNConfigWriter(tmp_work_dir, IBC['SVN_URL'], IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], siteid)
        for file in files:
            cw.delete(siteid, file)

@app.task(ignore_result=True)
def subscription_del(siteid, subscription):
    """
    This function create SVN connection and delete subscription file from SVN for a site
    :param siteid: type str, site ID
    :param subscription: type str, subscription name
    :return: None
    """
    subscription_file = 'subscription_links/{subscription}'.format(subscription=subscription)
    with temp_dir(prefix='configgen_', parent_dir=IBC['WORKING_DIRECTORY']) as tmp_work_dir:
        cw = SVNConfigWriter(tmp_work_dir, IBC['SVN_URL'], IBC['SVN_USERNAME'], IBC['SVN_PASSWORD'], siteid)
        cw.delete(siteid, subscription_file)
