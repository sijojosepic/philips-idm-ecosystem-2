from __future__ import absolute_import
from phim_backoffice.celery import app
from phim_backoffice.datastore import (
    upsert_one_by_id,
    find_one,
    find,
    delete_many
)
from phim_backoffice.event import send_event
from phim_backoffice.helpers import get_validated_fields
from tbconfig import DASHBOARD
from celery.utils.log import get_logger

logger = get_logger(__name__)


@app.task(ignore_results=True)
def handle_config_event(site_config_data):
    identifiers = []
    siteid = site_config_data.get('siteid')
    valid_hosts = []
    for host_data in site_config_data.get('payload'):
        valid_hosts.append(host_data['hostname'])
    purge_missing_configurations(siteid, valid_hosts)
    for host_data in get_host_data(site_config_data.get('payload'), siteid):
        host_data['siteid'] = siteid
        identifier = get_identifier(siteid, host_data)
        upsert_one_by_id('SITE_CONFIGURATION', identifier, host_data)
        identifiers.append(identifier)
    handle_deviation.delay(identifiers, siteid)


def purge_missing_configurations(siteid, valid_hosts):
    excedent_hosts_filter = {'siteid': siteid, 'hostname': {'$nin': valid_hosts}}
    invalid_hosts = list(find('SITE_CONFIGURATION', excedent_hosts_filter))
    if invalid_hosts:
        delete_many('SITE_CONFIGURATION', excedent_hosts_filter)


def get_host_data(payload, siteid):
    for host_data in payload:
        yield dict((k, v) for k, v in host_data.iteritems() if k in frozenset(['hostaddress', 'hostname','module_type',
                                                                               'ram_size', 'cpu_cores']))


def get_identifier(siteid, host_data):
    return '{siteid}-{host}'.format(siteid=siteid, host=host_data.get('hostname'))


@app.task(ignore_results=True)
def update_approved_deviation(**kwargs):
    data = get_validated_fields(frozenset(['siteid', 'ids']), **kwargs)
    handle_deviation.delay(data['ids'], data['siteid'])


@app.task(ignore_result=True)
def handle_deviation(identifiers, siteid):
    deviations = {}
    for identifier in identifiers:
        data = find_one('SITE_CONFIGURATION', identifier, {'_id': 0})
        try:
            comparision_keys = data['gold_copy'].keys()
            for key in comparision_keys:
                if cmp(data.get(key), data['gold_copy'].get(key)) != 0:
                    if identifier not in deviations.keys():
                    	deviations[identifier] = {}
                    deviations[identifier]["current_{param}".format(param=key)] = data.get(key)
                    deviations[identifier]["required_{param}".format(param=key)] = data['gold_copy'].get(key)
        except KeyError, e:
            pass
    event = dict(siteid=siteid,
                 hostname='localhost',
                 type='PROBLEM',
                 hostaddress='127.0.0.1',
                 service=DASHBOARD['PROBLEMATIC_SITE_CONFIGURATION_KEY'],
                 state='CRITICAL',
                 payload=dict(output='CRITICAL - {reason}'.format(reason=deviations)))
    if not len(deviations.keys()):
        event.update(
            {'state': 'OK', 'type': "RECOVERY", "payload": dict(output='OK - {reason}'.format(reason=deviations))}
        )
    send_event(**event)