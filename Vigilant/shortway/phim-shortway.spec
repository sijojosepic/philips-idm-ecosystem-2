# The following software is released as specified below.
# This spec file is released to the public domain.

# No binaries here, do not build a debuginfo package
%global debug_package %{nil}

%global phimuwsgi %{_prefix}/lib/philips/uwsgi/

# Basic Information
Name:      phim-shortway
Version:   %{_phiversion}
Release:   1%{?dist}
Summary:   phim-shortway setup

Group:     none
License:   GPL
URL:       http://www.philips.com/healthcare
# Packager Information
Packager:  Leonardo Ruiz <leonardo.ruiz@philips.com>

# Build Information
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

# Source Information
Source0:   %{name}-%{version}.tar.gz
#Patch0:

Requires:  phimutils
Requires:  phim-uwsgi
Provides:  phim-shortway
Conflicts: phim-gateway

%description
phim-shortway setup.


%prep
%setup -q

%build
echo OK

# Using Multilib Exempt Location %{_prefix}/lib instead of %{_libdir}

%install
%{__rm} -rf %{buildroot}
%{__install} -d -m 0755 %{buildroot}%{_sysconfdir}/uwsgi
%{__cp} %{_builddir}/%{name}-%{version}/uwsgi.shortway.ini %{buildroot}%{_sysconfdir}/uwsgi/shortway.ini
%{__install} -d -m 0755 %{buildroot}%{phimuwsgi}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/shortway.py %{buildroot}%{phimuwsgi}
%{__install} -Dp -m 0755 %{_builddir}/%{name}-%{version}/swconfig.py %{buildroot}%{phimuwsgi}


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/uwsgi/shortway.ini
%{phimuwsgi}
%config(noreplace) %{phimuwsgi}/swconfig.py


%changelog
* Tue Apr 07 2015 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Added uwsgi ini to be picked up by uwsgi emperor

* Thu Aug 07 2014 Leonardo Ruiz <leonardo.ruiz@philips.com>
- Initial Spec File
