SVN = {
    'host': '161.85.111.160',
    'username': 'root',
    'password': 'ph!lt0r',
    'xml_dir': '/root/ibc/common/subscriptions/'
}

REPO = {
    'host': '161.85.111.153',
    'username': 'root',
    'password': 'ph!lt0r',
    'pkg_dir': '/repo/philips/Tools/PCM/packages/'
}

PHIREPO = {
    'host': '161.85.111.153',
    'username': 'root',
    'password': 'ph!lt0r',
    'pkg_dir': '/repo/philips/Tools/PCM/packages/'
}

MONGO = {
    'host': '192.168.180.58',
    'port': 27017,
    'username': 'administrator',
    'password': 'st3nt0r',
    'database': 'somedb'
}

LOCAL_PATH = {
    'xml_path': '/repo/philips/Tools/PCM/packages/ISP/ISP-4.4.552.11.201712051356.xml',
    'zip_path': '/repo/philips/Tools/PCM/packages/ISP/ISP-4.4.552.11.201712051356.zip'
    }
