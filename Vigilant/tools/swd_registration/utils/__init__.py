import os
import os.path
import logging
import time

LOG_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'
LOG_FORMAT = '%(asctime)s.%(msecs).06dZ - %(process)d - %(name)s - %(levelname)s - %(message)s'
LOG_FILE_NAME = 'swd_registrator.log'


def _verify_dir(dir_name=None):
    if not dir_name:
        dir_path = os.path.join(os.path.expanduser('~'),
                                '/var/log/swd_registration')
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)
    return dir_path


def initialize_logger(log_file_name, log_date_format, log_format):
    logging.basicConfig(level=logging.DEBUG,
                        format=log_format,
                        datefmt=log_date_format,
                        filename=os.path.join(_verify_dir(), log_file_name),
                        filemode='w')

    logging.Formatter.converter = time.gmtime
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    return logging.getLogger('idm.registrator')


logger = initialize_logger(LOG_FILE_NAME, LOG_DATE_FORMAT, LOG_FORMAT)
