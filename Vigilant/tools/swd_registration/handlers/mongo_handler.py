from pymongo import MongoClient
from pymongo.errors import *
from utils import logger


class MongoHandler(object):
    def __init__(self, host, port, database, username='', password=''):
        self.host = host
        self.port = port
        self.database = database
        self.username = username
        self.password = password

    def create_doc(self, product_dict):
        subs_doc = {}
        for name, version in product_dict.iteritems():
            if 'main_package' in name:
                name, version = version.keys()[0], version.values()[0]
                doc_id = name + ''.join(version.split('.')[:4])
                path = "/philips/Tools/PCM/packages/{0}".format(name)
                doc_json = {
                              "_id": "{0}".format(doc_id),
                              "path": path,
                              "rules": [
                                "singlesitedeployment"
                              ],
                              "countries": [
                                "United Kingdom",
                                "United Arab Emirates",
                                "United States",
                                "Canada",
                                "Cuba",
                                "Brazil",
                                "Argentina",
                                "The Netherlands",
                                "Belgium",
                                "Italy",
                                "Germany",
                                "France",
                                "Egypt",
                                "Qatar",
                                "Saudi Arabia",
                                "Australia",
                                "Singapore",
                                "South Korea",
                                "INDIA",
                                "Austria"
                              ],
                              "version": [
                                "{0}".format(version)
                              ]
                            }
            else:
                subs_doc.update({name: version})
        if subs_doc:
            doc_json.update({'dependencies': subs_doc})
        logger.info('Document to insert to mongo: {0}'.format(doc_json))
        return doc_json

    def create_mongo_client(self, host, port, database):
        db_string = 'mongodb://{0}:{1}'.format(host, port)
        client = MongoClient(db_string)[database]
        return client

    def insert_doc(self, product_dict):
        client = self.create_mongo_client(self.host, self.port, self.database)
        subs_doc = self.create_doc(product_dict)
        try:
            client.subscriptions.insert_one(subs_doc)
        except DuplicateKeyError:
            pass
