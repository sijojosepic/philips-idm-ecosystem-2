import os
import xml.etree.cElementTree as ET
from utils import logger


class SVNHandler(object):
    def __init__(self, ssh_client, svn_path):
        self.ssh_client = ssh_client
        self.svn_path = svn_path

    def create_xml(self, product_dict):
        for key, value in product_dict.iteritems():
            manifest = ET.Element("manifest")
            tools = ET.SubElement(manifest, "dir", name="Tools")
            pcm = ET.SubElement(tools, "dir", name="PCM")
            packages = ET.SubElement(pcm, "dir", name="packages")
            name, version, file_name = key, value, key
            if key == 'main_package':
                name, version = value.keys()[0], value.values()[0]
                file_name = name + ''.join(version.split('.')[:4])
            attr = ET.SubElement(packages, "dir", name="{0}".format(name))
            ET.SubElement(attr, "file", name="{0}-{1}.xml".format(name,
                                                                  version))
            ET.SubElement(attr, "file", name="{0}-{1}.zip".format(name,
                                                                  version))
            self.svn_commit_file(file_name, ET.ElementTree(manifest))
        self.ssh_client.close()
        return product_dict

    def svn_create_file(self, file_name, xml_data):
        ssh_ftp = self.ssh_client.open_sftp()
        file_path = '{0}{1}.xml'.format(self.svn_path, file_name)
        local_file_path = '{0}.xml'.format(file_name)
        xml_data.write(local_file_path)
        ssh_ftp.put(local_file_path, file_path)

    def delete_local_file(self, file_name):
        local_file_path = '{0}.xml'.format(file_name)
        os.remove(local_file_path) if os.path.exists(local_file_path) else None

    def svn_commit_file(self, file_name, xml_data):
        self.svn_create_file(file_name, xml_data)
        cmd = 'cd {0};svn add "{1}.xml"; svn ci -m "Added {1}.xml"'
        cmd_to_run = cmd.format(self.svn_path, file_name)
        self.ssh_client.exec_command(cmd_to_run)
        self.delete_local_file(file_name)
        logger.info('Xml file {0} created on SVN.'.format(file_name))
