import os
from utils import logger
from utils.tbconfig import REPO, LOCAL_PATH, PHIREPO
from handlers.ssh_file_handler import SSHFileHandler


class RepoHandler(object):
    def __init__(self, local_zip_path, repo_pkg_dir, phirepo_pkg_dir,
                 product_dict):
        self.local_zip_path = local_zip_path
        self.repo_pkg_dir = repo_pkg_dir
        self.phirepo_pkg_dir = phirepo_pkg_dir
        self.product_dict = product_dict

    def get_files_path_to_copy(self):
        path_list = []
        zip_path_split = LOCAL_PATH['zip_path'].split('/')[:-2]
        base_path = '/'.join(zip_path_split) + '/'
        for name, version in self.product_dict.iteritems():
            if 'main_package' not in name:
                zip_path = base_path + name + '/' + name + '-' + version + '.zip'
                xml_path = base_path + name + '/' + name + '-' + version + '.xml'
            else:
                for key, value in version.iteritems():
                    zip_path = base_path + key + '/' + key + '-' + value + '.zip'
                    xml_path = base_path + key + '/' + key + '-' + value + '.xml'
            if os.path.exists(zip_path):
                path_list.append(zip_path)
            if os.path.exists(xml_path):
                path_list.append(xml_path)
        logger.info('Paths to copy to remote server: {0}'.format(path_list))
        return path_list

    def create_ssh_client(self):
        repo_file_handler = SSHFileHandler(hostaddress=REPO['host'],
                                           username=REPO['username'],
                                           password=REPO['password'],
                                           repo_path=REPO['pkg_dir'])
        repo_ssh_client = repo_file_handler.create_ssh_client()
        phirepo_file_handler = SSHFileHandler(hostaddress=PHIREPO['host'],
                                              username=PHIREPO['username'],
                                              password=PHIREPO['password'],
                                              repo_path=PHIREPO['pkg_dir'])
        phirepo_ssh_client = phirepo_file_handler.create_ssh_client()
        return repo_file_handler, phirepo_file_handler, repo_ssh_client,\
               phirepo_ssh_client

    def copy_files_to_repo(self, path_list, repo_file_handler,
                           phirepo_file_handler, repo_ssh_client,
                           phirepo_ssh_client):
        repo_file_list, phirepo_file_list = [], []
        for path in path_list:
            file_name = path.split('/')[-1]
            folder_name = file_name.split('-')[0]
            repo_file_path = self.repo_pkg_dir + folder_name + '/' + file_name
            phirepo_file_path = self.phirepo_pkg_dir + folder_name + '/' + file_name
            repo_file_handler.ssh_copy_file(repo_ssh_client, path, repo_file_path)
            phirepo_file_handler.ssh_copy_file(phirepo_ssh_client, path,
                                               phirepo_file_path)
            repo_file_list.append(repo_file_path)
            phirepo_file_list.append(phirepo_file_path)
            logger.info('File {0} copied to repo server.'.format(file_name))
        return repo_file_list, phirepo_file_list

    def zsync_make(self, repo_ssh_client, phirepo_ssh_client, repo_file_list,
                   phirepo_file_list):
        for repo_path, phirepo_path in zip(repo_file_list, phirepo_file_list):
            repo_dir = REPO['pkg_dir'] + repo_path.split('/')[-1].split('-')[0]
            phirepo_dir = REPO['pkg_dir'] + phirepo_path.split('/')[-1].split('-')[0]
            file_name = repo_path.split('/')[-1]
            repo_cmd_to_execute = 'cd {0};zsyncmake {1}'.format(repo_dir, file_name)
            phirepo_cmd_to_execute = 'cd {0};zsyncmake {1}'.format(phirepo_dir, file_name)
            repo_ssh_client.exec_command(repo_cmd_to_execute)
            phirepo_ssh_client.exec_command(phirepo_cmd_to_execute)

    def main(self):
        path_list = self.get_files_path_to_copy()
        repo_file_handler, phirepo_file_handler, repo_ssh_client,\
        phirepo_ssh_client = self.create_ssh_client()
        repo_file_list, phirepo_file_list = self.copy_files_to_repo(path_list,
                                                        repo_file_handler,
                                                        phirepo_file_handler,
                                                        repo_ssh_client,
                                                        phirepo_ssh_client)
        self.zsync_make(repo_ssh_client, phirepo_ssh_client, repo_file_list,
                        phirepo_file_list)
        repo_file_handler.close_ssh_connection(repo_ssh_client)
        phirepo_file_handler.close_ssh_connection(phirepo_ssh_client)
