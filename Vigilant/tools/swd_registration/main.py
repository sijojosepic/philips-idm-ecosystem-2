from handlers.mongo_handler import MongoHandler
from handlers.ssh_file_handler import SSHFileHandler
from handlers.svn_handler import SVNHandler
from handlers.repo_handler import RepoHandler
from utils.tbconfig import MONGO, SVN, REPO, LOCAL_PATH, PHIREPO
from utils import logger


def main():
    logger.info('Starting registration process.')
    ssh_file_handler = SSHFileHandler(xml_path=LOCAL_PATH['xml_path'],
                                      zip_path=LOCAL_PATH['zip_path'])
    xml_data = ssh_file_handler.read_xml()
    if xml_data:
      product_dict = ssh_file_handler.parse_xml(xml_data)
    else:
      product_dict = ssh_file_handler.get_product_dict_no_xml()
    logger.info('Product dict created, starting coping files to repo.')
    repo_handler = RepoHandler(LOCAL_PATH['zip_path'], REPO['pkg_dir'],
                               PHIREPO['pkg_dir'], product_dict)
    repo_handler.main()
    logger.info('Files coping to repo node completed.')
    logger.info('Starting posting xml files to SVN.')
    svn_ssh_file_handler = SSHFileHandler(SVN['host'], SVN['username'],
                                          SVN['password'])
    svn_ssh_client = svn_ssh_file_handler.create_ssh_client()
    svn_handler = SVNHandler(svn_ssh_client, SVN['xml_dir'])
    product_dict = svn_handler.create_xml(product_dict)
    logger.info('Xml files coping to SVN completed.')
    logger.info('Creating mongo document in subscriptions collection.')
    mongo_handler = MongoHandler(MONGO['host'], MONGO['port'],
                                 MONGO['database'], MONGO['username'],
                                 MONGO['password'])
    mongo_handler.insert_doc(product_dict)
    logger.info('Creating mongo document in subscriptions collection completed.')
    logger.info('Registration process completed.')


if __name__ == '__main__':
    main()
