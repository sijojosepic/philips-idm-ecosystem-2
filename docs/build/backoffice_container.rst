===================
Building Backoffice 
===================

**************
Prerequisites:
**************

		1.	Docker Engine and docker-compose should be available in worker nodes
		2.	Registry should be accessbile by configuring on /etc/hosts.
::


To Spin up system:
******************

		1.	Check Jenkins Build in (161.85.111.196) where Images are built and pushed.There is a saperate repo maintained for Docker. Node the repositry details bcos thats where we will do pull.
		2.	We are building 2 images which are for nginx and idmngapp{env}  where env can be dev,stg,prod depending on environment. nginx and idmngapp containers are wired to serve the requests.
        3.  On nginx container and idmngapp{env} container pull the respective images ::

        Example:

        ========
        ::

        - To pull nginx image into your node,run below command 
          *docker run -d --name cfnginx  idmregistry.phim.isyntax.net/idm-nginx-lb*

        ::

        - To pull idmngapp image into your node,run 
          *docker run -d --name cfidmngapp idmregistry.phim.isyntax.net/idmngappdev*

        Note::
         1) Argument to --name can be your choice but for repositry && build information refer Jenkins build or next-gen-vig branch on Bitbucket 