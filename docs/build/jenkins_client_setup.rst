..  _jenkins_client_setup:

==============================
Jenkins Client Setup for Linux
==============================

The following steps will get a RHEL based system ready to accept the jenkins agent for use with a Jenkins Master server.
All connections will happen over SSH, thus password-less ssh keys are used. The following will outline the steps needed
to get the agent prerequisites ready.

.. note:: The ``jre`` group comes from phirepo, thus the ``phirepo-1.0-1.el6.noarch`` must be installed first

::

    yum -y install jre
    useradd -m -g users jenkins
    su - jenkins -c 'ssh-keygen -t rsa -N "" -f /home/jenkins/.ssh/id_rsa'
    su - jenkins -c 'cp -v /home/jenkins/.ssh/id_rsa.pub /home/jenkins/.ssh/authorized_keys'
    su - jenkins -c 'ssh-keyscan -t rsa localhost >> /home/jenkins/.ssh/known_hosts'
    su - jenkins -c 'cat >> /home/jenkins/.ssh/authorized_keys << "EOF"
    ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAzrlISRmR3f0NrmG05XPD91fShHvHhj0ldGNqqn2rN9lhSFvchHn52KDIQHruWH6wg5sQBSNv5152UWXA6Mi3hXNMx/vIMtiZUyGh8T8pCwa9MujjQpr3/7jf0PTMOi5PjS9aHt208pmcX+oMbAWGF4SXgeq1X6+5WplXqn0uu0vR+L5F6ErvRpN+VHZHO8FLo02Un2rFWgumGFfrtrMbkit9/OfF4G1V7uHowxHt8MrrrspELISW6qE8uhZvA5kKDAtGbPpWaqaBU6IxlsWHYbl5dSriR0WooiXAEuLsYqjGvFRIC3UX3KxMT78W8c4rYb9OsL4slUmRAp/aaxZ64Q==
    EOF'
    cat > /etc/sudoers.d/01-jenkins << "EOF"
    Defaults:jenkins    !requiretty
    jenkins    ALL=(ALL)   NOPASSWD: ALL
    EOF
