..  _ci_setup:

============================
Continuous Integration Setup
============================

High level Overview
-------------------

.. image:: ../images/ci_highlevel.png

Prerequisites
-------------

- Each instance that is part of the CI environment is built from a base installation of a CentOS 6.4 minimal ISO.
- For the repository instances, an additional disk drive is needed to mount the ``/repo`` location.
- All the target systems must have the password-less ssh keys for the build server user to connect for the rsync

Endpoints
---------

The following nodes are for the CI environment

::

    172.16.7.191 - idm-ci-repo-r1
    172.16.7.192 - idm-ci-repo-r2
    172.16.7.193 - idm-ci-lb
    172.16.7.194 - idm-ci-svn
    172.16.7.195 - idm-ci-vigilant-r1
    172.16.7.196 - idm-ci-svn-r1-only

Endpoint Mappings that are located on the Load Balancer instance

::

    172.16.7.199 - IDM Endpoint that references the Release 1 state
    172.16.7.201 - Standard IDM Vigilant for Latest trunk
    172.16.7.202 - Jenkins Build endpoint for Latest trunk

Application breakdown
---------------------

- ``CI Master (testctrl)`` instance role is Jenkins client running Ansible and vSphere connections
- Each ``Repository`` instance role uses ``nginx`` as the web server and ``pypi-server`` for Pip packages
- Each ``Subversion`` instance role uses ``httpd`` as the web server due to the ``mod_svn`` components
- Each ``Load Balancer`` instance role uses ``nginx`` for proxy'ing the connections

Setup
-----

.. note:: Assumption is a based non-configured instance is ready with the appropriate network bootstrapping and a host
          entry for the upstream repository named ``repo.phim.isyntax.net``. The system should then be accessible over
          the network via ssh connections.

::

    echo "167.81.182.65 repo.phim.isyntax.net" >> /etc/hosts
    chkconfig iptables off
    chkconfig ip6tables off
    sed -i 's/=enforcing/=disabled/g' /etc/selinux/config
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime
    yum --disablerepo=* -y install http://repo.phim.isyntax.net/phirepo/noarch/phirepo-1.1.1734-1.el6.noarch.rpm
    yum --disablerepo=* --enablerepo=phirepo -y install pcentos pepel pmongodb prpmforge
    yum -y install rsync
    ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
    cat >> ~/.ssh/authorized_keys << "EOF"
    ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA8znQLFOaw28m/80MKD2v9dRG2F44eVSKXwj1DQk24Mc3oe0JzsjQraDsRFs30AwxWvtfbx2WYQE54H4gWlZu57Z9CofRYgI9veanCZq+VhlLLsnBcn0cWztrgimys5p4F2pNaGfB3MbMk1EsZxi7wCytytgR0hVIfZXHOdZN2d4Tf3xKbug8IwDsEnekI6Wljo4aXjsd7FdQ1TMQd533sUSDgljHbxV6Rxd8v2+KyS5GwJM5R8lBPbK4O//c4DoJEj4/hfTOb77aVdRsPrgij9x/AtAASqFlofiXFWEYBj9mR2DJ7d7U1BMykkVIgTaZszOiO+L8djhVyGpFnwSKXw==
    EOF
    reboot

CI Master
^^^^^^^^^

- This is a Jenkins client that is used to control the CI environment. It handles the calls to the vSphere for any VM
  object needs. It also is the Control Master for Ansible for nodes that are not using the SVN pull mechanism.
- *This server must have access to the code archive for checkout of the necessary tasks*.
- For the Jenkins client setup, reference the following link

:ref:`jenkins_client_setup`

- Execute the following steps to bring the system online after the node has been joined to the Jenkins Master instance

.. note:: The svn client must be >=1.7 for the ``wcroot-abspath`` in the xml output.

::

    yum -y install gcc python-devel python-setuptools openssh-server openssh-clients make wget vixie-cron tmux sudo tcpdump telnet traceroute tree lsof rsync patch rpmlint
    yum -y install python-yaml python-blist python-requests
    yum -y install python-dateutil python-argparse libxml2-devel
    yum -y install libxml2-python python-lxml
    yum -y install http://sfo-ce-webhead.sfohi.philips.com/thirdparty/Collabnet/CollabNetSubversion-client-1.7.13-2.x86_64.rpm
    /usr/bin/easy_install -i 'http://repo.phim.isyntax.net:8084/simple' pip
    cat > /etc/profile.d/collabnet.sh << "EOF"
    pathmunge /opt/CollabNet_Subversion/bin
    EOF
    chmod 0755 /etc/profile.d/collabnet.sh
    mkdir -pv ~/.pip
    cat > ~/.pip/pip.conf << "EOF"
    [global]
    timeout = 60
    index-url = http://repo.phim.isyntax.net:8084/simple
    EOF
    pip install virtualenv
    su - jenkins -c "mkdir -pv ~/.pip"
    su - jenkins -c 'cat > ~/.pip/pip.conf << "EOF"
    [global]
    timeout = 60
    index-url = http://repo.phim.isyntax.net:8084/simple
    EOF'
    su - jenkins -c "virtualenv --system-site-packages venv"
    su - jenkins
    source venv/bin/activate
    pip install ansible pysphere bottle pexpect
    deactivate
    exit
    reboot


Checkout the working copy into ``/home/jenkins/Monitoring``

::

    su - jenkins
    svn co https://svn-01.ta.philips.com/svn/eici-monitoring/IDMEcosystem/trunk Monitoring --username=sfo-monitoring --config-option servers:global:store-passwords=yes

Manually execute the versioning to produce the BuildDetails needed for Jenkins to display the version information.

::

    python /home/jenkins/Monitoring/build_scripts/linux/generate_build_manifest.py -wc /home/jenkins/Monitoring -o /home/jenkins/Monitoring/build_scripts/linux/IDMEcosystem_BuildDetails.txt -vxl /home/jenkins/Monitoring/MasterBuild/Versioning/IDMEcosystem/trunk/IDMEcosystemVersionLog.xml -pn 'Intelligent Device Management'

Add the following build steps to the execute the CI

::

    /home/jenkins/venv/bin/python /home/jenkins/Monitoring/build_scripts/ci/snapper.py -vc 167.81.176.83 -u administrator -p st3nt0r -vm idm-ci-neb-01
    /home/jenkins/venv/bin/python /home/jenkins/Monitoring/build_scripts/ci/snapper.py -vc 167.81.176.83 -u administrator -p st3nt0r -vm idm-ci-vigilant-01

Add the following for the test systems that are manually built

::

    /home/jenkins/venv/bin/python /home/jenkins/Monitoring/build_scripts/ci/snapper.py -vc 167.81.176.83 -u administrator -p st3nt0r -vm idm-ci-neb-test-01
    /home/jenkins/venv/bin/python /home/jenkins/Monitoring/build_scripts/ci/snapper.py -vc 167.81.176.83 -u administrator -p st3nt0r -vm idm-ci-vigilant-test-01

Adding the system test framework to the control server

::

    cd ~
    svn co https://svn-01.ta.philips.com/svn/eici-monitoring/TestFramework/trunk TestFramework --username=sfo-monitoring

Within Jenkins, add an execution step to the ``IDM CI`` after the commands reverting the snapshot of the target nodes to
 execute the test framework

::

    /usr/bin/perl /home/jenkins/TestFramework/idmautotest.pl

Repository Role
^^^^^^^^^^^^^^^

- Add an additional virtual disk to the node
- Power on / reboot the node so that the drive is picked up by the kernel
- Execute the following steps to configure the new mount point for the data

.. note:: This assumes your additional disk is the second physical disk - being ``sdb``. The fdisk command will create a single partition utilizing all the drive space.

::

    echo -e "n\np\n1\n\n\nw" | fdisk /dev/sdb
    mkfs.ext4 /dev/sdb1
    mkdir -pv /repo
    echo "UUID=`blkid /dev/sdb1 | cut -d'"' -f2` /repo ext4 defaults 1 2" >> /etc/fstab
    mount -av

- Execute the following steps install the necessary applications

::

    yum -y install nginx
    yum -y install gcc python-devel python-setuptools createrepo
    /usr/bin/easy_install -i 'http://repo.phim.isyntax.net:8084/simple' pip
    pip install -i 'http://repo.phim.isyntax.net:8084/simple' pypiserver
    pip install -i 'http://repo.phim.isyntax.net:8084/simple' supervisor
    yum -y install http://repo.phim.isyntax.net/phirepo/noarch/supervisor-daemon-3.0-1.el6.noarch.rpm

Nginx configuration items

::

    sed -i 's/keepalive_timeout  65/keepalive_timeout  10/' /etc/nginx/nginx.conf
    sed -i 's/#gzip  on/gzip  on/' /etc/nginx/nginx.conf
    sed -i '/keepalive_timeout  10/a \ \ \ \ client_max_body_size  20m;' /etc/nginx/nginx.conf
    sed -i 's/*.conf/philips*.conf/' /etc/nginx/nginx.conf

    cat > /etc/nginx/conf.d/philips-repo.conf << "EOF"
    server {
        listen *:80;
        server_name  _;

        location ~ ^/(base|bin|bundles|epel|extras|mongo|philips|phirepo|rpmforge|updates|nginx)/ {
            autoindex on;
            root /repo;
        }

        location ~ ^/(simple|packages)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://127.0.0.1:8084;
        }
    }
    EOF

    service nginx start
    chkconfig nginx on

PyPI Server configuration items - Go to R1 section below if setting up a specific R1 repo.

::

    useradd repo
    mkdir -pv /repo/phipypi

    cat > /etc/supervisord.d/philips-pypi.conf << "EOF"
    [program:pypiserver]
    command=/usr/bin/pypi-server -p 8084 /repo/phipypi/
    user=repo
    autorestart=true
    log_stderr=true
    redirect_stderr=true
    logfile=/var/log/supervisor/pypiserver.log
    logfile_maxbytes=10MB
    logfile_backups=10
    EOF

    service supervisord start
    chkconfig supervisord on

Create dummy repos

::

    mkdir -pv /repo/{base,bin,bundles,epel,extras,mongo,rpmforge,updates,nginx}/x86_64
    createrepo --basedir=/repo/base x86_64
    createrepo --basedir=/repo/bin x86_64
    createrepo --basedir=/repo/bundles x86_64
    createrepo --basedir=/repo/epel x86_64
    createrepo --basedir=/repo/extras x86_64
    createrepo --basedir=/repo/mongo x86_64
    createrepo --basedir=/repo/rpmforge x86_64
    createrepo --basedir=/repo/updates x86_64
    createrepo --basedir=/repo/nginx x86_64
    
Repository R1
^^^^^^^^^^^^^
Handle the Repository setup information but make the following adjustments for setting up a specific R1 repo

PyPI Server configuration items

::

    useradd repo

    cat > /etc/supervisord.d/philips-pypi.conf << "EOF"
    [program:pypiserver]
    command=/usr/bin/pypi-server -p 8084 /repo/pypi/
    user=repo
    autorestart=true
    log_stderr=true
    redirect_stderr=true
    logfile=/var/log/supervisor/pypiserver.log
    logfile_maxbytes=10MB
    logfile_backups=10
    EOF

    service supervisord start
    chkconfig supervisord on

Subversion Role
^^^^^^^^^^^^^^^

- Execute the following steps install the necessary applications

::

    yum -y install http://repo.phim.isyntax.net/base/x86_64/Packages/mailcap-2.1.31-2.el6.noarch.rpm
    yum -y install httpd mod_dav_svn

Configure Apache to serve the svn instance.

::

    cat > /etc/httpd/conf.d/svn.conf << "EOF"
    <Location /svn>
      DAV svn
      SVNPath /var/www/html/ibcsvn

      # Authentication: Basic
      AuthName "Subversion repository"
      AuthType Basic
      AuthUserFile /var/www/html/svn-auth.htpasswd

      # Authorization: Authenticated users only
      Require valid-user

    </Location>
    EOF

    service httpd start
    chkconfig httpd on

Configure user account access for the svn container

::

    htpasswd -bc /var/www/html/svn-auth.htpasswd operator st3nt0r
    htpasswd -b /var/www/html/svn-auth.htpasswd phisvnuser st3nt0r

Load Balancer Role
^^^^^^^^^^^^^^^^^^

The load balancer distribution is handled by configuring ``nginx`` to act as a reverse style proxy, abstracting the
actual roles behind the load balancer that the client needs to consume services from.

This instance will use a primary interface for all management traffic, but virtual / sub interfaces will be used to host
the service endpoints.

- Configuration of the sub interfaces

.. note:: This assumes the eth0 is the primary interface and is already configured. You will need to change the IP
          Address for the substitution if additional interfaces are needed or the ip range is different.

::

    cp -v /etc/sysconfig/network-scripts/ifcfg-eth0{,:1}
    sed -i 's/=eth0/=eth0:1/g' /etc/sysconfig/network-scripts/ifcfg-eth0:1
    sed -i 's/.7.193/.7.201/g' /etc/sysconfig/network-scripts/ifcfg-eth0:1
    sed -i '/GATEWAY=/d' /etc/sysconfig/network-scripts/ifcfg-eth0:1
    ifup eth0:1

    cp -v /etc/sysconfig/network-scripts/ifcfg-eth0{,:2}
    sed -i 's/=eth0/=eth0:2/g' /etc/sysconfig/network-scripts/ifcfg-eth0:2
    sed -i 's/.7.193/.7.202/g' /etc/sysconfig/network-scripts/ifcfg-eth0:2
    sed -i '/GATEWAY=/d' /etc/sysconfig/network-scripts/ifcfg-eth0:2
    ifup eth0:2

    cp -v /etc/sysconfig/network-scripts/ifcfg-eth0{,:3}
    sed -i 's/=eth0/=eth0:3/g' /etc/sysconfig/network-scripts/ifcfg-eth0:3
    sed -i 's/.7.193/.7.209/g' /etc/sysconfig/network-scripts/ifcfg-eth0:3
    sed -i '/GATEWAY=/d' /etc/sysconfig/network-scripts/ifcfg-eth0:3
    ifup eth0:3

    cp -v /etc/sysconfig/network-scripts/ifcfg-eth0{,:4}
    sed -i 's/=eth0/=eth0:4/g' /etc/sysconfig/network-scripts/ifcfg-eth0:4
    sed -i 's/.7.193/.7.210/g' /etc/sysconfig/network-scripts/ifcfg-eth0:4
    sed -i '/GATEWAY=/d' /etc/sysconfig/network-scripts/ifcfg-eth0:4
    ifup eth0:4

    cp -v /etc/sysconfig/network-scripts/ifcfg-eth0{,:5}
    sed -i 's/=eth0/=eth0:5/g' /etc/sysconfig/network-scripts/ifcfg-eth0:5
    sed -i 's/.7.193/.7.199/g' /etc/sysconfig/network-scripts/ifcfg-eth0:5
    sed -i '/GATEWAY=/d' /etc/sysconfig/network-scripts/ifcfg-eth0:5
    ifup eth0:5

- Execute the following steps install the web server

::

    yum -y install nginx

- The following nodes are considered to be the backend services. These IP Addresses can be changed based on the setup.

::

    sed -i 's/keepalive_timeout  65/keepalive_timeout  10/' /etc/nginx/nginx.conf
    sed -i 's/#gzip  on/gzip  on/' /etc/nginx/nginx.conf
    sed -i '/keepalive_timeout  10/a \ \ \ \ client_max_body_size  20m;' /etc/nginx/nginx.conf
    sed -i 's/*.conf/philips*.conf/' /etc/nginx/nginx.conf

    cat > /etc/nginx/conf.d/philips-lb-latest.conf << "EOF"
    server {
        #
        # IDM R2 (latest) rules
        #
        listen 172.16.7.201:80;
        server_name  _;
        location ~ ^/(notification|perfdata|state|configuration|discovery|heartbeat|health|queuetransport) {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.205;
        }
        location ~ ^/(base|bin|bundles|epel|extras|mongo|philips|phirepo|rpmforge|updates|nginx)/ {
            autoindex on;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.192;
        }
        location ~ ^/(svn|viewvc|viewvc-static)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.194;
        }
        location ~ ^/(simple|packages)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.192:8084;
        }
    }
    EOF

    cat > /etc/nginx/conf.d/philips-jenkins-latest.conf << "EOF"
    server {
        #
        # IDM Jenkins Build notification - Persephone
        #
        listen 172.16.7.202:80;
        server_name  _;
        location ~ ^/(notification|perfdata|state|configuration|discovery|heartbeat|health|queuetransport) {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://167.81.182.91:9081;
        }
    }
    EOF

    cat > /etc/nginx/conf.d/philips-test-01.conf << "EOF"
    server {
        #
        # IDM R2 (latest) rules
        #
        listen 172.16.7.209:80;
        server_name  _;
        location ~ ^/(notification|perfdata|state|configuration|discovery|heartbeat|health|queuetransport) {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.207;
        }
        location ~ ^/(base|bin|bundles|epel|extras|mongo|philips|phirepo|rpmforge|updates|nginx)/ {
            autoindex on;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.192;
        }
        location ~ ^/(svn|viewvc|viewvc-static)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.194;
        }
        location ~ ^/(simple|packages)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.192:8084;
        }
    }
    EOF

    cat > /etc/nginx/conf.d/philips-jenkins-test-01.conf << "EOF"
    server {
        #
        # IDM Jenkins Build notification - Persephone
        #
        listen 172.16.7.210:80;
        server_name  _;
        location ~ ^/(notification|perfdata|state|configuration|discovery|heartbeat|health|queuetransport) {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://167.81.182.91:9082;
        }
    }
    EOF

    cat > /etc/nginx/conf.d/philips-r1.conf << "EOF"
    server {
        #
        # IDM R1 rules
        #
        listen 172.16.7.199:80;
        server_name  _;
        location ~ ^/(notification|perfdata|state|configuration|discovery|heartbeat|health|queuetransport) {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.195;
        }
        location ~ ^/(base|bin|bundles|epel|extras|mongo|philips|phirepo|rpmforge|updates|nginx)/ {
            autoindex on;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.191;
        }
        location ~ ^/(svn|viewvc|viewvc-static)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.196;
        }
        location ~ ^/(simple|packages)/ {
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://172.16.7.191:8084;
        }
    }
    EOF

Verify the configurations

::

    /usr/sbin/nginx -t -c /etc/nginx/nginx.conf

The output should be as follows:

| ``nginx: the configuration file /etc/nginx/nginx.conf syntax is ok``
| ``nginx: configuration file /etc/nginx/nginx.conf test is successful```

Turn the service to start at boot time and turn the service on now

::

    chkconfig nginx on
    service nginx start

Verify that your addresses are listening properly

::

    netstat -ant | grep 80

Response output:

| ``tcp        0      0 172.16.7.201:80             0.0.0.0:*                   LISTEN``
| ``tcp        0      0 172.16.7.202:80             0.0.0.0:*                   LISTEN``
| ``tcp        0      0 172.16.7.210:80             0.0.0.0:*                   LISTEN``
| ``tcp        0      0 172.16.7.209:80             0.0.0.0:*                   LISTEN``


Jenkins Build Steps
^^^^^^^^^^^^^^^^^^^

.. image:: ../images/ci_jenkins_build_steps.png

Under the build for ``PHIRepo``, a build step needs to be added.

Add an ``Execute shell`` step *after* the ``phirepo/fabfile.py``

Below are the commands that need to be entered into the shell steps.

::

    /usr/bin/rsync -rlDvz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o IdentityFile=/home/jenkins/.ssh/id_rsa" /home/jenkins/phirepo root@172.16.7.192:/repo/
    /usr/bin/rsync -rlDvz --delete -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o IdentityFile=/home/jenkins/.ssh/id_rsa" /home/jenkins/phipypi root@172.16.7.192:/repo/
    /usr/bin/rsync -rlDvz --delete --perms --chmod=ugo=rwx -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o IdentityFile=/home/jenkins/.ssh/id_rsa" /home/jenkins/ibcsvn root@172.16.7.194:/var/www/html/

Click ``Apply``

Click ``Save``

At this point the new steps have been added to the build.
