change docker proxy to proxy on sysctl/docker, and restart docker

tail -2 /etc/sysconfig/docker
HTTP_PROXY='http://199.168.151.10:9480'
HTTPS_PROXY='http://199.168.151.10:9480'

 /bin/systemctl restart  docker


docker pull docker.io/redis

docker pull docker.io/rabbitmq

docker pull docker.io/centos:6

docker pull docker.io/nginx


########### LREPOS BASE #################

#! on host
docker run -it --name lrepos --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 centos:6 /bin/bash


inside here point yum to build repos, and pypi
disable all other repos

## not needed with the --add-host
##add to /etc/hosts (tihs does not survive a restart/keep that in mind)
##172.16.7.201  repo.phim.isyntax.net
##172.16.7.201  pypi.phim.isyntax.net


cat /etc/yum.repos.d/phirepo.repo
# file /etc/yum.repos.d/phirepo.repo
[phirepo]
name=phirepo
baseurl=https://repo.phim.isyntax.net/phirepo
        http://repo.phim.isyntax.net/phirepo
proxy=_none_
enabled=1
gpgcheck=0

[pbase]
name=CentOS-$releasever - Base
baseurl=https://repo.phim.isyntax.net/base/$basearch/
        http://repo.phim.isyntax.net/base/$basearch/
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
proxy=_none_

#released updates
[pupdates]
name=CentOS-$releasever - Updates
baseurl=https://repo.phim.isyntax.net/updates/$basearch/
        http://repo.phim.isyntax.net/updates/$basearch/
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
proxy=_none_

#additional packages that may be useful
[pextras]
name=CentOS-$releasever - Extras
baseurl=https://repo.phim.isyntax.net/extras/$basearch/
        http://repo.phim.isyntax.net/extras/$basearch/
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
proxy=_none_

[pepel]
name=Extra Packages for Enterprise Linux 6 - $basearch
baseurl=https://repo.phim.isyntax.net/epel/$basearch
        http://repo.phim.isyntax.net/epel/$basearch
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6
proxy=_none_

[pmongodb]
name=MongoDB Repository
baseurl=https://repo.phim.isyntax.net/mongo/$basearch/
        http://repo.phim.isyntax.net/mongo/$basearch/
gpgcheck=0
enabled=1
proxy=_none_

[pnginx]
name=Nginx Repository
baseurl=https://repo.phim.isyntax.net/nginx/$basearch/
        http://repo.phim.isyntax.net/nginx/$basearch/
gpgcheck=0
enabled=1
proxy=_none_
### Name: RPMforge RPM Repository for RHEL 6 - dag
### URL: http://rpmforge.net/

[prpmforge]
name = RHEL $releasever - RPMforge.net - dag
baseurl = https://repo.phim.isyntax.net/rpmforge/$basearch
          http://repo.phim.isyntax.net/rpmforge/$basearch
enabled = 1
protect = 0
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rpmforge-dag
gpgcheck = 1
proxy=_none_

[prpmforge-extras]
name = RHEL $releasever - RPMforge.net - extras
baseurl = https://repo.phim.isyntax.net/rpmforge_extras/$basearch
          http://repo.phim.isyntax.net/rpmforge_extras/$basearch
enabled = 0
protect = 0
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rpmforge-dag
gpgcheck = 1
proxy=_none_

#!-- end phirepo file

mkdir /root/.pip
cat /root/.pip/pip.conf
[global]
index-url = http://pypi.phim.isyntax.net/simple/
use-mirrors = false
trusted-host = pypi.phim.isyntax.net
# seems like using custom build dir prevents cleanup after build, which in turn creates failures on updates
# commenting out for now
# build = /root/tmp


yum install python-setuptools

/usr/bin/easy_install -i 'http://pypi.phim.isyntax.net/simple/' pip
pip install setuptools --upgrade


# create some handy groups (tihs will help with keeping UID/GID straight in dependant images)
useradd philips
useradd -G philips nagios



exit

#! on host
docker commit lrepos phim/lrepos

#############################################

############ Shinken base ##################

#! on host
docker run -it --name shinkenbase --add-host "repo.phim.isyntax.net":172.16.7.201 phim/lrepos /bin/bash

### NEED TO FIX THIS TO NOT NEED THE REPOS #####
#enable the following repos with proxy
vi /etc/yum.repos.d/CentOS-Base.repo
##-- start file content

[base]
name=CentOS-$releasever - Base
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os&infra=$infra
#baseurl=http://mirror.centos.org/centos/$releasever/os/$basearch/
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
#enabled=0
proxy=http://199.168.151.10:9480

#released updates
[updates]
name=CentOS-$releasever - Updates
mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=updates&infra=$infra
#baseurl=http://mirror.centos.org/centos/$releasever/updates/$basearch/
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
#enabled=0
proxy=http://199.168.151.10:9480

##-- end file content

yum clean all

yum install shinken

##### The following may be off as in a run, shinken 2.0.3 is being installed at this point
## At this point the latest shinken is installed, this is not what is needed. 2.0.3 is needed (TESTED version)

yum remove shinken

#! Disable the two enabled repos in the previous step

vi /etc/yum.repos.d/CentOS-Base.repo
##-- start file content

[base]
...
enabled=0
proxy=http://199.168.151.10:9480

#released updates
[updates]
...
enabled=0
proxy=http://199.168.151.10:9480

##-- end file content



yum clean all
yum install shinken


mkdir -p /var/lib/shinken/share
mkdir -p /var/lib/shinken/doc

exit

#! on host
docker commit shinkenbase phim/shinkenbase

#############################################

############ Shinken arbiter ##################

#! on host
docker run -it --name shinkenarbiter --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/shinkenbase /bin/bash

yum install shinken-arbiter

exit

#! on host
docker commit shinkenarbiter phim/shinkenarbiter

#############################################

############ Shinken broker ##################

#! on host
docker run -it --name shinkenbroker --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/shinkenbase /bin/bash

yum install shinken-broker
yum install shinken-module-livestatus shinken-module-npcdmod shinken-celery_task_perfdata-broker

exit

#! on host
docker commit shinkenbroker phim/shinkenbroker

#############################################

############ Shinken poller ##################

#! on host
docker run -it --name shinkenpoller --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/shinkenbase /bin/bash

yum install shinken-poller
yum install shinken-module-cryptresource-poller
yum install fping nmap flex bison nagios-plugins nagios-plugins-all
yum remove perl-XML-SAX-Base
yum install -y perl-XML-LibXML perl-CGI --exclude=perl-XML-SAX-Base
yum install python-requests python-beautifulsoup4 python-netifaces python-minimock python-mock python-yaml
pip install requests --upgrade
pip install pynagios --upgrade
pip install jsonpath_rw --upgrade

## ENABLE THOSE TWO REPOS AGAIN BEFORE NEXT STEP
/usr/bin/yum -y groupinstall nagiosphi --exclude=perl-XML-SAX-Base --exclude=perl-Scalar-List-Utils
yum install gcc
yum install libffi-devel
yum install openssl-devel
## DISABLE THOSE TWO REPOS AGAIN AFTER PREVIOUS STEP

yum install python-devel

pip install cryptography --upgrade

exit

#! on host
docker commit shinkenpoller phim/shinkenpoller

#############################################

############ Shinken scheduler ##################

# scheduler does notifications, so it will need plugins for that ## or does it?, maybe it is the reactionner that needs it

#! on host
docker run -it --name shinkenscheduler --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/shinkenbase /bin/bash

yum install shinken-scheduler
yum install shinken-module-pickle-retention-file-scheduler
## yum install nagios-plugins-post_notification  ## maybe not needed

exit

#! on host
docker commit shinkenscheduler phim/shinkenscheduler

#############################################


############ Shinken reactionner ##################

#! on host
docker run -it --name shinkenreactionner --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/shinkenbase /bin/bash

yum install shinken-reactionner
yum install nagios-plugins-post_notification

exit

#! on host
docker commit shinkenreactionner phim/shinkenreactionner

#############################################

############ Shinken receiver ##################

#! on host
docker run -it --name shinkenreceiver --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/shinkenbase /bin/bash

yum install python-yaml
yum install shinken-receiver
yum install shinken-ws_passive_service_map-receiver

exit

#! on host
docker commit shinkenreceiver phim/shinkenreceiver

#############################################

############ uwsgi ###########################

#! on host
docker run -it --name uwsgi --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/lrepos /bin/bash

## ENABLE THOSE TWO REPOS AGAIN BEFORE NEXT STEP
yum install gcc
yum install python-devel
yum install perl perl-ExtUtils-Embed
## DISABLE THOSE TWO REPOS AGAIN AFTER PREVIOUS STEP

pip install uwsgi

# On host
#! copy uwsgi-2.0.11.2.tar.gz from thirdparty to docker host
docker cp uwsgi-2.0.11.2.tar.gz uwsgi:/root/
#on container
cd /root/
tar -zxvf uwsgi-2.0.11.2.tar.gz
cd uwsgi-2.0.11.2
/usr/bin/uwsgi --build-plugin plugins/psgi
mkdir -p /usr/lib/uwsgi/plugins
mv psgi_plugin.so /usr/lib/uwsgi/plugins/
useradd uwsgi

exit

#! on host
docker commit uwsgi phim/uwsgi

#############################################

############ Thruk ###########################

#! on host
docker run -it --name thruk --add-host "repo.phim.isyntax.net pypi.phim.isyntax.net":172.16.7.201 phim/uwsgi /bin/bash

## ENABLE THOSE TWO REPOS AGAIN BEFORE NEXT STEP
yum install perl-File-Slurp perl-Date-Calc local-perl-Module-Load perl-Template-Toolkit perl-Log-Log4perl perl-GD perl-Apache-LogFormat-Compiler perl-File-ShareDir-Install perl-HTTP-Tiny perl-Plack perl-podlators perl-Pod-Usage perl-Stream-Buffered
yum install thruk
yum install perl-JSON-XS
## DISABLE THOSE TWO REPOS AGAIN AFTER PREVIOUS STEP

mkdir -p /var/{cache,lib,log}/thruk
chown uwsgi:uwsgi /var/{cache,lib,log}/thruk

mkdir -p /etc/uwsgi

#! copy contents from thruk.ini from thruk role to /etc/uwsgi/thruk.ini !-> changinf 127.0.0.1 to 0.0.0.0
### START contents!

[uwsgi]
plugin-dir = /usr/lib/uwsgi/plugins/
plugin = psgi
socket = 0.0.0.0:4040
master = True
processes = 1
threads = 2
env = THRUK_CONFIG=/etc/thruk/
env = PERL5LIB=$PERL5LIB:/usr/share/thruk/lib/:/usr/lib/thruk/perl5
psgi = /usr/share/thruk/script/thruk.psgi


### END contents!

chown -Rv uwsgi:uwsgi /var/lib/thruk

#! copy contents from thruk_local.conf from thruk role to /etc/thruk/thruk_local.conf !! modifying the peer address from localhost to broker0
### START contents!

############################################
# put your own settings into this file
# settings from this file will override
# those from the thruk.conf
############################################
<Component Thruk::Backend>
    <peer>
        name    = local
        id      = 7215e
        type    = livestatus
        <options>
            peer          = broker0:50000
        </options>
    </peer>
</Component>


### END contents!


##### FOR REFERENCE start uwsgi for thruk with "uwsgi --ini /etc/uwsgi/thruk.ini"

exit

#! on host
docker commit thruk phim/thruk

#############################################

############ Nginx ###########################

#! on host
docker run -it --name pnginx docker.io/nginx /bin/bash

# this image does not have vim, you can edit on the host and to a "docker cp" to get files in right location

mkdir /etc/nginx/conf.d/default_server_includes

#! add hte following in /etc/nginx/conf.d/default.conf inside the "server" block
include /etc/nginx/conf.d/default_server_includes/*.conf;

#! copy contents from proxy.conf from nginx role to /etc/nginx/conf.d/proxy.conf !!
### START contents!

proxy_connect_timeout   90;
proxy_send_timeout      90;
proxy_read_timeout      90;
proxy_buffer_size       16k;
proxy_buffers      32   16k;
proxy_busy_buffers_size 64k;

proxy_set_header    Host                $http_host;
proxy_set_header    X-Real-IP           $remote_addr;
proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;

### END contents!


#! copy contents from nginx.thruk.conf from thruk role to  /etc/nginx/conf.d/default_server_includes/thruk.conf !! modifying it to not have https, and to use thruk0 as throk host
### START contents!


# add the slash if not passed, otherwise it may break :(
location = /thruk {
    return 301 http://$host/thruk/;
}

location /nagios {
    return 301 http://$host/thruk/;
}

location /thruk/documentation.html {
     alias /usr/share/thruk/root/thruk/documentation.html;
}
location /thruk/startup.html {
     alias /usr/share/thruk/root/thruk/startup.html;
}
location ~ ^/thruk/plugins/(.*?)/(.*)$ {
     alias /etc/thruk/plugins/plugins-enabled/$1/root/$2;
}
location /thruk/themes/ {
     alias /etc/thruk/themes/themes-enabled/;
}

location @thruk {
    uwsgi_pass       thruk0:4040;
    uwsgi_param      REMOTE_USER $remote_user;
    uwsgi_modifier1  5;
    include uwsgi_params;
}

location /thruk/ {
    auth_basic              "Monitoring Access";
    auth_basic_user_file    /etc/thruk/htpasswd;

    root /usr/share/thruk/root;
    # First attempt to serve request as file, then
    # as directory, then fall back to displaying a 404.
    try_files $uri @thruk;
}


### END contents!



#! copy contents from nginx.pnp4nagios.conf from pnp4nagios role to  /etc/nginx/conf.d/default_server_includes/pnp4nagios.conf !! modifying 127.0.0.1:9000 to use pnp4nagiosphp0:9000
### START contents!


# PNP4Nagios Configuration
location /pnp4nagios {
    alias /usr/share/nagios/html/pnp4nagios;
    index index.php;
    try_files $uri $uri/ @pnp4nagios;
}

location @pnp4nagios {
    root /usr/share/nagios/html/pnp4nagios;

    if ($uri !~ /pnp4nagios/index.php(.*)) {
        rewrite ^/pnp4nagios/(.*)$ /pnp4nagios/index.php/$1;
        break;
    }

    fastcgi_pass pnp4nagiosphp0:9000;
    fastcgi_index index.php;
    include fastcgi_params;
    # this splits out the trailing path
    # eg index.php?host -> $fastcgi_path_info == 'host'
    fastcgi_split_path_info ^(.+\.php)(.*)$;
    fastcgi_param PATH_INFO $fastcgi_path_info;
    fastcgi_param SCRIPT_FILENAME $document_root/index.php;
}



### END contents!


mkdir -p /etc/thruk

#! set /etc/thruk/htpasswd file to have the following
thrukadmin:$apr1$WxGu2RE4$Dmak9InbEanE9.XP03i5E/
thrukro:$apr1$wTSoBWmj$s2IXkmU/REaT1/4oYly1L1

exit

#! on host
docker commit pnginx phim/pnginx


#############################################

############ pnp4nagios ###########################

#! on host
docker run -it --name pnp4nagios --add-host repo.phim.isyntax.net:172.16.7.201 phim/lrepos /bin/bash


yum install icinga # provides nagios but with fewer dependencies, this will not be running, pnp4nagios has dependency on nagios (just for the sake of it)

## ENABLE THOSE TWO REPOS AGAIN BEFORE NEXT STEP
yum install pnp4nagios
## DISABLE THOSE TWO REPOS AGAIN AFTER PREVIOUS STEP

##### FOR REFERENCE start npcd with "/usr/sbin/npcd -f /etc/pnp4nagios/npcd.cfg"

### useradd nagios -- this should already be here from lrepos parent

exit

#! on host
docker commit pnp4nagios phim/pnp4nagios

#############################################


############ pnp4nagios php ###########################

## this is cheap as it will have pnp4nagios baked in from parent image. but may be easier than setting up all dependencies on php-fpm image from the internet

#! on host
docker run -it --name php --add-host repo.phim.isyntax.net:172.16.7.201 phim/pnp4nagios /bin/bash

## ENABLE THOSE TWO REPOS AGAIN BEFORE NEXT STEP
yum install php-fpm
## DISABLE THOSE TWO REPOS AGAIN AFTER PREVIOUS STEP


#! copy contents from pnp4nagios.config.php from pnp4nagios role to /etc/pnp4nagios/config.php !-> modifying tcp:localhost:50000 to tcp:broker0:50000
# !!!! Ensure not to add extra new line at the end of the file, this would break pnp4nagios
### TOO LONG TO COPY HERE ######


#! edit file /etc/php-fpm.d/www.conf set 'listen = 127.0.0.1:9000' to 'listen = 9000' to just use port (no address == all addresses) and comment out listen.allowed_clients = 127.0.0.1 (using ";")

####### Skipped this
###### NOT needed! ! copy contents from php.ini from php role to /etc/php.ini
############# TOO LONG TO COPY HERE ######

mkdir -p /var/lib/php/session
chown apache:apache /var/lib/php/session

##### FOR REFERENCE start php-fpm with " /usr/sbin/php-fpm"

exit

#! on host
docker commit php phim/php

#############################################


######################################
##################### CONFIG ##############
########################################

############ Shinken Config ##################

#! on host
docker run -it --name shinkencfg -v /etc/philips/shinken --add-host repo.phim.isyntax.net:172.16.7.201 phim/shinkenbase /bin/bash


yum install phim-shinkencfg
# get servicemap.yml from rpm shinken-ws_passive_service_map-receiver to /etc/philips/shinken/

edit /etc/philips/shinken/arbiters/arbiter-master.cfg
replace "address         localhost" with "address         arbiter0"

edit /etc/philips/shinken/brokers/broker-master.cfg
replace "address         localhost" with "address         broker0"

edit  /etc/philips/shinken/pollers/poller-master.cfg
replace "address         localhost" with "address         poller0"

edit  /etc/philips/shinken/reactionners/reactionner-master.cfg
replace "address         localhost" with "address         reactionner0"

edit /etc/philips/shinken/receivers/receiver-master.cfg
replace "address         localhost" with "address         receiver0"

edit /etc/philips/shinken/schedulers/scheduler-master.cfg
replace "address         localhost" with "address         scheduler0"

edit  /etc/philips/shinken/modules/ws_passive_service_map.cfg
uncomment the arbiter_address line and replace the address:
arbiter_address  arbiter0

edit  /etc/philips/shinken/modules/npcdmod.cfg
comment the line with "config_file"
add the lines:
perfdata_spool_dir   /var/spool/pnp4nagios
perfdata_file        /var/log/pnp4nagios/perfdata.dump

exit

####! on host
#### dont do this
####docker commit shinkencfg phim/shinkencfg


###### Network #######################

docker network create neb0

#############################################



######################################
##################### START ##############
########################################
## --net is for docker 1.10 (ATOMIC) after it is called --network



## npcd ##
docker run -it --name npcd0 -v /var/spool/pnp4nagios -v /var/log/pnp4nagios -v /var/lib/pnp4nagios --net=neb0 phim/pnp4nagios /bin/bash
/usr/sbin/npcd -f /etc/pnp4nagios/npcd.cfg


## scheduler ##
docker run -it --name scheduler0 --net=neb0 --volumes-from shinkencfg phim/shinkenscheduler /bin/bash
/usr/sbin/shinken-scheduler -c /etc/philips/shinken/daemons/schedulerd.ini

## reactionner ##
docker run -it --name reactionner0 --net=neb0 --volumes-from shinkencfg phim/shinkenreactionner /bin/bash
/usr/sbin/shinken-reactionner -c /etc/philips/shinken/daemons/reactionnerd.ini

## broker ##
docker run -it --name broker0 --net=neb0 --volumes-from shinkencfg --volumes-from npcd0 phim/shinkenbroker /bin/bash
/usr/sbin/shinken-broker -c /etc/philips/shinken/daemons/brokerd.ini

## receiver ##
docker run -it --name receiver0 --net=neb0 --volumes-from shinkencfg phim/shinkenreceiver /bin/bash
/usr/sbin/shinken-receiver -c /etc/philips/shinken/daemons/receiverd.ini



## poller ##
docker run -it --name poller0 --net=neb0 --volumes-from shinkencfg phim/shinkenpoller /bin/bash
/usr/sbin/shinken-poller -c /etc/philips/shinken/daemons/pollerd.ini


## arbiter ##
docker run -it --name arbiter0 --net=neb0 --volumes-from shinkencfg phim/shinkenarbiter /bin/bash
su - nagios -s /bin/bash -c "/usr/sbin/shinken-arbiter -r -c /etc/philips/shinken/shinken.cfg"


## thruk ##
docker run -it --name thruk0 -v /usr/share/thruk -v /etc/thruk/plugins -v /etc/thruk/themes --net=neb0 phim/thruk /bin/bash
/usr/bin/uwsgi --ini /etc/uwsgi/thruk.ini


## php ##
docker run -it --name php0 --volumes-from  npcd0 -v /usr/share/nagios/html/pnp4nagios --net=neb0 phim/php
/usr/sbin/php-fpm




### nginx #####
docker run -it --name nginx0 --volumes-from thruk0 --volumes-from pnp4nagiosphp0 -p 80:80 --net=neb0 phim/pnginx /bin/bash
nginx -g 'daemon off;'





### DEBUG #####
docker run -it --name bug0 --net=neb0 phim/lrepos /bin/bash


172.16.7.201   repo.phim.isyntax.net
