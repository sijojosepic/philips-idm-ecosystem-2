..  _neb_api_passive:

-----------
Passive API
-----------

To register for passive api service.
------------------------------------

::

"http://{{neb-ip}}/api/v1/register"

Note: this is a one time operation

The request body of the api should contain the hostname and siteid information.::

{"hostname": "host-ip", "siteid": "site-id"}

The response will be a key which should be persisted by the callee for future use.::

{"key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJob3N0bmFtZSI6IjE5Mi4xNjguNTQuMTIiLCJzaXRlaWQiOiJTVFQxNyJ9.o2PSmmEn3ruOFhPCLVqZIiYWJHfLRIZV3Verq8J7vKM"}

Error response from the api on posting an invalid hostname or siteid.::

{"description": "Invalid hostname or siteid.", "ret_code": "invalid_creds"}

To get the token from passive api service.
------------------------------------------

::

"http://{{neb-ip}}/api/v1/authenticate"

The request should contain the earlier obtained key as part of the request body.::

{"key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJob3N0bmFtZSI6IjE5Mi4xNjguNTQuMTIiLCJzaXRlaWQiOiJTVFQxNyJ9.o2PSmmEn3ruOFhPCLVqZIiYWJHfLRIZV3Verq8J7vKM"}

The response will be a token which has got a life of 10 minutes.::

{"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0aW1lc3RhbXAiOjE0ODc4ODQ1ODcuNTExNDMxLCJleHAiOjE0ODc4ODUxODcuNTExNDMxOSwia2V5IjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5Sm9iM04wYm1GdFpTSTZJakU1TWk0eE5qZ3VOVFF1TVRJaUxDSnphWFJsYVdRaU9pSlRWRlF4TnlKOS5vMlBTbW1FbjNydU9GaFBDTFZxWklpWVdKSGZMUklaVjNWZXJxOEo3dktNIn0.dRzP8qQujOiaudtPn2G6X5sPlBdH3q3P7dL2FXp8vTU"}

Error reponse from the api on posting an invalid key.::

{"description": "Invalid key.", "ret_code": "invalid_key"}

To post metrics data via passive api.
-------------------------------------

The earlier obtained token can be used to post the data.::

"http://{{neb-ip}}/api/v1/metricsdata"

Payload .::

{"key": "your custom data", "timestamp" : "ISO 8601 time stamp"}

+-------------------------+-----------------------------------------+
| Header                  | value                                   |
+=========================+=========================================+
| Content-Type            | application/json                        |
+-------------------------+-----------------------------------------+
| Authorization           | Bearer {{token-obtained}}               |
+-------------------------+-----------------------------------------+

Error response from the api on posting an invalid token.::

{"description": "Invalid or expired token.","ret_code": "invalid_header"}
