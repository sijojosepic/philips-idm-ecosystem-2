..  _generic_discovery:

---------------------
Generic Discovery API
---------------------

Endpoints are located in the lhost.yml file for a particular site. The ``address`` and ``scanner`` keys are required for
all endpoints. Other keys may be required depending on the scanner and type of system. Most systems will require
the ``username`` and ``password`` for access.
This API will be used by scanners to get all nodes exposed by ``discovery_url`` present in lhost.yml.

For Example :::

  endpoints:
  - address: 162.65.66.78
    scanner: RWS
    username: administrator
    password: st3nt0r
    hmac_enabled: <true/false> # default 'true'
    discovery_url: http://rws/discovery

* Address - This is the IP or Hostname that stores configuration of the product to scan.
* Scanner - What type of scanner to use on this. For eg . I4GD, HSOPGD etc.
* Username - Username of the product scanner to retrieve configuration
* Password - Password of the username on the product scanner system.
* Discovery_url - The URL implemented by the endpoint which has all the host information
* hmac_enabled - By default Generic Discovery is HMAC authorized, set to 'false' for disabling it

With generic discovery it is easy to integrate any product with IDM. IDM dynamically creates the configuration files for monitoring based on the information provided by any product generic discovery API response.
Generic discovery API response template:

Json :::

  {
  "productid": "productid1",
  "productname": "Product Name"
  "productversion": "2.2.0.1256",
  "environmentType": "Production",
  "hostinfo": [
    {
      "address": "0.0.0.1",
      "name": "a.b.c.d",
      "role": "role1",
      "applications": [
        {
          "name": "service1",
          "version": "2.2.0.1256",
          "applicationType": "WebAPI",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "/path1/path2/Monitor",
              "port": "443",
              "protocol": "https",
              "checkType": "Ping",
              "authType": {
                "auth": "None"
              },
              "Sample": {
                "payload": null,
                "response": "200"
              }
            }
          }
        },
       {
          "name": "service2",
          "version": "2.2.0.1256",
          "applicationType": "WebAPI",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "/path1/path2/Monitor",
              "port": "443",
              "protocol": "https",
              "checkType": "Health",
              "authType": {
                "auth": "None"
              },
              "Sample": {
                "payload": null,
                "response": "200"
              }
            }
          }
  },
  {
          "name": "service3",
          "version": "2.2.0.1256",
          "applicationType": "Windows-Service",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "I4LoggingService",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }
            }
          }
        },
       {
          "name": "service4",
          "version": "2.2.0.1256",
          "applicationType": "Windows-Process",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "I4Logging.exe",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }
            }
          }
        },
        {
          "name": "service5",
          "version": "2.2.0.1256",
          "applicationType": "Passive-Check",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }
            }
          }
        },
        {
          "name": "service6",
          "version": "2.2.0.1256",
          "applicationType": "Windows-Firewall",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }
            }
          }
        }
  ]
  }]
 }


In the above JSON
productid, productname, productversion indicates respective product parameters and environmentType indicates whether the environment is Production or Test.
environmentType can only be one of these - 'Production', 'Test', 'BCS'. If any other value is provided, the discovery shall fail and environmentType is not mandatory field.

• Hostinfo – information about the all the hosts.

       o  “address": Host address,

       o  "name": <hostname/FQDN>,

       o  "role": <role of the host>

Here the combination of productid and role will be mapped in the services.yml file against the host group.
Applications
Application inside the host object are the applications to be monitored.
As of now four type of monitoring capabilities are there:

a.  Windows process by exe name

b.  Windows process by service name

c.  HTTP ping

d.  HTTP health

e.  Passive check

f.  Windows firewall state

Important fields in the Application objects

• Name – <name> # used for service description creation
• Version - <version of the application>
• applicationType - <type> #Distinguish what type of monitoring eg. Windows-process, webAPI etc
• enableMonitoring - <true/false>
• applicationMonitoring: Object contains Monitoring related.which are.
      a.  runbookMessages: Instruction for the SCS engineer to repair
      b.  monitoringDetails: Has Resource of monitoring and related details according to applicaton type. For eg. windows-service application type resource would be the name of service.

With earlier discovery for any product IDM creates only host configuration file by reading the discovery facts. Now with generic discovery IDM creates configuration files for services as well.
The following services will be created dynamically.

a.  Windows Process Monitoring

b.  HTTP Based Monitoring

c.  Passive check

d.  Windows firewall state

**Window Process Monitoring:**

 Windows processes can be monitored Two ways

 a.  Using exe names:::

          "applicationType": "Windows-Process",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "abc.exe",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
            }

b.  Using service name:::

          "applicationType": "Windows-Service",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "abcService",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }


**HTTP Monitoring:**

a.  PING:::

            "applicationType": "WebAPI",
            "enableMonitoring": "true",
            "applicationMonitoring": {
              "monitoringType": "shinken",
              "runbookMessages": {
                  "warning": "action warning message for scs",
              "critical": "action critical message for scs"
                },
              "monitoringDetails": {
                  "resource": "/IdmServices/MissionBriefing/Monitor",
                  "port": "443",
                  "protocol": "https",
                  "checkType": "Ping",
                  "authType": {
                    "auth": "None"
                  },
                  "Sample": {
                    "payload": null,
                    "response": "200"
                  }


b.  Health:::

            "applicationType": "WebAPI",
            "enableMonitoring": "true",
            "applicationMonitoring": {
              "monitoringType": "shinken",
              "runbookMessages": {
                  "warning": "action warning message for scs",
                  "critical": "action critical message for scs"
                },
              "monitoringDetails": {
                  "resource": "/IdmServices/MissionBriefing/Monitor",
                  "port": "443",
                  "protocol": "https",
                  "checkType": "Health",
                  "authType": {
                    "auth": "None"
                  },
                  "Sample": {
                    "payload": null,
                    "response": "200"
                  }

**Passive check:**

 As part of generic discovery, passive service monitoring is supported via passive check. And following is the json structure::

            "applicationType": "Passive-Check",
            "enableMonitoring": "true",
            "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }
            }
          }

**Windows firewall state:**

 Windows firewall state can be monitored through generic discovery. The json structure for firewall state monitoring as follows::

          "applicationType": "Windows-Firewall",
          "enableMonitoring": "true",
          "applicationMonitoring": {
            "monitoringType": "shinken",
            "runbookMessages": {
              "warning": "action warning message for scs",
              "critical": "action critical message for scs"
            },
            "monitoringDetails": {
              "resource": "",
              "port": null,
              "protocol": null,
              "checkType": null,
              "authType": null,
              "Sample": {
                "payload": null,
                "response": null
              }
            }
          }


**Services.yml:**

This is the configuration file which has mapping with hostgroups of <Productid_role>. For each application type this mapping has to be done.

**How to integrate a new product using Generic Discovery?**

a.  Create a Generic discovery scanner.

b.  Create hostgroup and host template

c.  Map the host template in Nagios configuration rules file.

d.  Map the <productid_role>  to hostgroup in services.yml.


**Automatic Service Configuration**

Generic discovery API will have applications field with many number of application either windows or webapi ( which can contain uri path and port). As part of this discovery pipeline the shinken services will be added as part of SVN in siteid/services folder. Below is one sample for windows and webapi shinken service configuration

for example:::

    define service {
       use                            standard-service
       service_description            Product__productid1__WebAPI__service1__status
       check_command                  check_ping_status!443!/IdmServices/LocationTopologyProxy/Monitor!https!False
       hostgroup_name                 i4-rv-servers
     }

    define service {
      use                            wmi-dependant-service
      service_description            Product__productid1__Windows-Service__service2__status
      check_command                  check_win_service!^"GfnApplicationService"$!_NumGood=1:
      hostgroup_name                 i4-rv-servers
     }


**service_description**

*Product__productid1__WebAPI__service1__status*

Is created using Productid(productid1), applicationType(WebAPI), service1(application name).


**To Do /Enhancement**

a. Validation of the JSON: Verification and validation of JSON that we got from Consumer with the IDM GD template. To avoid manually validation of JSON will have validation script for Generic Discovery API response.

b. Add credential in host shinken configuration.