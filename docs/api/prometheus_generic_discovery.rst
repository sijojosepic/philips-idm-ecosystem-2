..  _prometheus_generic_discovery:

--------------------------------
Prometheus Generic Discovery API
--------------------------------

Endpoints are located in lhost.yml same as Generic discovery api. Prometheus Generic discovery will be having a new field as prometheusAgents which contains multiple objects with name of agent or exporter and uri proxy of agent. URI proxy is when exporter runs into a different port and redirect that port to https 443 port. 

JSON :::

 {
	"version": "1.0",
	"productid": "FCS",
	"productname": "Forcare Suite",
	"productversion": "2019-4",
	"environmentType": "production",
	"hostinfo": [
		{
			"address": "192.168.59.64",
			"name": "", 
			"role": "Client Store",
			"osType": "Linux",
			"applications": [],
			"prometheusAgents":[
			{
				"name": "node_exporter",
				"uri": "/node/metrics/"
			},
			{
				"name": "process_exporter",
				"uri": "/process/metrics/"
			}]
		}
	]
 }


Prometheus.yml:::

	alerting:
	  alertmanagers:
	  - static_configs:
	    - targets: ['127.0.0.1:9093']
		global: {evaluation_interval: 15s, scrape_interval: 15s}
		rule_files: [/usr/lib64/prometheus/prometheussrc/prometheusrules/node_exporter.yml,
		  /usr/lib64/prometheus/prometheussrc/prometheusrules/FCS_process_exporter.yml]
		scrape_configs:
		- job_name: prometheus
		  static_configs:
		  - targets: ['localhost:9090']
		- job_name: node_exporter
		  metrics_path: /node/metrics/
		  scheme: https
		  static_configs:
		  - targets: [192.168.59.64]
		  tls_config: {insecure_skip_verify: true}
		- job_name: process_exporter
		  metrics_path: /process/metrics/
		  scheme: https
		  static_configs:
		  - targets: [192.168.59.64]
		  tls_config: {insecure_skip_verify: true}

Here metrics_path is agent uri. which will redirect host:port/metrics path to host:443/node/metrics.

After successful ansible pull the updated prometheus.yml, will reflect in neb and host will be monitored using prometheus.