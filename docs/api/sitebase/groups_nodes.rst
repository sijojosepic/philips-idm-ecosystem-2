=================
Groups REST API
=================
Provides means to query IDM for Groups Data.

Distribution
-------------

Get Groups
-----------
To get all the groups associated with a site id issue GET request to:

`/group_nodes/groups/<site_id>`

site_id is a mandatory field. This will return list of all groups information in following structure:

{
    "result": [
        {
            "created_on": "2019/01/31  12:59:17",

            "triplet": [],

            "siteid": "ALR00",

            "name": "G100",

            "created_by": "raman.pandey@philips.com"

        }

    ]

}

Add To Group
--------------

To add nodes in a group issue POST request to:

`/group_nodes/groups/addgroup`

All fields are mandatory and request payload structure will be:

{
	"siteid":"ALR00",

	"group_name":"group100",

	"triplet":["192.168.100"],

	"created_by":"raman.pandey@philips.com"

}

This API will create/update the group of nodes of which triplet provided for respective site id.

if any field is not provided then response code 203 will be returned.

If any user try to modify the triplet(s) in any group that is created by another user then reseponse code UNAUTHORIZED_USER will be returned.

response structure will be:

{
    "result": 200

}

Update Group
------------

To update a group issue POST request to:

`/groups_nodes/update/<site_id>`

site_id and group_name are mandatory fields.

request payload will be:

{
	"siteid":"ALR00",

	"old_group_name":"group100",

	"new_group_name":"group100"

	"created_by":"raman.pandey@philips.com"

}

response structure will be:

{
    "result": 200

}

Delete Group
------------

To delete a group issue DELETE request to:

`/groups_nodes/delete/<site_id>/<group_name>`

site_id and group_name are mandatory fields. This API will delete the group only if it has empty triplets.

response structure will be:

{
    "result": 200

}
