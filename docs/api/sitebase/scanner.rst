================
Scanner REST API
================
Provides means to query IDM for Scanners Data.


Query
-----
The way to query is by issuing a GET request to:

`/scanner/details`

An example for the result may be::

    {"result": [
        {
            "scanner" : "ISP",
            "credentials" : [ 
                {
                    "username" : "USER5",
                    "password" : "USER6"
                }, 
                {
                    "username" : "USER143",
                    "password" : "USER144"
                }, 
                {
                    "username" : "USER56",
                    "password" : "USER57"
                }
            ]
        },
        {
            "scanner" : "I4",
            "credentials" : [ 
                {
                    "username" : "USER33",
                    "password" : "USER34"
                }
            ]
        }
    ]}