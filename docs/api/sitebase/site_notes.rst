===================
Site Notes REST API
===================
Provides means to query IDM for Site Notes Data.


Workflow Instructions
---------------------
* User can add Note in Notes section for any Site ID.
* The Note added can be up to 256 characters.
* User can add up to 20 notes.
* Each note shall have details of user who created the note, timestamp of creation and Delete icon.
* The user has to delete exisiting notes to add a new note if the maximum limit of 20 is reached.
* User can delete the existing notes and add new notes.
* Updating an existing note is not allowed.

Query
-----
The way to query is by issuing a GET request to:

/notes/<siteid>

Filters may be passed via query string (user).
For example querying `/notes/ABC?user=user1` may return::

    {"result": [
        {
            "elementid": "ABC123",
            "message": "Notes",
            "siteid": "ABC",
            "created_on": "2018/08/03  10:41:58",
            "created_by": "user1"

        }
    ]}

Saving Site Notes
-----------------
To save a note a post request with JSON information to be used for saving must be sent.

There fields are: siteid, message, created_by

For example to save the site assignment rule for siteid "ABC", issuing a POST request with JSON to  the
endpoint `/notes/` will result in a new rule: ::

        {
            "siteid": "ABC",
            "message": "Notes",
            "created_by": "user1",
        }

Delete Site Notes
-----------------
To delete a site data a DELETE request with the siteid and type to be deleted must be sent to the endpoint:

`/notes/?ids=id1`

For example to delete a site notes entry for siteid='ABC' and elementid='ABC-1' is by issuing a DELETE request::

    /notes/?ids=ABC-1

will result in deleted site notes