==================
Directory REST API
==================
Provides means to query IDM for Directory Data.

Distribution
------------
The way to query is by issuing a GET request to:

/directory/<siteid>

Filters may be passed via query string.

Example querying to get directory data `/directory/test112`
may return::

    {"result": [
        {
            "poc_number": "+91 808179111",
            "poc_name": "ABC1",
            "site_id": "test112",
            "role": "Manager_1",
            "address": "",
            "_id": "test112-1546506783"
        },
        {
            "poc_number": "88888",
            "role": "Manager_1",
            "_id": "test112-1546507689",
            "site_id": "test112",
            "address": "Main"
        },
        ...
    ]}

Insert Directory
----------------
To insert directory into a post request with JSON information to be used for the data must be sent in the body.

For example if you want to insert directory under a site, a call to endpoint `/directory/` with body::

    {
        "site_id" : "test112",
        "poc_number" : "8087123567",
        "role" : "Managr_1",
        "poc_name" : "abc",
        "address" : "xyz"
    }

Update Directory
----------------
To update existing directory into a post request with JSON information to be used for the update must be sent in the body.

For example if you want to update directory under a site, a call to endpoint `/directory/update` with body::

   {
        "site_id" : "test12",
        "poc_number" : "+91 808179111",
        "role" : "Manager_1",
        "poc_name" : "ABC1",
        "address" : " ",
        "_id":"test112-1546506783"
    }

Delete Directory
----------------
To delete existing directory into a post request with JSON information to be used for the update must be sent in the body.

For example if you want to delete directory under a site, a call to endpoint `/directory/directory/?ids=test112-1547620560`

For example to delete a directory entry for siteid='test112' and elementid='1547620560' is by issuing a DELETE request::

    /directory/?ids=test112-1547620560

will result in deleted directory
