=====================
Technical Id REST API
=====================
Provides means to query IDM for Technical Id.

Get All Technical Ids
---------------------
Use this API get all the technical ids for all products used in IDM.

The way to query is by issuing a GET request to:

`/technical_id/all_ids`

For example the query '/technical_id/all_ids' may return::

  {
    "SHDB": 203568,
    "HL7": 197196,
    "ESXi": 203568,
    "ISP": 197196,
    "MGNode": 197196
    ...
  }

Get Technical Id
----------------
Use this API get the corresponding technical id for a given product id

The way to query is by issuing a GET request to:

`/technical_id/product/<product_id>`

For example a sample query '/technical_id/product/ISPACS' may return::

  {
    "technical_id": 197196
  }

