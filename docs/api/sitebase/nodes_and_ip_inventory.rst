===============================
Nodes and IP Inventory REST API
===============================
Provides means to query IDM for Nodes and IP Inventory Data.

Distribution
------------

Get Available IPs
-----------------
To get all available IPs for a site id issue GET request to:

`/nodesipinventory/ips/available/<site_id>`

site_id is a mandatory field. This will return list of all available IPs for a site id in following structure:

{
    "result": [
        {

            "status": "available",

            "modified_by": "raman",

            "ip": "192.168.100.53",

            "group_name": "ALL",

            "siteid": "ALR00",

            "message": "test"

        }

    ]

}

Get Reserved IPs
----------------

To get all reserved IPs for a site id issue GET request to:

`/nodesipinventory/ips/reserved/<siteid>`

site_id is a mandatory field. This will return list of all reserved IPs for a site id in following structure:

{
    "result": [
        {

            "status": "reserved",

            "modified_by": "raman",

            "ip": "192.168.100.57",

            "group_name": "ALL",

            "siteid": "ALR00",

            "message": "test"

        }

    ]

}

Get Consumed IPs
----------------

To get all consumed IPs for a site id issue GET request to:

`/nodesipinventory/ips/consumed/<siteid>`

site_id is a mandatory field. This will return list of all consumed IPs for a site id in following structure:

{
    "result": [
        {
            "ip": "192.168.100.52",

            "hostname": "192.168.100.52",

            "group_name": "ALL"

        }

    ]

}

Get Nodes
---------

To get all nodes information for a site id issue GET request to:

`/nodesipinventory/nodes/<site_id>`

site_id is a mandatory field. This will return list of all nodes including vcenter data(if any) for a site id in following structure:

{
    "result": [
        {
            "vendor": "IBM",

            "nodename": "192.168.100.52",

            "type": "ESXi",

            "version": "5.0.0",

            "virtualnodes": [],

            "location": "ALL",

            "model": "System x3630 M3 -[7377C2G]-",

            "ipaddress": "192.168.100.52",

            "productid": "NA"

        }

    ]

}

Get IP Tree
-----------

To get Tree View Structure of IPs for a site id issue GET request to:

`/nodesipinventory/facts/consumed/<siteid>`

site_id is a mandatory field. This will return Tree Structre of consumed IPs with count of respective child ips for a site id in following structure:

{
    "result": [
        {
            "count": 1,

            "ip": "172.X.X.X",

            "children": [

                {
                    "count": 1,

                    "ip": "172.16.X.X",

                    "children": [

                        {
                            "count": 2,

                            "ip": "172.16.40.X",

                            "children": [

                                "172.16.40.200",

                                "172.16.40.201"

                            ]

                        }

                    ]

                }

            ]

        }

    ]

}

Set IP status
-------------

To set status of a IP issue PUT request to:

`/nodesipinventory/ips/setstatus`

request payload structure will be:

{
	"ip":"192.168.100.55",

	"status":"available",

	"message":"test",

	"modified_by":"test",

	"siteid":"ALR00"

}

This API will set the status available/reserve of a IP for respective site id.

response structure will be:

{
    "result": 200

}
