===========================
Solution Landscape REST API
===========================
Provides means to query IDM for Solution Landscape Data.

Distribution
-------------

Get Solution Landscape File
---------------------------

To get all the groups associated with a site id issue GET request to:

`/solution_landscape/<site_id>`

site_id is a mandatory field. This will return list of solution landscape information in following structure:

{
    "result": [
        {
            "siteid": "ALR00",

            "file_url": "https://cf-s3-c243ff8b-5648-456e-9840-a655aeeea0c8.s3.amazonaws.com/ALR00/dummy.pdf?Signature=ZDgbff8hUHbIT2vtGHVb7jIsgpg%3D&Expires=1549016715&AWSAccessKeyId=AKIAIXSQMXJFHJKXWR7Q",

            "file": "dummy.pdf",

            "modified_by": "raman"

        }

    ]

}

Save Solution Landscape File
----------------------------

To add nodes in a group issue POST request to:

`/solution_landscape/`

All fields are mandatory and request payload structure will be in form-data:

{
    siteid : 'ALR00',

    file : 'dummy.pdf',

    modified_by : 'raman'

}

if any field is not provided then response code 203 will be returned.

response structure will be:

{
    "result": 200

}

Update Solution Landscape File
------------------------------

To add nodes in a group issue PUT request to:

`/solution_landscape/`

All fields are mandatory and request payload structure will be in form-data:

{
    siteid : 'ALR00',

    file : 'dummy.pdf',

    modified_by : 'raman'

}

if any field is not provided then response code 203 will be returned.

response structure will be:

{
    "result": 200

}
