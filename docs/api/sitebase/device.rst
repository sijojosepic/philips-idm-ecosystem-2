===============
Device REST API
===============
Provides means to query IDM for Device Data.

Distribution
------------
The way to query is by issuing a GET request to:

/device/

Example querying to get directory data `/directory/`
may return::

    {
        "results": [
            {
                "_id": "dell_113-1547634153",
                "type": "EXCIa",
                "name": "dell_113"
            },
            {
                "_id": "hp-1547717224",
                "type": "EXI",
                "name": "hp"
            }
        ]
    }

Insert Device
----------------
To insert device into a post request with JSON information to be used for the data must be sent in the body.

For example if you want to insert device under a site, a call to endpoint `/device/` with body::

    {
        "name":"hp",
        "type":"EXI"
    }

Update Device
----------------
To update existing device into a post request with JSON information to be used for the update must be sent in the body.

For example if you want to update device under a site, a call to endpoint `/device/update` with body::

   {
        "name":"dell",
        "type":"EXI"
    }

Delete Device
----------------
To delete existing device into a post request with JSON information to be used for the update must be sent in the body.

For example if you want to delete device under a site, a call to endpoint `/directory/directory/?ids=hp-154764274117`

For example to delete a device entry for siteid='hp' and elementid='154764274117' is by issuing a DELETE request::

    /directory/?ids=hp-154764274117

will result in deleted devices
