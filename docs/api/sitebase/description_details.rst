============================
Description Details REST API
============================
Provides means to query IDM for Description details Data.

Distribution
------------
The way to query is by issuing a GET request to:

/description_details/<siteid>

Example querying to get description data `description_details/test6`
may return::

    {
        "result": [
            {
                "CSM": "Dont Mary John",
                "Description": "Hospital Description",
                "Site_id": "test6",
                "Number": "8087111111",
                "Market_Leader": "John Hoe",
                "ROle": "Maneger",
                "Address": "Main Site Address",
                "POC": "POC of Main Site"
            }
        ]
    }

Insert Description
------------------
To insert description into a post request with JSON information to be used for the data must be sent in the body.

For example if you want to insert description under a site, a call to endpoint `/description_details/` with body::

    {
        "siteid":"ALR00",
        "csm":"Mary Johns",
        "market_leader":"John Doe",
        "number":"+91 8087123456",
        "description":"LoremsZupdate",
        "poc":"xyz",
        "address":"E1, Ground Floor, Beech Building,d\nBelow Escape Food Court Embassy,\nManayata Tech Park, Thanisandra,\nBengaluru, Karnataka 560045",
        "role":"Support Engineer"
    }

Update Description
------------------
To update existing description into a post request with JSON information to be used for the update must be sent in the body.

For example if you want to update description under a site, a call to endpoint `description_details/update` with body::

   {
        "siteid":"ALR00",
        "csm":"Mary Johns",
        "market_leader":"John Doe",
        "number":"+91 8087123456",
        "description":"LoremsZupdate",
        "poc":"abc",
        "address":"E1, Ground Floor, Beech Building,d\nBelow Escape Food Court Embassy,\nManayata Tech Park, Thanisandra,\nBengaluru, Karnataka 560045",
        "role":"Support Engineer"
    }

