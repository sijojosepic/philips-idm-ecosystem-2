==============
Audit REST API
==============
Provides means to query IDM for Audit Data.


Distribution
------------
The way to query is by issuing a GET request to:

/audit/distribution

Filters may be passed via query string.

Example querying to get distribution data `/audit/distribution?site_id=SITEID&subscription=PKG1&start_date=YYYY-mm-dd&end_date=YYYY-mm-DD`
may return::

    {"result": [
        {
            "subscription": "anywhere-1.2",
            "status": "Submitted",
            "timestamp": "2016-03-21T11:14:37.412Z",
            "user": "IDM Portal",
            "siteid": "AMC01"
        },
        ...
    ]}


Deployment
----------
The way to query is by issuing a GET request to:

/audit/deployment

Filters may be passed via query string.

Example querying to get deployment data `/audit/deployment?site_id=SITEID&package=PKG1&start_date=YYYY-mm-dd&end_date=YYYY-mm-DD`
may return::

    {"result": [
        {
            "package" : "anywhere",
            "status" : "Submitted",
            "timestamp": "2016-03-21T11:14:37.412Z",
            "siteid" : "AMC01",
            "hosts" : ["ahr00-am002"],
            "user" : "IDM Portal",
            "version" : "v1"
        },
        ...
    ]}


Insert Audit Log
----------------
To insert log into a post request with JSON information to be used for the update must be sent in the body.

For example once Software distribution request is issued, a call to endpoint `/in/audit/distribution` with body::

    {
        "subscription": "anywhere-1.2",
        "status": "Submitted",
        "user": "IDM Portal",
        "siteid": "BENHC"
    }

For example once Software deployment request is issued, a call to endpoint `/in/audit/deployment` with body::

    {
         "package" : "anywhere",
         "status" : "Submitted",
         "siteid" : "AMC01",
         "hosts" : ["ahr00-am002"],
         "user" : "IDM Portal",
         "version" : "v1"
    }

For example user has logged into new portal a call to endpoint `/in/audit/login` with body::

    {
         "email" : "test@philips.com",
         "headers":{'agent': 'Chrome', 'version': 'X'}
    }

For example once case is created from portal a call to endpoint `/in/audit/case` with body::

    {
         'case_number': '732123',
         'hostname': 'eid.test.com',
         'service': 'Service_with_hostame_and_siteid',
         'siteid': 'siteid',
         'uname': 'test@philips.com'
    }

 For example once new acknowledge tag is created from portal a call to endpoint `/in/audit/ack` with body::

    {
         'tag': 'ack_tag',
         'hostname': 'eid.test.com',
         'service': 'Service_with_hostame_and_siteid',
         'siteid': 'siteid',
         'uname': 'test@philips.com'
    }