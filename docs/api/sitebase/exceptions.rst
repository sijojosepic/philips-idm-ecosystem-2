===================
Exceptions REST API
===================
Provides means to query IDM for exceptions found.

Exceptions in this context are pieces information found to be missing from collections such as service namespace related
information and site related information.

Query
-----
The way to query is by issuing a GET request to:

/exceptions/<exception_type>

For example querying `/exceptions/site` may return::

    {"exceptions": ["ABC01", "XYZ00"]}


Clearing Exceptions
-------------------
Exceptions are cleared when descriptors are inserted/updated for the items causing the exception.

To update descriptors a post request with JSON information to be used for the update.

For site exceptions the `siteid` field is required, other fields passed will overwrite any existing data. If there is an
existing value for the field and it is not passed, the existing value will remain unchanged.

For example for siteid "ABC01" issuing a POST request with the following JSON to endpoint `/in/descriptor/site` will
result in updated descriptors and existing exception related to this site to be cleared: ::

    {
        "siteid": "ABC01",
        "hbdbsiteid": "ABC01",
        "name": "Allison Bay Clinic",
        "pacs_version": "4,4,5 turbo",
        "production": true,
        "neb_address": "10.4.5.243",
        "country": {
            "market": "APAC",
            "sap_code": "AU",
            "country": "Australia"
        }
    }
