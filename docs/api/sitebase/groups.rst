==============
Group REST API
==============
Provides means to query IDM for Groups Created in IDM.


Query
-----
The way to query is by issuing a GET request to:

`/groups/`

For example querying `/groups` may return::
{
    "result": [
        {
            "managingOrganization": "1b9dfb4b-c5cf-4c39-9887-ee866f70dfec",
            "group_id": "f119f045-d34b-431a-9717-236e1c7bf8b7",
            "name": "G4",
            "timestamp": "2018/10/09  01:41:18",
            "description": "IDM Portal Group"
        }
    ]
}