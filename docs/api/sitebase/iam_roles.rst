=============
ROLE REST API
=============
Provides means to query IDM for roles Created in IDM.


Query
-----
The way to query is by issuing a GET request to:

`/iam_roles/`

For example querying `/iam_roles` may return::
{
    "result": [
        {
            "role_id" : "2e075c25-9b2e-44a8-93f6-3ef0318c80f6",
            "managingOrganization" : "1b9dfb4b-c5cf-4c39-9887-ee866f70dfec",
            "name" : "SWD",
            "timestamp" : ISODate("2018-12-04T18:55:13.319Z"),
            "description" : "IDM Roles",
            "views" : [ 
                "SOFTWARE_DISTRIBUTION", 
                "SOFTWARE_DEPLOYMENT"
            ]
        }
    ]
}