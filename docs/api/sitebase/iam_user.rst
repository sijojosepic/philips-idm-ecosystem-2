=================
IAM USER REST API
=================
Provides means to query IDM for User details.


All users
---------
To get all users present in iam_user collection is by issuing a GET request to iam_admin_user controller:

`/iam_admin_user/`

An example for the result may be::

    {"result": [
        {
            "userUUID": "d932f582",
            "givenName": "Naveen",
            "groupID": [
                "23159b61-4f2b"
            ],
            "familyName": "Penumala",
            "loginId": "naveen.penumala@philips.com"
        },
        {
            "userUUID": "39ad2fc2-1aca",
            "givenName": "Karthik",
            "groupID": [
                "e2d3f12c-689"
            ],
            "familyName": "mohan",
            "loginId": "karthik.mohan@philips.com"
        }
    ]}


Specific user
-------------
To get a specific users present in iam_user collection is by invoking get_user_details method of IAMUserModel:

It expects loginId as parameter:

Example of the return result maybe::

    {
        "givenName": "Karthik",
        "groupID": [
            "e2d3f12c-689"
        ],
        "familyName": "mohan",
    }


Add new user
------------
To add new user to iam_user collection is by invoking add_new_user method of IAMUserModel:

It excepts 2 parameters:

1. userUUID for the document match:

2. fields that needs to updated:


Update user group details
-------------------------
To user group information present in iam_user collection is by issuing a POST request to iam_admin_user controller:

`/iam_admin_user/update_group`

It expects request_object containg userUUID and groups array that needs to be updated:

If userUUID is matched it returns ::

    {'valid': False}

If If userUUID is not matched it returns ::

    {'valid': True}

