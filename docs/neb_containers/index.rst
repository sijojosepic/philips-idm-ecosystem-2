Containerized Neb
*****************

===============
Steps to deploy
===============

	**Prerequisites:**
		1.	Docker Engine and docker-compose should be available
		2.	Registry should be accessbile by configuring on /etc/hosts
	**To Spin up system:**
		1.	The compose file is available at /var/philips/views/docker/compose. Alternatively, the compose file if available at the root directory of the next-gen repo
		2.	Run the command docker-compose up –d with current path at the compose file to spin up NEB
	**Endpoints:**
		Registry:
			Hostname: idmregistry.phim.isyntax.net
			IP: 161.85.111.189


=======================================================
Steps to Deploy Containerized NEB v1.0 – Alpha Release:
=======================================================
		1.	Use the template Centos7-NEB-Docker to spin up a VM
		2.	Create a site in Vigilant using the Newsitesdock.sh script. 
		3.	Run Initialization.py script. Remember to not use the Secure Flag. Vigilant needs to be set to 157.
		4.	Run ansible on host (target VM). 
	Things to check for verification purposes:
		1.	Ensure that certificates are in place in the /etc/docker/certs.d path for the IDM Docker registry
		2.	Verify that the CA certificate is in place in /etc/pki/ca-trust/source/anchors as {Registry-FQDN}.crt
		3.	Docker daemon is running: Run cmd – service docker status
	Expected Behavior:
		1.	Ansible run will pull the necessary images from the registry and run compose to wire up the containers
		2.	7 images should be pulled from the registry. To check the same use the docker images command.
		3.	To check if container have spun up: docker ps –a
			a.	All of them should be in running status
		4.	Verify container boot up(optional):
			a.	Shinken:
				i.	Get into the container using the following command: docker exec –it nb.shinken /bin/bash
				ii.	Check if ansible run was successful: tail –f /tmp/ansible_pull.log
			b.	Celery:
				i.	Get into the container using the following command: docker exec –it nb.celery /bin/bash
				ii.	Verify that RabbitMQ and Redis are configured correctly as follows in tbconfig and celeryconfig.py:
					1.	'RABBITMQ': {'URL': 'amqp://guest:guest@nb.rabbitmq:5672'}
					2.	'REDIS': 'URL': 'redis://nb.redis:6379'
		5.	Check if Thruk is launching
		6.	Check if Dashboards are loaded on Grafana at {hostIP}:3000
			a.	Credentials are: admin,admin (Always skip from changing password



========================
Docker Registry Creation
========================


Please follow the guidelines as mentioned in the document.

:doc:`doc/docker_registry _setup.pdf`


To access the image catalog at the registry from a remote host, execute the following command:
 
                curl -X GET https://idmregistry.phim.isyntax.net:443/v2/_catalog –k
 
Certain Pre-requisites you should consider:

1.       IDM certificates should be available and configured at the host you’re trying to execute the command from.
2.       Docker level set up of the certificates should be complete
3.       We have authentication facilities in place for the registry but they are currently disabled for Dev use.
 
If you want an overview of the process to setup certificates, refer to the attached document. You will have to execute the latter parts of Step 2 and Step 5. The registry setup process is already automated on the containerized NEB FYI thanks to Naveen.
 
I recommend using one of the pre-configured NEBs to access the Registry catalog. You may use the follow node for the same:

1.       NEB IP: 192.168.59.42
2.       User: root
3.       Password: ph!lt0r

=======
Branch:
=======

RPM's from Neb must be built from C7-Develop branch and then from C7-Release going forward.
This branch has container specific fixes and using other branch is not advised. 

===============
Neb Components:
===============

Currently we have six dockers

1. celery aka **nb.celery**
2. shinken aka **nb.shinken**
3. cadvisor aka **nb.cadvisor**
4. rabbitmq aka **nb.rabbitmq**
5. prometheus aka **nb.prometheus**
6. redis aka **nb.redis**

**Functionalties:**

    *Cadvisor*:

cAdvisor (Container Advisor) provides container users an understanding of the resource usage and performance characteristics of their running containers. It is a running daemon that collects, aggregates, processes, and exports information about running containers.

    *Prometheus*:

It regularly scraps cadvisor to see containers health, rabbitmq health.

    *Grafana*:
It's the UI for Prometheus just like thruk for Shinken.

Important Concepts:
===================

Wiring:
#######


Containers are started by running ansible pull on docker hosts, which will build all images.
Ansible will take responsibility of pulling all certificates, latest packages etc..


Volumes:
########

Dockers are ephemeral,By “ephemeral”, we mean that the container can be stopped and destroyed, then rebuilt and replaced with an absolute minimum set up and configuration.

By default all files created inside a container are stored on a writable container layer. This means that:

The data doesn’t persist when that container no longer exists, and it can be difficult to get the data out of the container if another process needs it.
A container’s writable layer is tightly coupled to the host machine where the container is running. You can’t easily move the data somewhere else.

Docker has two options for containers to store files in the host machine, so that the files are persisted even after the container stops: *volumes*, and *bind mounts*.

**Volumes** are stored in a part of the host filesystem which is managed by Docker (/var/lib/docker/volumes/ on Linux). Non-Docker processes should not modify this part of the filesystem. Volumes are the best way to persist data in Docker.

Volumes are created in docker-compose.yml .Any persistent data should be maintained using volumes .
To see all volumes at any point of time on Neb,

*docker volume ls*

You can further more granularity by running 

*docker volume inspect*


Logging into individual containers:
===================================

*docker exec -it <container_name> /bin/bash*

Minimalistic images will not have bash,in that case just use /bin/sh.
After logging one can verify required functionalities.


Various End Points:
===================

Grafana:
https://<NebIp>/grafana

Prometheus:
https://<NebIp>/prometheus

Cadvisor:
http://<NebIp>:8080/containers
For Cadvisor its on http,cadvisor metrics is availabe at http://<NebIp>:8080/metrics

To be updated..

Work to be done:
================

1) Neb IP is not captured in Thruk.
2) Containers are bit shaking when Neb is spinned newly.Later its stable but its worth time spending.
3) Setting up Main registry in Cloud.
4) Security testing.
5) Redirect error logs from shinken container to stdout.
6) Health check for other containers.
7) Docker Registry namespacing
8) Upgrade docker-compose.yml to version 3 as advised by Naveen.


