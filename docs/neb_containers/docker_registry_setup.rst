**Pre-requisites:**

1. Registry Image
2. Openssl
3. Nginx Image/ Nginx on host (optional)

************************
  Deploying the Registry
************************


**Step 1: Preliminary Setup**

Execute the following in a path according to the requirement at user discretion.


*mkdir –pv {root-directory}/docker-registry/certs*

*mkdir {root-directory}/docker-registry/data*

*cd {root-directory}/docker-registry/*

**Optional: (For registry served throgh NGINX container)**

*mkdir nginx*


**Step 2: Creating the certificates**

Since Docker currently doesn't allow us to use self-signed SSL certificates this is a bit more complicated than usual — we'll also have to set up our system to act as our own certificate signing authority.

Execute the following in the certs directory that was created. 
Generate the key and crt for the CA:

*openssl genrsa -out ca.key 2048*
*openssl req -x509 -new -nodes -key ca.key -days 10000 -out ca.crt*

Generate the server **key**, the **csr** and then the **crt** using the CA creds: 

*openssl genrsa -out domain.key 2048*

*openssl req -new -key domain.key -out docker-registry.csr*

After you type this command, OpenSSL will prompt you to answer a few questions. Write whatever you'd like for the first few, but when OpenSSL prompts you to enter the **"Common Name"** make sure to type in the domain (referred to as Registry Domain going forward) or IP of your server. 

Do not enter a challenge password or optional company name.

Now we sign the certificate using the CA credentials we just created:

*openssl x509 -req -in docker-registry.csr -CA ca.crt -CAkey ca.key - CAcreateserial -out domain.crt -days 10000*


Since the certificates we just generated aren't verified by any known certificate authority, we need to tell any clients that are going to be using this Docker registry that this is a legitimate certificate. The following operations have to be performed on any host that will use the remote registry:

1. Copy the domain.crt file to the target machine at /etc/docker/certs.d/{Registry Domain}:5000/ca.crt

2. Copy the ca.crt(CA crt) file to the target machine to the following path. Make sure the crt is renamed to the Registry Domain:

    /etc/pki/ca-trust/source/anchors/{Registry-Domain}.crt
    
3. Run: update-ca-trust


**Step 3: Setting up the basic registry**

*Approach 1: Using a compose file*

Create the following compose file in the docker-registry directory:

        nginx:                         # 
           image: “nginx”              # 
           restart: “always”           # 
           ports:                      #
              - 443:443                # 
           links:                      # 
                - registry:registry    # 
           volumes:                    # Use to deploy over NGINX
            - ./nginx/:/etc/nginx/conf.d:ro
            - ./certs.:/etc/nginx/conf.d:ro registry:
           image: registry 
           restart: “always”
           ports:
            - 127.0.0.1:5000:5000
            environment:              #Use to deploy over NGINX
                - REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: /data.   #               -                     
                #-REGISTRY_HTTP_TLS_CERTIFICATE= {path_to_cert}/domain.crt #
                #- REGISTRY_HTTP_TLS_KEY= {path_to_cert}/domain.key
               #If the registry is not deployed through NGINX, the above two lines
               will have to be set
            volumes:
            - ./data:/data
            - ./certs.:/etc/certs    

**Approach 2: Simple registry deployment**

Run the below command from docker-registry directory:

docker run -d \ --restart=always \ --name registry \
    -v `pwd`/certs:/certs \
    -v `pwd`/data:/data \
    -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
    -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \ 
    -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
    -p 443:443 \
registry


**Step 4: Setting up NGINX (optional)**

Use the following for registry.conf:

*

upstream docker-registry { 
    server registry:5000;
}

*

server {
    listen 443;
    server_name {Registry-Domain};
# SSL
    ssl on;
    ssl_certificate /etc/nginx/conf.d/domain.crt; 
    ssl_certificate_key /etc/nginx/conf.d/domain.key;
    # disable any limits to avoid HTTP 413 for large image uploads
      client_max_body_size 0;
    # required to avoid HTTP 411: see Issue #1486 (https://github.com/docker/docker/issues/1486)
      chunked_transfer_encoding on;
      location /v2/ {
     # Do not allow connections from docker 1.5 and earlier
     # docker pre-1.6.0 did not properly set the user agent on ping, catch "Go *" user agents
        if ($http_user_agent ~ "^(docker\/1\.(3|4|5(?!\.[0-9]-dev))|Go ).*$" )
         {
            return 404; 
         }
        proxy_pass http://docker-registry ;
        proxy_set_header  Host $http_host;
        proxy_set_header X-Real-IP  $remote_addr;
        proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_read_timeout 900;
    } 

}

**Step 5: Set Registry host in /etc/hosts on the target host/client machine**


Append the following to hosts: 

IP {Registry-Domain}

