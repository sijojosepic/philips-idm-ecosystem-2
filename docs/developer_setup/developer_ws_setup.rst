===========================
Developer Workstation Setup
===========================

Introduction
------------

Known Issues
""""""""""""
Since the target systems are all operating within the linux platform, line endings for files are important to take note
of for committing into the code archive. If the developer IDE is operating on a Windows based platform, the IDE will
normally be defaulted to operate with the system default line endings. In Windows, this is CRLF where linux applications
need a LF end of line.

Setup Steps
-----------

#. Install a latest requested CentOS x86_64 minimal

``\\usdsfosfc1msna1.code1.emi.philips.com\ComputeEnvironment\thirdparty\CentOS\iso\CentOS-6.4-x86_64-minimal.iso``

#. Select all the defaults during the installation

:ref:`centos_os_installation`

#. Drive size should be no larger than 20GB
#. Setup Network and hostname information, files needing adjusting listed below

::

    /etc/sysconfig/network-scripts/ifcfg-eth0
    /etc/hosts -> TO add 167.81.182.65  repo.phim.isyntax.net
    /etc/sysconfig/network

#. Recommend to turn off SELinux and the firewall

::

    chkconfig iptables off
    chkconfig ip6tables off
    sed -i 's/=enforcing/=disabled/g' /etc/selinux/config

#. Reboot - ``shutdown -r now``
#. Once the system is on the network follow these steps, these must be executed in this order to eliminate any
   dependency conflicts

::

    ln -sf /usr/share/zoneinfo/UTC /etc/localtime
    yum --disablerepo=* -y install http://repo.phim.isyntax.net/phirepo/noarch/phirepo-1.1.1734-1.el6.noarch.rpm
    yum --disablerepo=* --enablerepo=phirepo -y install pcentos pepel pmongodb prpmforge
    yum -y install gcc python-devel python-setuptools openssh-server openssh-clients make wget vixie-cron tmux sudo tcpdump telnet traceroute tree lsof rsync patch rpmlint
    yum -y groupinstall localperl
    yum -y install http://repo.phim.isyntax.net/base/x86_64/Packages/mailcap-2.1.31-2.el6.noarch.rpm
    yum -y install perl-XML-SAX
    yum -y install python-dateutil python-argparse perl-Test-Simple perl-ExtUtils-MakeMaker cmake libjpeg-devel libpng-devel libtiff-devel libxml2-devel tcp_wrappers-devel CharLS-devel doxygen perl perl-Crypt-SSLeay perl-ExtUtils-MakeMaker perl-libwww-perl perl-Net-SSLeay perl-SOAP-Lite perl-URI perl-XML-LibXML rpm-build uuid-perl perl-Class-MethodMaker
    yum -y install libxml2-python python-lxml
    yum -y install fakeroot man
    yum -y install python-yaml python-mock python-minimock python-blist python-requests mongoose
    yum --disablerepo=* -y install http://repo.phim.isyntax.net/base/x86_64/Packages/rpmdevtools-7.5-2.el6.noarch.rpm
    /usr/bin/easy_install -i 'http://repo.phim.isyntax.net:8084/simple' pip
    mkdir -pv ~/.pip
    cat > ~/.pip/pip.conf << "EOF"
    [global]
    timeout = 60
    index-url = http://repo.phim.isyntax.net:8084/simple
    EOF
    pip install virtualenv
    useradd -m -g users developer
    passwd developer
        - password
    cat > /etc/sudoers.d/01-developer << "EOF"
    developer ALL=(ALL)   NOPASSWD: ALL
    EOF
    su - developer -c rpmdev-setuptree
    su - developer -c "sed -i 's/%__arch_install_post/#%__arch_install_post/g' /home/developer/.rpmmacros"
    su - developer -c "mkdir -pv ~/IDMEcosystem/test_output"
    su - developer -c "mkdir -pv ~/.pip"
    su - developer -c 'cat > ~/.pip/pip.conf << "EOF"
    [global]
    timeout = 60
    index-url = http://repo.phim.isyntax.net:8084/simple
    EOF'
    su - developer -c "virtualenv --system-site-packages venv"
    su - developer
    source venv/bin/activate
    pip install fabric coverage nose sphinx pysphere
    deactivate

8. Setup pycharm for Deployment and Remote interpreter.

:ref:`ide_pycharm_settings`

::

    cat >> ~/.bashrc << "EOF"
    export P_PROJECT_ROOT=/home/developer
    export P_WORKING_COPY=IDMEcosystem
    export P_THIRD_PARTY_SRC=http://167.81.176.20/thirdparty
    export P_DOC_DIRECTORY=$P_PROJECT_ROOT/$P_WORKING_COPY/docs/
    export P_WEB_DIRECTORY=/var/www/html/docs/
    EOF
    cd IDMEcosystem
    source ~/.bashrc
    source ../venv/bin/activate
    fab build_all:exclude=dmctk,depocopy=False -H localhost -f build_scripts/linux/fabfile.py

The following are the optional ways to execute the builds

Execution of all packages, showing multiple packages excluded

.. note:: Local developer builds should always have ``depocopy=False``

::

    fab build_all:exclude='dmctk|phimutils',depocopy=False -H localhost -f build_scripts/linux/fabfile.py

Execution of a single package

.. note:: The ``depocopy`` parameter needs to be removed or the initial execution will fail

::

    fab build_only:include='phimutils' -H localhost -f build_scripts/linux/fabfile.py

Output RPMs are located in ``../BuildDepot/el6-x86_64-rpmbuild/`` if execution of the ``build_all`` task.

For ``build_only`` task, the files will be in ``~/rpmbuild/RPMS/``

.. note:: Normal development is not to be within the python virtual environment, the global python implementation will
          contain all the necessary libraries for development. The virtual environment is only meant for building the
          bundle output

Documentation builds
--------------------

The following will build the documentation and copy to the web location

::

    make -C $P_DOC_DIRECTORY html && sudo mkdir -pv $P_WEB_DIRECTORY && sudo /usr/bin/rsync -avz --delete ${P_DOC_DIRECTORY}_build/html/ $P_WEB_DIRECTORY

To start the webserver to check the documentation

::

    mongoose -r /var/www/html/docs/ -p 8181

Offline third party packages
----------------------------

