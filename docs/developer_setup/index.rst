Developer Setup
===============

Contents:

.. toctree::
   :maxdepth: 2

   developer_ws_setup
   ide_pycharm_settings
   dev_ci_setup
   vwmare_workstation_dev_wks
