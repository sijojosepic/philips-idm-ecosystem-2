=======
Actions
=======

High Level Overview
-------------------

Actions are items that may be performed by the PCM package. The goal is not to replace existing packaging tools such as
MSI, Deploy.ps1, or .zip. The actions defined here should be the minimal steps to invoke and configure an application

Actions are structured in XML with the following nodes

.. code-block:: xml

    <Action EffectiveUserName="" EffectiveUserPassword="" RetryCount="">
      <ActionType></ActionType>
      <ActionFile></ActionFile>
      <ActionArguments></ActionArguments>
      <ActionTimeout></ActionTimeout>
      <ActionReturnCode></ActionReturnCode>
      <ActionRequiresReboot></ActionRequiresReboot>
    </Action>


- The attributes ``EffectiveUserName`` and ``EffectiveUserPassword`` are optional and allow the action to execute under that credentials context. This is mostly used for actions requiring domain administrative impersonation.
 
- ``ActionType`` Specifies the action type to be performed 
- ``ActionFile`` Target of the action, relative to the expanded PCM package directory. 
- ``ActionArguments`` Any arguments needed for the script. Environmental and Databag variables are expanded before
  execution
- ``ActionTimeout`` Timeout in seconds for how long to allow the action to process. Timer interrupt is an automatical
  fail condition.
- ``ActionReturnCode`` The expected return code of the process to be considered a success. Usually 0 however may be set
  to any numerical status. 
- ``ActionRequiresReboot`` Boolean value to specify if a reboot should be performed to complete the action. 
   .. note:: Reboot support should only be used as the final action in a sequence currently


Action Types
------------

The following are the currently supported action types and how to use them

Command
^^^^^^^

Identical to `Batch Script`_.

Windows Installer
^^^^^^^^^^^^^^^^^

The ``WindowsInstaller`` action is used for running msi and msp files. The options '/qb! /l*vx' to msiexec.exe are
automatically appended.   

- ``ActionFile`` The installer location
- ``ActionArguments`` Must be one of the options *install* or *uninstall*. Case is insensitive  

Example :

.. code-block:: xml

    <Action>
      <ActionType>WindowsInstaller</ActionType>
      <ActionFile>SystemTestServiceInstaller.msi</ActionFile>
      <ActionArguments>install</ActionArguments>
      <ActionTimeout>30000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>


Executable
^^^^^^^^^^

The ``Executable`` action is used for stand alone executables 

- ``ActionFile`` The exe file to execute.
- ``ActionArguments`` Arguments to be passed into the exe as if it was on the command line 
- ``ActionReturnCode`` The expected return code of the action.  

Examples : 

.. code-block:: xml


    <Action >
      <ActionType>Executable</ActionType>
      <ActionFile>SystemTestExecutable.exe</ActionFile>
      <ActionArguments>--service Foo -qwerty</ActionArguments>
      <ActionTimeout>10000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>

Batch Script
^^^^^^^^^^^^

The ``BatchScript`` action is used for executing batch files via a cmd.exe 

- ``ActionFile`` The batch file to execute.
- ``ActionArguments`` Arguments to be passed into the script as if it was on the command line 

Examples : 

.. code-block:: xml

    <Action>
      <ActionType>BatchScript</ActionType>
      <ActionFile>BatchInstall.bat</ActionFile>
      <ActionArguments>foobar</ActionArguments>
      <ActionTimeout>10000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
    
Windows Script
^^^^^^^^^^^^^^

The ``WindowsScript`` action is used for executing .vbs files and other cscripts

- ``ActionFile`` The file to execute.
- ``ActionArguments`` Arguments to be passed into the script as if it was on the command line 
- ``ActionReturnCode`` The expected return code of the action.  

Examples : 

.. code-block:: xml

    <Action>
      <ActionType>WindowsScript</ActionType>
      <ActionFile>install.vbs</ActionFile>
      <ActionArguments>foobar86754</ActionArguments>
      <ActionTimeout>10000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>

Powershell Script
^^^^^^^^^^^^^^^^^

The ``PowershellScript`` action is used for executing any stored powershell scripts. Though please note that some script
execution policies may need to be dealt with before executing custom powershell scripts. 

- ``ActionFile`` The powershell file to execute. The 64 Bit PS engine will be attempted before 32 bit PS engine
- ``ActionArguments`` Arguments to be passed into the script as if it was on the command line 

Examples : 

.. code-block:: xml

   <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PCM\Scripts\CleanProcesses.ps1</ActionFile>
      <ActionArguments></ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
   </Action>
   
   <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PCM\Scripts\PlacePackages.ps1</ActionFile>
      <ActionArguments>-PlatformFolder [databag@DeploymentRootFolder] -PackageFolder ".\Server"</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
    
    <Action>
      <ActionType>PowershellScript</ActionType>
      <ActionFile>PCM\Scripts\CreateDeploymentXML.ps1</ActionFile>
      <ActionArguments>-InstallScriptsFolder [databag@deploymentscripts] -NodeType [databag@RoleType] -LocalizationCode [databag@LocalizationCode] -ISiteServiceAccount [databag@ISiteServiceAccountUser]  -ISiteServiceAccountPassword [databag@ISiteServiceAccountPassword] [databag@Federated]</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>
       
    <Action EffectiveUserName="[databag@DomainAdmin]" EffectiveUserPassword="[databag@DomainAdminPassword]" RetryCount="5">
      <ActionType>PowershellScript</ActionType>
      <ActionFile>[databag@deploymentscripts]\deploy.ps1</ActionFile>
      <ActionArguments>-DeploymentConfigurationFile [databag@deploymentscripts]\DeploymentConfiguration.xml</ActionArguments>
      <ActionTimeout>1000000</ActionTimeout>
      <ActionReturnCode>0</ActionReturnCode>
      <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>

Library
^^^^^^^

Not currently implemented

SecurityPolicy
^^^^^^^^^^^^^^

Not currently implemented

Decompress
^^^^^^^^^^

The ``Decompress`` action is used to uncompresses a stored zip file, such as wrapped delivery package, into the
corresponding directory

- ``ActionFile`` The zip or tarball to decompress relative to the PCM Package directory
- ``ActionArguments`` Destination on the system to expand to. Will create if not present. 

Example :

.. code-block:: xml

    <Action>
     <ActionType>Decompress</ActionType>
     <ActionFile>IMACSDeployment.zip</ActionFile>
     <ActionArguments>[databag@DeploymentRootFolder]\VLCapture</ActionArguments>
     <ActionTimeout>10000</ActionTimeout>
     <ActionReturnCode>0</ActionReturnCode>
     <ActionRequiresReboot>False</ActionRequiresReboot>
    </Action>  
        
Place
^^^^^

The ``Place`` action stores a file or directory tree into a given location.

.. note:: templating actions for replacement of values will be supported in future releases. 

- ``ActionFile`` The file or directory relative to the PCM Package directory
- ``ActionArguments`` Destination on the system to expand to. Will create if not present. 
  
Example :

.. code-block:: xml

        <Action>
          <ActionType>Place</ActionType>
          <ActionFile>vlConfig.xml</ActionFile>
          <ActionArguments>[databag@DeploymentRootFolder]\VLCapture\configuration\</ActionArguments>
          <ActionTimeout>10000</ActionTimeout>
          <ActionReturnCode>0</ActionReturnCode>
          <ActionRequiresReboot>False</ActionRequiresReboot>
        </Action>  


Service 
^^^^^^^

- ``ActionFile`` The existing service to act upon. This can be a recently installed service, though beware of timing issues. 
- ``ActionArguments`` Must be one of the options *start*, *stop*, *restart*. Case is insensitive  

Example :

.. code-block:: xml

        <Action>
          <ActionType>Service</ActionType>
          <ActionFile>iSiteMonitor</ActionFile>
          <ActionArguments>[databag@DeploymentRootFolder]\VLCapture</ActionArguments>
          <ActionTimeout>10000</ActionTimeout>
          <ActionReturnCode>0</ActionReturnCode>
          <ActionRequiresReboot>False</ActionRequiresReboot>
        </Action>  
