============
PCM Packager
============

PCM Packager is a helper application for use inside of a CI build system as well as other useful functions.

Installation
------------
PCM Packager is available as an SVN checkout to ensure that the latest version is always used. This may also be set as
an external under the Build/Tools directory

https://svn-01.ta.philips.com/svn/masterbuild/tools/trunk/pcmPackager

DataBag Encryption
------------------

Databags may store their key value pairs as an encrypted string, which is highly beneficial for sensitive items such as
domain passwords. Using the encrypt or decrypt command allows a user to prepare or decode values. 

Example :

::

   pcmPackager.exe /cmd:encrypt /str:st3nt0r

would produce an encrypted value of 

::

   mJvIHQNgaILCc8QSpirQjUbj+lb0ShbwMeXiflRrSHxiKzvRIgmd1ag4bE6KRXsjOmiCWgBgsjQm9+8oIc9ZJBZAJZQ2mYvsoNk0Lx09KSOslucLPTNpot5Uvi57u+iYMmSH6r2DcF4wwVTrJnLnVsDbRKbw1TSUla9GUeRQkUIFYgjXYBmu5YqaJwq58Ed63VtpMG9JmzKqQThIsuuRKlvq54Zj2yKHn/U+ZWnp8xMHmEIjwdf4Gl0DIb3X459YoBNUVXEcJhas1Mev5xx5PjK6UwmxxqjrWjKVtbZU9jM3OfJ/pLF1QzZesAK2JgacOhsaHHavlgQnKaIP1WbqQQ==``

To determine what an existing encrypted value is, a technician may decrypt the key for troubleshooting purposes. 

::

   pcmPackager.exe /cmd:decrypt /str:mJvIHQNgaILCc8QSpirQjUbj+lb0ShbwMeXiflRrSHxiKzvRIgmd1ag4bE6KRXsjOmiCWgBgsjQm9+8oIc9ZJBZAJZQ2mYvsoNk0Lx09KSOslucLPTNpot5Uvi57u+iYMmSH6r2DcF4wwVTrJnLnVsDbRKbw1TSUla9GUeRQkUIFYgjXYBmu5YqaJwq58Ed63VtpMG9JmzKqQThIsuuRKlvq54Zj2yKHn/U+ZWnp8xMHmEIjwdf4Gl0DIb3X459YoBNUVXEcJhas1Mev5xx5PjK6UwmxxqjrWjKVtbZU9jM3OfJ/pLF1QzZesAK2JgacOhsaHHavlgQnKaIP1WbqQQ==

would decrypt the value of ``st3nt0r``


Checksum
--------

Checksum is a way to manually compute the checksum of a zip package for inclusion in hand crafted PCM packages.

::

   pcmPackager /command:checksum /pkg:"D:\Views\pcmTrunk\Output\pcmChecksumTests\Basic\Basic-2.0.1.0.zip"

might produce a value of ``70d91922c2a916448c6ec89ca350471f``


Validate
--------

Validate is a way to manually verify a xml metadata files is properly constructed

::

  pcmPackager /command:validate /meta:"D:\Views\pcmTrunk\Output\pcmChecksumTests\Basic\Basic-2.0.1.0.xml" 
  
The return code is 0 for good, -1 for not.


Build Integration (Populate)
----------------------------

Below is an example of how to integrate PCM Package into the MasterBuild framework. 

- ``Replace`` inserts the current build number into the raw meta-data file
- ``Tar`` performs the final packaging into the end zip file 
- ``Execute`` Populates the XML with the checksum and version info. Also relabels both files

Example :

.. code-block:: xml

   <Replace FriendlyName="Stamp appversion.txt" RunIfTrue="%UTApplyVersionToFiles%">
    <RegEx OldText='BUILD_VERSION' NewText="$PBuildVersion" src="%GBuildRoot%\PCM\PCM-0.0.0.0.xml"/>
  </Replace>
  
   <Tar>
    <Folder Src="%GBuildRoot%\Output\pcm" Dest="%GBuildRoot%\Output\Packages\%PProductName%-%PFullVersion%.zip" Include="*.*" Exclude="*.pdb" Type="zip"/>
   </Tar>
   
     <Execute TestDescr="Package Up the Zips with Metadata">
        <Command Program="%GBuildRoot%\Build\Tools\pcmPackager\PCMPackager.exe" 
        Params="/cmd:populate /metadata:%GBuildRoot%\Output\Packages\pcm-0.0.0.0.xml /pkg:%GBuildRoot%\Output\Packages\pcm-%PFullVersion%.zip" 
        StartIn="%GBuildRoot%\Output\"/>
    </Execute> 


Troubleshooting
---------------

- Make sure that the SVN directory has the latest version of PCM Packager from the PCM build. Newer actions and
  conditions may not have been propagated there, causing XML errors

- Extract the PCM Packager execution line from FB and attempt manually from the build machine. 
