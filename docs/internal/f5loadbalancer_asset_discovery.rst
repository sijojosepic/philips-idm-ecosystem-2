
========================================
F5 LoadBalancer Asset Discovery Overview
========================================

----------
Overview
----------

F5 LoadBalancer Asset discovery feature provide the statistics of Pool and Pool members. It also provide the information on
list of installed certificates in loadbalancer with the expiry date information. This information will be stored in the
facts collection database.

iControl REST API provided by F5 is used to retrieve the Certificate Expiry, Pool and Pool members information.

-----------------------------------------------------------------------------------
Example of F5 LoadBalancer Asset Discovery information in facts collection database
-----------------------------------------------------------------------------------

The following example shows the facts collection of Pool, Pool members and Certificate expiry information.

Example: ::

    {
        "_id" : "IDM01-192.168.180.6",
        "hostname" : "192.168.180.6",
        "siteid" : "IDM01",
        "F5LB" : {
            "Certificate" : {
                "default" : {
                    "expiration date" : "Dec 13 13:52:29 2027 GMT"
                },
                "ca-bundle" : {
                    "expiration date" : "Dec 31 23:59:59 2029 GMT"
                },
                "f5-irule" : {
                    "expiration date" : "Aug 13 21:21:29 2031 GMT"
                }
            },
            "Pool" : {
                "DICOM_202" : {
                    "status" : "offline",
                    "pool_members" : {}
                },
                "DICOM_I04" : {
                    "status" : "available",
                    "pool_members" : {
                        "IDM01PR3:104" : {
                            "state" : "up",
                            "address" : "192.168.180.32"
                        }
                    }
                },
                "DICOM_IN_104" : {
                    "status" : "available",
                    "pool_members" : {
                        "IDM01PR2:104" : {
                            "state" : "up",
                            "address" : "192.168.180.31"
                        },
                        "IDM01PR1:104" : {
                            "state" : "up",
                            "address" : "192.168.180.30"
                        }
                    }
                }
            }
        },
        "product_id" : null,
        "endpoints" : [
            {
                "scanner" : "F5LB",
                "address" : "192.168.180.6"
            }
        ],
        "modules" : [
            "F5LB"
        ]
    }


