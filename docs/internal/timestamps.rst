----------
Timestamps
----------

It is important to ensure timezones are being considered when sending data.

Messaging components that require timestamps as input will accept them in UNIX epoch format (The number of seconds since
the UNIX epoch; Thursday, 1 January 1970 00:00:00 UTC).

UNIX epoch is a very standard format that does not need to deal with time zone conversion, Nagios has a built in macro
to pass a timestamp in this format (TIMET).

Messages carrying timestamp information will encode it in a string using ISO 8601 extended format in UTC with no offset.
An example would be "2013-07-23T13:38:45.404123Z"

ISO 8601 is widely used and it should be a workable format using any language, not including the time zone places the
responsibility of determining the time zone to use in the entity presenting the data.

For python code offset-naive datetime objects generated from UTC should be used.
