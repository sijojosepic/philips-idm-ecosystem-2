====================================
IDM DistRad Discovery and Monitoring
====================================

Overview
--------
::

    1. Monitoring DistRad Prime set up
    2. Monitoring DistRad Connector set up



DisRAD Prime Monitoring
_______________________

DisRAD prime set up is similar to ISPACS/UDM set up, so the discovery and monitoring of DisRAD prime is almost similar to ISPACS/UDM monitoring.
wherein the DistRAD (Distradprime, KeyCloak, RWS etc) related nodes information would be available along with the iSiteWeb extended nodes


Here DisRAD prime monitoring is done by using the same ISP scanner
But in order to identify a DistRAD environment is_distrad_prime flag will be set to  yes in the host's facts.


Types of nodes in DisRAD set up

Core nodes
----------
Dinfra - Same as ISP infra node except below services are disabled:

    * Product__IntelliSpace__PACS__MirrorConfig__Status
    * Product__IntelliSpace__PACS__RemoteCache_MigrationConfig__Status
    * Product__IntelliSpace__PACS__WaitingStackConfig__Status
    * Product__IntelliSpace__PACS__ActiveStackConfig__Status

Non Core OR extended nodes
--------------------------
    Rhapsody (Clustered)
    RabbitMQ (Clustered)
    Database (Clustered)
    Distradprime
    KeyCloak (Clustered)
    RWS

Here the Distradprime and RWS nodes are exposing its own GD APIs,
So during the discovery process, respective GD apis will be invoked in order to get the nodes and services in formation.
Based on the response configuration files would be added.
Distradprime GD api provides information about KeyCloak nodes also.

For more information on the same refer IDM Generic Discovery documentation

Default Generic Discovery URLs:

    * Distradprime - 'https://<DRP_Node_Address>/DistRadNodeManager/v1/discover'
    * RWS -  'https://<AW_Node_Address>/ConfigurationService/api/configuration/discovery'

During Discovery the URL for a particular module_type can be overriden in the lhost.yml endpoint definition by adding _URL suffix with module_type
Example - I4_URL, RWS_URL, DISTRADPRIME_URL (all CAPS or LOWERCASE)

By default IDM GD API is HMAC authorized, if it requires a product can disable HMAC autorization during GD by setting [module_type]_hmac_enabled to false in the endpoint configuration all CAPS or LOWERCASE
Example - rws_hmac_enabled: false

endpoint:

::


      - address: DSG01DI1.DSG01.iSyntax.net
        distradprime_hmac_enabled: false
        distradprime_url: https://dsg01dp1.dsg01.isyntax.net/DistRadNodeManager/v1/discover
        password:
          $resource: $USER57$
        scanner: ISP
        tags: test
        username:
          $resource: $USER56$


DistRAD Prime Roles:
    * distrad-prime
    * distrad-rmq-prime
    * distrad-db-prime
    * distrad-hl7-prime
    * distrad-forindex
    * distrad-keycloak

DistRad Connector
-----------------

DistRAD Connector will be part of UDM set up, if DistRAD connector is added along with iSiteWeb Extended nodes,
IDM will invoke its GD API, and based on the responses hosts and services will be created, this behaviour is same as IDM Generic Discovery please refer the documenation on the same for further information

DistRad Connector Roles:

    * distrad-connector
    * distrad-rmq-server
    * distrad-db-server
    * distrad-hl7-server
    * distrad-forstore

