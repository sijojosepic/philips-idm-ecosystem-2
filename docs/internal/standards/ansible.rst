=======
Ansible
=======

Indentation
-----------
Indent YAML with 2 spaces.

Naming
------
Use underscores if needed, avoid dashes.

Variables
---------
Do not use deprecated variable syntax "$var".

No space around JINJA variables {{some_var}}

Roles
-----
Roles should be self-contained.

Avoid cross role task usage.

Playbooks
---------
Should only use roles, except if pre_tasks or post_tasks are needed.

Paths
-----
Do not include trailing slashes when defining path.

Tasks
-----
If a task is too long to fit in one line, use the YAML dictionary approach to break it out in lines. ::

    - name: ensure apache is at the latest version
      yum:
        pkg: httpd
        state: latest
