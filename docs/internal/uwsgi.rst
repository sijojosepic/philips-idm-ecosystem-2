====
UWGI
====

UWISG is the primary HTML endpoint for Vigilant utilizing the UWSGI protocol. There are actually two items hosted in this manner. Configurations are located in `/etc/uwsgi/` 

Gateway
-------

This endpoint provides the Vigilant health check as well as routes the HTML payloads to the correct Celery task queue for processing. These endpoints are generally input only with HTML POST commands

*  /notification :  phim_backoffice.routers.do_notification 
*  /state :  phim_backoffice.routers.do_state 
*  /perfdata :  phim_backoffice.routers.do_perfdata 
*  /discovery :  phim_backoffice.routers.do_discovery 
*  /heartbeat :  phim_backoffice.transport.hbmessage2hbdashboard 
*  /queuetransport :  phim_backoffice.transport.queuemapper 

Tank
----

Tank exposes the following endpoints which are documented in the API section of this document.
 

* /info/
* /dashboard/
* /facts/
* /exceptions/
* /field/
* /sites/
* /subscriptiongroups
* /subscriptiongroups/<path:path>
* /subscriptions
* /subscriptions/<path:path>
* /in/<resource>/<action>
* /health