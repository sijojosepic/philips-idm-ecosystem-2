=======
Modules
=======

Modules in Shinken 2 changed in the way they are installed. All modules were split into separate repos and are not
bundled with Shinken as they were in 1.x.

Downloading Modules
===================

To download module tgz files and not install them the shinken command must be used. For example: ::

    shinken --proxy http://199.168.151.10:9480 install --download-only livestatus

Phim Modules
============
Modules created for specific purposes of IDM monitoring.

Cryptresource
-------------
The crypt resource poller module will pre-process command strings to replace passed in values with decrypted values
before executing them.

Resources to be decrypted are expected to be enclosed in {+ +}. For example for an encrypted string abc123encrypted may
be configured in the resource file as::

    $USER55$={+abc123encrypted+}

The actual value decrypted from abc123encrypted will be what is passed to the plugin.

The commands that need to be processed by this module must have cryptresource module_type in the command definition::

    define command {
        command_name    check_nija_things
        command_line    $USER1$/check_nija_things -H $HOSTADRESS$ -u $USER99$ -p $USER92$
        module_type     cryptresource
    }
