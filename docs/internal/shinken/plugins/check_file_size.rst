*******************
check_file_size.py
*******************

The **check_file_size.py** plugin can be used to check the size of a file or folder in a windows host, also its been customized to Monitor RabbitMQ log file size, where the Log file name has to be dynamically computed(rabbit@hostname.log).

The plugin executes **PowerSell** queries over **WinRM** to get to know the size.


Examples::


  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'folder' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log11' -w 450 -c 500*

   WARNING : Path does not exist - S:/Philips/apps/iSite/data/rabbitmQ/log11


  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'folder' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log' -w 450 -c 500*

   OK: Current Folder Size is 5.73MB.

  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'rabbitmq' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/' -w 450 -c 500*

   OK: The Rabbitmq Log file size is 3.75MB.

  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'rabbitmq' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log12' -w 450 -c 500*

   WARNING : Could not locate RabbitMQ log file (rabbit@<hostname>.log), in the given path S:/Philips/apps/iSite/data/rabbitmQ/log12


  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'file' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/rabbitmq@idm04if1.log' -w 450 -c 500*

  WARNING : Path does not exist - S:/Philips/apps/iSite/data/rabbitmQ/log/rabbitmq@idm04if1.log


  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'user' -P pwd' -t 'file' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/rabbit@idm04if1.log' -w 450 -c 500*

   OK: Current File Size is 3.75MB.


  -*./check_file_size.py -H idm04if1.idm04.isyntax.net -U 'usero' -P pwd' -t 'file' -p 'S:/Philips/apps/iSite/data/rabbitmQ/log/rabbit@idm04if1.log' -w 450 -c 500*

   UNKNOWN : Authentication Error - the specified credentials were rejected by the server