===============================================
Veeam Backup and Replication License Monitoring
===============================================

Overview
--------

The plugin expect Host Name/IP address, User Name, Password, and Critical_val. Critical_val in days, it checks for the
number of days license remaining against the license expiration date. if the days remaining matches or lower than the
Critical_val, it will return CRITICAL.
It uses python WinRM module and invokes the veeam powershell commands to get the veeam license expiration date and
subtracts against actual system date.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK: 50 days remaining for Veeam Backup and Replication license to expire.

2. CRITICAL Status ::
    CRITICAL : 25 days remaining for Veeam Backup and Replication license to expire.

Service Check Name
------------------

Product__ISEE__Veeam__License_Expiry__Status

Frequency
---------

The frequency of the service check - Once in a week
Retry interval - 15 minutes
Retry attempts - 3

Example
-------

Run the script with command below ::

check_veeam_license_info.py -H 192.168.xx.xx -U 'username' -P 'pass' -C '30' #days for critical threshold.