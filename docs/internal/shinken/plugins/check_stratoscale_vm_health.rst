==========================
Check Stratoscale VM Health
==========================

Overview
--------

This plugin checks the status of VM usage of CPU, Memory and Network on a Startoscale Cluster.
It invokes the REST APIs exposed by stratoscale to get the resource usage value and computes the usage in percentage.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects hostname (or IP address of the Startoscale cluster), domainname, username, password, vmid, warning threshold, critical threshold and the resource type (MEMORY|CPU|NETWORK_TX|NETWORK_RX)
as input parameters.

Example
-------

Run the script with command below ::

    python check_stratoscale_health.py -H [IP Address of the Stratoscale Cluster] -i [Stratoscale VM ID] -d [Domain name on Stratoscale] -u [Username] -p [Password] -w [Warning Threshold] -c [Critical Threshold] -r [CPU|MEMORY|NETWORK_TX|NETWORK_RX]

Frequency
---------

The script is executed every 5 minutes for each of the resources(CPU, MEMORY, NETWORK_TX and NETWORK_RX).

Return Code
-----------

0. OK

1. WARNING

2. CRITICAL

Output
------

| The script outputs the following data

1. OK Status ::
    <<CPU|MEMORY|NETWORK_TX|NETWORK_RX>> Usage = <<Usage Value>>%, Status is Ok

2. Warning Status ::
    <<CPU|MEMORY|NETWORK_TX|NETWORK_RX>> Usage = <<Usage Value>>%, Status is Warning

2. Critical Status ::
    <<CPU|MEMORY|NETWORK_TX|NETWORK_RX>> Usage = <<Usage Value>>%, Status is Critical
