=============================
Retrieve distributed packages
=============================

Overview
--------

This plugin is used retrieve distributed packag files available on neb at '/var/philips/repo/Tools/PCM/packages/' and translate them to downloadable url for every package

This plugin is also can be used to retrieve the files available on neb at '/var/philips/info/' and translate them to downloadable url

The service namespace which is using this plugin will be 'Administrative__Philips__Distributed__Packages__Information' and .

This service will be running on localhost of the NEB node.

This will be an informational service and no events will be displayed in portal for the state change of this service.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework.

Example
-------

Run the script with command below ::

    python check_distributed_packages.py -p '/var/philips/repo/Tools/PCM/packages/' -e '["xml", "json"]'

    python check_distributed_packages.py -p '/var/philips/info/' -e '["xml"]'

Frequency
---------

The script will be executed twice in a day

Output
------

| The script outputs the following data if there no packages distributed

No packages distributed

| The script outputs the following data if there are packages distributed

CE411

https://192.168.59.66/repo/Tools/PCM/packages/CE411/CE_Bundle-4.11.xml
https://192.168.59.66/repo/Tools/PCM/packages/CE411/CE_Bundle-4.11.zip

ISP

https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.552.28.201904081308.xml
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.552.28.201904081308.zip
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.553.30.201907081308.xml
https://192.168.59.66/repo/Tools/PCM/packages/ISP/ISP-4.4.553.30.201907081308.zip

| The plugin output for package siteinfo infomation

https://192.168.59.66/info/siteinfo1234.xml
https://192.168.59.66/info/siteinfo2345.xml