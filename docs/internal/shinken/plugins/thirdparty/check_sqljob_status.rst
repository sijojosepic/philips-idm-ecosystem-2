Check Sql Job Status
========================

Check the status of sql backup job running on the Server node. Job status will be displayed based on the status received
as response.

Exit Status - **OK** or **CRITICAL**

**Critical**

    1) When the sql backup job is not running in the server.
	2) Occurance of an error, (connectivity, authentication).


**Python Dependent Libraries**

	argparse 1.2.1
	pymssql 2.1.0

**Execution**

usage: check_sqljob_status.py [-h] -H HOSTNAME -U USERNAME -P PASSWORD
                              -D DATABASE -J JOB

Monitor sql backup jobs
	optional arguments:
	  -h, --help            show this help message and exit
	  -H HOSTNAME, --hostname HOSTNAME
	                        The hostname of the server
	  -U USERNAME, --username USERNAME
	                        The user(eg `DOMAIN\user`) name of the server
	  -P PASSWORD, --password PASSWORD
	                        The password for the server
	  -D DATABASE, --database DATABASE
	                        The database name of the server
      -J JOB, --job JOB
	                        The job name of the server
      -V VIGILANT_URL, --vigilant-url VIGILANT_URL
                            The url of the back-office



**Example**
    ./check_sqljob_status.py  -H 1.1.1.1 -U 'ISEE02\phiadmin' -P 'password' -D mydb -J 'PhilipsDatacenter.Every 15 Minutes' -V 'https://vigilant/'
