Check Sql Jobs Status
=====================

Check the status of all sql backup jobs running on the Server node. Jobs status will be displayed based on the status
received as response.

Exit Status - **OK** or **CRITICAL** or **WARNING**
Priority is given to critical status over warning status

**Ok**

    1) When all the configured sql backup jobs are in succeeded state in the server job history.

**Critical**

    1) When atleast one sql backup job is in failed state in the server job history.
	2) Occurance of an error, (connectivity, authentication).

**Warning**

    1) When atleast one sql backup job is in canceled state in the server job history.


**Python Dependent Libraries**

	argparse 1.2.1
	pymssql 2.1.0


**Execution**

usage: check_sql_jobs_status.py [-h] -H HOSTNAME -U USERNAME -P PASSWOR -D DATABASE

Monitor sql backup jobs
	optional arguments:
	  -h, --help            show this help message and exit
	  -H HOSTNAME, --hostname HOSTNAME
	                        The hostname of the server
	  -U USERNAME, --username USERNAME
	                        The user(eg `DOMAIN\user`) name of the server
	  -P PASSWORD, --password PASSWORD
	                        The password for the server
	  -D DATABASE, --database DATABASE
	                        The database name of the server
      -V VIGILANT_URL, --vigilant-url VIGILANT_URL
                            The url of the back-office


**Example**
    ./check_sql_jobs_status.py  -H 1.1.1.1 -U 'db1.IDM04' -P 'password' -D mydb -V 'https://vigilant/'
