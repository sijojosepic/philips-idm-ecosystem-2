check_hp_crc
============

From this address:

https://exchange.nagios.org/directory/Plugins/Hardware/Network-Gear/HP/HP-ProCurve-Switches-CRC-2FPackets-Check/details

Requirements
- The following shell commands must exist and be executable by your Nagios user: snmpwalk, awk
- SNMP must be enabled on the device.