====================
Check Scanner Sanity
====================

Overview
--------

This plugin will check for the presence of ISP and vCenter scanners in discovery.yml.

The service namespace which is using this plugin will be 'Administrative__Philips__Site_Config__Scanner_Sanity__Status'.

This service will be running on localhost of the NEB node.

This will be an informational service and no events will be displayed in portal for the state change of this service.

The service check will  give as OK status when both ISP and vCenter scanners are present in discovery.yml file. Also it will check for ISPACS version and if it is greater than '4.4' along with the scanners it will give as OK status.

When any of the respective scanners are not present in discovery.yml then it will rasie a CRITICAL alert with proper message.

This service check will be considered as a master service for site-configuration tasks and when it will go to OK state the site-configuration service will be executed.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework.

Example
-------

Run the script with command below ::

    python check_scanners.py

Frequency
---------

The script will be executed twice in a day

Return Code
-----------

0. OK

2. CRITICAL

Output
------

| The script outputs the following data

1. OK Status ::
    OK: ISP and vCentrer Scanners are present in discovery.yml


2. Critical Status ::
    CRTICAL: vCenter Scanner is not present in discovery.yml

3. Critical Status ::
    CRITICAL: ISP Scanner is not present in discovery.yml

4. Critical Status ::
    CRITICAL: Please check discovery.yml for site-configuration