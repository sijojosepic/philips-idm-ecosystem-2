Check Recursive File Age
========================


Check the age of files, in a given folder path, including subfolders, based on the threshold (minutes) provided.
The default threshold is 30 minutes and plugin uses WinRM, to execute PowerShell scripts

Exit Statues - **OK** or **CRITICAL**

**Critical**

	   1) When the age of at least one file is more than the
	 		threshold provided.
	   2) Occurance of an error.



**Python Dependent Libraries**

	Pywinrm 0.3.0
	xmltodict 0.9.2
	requests >=2.9.1
	requests_ntlm >=0.3.0 
	ntlm-auth >=1.0.2



**Execution**

usage: check_recursive_file_age.py [-h] -T THRESHOLD -H HOST -U USERNAME -D
                                   DRIVE -P PASSWORD -F FOLDERPATH

Monitor the age of files inside a folder, and its subfolders

	optional arguments:
	  -h, --help            show this help message and exit
	  -T THRESHOLD, --threshold THRESHOLD
	                        Threshold in minutes
	  -H HOST, --host HOST  The hostname of the server
	  -U USERNAME, --username USERNAME
	                        The user(eg `DOMAIN\user`) name of the server
	  -D DRIVE, --drive DRIVE
	                        The Drive Name (eg S)
	  -P PASSWORD, --password PASSWORD
	                        The password for the server
	  -F FOLDERPATH, --folderpath FOLDERPATH
	                        The folder path (eg `\Stentor\forward`)



**Example**
	./check_recursive_file_age.py -H 1.1.1.1 -D S -F '/Stentor/Forward' -T 30 -U 'IDM01\user' -P 'passwd'