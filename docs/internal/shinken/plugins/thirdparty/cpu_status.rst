=====================
Check CPU Utilization
=====================

Overview
--------

This plugin checks the overall usage of CPU and top 3 processes in terms of CPU utilization.
It invokes the powershell script which gets all the process details.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects hostname (IP address), username, password, warning threshold and critical threshold as input parameters.

Example
-------

Run the script with command below ::

    python cpu_status.py -H [IP Address] -U [Username] -P [Password] -w [Warning Threshold] -c [Critical Threshold]

Frequency
---------

The script is executed in every 10 minutes.

Return Code
-----------

0. OK

1. WARNING

2. CRITICAL

Output
------

| The script outputs the following data

1. OK Status ::
    OK: Total CPU usage - <<Usage Value>>%, Top 3 processes: <<Process1 Name>> - <<Usage Value>>%, <<Process2 Name>> - <<Usage Value>>%, <<Process3 Name>> - <<Usage Value>>%

2. Warning Status ::
    Warning: Total CPU usage - <<Usage Value>>%, Top 3 processes: <<Process1 Name>> - <<Usage Value>>%, <<Process2 Name>> - <<Usage Value>>%, <<Process3 Name>> - <<Usage Value>>%

2. Critical Status ::
    Critical: Total CPU usage - <<Usage Value>>%, Top 3 processes: <<Process1 Name>> - <<Usage Value>>%, <<Process2 Name>> - <<Usage Value>>%, <<Process3 Name>> - <<Usage Value>>%
