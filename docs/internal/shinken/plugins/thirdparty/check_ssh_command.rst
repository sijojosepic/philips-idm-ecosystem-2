=====================
Check SSH Command
=====================

Overview
--------

This plugin execute SSH command and provide status on the basis of command used. ex. partition
It invokes the SSH script which gets all the details for monitoring.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework.
The script expects hostname (IP address), SSH username, SSH password, command, warning threshold and critical threshold as input parameters.

Example
-------

Run the script with command below ::

    python check_ssh_command.py -H [IP Address] -u [SSH_Username] -p [SSH_Password] -x [Command] -w [Warning Threshold] -c [Critical Threshold]


Command
-------
Currently "partition" and "license" command is supported by this plugin.


Partition Monitoring
--------------------
This plugin will provided mount partition status of the host.
The plugin will return CRITICAL/WARNING status if any of the mount partition crosses the critical threshold/warning threshold.

For partition monitoring - SSH Command used is df -h, to get all the mount details.

Frequency: Every 5 minutes.


License Monitoring
--------------------
This plugin will provided license status of the host.
The plugin will return CRITICAL if license is expired or is going to expire. (5 days before alert has to be triggered of temp license.)
It will return OK in case of Permanent License or if the license has time for expire.

For license monitoring - SSH Command used is 'tmsh show /sys license | grep License', to get the license details.

Frequency:  24hrs


Return Code
-----------

0. OK

1. WARNING

2. CRITICAL


Default Threshold Values:
-------------------------

Mount Partition CRITICAL : 85
Mount Partition WARNING  : 80
License CRITICAL: 5 (Days)