Check HTTP/HTTPS PING with HMAC
===================

------------
Intended Use
------------

Check the ping of an application through HTTP/HTTPs GET method call.
Upon 200 HTTP response, application status will be displayed based on the status received
through response content.


In the Response status has to be any of [OK', 'WARNING', 'CRITICAL', 'UNKNOWN']


CRITICAL status with appropriate message will be displayed for non 200 response statuses.



Usage
***** 
    check_https_hmac.py [-h] -H HOST -u SERVICE_URL [-p PROTOCOL]
    
    Check the Ping of an Application over HTTP

    optional arguments:
      -h, --help            show this help message and exit
      -H HOST, --host HOST  The hostname of the server
      -u SERVICE_URL, --service_url SERVICE_URL
                            The Service URL
      -p PROTOCOL, --protocol PROTOCOL
                            The Request protocol HTTP vs HTTPS, defaults to HTTPS
    


Example
-------
::

 check_https_hmac.py -H <host_name> -u /LocationTopology/Diagnostics/v1/ping