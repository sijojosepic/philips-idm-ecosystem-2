======================
Check Internet Status
======================

Overview
--------

This plugin checks the node internet access.
It invokes the powershell script which checks for presence of internet in a node.

This plugin is to determine the presence of internet access from a targed windows host.

As per the feautre, In the Plugin there are two ways used to identify internet access.

First - check for access of  www.google.com

Second - check for access of www.usa.philips.com

If any of them is reponding with status code as 200, then internet access is present,and respective alert would be raised

The PowerShell script to identify internet access behaves as follows:

1. [System.Net.WebRequest]::Create($r).GetResponse() if the query returns 200 - internet access is there.
2. if the WebExpection with NameResolution is the ExceptionStatus - then no internet access is there
3. if the WebExpection with NameResolution is not the ExceptionStatus - then internet access is there.
4. if any other exception, which means the powershell script execution fails.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects hostname (IP address), username, password as input parameters.

Example
-------

Run the script with command below ::

    python check_internet.py -H [IP Address] -U [Username] -P [Password]

Frequency
---------

The script will be executed every week once.

Return Code
-----------

0. OK

2. CRITICAL

Output
------

| The script outputs the following data

1. OK Status ::
    OK: Internet cannot be accessed from the Node


2. Critical Status ::
    CRITICAL: Internet can be accessed from the Node
