========================
Check Network Usage
========================


Overview
--------

This plugin capture the present network usage and report using power shell query.
Whenever the network usage crosses critical threshod value the plugin provide a critical alert, same as in the
case of warning.

Powershell Query
----------
$interface = Get-WmiObject -class Win32_PerfFormattedData_Tcpip_NetworkInterface |
select BytesTotalPersec, CurrentBandwidth,PacketsPersec|where {$_.PacketsPersec -gt 0} ;
$bitsPerSec = $interface.BytesTotalPersec*8 ; $totalBandwidth = $interface.CurrentBandwidth ;
$network_usage = (( $bitsPerSec / $totalBandwidth) * 100); $rounded_network_usage = [math]::Round($network_usage);
Write-host $rounded_network_usage

Deployment
----------
The plugin is available as part of nagios plugin in Neb node.

Note
----
The network usage percentage has some slight variance when you compare the result with the
task manager and plugin output.


Plugin Argument
---------------

The plugin expect windows IP address, User Name, Password, Critical, Warning.

Service Check Name
------------------

OS__Microsoft__Windows__Network__Usage

Execution
------------------

usage: check_windows_network_usage_status.py [-h] -H HOSTNAME -U USERNAME -P
                                             PASSWORD -C CRITICAL -W WARNING


Monitor the windows network usage.

optional arguments:
  -h, --help            show this help message and exit
  -H HOSTNAME, --hostname HOSTNAME
                        The hostname of the server
  -U USERNAME, --username USERNAME
                        The user(eg `DOMAIN\user`) name of the server
  -P PASSWORD, --password PASSWORD
                        The password for the server
  -C CRITICAL, --critical CRITICAL
                        The Critical threshold value
  -W WARNING, --warning WARNING
                        The Warning threshold value

Example
-------

	./check_windows_network_usage_status.py -H 192.168.xx.xxx -U 'dmin' -P 'pa55w0rd' -C 90 -W 75