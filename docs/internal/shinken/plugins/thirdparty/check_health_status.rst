Check Health Status
===================

------------
Intended Use
------------

Check the health of an application through HTTP/HTTPs GET method call.
Upon 200 HTTP response, application status will be displayed based on the status received
through response content.


In the Response status has to be any of [OK', 'WARNING', 'CRITICAL', 'UNKNOWN']


CRITICAL status with appropriate message will be displayed for non 200 response statuses.


**A Typical Response**:: 

		    {
    		 "productid": "UDM",
    		 "productname": "UDM Connector",
    		 "version": "1.2.0.0",
    		 "status": "OK",
    		 "data": [
    		   {
    		     "message": "Cumulative Calls : 0 , Success Calls : 0, Failed Calls : 0"
    		   }
    		 ]
    		}

Usage
***** 
    check_health_status.py [-h] -H HOST -s SERVICE_URL [-p PROTOCOL]
    
    Check the Health of an Application over HTTP

    optional arguments:
      -h, --help            show this help message and exit
      -H HOST, --host HOST  The hostname of the server
      -s SERVICE_URL, --service_url SERVICE_URL
                            The Service URL
      -p PROTOCOL, --protocol PROTOCOL
                            The Request protocol HTTP vs HTTPS, defaults to HTTPS
    


Example
-------
::

 check_health_status.py -H <host_name> -s InstanceDiscovery/Diagnostics/v1/healthstatus