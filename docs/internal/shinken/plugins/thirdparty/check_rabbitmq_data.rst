=====================
Check RabbitMQ Data
=====================

Overview
--------

This plugin checks the channel objects, network partition status, memory watermark status, queues length(number of messages) and cluster member status using the API call to RabbitMQ server.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects below input parameters:

1. hostname - host address
2. port - port where RabbitMQ is running
3. username - RabbitMQ username
4. password - RabbitMQ password
5. protocol - http/https (default - https)
6. subpath - queues/channels/nodes/overview
7. service - watermark/partitions/cluster/disk
8. warning threshold - (default - 500)
9. critical threshold - (default - 1000)
10. timeout - request timeout for API call (default - 15)


Example
-------

Run the script with command below :

1. python check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "queues" -w 950 -c 1000 -t 15

2. python check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "channels" -w 500 -c 1000 -t 15

3. python check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "nodes" -S "watermark" -w 500 -c 1000 -t 15

4. python check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "nodes" -S "disk" -w 500 -c 1000 -t 15

5. python check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "nodes" -S "partitions" -w 500 -c 1000 -t 15

6. python check_rabbitmq_data.py -H "hostaddress" -P 15672 -u username -p password -r http -s "nodes" -S "cluster"

Frequency
---------

The script is executed in every 5 - 10 minutes.

Return Code
-----------

0. OK

1. WARNING

2. CRITICAL

3. UNKNOWN

Output
------

1. Total objects on channels.

    1.1. It returns 0, if objects are less than warning param.
        Output Message: 'OK: Gathered Object Counts: channel= <Channel Object Counts>'

    1.2. It returns 1, if objects are more than warning but less than critical param.
        Output Message: 'WARNING: Gathered Object Counts: channel= <Channel Object Counts>'

    1.3. It returns 2, if objects are more than critical param.
        Output Message: 'CRITICAL: Gathered Object Counts: channel= <Channel Object Counts>'

2. Memory watermark status.

    2.1. It returns 0, if watermark is less than 0.4 or 40%.
        Output Message: 'OK: RabbitMQ Memory utilization is OK.'

    2.2. It returns 2, if watermark is more than 0.4 or 40%.
        Output Message: 'CRITICAL: Watermark is critical, memory alarm is True. Memory limit: <value>, Memory used: <value>'


3. Memory Disk status.

    3.1. It returns 0, if watermark is less than 0.4 or 40%.
        Output Message: 'OK: RabbitMQ Disk utilization is OK.'

    3.2. It returns 2, if watermark is more than 0.4 or 40%.
        Output Message: 'CRITICAL: Disk is critical, disk alarm is True. Disk free limit: <value>, Disk free: <value>'

4. Network partition status.

    4.1. It returns 0, if no partition found.
        Output Message: 'OK: No network partitions.'

    4.2. It returns 2, if partition found.
        Output Message: 'CRITICAL: Network partitions detected: <Partitions>'

5. Queues (Length) number of messages status

    5.1. It returns 0, if number of messages are less than warning param.
        Output Message: 'OK: Total number of queues are <Total Number of Queues>'

    5.2. It returns 1, if number of messages are more than warning but less than critical param.
        Output Message:
        'WARNING: Out of <Total number of queues> queues, <Number of warning queues> queues breaches WARNING Threshold 950.
        QueueName1 - <Number of Messages>
        QueueName2 - <Number of Messages>'
        .
        .

    5.3. It returns 2, if number of messages are more than critical param.
        Output Message:
        "CRITICAL: Out of <Total number of queues> queues, <Number of critical queues> queues breaches CRITICAL Threshold 1000, <Number of warning queues> Queues breaches WARNING Threshold 950.
        CRITICAL
        --------
        QueueName1 - <Number of Messages>
        .
        .
        WARNING
        --------
        QueueName2 - <Number of Messages>"
        .
        .

    5.4 It returns 2, if data is corrupted of any other exception occurs.
        Output Message: 'CRITICAL : Error while retrieving response data <Error Details>'

6. RabbitMQ Cluster Member Status:

    Description:
        *   When node is out of network, will fetch the details from https://rabbitmq-hostname:port/api/nodes API call.
                Running status for the node will be False when it is out of network or not reachable.
        *   To check if node is out of cluster, will fetch the details from https://rabbitmq-hostname:port/api/overview API call.
                In the RabbitMQ overview API response there are 'contexts' and 'listeners' attributes available.
                Both of these contains information about the nodes in the cluster.
                When a node is out a cluster, it will present in listeners but not in context.
                By comparing these contexts and listeners, the difference of which provides which all nodes are out of the Cluster



    6.1. It returns 0, if all the cluster member are up and running.
        Output Message: 'OK: All nodes are up and running <list of all node names>'

    6.2. It returns 2, if any cluster member id out of network or not reachable.
        Output Message: 'CRITICAL: Node is out of network/not reachable <list of node names which are out of network>'

    6.3 It returns 2, if data is corrupted of any other exception occurs.
        Output Message: 'CRITICAL: Node is out of cluster <list of node names which are out of cluster>'

7. Exceptions

    7.1 It returns 3, if RabbitMQ credentials are not correct
        Output Message: 'UNKNOWN : HTTPError - <Error Details>'

    7.2 It returns 2, if Connection or request error occur
        Output Message: 'CRITICAL : Request Error <Error Details>'

    7.3 It returns 2, if any other Exception occurs
        Output Message: 'CRITICAL : Error while retrieving response data <Error Details>'
