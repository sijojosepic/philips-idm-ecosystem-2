================
Check WMI Status
================

Overview
--------

This plugin checks for OS details and authentication using WinRM.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects hostname (IP address), username and password as input parameters.

Example
-------

Run the script with command below ::

    python check_wmi_status.py -H [IP Address] -U [Username] -P [Password]

Frequency
---------

The script is executed in every 1 hour.

Return Code
-----------

0. OK

2. CRITICAL


Output
------

| The script outputs the following data

1. OK Status ::
    OK: <<OS Details>>

2. Critical Status ::
    2.1. CRITICAL: Authentication Error. You might have your username/password wrong or the user's access level is too low

    2.2. CRITICAL : Authentication Error - <<Exception Message>>>