*******************
node_status.py
*******************

Aim of this plugin is to update the varios statuses of a host to the  facts collection.
This plugin will push the payload to backoffice using version endpoint
-------------
'VERSION': {
        'ENDPOINT': 'http://vigilant/version'
    },
-------------

The hosts are differentiated with the **module_type** argument

RabbitMQ
==========

RabbiMQ version updation, from all RabbitMQ core nodes (not from the virtualhost created for monitoring RabbitMQ cluster set up)
the version would be updated to the facts collection towards respective hosts *Components*, and it would be visible over the install base report

Service Name - **Administrative__Update__RabbitMQ__Node__Status**

Sevice is configured to run once in every 6 hours.


NOTE :- *All the future hosts status updates to the facts, should be integrated with this plugin.*

Example

./node_status.py -H 'rabbitmq-server.isyntax.net' -U phiadmin -P '1nf0M@t1csPh!lt0r' -M 'RabbitMQ'

