============================================
Check Time sync status against Neb Nodes
============================================

Overview
--------

This plugin shall time synchronize Neb Nodes with API Call of Load Balancer. This plugin uses python requests module
and make an API Call to Load Balancer to get the current time of Load Balancer . This plugin always sync the Neb Nodes with Load Balancer time.

It enables the service "Product__IDM__Nebuchadnezzar__Time_Sync__Status" against Neb Nodes.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

No Arguments are required for below service "Product__IDM__Nebuchadnezzar__TIme_Sync__Status"

Frequency
---------

The frequency of the service check - 5 minutes
Retry interval - 1 minute
Max. Check Attempts - 4


Service Check Name
------------------

Product__IDM__Nebuchadnezzar__TIme_Sync__Status

    
Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK - Successfully Updated Time.

2. CRITICAL Status ::
    CRITICAL - Updating time failed due to Error.


Example
-------

Run the script with command below ::

    check_ntp_timesync.py
