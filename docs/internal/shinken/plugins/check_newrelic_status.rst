====================
New Relic Monitoring
====================

Overview
--------

This plugin shall retrieve the violation events from New Relic alert violations API. This plugin uses python request
module to make API requests. If any one of the violation doesn't have "closed_at" key then it will return CRITICAL
or WARNING based on the priority of violation event status with the APM name, its Alert policy and its label description.
This plugin needs internet connection to connect to New Relic API.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect API key.


Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK - No Alerts violations found.

2. WARNING Status ::
    WARNING - APM name -myapp, Alert Policy -Response time (90%) < 1 seconds for at least 5 minutes, Alert label-check web latency.

3. CRITICAL Status ::
    CRITICAL - APM name -myapp_new, Alert Policy -Response time (90%) < 1 seconds for at least 5 minutes, Alert label-check web latency.



Example
-------

Run the script with command below ::

    check_time_sync.py -K [API_KEY]

