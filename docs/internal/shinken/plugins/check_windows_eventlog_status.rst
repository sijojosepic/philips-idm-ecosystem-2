=============================================================
Check DFS Replication error in windows Domain Controller Node
=============================================================

Overview
--------

  This is intended to find the DFS Replication error in the windows Domain Controller Node.
  The plugin status should be "CRITICAL" if there is any error with event id : 5002 in the windows event log.
  Behaviour
  ---------

  IF there is DFS Replication error and the event id is 5002
          plugin exit with CRITICAL status with the count of occurances and error message

  else
          plugin exit with OK status

  The event id which needs to monitor should be configurable.
  if any more event id  which needs to be checked/monitored can be supplied to the Plugin
  The numer of hours the error logs analyed should be configurable default should be 10 hours.


  example
  python check_windows_eventlog_status.py -H 10.10.xxx.xxx -U 'admin' -P 'password' -T 10 -E "['5002','5008']" -S DFSR

  CRITICAL : EventId-5002,Count-17, The DFS Replication service encountered an error communicating with partner
  ISEEDC1 for replication group Domain System Volume.
  occur if the host is unreachable, or if the DFS Replication service is not running on the server.
  OK : EventId-5008,No Error Log Found


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password , Hours, EventID, Source.

Frequency
---------

check_interval          60

Service Check Name
------------------

OS__Microsoft__Windows__ DFS_Replication_Error__Status


Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK : EventId-1000,No Error Log Found.

3. CRITICAL Status ::
    CRITICAL : EventId-5002,Count-17, The DFS Replication service encountered an error communicating with partner
    ISEEDC1 for replication group Domain System Volume.


Example
-------

Run the script with command below ::

    check_windows_eventlog_status.py -H [IP Address of the Node] -U [User Name] -P [Password] -T [Hours] -E [EventID] -S [SOURCE]

