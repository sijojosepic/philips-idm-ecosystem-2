=====================================
Check firewall profile status against Windows Node
=====================================

Overview
--------

Windows firewall state will be monitored through File based generic discovery(FGD).
Monitoring for service can be enabled or disabled for a particular node through FGD.

Plugin functionality

This is intended to find the windows firewall profile state,
 firewall state can be any of Domain, Private, Public, or all of them.
 The plugin status should be "CRITICAL" if there is no firewall profile configured.
 Behaviour
 ---------
 profile state is on
    plugin exit with OK status
 Profile state is off,
    plugin exit with CRITICAL status

 The firewall state which needs to be checked/monitored can be supplied to the Plugin
 Domain state will be checked all the times, even if its not suplied through cmd args

 example
 python check_firewall_status.py -H 192.168.xx.xxx -U 'username' -P 'pass' -F '["Domain","Private","public"]'

 CRITICAL - The Domain Firewall state is OFF.
 The Private Firewall state is OFF.
This service shall be executed once in a day.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password and firewall profiles. Firewall profiles are not mandatory(default domain).

Frequency
---------

max_check_attempts      0
check_interval          1440

Service Check Name
------------------

OS__Windows__firewall__Profiles__Status


Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK : The Domain firewall state is ON.

3. CRITICAL Status ::
    CRITICAL - The Domain firewall state is OFF.
    CRITICAL - The Public firewall state is OFF.
    CRITICAL - The Private firewall state is OFF.


Example
-------

Run the script with command below ::

    check_firewall_status.py -H [IP Address of the Node] -U [User Name] -P [Password] -F [fwall_profiles]

