==========================
Check Stratoscale Health
==========================

Overview
--------

This plugin checks the status of Overall usage of CPU, Memory and Storage of a Startoscale Cluster.
It invokes the REST APIs exposed by stratoscale to get the resource usage value and computes the usage in percentage.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects hostname (or IP address of the Startoscale cluster), domainname, username, password, warning threshold, critical threshold and the resource type (MEMORY|CPU|Storage)
as input parameters.

Example
-------

Run the script with command below ::

    python check_stratoscale_health.py -H [IP Address of the Stratoscale Cluster] -d [Domain name on Stratoscale] -u [Username] -p [Password] -w [Warning Threshold] -c [Critical Threshold] -r [CPU|MEMORY|STORAGE]

Frequency
---------

The script is executed every 5 minutes for each of the resources(CPU, MEMORY and STORAGE).

Return Code
-----------

0. OK

1. WARNING

2. CRITICAL

Output
------

| The script outputs the following data

1. OK Status ::
    <<CPU|MEMORY|STORAGE>> Usage = <<Usage Value>>%, Status is Ok

2. Warning Status ::
    <<CPU|MEMORY|STORAGE>> Usage = <<Usage Value>>%, Status is Warning

2. Critical Status ::
    <<CPU|MEMORY|STORAGE>> Usage = <<Usage Value>>%, Status is Critical
