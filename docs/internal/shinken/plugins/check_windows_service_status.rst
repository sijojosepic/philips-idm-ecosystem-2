============================
Check Windows Service Status
============================


Overview
--------

This plugin shall monitor a Windows service. It uses python WinRM module and invokes the powershell command to get the
desired service start up type and status of the service.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password, and Service name. Optionally it accepts the argument
(INSTALLED/NOTINSTALLED) and the service state(CRITICAL/WARNING/OK).


Plugin Exit State based on Service state
----------------------------------------

      +------------------------+------------------+--------------------+
      |  Service Startup Type  |  Service State   | Plugin Status      |
      +------------------------+------------------+--------------------+
      |  Automatic             |  Running         |      OK            |
      +------------------------+------------------+--------------------+
      |  Automatic             |  Stopped         |      CRITICAL      |
      +------------------------+------------------+--------------------+
      |  Disabled              |  NA              |      OK            |
      +------------------------+------------------+--------------------+
      |  Manual                |  Running         |      OK            |
      +------------------------+------------------+--------------------+
      |  Manual                |  Stopped         |      OK            |
      +------------------------+------------------+--------------------+


Example
-------

Run the script with command below ::

    check_windows_service_status.py -H [IP Address of cluster] -U [User Name] -P [Password] -S ['SERVICE']
    --argument [ARGUMENT] -s [STATE]
    OR
    check_windows_service_status.py -H [IP Address of cluster] -U [User Name] -P [Password] -S ['SERVICE']

    Monitor Windows service

optional arguments:
  -h, --help            show this help message and exit
  -H HOST, --host HOST  The hostname of the server
  -U USERNAME, --username USERNAME
                        The user(eg `DOMAIN\user`) name of the server
  -P PASSWORD, --password PASSWORD
                        The password for the server
  -S SERVICE, --service SERVICE
                        The name of the Windows service to monitor
  --argument ARGUMENT   The argument to check whether service is by
                        default INSTALLED or NOTINSTALLED
  -s STATE, --state STATE
                        Provide service check state(CRITICAL/WARNING/OK) to return in case
                        --argument "INSTALLED" is provided



