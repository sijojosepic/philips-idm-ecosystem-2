=====================================
Check Time sync status against Domain
=====================================

Overview
--------

This plugin shall check the Windows time synchronization against primary Domain. This plugin uses python WinRM module
and invokes the powershell command to get the time offset. This plugin can also check the time difference against domain
name that is supplied as an argument.

To enable the service "OS__Microsoft__Windows__DomainTimeSync__Status" against MG node manual scanner, please supply the
following flag in the scanner.

time_sync = yes


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password, and Minutes. Minutes to check time difference against
primary domain. Optionally it can also accept the argument Domain(specify the domain name to check against). By default
the Domain argument(TIMESYNC_DOMAIN) will have empty value and if you want to update with domain name then please update
the variable 'TIMESYNC_DOMAIN' in lhost. The 'TIMESYNC_DOMAIN' variable is only applicable for the below service.
"OS__Microsoft__Windows__DomainTimeSync__Status"

Frequency
---------

The frequency of the service check - 5 minutes
Retry interval - 2 minutes
Retry attempts - 3


Service Check Name
------------------

OS__Microsoft__Windows__TimeSync__Status
OS__Microsoft__Windows__DomainTimeSync__Status


Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK - The current time of the node is in sync with primary domain node.

2. WARNING Status ::
    WARNING - WARNING: The node is not part of domain.

3. CRITICAL Status ::
    CRITICAL - Time not synchronized. The current time of the node is (x time) slower than primary domain.
    CRITICAL - Time not synchronized. The current time of the node is {x time} faster than primary domain.
    CRITICAL - Error fetching domain name or DNS request timed out.
    CRITICAL - Error in getting the time offset response value.
    CRITICAL - Unable to retrieve time offset response value from the server.


Example
-------

Run the script with command below ::

    check_time_sync.py -H [IP Address of cluster] -U [User Name] -P [Password] -M [Minutes] -D [Domain]

