===========================================
Check Status of Tivoli backup job schedule
===========================================

Overview
--------

This plugin shall provide the Tivoli backup scheduled jobs statistics. This plugin uses python WinRM module and invokes
the powershell command on utility server to get the last 32 days backup jobs log history. The Utility server should be
running with Tivoli client software.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Utility node IP address, User Name, Password, and Tivoli client User Name, Password.


Frequency
---------

The frequency of service check is scheduled to run once in 24hrs. between 1PM - 2PM.


Service Check Name
------------------

Product__ISEE__UtilityServer__TIVOLI_Backup__Status

Return Code
-----------

0. OK

1. WARNING

2. CRITICAL


Output
------

| The script outputs the following data

1. OK Status ::
    Ok - All backup scheduled jobs completed successfully

2. CRITICAL Status ::
    Critical - Failed back up jobs with following (Schedule Name, Scheduled Start, Node Name, Actual Start, Completed,
     Status, Result, Reason)

3. WARNING Status ::
    Warning - Backup jobs still in progress with following (Schedule Name, Scheduled Start, Node Name, Actual Start,
     Completed, Status, Result, Reason)




