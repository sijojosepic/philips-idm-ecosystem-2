==========================
Check Elasticsearch Status
==========================

Overview
--------

Check the status of the elasticsearch and kibana with the version.

Deployment
----------

The plugin is available as part of nagios plugin in Neb node.

Execution
---------

The script should be triggered by the Shinken framework. The script expects IP address (or hostname), type (esstatus|eshealth|kibanastatus) and the protocol (by default is 'http')
as input parameters.

Example
-------

Run the script with command below ::

    python check_elasticsearch_kibana_status.py -H [IP Address of the Host] -T [estatus|eshealth|kibanastatus] -P [http by default]

Frequency
---------

The script is executed every 5 minutes.

Return Code
-----------

0. OK

2. CRITICAL

Output
------

| The script outputs the following data

1. Elasticsearch OK Status ::
    OK - Elasticsearch is running; version:<version number>

2. Elasticsearch Critical Status ::
    CRITICAL - Elasticsearch is not running

3. Kibana OK Status ::
    OK - Kibana is running; version:<version number>

4. Kibana Critical Status ::
    CRITICAL - kibana is not running