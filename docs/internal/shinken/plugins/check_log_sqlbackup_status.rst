=====================================
Check sql backup status against Db Node
=====================================

Overview
--------

This plugin is used to monitor the msssql database backup status in the various DB nodes. This plugin uses python WinRM module
and invokes the powershell command to get the SqlErrorlog and collects the db status of Sql backups (FULL , DIFFERENTIAL, LOG) in ASCENDING ORDER.
As per the powershell query response the plugin will classify the output.

If any DBs latest status is FAILED the service status should be CRITICAL.
If the latest status of the DB is SUCCESSFUL and any previous status is FAILED, then the service status should be WARNING.
and the status of all DBs are SUCCESSFUL then the service status should be OK.



Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password

Frequency
---------

max_check_attempts      0
check_interval          360

Service Check Name
------------------

MSSQL__Database__SQL__BACKUP__Status


Plugin Output
-------------

| The script outputs the following data

1. OK Status ::
    OK - SUCCESSFUL : Database msdb BackupType FULL is successful.
    Database msdb BackupType DIFFERENTIAL is successful.

2. WARNING Status ::
    WARNING - WARNING : Database Xcelera BackupType DIFFERENTIAL failed On 11/21/2019 4:28:12 PM successful On 11/26/2019 12:15:12 AM.
    Database Xcelera BackupType FULL failed On 11/19/2019 10:02:48 PM successful On 11/23/2019 12:00:19 AM.
    SUCCESSFUL : Database msdb BackupType FULL is successful.
    Database msdb BackupType DIFFERENTIAL is successful.

3. CRITICAL Status ::
    CRITICAL - FAILED : Database Echo BackupType FULL failed On 11/26/2019 2:49:28 PM.
    WARNING : Database Xcelera BackupType DIFFERENTIAL failed On 11/21/2019 4:28:12 PM successful On 11/26/2019 12:15:12 AM.
    SUCCESSFUL : Database msdb BackupType FULL is successful.


Example
-------

Run the script with command below ::

    check_log_sqlbackup_status.py -H [IP Address of the Node] -U [User Name] -P [Password]

