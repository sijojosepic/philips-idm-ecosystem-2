..  _vmware_vcenter_disk_usage:

=========================
VMWare vCenter Disk Usage
=========================

Overview
--------

VMWare vCenter Disk Usage plugin shall provide the Disk usage information of vCenter 6.x appliance and vCenter 5.0. The
plugin connect to the vCenter api to gathers disk usage statistics.


Plugin Argument
---------------

The plugin expect vCenter IP address, User Name, Password, Warning and Critical threshold.


Frequency
---------

The frequency of the service check - 5 minutes
Retry interval - 2 minutes
Retry attempts - 3
WARNING - 80
CRITICAL - 90


Service Check Name
------------------

Virtualization__VMware__Vcenter__Disk__Usage


