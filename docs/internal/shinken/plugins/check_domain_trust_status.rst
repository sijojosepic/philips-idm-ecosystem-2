=========================
Check Domain Trust Status
=========================


Overview
--------

This plugin shall monitor a one way outgoing trust between two different domains in the forest.  Also it can monitor the
trust relationship between parent and child domain with in the forest. It uses python WinRM module and invokes the
powershell command to get the desired domain trust verification status.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password, trusteddomainname, trusteddomainuser, trusteddomainpassword,
trustingdomainname, trustingdomainuser, trustingdomainpassword. Optionally it accepts the argument trust type as
('one_way'/'two_way'), by default it is defined as 'two_way'.


Example
-------

Run the script with command below ::

    Following script argument to be used to monitor the one way outgoing trust(between Hospital domain and ISPACS domain)

    check_domain_trust_status.py  -H HOST -u USERNAME -p PASSWORD -d TRUSTEDDOMAINNAME -ud TRUSTEDDOMAINUSER
                                    -pd TRUSTEDDOMAINPASSWORD -td TRUSTINGDOMAINNAME -uo TRUSTINGDOMAINUSER
                                    -po TRUSTINGDOMAINPASSWORD -t TRUSTTYPE
    OR

    Following script argument to be used to monitor the two way trust(between ISPACS domain and ISEE domain)

    check_domain_trust_status.py  -H HOST -u USERNAME -p PASSWORD -d TRUSTEDDOMAINNAME -ud TRUSTEDDOMAINUSER
                                    -pd TRUSTEDDOMAINPASSWORD


plugin arguments description:
  -h, --help            show this help message and exit
  -H HOST, --host HOST  The hostname of the server
  -U USERNAME, --username USERNAME
                        The user(eg `DOMAIN\user`) name of the server
  -P PASSWORD, --password PASSWORD
                        The password for the server
  -d TRUSTEDDOMAINNAME, --trusteddomainname TRUSTEDDOMAINNAME
                        The trusted domain name (hospital domain name)
  -ud TRUSTEDDOMAINUSER, --trusteddomainuser TRUSTEDDOMAINUSER
                        The user(eg `TRUSTEDDOMAIN\user`) The hospital domain account to
                         make the connection with the hospital domain
  -pd TRUSTEDDOMAINPASSWORD, --trusteddomainpassword TRUSTEDDOMAINPASSWORD
                        The hospital domain account password to make the connection with the hospital domain
  -td TRUSTINGDOMAINNAME, --trustingdomainname TRUSTINGDOMAINNAME
                        The trusting domain name(The ISPACS domain name)
  -uo TRUSTINGDOMAINUSER, --trustingdomainuser TRUSTINGDOMAINUSER
                        The user(eg `TRUSTINGDOMAIN\user`)account to use to
                        make the connection with the trusting domain(ISPACS domain account)
  -po TRUSTINGDOMAINPASSWORD, --trustingdomainpassword TRUSTINGDOMAINPASSWORD
                        The password of the trusting domain user(ISPACS domain account password)
  -t TRUSTTYPE, --trusttype TRUSTTYPE
                        The trust type, whether one way trust(eg `one_way`) or
                        two way trust(eg `two_way`)

