..  _vmware_information:

==================
VMWare Information
==================

Overview
--------

VMWare Information Script pulls Hardware Model Number and Serial Number.

Deployment
----------

VMWare Information script can be deployed either by manually copying the script to Neb Node or by pushing it through the
IDM pipeline.

Execution
---------

The script should be triggered from a job scheduler. The script expect IP address, User Name, Password of ESX Host, and
value ''servicetag'' OR ''model''  as input parameter.

Example
-------

Run the script with command below ::

    vmware_information.py -H [IP Address of ESXi Host] -u [User Name of ESXi Host] -p [Password of ESXi Host] model
    OR
    vmware_information.py -H [IP Address of ESXi Host] -u [User Name of ESXi Host] -p [Password of ESXi Host] servicetag

Frequency
---------

The script should be executed once every night.

Return Code
-----------

0. OK
1. WARNING
2. CRITICAL
3. UNKNOWN

Output
------

| The script gathers following data

1. Checks for the Service Tag ::

    Service Name : Hardware__Server__Hardware__Information__Model
    Status : OK, Warning, Critical
    Status Information : Model Number of the Hardware.

2. Checks for the Model Number ::

    Service Name : Hardware__Server__Hardware__Information__ServiceTag
    Status : OK, Warning, Critical
    Status Information : Service Tag of the Hardware.

