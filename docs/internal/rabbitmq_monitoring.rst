RabbitMQ Monitoring
===================
There are two strategies available for Monitoring RabbitMQ services

1. Monitoring RabbitMQ on a single node set up
2. Monitor RabbitMQ Cluster set up

RabbitMQ Version Update
=======================
RabbitMQ version will be updated from all RabbitMQ core nodes(not from the virtual host in the clustered env),
service name - **Administrative__Update__RabbitMQ__Node__Status**, this service would run once in every 6 hours.

Redis Cache
===========
The services make multiple RabbitMQ API calls during execution, which leads to making the same API calls, multiple times within a very short span of time.

In order to avoid this duplicate API calls, the caching layer got introduced with 4mns TTL (TimeToLive) as the default service frequency is 7mns.

This cache can be disabled by passing the argument --enable_cache with value as 'no'.

During retries or non OK previous service status cached data would not be used, rather the information would be fetched directly from the API.

For storing the data in the cache the key will be composed using the following convention
    * '<hostname>_rabbitmq_<subpath>' (subpath - queues,channels,connections, nodes, overview).

Note : - In the Key, there is no iValut specific differentiator.

Below are the rabbitmq services which will use cache:
    * RabbitMQ Watermark Alerts
    * RabbitMQ Disk Alerts
    * RabbitMQ Channel Object Status
    * RabbitMQ Queue length
    * RabbitMQ Connection Count
    * RabbitMQ Connection State
    * RabbitMQ Partition status
    * RabbitMQ Cluster member status

I. Monitoring RabbitMQ on a Single Node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a site if RabbitMQ set up is available on an individual node (a node with module_type as Rabbitmq), then apart from basic OS parameters, following RabbitMQ specific monitoring aspects would be added on it.

•	RabbitMQ Alive Status (HTTP)
•	RabbitMQ Heartbeat Status (HTTP)
•	RabbitMQ Watermark Alerts (HTTP)
•	RabbitMQ Disk Alerts (HTTP)
•	RabbitMQ log file status (PowerShell File size Query)
•	RabbitMQ Channel Object Status (HTTP)
•	RabbitMQ Queue length
•	RabbitMQ Connection Count
•	RabbitMQ Connection State


RabbiMQ Alive status:
    * Get request is performed to the below API, to check the aliveness of RabbitMQ node. This API call will declare a test queue, then publish and consume a message.
    * HTTP API - https://rabbitmq_host:rabbitmq_port/api/aliveness-test/%2F

RabbitMQ Heartbeat status:
    Below are the four checks which are performed to check RabbitMQ Heartbeat status.

    Check queues details:
        HTTP API - https://rabbitmq_host:rabbitmq_port/api/queues

    Delete Queue:
        HTTP API - https://rabbitmq_host:rabbitmq_port/api/queues/%2F/queue_name

    Publish message:
        HTTP API - https://rabbitmq_host:rabbitmq_port/api/exchanges/%2f/exchange_name/publish

    Get message:
        HTTP API -  https://rabbitmq_host:rabbitmq_port/api/queues/%2f/queue_name/get

RabbitMQ WaterMark Status
    * Whenever RabbitMQ consumes more than configured memory it's possible to capture RabbitMQ memory alarms using RabbitMQ nodes API.
    * This service check will provide alert if any of the RabbitMQ node memory is out of the limit provided.
    * HTTP API - https://rabbitmq_host:rabbitmq_port/nodes
    * To reproduce the memory alert scenario:
        * Check the available memory limit using - rabbitmqctl status
        * Assign the vm_memory_high_watermark less than memory limit using:
        * rabbitmqctl set_vm_memory_high_watermark  absolute <value>
        * Example : rabbitmqctl set_vm_memory_high_watermark  absolute 10

RabbitMQ Disk Status
    * Whenever RabbitMQ consumes more than configured disk it's possible to capture RabbitMQ disk alarms using RabbitMQ nodes API.
    * This service check will provide alert if any of the RabbitMQ node disk is out of the limit provided.
    * HTTP API - https://rabbitmq_host:rabbitmq_port/nodes
    * To reproduce the disk alert scenario:
        * Check the available disk space using - rabbitmqctl status
        * Assign the disk_free_limit greater than available disk space using:
        * rabbitmqctl set_disk_free_limit <value>
        * Example: rabbitmqctl set_disk_free_limit 1000000000000

RabbitMQ Channel Status:
    * Checks the number of all open channels, raise alarms based on the threshold provided.
    * HTTP API - https://rabbitmq_host:rabbitmq_port/channels

RabbitMQ log file status:
    * Check size of the RabbitMQ log file, raise alarms based on the threshold provided.

RabbitMQ Queue status:
    * HTTP API - https://rabbitmq_host:rabbitmq_port/queues
    * Verify the number of messages within, all the available Queues in RabbitMQ node, raise alarms respectively based on the threshold provided.

RabbitMQ Connection Count:
    * HTTP API - https://rabbitmq_host:rabbitmq_port/connections
    * Verify the number of connections in RabbitMQ nodes, raise alarms respectively based on the threshold provided.

RabbitMQ Connection State:
    * HTTP API - https://rabbitmq_host:rabbitmq_port/connections
    * Verify the RabbitMQ all connections state, raise CRITICAL alarms if any connection is in flow control state.

II. Monitor RabbitMQ Cluster Set Up
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When RabbitMQ implementations is identified in two or more nodes, then it will be considered as RabbitMQ cluster set up, and the monitoring follows as below,
    * A virutual host's cfg file will be generated during discovery and cluster level monitoring would be add it.
    * At the cluster level all the API calls would be made on the hostname 'rabbitmq-server.isyntax.net' by default, and it can be overwritten within the discovery endpoint
    * All of the below parameters would be monitored at the Cluster level
        * RabbitMQ Heartbeat
        * RabbitMQ Alive Status
        * RabbitMQ Channel Objects
        * RabbitMQ Queue length
        * RabbitMQ Disk
        * RabbitMQ WaterMartk
        * RabbitMQ Connection Count
        * RabbitMQ Connection State
        * RabbitMQ Partition status
        * RabbitMQ Cluster member status

RabbitMQ Partition Status:
    * HTTP API - https://rabbitmq_host:rabbitmq_port/nodes
    * Check for partition details in all the available nodes. And if partition reported it will raise CRITICAL alert.

On the RabbitMQ node (members of the Cluster) the following things will be monitored.
    * Basic OS parameters (CPU, RAM, IO etc)
    * RabbitMQ Log file status

RabbitMQ Cluster member status
    A new Cluster member status service check along with partitioning would be added at the cluster level,

    * One or more nodes are out of cluster
        * This is been accomplished by using the RabbitMQ overview API. In the overview API response there are two attributes context and listener. When a node is out of cluster context and listener attributes would mismatch. With a comparison of these two attributes its possible to identify the node which is out of a network.
        * To make node out of cluster following commands are used:
            * Rabbitmqctl stop_app
            * Rabbitmqctl reset
            * Rabbitmqctl start_app

    * Out of network
        * One or more nodes are not reachable over network
        * When a node is not reachable over network, the RabbitMQ nodes API provides the status as false, also if any node is out of cluster its information will not be available within api/nodes.

    * Partitioning
        * In case of partition, inside the listener attribute of api/overview, all node details will not be available for protocol clustering.
        * But context attribute of api/overview, all node name will be available.
        * Difference of context attribute and listener attribute will provide us information that which all nodes are not part of cluster, that is because of network partitioning.
