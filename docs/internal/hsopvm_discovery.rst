
========================================
HSOP VMs Discovery Overview
========================================

----------
Overview
----------

HSOP VMs discovery discovers the VMs on the Stratoscale Instance or site.
Stratoscale provides REST API to discover all the VMs currently spawned on the Stratoscale instance.The API also provides other
information about the VMs. From the API, address,VM ID etc are retrieved and converted to the format of facts. 
This information will be stored in the facts collection database.


-----------------------------------------------------------------------------------
Example of Stratoscale VM Discovery response
-----------------------------------------------------------------------------------

The following example shows the snap of VMs information from the Stratoscale API.

Example: ::

    [
        {
            "disable_delete": false,
            "imageId": "3967a755-7ea6-4fb5-bde4-c585afabf099",
            "id": "a923f8ae-a211-4279-96dd-00b5acefecd8",
            "user_id": "admin",
            "key_map": "en-us",
            "slaProfile": "on demand",
            "hostname": "stratonode3.node.strato",
            "restart_on_failure": false,
            "networks": [
                {
                    "port_id": "158d2c23-bf67-4195-ae0a-5f85a78fc676",
                    "id": "c4d23a7a-e903-4035-804c-da3df64ddf2f",
                    "address": "10.11.14.28"
                }
            ],
            "key_pair": null,
            "metadata": {},
            "status": "active",
            "updated": "2018-07-11T06:07:54Z",
            "tags": [],
            "diskGB": 100.0,
            "hw_firmware_type": "bios",
            "bootVolume": "b9952237-92c4-413d-aaab-f060fe11cc2b",
            "name": "SQL for Stratoscale",
            "created": "2018-07-11T06:03:02Z",
            "tenantId": "d134786daf55448d8c02b97601aa38e8",
            "vcpus": 4,
            "volumes": [],
            "ramMB": 8192,
            "vpc_id": null,
            "instanceType": "c5.xlarge"
        },
        {
            "disable_delete": false,
            "imageId": "a904f979-5a8a-48db-be22-73819c45f25d",
            "id": "0cad0509-230e-4d4c-951a-aaf8dd308f05",
            "user_id": "7c26ce439bfc4521bd9afdf9db3a5096",
            "key_map": "en-us",
            "slaProfile": "on demand",
            "hostname": "stratonode0.node.strato",
            "restart_on_failure": false,
            "networks": [
                {
                    "port_id": "a0867266-ceee-4bf4-a2de-1698e594ec16",
                    "id": "c4d23a7a-e903-4035-804c-da3df64ddf2f",
                    "address": "10.11.14.30"
                }
            ],
            "key_pair": null,
            "metadata": {},
            "status": "active",
            "updated": "2018-07-10T11:58:22Z",
            "tags": [],
            "diskGB": 60.0,
            "hw_firmware_type": "bios",
            "bootVolume": "ce5b1c1f-8105-4607-9742-4e6be68de971",
            "name": "IDM-VIGILANT",
            "created": "2018-07-10T11:50:07Z",
            "tenantId": "d134786daf55448d8c02b97601aa38e8",
            "vcpus": 2,
            "volumes": [
                "bf9af9c5-91b9-40c3-a226-1e3f4cb780b2"
            ],
            "ramMB": 4096,
            "vpc_id": null,
            "instanceType": "__strato_2CPUS_4096ramMb_d134786daf55448d8c02b97601aa38e8"
        }
    ]

The following example shows the facts marshalled from the response of the Stratoscale API.

    {
        "facts": {
            "10.11.14.28": {
                "product_id": "HSOP",
                "product_version": "1,0,0",
                "modules": [
                    "linux",
                    "HsopVMProductScanner"
                ],
                "address": "10.11.14.28",
                "HsopVMProductScanner": {
                    "endpoint": {
                        "scanner": "HsopVMProductScanner",
                        "address": "130.147.86.215"
                    }
                },
                "product_name": "HSOP",
                "uuid": "a923f8ae-a211-4279-96dd-00b5acefecd8"
            },
            "10.11.14.30": {
                "product_id": "HSOP",
                "product_version": "1,0,0",
                "modules": [
                    "linux",
                    "HsopVMProductScanner"
                ],
                "address": "10.11.14.30",
                "HsopVMProductScanner": {
                    "endpoint": {
                        "scanner": "HsopVMProductScanner",
                        "address": "130.147.86.215"
                    }
                },
                "product_name": "HSOP",
                "uuid": "0cad0509-230e-4d4c-951a-aaf8dd308f05"
            }
        }
    }

The following example shows the cfg file generated from one of the facts for a VM

    define host {
    host_name                      10.11.14.30
    use                            stratoscale-service
    _discovery_components          [{"version": "1.0.0", "name": "HSOP"}]
    _discovery_modules             ["linux", "HSOP"]
    _vm_id                         0cad0509-230e-4d4c-951a-aaf8dd308f05
    address                        130.147.86.215
    alias                          10.11.14.30
    hostgroups                     +stratoscale-vms
    }




