..  _dbconfigcheck:

============================
Database Configuration Check
============================

Overview
--------

The Database Configuration Script is made of two sections, information only and missing configurations.

**Information Only** - Returns the configuration value to the IDM pipeline and further analysis need to be done on this
data to determine if these values are appropriately set for that customer profile.

**Missing Configurations** - Are critical errors that need to be addressed to ensure stability of the Database or
Mirroring Solution.

Deployment
----------

Database configuration check can be deployed either by manually copying the script file to ``S:\philips\common\scripts``
or by pushing it through the IDM pipeline.

Execution
---------

The script should be triggered from a job scheduler, the IP address of the endpoint shall be provided as the parameter. 

Example
-------

Run the script with command below ::

    S:\philips\common\scripts\DBConfigCheck.ps1 -EndPointIP=[IP Address of IDM Endpoint]

Frequency
---------

The script should be executed once every night.

Return Code
-----------

0. OK
1. WARNING
2. CRITICAL

Output
------

The script gathers following data

#. Checks for the backup Jobs ::

    Service Name : Administrative__Philips__Database__Backup__CheckJob
    Status : OK, Warning, Critical
    Status Information : name of the Job not found.

#. Checks for the Job History ::

    Service Name : Administrative__Philips__Database__Information__CheckDBAdminJobHistory
    Status : OK, Warning, Critical
    Status Information : Last 5 errors.

#. Checks for Error Logs ::

    Service Name : Administrative__Philips__Database__Information__ErrorLogs
    Status : OK, Warning, Critical
    Status Information : Last recorded error.

#. Get Database Configuration ::

    Service Name : Administrative__Philips__Database__Information__GetConfig
    Status : OK
    Status Information : Returns the database configuration value records. The name of the configuration, Minimum Value, Maximum Value, the Configuration Value and the Run Value.

#. Gets the count of tempdb files ::

    Service Name : Administrative__Philips__Database__Information__TempDBFileCount
    Status : OK
    Status Information : Returns the number of data files tempdb has.

#. Checks the existence of Mirroring Alert ::

    Service Name : Administrative__Philips__Database__Mirroring__CheckAlert
    Status : OK, Warning, Critical
    Status Information : Returns an error if mirroring alert is not created on database node.

#. Checks for missing mirroring files ::

    Service Name : Administrative__Philips__Database__Mirroring__CheckFile
    Status : OK, Warning, Critical
    Status Information : Returns an error if one or more mirroring files are missing.

#. Checks for Mirroring Jobs ::

    Service Name : Administrative__Philips__Database__Mirroring__CheckJob
    Status : OK, Warning, Critical
    Status Information : Returns an error if mirroring job is missing.

#. Checks for Mirroring Status ::

    Service Name : Administrative__Philips__Database__Mirroring__CheckStatus
    Status : OK
    Status Information : Returns the Mirroring Status of each and every user database.

#. Checks is stentorkeylogin exist ::

    Service Name : Administrative__Philips__Database__Mirroring__CheckStentorKeyLogin
    Status : OK, Warning, Critical
    Status Information : Returns an error if statentorkey login does not exist on database node.

#. Checks for the WFL Databases ::

    Service Name : Administrative__Philips__Database__Mirroring__CheckWFLStatus
    Status : OK
    Status Information : Returns information on whether WFL databases exist on Database node.

#. Checks for DNS Entries ::

    Service Name : Administrative__Philips__Host__Information__CheckDNS
    Status : OK, Warning, Critical
    Status Information : Returns an error if DNS entry for primary and mirror NIC does not exist.

#. Checks the power plan of VM ::

    Service Name : Administrative__Philips__Host__Information__CheckPowerPlan
    Status : OK, Warning, Critical
    Status Information : Returns an error if the power plan of VM is not set to High Performance.

#. Checks if the host is up ::

    Service Name : Administrative__Philips__Host__Information__IPAddress
    Status : OK, Warning, Critical
    Status Information : returns the status of host.
