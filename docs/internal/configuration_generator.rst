=======================
Configuration Generator
=======================

Overview
--------

Configuration generator interprets data found using the discovery process and produces configuration which is stored in
 the configuration repository intended to provide install base configuration to IDM on site nodes.

Generators
----------

Configuration generators return a dictionary, with the keys being the configuration file names and the value the content
 that should be present in the file.

For example: ::

    {
        'hosts/host1.cfg': """define host{
    use            some-nodes
    host_name      samenode.example.com
    address        192.168.0.214
    }"""
    }

Writer
------

The writer simple takes a dictionary as generators produce and produces the actual files in the containers specified.
For example SVN.
