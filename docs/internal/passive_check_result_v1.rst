========================
Passive Check Results v1
========================
Overview
--------
The passive check result v1 is an extension of passive check result with supporting basic authentication.


Endpoint
--------
The url:
https://<neb node address>/check_result/v1

The expected Content-Type: application/json

The user name and password required to supplied as part of basic auth is passive_user/1nf0M@t1cs

Examples
--------
An example of using a curl POST command using basic auth and certificate::

    curl  -XPOST --cacert /etc/nginx/ssl/server.crt -H "Content-Type: application/json"
    --data '{"service": "Product__ISEE __IBE__ISP_ComPoints__Status", "return_code": "2",
    "output": "passive submission", "host": "IBEISEEAPTST1.ISEE02.BOS02.iSyntax.net" }'
     https://<neb node fqdn name>/check_result/v1 -u passive_user:'1nf0M@t1cs'


An example of using a curl POST command using basic auth and with out certificate::

    {
     curl  -XPOST -H "Content-Type: application/json"
    --data '{"service": "Product__ISEE __IBE__ISP_ComPoints__Status", "return_code": "2",
    "output": "passive submission", "host": "IBEISEEAPTST1.ISEE02.BOS02.iSyntax.net" }'
     https://<neb node address>/check_result/v1 -u passive_user:'1nf0M@t1cs' -k
    }

