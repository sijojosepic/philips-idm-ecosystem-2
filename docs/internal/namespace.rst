-----------------
Message Namespace
-----------------
Messages carry information about services provided by a host, messages are to explicitly state the identity of the host.
A namespace will aid in the identification and correlation of pertinent service information.

The intent is to have an easy to follow guideline for the name of items that are being referenced in messages, logically
grouping them by their defining factors. These items represent the services or parts of a particular host. The host may
be a server, a networking appliance, a virtual server, a cluster or any entity individually identifiable.

A hierarchical namespace with the following definition has been chosen:

<Category>__<Vendor>__<Object>__<Attribute>__<Dimension>

Specific containers are not predetermined and are expected to change as required.

Container names may have spaces, special characters should be avoided, and colons should not be used as they have
special interpretation.

The first container "Category" defines a very wide grouping of items, the categories considered when defining the
namespace are the high level major components of the infrastructure: Storage, Server, Virtualization, Network, Product,
OS, Software and Database.

The number of Categories is not expected to grow by much, some may be split or redefined as the need arises.

The "Vendor" container groups by the company or entity associated with item that is to be referenced. Segregating by
vendor seems sensible as an escalation path or expertise may be refined from identifying issues or messages relating to
a particular vendor. For example, items with a vendor "IBM" may be forwarded to a group that works with issues with that
vendor. As another example, items with vendor "Cisco" may be forwarded to network specialists.

The first tangible component is the "Object", which is intended to be an instance of a type of host, as an example a
particular model of a switch or a disk array.

The "Attribute" is the specific aspect of the "Object" an item refers to.

The "Dimension" describes what is being measured of the "Attribute".

An example of a name referring to the CPU Utilization of an ESX host would be:

Virtualization__VMware__ESX__CPU__Utilization

A special case for information that may only be associated with the "Object" in general may be used by creating an
"Information" Attribute with the piece of information name as the "Dimension". For example the version number the
anywhere viewer may be found using the name:

Product__IntelliSpace__Anywhere__Information__Version

The following is a tree view of some of the identified components as they would be broken down within the namespace.

#. Storage

   #. IBM

      #. V7000

         #. RAID

            #. Status
            #. <Key>

         #. Disk

            #. Vendor
            #. MFR Date
            #. Capacity

         #. Volume

            #. Status

         #. Canister

            #. Status

         #. Enclosure

            #. Health
            #. Battery
            #. PSU

   #. DELL

      #. Equallogic

         #. Disk

            #. Status
            #. Usage

         #. Overall

            #. Health

         #. RAID

            #. Status

         #. Ethernet Interface

            #. Status

         #. PS

            #. Status

#. Product

   #. IntelliSpace

      #. WFL

         #. Critical Services

            #. Status

         #. Event Log

            #. Errors

         #. Daemon

            #. Status

         #. Information

            #. Version

      #. Anywhere

         #. Information

            #. Version

      #. iSite

         #. iSyntaxServer

            #. Status

      #. HeartBeat

         #. Logon

            #. Status

      #. Process

         #. <Process Name>

            #. Status

            #. WorkingSet

      #. MSMQ

         #. MSG Count

      #. S:\Stentor\Input

         #. File Count

      #. Node

         #. Information

            #. Version

#. OS

   #. Microsoft

      #. Windows

         #. CPU

            #. Utilization

         #. Disk Space

            #. Total Utilization
            #. Total Free
            #. Total Used
            #. Total Capacity
            #. C: Utilization
            #. C: Free
            #. C: Used
            #. C: Capacity
            #. S: Utilization
            #. S: Free
            #. S: Used
            #. S: Capacity

         #. Memory

            #. Utilization

         #. Network

            #. Utilization
            #. Number of Interfaces
            #. Interface 1 IP Address
            #. Interface 1 Speed
            #. Interface 1 Packets Received Errors
            #. Interface 1 Send Rate

         #. Information

            #. Version

   #. CentOS

      #. Linux

         #. CPU

            #. Utilization

         #. Disk Space

            #. Utilization

         #. Memory

            #. Utilization

         #. Network

            #. Utilization

         #. Information

            #. Version
