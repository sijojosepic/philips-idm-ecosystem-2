UDM Monitoring based on Configuration
=====================================

For below UDM processes enable and disable monitoring based on the configuration available under "UDMServices/Configuration/UDMPROCESSANDSERVICES".

Process list:
    * MessagingFramework.ReplayManager.EXE
    * Prefetch.ManagerLauncher.EXE
    * Prefetch.MessageTransformLauncher.EXE
    * Philips.CILM.DataService.Host.EXE
    * Philips.CILM.Prefetch.Scheduler.Host.EXE
    * ImagingService.Host.exe
    * RuleEngine.jar

These process monitoring will be available only for below nodes:
    * UDMAppServer - 1024
    * UDMStorageServer - 512
    * DICOMProcessing - 2
    * EID - 3


High Level Workflow
-------------------

Monitor various UDM processes based on the configuration available under the - "UDMServices/Configuration/UDMPROCESSANDSERVICES".

Here with each process, there will be a hostgroup associated with it. While reading the process configuration
available in "UDMServices/Configuration/UDMPROCESSANDSERVICES" for UDMAppServer, UDMStorageServer, DICOMProcessing and EID nodes.
If the process state is 1(enabled), then the hostgroup will be added to the host's cfg file.
Which in turn make the service check to be part of that particular hosts.









