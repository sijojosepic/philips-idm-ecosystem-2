ISP_Config Overview
===================

The isp_config file contains all the classes and functions which is responsible for reading the process configuration
available in the iSite Registry under 'iSyntaxServer\\--' and provides the details on module type, federation
configuration, DICOMInputDirectory, software version and custom list of processes.

High Level Workflow
-------------------

Monitor various windows processes based on the configuration available under the - iSyntaxServer\ProcessesAndServices

Process List:

1. Audit Service

2. iSite Monitor

3. Storage service.

4. Heartbeat

5. iExportApp

6. DmwlApp

7. iArchive

8. DICOMService

9. StudyRouter

10. NotificationService

11. StudyMigration

Here with each process, there will be a hostgroup associated with it. While reading the process configuration
available in the iSite Registry under 'iSyntaxServer\\ProcessesAndServices' for every node, if the process status is 1
(enabled), then the hostgroup will be added to the host's cfg file.
Which in turn make the service check to be part of that particular hosts.

Components
----------

1. ISPAuthenticationError: Pass if any AuthenticationError occurs.

2. ISPConfiguration: Derived class from HTTPRequester responsible for various 'iSyntaxServer\\ -"  iSite Registry API calls and reading Module Type, Federation configuration, DICOMInputDirectory, Software Version and Custom list of Processes which needs to be enabled.

3. ISPHostConfiguration: Reading, decoding and objectify xml data.








