========
Shortway
========
Overview
--------
Shortway is a replacement for gateway, to be used in very specific situations, like when monitoring backoffice
components where sending messages about failed components to the failed component would not be sensible.

Shortway provides means to forward messages to an http endpoint, or via email.

Installation
------------

The way to setup a shortway node is to first create the node as a gateway node, then
remove phim-gateway and install phim-shortway::

    yum remove phim-gateway
    yum install phim-shortway

Initial, configuration can be found in the source control archive for IDM under Vigilant/configuration/shortway/

Create the directory for notification templates::

    mkdir -p /etc/philips/notification_templates/

Place email.j2 and subject.j2 there

Place contact_rules.conf and notification_contacts.conf in /etc/philips/


Configuration
-------------

The files email.j2 and subject.j2 in notification_templates folder are Jinja2 templates used when an email notification
is to be sent.

Variables available in email.j2 and subject.j2 are:
- service
- siteid
- type
- state
- timestamp
- payload

These files may be edited to the desired format.

The file contact_rules.conf contains a JSON object representing the rules to be used for the decider object used,
decider is described in the phimutils_deceder documentation.

The initial configuration looks like: ::

     { "rules":
        [
            {
                "sites": [],
                "namespaces": [],
                "result": ["backoffice"]
            }
        ]
    }

Rules may be added there as needed.

The notification_contacts.conf file contains a JSON object describing the contacts to be used.

There are two types of actions to be used for contact currently. "http" will post whatever was posted into shortway to
the address provided. "email" will send an email.

The initial configuration for contacts is defined as ::

    {
        "backoffice": {
            "address": "http://vigilant/notification",
            "action": "http"
        }
    }

More contacts can be added, an aditional field "types" may be added as a list of notification types to contact on, if
this is not defined then contact gets all notification types.

An example of an aditional contact that is only to be reached on "PROBLEM" and "RECOVERY" types: ::

    {
        "backoffice": {
            "address": "http://vigilant/notification",
            "action": "http"
        },
        "hb_email": {
            "address": "hbeat@stentor.com",
            "action": "email",
            "types": ["PROBLEM", "RECOVERY"]
        }
    }


Configuration on for the properties of the email server to use and such are to be made in
/usr/lib/philips/uwsgi/swconfig.py

Changes
-------

uwsgi service must be restarted for changes to take effect::

    service uwsgi restart
