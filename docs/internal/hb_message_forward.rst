========================
Hertbeat Message Forward
========================
Overview
--------

IDM Backoffice worker provides means to forward messages to Hertbeat, which requires messages be sent in XML format via
SMTP.

Site Configuration
------------------
Messages may be sent to different addresses based on configuration.
Sites not mapped will be sent to the default address.

If the file is not present, all will be sent to default.

The file '/etc/philips/emailmap.conf' containing a JSON object like the following: ::

     {
        "xyz01": "some.address@philips.com",
        "leo00": ""
     }

The key is the siteid as configured in the neb node for the site, the value is the address to send the email to. If the
value is empty, no email will be sent.

Message Configuration
---------------------
Create the file '/etc/philips/hb_code_map.conf' containing a JSON object like the following: ::

    {
        "Product__IDM__Nebuchadnezzar__Alive__Status": {
            "error_type": "CE_monitoring_lost_contact",
            "error_code": "9116060101"
        },
        "Product__IntelliSpace__PACS__iSyntaxServer__Aggregate": {
            "error_type": "iSyntaxServer_restart_exhausted",
            "error_code": "9116060102",
            "types": ["PROBLEM", "RECOVERY"]
        }
    }



Each key in the object is an object with 3 fields, the key must be the service name as configured in Shinken:

error_type
    The type to place in the Heartbeat message

error_code
    The error code for the heartbeat message

types
    A list of the type of notification messages to forward, if not present, all types will be forwarded.

Changes
-------
Celery must be restarted for changes to take effect.
