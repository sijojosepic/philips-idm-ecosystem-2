--------------------
Dashboard Collection
--------------------

The dashboard collection provides a centralized place to access for all items required to be presented through the GUI.

Each entry in the dashboard collection consists of a service in a specific host in a specific site, that is the
site-host-service combination is unique. An entry present in the dashboard collection represents a service in a PROBLEM
state, once a service present in the collection returns to an OK state it will be removed.

Entries in this collection consist of the same data as notifications currently contain, with additional data required
for it to be presented without additional queries.

The data is to be inserted whenever a PROBLEM message is received, at this point the additional relevant information
found from other collections will be inserted, whatever is needed and not found will be sent to a separate collection to
give the opportunity for it to be populated, a default value will be set for these non found data. Additional data for
case number and status may be inserted at any point after the dashboard entry has been created.

A separate mean to scan for incoming state data in case recovery messages may have been missed is to be defined. This
will find states for services in the dashboard collection, if the state is OK, a recovery notification will be issued.

A way to determine if dashboard entries are no longer valid by fact of the relevant node being removed or upgraded
making the entry obsolete is to be determined.

Data fields in notification:

- state/status
- service
- timestamp
- hostname
- perfdata
- siteid
- hostaddress
- output
- type
- namespace

Data fields to be included from the Master Site Collection:

- Site Name
- Site Version

Data fields to be populated by Portal (not inserted on event):

- Case Number (OneEMS)

Data fields computed from data in the collection:

- unreachable (boolean indicating that the service is unreachable due to site or host being unreachable)

Possible document structure: ::

    {
        "_id": "<siteid>-<host>-<service>",
        "state": "...",
        "service": "Example__Vendor1__Device_type1__Memory__Status",
        "timestamp": "...",
        "hostname": "...",
        "perfdata": [...],
        "siteid": "...",
        "hostaddress": "...",
        "output": "...",
        "type": "...",
        "site": {
            "name": "...",
            "version": "...",
        },
        "namespace": {
            "category": "Example",
            "vendor": "Vendor1",
            "object": "Device_type1",
            "attribute": "Memory",
            "dimension": "Status"
        }
        "case_number": "...",
        "unreachable": true
    }


.. graphviz::

   digraph dashboard_collection {
        node[shape=box]
        Notification [style=rounded]
        "Case Info" [style="rounded, dashed"]
        node[shape=box3d]
        Events
        Dashboard

        subgraph cluster_pull_collections {
            style = filled
            color = lightgrey
            "Master Code"
            "Master Site"
            label = "Pulled From Collections"
        }

        {"Master Code" "Master Site"} -> Dashboard [style=dashed]
        Notification -> {Dashboard Events}
        "Case Info" -> Dashboard [style=dashed]
   }

