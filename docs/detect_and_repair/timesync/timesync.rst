..  _timesync:

==========================
TimeSync Detect and Repair 
==========================

Overview
--------

IDM has introduced a detect and repair functionality for solving the timesync discrepancy on ISPACS nodes version above and equal 4.4. The timesync functionality is achieved using a powershell script shared by CE Team from R&D.


Script's functionality

1. Make sure the Windows Time service running
2. Windows Time Service start type is Automatic 
3. Disable the synchronize guest time with host option
4. Changes the Windows Time service Registry entries as below

    ON Domain Contorller node
     type :NTP

     NTTPSERVER - value provided to the script during execution

    on other nodes
      type: NT5DS

      NTTPSERVER: fqdn of the iVault
5. Push the execution status to the status service 

Implementation
--------------

The Timesync detect & repair is applicable only for ISPACS versions equal and above 4.4.
implementation is done with two shinken service checks, as the script takes up to 5mns for execution(Default timeout for a shinken service check is 1mns)
Variable named NTP_SERVERS can be set from the site's lhost.yml file,

Shinken Services
----------------

1. Trigger service (Product__IntelliSpace__TimeSync__Service__Trigger)
Trigger service is an informational service, its responsiblity is to submits the powershell script, as a fire and forget powershell job, then the script executes at the target node.
push the status back to the status service

2. Status Service (Product__IntelliSpace__TimeSync__Service__Status)
its a Shinken passive service, which accepts the status of script execution and then reflect the same accordinly on the IDM dashboard
typical payload format

{
    "service": "Product__IntelliSpace__TimeSync__Service__Status",

    "return_code": 0,

    "output": "{'message': 'The system is found to be not in desired state. Fix applied and recheck passed','DateTime': '04/17/2020 03:06:12','TimeZome': 'Coordinated Universal Time','NTPSource': 'IDM01.ISYNTAX.NET,0x1','TriggerId': 'bcb427b2-8084-11ea-b656-0050568db0bf'}",

    "host": "idm01if1.idm01.isyntax.net"

}


configuration
-------------
The frequency of the service check - 12 hours

Retry interval - 2 minutes

Retry attempts - 3



Plugin
------
Name : check_isp_timesync.py

The plugin will be invoked from a trigger service which will be an informational service. This trigger service will submit the power shell script to the targeted node.

Plugin uses WinRM to submit time sync script using Powershell's  fire and forget capability, 
In the PSscript there is 5mns sleep is provided, which is requried to keep the job alive, even after WinRM session expiry.
Otherwise the job seems to be stoppend along with winRM Time out.

The status message of the plugin will contain a trigger_id which will be matched with the trigger_id of the script response to status service.

The Plugin will incorporate the status service URL(passive service URL) with the power shell script upon execution. For that it needs to retrieve the NEB node IP address. Upon the failure for retrieval of NEB node IP it will send the script for execution on the respective node without the passive service URL and the status service will not receive any updates from the power shell script, but the script will achieve its functionality. In this case the trigger service will give as a Warning status with the appropriate message.

The NTP_SERVERS list is provided in shinken thresholds which will be received as an argument to the plugin upon trigger service execution. By default the NTP_SERVERS list is kept empty and then plugin status output in this condition will be OK.


Plugin Argument
---------------

The plugin expect IP address, User Name, Password and NTP_SERVERS as an arguments upon execution. NTP_SERVERS list will be configured in shinken resources under threshold section which can be provided from lhost.yml.


Plugin Output
-------------

| The Trigger service outputs the following data

1. OK Status ::
    OK : Submitted the Powershell script with trigger_id : <trigger_id>

    set NTP_SERVERS and try again
    
2. WARNING Status ::
    Unable to retrive Neb IP, Submitted the Powershell script with trigger_id : <trigger_id>

| The Status service outputs the following data

1. OK Status ::
    {'message': 'The system is found to be/not in desired state. Fix applied and recheck passed','DateTime': '<date>','TimeZome': 'Coordinated Universal Time','NTPSource': '<ntp_servers>,0x1','TriggerId': '<trigger_id>'}
    
2. CRITICAL Status ::
    {'message': 'Unknown error occurred','DateTime': '<date>','TimeZome': 'Coordinated Universal Time','NTPSource': '<ntp_servers>,0x1','TriggerId': '<trigger_id>'}
    

Example
-------

Plugin execution ::

    check_isp_timesync.py -H 'IP Address' -U 'username' -P 'password' -a '["NTP1", "NTP2"]'

Ops Activity
------------

Providing the NTP_SERVERS value from lhost.yml follows a particular format.

From the NG Portal while providing the threshold values per site, the NTP_SERVERS should be filled like : '["NTP1","NTP1"]'

The shinken resourse value while providing lhost for 2008 machines has to be updated according to the domain name of the environment.

This is necessary because script execution will fail in 2008 nodes if domain name is not given as prefix along with user while executing it.

i.e $USER5$ and $USER150$ has to be updated along with the domain name as a prefix.

eg: $USER5$ - '<domain name>\<username>'