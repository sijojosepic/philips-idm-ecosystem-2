===========================
Setup a Repository instance
===========================

#. Deploy a Vigilant appliance
#. Add an additional hard disk for the /repo mount point - currently a 500GB Thin provisioned disk is used.
#. Turn off all service for now ::

    chkconfig celerybeat off
    chkconfig celeryd off
    chkconfig elasticsearch off
    chkconfig httpd off
    chkconfig mongod off
    chkconfig mysqld off
    chkconfig nagios off
    chkconfig nginx off
    chkconfig postfix off
    chkconfig rabbitmq-server off
    chkconfig redis off
    chkconfig shinken-arbiter off
    chkconfig shinken-broker off
    chkconfig shinken-poller off
    chkconfig shinken-reactionner off
    chkconfig shinken-receiver off
    chkconfig shinken-scheduler off
    chkconfig squid off
    chkconfig supervisord off
    chkconfig thruk off
    chkconfig uwsgi off
    chkconfig webmin off

#. Remove the proxy.sh file ::

    rm /etc/profile.d/proxy.sh

#. Correct the host file to point to the master internal repository or edit the resolv.conf  for a DNS to use the
   internet respositories ::

    167.81.182.65    repo.phim.isyntax.net
    167.81.182.65    phirepo.phim.isyntax.net
    167.81.182.65    pypi.phim.isyntax.net

#. Issue the following commands to install the necessary software and setup the additional hard disk from above. ::

    yum install createrepo
    fdisk /dev/sdc
    mkfs.ext4 /dev/sdc1
    mkdir -pv /repo
    blkid /dev/sdc1
    echo "UUID={from above output} /repo ext4 defaults 1 2" >> /etc/fstab
    mount -av

#. Begin the rsync of the repo contents ::

    rsync -avzh -e ssh root@167.81.182.65:/repo /

# TODO pypi-server install and supervisord information
# TODO downloading other pypi packages to the new repository
