=====================
IDM lhost.yml configuration
=====================

All sites in IDM has a **lhost.yml** configuration file, site specific configurations goes in to this file.

There are mainly 4 configurable section in the lhost.yml file.
***************************************************************

#. **DNS Name Servers**

	DNS server  details.

#. **Endpoints**

     Under this section the details about a site can be provided.

	**address** : IP address

	**scanner** : Product Scanner name 

		eg : IBE, ISP etc

	**username** : username

	**password** : password

	**monitor** : This flag is used to enable(true) or disable(false) monitoring for a particular site
	
			::

			   true : to Enable  Monitoring(default)
			   false : to Disable Monitoring

			If we set this flag to false, then during discovery *<hostname>.cfg* file would not be get created in the **SVN**, without this file monitoring may not be possible for any site.

	**discovery_url** : Discovery URL only when  generic Discovery is used ( See Generic discovery for  more details)


	**product_name**:  Name of the product, which will be part of discovery payload and will be used to display in the IDM Dashboard

	**product_id**: ID of the product, which will be part of discovery payload and will be used to display in the IDM Dashboard

	**product_version**: Product Version

       In the **lhost.yml** or **discovery.yml** files the resource values are case insensitive in nature.
        
        Example::  All below cases are valid::      
	

		-   address: MASTER.IDM10.iSyntax.net
                    dbpassword: Nsah2damsh@60s!
                    dbuser: phisqlmonitor
                    password: [$User61$, $user63$]
                    scanner: ISP
                    username: [$USER61$, $USER63$]
                    abc: {$resource: $USER24$}
                    efg: {$resource: $User26$}
                    xyz: {$resource: $user20$}

    **But in resource.cfg or while updating new values for shinken_resources through lhost.yml, only CAPITAL letters can be used.**
    That is, ::
    			USER143:  - valid
    			user56: - invalid
    			User56: - invalid 


#. **Shinken Resources**


	These are site-specific resources generally in an encoded format used to supply set of variables to Shinken.

	Thease values will be written to Shinken's `resource.cfg` file,
        


#. **Threshold values** 

	Under this section its possible to configure WARN and CRIT conditions for various service checks (see Threshold configuration section for more details)

#. **An Example lhost.yml file**


.. image::  images/lhost_yml.png
    :align: center
    :alt: lhost.yml
