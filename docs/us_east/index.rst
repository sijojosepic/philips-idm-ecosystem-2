US EAST Node Deployment
***********************

========
Strategy
========

To deploy a neb against US East backoffice all which is on open Internet, one must use fqdn instead of IP to configure 
Neb. FQDN currently used is managed by HSDP and shall not have static IP since its behind HSDP Load balancer.
FQDN purely depends on space being used and one shall contact DevOps for name.

=======================
Initialization changes:
=======================

Since we no longer dependent on IP (-e argument) and its completely dependent on hostname, we have introduced 3 new variables namely -f (fqdn),-p (proxy), -pp (proxy port).

Proxy and its port are not mandatory but its required to deploy in PGN Environment. However in real world scenario Neb will communicate against FQDN directly without having to use proxy.

One must note we should always use secure flag while deploying since communication is on open internet.

**Ex script:**

*python initialization.py ** -f idmcftestlb.cloud.pcftest.com -p 165.225.96.34 -pp 9480 ** -n 255.255.255.0 -g 192.168.59.1 -d 1.1.1.1 -s EST01 -i 192.168.59.165 -S*


Code Changes Required
=====================

In our ansible scripts and in plugins we have hardcoded various endpoints like vigilant,repo.phim.isyntax.net,sc.phim.isyntax.net,persephone.phim.isyntax.net,api.phim.isyntax.net etc..

1) So we can go to individual files and change do replace of this hostnames with fqdn and sometimes we might need to restart certain services.
2) Other way is to use on-premise Neb LB as a reverse proxy.

Following are the changes for latter to work,
1) Persist FQDN (/etc/fqdn),Proxy(/etc/proxy) value in Neb just like siteid (/etc/siteid).
2) First playbook to be run during ansible-pull is to run playbooks related to nginx.
3) SVN uses its own proxy,the path for proxy is at '/root/.subversion/servers'.SVN doesnt respect system proxy .


Pending Work
============

- Verifying against HTTPS FQDN, since all the requests from HSDP LB is forwarded only on port 80 and many more reasons.
- Minor Code cleanup required
- Verifying without having to use proxy,we don't have environment todo this.