..  _apache_spark:

============================
Apache Spark RPM Information
============================

Engineering specs
-----------------

Pre-req ``oracle jre 1.7``

::

    Let us have Spark structure as below. I will copy the init.d scripts from Cloudera and send you back

    Spark entirely in /usr/lib/spark
    Conf moved to /etc/spark/conf
    Work at /var/run/spark/work
    Logs at /var/log/spark

    Pids at /var/run/spark

    Please add the following lines also to spark-env.sh
    export SPARK_LIBRARY_PATH=${SPARK_HOME}/lib
    export SCALA_LIBRARY_PATH=${SPARK_HOME}/lib
    export SPARK_MASTER_WEBUI_PORT=18080
    export SPARK_MASTER_PORT=7077
    export SPARK_WORKER_PORT=7078
    export SPARK_WORKER_WEBUI_PORT=18081
    export SPARK_WORKER_DIR=/var/run/spark/work
    export SPARK_LOG_DIR=/var/log/spark
    export SPARK_HISTORY_SERVER_LOG_DIR='/user/spark/applicationHistory'
    export SPARK_HISTORY_OPTS="${SPARK_HISTORY_OPTS} -Dspark.history.ui.port=18088 -Dspark.history.fs.logDirectory=${SPARK_HISTORY_SERVER_LOG_DIR}"
    export SPARK_PID_DIR='/var/run/spark/'

    if [ -n "$HADOOP_HOME" ]; then
      export SPARK_LIBRARY_PATH=$SPARK_LIBRARY_PATH:${HADOOP_HOME}/lib/native
    fi

    export HADOOP_CONF_DIR=/etc/hadoop/conf


