..  _concerto:


===========================
F5 LB Dicom Pool Management
===========================


I Overview
##########

A need for disabling and enabling the Dicom (Proc) node in F5 load balancer has been determined as a simple workaround.
The enable/disable state of Proc node from the LB is to be managed when state of the Shinken monitoring service for ‘input folder age’ and ‘input file size’ is changed.
This document describes the components that are part of the solution. The solution utilizes the existing monitoring framework of IDM. The communication from NEB to the LB is over iControl REST API service layer provided by F5. No new component is to be installed to modify the state of LB.


II Problem Detection
####################
Identifying the cause is achieved by checking the Shinken state of the service Product__IntelliSpace__PACS__Input_Directory__Aggregate.
The above mentioned service checks the following shinken service states::

       1.  Product__IntelliSpace__PACS__input_Directory__File_Age
       2.  Product__IntelliSpace__PACS__input_Directory__File_Count

When the state of both services turns CRITICAL, the Product__IntelliSpace__PACS__Input_Directory__Aggregate service will also turn CRITICAL.
When the state of both services turns OK from CRITICAL, the Product__IntelliSpace__PACS__Input_Directory__Aggregate service will also turn OK



III Detection Aggregation
#########################

An aggregate check is configured to consider the status of the ‘base’ checks. It is on the status of this aggregate check that the disable or enable of Proc node is determined.
Base checks are scheduled every 5 minutes. if a problem is detected, a retry is done to check the status every minute.
The aggregate check is to run every 2 minutes, regardless of the state. This is to make sure that there are no false positives generated, If the aggregate check takes more time than the base checks.
A notification is sent to the IDM dashboard on the change of any state of the F5 load balancer, i.e. when a Proc node is disabled or enabled.

The following graphic depict the states that the checks would go through in a scenario for disabling or enabling the Proc node in F5 load balancer Dicom pool




**Figure1:**

.. image::  images/workflow_of_f5_pool_management.PNG
    :width: 1000px
    :align: center
    :height: 530px
    :alt: default values




IV Notification
###############



Below graphic depicts the notification message flow for enabling and disabling. This will also help in keeping track of success and failure




**Figure2:**

.. image::  images/notification_message_flow.PNG
    :width: 1000px
    :align: center
    :height: 530px
    :alt: default values