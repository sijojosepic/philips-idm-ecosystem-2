Overview:
As part of continuous improvement to monitor critical assets and service for IS PACS ecosystem, IDM has added a support for monitoring I4 RabbitMQ Waiting Message count for certain queues(Illumeo Specific) 
This document provides an overview of the each of the service checks being monitored via IDM.

Service Checks:
This section provides details about each service checks and default thresholds.

01. I4PreProcessing Queue: 

Namespace: Product__IntelliSpace__Illumeo__I4PreProcessing_Queue__Status
Description:   This service check monitors waiting messages in I4PreProcessing queue.

02.	I4PreProcessingError Queue:

Namespace: Product__IntelliSpace__Illumeo__I4PreProcessingError_Queue__Status
Description:   This service check monitors waiting messages in I4PreProcessingError queue.


Configuration of services:
All these services have same configuration. These services are checked on every 5 minutes interval. If service state is CRITICAL or WARNING, IDM tries for another 2 times and if CRITICAL or WARNING state persists, IDM posts the status. Therefore, after 11 minutes IDM posts the state to portal.
Interval	5 minutes
Retry interval	2 minutes
Retry count	3

Note: Retry interval and retry count comes into picture when this service check goes into critical sate after breaching thresholds. IDM will not send critical event immediately but will retry with retry count at a retry interval to see if the thresholds are still breached i.e. service stays in critical state. Only after all retries, IDM will emit critical event. Retry count and retry interval are implemented to prevent any spikes and prevent false positives.

Service checks with threshold values:

Product__IntelliSpace__Illumeo__I4PreProcessing_Queue__Status Warning: 30 Critical: 60
Product__IntelliSpace__Illumeo__I4PreProcessingError_Queue__Status	Warning: 30 Critical: 60

Note: These threshold values are configurable.
