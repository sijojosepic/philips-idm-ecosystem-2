..  _pb:

=========================================
Genomics - NewRelic Violations monitoring
=========================================

This document provides an overview of the service checks being monitored for Genomics(NewRelic violations) via IDM.
IDM requires API key to retrieve NewRelic violations from the API endpoint - https://api.newrelic.com/v2/alerts_violations.json.
IDM neb node requires internet connection to connect to NewRelic API. Except Application specific check mentioned below all other checks are associated as passive checks.
The Genomics node can be discovered using neb local ip or dummy ip address and username, password may not be required. please refer the scanner example below.

**ipaddress** - 127.0.0.1
**Scanner** - GENOMICS
**username** - user
**password** - passwd

**Hostgroups** - genomic-newrelic-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interval"
   :widths: 20, 20, 20

    3, "0h 10m 0s", "0h 2m 0s"

Service Checks for Gemonics
###########################
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Genomics_NewRelicViolations", "Product__ISEE__Genomics__NewRelic__Violations", check_newrelic
