..  _maa:

==============================
Multi Active Archive Services
==============================

F5 Network Event Handling Design
================================

Problem Definition
------------------

To support huge study volume, ISPACS has come up with configuring Multi Active archives (MAA). IDM will be monitoring the below services as part of MAA



a. Mirror configuration check

b. Remote location Migration chain check

c. Multiple active archive monitoring

d. Waiting archives monitoring

e. H\W specification check for active and waiting archives



 Note:

1. changed the frequency to 30 minutes and 4 hours based on the discussion

2. Removed the Generic migration correctness check as part of this feature and moved to the other feature


Use Case Definition
-------------------

a. Mirror configuration check

- IDM detects the Mirror node on ISPACS ivault and validates the Archive Stack to the Mirror Stack mapping. In case the configuration doesn't match, IDM raise CRITICAL event

- When there is no mirror node configured as part of ISPACS ivault, IDM send 'OK' message

Frequency – 4 hours

b. Remote location Migration chain check

- IDM detects the Remote Cache node availability and validates whether Remote cache node is part of Migration chain. In case the node is mot part of MIgration chain IDM raises CRITICAL event.

- When there is no Remote Cache node configured as part of ISPACS ivault, IDM send 'OK' message

Frequency – 4 hours

c. Multiple active archive monitoring

- IDM reads the number of active archives configured on the isiteweb and validates against the actual number of active nodes(stacks) in strategy configuration. In case the configuration doesn't match raise CRITICAL event.

Frequency – 30 minutes

d. Waiting archives monitoring

- IDM reads the number of waiting stacks configured in strategy configuration and validates against the threshold based on the below table



Frequency – 30 minutes

e. H\W specification check for active and waiting archives

- IDM validates the active and waiting stack, hardware configuration to ensure it matches the recommended 8 core and 16 GB RAM

Frequency – 30 minutes


.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Mirror


.. csv-table:: **Multi Active Archive Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Mirror", "Product__IntelliSpace__PACS__MirrorConfig__Status", check_multi_active_archive!mirror
   "Migration", "Product__IntelliSpace__PACS__RemoteCache_MigrationConfig__Status", check_multi_active_archive!migration
   "Waiting", "Product__IntelliSpace__PACS__WaitingStackConfig__Status", check_multi_active_archive_study!waiting!$STUDY_VOLUME$
   "Active", "Product__IntelliSpace__PACS__ActiveStackConfig__Status", check_multi_active_archive!active
   "Hardware", "Product__IntelliSpace__PACS__ArchiveHardware__Status", check_multi_active_hardware_specification!hardware