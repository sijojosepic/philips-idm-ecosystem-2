..  _proc:

==================
Proc Node Services
==================

**HostScanner** - ISP

**Hostgroups** - consolidation-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 10m 0s", "0h 2m 0s"

Service Checks
##############
 - Proc Server

.. csv-table:: **Proc Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Proc", "Product__IntelliSpace__PACS__forward_Directory__File_Count", "check_win_filecount!$_HOSTFORWARDFILEWARN$!$_HOSTFORWARDFILECRIT$!$_HOSTDATADRIVE$!$_HOSTISP_FORWARD_FOLDER$"