Overview:
As part of continuous improvement to monitor critical assets and service for IS PACS ecosystem, IDM has added a support for monitoring Proc node forward folder file count.

This document provides an overview of the service checks being monitored via IDM.

Service Checks:
This section provides details about each service checks and default thresholds.

**Forward folder file count**  

Namespace: Product__IntelliSpace__PACS__forward_Directory__File_Count

Description:   This service check monitors count of files in forward folder for proc node.

Service checks with threshold values:

Product__IntelliSpace__PACS__forward_Directory__File_Count Warning: 500 Critical: 1000

Note: These threshold values are configurable.