..  _iecg:

==============================
iECG Integration
==============================

**HostScanner** - IECGHostScanner

**Hostgroups** - iecg-servers


.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - mssql checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "TMVIOProxyService", "Product__ISEE__iECG__TraceMasterVueIOProxy__Status", check_win_process!TMVIOProxyService.exe
   "TMVServiceManager", "Product__ISEE__iECG__TraceMasterVueServiceManager__Status", check_win_process!TMVServiceManager.exe
   "TMVStatusManager", "Product__ISEE__iECG__TraceMasterVueStatusManager__Status", check_win_process!TMVStatusManager.exe

iECG Folder Monitoring:
#######################

There are five services we have added to *iech-servers* hostgroup as part of iECG folder monitoring.
These services monitors folder file count and age.

**Services:**

1. Product__ISEE__iECG__Error_Directory__File_Count
2. Product__ISEE__iECG__PageWriter_Directory__File_Count
3. Product__ISEE__iECG__PageWriter_Directory__File_Age
4. Product__ISEE__iECG__IntelliVue_Directory__File_Count
5. Product__ISEE__iECG__IntelliVue_Directory__File_Age


**1. Product__ISEE__iECG__Error_Directory__File_Count:**

This service is reponsible to monitor the file count in */TraceMasterVue/Inbound/Error/* folder.

a). Alert CRITICAL if folder's file count > 0.

b). OK message with 0 file count if folder doesn't exists.

c). No WARNING alert for this service.

**2. Product__ISEE__iECG__PageWriter_Directory__File_Count:**

This service is responsible to monitor file count in */TraceMasterVue/Inbound/PageWriter/* folder.

a). CRITICAL alert if folder's file count >= 10.

b). WARNING alert If  file count >= 5.

c). OK message if file count < 5.

d). OK message with 0 file count if folder doesn't exists.

**3. Product__ISEE__iECG__PageWriter_Directory__File_Age:**

This service is responsible to monitor file's age in */TraceMasterVue/Inbound/PageWriter/* folder.

a). Alert CRITICAL if folder's files stay more than 24 hours.

b). No WARNING alert for this service.

c). OK message if files are not older than 24 hours

**4. Product__ISEE__iECG__IntelliVue_Directory__File_Count:**

This service is responsible to monitor file count in */TraceMasterVue/Inbound/IntelliVue/* folder.

a). CRITICAL alert if folder's file count >= 10.

b). WARNING alert If  file count >= 5.

c). OK message if file count < 5.

d). OK message with 0 file count if folder doesn't exists.


**5. Product__ISEE__iECG__IntelliVue_Directory__File_Age:**

This service is responsible to monitor file's age in */TraceMasterVue/Inbound/IntelliVue/* folder.

a). Alert CRITICAL if folder's files stay more than 24 hours.

b). No WARNING alert for this service.

c). OK message if files are not older than 24 hours.
