..  _HSOP_stratoscale_servers:

=========
OVERVIEW
=========
This service will be use for updating all the services and version in install base report. Following is the format of consul api:::

	{
	    "consul": [],
	    "elasticsearch-161-92-250-50": [
	        "Version-6.8.2"
	    ],
	    "elasticsearch-161-92-250-52": [
	        "Version-6.8.2"
	    ],
	    "elasticsearch-161-92-250-57": [
	        "Version-6.8.2"
	    ],
	    "fluentd-161-92-250-55": [
	        "Version-3"
	    ],
	    "kibana-161-92-250-47": [
	        "Version-6.8.2"
	    ]
	}

==============================
 HSOP Stratoscale Services
==============================

**HostScanner** - HSOP

**Hostgroups** - stratoscale-servers

.. csv-table:: *informational-service*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    2, "12h 0m 0s", "0h 15m 0s"

Service Checks
##############
 - Stratoscale Servers

.. csv-table:: **Stratoscale Servers**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 20

   "HSOP-STRATOSCALE", "Administrative__Philips__HSOP__Services__Version", "hsop_services_version!$CONSUL_API$"