===============
HSOP Hardware
===============

**HostScanner** - HSOPHW

**Hostgroups** - hsop-hardware

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - HP Hardware - Temperature Status
 - HP Hardware - Fan Status
 - HP Hardware - Battery Status
 - HP Hardware - Voltage Status
 - HP Hardware - Memory Status


.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Temperature", "hsop-hardware-temperature-service", check_hsop_hardware_temperature/$PLUGINSDIR$/check_hsop_hardware_health.py
   "Fan", "hsop-hardware-fan-service", check_hsop_hardware_fan/$PLUGINSDIR$/check_hsop_hardware_health.py
   "Battery", "hsop-hardware-battery-service", check_hsop_hardware_battery/$PLUGINSDIR$/check_hsop_hardware_health.py
   "Voltage", "hsop-hardware-voltage-service", check_hsop_hardware_voltage/$PLUGINSDIR$/check_hsop_hardware_health.py
   "Memory", "hsop-hardware-memory-service", check_hsop_hardware_memory/$PLUGINSDIR$/check_hsop_hardware_health.py



==========================
Configurations
==========================
#. lhost.yml::

        endpoints :
        - address: <address-1,address-2, address-3, ____ , address-n>
          scanner: HSOPHW
          username: <username>
          password: <password>

#. Plugin

      **Overview-**

         This plugin checks the status of Fan, Voltage, Temperature, Battery and Memory of a HSoP HP harwdare.
         it invokes the HP ilo REDFISH API's to get the Aggregate status of Fan, Voltage, Temperature, Battery and Memory Services.

      **Deployment-**

          The plugin is available as part of nagios plugin in Neb node.

      **Execution-**


          The script should be triggered by the Shinken framework. The script expects hostname (or IP address of the HP hardware ilo 
          endpoint), username, password and the service type (FAN|TEMPERATURE|VOLTAGE|BATTERY|MEMORY) as input parameters.

      **Example-**

         Run the script with command below ::

          python check_hsop_hardware_health.py -H [IP Address of the HP ILO endpoint] -U [Username] -P [Password] -D [FAN|TEMPERATURE|VOLTAGE|BATTERY|MEMORY]

      **Frequency-**


        The script is executed every 5 minutes for each of the services(FAN, TEMPERATURE, VOLTAGE, BATTERY, MEMORY).

      **Return Code-**


        0. OK

        1. WARNING

        2. CRITICAL

      **Output-**


        | The script outputs the following data

        1. OK Status ::
           <<FAN|TEMPERATURE|VOLTAGE|BATTERY|MEMORY>> Status is OK

        2. Warning Status ::		   
           <<FAN|TEMPERATURE|VOLTAGE|BATTERY|MEMORY>> Status is WARNING
	   
            - **VOLTAGE :** Serial Number, Spare Part Number, Bay Number, State and Status.
            - **TEMPERATURE :** Sensor Name, Reading Celcius, State and Status.
            - **FAN :** Fan Name, Reading Percent, State and Status.
            - **BATTERY :** Part Number, Serial Number, Charge Level Percent, Model, State and Status.
            - **MEMORY :** Slot, Controller, Channel, Socket, State and Status.

        3. Critical Status ::
           <<FAN|TEMPERATURE|VOLTAGE|BATTERY|MEMORY>> Status is CRITICAL

            - **VOLTAGE :** Serial Number, Spare Part Number, Bay Number, State and Status.
            - **TEMPERATURE :** Sensor Name, Reading Celcius, State and Status.
            - **FAN :** Fan Name, Reading Percent, State and Status.
            - **BATTERY :** Part Number, Serial Number, Charge Level Percent, Model, State and Status.
            - **MEMORY :** Slot, Controller, Channel, Socket, State and Status.

             