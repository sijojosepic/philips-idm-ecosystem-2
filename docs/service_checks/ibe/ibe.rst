..  _ibe:

===============
IBE Integration
===============
Monitoring of IBE servers can be done in 3 ways. There are 2 different nodes in IBE, the application node and the database node. There are different set of services for both the nodes which has to be monitored. There is also a generic discovery for IBE servers in which both the services of application node as well as database node is being monitored.

**Scanner** - IBE, IBEAPP, IDEDB

**Hostgroups** - ibe-servers, ibe-app-servers, ibe-db-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks for IBE-App Node
###############################
 - Basic Checks
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Rhapsody_aliveness", "Product__ISEE__IBE__Rhapsody_Aliveness__Status", check_isee_ibe_rhapsody_alive!https
   "Rhapsody_error_queue", "Product__ISEE__IBE__Rhapsody_ErrorQueue__Status", check_isee_ibe_error_queue!errorqueue
   "Rhapsody", "Product__ISEE__IBE__Rhapsody__Status", check_win_process!wrapper.exe
   "Rhapsody_routes_status", "Product__ISEE__IBE__Rhapsody_Route__Status", check_isee_ibe_rhapsody_route
   "DICOMManagerService", "Product__ISEE__IBE__DICOMManagerService__Status", check_win_process!Philips.IBE.DICOMManagerService.exe
   "NetTCPPortSharing", "Product__ISEE__IBE__NetTCPPortSharing__Status", check_win_process!SMSvcHost.exe
   "HealthecareIntegrationFoundation", "Product__ISEE__IBE__HealthecareIntegrationFoundation__Status", check_win_process!Philips.HIF.ServiceHost.exe
   "ICIPiXProxy", "Product__ISEE__IBE__ICIPiXProxy__Status", check_win_process!Philips.IBE.ICIPiXProxy.exe
   "DcomLaunch", "Product__ISEE__IBE__DcomLaunch__Status", check_win_service!^DcomLaunch$!_NumGood=1:
   "Dhcp", "Product__ISEE__IBE__DHCPClient__Status", check_win_service!^Dhcp$!_NumGood=1:
   "Dnscache", "Product__ISEE__IBE__DNSClient__Status", check_win_service!^Dnscache$!_NumGood=1:
   "EncryptingFileSystem", "Product__ISEE__IBE__EncryptingFileSystem__Status", check_win_service!^EFS$!_NumGood=1:
   "GroupPolicyClient", "Product__ISEE__IBE__GroupPolicyClient__Status", check_win_service!^gpsvc$!_NumGood=1:
   "NlaSvc", " Product__ISEE__IBE__NetworkLocationAwareness__Status", check_win_service!^NlaSvc$!_NumGood=1:
   "nsi", "Product__ISEE__IBE__NetworkStoreInterfaceService__Status", check_win_service!^nsi$!_NumGood=1:
   "Power", "Product__ISEE__IBE__Power__Status", check_win_service!^Power$!_NumGood=1:
   "TermService", "Product__ISEE__IBE__RemoteDesktopServices__Status", check_win_service!^TermService$!_NumGood=1:
   "RpcSs", "Product__ISEE__IBE__RemoteProcedureCall__Status", check_win_service!^RpcSs$!_NumGood=1:
   "SamSs", "Product__ISEE__IBE__SecurityAccountsManager__Status", check_win_service!^SamSs$!_NumGood=1:
   "LanmanServer", "Product__ISEE__IBE__LanmanServer__Status", check_win_service!^LanmanServer$!_NumGood=1:
   "SENS", "Product__ISEE__IBE__SystemEventNotification__Status", check_win_service!^SENS$!_NumGood=1:
   "ProfSvc", "Product__ISEE__IBE__UserProfileService__Status", check_win_service!^ProfSvc$!_NumGood=1:
   "EventLog", "Product__ISEE__IBE__WindowsEventLog__Status", check_win_service!^EventLog$!_NumGood=1:
   "MpsSvc", "Product__ISEE__IBE__WindowsFirewall__Status", check_win_service!^MpsSvc$!_NumGood=1:
   "Winmgmt", "Product__ISEE__IBE__WMI__Status", check_win_service!^Winmgmt$!_NumGood=1:
   "WAS", "Product__ISEE__IBE__WAS__Status", check_win_service!^WAS$!_NumGood=1:
   "wuauserv", "Product__ISEE__IBE__WindowsUpdate__Status", check_win_service!^wuauserv$!_NumGood=1:
   "W3SVC", "Product__ISEE__IBE__W3SVC__Status", check_win_service!^W3SVC$!_NumGood=1:9


Service Checks for IBE-DB Node
##############################
 - Basic Checks
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "SQL Server", "Product__ISEE__IBE__SQLServer__Status", check_win_process!sqlservr.exe
   "NetTCPPortSharing", "Product__ISEE__IBE__NetTCPPortSharing__Status", check_win_process!SMSvcHost.exe
   "HealthecareIntegrationFoundation", "Product__ISEE__IBE__HealthecareIntegrationFoundation__Status", check_win_process!Philips.HIF.ServiceHost.exe
   "ICIPiXProxy", "Product__ISEE__IBE__ICIPiXProxy__Status", check_win_process!Philips.IBE.ICIPiXProxy.exe
   "DcomLaunch", "Product__ISEE__IBE__DcomLaunch__Status", check_win_service!^DcomLaunch$!_NumGood=1:
   "Dhcp", "Product__ISEE__IBE__DHCPClient__Status", check_win_service!^Dhcp$!_NumGood=1:
   "Dnscache", "Product__ISEE__IBE__DNSClient__Status", check_win_service!^Dnscache$!_NumGood=1:
   "EncryptingFileSystem", "Product__ISEE__IBE__EncryptingFileSystem__Status", check_win_service!^EFS$!_NumGood=1:
   "GroupPolicyClient", "Product__ISEE__IBE__GroupPolicyClient__Status", check_win_service!^gpsvc$!_NumGood=1:
   "NlaSvc", " Product__ISEE__IBE__NetworkLocationAwareness__Status", check_win_service!^NlaSvc$!_NumGood=1:
   "nsi", "Product__ISEE__IBE__NetworkStoreInterfaceService__Status", check_win_service!^nsi$!_NumGood=1:
   "Power", "Product__ISEE__IBE__Power__Status", check_win_service!^Power$!_NumGood=1:
   "TermService", "Product__ISEE__IBE__RemoteDesktopServices__Status", check_win_service!^TermService$!_NumGood=1:
   "RpcSs", "Product__ISEE__IBE__RemoteProcedureCall__Status", check_win_service!^RpcSs$!_NumGood=1:
   "LanmanServer", "Product__ISEE__IBE__LanmanServer__Status", check_win_service!^LanmanServer$!_NumGood=1:
   "ProfSvc", "Product__ISEE__IBE__UserProfileService__Status", check_win_service!^ProfSvc$!_NumGood=1:
   "EventLog", "Product__ISEE__IBE__WindowsEventLog__Status", check_win_service!^EventLog$!_NumGood=1:
   "MpsSvc", "Product__ISEE__IBE__WindowsFirewall__Status", check_win_service!^MpsSvc$!_NumGood=1:
   "Winmgmt", "Product__ISEE__IBE__WMI__Status", check_win_service!^Winmgmt$!_NumGood=1:
   "WAS", "Product__ISEE__IBE__WAS__Status", check_win_service!^WAS$!_NumGood=1:
   "wuauserv", "Product__ISEE__IBE__WindowsUpdate__Status", check_win_service!^wuauserv$!_NumGood=1:
   "W3SVC", "Product__ISEE__IBE__W3SVC__Status", check_win_service!^W3SVC$!_NumGood=1:9

Service Checks for IBE Generic Discovery
########################################
 - Basic Checks
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Rhapsody_aliveness", "Product__ISEE__IBE__Rhapsody_Aliveness__Status", check_isee_ibe_rhapsody_alive!https
   "Rhapsody_error_queue", "Product__ISEE__IBE__Rhapsody_ErrorQueue__Status", check_isee_ibe_error_queue!errorqueue
   "Rhapsody", "Product__ISEE__IBE__Rhapsody__Status", check_win_process!wrapper.exe
   "Rhapsody_routes_status", "Product__ISEE__IBE__Rhapsody_Route__Status", check_isee_rhapsody_route
   "SQL Server", "Product__ISEE__IBE__SQLServer__Status", check_win_process!sqlservr.exe
   "DICOMManagerService", "Product__ISEE__IBE__DICOMManagerService__Status", check_win_process!Philips.IBE.DICOMManagerService.exe
   "NetTCPPortSharing", "Product__ISEE__IBE__NetTCPPortSharing__Status", check_win_process!SMSvcHost.exe
   "HealthecareIntegrationFoundation", "Product__ISEE__IBE__HealthecareIntegrationFoundation__Status", check_win_process!Philips.HIF.ServiceHost.exe
   "ICIPiXProxy", "Product__ISEE__IBE__ICIPiXProxy__Status", check_win_process!Philips.IBE.ICIPiXProxy.exe
   "DcomLaunch", "Product__ISEE__IBE__DcomLaunch__Status", check_win_service!^DcomLaunch$!_NumGood=1:
   "Dhcp", "Product__ISEE__IBE__DHCPClient__Status", check_win_service!^Dhcp$!_NumGood=1:
   "Dnscache", "Product__ISEE__IBE__DNSClient__Status", check_win_service!^Dnscache$!_NumGood=1:
   "EncryptingFileSystem", "Product__ISEE__IBE__EncryptingFileSystem__Status", check_win_service!^EFS$!_NumGood=1:
   "GroupPolicyClient", "Product__ISEE__IBE__GroupPolicyClient__Status", check_win_service!^gpsvc$!_NumGood=1:
   "NlaSvc", " Product__ISEE__IBE__NetworkLocationAwareness__Status", check_win_service!^NlaSvc$!_NumGood=1:
   "nsi", "Product__ISEE__IBE__NetworkStoreInterfaceService__Status", check_win_service!^nsi$!_NumGood=1:
   "Power", "Product__ISEE__IBE__Power__Status", check_win_service!^Power$!_NumGood=1:
   "TermService", "Product__ISEE__IBE__RemoteDesktopServices__Status", check_win_service!^TermService$!_NumGood=1:
   "RpcSs", "Product__ISEE__IBE__RemoteProcedureCall__Status", check_win_service!^RpcSs$!_NumGood=1:
   "SamSs", "Product__ISEE__IBE__SecurityAccountsManager__Status", check_win_service!^SamSs$!_NumGood=1:
   "LanmanServer", "Product__ISEE__IBE__LanmanServer__Status", check_win_service!^LanmanServer$!_NumGood=1:
   "SENS", "Product__ISEE__IBE__SystemEventNotification__Status", check_win_service!^SENS$!_NumGood=1:
   "ProfSvc", "Product__ISEE__IBE__UserProfileService__Status", check_win_service!^ProfSvc$!_NumGood=1:
   "EventLog", "Product__ISEE__IBE__WindowsEventLog__Status", check_win_service!^EventLog$!_NumGood=1:
   "MpsSvc", "Product__ISEE__IBE__WindowsFirewall__Status", check_win_service!^MpsSvc$!_NumGood=1:
   "Winmgmt", "Product__ISEE__IBE__WMI__Status", check_win_service!^Winmgmt$!_NumGood=1:
   "WAS", "Product__ISEE__IBE__WAS__Status", check_win_service!^WAS$!_NumGood=1:
   "wuauserv", "Product__ISEE__IBE__WindowsUpdate__Status", check_win_service!^wuauserv$!_NumGood=1:
   "W3SVC", "Product__ISEE__IBE__W3SVC__Status", check_win_service!^W3SVC$!_NumGood=1:9
