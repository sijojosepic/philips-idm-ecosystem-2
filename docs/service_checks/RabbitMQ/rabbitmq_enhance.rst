=========================
RabbitMq Nodes Monitoring
=========================

Under RabbitMQ nodes API, the following attributes and Alarms are getting monitored

::

    API                 ->   /api/nodes
    Service Name        ->   Messaging__Pivotal__RabbitMQ__NodesAPI__Status
    Service Frequency   ->   1 mns
    Retry interval      ->    0

Attributes With Thresholds
--------------------------

.. csv-table::
   :header: "Attribute", "Warning", "PlaceHolder", "Crititcal", "PlaceHolder"
   :widths: 20, 20, 40, 20, 40

   "mem_used", "50%", "RBQ_MEM_USED_WARN", "60%", "RBQ_MEM_USED_CRIT"
   "disk_free", "5 GB", "RBQ_DISK_FREE_WARN", "3 GB", "RBQ_DISK_FREE_CRIT"
   "fd_used", "4096", RBQ_FD_USED_WARN, "6147", RBQ_FD_USED_CRIT


Alarms
-------

.. csv-table::
   :header: "Alarm Type", "Attribute"
   :widths: 40, 30

   "Memory Alarm", "mem_alarm"
   "Disk Alarm", "disk_free_alarm"
   "Partition", "partitions"


The default Threshold can be changed at a site level in the lhost.yml file under threshold_values

::

    threshold_values:
        RBQ_MEM_USED_WARN:80
        RBQ_MEM_USED_CRIT:70
        RBQ_DISK_FREE_WARN:10
        RBQ_DISK_FREE_CRIT:38
        RBQ_FD_USED_WARN:4096
        RBQ_FD_USED_CRIT:6147


In a Cluster RabbitMQ set up if a node is in a problem state, then its API/nodes monitoring attributes can be missing in the API/nodes response.
Then such nodes would be skipped, as there is a service to do monitor the problems with the nodes and it would report the problem.

============================
RabbitMQ Overview Monitoring
============================

Under RabbitMQ Overview API, monitoring is as follows.

Functionality
-------------

    CRITICAL - When connection, channel, consumers and unacknowledged messages count is greater or equal the CRITICAL threshold.

    WARNING  - When connection, channel, consumers and unacknowledged messages count is greater or equal to the WARNING threshold.

    OK       - When connection, channel, consumers and unacknowledged messages count is under the threshold values.

::

        RabbitMQ API           ->  api/overview
        Service Name           ->  Messaging__Pivotal__RabbitMQ__OverviewAPI__Status
        Service Frequency      ->  1mns
        Retries                ->  0


.. csv-table::
   :header: "Attributes", "Warning_Formula", "Warning", "Critical_Formula", "Critical"
   :widths: 20, 20, 40, 20, 40

   "object_totals.connections", "#nodes*6", "RBQ_NUM_NODES*RBQ_CONN_WARN", "#nodes*7", "RBQ_NUM_NODES*RBQ_CONN_CRIT"
   "object_totals.channels", "#nodes*13", "RBQ_NUM_NODES*RBQ_CHANNEL_WARN", "#nodes*14", "RBQ_NUM_NODES*RBQ_CHANNEL_CRIT"
   "object_totals.consumers", "#nodes*8", RBQ_NUM_NODES*RBQ_CONSUMER_WARN, "#nodes*9", "RBQ_NUM_NODES*RBQ_CONSUMER_CRIT"
   "queue_totals.messages_unacknowledged", "--", "RBQ_UNACK_MSG_WARN", "--", "RBQ_UNACK_MSG_CRIT"


| Here **#nodes => the SUM of number of ISPACS/UDM core nodes + I4 Nodes + RWS nodes**.
| During Monitoring IDM makes iSiteWeb request's for getting the number of UDM core nodes, and the response is cached for an hour.
| If there is any problem with the iSiteWeb API, then IDM uses the number of core nodes from the latest discovered nodes.

**Default threshold values:**
::

    RBQ_NUM_NODES: 100
    RBQ_CONN_WARN: 6
    RBQ_CONN_CRIT: 7
    RBQ_CHANNEL_WARN: 13
    RBQ_CHANNEL_CRIT: 14
    RBQ_CONSUMER_WARN: 8
    RBQ_CONSUMER_CRIT: 9
    RBQ_UNACK_MSG_WARN: 50
    RBQ_UNACK_MSG_CRIT: 100

**Here RBQ_NUM_NODES => Sum of number of I4 Nodes + RWS Nodes**




In the **lhost.yml** under **threshold_values** site level configuration all these can be configured if required.
::

   threshold_values:
        RBQ_NUM_NODES: 10
        RBQ_CONN_WARN: 6
        RBQ_CONN_CRIT: 7
        RBQ_CHANNEL_WARN: 13
        RBQ_CHANNEL_CRIT: 14
        RBQ_CONSUMER_WARN: 8
        RBQ_CONSUMER_CRIT: 9
        RBQ_UNACK_MSG_WARN: 50
        RBQ_UNACK_MSG_CRIT: 100

=========================
RabbitMQ Queue Monitoring
=========================



Monitoring the length of messages in RabbitMQ Queues respectively for UDM, RWS, I4 based on RabbitMQ api/queues response.

Functionality
-------------

    CRITICAL - When a queue is having messages greater or equal to the defined critical threshold for it

    WARNING  - When a queue is having messages greater or equal to defined WARNING threshold and less than its CRITICAL threshold

    OK       - When None of the Queues has breached the WARNING or CRITICAL threshold

::

        RabbitMQ API           ->  api/queues
        Monitoring attribute   ->  length of 'messages' under each queue
        Service Name           ->  Messaging__Pivotal__RabbitMQ__QueuesAPI__Status
        Service Frequency      ->  1mns
        retry                  ->  0

Thresholds
-----------

    | There is a default WARNING and CRITICAL thresholds defined for all Queues.
    | Those queues which are monitored with a different threshold, as given by respective product teams are defined separately.
    | On top of these, it is possible to customize queues at a site level in the lhost.yml file with respective thresholds.



How thresholds work.?
------------------------
    | Default threshold would be applicable for any queues if it's not defined anywhere else
    | Product's Defined threshold would be applicable for those queues given by product team
    | If the threshold for any queues are defined in the lhost.yml file, it will get the highest precedence


**Default thresholds**

::

              WARNING  - RBQ_QUEUE_WARN, 950
              CRITICAL - RBQ_QUEUE_CRIT, 1000

The Default thresholds can be changed in the site's lhost.yml under **threshold_values**

::

   threshold_values:
        - RBQ_QUEUE_WARN: 1500
        - RBQ_QUEUE_CRIT: 1750



**Product's Defined Queues**

::


      RABBITMQ_DEF_QUES:
        - HoldingQueue,50,100
        - PrefetchMessageTransform,400,600
        - PrefetchManagerQueue,400,600
        - ISPACSOutboundNotification,50,100
        - awsq1_heartbeat,50,100
        - awsq2_heartbeat,50,100
        - awsq3_heartbeat,50,100
        - awsq4_heartbeat,50,100
        - IllumeoImageProcessingStatus,100,500
        - AutoAssignment,100,200
        - AssignExamQueue,100,200
        - FirstPhase,20,50
        - SecondPhase,50,100
        - ThirdPhase,50,100
        - I4Error,50,100
        - I4Waiting,50,100


**Customize Queues in lhost.yml**


In the **lhost.yml** under **threshold_values** site level configuration for Queues can be provided in an yaml array format against the variable **RABBITMQ_QUEUES**

<*QueueName*>, <*Warning*>, <*Critical*>

::

   threshold_values:
      - RABBITMQ_QUEUES:
            - Queue1, 250, 300
            - Queue2, 550, 600
            - Queue3, 2000, 2500
            - ISPACSOutboundNotification, 2000, 2500


   OR

   threshold_values:
      - RABBITMQ_QUEUES: ['Queue1, 250, 300', 'Queue2, 550, 600', 'Queue3, 2000, 2500', 'ISPACSOutboundNotification, 2000, 2500']


*Note: Thresholds customized under lhost.yml RABBITMQ_QUEUES section would have the highest precedence.*

Example::
    For the queue '**ISPACSOutboundNotification**' Threshold used for monitoring would be WARNING - 2000, CRITICAL 2500,
    even though its defined under RABBITMQ_DEF_QUES with thresholds respectively '**ISPACSOutboundNotification,50,100**'
