Overview:
As part of continuous improvement to monitor critical assets and service for IS PACS ecosystem, IDM has added a support for monitoring Database node for both ISPACS version greater and less than 4.4.552.
For version less than 4.4.552, We have provided a manual scanner for Database node and for version greater than 4.4.552 it will discover with ISPACS info.

This document provides an overview of the each of the service checks being monitored via IDM.

Service Checks:
This section provides details about each service checks and default thresholds.

01.	 OS Database Disk and Mount Point: 

Namespace: OS__Microsoft__Windows__Databasedisk__Usage
Description:   This service check monitors Database node both disk and mount point storage usage. 

02.	 Database SQL server agent status:

Namespace: Product__IntelliSpace__DB__SQLSERVERAGENT__Status
Description:   This service check monitors DB node SQL server agent status.

Service checks with threshold values:

OS__Microsoft__Windows__Databasedisk__Usage Warning: 75 Critical: 85
Product__IntelliSpace__DB__SQLSERVERAGENT__Status	IDM sends alert when SQLAGENT is down.

Note: These threshold values are configurable.