..  _pb:

======================
Performance Bridge(PB)
======================
There are 2 different nodes in PB, the Platform node which runs on Windows and the Practice node which runs on Linux. There are different set of services for both the nodes which has to be monitored.

PB Practice node which run on Linux will be monitored using Shinken passive checks(check_result). PB team shall push the service state and message for the passive service checks. IDM shall monitor PB 2.0 version onwards.

**Scanner** - PBWindows
**Scanner** - PBLinux

**Hostgroups** - pb-windows-servers
**Hostgroups** - pb-linux-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interval"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks for PB Windows Node
##################################
 - Basic Checks
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Sinse_CLRConnectorsContainer", "Product__ISEE__PB__Sisense_CLRConnectorsContainer__Status", check_win_service_without_argument!Sisense.CLRConnectorsContainer
   "Sisense_Discovery", "Product__ISEE__PB__Sisense_Discovery__Status", check_win_service_without_argument!Sisense.Discovery
   "Sisense_JVMConnectorsContainer", "Product__ISEE__PB__Sisense_JVMConnectorsContainer__Status", check_win_service_without_argument!Sisense.JVMConnectorsContainer
   "Sisense_MonitoringAgent", "Product__ISEE__PB__Sisense_MonitoringAgent__Status", check_win_service_without_argument!Sisense.Monitoring.Agent
   "Sisense_Orchestrator", "Product__ISEE__PB__Sisense_Orchestrator__Status", check_win_service_without_argument!Sisense.Orchestrator
   "Sisense_Oxygen", "Product__ISEE__PB__Sisense_Oxygen__Status", check_win_service_without_argument!Sisense.Oxygen
   "Sisense_Pulse", "Product__ISEE__PB__Sisense_Pulse__Status", check_win_service_without_argument!Sisense.Pulse
   "Sisense_Repository", "Product__ISEE__PB__Sisense_Repository__Status", check_win_service_without_argument!Sisense.Repository
   "ElastiCubeManagementService", "Product__ISEE__PB__ElastiCubeManagementService__Status", check_win_service_without_argument!ElastiCubeManagementService


Passive Service Checks for PB Linux Node
########################################
 - Application Specific Checks

.. csv-table:: **Application Specific Checks**
   :header: "Namespace"
   :widths: 40

   "Product__ISEE__PB__Mirth__Status"
   "Product__ISEE__PB__DataManager__Status"
   "Product__ISEE__PB__OpenDJ__Status"
   "Product__ISEE__PB__OpenAM__Status"
   "Product__ISEE__PB__Indexer__Status"
   "Product__ISEE__PB__MirthQueue__Status"
   "Product__ISEE__PB__PostgressConnection_Count__Status"
   "Product__ISEE__PB__SupportContainer__Status"
   "Product__ISEE__PB__CurrentLoad__Status"
   "Product__ISEE__PB__Wildfly__Status"
   "Product__ISEE__PB__WildflyHeapbuffer__Status"
   "Product__ISEE__PB__RabbitMQ_Queue__Status"
   "Product__ISEE__PB__RabbitMQ__Status"
   "Product__ISEE__PB__APM__Status"
   "Product__ISEE__PB__DataDisk__Usage"
   "Product__ISEE__PB__"RootDisk__Usage"
   "Product__ISEE__PB__ServersDisk__Usage"
   "Product__ISEE__PB__Sftp__Status"
   "Product__ISEE__PB__Quartz__Status"
