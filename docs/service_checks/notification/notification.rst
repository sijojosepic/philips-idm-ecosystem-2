..  _concerto:


======================
Notification
======================

In IDM external notification includes
1. WhatsApp notification using Twilio
2. Autocase creation

For any given site, the services can be configured for WhatsApp notification and/or Autocase creation.
If a service is configured for notification, then the next CRITICAL alert will be notiifed and the status will be updated in the Database.
For any configured service there will be only one notification per CRITICAL to OK status turn around.
That is if a service happend to be CRITICAL for a longer duration then Shinken monitoring framework may update the CRITICAL status multiple times depends on its configuration, In this case there is no need of duplicate WhatsAPP notification or autocase creation.

Database
=========
There are three collections
1. Config Collection  - Site level services configuration available here
2. Status Collection - After performing WhatsApp notification or Autocase creation status of the action will be updates in to status collection
  - incase of autocase creation case id also be updated along with success status
  - incase of WhatsApp notification twilio_msg_id along with its status will also be get updated here
  - If an already notified service turns to OK, then the respective service entry will be deleted from status collection

3. Notification Audit - Each time when a notification API is invoked an entry will be there in the the audit collection with its status - SUCCESS/FAILURE

Maintenance
============
If the maintenance window is opened at the site level or a host level then notification will be disabled for the same.



Retrys
======
If the notification fails then the respective task will be retried for configured number of times.

Case creation
============

Case creation is marked as SUCCESS, only if the response contains case_id.
Upon ConnectTimeout error the task will be retried for the configured number of times
Status collection updates SUCCESS/FAILURE status for each time autocase creation is attempted, there will be only one update even if there are retry attempts.
Audit Collection updates the status, each time when the Autocase API is invoked
  - If the task is tried multiple times, each time there will be an entry in audit collection.
