..  _hl7:

==============================
HL7 Services
==============================

**HostScanner** - HL7

**Hostgroups** - isp-hl7-servers, isp-hl7hd-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - HL7 Server

.. csv-table:: **HL7 Server**
   :header: "Service", "Namespace", "port", "Check Command"
   :widths: 20, 40, 20, 40

   "HL7", "Product__IntelliSpace__PACS__Error_Queue__Status", "8544", "check_hl7_hold_queue!errorqueue"
   "HL7", "Product__IntelliSpace__PACS__Hold_Queue__Status", "8544", "check_hl7_hold_queue!holdqueue"
   "HL7", "Product__IntelliSpace__PACS__Rhapsody_Version__Status", "8544", "check_rhapsody_version!1"
   "HL7", "Product__IntelliSpace__PACS__HL7FilingQueue__Status", "8544", "check_hl7filing_service"
   "HL7", "Product__IntelliSpace__PACS__HL7FilingService__Status", "8544", "check_http_auth!/HL7FilingService/HL7CDRFilingService.svc?wsdl"
