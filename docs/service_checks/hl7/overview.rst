Overview:
As part of continuous improvement to monitor critical assets and service for IS PACS ecosystem, IDM has added a support for monitoring HL7 broker, Rhapsody starting v6.2.x.  
This document provides an overview of the each of the service checks being monitored via IDM.

Service Checks:
This section provides details about each service checks and default thresholds.

01.	 Rhapsody Error Queue: 

Namespace: Product__IntelliSpace__PACS__Error_Queue__Status
Description:   This service check monitors messages in error queue of Rhapsody engine at a given interval. 

02.	 Rhapsody Hold Queue:

Namespace: Product__IntelliSpace__PACS__Hold_Queue__Status
Description:   This service check monitors messages in hold queue of Rhapsody engine at a given interval.

03.	Rhapsody Version:

Namespace: Product__IntelliSpace__PACS__Rhapsody_Version__Status
Description:   This service checks for Rhapsody version.

04.	Rhapsody HL7 Filing In Queue and Out Queue:

Namespace: Product__IntelliSpace__PACS__HL7FilingQueue__Status
Description:   It monitors the status of Locker/PACS_Platinum_HL7_MULTIPORT/PACS Platinum HL7 Filing Service OUT/ communication point. IDM fetches the total number of messages in ‘In Queue’ and ‘Out Queue’. If any of the ‘In Queue’ or ‘Out Queue’ exceeds Warning or Critical threshold, it posts the alert to IDM.

05.	 HL7 Filing Service Web service:

Namespace: Product__IntelliSpace__PACS__HL7FilingService__Status
Description: It monitors the HL7FilingService webservice. IDM is accessing this REST API and rasies CRITICAL if service is down.

Configuration of services:
All these services have same configuration. These services are checked on every 5 minutes interval. If service state is CRITICAL or WARNING, IDM tries for another 2 times and if CRITICAL or WARNING state persists, IDM posts the status. Therefore, after 11 minutes IDM posts the state to portal.
Interval	5 minutes
Retry interval	2 minutes
Retry count	3

Note: Retry interval and retry count comes into picture when this service check goes into critical sate after breaching thresholds. IDM will not send critical event immediately but will retry with retry count at a retry interval to see if the thresholds are still breached i.e. service stays in critical state. Only after all retries, IDM will emit critical event. Retry count and retry interval are implemented to prevent any spikes and prevent false positives.

Service checks with threshold values:

Product__IntelliSpace__PACS__Error_Queue__Status Warning: 20 Critical: 30
Product__IntelliSpace__PACS__Hold_Queue__Status	Warning: 5 Critical:10
Product__IntelliSpace__PACS__Rhapsody_Version__Status	IDM sends alert when unable to fetch version.
Product__IntelliSpace__PACS__HL7FilingQueue__Status	Warning: 1000 Critical: 1500
Product__IntelliSpace__PACS__HL7FilingService__Status	IDM sends alert when webservice is down.
Note: These threshold values are configurable.
