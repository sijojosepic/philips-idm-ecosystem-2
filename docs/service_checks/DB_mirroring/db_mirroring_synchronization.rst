..  _DB_Mirroring_Services:

======================================
 DB Mirroring Synchronization Services
======================================

**HostScanner** - ISP

**Hostgroups** - isp-db-mirrors

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Database Server - DB Mirroring Synchronization Status

.. csv-table:: **DB Mirroring Synchronization Status**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "DB Mirroring", "MSSQL__Database__Mirroring__Synchronization__Status", "check_sql_mirroring_synchronization_status!msdb"
