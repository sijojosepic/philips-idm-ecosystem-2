*********
Overview:
*********
This plugin facilitates service check for synchronization, split brain and failover. And it uses pymssql python module to connect with SQL server DB node.


DB Mirroring Service Plugin:
****************************


**1. Synchronization:**
   Database synchronization is the process of establishing data consistency between two databases, automatically copying changes back and forth. Harmonization of the data over time should be performed continuously. Pulling out data from source (Principal) database to destination (Mirror) is the most trivial case.

**2. Split Brain:**
   On Principal DB failure, database nodes transforms from “Principal to Mirror” and “Mirror to Principal”. The DB nodes enters into split brain state at the period of transformation. If transformation of DB failed by any reasons, then DB nodes stays in split brain state for infinite time until resolving issue.

**3. Failover:**
   On Principal DB failure, database nodes transforms from “Principal to Mirror” and “Mirror to Principal”. In case of successful transformation of DB nodes, it’s failover state.

Configuration of services:
All these services have same configuration. These services are checked on every 5 minutes interval. If service state is CRITICAL or WARNING, IDM tries for another 2 times and if CRITICAL or WARNING state persists, IDM posts the status.


Interval	5 minutes

Retry interval	2 minutes

Retry count	3

Note: Retry interval and retry count comes into picture when this service check goes into critical state. IDM will not send critical event immediately but will retry with retry count at a retry interval to see if service stays in critical state. Only after all retries, IDM will emit critical event. Retry count and retry interval are implemented to prevent any spikes and prevent false positives.