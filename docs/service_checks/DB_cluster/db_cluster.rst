..  _DB_Cluster_Services:

================================
 SQL Always ON Failover Services
================================

**HostScanner** - ISP

**Hostgroups** - isp-db-clusters

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Database Server - DB Cluster Services Status

.. csv-table:: **DB Cluster Services Status**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "DB Failover Status", "MSSQL__Cluster__Database__Failover__Status", "check_frequent_failover!msdb"
