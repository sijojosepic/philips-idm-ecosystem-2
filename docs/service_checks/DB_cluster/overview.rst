*********
Overview:
*********
As part of continuous improvement to monitor critical assets and services for IS PACS ecosystem, IDM has added a support for monitoring Database Failover event in cluster environment.

This service check for frequent failover status. If in predefine period, failover has happened more than predefine failover count, secondary database will turn into resolving state. To prevent this when failover has happened equal to failover count, IDM will raise a CRITICAL alarm.


SQL Always ON Failover Service Plugin:
**************************************

  Namespace: MSSQL__Cluster__Database__Failover__Status

  Description:   This service check monitors number of failovers in predefine period. There are two conditions that this service check is checking:

  a. If number of failover is less than the predefine count and not equal to zero than it will raise warning alarm.
  b. If number of failover is more than the predefine count than it will raise critical alarm.


  Configuration of services:

   This service is checked on every 5 minutes interval. If service state is CRITICAL or WARNING, IDM tries for another 2 times and if 
   CRITICAL or WARNING state persists, IDM posts the status. Therefore, after 11 minutes IDM posts the state to portal.

   Interval	5 minutes

   Retry interval	2 minutes

   Retry count	3

   Note: Retry interval and retry count comes into picture when this service check goes into critical sate after breaching thresholds. IDM will not send critical event immediately but will retry with retry count at a retry interval to see if the thresholds are still breached i.e. service stays in critical state. Only after all retries, IDM will emit critical event. Retry count and retry interval are implemented to prevent any spikes and prevent false positives.
