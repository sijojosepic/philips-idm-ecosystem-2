*********
Overview:
*********
These plugins service check for SQL Always On and it facilitates HA(High Availability) and DR(Disaster Recovery). And it uses pymssql python module to connect with SQL server DB node.



SQL Always On Service Plugins:
******************************


**1. Synchronization:**
   Database synchronization is the process of establishing data consistency between two databases, automatically copying changes back and forth. Harmonization of the data over time should be performed continuously. Pulling out data from source (Primary) database to destination (Secondary) is the most trivial case.

**2. Failover:**
   On Primary DB failure, Availability group changes db role from “Primary to Secondary” and “Secondary to Primary”. In case of successful db role change of DB nodes, it’s failover state.

**3. Availability Check**
   This service check gives information about availability. It covers unexpected DB drop down, network fails to detect DB node and resolving struck at failover.

**4. Diagnosis:**
   This service provides SQL internal health, it covers lease expired and timeout state. Basically, it gives log snapshot of Error:19407 of lease related issues for diagnosis.
