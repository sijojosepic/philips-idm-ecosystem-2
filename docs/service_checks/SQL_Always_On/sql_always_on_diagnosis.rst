..  _SQL_Always_On_Services:

================================
SQL Always On Diagnosis Services
================================

**HostScanner** - ISP

**Hostgroups** - isp-db-clusters

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Database Server - SQL Always On Diagnosis Status

.. csv-table:: **SQL Always On Diagnosis Status**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Diagnosis", "MSSQL__Cluster__Database__Diagnosis__Status", "check_sql_always_on_diagnosis_status!msdb"
