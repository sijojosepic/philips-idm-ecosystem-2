..  _SQL_Always_On_Services:

===================================
SQL Always On Availability Services
===================================

**HostScanner** - ISP

**Hostgroups** - isp-db-clusters

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Database Server - SQL Always On Availability Status

.. csv-table:: **SQL Always On Availability Status**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Availability", "MSSQL__Cluster__Database__Availability__Status", "check_sql_always_on_availability_status!msdb"
