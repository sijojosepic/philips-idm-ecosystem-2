========================
Package Deletion on NEB
========================

Introduction
============
IDM has introduced a new functionality to maintain the available packages on the NEB. Currently, when you distribute a package to NEB it stays there always until it is deleted manually. Due to software distribution of Product & CE packages, NEB node /var/ mount is getting full causing failures in monitoring and distribution.

Impact:

- Discovery and monitoring failure
- Software distribution and deployment failure.
- Manual intervention is required to clean up.

IDM has come up with automated package-cleanup on NEB so that we can avoid the above failures. The automated package-cleanup algorithm is based on available timestamps where the latest distributed package will be considered as N, the second latest N-1 and so on.

N is a configurable value and default value is 6.

- If N = 6 which means the latest 6 packages will be retained.
- if N = 2 which means the latest 2 packaged will be retained.


*****************
Service Namespace
*****************

- Administrative__Philips__Package_Deletion__Task__Trigger

- Administrative__Philips__Package_Deletion__Task__Status

Check Interval
**************
Once in a day at 3:00 AM.

Retry check interval
********************
2 minutes

*This service check is hosted under localhost*


Workflow
========
- Once the service *Administrative__Philips__Package_Deletion__Task__Trigger* has been triggered. It hits an API to back office to get all the available packages for a site. Based on the configured value( Default Value 6), service will retain those many packages and rest it will delete from NEB.

**Backoffice API:**

http://vigilant/pma/audit/package_details/<site_id>

- Once the package is deleted, we need to delete the package related data as well. Below are the actions will be performed post package deletion.

1. Delete respective subscription files if any from NEB and SVN.

2. Delete respective siteinfo files if any from NEB and SVN.

3. Post a new distribution status *Deleted* to back-office for that package in Audit collection.

4. Post a new deployment status *Deleted* to back-office after siteinfo deletions in Audit collection.


- Once package related data is deleted, update the package list on the deployment page with the latest available packages on the NEB.

