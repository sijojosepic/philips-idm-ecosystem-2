..  _i4ev:

==============================
I4EV Integration
==============================

**HostScanner** - I4EVHostScanner

**Hostgroups** - i4ev-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - Basic Checks
 - Rest Checks

.. csv-table:: *Application Specific Checks*
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "NetTcpPortingSharing", "Product__IntelliSpace__I4EV__NetTcpPortingSharing__Status", check_win_process!SMSvcHost.exe
   "PortalService", "Product__IntelliSpace__I4EV__PortalService__Status", check_win_process!PortalService.exe
   "PmsPortalAdminService", "Product__IntelliSpace__I4EV__PmsPortalAdminService__Status", check_win_process!PmsPortalAdmin.exe
   "PmsCTLogService", "Product__IntelliSpace__I4EV__PmsCTLogService__Status", check_win_process!LogService.exe
   "ExtendRemotingService", "Product__IntelliSpace__I4EV__ExtendRemotingService__Status", check_win_process!ExtendedRemotingService.exe
   "E2EService", "Product__IntelliSpace__I4EV__E2EService__Status", check_win_process!E2E.Service.exe
   "I4LoggingService", "Product__IntelliSpace__I4EV__I4LoggingService__Status", check_win_process!I4LoggingService.exe
   "W3SVC", "Product__IntelliSpace__I4EV__W3SVC__Status", check_win_service!W3SVC
   "IDSSystemService", "Product__IntelliSpace__I4EV__IDSSystemService__Status", check_win_process!IDSSystemService.exe
   "DataProvider", "Product__IntelliSpace__I4EV__DataProvider__Status", check_json_status!DataProvider/DataProvider/Monitor!Applications[*]!Status!Message!ApplicationName
   "MissionBriefing", "Product__IntelliSpace__I4EV__MissionBriefing__Status", check_json_status!IdmServices/MissionBriefing/Monitor!Applications[*]!Status!Message!ApplicationName
   "SnapITWebService", "Product__IntelliSpace__I4EV__SnapITWebService__Status", check_json_status!IdmServices/SnapITWebService/Monitor!Applications[*]!Status!Message!ApplicationName
   "ClinicalLabel", "Product__IntelliSpace__I4EV__ClinicalLabel__Status", check_json_status!ClinicalLabelService/ClinicalLabel/Monitor!Applications[*]!Status!Message!ApplicationName