..  _concerto:


======================
IDM Threshold Configuration 
======================


The aim of site specific threshold configuration is to,  customize the state when to trigger **WARNING** and **CRITICAL** conditions for various **OS**, **VMware**, **HA Cloud** and **ISP-Server** parameters for a particular site. All the parameters under each category is defined with a default value and it can be further customized for a particular site in its ``lhost.yml`` file. 
Shinken will refer these values at run time and raise the appropriate status respectively. 


Default Configurations 
##################


All the threshold parameters are defined in the  ``EMP/ansible/roles/shinken/defaults/main.yml`` file 
With a default value as shown in the image below.

**Figure1:**

.. image::  images/thresholdnew.jpg
    :width: 1000px
    :align: center
    :height: 530px
    :alt: default values

Customizing in the ``lhost.yml`` file
#########################


For changing the value of any of the variables, it can be given in the site-specific ``lhost.yml`` file, 
under the **threshold_values:** section with the updated value.  

**Figure2:**

.. image:: images/lhost_yml.jpg
    :align: center
    :alt: alternate text

Ansible Template and Task
#####################

At the **Ansible** side a new  jinja template and a task is been added to the file with the shinken role.

Template content (``EMP/ansible/roles/shinken/templates/threshold.cfg.j2``)
***************************************************************************************


**Figure3:**

.. image:: images/template.jpg
    :width: 800px
    :align: center
    :height: 200px


Task details (``EMP/ansible/roles/shinken/tasks/main.yml``)
*********************************************************************

**Figure4:**

.. image:: images/task.jpg
    :width: 800px
    :align: center
    :height: 200px



How it works?
############

   1) During the ansible pull, a ``threshold.cfg`` file will be get created with the respective value for each parameter, in the Neb’s ``/etc/philips/shinken/resource.d`` directory.  

   2) If you customize any of the parameters as described above inside the ``lhost.yml`` file, it will be in effect instead of default value inside the ``threshold.cfg`` file.   

   3) At run time shinken will refer to the threshold.cfg file and raise the states (WARNING, CRITICAL) accordingly. 