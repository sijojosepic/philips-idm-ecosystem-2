Overview:
As part of continuous improvement to monitor critical assets and services for ISEE utility server, IDM has added a support for monitoring Veeam Backup Jobs through cmdlets. 
This document provides an overview of Veeam Backup jobs service checks being monitored via IDM.

Service Checks:
This section provides details about each service checks.

1.Veeam Backup Jobs: 

  Namespace: Product__ISEE__UtilityServer__VEEAM_Backup__Status

  Description:   This service check monitor the Backup jobs where jobtype is Backup. 

2.Veeam Copy Jobs:

  Namespace: Product__ISEE__UtilityServer__VEEAM_Copy__Status

  Description:   This service check monitors the Backup jobs where jobtype is Copy.


Configuration of services:
All these services have same configuration. These services are checked on every 24 hours interval between 1AM to 2 AM. If service state is CRITICAL or WARNING, IDM tries for another 2 times and if CRITICAL or WARNING state persists, IDM posts the status. This service checks will behave according to following conditions:

a. If any of the job in failed state, service status will turn into CRITICAL.
b. If there is no failed state, but some jobs are in Warning state, service status will turn into WARNING.
c. If all jobs run successfully, service status will turn into OK.


Interval	24 hours

Retry interval	15 minutes

Retry count	3

Note: Retry interval and retry count comes into picture when this service check goes into critical state. IDM will not send critical event immediately but will retry with retry count at a retry interval to see if service stays in critical state. Only after all retries, IDM will emit critical event. Retry count and retry interval are implemented to prevent any spikes and prevent false positives.