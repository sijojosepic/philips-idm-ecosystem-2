======================
 Veeam Backup Services
======================


Overview
--------

This plugin shall monitor a veeam backup and veeam sure backup jobs . It uses python WinRM module and invokes the
powershell command to get the job status.


Deployment
----------

The plugin is available as part of nagios plugin in Neb node.


Plugin Argument
---------------

The plugin expect Host Name/IP address, User Name, Password, and backup job type. Optionally it accepts the backup type
argument('veeam_backup'/'sure_backup') based on the job type it gets the job status).


Service Check Configuration
---------------------------

**HostScanner** - ISEEUtilityServer

**Hostgroups** - utility-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "24h 0m 0s", "0h 15m 0s"

Service Checks
##############
 - Utility Server - Veeam Backup

.. csv-table:: **Veeam Backup**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Veeam", "Product__ISEE__UtilityServer__VEEAM_Backup__Status", "check_veeam_backup!Backup"
   "Veeam", "Product__ISEE__UtilityServer__VEEAM_Copy__Status", "check_veeam_backup!Copy"
   "Veeam", "Product__ISEE__VEEAM__SureBackup__Status", "check_veeamsure_backup!DRV!sure_backup"
