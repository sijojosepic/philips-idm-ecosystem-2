..  _i4_services:

==============================
 I4 Services
==============================

**HostScanner** - ISP

**Hostgroups** - i4-viewer-servers,i4-ev-servers,i4-prep-servers

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

Service Checks
##############
 - I4 Server

.. csv-table:: **I4 Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "I4", "Product__IntelliSpace__I4__CONFIG_Health__Status", "check_health_status_i4!$I4_CONFIG_SERVICE$"
   "I4", "Product__IntelliSpace__I4__IllUMEO_CONFIGURATOR_Health__Status", "check_health_status_i4!$IllUMEO_CONFIGURATOR$"
   "I4", "Product__IntelliSpace__I4__LOCKING_SERVICE_Health__Status", "check_health_status_i4!$I4_LOCKING_SERVICE$"
   "I4", "Product__IntelliSpace__I4__FINDING_DBSERVICE_Health__Status", "check_health_status_i4!$I4_FINDING_DBSERVICE$"
   "I4", "Product__IntelliSpace__I4__HANGING_DBSERVICE_Health__Status", "check_health_status_i4!$I4_HANGING_DBSERVICE$"
   "I4", "Product__IntelliSpace__I4__REG_DBSERVICE_Health__Status", "check_health_status_i4!$I4_REG_DBSERVICE$"
   "I4", "Product__IntelliSpace__I4__LOCATION_TOPOLOGY_SERVICE_Health__Status", "check_health_status_i4!$LOCATION_TOPOLOGY_SERVICE$"
   "I4", "Product__IntelliSpace__I4__RABBITMQ_SERVICE_Health__Status", "check_health_status_i4!$RABBITMQ_SERVICE$"
   "I4", "Product__IntelliSpace__I4__E2EService__Status", "check_win_process!E2E.Service.exe"   