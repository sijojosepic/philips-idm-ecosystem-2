..  _udm:

============
UDM Services
============

**HostScanner** - UDMHostScanner

**Hostgroups** - udm-redis-server, udm-app-server, udm-prep-server, udm-strg-server

|

.. csv-table:: *Standard Services*
   :header: "Max. Check Attempts", "Normal Check Interval", "Retry Check Interal"
   :widths: 20, 20, 20

    3, "0h 5m 0s", "0h 2m 0s"

|

Service Checks
##############
 - Redis Server
 - UDM App Server
 - UDM prep Server
 - UDM Storage Server

|

.. csv-table:: **Redis Server**
   :header: "Service", "Namespace", "port", "Check Command"
   :widths: 20, 40, 20, 40

   "Redis", "Product__IntelliSpace__UDM__Redis__ConnectionResponse__Status", "6379", "check_redis!-T $REDIS_CONNTIME_WARN$,$REDIS_CONNTIME_CRIT$"
   "Redis", "Product__IntelliSpace__UDM__Redis__Memory__Utilization", "6379", "check_redis!--memory_utilization=$REDIS_MEMORY_WARN$,$REDIS_MEMORY_CRIT$ -M $REDIS_MAXMEMORY$"
   "Redis", "Product__IntelliSpace__UDM__Redis__Client_Connections__Count", "6379", "check_redis!--connected_clients=$REDIS_CONN_COUNT_WARN$,$REDIS_CONN_COUNT_CRIT$"

|
|

.. csv-table:: **UDM App Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Hybrid Storage Scheduler Service", "Product__IntelliSpace__UDM__HybridStorage__SchedulerService__Status", "check_win_process!Philips.UDM.HybridStorage.WindowsService.EXE"
   "UDM Monitor", "Product__IntelliSpace__UDM__MonitoringService__Status", "check_win_process!Philips.UDM.Monitor.EXE"
   "CILM Dataservice", "Product__IntelliSpace__UDM__CILMDataservice__Status", "check_win_process!Philips.CILM.DataService.Host.EXE"
   "CILM Scheduler", "Product__IntelliSpace__UDM__CILMScheduler__Status", "check_win_process!Philips.CILM.Prefetch.Scheduler.WindowsService.EXE"
   "Prefetch Message Transformer", "Product__IntelliSpace__UDM__PrefetchMessage__Transformer__Status", "check_win_process!Prefetch.MessageTransformLauncher.EXE"
   "Prefetch Manager","Product__IntelliSpace__UDM__Prefetch__Manager__Status", "check_win_process!Prefetch.ManagerLauncher.EXE"
   "Prefetch Replay Manager","Product__IntelliSpace__UDM__Prefetch_ReplayManager__Status", "check_win_process!MessagingFramework.ReplayManager.EXE"
   "Rule Engine","Product__IntelliSpace__UDM__RuleEngine__Status", "check_win_process!Java.EXE -jar RuleEngine.jar"
   "Imaging Services","Product__IntelliSpace__UDM__ImagingService__Status", "check_win_process!ImagingServices.Host.exe"
   "I-Site","Product__IntelliSpace__UDM__iSiteMonitor__Status", "check_win_process!iSiteMonitor.exe"
   "Storage Service","Product__IntelliSpace__UDM__StorageService__Status", "check_win_process!StorageService.exe"
   "IIS Server","Product__IntelliSpace__UDM__IISServer__Status", "check_win_process!inetinfo.exe"
   "Rule Engine DBA Service","Product__IntelliSpace__UDMRulEngineDBService__Status", "check_http_service!/RuleEngineDbService/RuleEngineDbService.svc"
   "UDM Service","Product__IntelliSpace__UDM__ConfigurationService__Status", "check_http_service!/UDMServices"
   "DICOM Wado Service","Product__IntelliSpace__UDM__WadoRS__Status", "check_http_service!/IHEAuthority/WadoRs/ping"
   "Non DICOM RS Service","Product__IntelliSpace__UDM__NonDICOMRS__Status", "check_http_service!/IHEAuthority/NondicomRs/ping"
   "DICOM RS Service","Product__IntelliSpace__UDM__DICOMRS__Status", "check_http_service!/IHEAuthority/DicomRs/ping"
   "ImagingStudy","Product__IntelliSpace__UDM__FHIRExtension__ImagingStudy_Status", "check_http_auth!/FHIRExtensionServices/fhir/"
   "StudyStatus","Product__UDM__FHIRExtension__StudyStatus__Status", "check_http_auth!/FHIRExtensionServices/StudyStatus"
   "FHIRServices","Product__IntelliSpace__UDM__FHIRExtension__FHIRServices_Status", "check_http_auth!/FHIRServices/fhir/ImagingStudy"
   "IAMServer","Product__IntelliSpace__UDM__IAMServer_Status", "check_hsdp_services!IAM"
   "SAPService","Product__IntelliSpace__UDM__SAPService__Status", "check_hsdp_services!SAP"
   "SAPBackupService","Product__IntelliSpace__UDM__SAPBackupService__Status", "check_hsdp_services!SAPBACKUP"
   "SAPArchive","Product__UDM__SAPArchive__Service__Status", "check_hsdp_services!SAPARCHIVE"
   "SSMServer","Product__IntelliSpace__UDM__SSMServer__Status", "check_hsdp_services!SSM"
   "Heartbeat","Product__IntelliSpace__UDM__Heartbeat__Status", "check_hsdp_services!S3"
   "CILM Purge Scheduler","Product__Intellispace__UDM__CILMPurgeScheduler__Status", "check_win_process!Philips.CILM.PurgeScheduler.EXE"
   "InstanceDiscovery Health","Product__IntelliSpace__UDM__InstanceDiscovery_Health__Status", "/InstanceDiscovery/Diagnostics/v1/healthstatus"
   "InstanceDiscovery Ping","Product__IntelliSpace__UDM__InstanceDiscovery_Ping__Status", "/InstanceDiscovery/Diagnostics/v1/ping"
   "LocationTopology Health","Product__IntelliSpace__UDM__LocationTopology_Health__Status", "/LocationTopology/Diagnostics/v1/healthstatus"
   "LocationTopology Ping","Product__IntelliSpace__UDM__LocationTopology_Ping__Status", "/LocationTopology/Diagnostics/v1/ping"
   "StudyExtensionService Health","Product__IntelliSpace__UDM__StudyExtensionService_Health__Status", "/StudyExtensionService/Diagnostics/v1/healthstatus"
   "StudyExtensionService Ping","Product__IntelliSpace__UDM__StudyExtensionService_Ping__Status", "/StudyExtensionService/Diagnostics/v1/ping"

|
|

.. csv-table:: **UDM Prep Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "BoS Eventing Service","Product__IntelliSpace__UDM__BoSEventingService__Status", ""
   "UDM Monitor", "Product__IntelliSpace__UDM__MonitoringService__Status", "check_win_process!Philips.UDM.Monitor.EXE"
   "Imaging Services","Product__IntelliSpace__UDM__ImagingService__Status", check_win_process!ImagingServices.Host.exe
   "I-Site","Product__IntelliSpace__UDM__iSiteMonitor__Status", "check_win_process!iSiteMonitor.exe"
   "Storage Service","Product__IntelliSpace__UDM__StorageService__Status", "check_win_process!StorageService.exe"
   "IIS Server","Product__IntelliSpace__UDM__IISServer__Status", "check_win_process!inetinfo.exe"
   "Imaging Services","Product__IntelliSpace__UDM__ImagingService__Status", check_win_process!ImagingServices.Host.exe
   "UDM Service","Product__IntelliSpace__UDM__ConfigurationService__Status", "check_http_service!/UDMServices"
   "Eventing Service","Product__IntelliSpace__UDM__Eventing_Service__Status", "Philips.Hi.UdmConnector.EventingService.Host.exe"
   "InstanceDelete Health","Product__IntelliSpace__UDM__InstanceDelete_Health__Status", "/Instancedelete/Diagnostics/v1/healthstatus"
   "InstanceDelete Ping","Product__IntelliSpace__UDM__InstanceDelete_Ping__Status", "/Instancedelete/Diagnostics/v1/ping"
   "InstanceDiscovery Health","Product__IntelliSpace__UDM__InstanceDiscovery_Health__Status", "/InstanceDiscovery/Diagnostics/v1/healthstatus"
   "InstanceDiscovery Ping","Product__IntelliSpace__UDM__InstanceDiscovery_Ping__Status", "/InstanceDiscovery/Diagnostics/v1/ping"
   "LocationTopology Health","Product__IntelliSpace__UDM__LocationTopology_Health__Status", "/LocationTopology/Diagnostics/v1/healthstatus"
   "LocationTopology Ping","Product__IntelliSpace__UDM__LocationTopology_Ping__Status", "/LocationTopology/Diagnostics/v1/ping"
   "MetadataStore Health","Product__IntelliSpace__UDM__MetadataStore_Health__Status", "/MetadataStore/Diagnostics/v1/healthstatus"
   "MetadataStore Ping","Product__IntelliSpace__UDM__MetadataStore_Ping__Status", "/MetadataStore/Diagnostics/v1/ping"
   "StudyExtensionService Health","Product__IntelliSpace__UDM__StudyExtensionService_Health__Status", "/StudyExtensionService/Diagnostics/v1/healthstatus"
   "StudyExtensionService Ping","Product__IntelliSpace__UDM__StudyExtensionService_Ping__Status", "/StudyExtensionService/Diagnostics/v1/ping"

|
|

.. csv-table:: **UDM Storage Server**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "Storage Service","Product__IntelliSpace__UDM__Storage__Status", "check_win_process!StorageService.exe"
   "IIS Server","Product__IntelliSpace__UDM__IIS__Server__Status", "check_win_process!inetinfo.exe"
   "SAP","Product__IntelliSpace__UDM__SAP__Status", "check_http_service!/storage-access/options"
   "Notification Service","Product__IntelliSpace__UDM__NotificationService__Status", "check_http_service!/notification/options"
   "Backup SAP Service","Product__IntelliSpace__UDM__BackupAccess__Status", "check_http_service!/backup-access/options"
   "Backup Notification Service","Product__IntelliSpace__UDM__BackupNotification__Status", "check_http_service!/backupnotification/options"
   "UDM Service","Product__IntelliSpace__UDM__ConfigurationService__Status", "check_http_service!/UDMServices"

|
|

.. csv-table:: **Proc, Archive, Cache, Archive Mirror, FilterCache**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "iSyntax Rs Health","Product__IntelliSpace__UDM__iSyntaxRs_Health__Status", "/isyntaxrs/Diagnostics/v1/healthstatus"
   "iSyntax Rs Ping","Product__IntelliSpace__UDM__iSyntaxRs_Ping__Status", "/isyntaxrs/Diagnostics/v1/ping"
   "iSyntax Rs Service","Product__IntelliSpace__UDM__iSyntaxRs_Service", "check_win_process!Philips.Hi.UdmConnector.iSyntaxRs.Host.exe"

|
|

.. csv-table:: **Service check applicable for all UDM version**
   :header: "Application Server", "Prep Storage"
   :widths: 40, 40

   "Product__IntelliSpace__UDM__CILMDataservice", "Product__IntelliSpace__UDM__MonitoringService"
   "Product__IntelliSpace__UDM__CILMScheduler", "Product__IntelliSpace__UDM__ImagingService"
   "Product__IntelliSpace__UDM__PrefetchMessage_Transformer", "Product__IntelliSpace__UDM__iSiteMonitor"
   "Product__IntelliSpace__UDM__Prefetch_Manager", "Product__IntelliSpace__UDM__IISServer"
   "Product__IntelliSpace__UDM__Prefetch_ReplayManager", "Product__IntelliSpace__UDM__ConfigurationService"
   "Product__IntelliSpace__UDM__RuleEngine"
   "Product__IntelliSpace__UDM__RuleEngineDBService"
   "Product__IntelliSpace__UDM__WadoRS"
   "Product__IntelliSpace__UDM__MonitoringService"
   "Product__IntelliSpace__UDM__ImagingService"
   "Product__IntelliSpace__UDM__iSiteMonitor"
   "Product__IntelliSpace__UDM__IISServer"
   "Product__IntelliSpace__UDM__ConfigurationService"

|
|

.. csv-table:: **Additional service check applicable for UDM version 1.0**
   :header: "Application Server", "ISP Integration node"
   :widths: 40, 40

   "Product__IntelliSpace__UDM__FHIRExtension__ImagingStudy", "Product__IntelliSpace__UDM__Availability__Status"
   "Product__UDM__FHIRExtension__StudyStatus", "Product__Intellispace__UDM__CILMPurgeScheduler"
   "Product__IntelliSpace__UDM__FHIRExtension__FHIRServices"

|
|

.. csv-table:: **Additional service check applicable for UDM version 1.2**
   :header: "Application Server", "ISPACS node(Dicom, Cache, Archive, Mirror, FilterCache)", "Prep Server"
   :widths: 40, 40, 40

   "Product__Intellispace__UDM__CILMPurgeScheduler", "Product__IntelliSpace__UDM__iSyntaxRs_Ping", "Product__IntelliSpace__UDM__MetadataStore_Ping"
   "Product__IntelliSpace__UDM__InstanceDiscovery_Ping", "Product__IntelliSpace__UDM__iSyntaxRs_Health", "Product__IntelliSpace__UDM__MetadataStore_Health"
   "Product__IntelliSpace__UDM__LocationTopology_Ping", "Product__IntelliSpace__UDM__iSyntaxRs_Service", "Product__IntelliSpace__UDM__Eventing_Service__Status"
   "Product__IntelliSpace__UDM__InstanceDiscovery_Health"
   "Product__IntelliSpace__UDM__LocationTopology_Health"

|


Note: Prep storage node will not be available for UDM version 1.0 and UDM version is identified based on the ISPACS version. ISPACS 4.4.551.72 version is considered as UDM 1.0 and 4.4.553.20 version is considered as UDM 1.1 and 4.4.554.0 version is considered as UDM 1.2

=======
UDM 2.1
=======
In UDM 2.1, There are QIDO, StowRS and Federation services added newly, apart from these all the services remains similar as in UDM1.2

QIDO
####

The QIDO services will be running in the Application nodes, there are two HTTP based service monitoring

.. csv-table:: **QIDO**
   :header: "Serivce", "Namespace", "URL"
   :widths: 20, 40, 35

   "PING", "Product__IntelliSpace__UDM__QIDO_Ping__Status", "/QidoRs/Diagnostics/v1/ping"
   "HEALTH", "Product__IntelliSpace__UDM__QIDO_Health__Status", "/QidoRs/Diagnostics/v1/healthstatus"
   "PING UDM3.1", "Product__IntelliSpace__UDM__QIDO_Ping__Status", "/DicomWebServices/Query/Diagnostics/v1/ping"
   "HEALTH UDM3.1", "Product__IntelliSpace__UDM__QIDO_Health__Status", "/DicomWebServices/Query/Diagnostics/v1/healthstatus"


Federation
##########

There are three Federation related processes, in the FilterCache node. These Processes will be Monitored only when Federation is enabled For the site. This information whether the federation is enabled or not is read from the iSite Registry **MASTER** infrastructure node or the **Infrastructure** node itself.

In the iSite Registry under **FederationConfiguration** ::

    EnableFederation is 1 => Federation enabled

    EnableFederation is 0 => Federation disabled



.. csv-table:: **Federation**
   :header: "Process Name ", "Namespace"
   :widths: 20, 40

   "Philips.iSite.Federation.HL7Service.exe", "Product__IntelliSpace__PACS__Federation_HL7Service__Status"
   "Philips.iSite.Federation.FetchStudyService.exe", "Product__IntelliSpace__PACS__Federation_FetchStudyService__Status"
   "Philips.iSite.Federation.UtilityService.exe", "Product__IntelliSpace__PACS__Federation_UtilityService__Status"

StowRS
######
 Monitor StowRs in DicomProcessing nodes.

.. csv-table:: **Service check applicable for UDM2.1 version**
   :header: "Service", "Namespace", "Check Command"
   :widths: 20, 40, 40

   "DICOM StowRS Service","Product__IntelliSpace__UDM__StowRS__Status", "check_http_service!/IHEAuthority/StowRs/ping"
