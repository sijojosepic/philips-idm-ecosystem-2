Zerto Alerts Monitoring
=======================

This plugin will fetch all the alerts provided by Zerto API at once, and then report the alert on the basis of Subcategory provided (Zerto - helpIdentifier).

    * Hostaddress, Username, Password, Category and Previous State parameters are mandatory.
    * Subcategory is mandatory, when category is 'alerts'.
    * URL and Port parameters are optional, In case not provided it will use default Zerto port and url.

To monitor new alerts only 'ALERT_DETAILS_DICT' dictionary need to be updated with 'alert identifiers' and 'ok message' details.

Redis Cache
===========
The services will make multiple Zerto Alerts API calls during execution to monitor different types of alerts, to avoid this duplicate API calls, the caching layer got introduced with 4mns TTL (TimeToLive) as the default service frequency is 7mns.

This cache can be disabled by passing the argument --enable_cache with value as 'no'.

During retries or non OK previous service status cached data would not be used, rather the information would be fetched directly from the API.

For storing the zerto alerts data in the cache the key will be composed using the following convention
    * '<hostaddress>_zerto_alerts'

I. Monitoring Host with no ZVRA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor Host without ZVRA, this service check will fetch the Zerto Alert for Host with no VRA and report it.

Below are the parameters required for Host-ZVRA monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'VRA' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API call:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Zerto Alert HelpIdentitfier:
    * VRA0001

Service Check Name:
    * Disaster__Recovery__Zerto__HostZVRA__Status

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'VRA' -X 'OK'

II. Monitoring Current RPO and RPO Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor RPO Alerts, this service check will fetch the Zerto Alerts for RPO (reported when current RPO is more than the target RPO).
If there is no RPO alert, then the plugin will monitor current RPO value by fetching the actual RPO value from 'vpgs' Zerto API.


Below are the parameters required for RPO monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'RPO' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts
    * https://zvm_ipaddress:zvm_port/v1/vpgs

Zerto Alert HelpIdentitfier:
    * VPG0009
    * VPG0010

Service Check Name:
    * Disaster__Recovery__Zerto__RPO__Status

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'RPO' -X 'CRITICAL'

III. Monitoring Other Zerto Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This service check will fetch the All the Zerto Alerts which are not part of any service-checks.
If there is no alert, then the plugin will provide 'OK' status.


Below are the parameters required for all other alerts monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'OTHER' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Service Check Name:
    * Disaster__Recovery__Zerto__Other__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'OTHER' -X 'CRITICAL'

IV. Monitoring VRA Health
~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor VRA Health, this service check will fetch all the general VRA Health alerts.(reported when there is any problem with any VRA).

Below are the parameters required for VRA Health monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'HEALTH' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Service Check Name:
    * Disaster__Recovery__Zerto__VRA_Health__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'HEALTH' -X 'CRITICAL'

V. Monitoring VRA Network Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor VRA Network Alerts, this service check will fetch all the network related VRA alerts.(like VRA without IP, VRAs not connected, etc).

Below are the parameters required for VRA Network monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'NETWORK' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Service Check Name:
    * Disaster__Recovery__Zerto__VRA_Network__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'NETWORK' -X 'CRITICAL'

VI. Monitoring VRA Journal Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor VRA Journal Alerts, this service check will fetch all the journal related VRA alerts.(like Journal size mismatch, Journal space low, etc).

Below are the parameters required for VRA Journal monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'JOURNAL' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Service Check Name:
    * Disaster__Recovery__Zerto__VRA_Journal__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'JOURNAL' -X 'CRITICAL'

VII. Monitoring VRA Host Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor VRA Host Alerts, this service check will fetch all the Host related VRA alerts.(like Host IP changes, Host rollback failed, etc).

Below are the parameters required for VRA Host monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'HOST' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Service Check Name:
    * Disaster__Recovery__Zerto__VRA_Host__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'HOST' -X 'CRITICAL'

VIII. Monitoring VRA Disk Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To monitor VRA Disk Alerts, this service check will fetch all the Disk related VRA alerts.(like Recovery disk missing, VM disk inaccessible, etc).

Below are the parameters required for VRA Disk monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'DISK' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts

Service Check Name:
    * Disaster__Recovery__Zerto__VRA_Disk__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'DISK' -X 'CRITICAL'

IX. Monitoring Zerto License Alerts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This service check will fetch all the license related alerts.(like license exceeded, license expired, etc).

Below are the parameters required for Zerto license monitoring:
    * Hostaddress: ZVM IP addresss (required)
    * Username: Zerto username(required)
    * Password: Zerto password (required)
    * Port: The port where zerto is hosted (optional)
    * URl: Zerto url (optional)
    * Category: The zerto API call - 'alerts' (required)
    * Subcategory: Alert Identifier - 'LICENSE' (required)
    * enable_cache: 'Yes' or 'No' (optional)
    * cache_timeout: By default 240 secs (4 mins) - (optional)
    * previous_state: Previous service status (required)

Zerto API calls:
    * https://zvm_ipaddress:zvm_port/v1/alerts
    * https://zvm_ipaddress:zvm_port/v1/license

Service Check Name:
    * Disaster__Recovery__Zerto__License__Alerts

Service Check Frequency:
    * 7 Mins

Zerto Alert Plugin Examples:

    * ./check_zerto_status.py -H hostaddress -u username -p password -s 'alerts' -a 'LICENSE' -X 'CRITICAL'