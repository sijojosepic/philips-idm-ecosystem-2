**IDM Disaster Recovery Monitoring**
------------------------------------

IDM monitor UDM DR tool Zerto


        
1. DR Discovery
""""""""""""""""""
With DR Discovery IDM monitors the Primary and Secondary ZVMs, IDM first check in the iSiteWeb whether the Disaster Recovery is enabled or not(DRflag).
If DR is enabled for the site, then discovery task collects facts for Primary and Secondary ZVMs and creates the configuration files respectively.


Along with CPU, RAM, Network... etc, the following Exes are also monitored in the ZVMs
::

    Zerto.LocalDbInstanceManagerService.exe
    Zerto.Online.Services.Connector.exe
    Zerto.Vba.VbaService.exe
    Zerto.Zvm.Service.exe


**Endpoint**::

    -   infra_address: master..isyntax.net
        password: <ZVM OS pwd>
        scanner: DR
        username: <ZVM OS user>
        address: [zvm1, zvm2]   
        zerto_username: <zerto username>
        zerto_password: <zerto password>
        vcenters: [vcenter1, vcenter2]

Typical ZVM host cfg file look like below
**ZVM.cfg**::

    define host {
      host_name                      <zvm-hostname>
      use                            zvm-server
      _discovery_components          [{"version": "6.5.0", "name": "DR-Site2"}]
      _key                           '{+gAAAAABdLuEOgCnOwqfvt_725Z2oSUdYai8q-=+}'
      _usr                           {+gAAAAABdLuEOpdmn9bNSIt1-==+}
      _vcenters                      [vcenter1, vcenter2]
      _zkey                          '{+gAAAAABdLuEOOK2P--Q2jSMI-OEGP0w==+}'
      _zusr                          {+gAAAAABdLuEOard6aB6rFo459guyORN4n0ZI3pMqI-t4Mt=+}
      address                        <zvm-ip>
      alias                          <zvm-alias>
    }

2. HL7 Monitoring
"""""""""""""""""
In the DR environment, unlike the other UDM nodes, HL7 nodes would be having different IPs in the Primary and Secondary.
For monitoring HL7 nodes, the nodes in the Primary and Secondary are needs to be included in the iSiteWeb extended discovery.


3. Zerto ZVM Status
"""""""""""""""""""
IDM check all the zerto services( vpg, vra rpo etc) from both (protected, recovery) ZVMs, so when a critical condition occurs, both the ZVMs notify it to IDM dashbord, this leads to duplication of the same event in the Dashboard. In order to avoid duplicate events from IDM dashboard, the monitoring of these services would be enabled from only one ZVM at a time, based on below approach,
             1. Using Zerto VPGs API, it can be figured out whether the site/ZVM is Protected or Recovery
             2. If the site is **Protected**, and as we are getting response from protected site's ZVM, implies its active, so services would be monitored from this ZVM.
             3. If the site is **Recovery**, then using Zerto *peersites* API, the paring status to **Protected** ZVM.
                           i)  If Pairing is successful, then monitoring of Zerto services would be disabled from Recovery site
                           ii) If the Pairing is unsuccessful, then monitoring would be enabled from Recovery site.

With this approach at a time Zerto services would be getting monitored only from active ZVM, thereby duplicate notification won't be happening.

API:
    1. https://zvm_ip:zvm_port/v1/vpgs - To get Primary ZVM details
    2. https://zvm_ip:zvm_port/v1/peersites - For pairing status of primary ZVM

Service Check:
    - Disaster__Recovery__Zerto__ZVM__Status

4. Zerto Alerts Monitoring
""""""""""""""""""""""""""
For Zerto Alerts Monitoring IDM will fetch all the Zerto alerts using the Zerto 'alerts' API call.
Later with help of alert helpIdentifier, IDM will report specific alerts within different service checks.

For Zerto alerts monitoring there will be multiple "v1/alerts" API calls during execution to monitor different types of alerts, to avoid this duplicate API calls, the caching layer is introduced with 4mns TTL (TimeToLive).

This cache can be disabled by passing the argument --enable_cache with value as 'no'.

During retries or non OK previous service status cached data would not be used, rather the information would be fetched directly from the API.

For storing the zerto alerts data in the cache the key will be composed using the following convention
'<hostaddress>_zerto_alerts'

4.1. RPO Alerts Monitoring
""""""""""""""""""""""""""
The ability to have data replicated from active to the passive system such that passive system is less than or equal to 15 seconds behind the active system.
i.e. support recovery point objective of 15 seconds.

Zerto will raise alerts when the recovery RPO is not being met, when current RPO is more than the target RPO.

Zerto Alerts HelpIdentifier for RPO are:
    1. VPG0009 - The recovery RPO is not being met. The current RPO is between 15% and 25% more than the target RPO.
    2. VPG0010 - The target RPO is not being met. The current RPO is at least 25% more than the target RPO.

If current RPO is under the target RPO, no alerts will be raised and IDM will fetch the current RPO value using Zerto 'vpgs' API.

API:
    1. https://zvm_ip:zvm_port/v1/alerts - To fetch all the Zerto alerts.
    2. https://zvm_ip:zvm_port/v1/vpgs - To fetch current RPO value.

Service Check:
    - Disaster__Recovery__Zerto__RPO__Status

4.2.  VRA Alerts Monitoring
""""""""""""""""""""""""""""

VRA alerts will be triggered when there is a problem with a VRA.

API:
    1. https://zvm_ip:zvm_port/v1/alerts - To fetch all the Zerto alerts.

All the VRA alerts are divided in 5 categories:

4.2.1.	VRA_Disk_Alerts:
""""""""""""""""""""""""

This service check will cover all the disk specific VRA alerts.

Zerto Alert HelpIdentitifier for VRA Disk Alerts :
    - VRA0006   Datastore for journal disk is full
    - VRA0008	Recovery disk and VMs missing
    - VRA0009	Recovery disk missing
    - VRA0010	Recovery disks turned off
    - VRA0011	Recovery disk inaccessible
    - VRA0012	Cannot write to recovery disk
    - VRA0013	I/O error to recovery disk
    - VRA0014	Cloned disks turned off
    - VRA0015	Cloned disk inaccessible
    - VRA0016	Datastore for clone disk is full
    - VRA0017	I/O error to clone
    - VRA0018	Protected disk and VM missing
    - VRA0019	Protected disk missing
    - VRA0021	VM disk inaccessible
    - VRA0022	VM disk incompatible
    - VRA0026	Recovery disk removed
    - VRA0027	Journal disk removed
    - VRA0052	Disk visible but not recognized
    - VRA0053	System disk removed

Service Check:
    - Disaster__Recovery__Zerto__VRA_Disk__Alerts

4.2.2.	VRA_Host_Alerts:
""""""""""""""""""""""""
All the issues for VRA Host and its connectivity will be covered under this service check.

Zerto Alert HelpIdentitifier for VRA Host Alerts:
    - VRA0001	Host without VRA
    - VRA0003	Host IP changes
    - VRA0049	Host rollback failed
    - VRA0050	Wrong host password

Service Check:
    - Disaster__Recovery__Zerto__VRA_Host__Alerts

4.2.3.	VRA_Journal_Alerts:
"""""""""""""""""""""""""""

All the VRA Journal alerts, will be reported under this category.

Zerto Alert HelpIdentitifier for VRA Journal Alerts:
    - VRA0030	Journal size mismatch
    - VRA0039	Journal reached configured limit
    - VRA0040	Journal space low
    - VRA0054	VRA journal alert in public cloud
    - VRA0007	I/O error to journal

Service Check:
    - Disaster__Recovery__Zerto__VRA_Journal__Alerts

4.2.4.	VRA_Network_Alerts:
"""""""""""""""""""""""""""

If there is any network related alerts for ZVRA then it will be reported under this category.

Zerto Alert HelpIdentitifier for VRA Network Alerts:
    - VRA0025	I/O synchronization
    - VRA0002	VRA without IP
    - VRA0004	VRA lost IP
    - VRA0005	VRAs not connected
    - VRA0037	Local MAC Address Conflict
    - VRA0038	MAC Address Conflict

Service Check:
    - Disaster__Recovery__Zerto__VRA_Network__Alerts

4.2.5.	VRA_Health_Alerts:
""""""""""""""""""""""""""
This service check will cover all the general VRA specific alerts.

Zerto Alert HelpIdentitifier for VRA Health Alerts:
    - VRA0020	VM powered off
    - VRA0023	VRA cannot be registered.
    - VRA0024	VRA removed
    - VRA0028	VRA powered off
    - VRA0029	VRA memory low
    - VRA0032	VRA out-of-date
    - VRA0033	Peer VRA out-of-date
    - VRA0055	VRA target volume alert in public cloud
    - VRA0056	VRA is shutting down

Service Check:
    - Disaster__Recovery__Zerto__VRA_Health__Alerts


4.3. Zerto License Monitoring
"""""""""""""""""""""""""""""
License alerts will be triggered when there is a problem with Zerto licensing. Like Zerto license is expired or exceeded.
All the Zerto License related alerts will be report in one service service.

Service Check:
    - Disaster__Recovery__Zerto__License__Alerts

Zerto Alert HelpIdentitifier for License Alerts:
        1.	LIC0001 -- License exceeded (number of virtual machines)
        2.	LIC0002 -- License exceeded (number of CPU sockets)
        3.	LIC0003 -- License about to expire
        4.	LIC0004 -- License expired and exceeded (number of virtual machines)
        5.	LIC0006 -- License expired
        6.	LIC0007 -- License exceeded
        7.	LIC0009 -- Public cloud replication not supported
        8.	LIC0010 -- vCD not supported
        9.	LIC0011 -- Cross hypervisor replication not supported

API:
    1. https://zvm_ip:zvm_port/v1/alerts - To fetch all the Zerto alerts.
    2. https://zvm_ip:zvm_port/v1/license - To fetch current license details.


4.4.  All Other Alerts Monitoring
"""""""""""""""""""""""""""""""""

As part of this service check IDM will fetch all the Zerto alerts which are not part of any other service-checks.
And it will report 'Critical' with all the alert descriptions in one service check.

Service Check:
    - Disaster__Recovery__Zerto__Other__Alerts

API:
    1. https://zvm_ip:zvm_port/v1/alerts - To fetch all the Zerto alerts.

4.5. Zerto Auth Status Check
""""""""""""""""""""""""""""

This service check will act as a parent check for "Disaster__Recovery__Zerto__ZVM__Status" service check. It will check for any authentication or API accessibility issues.
If Zerto API is accessible, then this service check will report "OK" and enable monitoring for "Disaster__Recovery__Zerto__ZVM__Status".

Service Check:
    - Disaster__Recovery__Zerto__API__Status
